/**
 * \file AppGlobalDef.c
 *
 * \copyright Copyright (c) 2015 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * This file may be used, copied, and distributed, with or without modification, provided
 * that all copyright notices are retained; that all modifications to this file are
 * prominently noted in the modified file; and that this paragraph is not modified.
 *
 *
 *
 */
#include "AppGlobalDef.h"
#include "SysSe/Comm/Ifx_Console.h"
#include "SysSe/Ext/At25xxx/Ifx_EfsShell.h"
#include <string.h>
#include <stdio.h>
#include <stdarg.h>
#include "Configuration.h"

void App_printBoardVersion(LB_FileBoardVersion *boardVersion, IfxStdIf_DPipe *io)
{
    /* Print board type */
    switch (boardVersion->boardType)
    {
    case Lb_BoardType_HybridKit_LogicBoard:
//        IfxStdIf_DPipe_print(io, "- Board: Hybrid kit logic board"ENDL);
        break;
    case Lb_BoardType_HybridKit_DriverBoardHp2:
//        IfxStdIf_DPipe_print(io, "- Board: Hybrid kit HP2 board"ENDL);
        break;
    case Lb_BoardType_HybridKit_DriverBoardHpDrive:
//        IfxStdIf_DPipe_print(io, "- Board: Hybrid kit HPDrive board with EiceSil driver"ENDL);
        break;
    case Lb_BoardType_HybridKit_DriverBoardHpDriveEiceSense:
//        IfxStdIf_DPipe_print(io, "- Board: Hybrid kit HPDrive board with EiceSense Driver"ENDL);
        break;
    case Lb_BoardType_HybridKit_DriverBoardHpDriveEiceSenseDsc:
//        IfxStdIf_DPipe_print(io, "- Board: Hybrid kit HPDrive board with EiceSense Driver, DSC version"ENDL);
        break;
    case Lb_BoardType_undefined:
//        IfxStdIf_DPipe_print(io, "- Board: Not defined"ENDL);
        break;
    default:
//        IfxStdIf_DPipe_print(io, "- Board: Unknown"ENDL);
        break;
    }

    /* Print logic board version */
    if (boardVersion->boardVersion == LB_BoardVersion_undefined)
    {
//        IfxStdIf_DPipe_print(io, "- Board version undefined"ENDL);
    }
    else if (boardVersion->boardVersion & 0x01000000)
    {
/*
        IfxStdIf_DPipe_print(io, "- Board version: %d.x"ENDL
            , ((boardVersion->boardVersion >> 16) & 0xFF));
*/
    }
    else
    {
/*
    	IfxStdIf_DPipe_print(io, "- Board version: %d.%d"
            , ((boardVersion->boardVersion >> 16) & 0xFF)
            , ((boardVersion->boardVersion >> 8) & 0xFF));

        if ((boardVersion->boardVersion & 0xF) >= 10)
        {
            IfxStdIf_DPipe_print(io, "%1X", (boardVersion->boardVersion & 0xF));
        }

        IfxStdIf_DPipe_print(io, ENDL);
*/
    }
    if (boardVersion->number)
    {
//        IfxStdIf_DPipe_print(io, "- Board number: %d"ENDL, boardVersion->number);
    }
    else
    {
//        IfxStdIf_DPipe_print(io, "- Board number: Unknown"ENDL);
    }

    if (boardVersion->productionDate.year)
    {
/*
        IfxStdIf_DPipe_print(io, "- Board production date: %d/%d/%d (DD/MM/YYYY)"ENDL,
            boardVersion->productionDate.day,
            boardVersion->productionDate.month,
            boardVersion->productionDate.year);
*/
    }
    else
    {
//        IfxStdIf_DPipe_print(io, "- Board production date: Unknown"ENDL);
    }

    if (boardVersion->TestDate.year)
    {
//        IfxStdIf_DPipe_print(io, "- Board test date: %d/%d/%d (DD/MM/YYYY)"ENDL, boardVersion->TestDate.day, boardVersion->TestDate.month, boardVersion->TestDate.year);
//        IfxStdIf_DPipe_print(io, "- Board test status: %s"ENDL, (boardVersion->TestStatus ? "passed" : "failed"));
    }
    else
    {
//        IfxStdIf_DPipe_print(io, "- Board test date: Unknown"ENDL);
    }
}


void App_printStatusTitle(pchar title, ...)
{
    if (Ifx_g_console.standardIo)
    {
        boolean quiet = Ifx_g_console.standardIo->txDisabled;
        Ifx_g_console.standardIo->txDisabled = FALSE;

        char    message[STDIF_DPIPE_MAX_PRINT_SIZE + 1];
        va_list args;
        va_start(args, title);
        vsprintf((char *)message, title, args);
        va_end(args);

        if (quiet)
        {
//            Ifx_Console_printAlign("%s", message);
        }
        else
        {
//            Ifx_Console_printAlign("%s"ENDL, message);
//            Ifx_Console_incrAlign(CFG_CONSOLE_TAB);
        }

        Ifx_g_console.standardIo->txDisabled = quiet;
    }
}


void App_printStatus(boolean status)
{
    if (Ifx_g_console.standardIo)
    {
        boolean quiet = Ifx_g_console.standardIo->txDisabled;
        Ifx_g_console.standardIo->txDisabled = FALSE;

        if (status)
        {
            if (quiet)
            {
//                Ifx_Console_printAlign(".........[OK]"ENDL);
            }
            else
            {
//                Ifx_Console_printAlign(".........[OK]"ENDL);
//                Ifx_Console_decrAlign(CFG_CONSOLE_TAB);
            }
        }
        else
        {
            if (quiet)
            {
//                Ifx_Console_printAlign(".........[NOK]"ENDL);
            }
            else
            {
//                Ifx_Console_printAlign(".........[NOK]"ENDL);
//                Ifx_Console_decrAlign(CFG_CONSOLE_TAB);
            }
        }

        Ifx_g_console.standardIo->txDisabled = quiet;
    }
}


void App_printQspiInitializationStatus(IfxQspi_SpiMaster_Config *config)
{
    if (Ifx_g_console.standardIo)
    {
/*
        App_printStatusTitle("Initializing QSPI%d", IfxQspi_getIndex(config->qspi));
        Ifx_Console_print("(CLK=P%d.%d, MTSR=P%d.%d, MRST=P%d.%d)"
            , IfxPort_getIndex(config->pins->sclk->pin.port), config->pins->sclk->pin.pinIndex
            , IfxPort_getIndex(config->pins->mtsr->pin.port), config->pins->mtsr->pin.pinIndex
            , IfxPort_getIndex(config->pins->mrst->pin.port), config->pins->mrst->pin.pinIndex
            );
        App_printStatus(TRUE);
*/
    }
}


void App_printAnalogInputConfig(Ifx_AnalogInputF32_Config *config)
{
//    Ifx_Console_printAlign("- %s"ENDL, config->name);
    if (config->params.lut != NULL_PTR)
    {
//		Ifx_Console_printAlign("    - Gain=%f"ENDL, config->params.gain);
//		Ifx_Console_printAlign("    - Offset=%f"ENDL, config->params.offset);
    }
    else
    {
//		Ifx_Console_printAlign("    - Look-up table"ENDL);
    }
    if (config->params.filterEnabled)
    {
//        Ifx_Console_printAlign("    - Low pass filter: gain=%f, F=%f Hz"ENDL, config->params.filterConfig.gain, config->params.filterConfig.cutOffFrequency);
    }
    if (config->params.onOutOfRange)
    {
//        Ifx_Console_printAlign("    - Limit: min=%f, max=%f"ENDL, config->params.lower, config->params.upper);
    }
}


boolean App_checkInput(Ifx_AnalogInputF32 *input)
{
    boolean       success = TRUE;
    Limits_Status value;

    value   = Ifx_AnalogInputF32_getStatus(input);
    success = value == Limits_Status_Ok;

    if (value == Limits_Status_Ok)
    {
//        Ifx_Console_printAlign("- %s: Ok"ENDL, input->name);
    }
    else if (value == Limits_Status_Min)
    {
//        Ifx_Console_printAlign("- %s: NOK (limit min error, value=%f)"ENDL, input->name, input->value);
    }
    else if (value == Limits_Status_Max)
    {
//        Ifx_Console_printAlign("- %s: NOK (limit max error, value=%f)"ENDL, input->name, input->value);
    }

    return success;
}


boolean App_initEeprom(Ifx_At25xxx *eeprom, IfxQspi_SpiMaster *qspi, IfxQspi_SpiMaster_Channel *sscEeprom, const IfxQspi_Slso_Out *cs, float32 baudrate)
{
    boolean result = TRUE;
    {
        IfxQspi_SpiMaster_ChannelConfig spiChannelConfig;
        IfxQspi_SpiMaster_initChannelConfig(&spiChannelConfig, qspi);
        spiChannelConfig.base.baudrate             = baudrate;
        spiChannelConfig.base.mode.enabled         = TRUE;
        spiChannelConfig.base.mode.autoCS          = 1;
        spiChannelConfig.base.mode.loopback        = FALSE;
        spiChannelConfig.base.mode.clockPolarity   = SpiIf_ClockPolarity_idleLow;
        spiChannelConfig.base.mode.shiftClock      = SpiIf_ShiftClock_shiftTransmitDataOnTrailingEdge;
        spiChannelConfig.base.mode.dataHeading     = SpiIf_DataHeading_msbFirst;
        spiChannelConfig.base.mode.dataWidth       = 8;
        spiChannelConfig.base.mode.csActiveLevel   = Ifx_ActiveState_low;
        spiChannelConfig.base.mode.csLeadDelay     = SpiIf_SlsoTiming_2;
        spiChannelConfig.base.mode.csTrailDelay    = SpiIf_SlsoTiming_2;
        spiChannelConfig.base.mode.csInactiveDelay = SpiIf_SlsoTiming_2;/* FIXME  could be 0 clock, to be checked */
        spiChannelConfig.base.mode.parityCheck     = FALSE;
        spiChannelConfig.base.mode.parityMode      = Ifx_ParityMode_even;
        spiChannelConfig.base.errorChecks.baudrate = FALSE;
        spiChannelConfig.base.errorChecks.phase    = FALSE;
        spiChannelConfig.base.errorChecks.receive  = FALSE;
        spiChannelConfig.base.errorChecks.transmit = FALSE;

        spiChannelConfig.sls.output.pin            = cs;

        spiChannelConfig.sls.output.mode           = IfxPort_OutputMode_pushPull;
        spiChannelConfig.sls.output.driver         = IfxPort_PadDriver_cmosAutomotiveSpeed1;

        IfxQspi_SpiMaster_initChannel(sscEeprom, &spiChannelConfig);
    }
    {
        Ifx_At25xxx_Config config;
        config.sscChannel = &sscEeprom->base;
        config.device     = &Ifx_g_At25xxx_at25256;
        result           &= Ifx_At25xxx_init(eeprom, &config);
    }

    return result;
}


boolean App_detectEeprom(Ifx_At25xxx *eeprom)
{
    boolean result   = FALSE;
    uint64  testData = 0xDEADBEEF;
    uint64  backup;

    if (Ifx_At25xxx_read(eeprom, 0, (uint8 *)&backup, 8))
    {
        if (Ifx_At25xxx_write(eeprom, 0, (uint8 *)&testData, 8))
        {
            result = Ifx_At25xxx_compare(eeprom, 0, (uint8 *)&testData, 8);
        }

        Ifx_At25xxx_write(eeprom, 0, (uint8 *)&backup, 8);
    }

    return result;
}


boolean App_createEfs(Ifx_Efs *efs, Ifx_Efs_Address offset, Ifx_Efs_PartitionSize partitionSize, Ifx_Efs_FileSize defaultFileSize, boolean quiet)
{
    boolean result = TRUE;

    if (!quiet)
    {
//        Ifx_Console_print("Un-mounting existing file system..."ENDL);
    }

    Ifx_Efs_unmount(efs);

    Ifx_Efs_Init(efs, efs->eeprom);
    result &= Ifx_Efs_partitionSizeSet(efs, partitionSize);
    result &= Ifx_Efs_defaultFileSizeSet(efs, defaultFileSize);
    result &= Ifx_Efs_tableOffsetSet(efs, offset);
    result &= Ifx_Efs_maxFileCountSet(efs, Ifx_Efs_getBestMaxFileCount(efs));

    Ifx_Efs_filenameZesSet(efs, TRUE);

    if (!quiet)
    {
//        Ifx_Console_print("Formating..."ENDL);
    }

    result &= Ifx_Efs_formatFast(efs);

    if (!quiet)
    {
        if (result)
        {
//            Ifx_Console_print("Success."ENDL);
//            Ifx_EfsShell_printInfo(efs, Ifx_g_console.standardIo);
        }
        else
        {
//            Ifx_Console_print("Error."ENDL);
        }
    }

    return result;
}


boolean App_createBoardVersionFile(Ifx_Efs *efs, LB_FileBoardVersion *configuration, Lb_BoardType boardType, LB_BoardVersion version, boolean quiet)
{
    configuration->boardVersion         = version;
    configuration->boardType            = boardType;

    configuration->productionDate.year  = 0x00000000;
    configuration->productionDate.month = 0x00000000;
    configuration->productionDate.day   = 0x00000000;
    configuration->TestDate.year        = 0x00000000;
    configuration->TestDate.month       = 0x00000000;
    configuration->TestDate.day         = 0x00000000;
    configuration->TestStatus           = 0;
    configuration->number               = 0;
    return App_createConfigFile(efs, EFS_CFG_FILE_NAME_BOARD_VERSION, configuration, sizeof(*configuration), EFS_CFG_FILE_VERSION_BOARD_VERSION, quiet);
}


boolean App_updateBoardVersionFile(Ifx_Efs *efs, LB_FileBoardVersion *configuration)
{
    return App_updateConfigFile(efs, EFS_CFG_FILE_NAME_BOARD_VERSION, configuration, sizeof(*configuration));
}


boolean App_loadBoardVersionFile(Ifx_Efs *efs, LB_FileBoardVersion *configuration)
{
    return App_loadConfigFile(efs, EFS_CFG_FILE_NAME_BOARD_VERSION, configuration, sizeof(LB_FileBoardVersion), EFS_CFG_FILE_VERSION_BOARD_VERSION);
}


boolean App_createConfigFile(Ifx_Efs *efs, pchar filename, void *configuration, uint16 fileSize, LB_FileVersion fileVersion, boolean quiet)
{
    boolean      result = TRUE;
    Ifx_Efs_File file;

    if (Ifx_Efs_isLoaded(efs))
    {
        /* Create the configuration file on the logic board */
        if (!quiet)
        {
//        	Ifx_Console_print("Creating %s"ENDL, filename);
        }

    	/* Delete old file if present */
    	Ifx_Efs_fileDelete(efs, filename);

        if (Ifx_Efs_fileCreate(efs, filename, &file))
        {
            ((App_Configuration *)configuration)->fileVersion = fileVersion;

            Ifx_Efs_fileDataCrcEnable(&file);

            if (Ifx_Efs_fileWrite(&file, 0, fileSize, (uint8 *)configuration) != fileSize)
            {
                Ifx_Efs_fileDataCrcSet(&file, (uint8 *)configuration);

                if (!Ifx_Efs_fileClose(&file))
                {
                    if (!quiet)
                    {
//                        Ifx_Console_print("Error: Error while closing the file"ENDL);
                    }

                    result = FALSE;
                }
            }
            else
            {
                if (!quiet)
                {
//                    Ifx_Console_print("Error: Data not written to the EEPROM"ENDL);
                }

                result = FALSE;
            }
        }
        else
        {
            if (!quiet)
            {
//                Ifx_Console_print("Error: Can't create the file"ENDL);
            }

            result = FALSE;
        }
    }
    else
    {
        if (!quiet)
        {
//            Ifx_Console_print("Error: No file system loaded"ENDL);
        }

        result = FALSE;
    }

    return result;
}


boolean App_updateConfigFile(Ifx_Efs *efs, pchar filename, void *configuration, uint16 fileSize)
{
    boolean      result = TRUE;
    Ifx_Efs_File file;

    if (Ifx_Efs_isLoaded(efs))
    {
        /* Create the configuration file on the logic board */
        if (Ifx_Efs_fileOpen(efs, filename, &file))
        {
            if (Ifx_Efs_fileWrite(&file, 0, fileSize, (uint8 *)configuration) != fileSize)
            {
                Ifx_Efs_fileDataCrcSet(&file, (uint8 *)configuration);

                if (!Ifx_Efs_fileClose(&file))
                {
//                    Ifx_Console_print("Error: Error while closing the file"ENDL);
                    result = FALSE;
                }
            }
            else
            {
//                Ifx_Console_print("Error: Data not written to the EEPROM"ENDL);
                result = FALSE;
            }
        }
        else
        {
//            Ifx_Console_print("Error: Can't open the file %s"ENDL, filename);
            result = FALSE;
        }
    }
    else
    {
//        Ifx_Console_print("Error: No file system loaded"ENDL);
        result = FALSE;
    }

    return result;
}


boolean App_loadConfigFile(Ifx_Efs *efs, pchar filename, void *configuration, uint16 fileSize, LB_FileVersion fileVersion)
{
    boolean      result = TRUE;

    Ifx_Efs_File file;

    if (Ifx_Efs_fileExist(efs, filename))
    {
        if (Ifx_Efs_fileOpen(efs, filename, &file))
        {
            /* Get the file version */
            LB_FileVersion version;

            if (Ifx_Efs_fileRead(
                    &file,
                    offsetof(App_Configuration, fileVersion),
                    sizeof(LB_FileVersion),
                    (uint8 *)&version)
                == 0)
            {
                if (version == fileVersion)
                {
                    if (fileSize == Ifx_Efs_fileSizeGet(&file))
                    {
                        if (Ifx_Efs_fileRead(
                                &file,
                                0,
                                fileSize,
                                (uint8 *)configuration)
                            == 0)
                        {
                            result &= Ifx_Efs_fileDataCrcCheck(&file, (uint8 *)configuration);
                        }
                    }
                    else
                    {
                        return FALSE;
                    }
                }
                else
                {
                    result = FALSE; /* Unsupported version*/
                }
            }
        }
        else
        {
            result = FALSE; /* Error while opening the configuration file */
        }
    }
    else
    {
        result = FALSE;
    }

    return result;
}


boolean protectEeprom(Ifx_At25xxx *eeprom)
{
    boolean                     result;
    uint8                       protectionIndex;
    Ifx_At25xxx_ProtectionState protectedBlocks;
    Ifx_At25xxx_ProtectionState unprotectedBlocks;
    Ifx_At25xxx_ProtectionState statusRegister;
    result = Ifx_At25xxx_getProtection(eeprom, &protectionIndex, &protectedBlocks, &unprotectedBlocks, &statusRegister);

    if (!((protectionIndex == 1)
          && (protectedBlocks == Ifx_At25xxx_ProtectionState_protected)
          && (unprotectedBlocks == Ifx_At25xxx_ProtectionState_protected)
          && (statusRegister == Ifx_At25xxx_ProtectionState_protected)
          ))
    {
        result &= Ifx_At25xxx_setProtection(eeprom, FALSE, 1, Ifx_At25xxx_ProtectionState_protected,
            Ifx_At25xxx_ProtectionState_protected, Ifx_At25xxx_ProtectionState_protected);
    }

    return result;
}


boolean unprotectEeprom(Ifx_At25xxx *eeprom)
{
    boolean                     result;
    uint8                       protectionIndex;
    Ifx_At25xxx_ProtectionState protectedBlocks;
    Ifx_At25xxx_ProtectionState unprotectedBlocks;
    Ifx_At25xxx_ProtectionState statusRegister;
    result = Ifx_At25xxx_getProtection(eeprom, &protectionIndex, &protectedBlocks, &unprotectedBlocks, &statusRegister);

    if (!((protectionIndex == 0)
          && (protectedBlocks == Ifx_At25xxx_ProtectionState_protected)
          && (unprotectedBlocks == Ifx_At25xxx_ProtectionState_protected)
          && (statusRegister == Ifx_At25xxx_ProtectionState_protected)
          ))
    {
        result &= Ifx_At25xxx_setProtection(eeprom, FALSE, 0, Ifx_At25xxx_ProtectionState_protected,
            Ifx_At25xxx_ProtectionState_protected, Ifx_At25xxx_ProtectionState_protected);
    }

    return result;
}
