/**
 * \file AppInterface.c
 * \brief Inverter interface.
 *
 * \copyright Copyright (c) 2015 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * This file may be used, copied, and distributed, with or without modification, provided
 * that all copyright notices are retained; that all modifications to this file are
 * prominently noted in the modified file; and that this paragraph is not modified.
 */

#include "AppInterface.h"
#include "Main.h"
#include "Library/AppLibrary.h"
#include "Library/AppHwDebugMode.h"

void ECU_setMotorPwmDuty(ECU_MotorIndex motorIndex, float32 *duty)
{
    IFX_ASSERT(IFX_VERBOSE_LEVEL_ERROR, motorIndex == 0); /* This code only handles 1 motor */

    Ifx_TimerValue period = IfxStdIf_Inverter_getPeriod(g_Lb.stdIf.inverter);
    Ifx_TimerValue tOn[3] = {period *duty[0], period * duty[1], period * duty[2]};

    IfxStdIf_Inverter_setPwm(g_Lb.stdIf.inverter, tOn);
}


void ECU_setInverterEnabled(boolean enable)
{
    if (enable)
    {
        IfxStdIf_Inverter_enable(g_Lb.stdIf.inverter);
    }
    else
    {
        IfxStdIf_Inverter_disable(g_Lb.stdIf.inverter);
    }
}


IfxStdIf_Pos* ECU_getPositionSensorAddress(ECU_MotorIndex motorIndex, ECU_PositionSensor sensor)
{
    switch (sensor)
    {
    case ECU_PositionSensor_encoder:
        return g_Lb.stdIf.encoder->driver ?  g_Lb.stdIf.encoder : NULL_PTR;
    case ECU_PositionSensor_tle5012:
    	return g_Lb.stdIf.encoder->driver ?  g_Lb.stdIf.encoder : NULL_PTR; /* iGMR encoder signal is used at run-time */
    case ECU_PositionSensor_ad2s1210:
    	return g_Lb.stdIf.ad2s1210->driver ?  g_Lb.stdIf.ad2s1210 : NULL_PTR;
    case ECU_PositionSensor_internalResolver0:
    	return g_Lb.stdIf.dsadcRdc[0]->driver ?  g_Lb.stdIf.dsadcRdc[0] : NULL_PTR;
    case ECU_PositionSensor_internalResolver1:
    	return g_Lb.stdIf.dsadcRdc[1]->driver ?  g_Lb.stdIf.dsadcRdc[1] : NULL_PTR;
    case ECU_PositionSensor_virtual:
    	if(g_App.hwState.specialMode == AppModeSpecial_normal)
    	{
        	return &g_appLibrary.motorControl.stdifVirtualPosition;
    	}
    	else
    	{
            return NULL_PTR;
    	}
    case ECU_PositionSensor_none:
    	return NULL_PTR;
    default:
        IFX_ASSERT(IFX_VERBOSE_LEVEL_ERROR, FALSE); /* Sensor not available */
        return NULL_PTR;
    }
}
void ECU_selectPositionSensor(ECU_MotorIndex motorIndex, ECU_PositionSensor sensor)
{
    switch (sensor)
    {
    case ECU_PositionSensor_encoder:
        g_Lb.stdIf.positionSensorX[0] = g_Lb.stdIf.encoder->driver ?  g_Lb.stdIf.encoder : NULL_PTR;
        break;
    case ECU_PositionSensor_tle5012:
        g_Lb.stdIf.positionSensorX[0] = g_Lb.stdIf.encoder->driver ?  g_Lb.stdIf.encoder : NULL_PTR; /* iGMR encoder signal is used at run-time */
        break;
    case ECU_PositionSensor_ad2s1210:
        g_Lb.stdIf.positionSensorX[0] = g_Lb.stdIf.ad2s1210->driver ?  g_Lb.stdIf.ad2s1210 : NULL_PTR;
        break;
    case ECU_PositionSensor_internalResolver0:
        g_Lb.stdIf.positionSensorX[0] = g_Lb.stdIf.dsadcRdc[0]->driver ?  g_Lb.stdIf.dsadcRdc[0] : NULL_PTR;
        break;
    case ECU_PositionSensor_internalResolver1:
        g_Lb.stdIf.positionSensorX[0] = g_Lb.stdIf.dsadcRdc[1]->driver ?  g_Lb.stdIf.dsadcRdc[1] : NULL_PTR;
        break;
    case ECU_PositionSensor_virtual:
    	if(g_App.hwState.specialMode == AppModeSpecial_normal)
    	{
    		g_Lb.stdIf.positionSensorX[0] = &g_appLibrary.motorControl.stdifVirtualPosition;
    	}
    	else
    	{
            IFX_ASSERT(IFX_VERBOSE_LEVEL_ERROR, FALSE); /* Sensor not available */
    	}
        break;
    case ECU_PositionSensor_none:
		g_Lb.stdIf.positionSensorX[0] = NULL_PTR;
    	break;
    default:
        IFX_ASSERT(IFX_VERBOSE_LEVEL_ERROR, FALSE); /* Sensor not available */
        break;
    }
}
void ECU_selectPositionSensorR(ECU_MotorIndex motorIndex, ECU_PositionSensor sensor)
{
    switch (sensor)
    {
    case ECU_PositionSensor_encoder:
        g_Lb.stdIf.positionSensorX[1] = g_Lb.stdIf.encoder->driver ?  g_Lb.stdIf.encoder : NULL_PTR;
        break;
    case ECU_PositionSensor_tle5012:
        g_Lb.stdIf.positionSensorX[1] = g_Lb.stdIf.encoder->driver ?  g_Lb.stdIf.encoder : NULL_PTR; /* iGMR encoder signal is used at run-time */
        break;
    case ECU_PositionSensor_ad2s1210:
        g_Lb.stdIf.positionSensorX[1] = g_Lb.stdIf.ad2s1210->driver ?  g_Lb.stdIf.ad2s1210 : NULL_PTR;
        break;
    case ECU_PositionSensor_internalResolver0:
        g_Lb.stdIf.positionSensorX[1] = g_Lb.stdIf.dsadcRdc[0]->driver ?  g_Lb.stdIf.dsadcRdc[0] : NULL_PTR;
        break;
    case ECU_PositionSensor_internalResolver1:
        g_Lb.stdIf.positionSensorX[1] = g_Lb.stdIf.dsadcRdc[1]->driver ?  g_Lb.stdIf.dsadcRdc[1] : NULL_PTR;
        break;
    case ECU_PositionSensor_virtual:
        g_Lb.stdIf.positionSensorX[1] = &g_appLibrary.motorControl.stdifVirtualPosition;
        break;
    case ECU_PositionSensor_none:
		g_Lb.stdIf.positionSensorX[1] = NULL_PTR;
    	break;
    default:
        IFX_ASSERT(IFX_VERBOSE_LEVEL_ERROR, FALSE); /* Sensor not available */
        break;
    }
}

IfxStdIf_Pos* ECU_getPositionSensor(ECU_MotorIndex motorIndex)
{
	return g_Lb.stdIf.positionSensorX[0];
}
IfxStdIf_Pos* ECU_getPositionSensorR(ECU_MotorIndex motorIndex)
{
	return g_Lb.stdIf.positionSensorX[1];
}

void ECU_setPositionSensor(ECU_MotorIndex motorIndex, IfxStdIf_Pos* sensor)
{
	g_Lb.stdIf.positionSensorX[0] = sensor;
}
void ECU_setPositionSensorR(ECU_MotorIndex motorIndex, IfxStdIf_Pos* sensor)
{
	g_Lb.stdIf.positionSensorX[1] = sensor;
}



void ECU_setMotorPwmEnabled(ECU_MotorIndex motorIndex, Ifx_Pwm_Mode mode)
{
    IFX_ASSERT(IFX_VERBOSE_LEVEL_ERROR, motorIndex == 0); /* This code only handles 1 motor */

    if (mode != Ifx_Pwm_Mode_off)
    {
        IfxStdIf_Inverter_setPwmOn(g_Lb.stdIf.inverter, mode);
    }
    else
    {
        IfxStdIf_Inverter_setPwmOff(g_Lb.stdIf.inverter);
    }
}


float32 ECU_getInverterVdc(ECU_MotorIndex motorIndex)
{
    IFX_ASSERT(IFX_VERBOSE_LEVEL_ERROR, motorIndex == 0); /* This code only handles 1 motor */
    return 250.0;
//    return IfxStdIf_Inverter_getVdc(g_Lb.stdIf.inverter);
}

void ECU_getInverterIgbtTemp(ECU_MotorIndex motorIndex, float32 *tempArray)
{
    IFX_ASSERT(IFX_VERBOSE_LEVEL_ERROR, motorIndex == 0); /* This code only handles 1 motor */
    IfxStdIf_Inverter_getSwitchTemps(g_Lb.stdIf.inverter, tempArray);
}

void ECU_getMotorPhaseCurrents(ECU_MotorIndex motorIndex, float32 *currents)
{
    IFX_ASSERT(IFX_VERBOSE_LEVEL_ERROR, motorIndex == 0); /* This code only handles 1 motor */
    IfxStdIf_Inverter_getPhaseCurrents(g_Lb.stdIf.inverter, currents);
}


float32 ECU_getMotorMechanicalPosition(ECU_MotorIndex motorIndex)
{
    IFX_ASSERT(IFX_VERBOSE_LEVEL_ERROR, motorIndex == 0); /* This code only handles 1 motor */
    return g_Lb.stdIf.positionSensorX[0] ? IfxStdIf_Pos_getPosition(g_Lb.stdIf.positionSensorX[0]) : 0.0;
}


float32 ECU_getMotorMechanicalSpeed(ECU_MotorIndex motorIndex)
{
    IFX_ASSERT(IFX_VERBOSE_LEVEL_ERROR, motorIndex == 0); /* This code only handles 1 motor */
    return g_Lb.stdIf.positionSensorX[0] ? IfxStdIf_Pos_getSpeed(g_Lb.stdIf.positionSensorX[0]) : 0.0;
}


ECU_InverterStatus ECU_getInverterStatus(ECU_MotorIndex motorIndex)
{
    uint32                   i;
    ECU_InverterStatus       status;
    IfxStdIf_Inverter_Status inverterStatus;
    IFX_ASSERT(IFX_VERBOSE_LEVEL_ERROR, motorIndex == 0); /* This code only handles 1 motor */

    inverterStatus                 = IfxStdIf_Inverter_getStatus(g_Lb.stdIf.inverter);

    status.positionSensor1Error    = g_Lb.stdIf.positionSensorX[0] ? IfxStdIf_Pos_getFault(g_Lb.stdIf.positionSensorX[0]).status != 0 : 0;
    status.positionSensor2Error    = g_Lb.stdIf.positionSensorX[1] ? IfxStdIf_Pos_getFault(g_Lb.stdIf.positionSensorX[1]).status != 0 : 0;
    // Fault is low active
    status.phase1TopDriverError    = inverterStatus.phase1TopDriverError;
    status.phase2TopDriverError    = inverterStatus.phase2TopDriverError;
    status.phase3TopDriverError    = inverterStatus.phase3TopDriverError;
    status.phase1BottomDriverError = inverterStatus.phase1BottomDriverError;
    status.phase2BottomDriverError = inverterStatus.phase2BottomDriverError;
    status.phase3BottomDriverError = inverterStatus.phase3BottomDriverError;
    /* Jimmy Create for gate driver enable pin */
    status.faultGDEN			   = inverterStatus.faultGDEN;
    status.faultPBEN			   = inverterStatus.faultPBEN;
    /* end of created */
    status.faultMain               = ((inverterStatus.error != 0) || (status.positionSensor1Error != 0)) ? 1 : 0;
    status.currentSensor1Error     = inverterStatus.phase1CurrentSensorError;
    status.currentSensor2Error     = inverterStatus.phase2CurrentSensorError;
    status.currentSensor3Error     = inverterStatus.phase3CurrentSensorError;

    status.faultInput     = inverterStatus.faultInput;
    status.faultLimit     = inverterStatus.faultLimit;
    status.faultIom     = inverterStatus.faultIom;

    IfxStdIf_Inverter_getSwitchTemps(g_Lb.stdIf.inverter, status.igbtTemp);

    status.motorTemp = *g_Lb.stdIf.motorTemp;

#if 0 // Jovi
    for (i = 0; i < 4; i++)
    {
        status.in[i] = *g_Lb.stdIf.in[i];
    }
#endif

    status.tempBoard = *g_Lb.stdIf.tempBoard;
    status.vAna50    = *g_Lb.stdIf.vAna50;
    status.vRef50    = *g_Lb.stdIf.vRef50;
    status.vDig50    = *g_Lb.stdIf.vDig50;
    status.kl30      = *g_Lb.stdIf.kl30;

    return status;
}


void ECU_getMotorConfiguration(ECU_MotorIndex motorIndex, Ifx_MotorModelConfigF32_File *configuration)
{
    IFX_ASSERT(IFX_VERBOSE_LEVEL_ERROR, motorIndex == 0); /* This code only handles 1 motor */
    *configuration = g_Lb.motorConfiguration.data;
}
