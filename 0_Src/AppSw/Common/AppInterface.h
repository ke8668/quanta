/**
 * \file AppInterface.c
 * \brief Inverter interface.
 *
 * \copyright Copyright (c) 2015 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * This file may be used, copied, and distributed, with or without modification, provided
 * that all copyright notices are retained; that all modifications to this file are
 * prominently noted in the modified file; and that this paragraph is not modified.
 *
 * \defgroup docsrc_application_interface 5 - ECU interface
 * \ingroup main_sourceCodeApplication
 *
 * The ECU interface enables easy change of the control algorithm by swapping the "Motor control" block. A set of APIs is defined to enable communication with the hw:
 * API  name                             | Description
 * ------------------------------------- | -----------------------------------------
 * \ref ECU_getInverterVdc()             | \copybrief ECU_getInverterVdc \copydetails  ECU_getInverterVdc
 * \ref ECU_getInverterIgbtTemp()        | \copybrief ECU_getInverterIgbtTemp \copydetails  ECU_getInverterIgbtTemp
 * \ref ECU_getMotorPhaseCurrents()      | \copybrief ECU_getMotorPhaseCurrents \copydetails  ECU_getMotorPhaseCurrents
 * \ref ECU_getMotorMechanicalPosition() | \copybrief ECU_getMotorMechanicalPosition \copydetails  ECU_getMotorMechanicalPosition
 * \ref ECU_getMotorMechanicalSpeed()    | \copybrief ECU_getMotorMechanicalSpeed \copydetails  ECU_getMotorMechanicalSpeed
 * \ref ECU_getInverterStatus()          | \copybrief ECU_getInverterStatus \copydetails  ECU_getInverterStatus
 * \ref ECU_setInverterEnabled()         | \copybrief ECU_setInverterEnabled \copydetails  ECU_setInverterEnabled
 * \ref ECU_setMotorPwmEnabled()         | \copybrief ECU_setMotorPwmEnabled \copydetails  ECU_setMotorPwmEnabled
 * \ref ECU_setMotorPwmDuty()            | \copybrief ECU_setMotorPwmDuty \copydetails  ECU_setMotorPwmDuty
 * \ref ECU_selectPositionSensor()       | \copybrief ECU_selectPositionSensor \copydetails  ECU_selectPositionSensor
 *
 *
 * This is configured by the switch \ref CFG_SETUP. The default setting is  \ref CFG_SETUP_LIBRARY
 *
 *
 * It also allows to swap the iLLD block to be compatible with a different microcontroller derivative. See the www.myinfineon.com website for additional derivative package download.
 *
 *
 * \section Standard interface
 *
 * The standard interface layer, enables to communicate with a peripheral always using the same interface. As an example, no code change is required to use either the GPT12 encoder or the AD2S1210 position sensor except the additional initialization of the driver itself.
 *
 * \section doc_softwareStructure_timing Timing
 *
 *
 * Slot name                             | Description                                                                                                                   | Usage in AppLibrary
 * ------------------------------------- | -------------------------------------------------------------------------------------------------- | --------------------------------------------
 * \ref ECU_slotInit()                       | \copybrief ECU_slotInit \copydetails ECU_slotInit             | Initialize the motor control algorithm, select position sensor, configure oscilloscope
 * \ref ECU_slotBackground()                 | \copybrief ECU_slotBackground \copydetails ECU_slotBackground | Used to poll for message to be displayed on the terminal
 * \ref ECU_slotOneMs()                      | \copybrief ECU_slotOneMs \copydetails ECU_slotOneMs           | Used to handle inputs from external, fill the new CAN message contents when in run mode
 * \ref ECU_slotShutdown()               	 | \copybrief ECU_slotShutdown \copydetails ECU_slotShutdown 	 | Used to switch off the inverter
 * \ref ECU_slotTenMs()                      | \copybrief ECU_slotTenMs \copydetails ECU_slotTenMs           | Not used
 * \ref ECU_slotPwmPeriodStart()             | \copybrief ECU_slotPwmPeriodStart \copydetails ECU_slotPwmPeriodStart | Not used
 * \ref ECU_slotTriggerPoint()               | \copybrief ECU_slotTriggerPoint \copydetails ECU_slotTriggerPoint     | Not used
 * \ref ECU_slotEndOfPhaseCurrentConversion()| \copybrief ECU_slotEndOfPhaseCurrentConversion \copydetails ECU_slotEndOfPhaseCurrentConversion| Execute the motor control algorithm
 *
 *
 * The below picture presents the periodical slots:
 * \image html "Application_Slots.png" "Periodical slots"
 *
 *
 * \note The gain in flexibility resulting from this setup adds some overhead which leads to increase code size and CPU resource required. This is accessed as neglectable for the demo application. It should be re-assessed for real production software.
 *
 *
 */
#ifndef APPINTERFACE_H
#define APPINTERFACE_H
#include "Configuration.h"
#include "SysSe/Bsp/Bsp.h"
#include "StdIf/IfxStdIf_Pos.h"

#include "SysSe/EMotor/Ifx_MotorModelConfigF32.h"
#include "AppGlobalDef.h"
/** \brief Motor status
 *
 */
typedef struct
{
    uint32  positionSensor1Error : 1;               /**<\brief  Position sensor 1. 0: no error, 1: position sensor error */
    uint32  positionSensor2Error : 1;               /**<\brief  Position sensor 2. 0: no error, 1: position sensor error */
    uint32  phase1TopDriverError : 2;               /**<\brief  Phase 1 top driver error. 0: no fault, 1: short circuit, 2:under voltage, 3:fault(generic) */
    uint32  phase1BottomDriverError : 2;            /**<\brief  Phase 1 bottom driver error */
    uint32  phase2TopDriverError : 2;               /**<\brief  Phase 2 top driver error */
    uint32  phase2BottomDriverError : 2;            /**<\brief  Phase 2 bottom driver error */
    uint32  phase3TopDriverError : 2;               /**<\brief  Phase 3 top driver error */
    uint32  phase3BottomDriverError : 2;            /**<\brief  Phase 3 bottom driver error */
    uint32  currentSensor1Error : 1;                /**<\brief  Current sensor 1 error  */
    uint32  currentSensor2Error : 1;                /**<\brief  Current sensor 2 error  */
    uint32  currentSensor3Error : 1;                /**<\brief  Current sensor 3 error  */
    uint32  faultMain : 1;                          /**<\brief  Inverter main fault */
    uint32  faultInput : 1;                         /**<\brief  Inverter input fault */
    uint32  faultLimit : 1;                         /**<\brief  Inverter limit fault */
    uint32  faultIom : 1;                           /**<\brief  Inverter IOM fault */
    uint32  faultGDEN : 1;							/* Jimmy created for gate driver enable pin */
    uint32  faultPBEN : 1;							/* Jimmy created for Power board enable pin */
    float32 igbtTemp[ECU_PHASE_PER_MOTOR_COUNT];    /**<\brief  IGBT temperature per phase */
    float32 motorTemp;                              /**<\brief  Motor temperature in �C */
    float32 in[4];
    float32 tempBoard;
    float32 vAna50;
    float32 vRef50;
    float32 vDig50;
    float32 kl30;
}ECU_InverterStatus;

typedef uint32 ECU_MotorIndex; /**< \brief motor index */

/** \addtogroup docsrc_application_interface
 * \{ */
/** \name Slots
 * \{ */

/** \brief This slot is periodically called in the background task when in state AppMode_run
 *
 * This is a special slot running at the lowest priority. It is called from the background loop.
 * \note The implementation of the slot function shall return periodically because other background tasks might required to be executed in the same background loop.
 */
void ECU_slotBackground(void);

/** \brief Slot executed every milliseconds when in state AppMode_run
 * This slot is called every ms, between the call to \ref Ifx_CanHandler_processInbox() and \ref Ifx_CanHandler_processOutbox()
 * This slot is run from an interrupt with priority ISR_PRIORITY_TIMER_1MS
 */
void ECU_slotOneMs(void);

/** \brief Slot executed when the shutdown state is entered
 * This slot is run from the backgroung loop
 */
void ECU_slotShutdown(void);

/** \brief Slot executed every ten millisecond when in state AppMode_run
 *
 * This slot is run from an interrupt with priority \ref ISR_PRIORITY_TIMER_10MS
 */
void ECU_slotTenMs(void);

/** \brief Slot executed at each PWM period start when in state AppMode_run
 *
 * This slot is run from an interrupt with priority \ref ISR_PRIORITY_M0_PWMHL_PERIOD
 */
void ECU_slotPwmPeriodStart(ECU_MotorIndex motorIndex);

/** \brief This slot is called when the ADC is triggered for the phase current conversion when in state AppMode_run
 *
 * This slot is run from an interrupt with priority \ref ISR_PRIORITY_M0_PWMHL_TRIGGER
 */
void ECU_slotTriggerPoint(ECU_MotorIndex motorIndex);

/** \brief This slot is called after the ADC finishes the conversion of the phase currents, and the position is updated when in state AppMode_run.
 *
 * The Osci is updated after the call to the slot.
 * This slot is run from an interrupt with priority \ref ISR_PRIORITY_ADC_M0
 *
 */
void ECU_slotEndOfPhaseCurrentConversion(ECU_MotorIndex motorIndex);

/** \brief This slot is called once the ECU hardware is initialised (Transition to AppMode_run)
 *
 * Its purpose is to initialise the "Motor control Algorithm block".
 */
boolean ECU_slotInit(void);
/** \} */

/** \name Output access APIs
 * \{ */

/** \brief Set motor PWM duty cycle
 * \param motorIndex Motor index
 * \param duty Pointer to an array of phase duty cycles in percent. The array size must be equal to the number of motor phases defined by ECU_PHASE_PER_MOTOR_COUNT
 * \return none
 */
void ECU_setMotorPwmDuty(ECU_MotorIndex motorIndex, float32 *duty);

/** \brief Enable / disable the inverter (driver ON/OFF)
 * \param enable If TRUE, the corresponding motor inverter is enabled, else disabled
 * \return none
 */
void ECU_setInverterEnabled(boolean enable);

/** \brief Select the postion sensor to use
 * \param motorIndex Motor index
 * \param sensor sensor to be used for the motor
 * \return none
 */
void ECU_selectPositionSensor(ECU_MotorIndex motorIndex, ECU_PositionSensor sensor);
IfxStdIf_Pos* ECU_getPositionSensorAddress(ECU_MotorIndex motorIndex, ECU_PositionSensor sensor);
void ECU_selectPositionSensorR(ECU_MotorIndex motorIndex, ECU_PositionSensor sensor);
IfxStdIf_Pos* ECU_getPositionSensor(ECU_MotorIndex motorIndex);
IfxStdIf_Pos* ECU_getPositionSensorR(ECU_MotorIndex motorIndex);
void ECU_setPositionSensor(ECU_MotorIndex motorIndex, IfxStdIf_Pos* sensor);
void ECU_setPositionSensorR(ECU_MotorIndex motorIndex, IfxStdIf_Pos* sensor);

/** \brief Enable / disable the motor PWM (PWM ON/OFF)
 * \param motorIndex Motor index
 * \param mode Set the PWM mode, if mode is Ifx_Pwm_Mode_off, the PWM is disabled. when disabled, all PWM output are set to inactive
 * \return none
 */
void ECU_setMotorPwmEnabled(ECU_MotorIndex motorIndex, Ifx_Pwm_Mode mode);
/** \} */

/** \name Input access APIs
 * \{ */

/** \brief Return the motor DC-link voltage in V
 * \param motorIndex Motor index
 * \return Return the motor DC-link voltage in V
 */
float32 ECU_getInverterVdc(ECU_MotorIndex motorIndex);

/** Return the IGBT temperature in �C
 *
 * \param motorIndex Motor index
 * \param tempArray array of 3 temperature value (U, V, W)
 * \return the IGBT temperature in �C
 *
 */
void ECU_getInverterIgbtTemp(ECU_MotorIndex motorIndex, float32 *tempArray);

/** \brief Return the motor phase currents in A
 * \param motorIndex Motor index
 * \param currents Pointer to an array of phase currents. The array size must be equal to the number of motor phases defined by ECU_PHASE_PER_MOTOR_COUNT
 *
 * \return None
 */
void ECU_getMotorPhaseCurrents(ECU_MotorIndex motorIndex, float32 *currents);

/** \brief Return the motor mechanical position in rad
 * \param motorIndex Motor index
 *
 * \return None
 */
float32 ECU_getMotorMechanicalPosition(ECU_MotorIndex motorIndex);

/** \brief Return the motor mechanical speed in rad/s
 * \param motorIndex Motor index
 *
 * \return None
 */
float32 ECU_getMotorMechanicalSpeed(ECU_MotorIndex motorIndex);

/** \brief Return the motor status
 * \param motorIndex Motor index
 *
 * \return Return the motor status
 */
ECU_InverterStatus ECU_getInverterStatus(ECU_MotorIndex motorIndex);

/** \} */

/** \name configuration APIs
 * \{ */

/** Return the motor configuration
 * \param motorIndex Motor index
 * \param configuration Returned motor configuration
 */
void ECU_getMotorConfiguration(ECU_MotorIndex motorIndex, Ifx_MotorModelConfigF32_File *configuration);
/** \} */
/** \} */

#endif /* APPINTERFACE_H */
