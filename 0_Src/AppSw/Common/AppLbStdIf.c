/**
 * \file AppLbStdIf.c
 * \brief Standard interface: Logic board
 *
 * \version disabled
 * \copyright Copyright (c) 2013 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 *
 */
#include "AppLbStdIf.h"
#include "Configuration.h"
#include "SysSe/Comm/Ifx_Shell.h"
#include "Lb.h"
#include "Main.h"

boolean AppLbStdIf_shellSetupCommon(AppLbStdIf *stdif, pchar args, void *data, IfxStdIf_DPipe *io)
{
    LB *board = &g_Lb;
    boolean              result              = FALSE;
    LB_FileBoardVersion *boardVersion        = &board->boardVersion;

    if (Ifx_Shell_matchToken(&args, "lb") != FALSE)
    {
        if (Ifx_Shell_matchToken(&args, "efs") != FALSE)
        {
            if (board->boardVersion.fileVersion == Lb_FileVersion_undefined)
            {
                IfxStdIf_DPipe_print(io, "Please set the board version with 'setup lb version <version> <revision> <step>'"ENDL);
            }
            else
            {
                IfxStdIf_DPipe_print(io, "Creating configuration file system"ENDL);
                AppLbStdIf_createConfigEfs(stdif, FALSE);
                if (!AppLbStdIf_createConfigFile(stdif, FALSE))
                {
                    IfxStdIf_DPipe_print(io, "Error board configuration could not be written"ENDL);
                }
            }

            result = TRUE;
        }
    	else if (Ifx_Shell_matchToken(&args, "load") != FALSE)
    	{
    		if (!App_loadBoardVersionFile(&board->driver.efsBoard, boardVersion))
    		{
    			IfxStdIf_DPipe_print(io, "Error board version could not be load"ENDL);
    		}

    		if (!AppLbStdIf_loadConfigFile(stdif))
    		{
    			IfxStdIf_DPipe_print(io, "Error board configuration could not be load"ENDL);
    		}

    		result = TRUE;
    	}
    	else if (!App_isLocked())
    	{
            boolean              updateBoardVersion  = FALSE;
			if (Ifx_Shell_matchToken(&args, "version") != FALSE)
			{
				uint32 version;
				uint32 revision;
				uint32 step = 0;

				if ((Ifx_Shell_parseUInt32(&args, &version, FALSE) != FALSE)
					&& (Ifx_Shell_parseUInt32(&args, &revision, FALSE) != FALSE)
					)
				{
					Ifx_Shell_parseUInt32(&args, &step, TRUE);

					boardVersion->boardVersion = (version << 16) | (revision << 8) | step;

					if (!Ifx_Efs_isLoaded(&board->driver.efsBoard))
					{
						{
							/* Initialize the EEPROM QSPI to match the board version */
							/* Board might have not been detected at startup  because file system was not found */
							if (version == 2)
							{
								App_initEeprom(&board->driver.eeprom, &board->driver.qspi0, &board->driver.sscEeprom, &IfxQspi0_SLSO0_P20_8_OUT, 3e6);
								IfxStdIf_DPipe_print(io, "SPI initialized for version %d.%d"ENDL, version, revision);
							}
							else if (version == 3)
							{
								if (revision >= 3)
								{
									App_initEeprom(&board->driver.eeprom, &board->driver.qspi0, &board->driver.sscEeprom, &IfxQspi0_SLSO0_P20_8_OUT, 3e6);
									IfxStdIf_DPipe_print(io, "SPI initialized for version %d.%d"ENDL, version, revision);
								}
								else
								{
									App_initEeprom(&board->driver.eeprom, &board->driver.qspi0, &board->driver.sscEeprom, &IfxQspi0_SLSO8_P20_6_OUT, 3e6);
									IfxStdIf_DPipe_print(io, "SPI initialized for version %d.%d"ENDL, version, revision);
								}
							}
							else
							{
								IfxStdIf_DPipe_print(io, "WARNING: SPI not initialized"ENDL);
							}


						}

						IfxStdIf_DPipe_print(io, "Creating board version file system"ENDL);
						unprotectEeprom(&board->driver.eeprom);
						if (version == 3)
						{
							if (!App_createEfs(&board->driver.efsBoard, CFG_LB30_BOARD_EFS_OFFSET, CFG_LB30_BOARD_EFS_PARTITION_SIZE, CFG_LB_BOARD_EFS_FILE_SIZE, FALSE))
							{
								Ifx_At25xxx_printStatus(board->driver.efsBoard.eeprom, io);
							}

						}
						protectEeprom(&board->driver.eeprom);
					}


					if (boardVersion->fileVersion != EFS_CFG_FILE_VERSION_BOARD_VERSION)
					{
						unprotectEeprom(&board->driver.eeprom);
						if (!App_createBoardVersionFile(&board->driver.efsBoard, boardVersion, Lb_BoardType_HybridKit_LogicBoard, board->boardVersion.boardVersion, FALSE))
						{
							IfxStdIf_DPipe_print(io, "Error board version could not be written"ENDL);
							Ifx_At25xxx_printStatus(board->driver.efsBoard.eeprom, io);
						}
						protectEeprom(&board->driver.eeprom);
					}
					else
					{
						updateBoardVersion  = TRUE;
					}

					if (!AppLbStdIf_isConfigFile(stdif))
					{
						IfxStdIf_DPipe_print(io, "Creating configuration file system"ENDL);
						AppLbStdIf_createConfigEfs(stdif, FALSE);
						AppLbStdIf_createConfigFile(stdif, FALSE);
					}

					result = TRUE;
				}
			}
			else if (Ifx_Shell_matchToken(&args, "number") != FALSE)
			{
				uint32 value;

				if ((Ifx_Shell_parseUInt32(&args, &value, FALSE) != FALSE))
				{
					board->boardVersion.number = (uint8)value;
					updateBoardVersion         = TRUE;
					result                     = TRUE;
				}
			}
			else if (Ifx_Shell_matchToken(&args, "date") != FALSE)
			{
				uint32 year, month, day;

				if (
					(Ifx_Shell_parseUInt32(&args, &year, FALSE) != FALSE)
					&& (Ifx_Shell_parseUInt32(&args, &month, FALSE) != FALSE)
					&& (Ifx_Shell_parseUInt32(&args, &day, FALSE) != FALSE)
					)
				{
					if ((month < 12) && (day < 31))
					{
						board->boardVersion.productionDate.year  = year;
						board->boardVersion.productionDate.month = month;
						board->boardVersion.productionDate.day   = day;
						updateBoardVersion                       = TRUE;
					}
					else
					{
						IfxStdIf_DPipe_print(io, "ERROR: invalid date"ENDL);
					}
				}

				result = TRUE;
			}
			else if (Ifx_Shell_matchToken(&args, "test") != FALSE)
			{
				uint32 satus, year, month, day;

				if (
					(Ifx_Shell_parseUInt32(&args, &satus, FALSE) != FALSE)
					&& (Ifx_Shell_parseUInt32(&args, &year, FALSE) != FALSE)
					&& (Ifx_Shell_parseUInt32(&args, &month, FALSE) != FALSE)
					&& (Ifx_Shell_parseUInt32(&args, &day, FALSE) != FALSE)
					)
				{
					if ((month < 12) && (day < 31))
					{
						board->boardVersion.TestStatus     = satus != 0;
						board->boardVersion.TestDate.year  = year;
						board->boardVersion.TestDate.month = month;
						board->boardVersion.TestDate.day   = day;
						updateBoardVersion                 = TRUE;
					}
					else
					{
						IfxStdIf_DPipe_print(io, "ERROR: invalid date"ENDL);
					}

					result = TRUE;
				}
			}
			else if (Ifx_Shell_matchToken(&args, "clean") != FALSE)
			{
				Ifx_Efs*efs = &g_Lb.driver.efsBoard;

				uint32 pattern = 0;
		        Ifx_Shell_parseUInt32(&args, &pattern, TRUE);

				IfxStdIf_DPipe_print(io, "Erasing entire Logic board EEPROM... "ENDL);
				unprotectEeprom(&board->driver.eeprom);
				if (Ifx_At25xxx_erase(efs->eeprom, 0, efs->eeprom->device->size, (uint8)pattern))
				{
					Ifx_Efs_unmount(efs);
					IfxStdIf_DPipe_print(io, "Success."ENDL);
					Ifx_EfsShell_printInfo(efs, io);
				}
				else
				{
					Ifx_Efs_unmount(efs);
					IfxStdIf_DPipe_print(io, "Error."ENDL);
				}
				protectEeprom(&board->driver.eeprom);
				result                     = TRUE;

			}

			if (result)
		    {

		        if (updateBoardVersion)
		        {
					unprotectEeprom(&board->driver.eeprom);

		            if (!App_updateBoardVersionFile(&board->driver.efsBoard, boardVersion))
		            {
		                IfxStdIf_DPipe_print(io, "Error board version could not be written"ENDL);
		        		Ifx_At25xxx_printStatus(board->driver.efsBoard.eeprom, io);
		            }
		            protectEeprom(&board->driver.eeprom);
		            /* FIXME print info that the application should restart to take effect*/
		        }
		    }
    	}
    }
    else if (Ifx_Shell_matchToken(&args, "select") != FALSE)
    {
        if (Ifx_Shell_matchToken(&args, "efs") != FALSE)
        {
            if (Ifx_Shell_matchToken(&args, "lbb") != FALSE)
            {
                board->driver.efsShell.efs = &board->driver.efsBoard;
                IfxStdIf_DPipe_print(io, "Logic board EFS selected"ENDL);
                result                     = TRUE;
            }
            else if (Ifx_Shell_matchToken(&args, "lbc") != FALSE)
            {
                board->driver.efsShell.efs = &board->driver.efsConfig;
                IfxStdIf_DPipe_print(io, "Logic board EFS selected"ENDL);
                result                     = TRUE;
            }
            else if (Ifx_Shell_matchToken(&args, "dbb") != FALSE)
            {
                board->driver.efsShell.efs = AppDbStdIf_getBoardVersionEfs(&board->driverBoardStdif);
                IfxStdIf_DPipe_print(io, "Driver board EFS selected"ENDL);
                result                     = TRUE;
            }
            else if (Ifx_Shell_matchToken(&args, "dbc") != FALSE)
            {
                board->driver.efsShell.efs = AppDbStdIf_getConfigEfs(&board->driverBoardStdif);
                IfxStdIf_DPipe_print(io, "Driver board EFS selected"ENDL);
                result                     = TRUE;
            }
        }
    }
	else if (Ifx_Shell_matchToken(&args, "unlock") != FALSE)
	{
		g_App.boardVersionLock = FALSE;
		IfxStdIf_DPipe_print(io, "Production data unlocked"ENDL);
        result                     = TRUE;
	}

    return result;
}

boolean AppLbStdIf_isMinimalSetup(AppLbStdIf *stdif, IfxStdIf_DPipe *io)
{
    boolean error = FALSE;
    LB *board = &g_Lb;

    /* Check EEPROM*/
	if (Ifx_Efs_isLoaded(&board->driver.efsBoard))
	{
		/* Check if files exist  */
		if (board->boardVersion.fileVersion == Lb_FileVersion_undefined)
		{
			error = TRUE;
//			IfxStdIf_DPipe_print(io, "WARNING: Logic board, File %s not found or file version not supported. Please create the file system with the command 'setup lb version <version> <revision> <step>'"ENDL, EFS_CFG_FILE_NAME_BOARD_VERSION);
		}
		else if (board->boardVersion.fileVersion != EFS_CFG_FILE_VERSION_BOARD_VERSION)
		{   /* Check files version */
			error = TRUE;
//			IfxStdIf_DPipe_print(io, "WARNING: Logic board, Version of file '%s'not supported. Please create the file system with the command 'setup lb version <version> <revision> <step>'"ENDL, EFS_CFG_FILE_NAME_BOARD_VERSION);
		}

		/* Check board type */
		if (!error)
		{
            if (!AppLbStdIf_isBoardTypeSupported(stdif))
			{
				error = TRUE;
//				IfxStdIf_DPipe_print(io, "WARNING: Logic board type not supported."ENDL);
			}
		}

		/* Check board version*/
		if (!error)
		{
            if (AppLbStdIf_isBoardVersionSupported(stdif))
			{}
			else
			{
				error = TRUE;
//				IfxStdIf_DPipe_print(io, "WARNING: Logic board version not supported."ENDL);
			}
		}
	}
	else
	{
		error = TRUE;
//		IfxStdIf_DPipe_print(io, "WARNING: Logic board, board information file system dose not exist. Please create the file system with the command 'setup lb version <version> <revision> <step>'"ENDL);
	}

	if (!error)
	{
		if (Ifx_Efs_isLoaded(&board->driver.efsConfig))
		{
            if (!AppLbStdIf_isConfigFile(stdif))
			{
				error = TRUE;
//				IfxStdIf_DPipe_print(io, "WARNING: Logic board, File %s not found or file version not supported. Please create the file system with the command 'setup lb efs'"ENDL, EFS_CFG_FILE_NAME_BOARD_CONFIGURATION);
			}
            else if (!AppLbStdIf_isConfigFileVersionActual(stdif))
			{   /* Check files version */
				error = TRUE;
//				IfxStdIf_DPipe_print(io, "WARNING: Logic board, Version of file '%s'not supported. Please create the file system with the command 'setup lb efs'"ENDL, EFS_CFG_FILE_NAME_BOARD_CONFIGURATION);
			}
		}
		else
		{
			error = TRUE;
//			IfxStdIf_DPipe_print(io, "WARNING: Logic board, configuration file system dose not exist. Please create the file system with the command 'setup lb efs'"ENDL);
		}
	}

    error                |= !AppDbStdIf_isMinimalSetup(&board->driverBoardStdif, io);

#if BYPASSEEPROM
    return TRUE;
#else if
    return !error;
#endif
}


