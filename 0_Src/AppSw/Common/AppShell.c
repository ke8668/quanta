/**
 * \file AppShell.c
 * \brief Application Shell
 *
 * \copyright Copyright (c) 2015 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * This file may be used, copied, and distributed, with or without modification, provided
 * that all copyright notices are retained; that all modifications to this file are
 * prominently noted in the modified file; and that this paragraph is not modified.
 */
//----------------------------------------------------------------------------------------
#include "Common/AppShell.h"
#include "SysSe/Time/Ifx_Datetime.h"
#include "Common/Main.h"
#include "Common/AppInterface.h"
#include "Common/AppTest.h"
#include "SysSe/Ext/At25xxx/Ifx_EfsShell.h"

#include "LogicBoard3_0/Config/LB30_Configuration.h"
#include "Common/LB.h"

#include "SysSe/EMotor/Ifx_MotorControl.h"
#include "Common/AppSetupWizard.h"
#include <stdio.h>
#include "SysSe/Bsp/Assert.h"
//----------------------------------------------------------------------------------------
boolean AppShell_status(pchar args, void *data, IfxStdIf_DPipe *io);
boolean AppShell_setup(pchar args, void *data, IfxStdIf_DPipe *io);
boolean AppShell_info(pchar args, void *data, IfxStdIf_DPipe *io);
boolean AppShell_shutdown(pchar args, void *data, IfxStdIf_DPipe *io);
boolean AppShell_start(pchar args, void *data, IfxStdIf_DPipe *io);
boolean AppShell_stop(pchar args, void *data, IfxStdIf_DPipe *io);
boolean AppShell_reset(pchar args, void *data, IfxStdIf_DPipe *io);
boolean AppShell_mode(pchar args, void *data, IfxStdIf_DPipe *io);
boolean AppShell_dump(pchar args, void *data, IfxStdIf_DPipe *io);
boolean AppShell_config(pchar args, void *data, IfxStdIf_DPipe *io);
boolean AppShell_speed(pchar args, void *data, IfxStdIf_DPipe *io);
boolean AppShell_torque(pchar args, void *data, IfxStdIf_DPipe *io);
boolean AppShell_amplitude(pchar args, void *data, IfxStdIf_DPipe *io);
boolean AppShell_current(pchar args, void *data, IfxStdIf_DPipe *io);
boolean AppShell_get(pchar args, void *data, IfxStdIf_DPipe *io);
boolean AppShell_set(pchar args, void *data, IfxStdIf_DPipe *io);
boolean AppShell_benchmark(pchar args, void *data, IfxStdIf_DPipe *io);
boolean AppShell_oneeyeCmd_0(pchar args, void *data, IfxStdIf_DPipe *io);
boolean AppShell_setParam(pchar args, void *data, IfxStdIf_DPipe *io);
boolean AppShell_getParam(pchar args, void *data, IfxStdIf_DPipe *io);

//----------------------------------------------------------------------------------------
IFX_EXTERN void _START(void);
void AppShell_SetDefaultConsole(Ifx_Shell *shell);
//----------------------------------------------------------------------------------------

/*
 * {"simulate", " : simulate a signal"ENDL
 *  "/s simulate <ch> <op> <wave> <freq> <ampl> <offs> <phase> : Simulate a signal generation"ENDL
 *      "/p <ch>   : {0 .. Channel count}"ENDL
 */

/** \brief Application shell command list */
/* *INDENT-OFF* */
const Ifx_Shell_Command g_AppShell_commands[] =
{
    {"status", "   : Show the application internal states"                   , &g_App,   &AppShell_status,     },
    {"info", "     : Show the application information (welcome screen)"      , &g_App,   &AppShell_info,       },
    //{"protocol", " : Start a communication protocol(direct shell mode)"      , &g_App.shell, &Ifx_Shell_protocolStart,  },
    {"help",         SHELL_HELP_DESCRIPTION_TEXT                             , &g_App.primaryShell, &Ifx_Shell_showHelp,       },
    {"shutdown", " : Shutdown the ECU "                                           , &g_App, &AppShell_shutdown       },
    {"start", " : Run the motor control"                                           , &g_App, &AppShell_start       },
    {"stop", " : Stop the motor control"                                           , &g_App, &AppShell_stop       },
    {"reset", " : Reset the error condition"                                           , &g_App, &AppShell_reset       },
    {"speed", " : Set the reference speed"ENDL
            "/s speed <value>: Set the reference speed"ENDL
				"/p <value>: Speed in rpm"
            , &g_App, &AppShell_speed       },
    {"torque", " : Set the reference torque"ENDL
            "/s torque <value>: Set the reference torque"ENDL
				"/p <value>: torque in Nm"
            , &g_App, &AppShell_torque       },
    {"current", " : Set the reference current"ENDL
            "/s current <iq> [<id>]: Set the reference current"ENDL
				"/p <iq>: Iq current in A"
				"/p <id>: Id current in A, optional"
            , &g_App, &AppShell_current       },
    {"amplitude", " : Set the openloop amplitude"ENDL
            "/s amplitude <value>: Set the openloop amplitude"ENDL
				"/p <value>: Amplitude factor in percent of max amplitude. Range=[0,100]"
            , &g_App, &AppShell_amplitude       },
    {"config", " : Enter or exit the configuration state "ENDL
            "/s config : Enter the configuration state"ENDL
            "/s config exit: Exit the configuration state"
            , &g_App, &AppShell_config       },
    {"mode", " : Set the motor control mode"ENDL
            "/s mode passive : Set to passive mode"ENDL
            "/s mode current : Set to current control mode"ENDL
            "/s mode torque : Set to torque control mode"ENDL
            "/s mode speed : Set to speed control mode"ENDL
            "/s mode open : Set to open loop mode"ENDL
            "/s mode openpos : Set to open loop with position mode"ENDL
            , &g_App, &AppShell_mode       },
	{"dump", " : Dump ECU info "ENDL
			"/s dump driver : Dump IGBT driver information"ENDL
			"/s dump driver diff: Dump IGBT driver information, limited to the differences between the last 'dump driver' command"
			, &g_App, &AppShell_dump       },
	{"benchmark", " : Run benchmarks "ENDL
			, &g_App, &AppShell_benchmark       },

	{"setup", "    : Setup commands"ENDL
			"/s setup print: Print the setup information"ENDL""ENDL
            "/s setup reset: Reset the hybrid kit configuration to default. The boards data are not affected"ENDL
            "/s setup console <primary console>: Set the primary console"ENDL
				"/p <primary console>: Select the primary console. Range={asc0, can0}"ENDL""ENDL

			"/= SAFETY"ENDL
			"/- Window watchdog"ENDL
			"/s setup wwd <enabled>: Enable / disable the window watchdog (only if supported by the hardware)"ENDL
				"/p <enabled>: Range={0=>disabled, 1=>enabled}"ENDL
			"/s setup wwd <trigger>: Select the window watchdog trigger"ENDL
				"/p <trigger>: Range={spi=>SPI trigger, wdi=>WDI pin trigger}"ENDL
			"/s setup wwd ow <time>: Select the window watchdog open window time"ENDL
				"/p <time>: Window time in s"ENDL
			"/s setup wwd cw <time>: Select the window watchdog closed window time"ENDL
				"/p <time>: Window time in s"ENDL

			"/- Functional watchdog"ENDL
			"/s setup fwd <enabled>: Enable / disable the functinal watchdog (only if supported by the hardware)"ENDL
				"/p <enabled>: Range={0=>disabled, 1=>enabled}"ENDL
			"/s setup fwd bh <period>: Select the functional heartbeat timer period"ENDL
				"/p <period>: Heartbeat timer period in s"ENDL
			"/s setup fwd s <period>: Select the functional service period"ENDL
				"/p <period>: Service timer period in s. The service period must be smaller to the heartbeat period to enable proper service of the watchdog"ENDL

			"/- Fault signaling protocol"ENDL
			"/s setup fsp <enabled>: Enable / disable the fault signaling protocol (only if supported by the hardware)"ENDL
				"/p <enabled>: Range={0=>disabled, 1=>enabled}"ENDL

			"/- IOM"ENDL
			"/s setup iom <enabled>: Enable / disable the IOM monitoring (only if supported by the hardware)"ENDL
				"/p <enabled>: Range={0=>disabled, 1=>enabled}"ENDL
			"/s setup iom fsp <enabled>: Enable / disable the FSP triggering by the IOM monitoring"ENDL
				"/p <enabled>: Range={0=>disabled, 1=>enabled}"ENDL
			"/s setup iom nmi <enabled>: Enable / disable the NMI triggering by the IOM monitoring"ENDL
				"/p <enabled>: Range={0=>disabled, 1=>enabled}"ENDL
			"/s setup iom threshold <value>: IOM event windows threshold"ENDL
				"/p <value>: event windows threshold in micro second"ENDL
			"/s setup iom flitertime <value>: IOM pin input signal filter time for delay debounce filter"ENDL
				"/p <value>: Pin input signal filter time for delay debounce filter in micro second"ENDL



			"/= BOARD DATA"ENDL
			"/- Logic board"ENDL
			"/s setup lb test <status> <year> <month> <day>: Set the test date and status (Tester mode)"ENDL
				"/p <status>: Status. Range={0=>failed, 1=>passed}"ENDL
				"/p <year>: Year. 4 digit"ENDL
				"/p <month>: Month. 2 digit"ENDL
				"/p <day>: Day. 2 digit"ENDL
			"/s setup unlock: Unlock the board production data (enter Tester mode)"ENDL
			"/s setup lb clean: Erase the complete logic board eeprom device content (Tester mode)"ENDL
            "/s setup lb version <version> <revision> <step>: Set the logic board version (Tester mode)"ENDL
                "/p <version>: Version. Range=[0,255]"ENDL
                "/p <revision>: Version. Range=[0,255]"ENDL
                "/p <step>: Version. Range=[A,F]"ENDL
			"/s setup lb number <no>: Set the logic board number (Tester mode)"ENDL
				"/p <no>: Board number. Range=[0,65535]"ENDL
			"/s setup lb date <year> <month> <day>: Set the logic board production date (Tester mode)"ENDL
				"/p <year>: Year. 4 digit"ENDL
				"/p <month>: Month. 2 digit"ENDL
				"/p <day>: Day. 2 digit"ENDL

			"/- Driver board"ENDL
			"/s setup db version <type> <version> <revision> [<step>]: Set the driver board version (Tester mode)"ENDL
				"/p <type>: board type. Range={dbhp2=>'HP2 driver board', dbhpd=>'HPDrive driver board with EiceSil driver', dbhpdsense=>'HPDrive driver board with EiceSense driver', dbdsc=>'HPDrive driver board with EiceSense driver, DSC version'}"ENDL""ENDL
				"/p <version>: Version. Range=[0,255]"ENDL
				"/p <revision>: Version. Range=[0,255]"ENDL
				"/p <step>: Version. Range=[A,F]"ENDL
			"/s setup db number <no>: Set the driver board number (Tester mode)"ENDL
				"/p <no>: Board number. Range=[0,65535]"ENDL
			"/s setup db date <year> <month> <day>: Set the driver board production date (Tester mode)"ENDL
				"/p <no>: Year. 4 digit"ENDL
				"/p <month>: Month. 2 digit"ENDL
				"/p <day>: Day. 2 digit"ENDL
			"/s setup db test <status> <year> <month> <day>: Set the test date and status (Tester mode)"ENDL
				"/p <status>: Status. Range={0=>failed, 1=>passed}"ENDL
				"/p <year>: Year. 4 digit"ENDL
				"/p <month>: Month. 2 digit"ENDL
				"/p <day>: Day. 2 digit"ENDL

			"/s setup db vdcscale <gain> <offset>: Set the Vdc full scale gain and offset "ENDL
				"/p <gain>: Gain. For 1EDI2010AS: Range={0=>0.242, 1=>0.364, 2=>0.727}"ENDL
				"/p <offset>: Offset. For 1EDI2010AS: Range={0=>0, 1=>0.5, 2=>1, 3=>1.5, 4=>2, 5=>2.5, 6=>3}"ENDL
			"/s setup db vdczoom <gain> <offset>: Set the Vdc full scale gain and offset "ENDL
				"/p <gain>: Gain. For 1EDI2010AS: Range={0=>0.242, 1=>0.364, 2=>0.727}"ENDL
				"/p <offset>: Offset. For 1EDI2010AS: Range={0=>0, 1=>0.5, 2=>1, 3=>1.5, 4=>2, 5=>2.5, 6=>3}"ENDL
			"/s setup db igbttempscale <gain> <offset>: Set the IGBT temp full scale gain and offset "ENDL
				"/p <gain>: Gain. For 1EDI2010AS: Range={0=>0.242, 1=>0.364, 2=>0.727}"ENDL
				"/p <offset>: Offset. For 1EDI2010AS: Range={0=>0, 1=>0.5, 2=>1, 3=>1.5, 4=>2, 5=>2.5, 6=>3}"ENDL
			"/s setup db activeclamping <mode> <activation time>: Set the active clamping "ENDL
				"/p <mode>: Active clamping mode. For 1EDI2010AS: Range={0=>low level, 1 high level, 2 DACLP enabled (Safe turn off), 3 DACLP enabled (Regular and safe turn off)}.For 1EDI2002AS: Range={0: Regular and safe turn off, 1: Safe turn off only}"ENDL
				"/p <activation time>: Active clamping activation time. Only for 1EDI2002AS: Range=[0,0xFF]"ENDL
			"/s setup db twolevelon <level> <delay>: Set the two level turn on plateau level and delay "ENDL
				"/p <level>: Gate turn on plateau level. For 1EDI2010AS: Range={0=>6V, 1=>7V, ..., 6=>12V, 7=>12V or Hard switching}.For 1EDI2002AS: Range={0=>9.25V, 1=>10.5V, 2=>11.4V, 3=>Hard switching}"ENDL
				"/p <delay>: Turn on delay. For 1EDI2010AS: Range=[0,0xFF]. For 1EDI2002AS: Range=[0,0xF]"ENDL
			"/s setup db twoleveloff <level> <delay>: Set the two level turn off plateau level and delay "ENDL
				"/p <level>: Gate turn off plateau level. For 1EDI2010AS: Range={0=>6V, 1=>7V, ..., 7=>13V}. For 1EDI2002AS: Range={0=>9.74V, 1=>9.82V, ..., 14=>10.95, 15=>11.6V}"ENDL
				"/p <delay>: Turn off delay. For 1EDI2010AS: Range=[0,0xFF]. For 1EDI2002AS: Range=[0,0xFF]"ENDL
			"/s setup db twolevelvbecomp <enabled>: Enable / disable VBE compensation "ENDL
				"/p <enabled>: 1: enabled. 0:disabled. Valid only for 1EDI2010AS, 1EDI2002AS"ENDL


			"/- Board file system"ENDL
			"/s setup lb efs: Create the logic board file system"ENDL
			"/s setup lb load: Load the logic board configuration"ENDL
            "/s setup db efs: Create the driver board file system"ENDL
            "/s setup db load: Load the driver board configuration"ENDL

			"/= APPLICATION"ENDL

			"/- Position sensor"ENDL
			"/s setup m0 position <sensor>: Set the motor 0 position sensor"ENDL
				"/p <sensor>: Position sensor Range={encoder, ad2s1210, rdc0, rdc1, tle5012, none}"ENDL
			"/s setup m0 position1 <sensor>: Set the motor 0 redundant position sensor"ENDL
				"/p <sensor>: Position sensor Range={encoder, ad2s1210, rdc0, rdc1, tle5012, none}"ENDL
			"/s setup posdir <sensor> <dir>: Set the motor 0 position sensor direction"ENDL
				"/p <sensor>: Position sensor Range={encoder, ad2s1210, rdc0, rdc1; tle5012}"ENDL
				"/p <dir>: Position sensor direction={0=>default, 1=>reversed}"ENDL
			"/s setup posresolution <sensor> <resolution>: Set the motor 0 position sensor resolution"ENDL
				"/p <sensor>: Position sensor. Range={encoder, ad2s1210, rdc0, rdc1}"ENDL
				"/p <resolution>: resolution in ticks"ENDL
			"/s setup posoffset <sensor> <offset>: Set the motor 0 position sensor resolution"ENDL
				"/p <sensor>: Position sensor. Range={encoder, ad2s1210, rdc0, rdc1, tle5012}"ENDL
				"/p <offset>: offset in ticks"ENDL
			"/s setup poscarrier <sensor> <frequency>: Set the motor 0 resolver position sensor carrier frequency"ENDL
				"/p <sensor>: Position sensor Range={ad2s1210, rdc}"ENDL
				"/p <frequency>: Position sensor resolution"ENDL
			"/s setup posperiod <sensor> <count>: Set the motor 0 resolver position sensor period per rotation count"ENDL
				"/p <sensor>: Position sensor Range={ad2s1210, rdc0, rdc1}"ENDL
				"/p <count>: Position sensor number of period per rotation"ENDL
			"/s setup posgain <sensor> <code>: Set the motor 0 resolver position sensor signal gain"ENDL
				"/p <sensor>: Position sensor Range={ad2s1210, rdc0, rdc1}"ENDL
				"/p <code>: Position sensor signal gain code. See board specific values"ENDL
			"/s setup possignalrange <sensor> <min> <max>: Set the motor 0 aurix resolver position sensor signal amplitude range"ENDL
				"/p <sensor>: Position sensor Range={rdc0, rdc1}"ENDL
				"/p <min>: Min signal amplitude of cosine and sine in DSADC result unit"ENDL
				"/p <max>: Max signal amplitude of cosine and sine in DSADC result unit"ENDL

			"/- Current sensor"ENDL
			"/s setup m0 current gain <gain>: Set the motor 0 current sensor gain"ENDL
				"/p <gain>: Sensor gain in A/V"ENDL
			"/s setup m0 current count <count>: Set the motor 0 current sensor count"ENDL
				"/p <count>: Number of current sensor used. Range ={2, 3}"ENDL
			"/s setup m0 current offset <offset>: Set the motor 0 current sensor offset"ENDL
				"/p <offset>: Sensor offset in V"ENDL
			"/s setup m0 current max <max>: Set the motor 0 current sensor error threshold"ENDL
				"/p <max>: Error threshold in V"ENDL
			"/s setup m0 current filter <filter>: Set the motor 0 current sensor filter"ENDL
				"/p <filter>: Sensor input filter. Range ={rc, active}"ENDL
			"/s setup m0 current digfilter <cut off frequency>: Set the motor 0 current digital filter cut off frequency"ENDL
				"/p  <cut off frequency>: Cut off frequency in Hz. 0 disables the filter"ENDL
			"/- PWM"ENDL
			"/s setup m0 pwm <module>: set the PWM module used "ENDL
				"/p <module>: module to be used. Range={ccu6, atom, tom}"ENDL

			"/= BOARD CONFIGURATION"ENDL
			"/s setup db vdcnom <vdcnom>: set the VDC nominal"ENDL
				"/p <vdcnom>: vdc nominal pulse in V"ENDL
			"/s setup db vdcmax <vdcmax>: set the VDC max"ENDL
				"/p <vdcmax>: vdc max in V"ENDL""ENDL
			"/s setup db vdcmin <vdcmin>: set the VDC min"ENDL
				"/p <vdcmin>: vdc min in V"ENDL""ENDL
			"/s setup db frequency <frequency>: set the PWM frequency "ENDL
				"/p <frequency>: frequency in Hz"ENDL
			"/s setup db deadtime <deadtime>: set the PWM deadtime "ENDL
				"/p <deadtime>: deadtime in us"ENDL
			"/s setup db minpulse <minpulse>: set the PWM minimal pulse "ENDL
				"/p <minpulse>: minimal pulse in us"ENDL""ENDL
			"/s setup db spifrequency <value>: Set the driver frequency (Only for 3.x logic board version)"ENDL
				"/p <value>: SPI frequency in Hz"ENDL


			"/= HARDWARE PULSE MODE"ENDL
			"/s setup hwpulse enable <enable>: enable / disable the mode"ENDL
				"/p <enable>: enable / disable the mode. Range={0:disabled, 1:enabled}"ENDL
			"/s setup hwpulse period <period>: set the pulse period"ENDL
				"/p <period>: pulse period in us"ENDL
			"/s setup hwpulse start <start>: set the 1st pulse width"ENDL
				"/p <start>: 1st pulse width in us"ENDL
			"/s setup hwpulse inc <increment>: set the pulse increment"ENDL
				"/p <increment>: pulse increment in us"ENDL
			"/s setup hwpulse count <count>: set the pulse count per sequence"ENDL
				"/p <count>: pulse count per sequence"ENDL
			"/s setup hwpulse pwmperiod <period>: set the PWM period used as a basis for the pulse generation"ENDL
				"/p <period>: PWM period in us"ENDL

			"/= OTHERS"ENDL
			"/s setup select efs <id> : Select an EFS to be used by the efs command"ENDL
				"/p <id>: EFS ID. Range={lbc=>'Logic board, configuration EFS', lbb=>'Logic board, board EFS', dbc=>'Driver board, configuration EFS', dbb=>'Driver board, board EFS'}"ENDL""ENDL
			"/s setup ready <value>: Set the configuration ready flag"ENDL
				"/p <value>: if 1, the software will go directly to the run state after boot, else the state will be configuration"ENDL""ENDL
			"/s setup verbose <level>: Set / return the verbose level"ENDL
				"/p <index>: Verbose level. Range=[0=>OFF, 1=>FAILURE, 2=>ERROR, 3=>WARNING, 4=>INFO, 5=>DEBUG]"ENDL
				, &g_App, &AppShell_setup       },

			{"set", " : Set feature"ENDL
				"/s set dout <index> <state>: Set the general output state"ENDL
					"/p <index>: output index. range=[0,3]"ENDL
					"/p <state>: Output level. Range=[0=low,1=high]"ENDL
					, &g_App, &AppShell_set       },
            {"get", " : Get feature"ENDL
				"/s get dout <index>: Return the general output state"ENDL
					"/p <index>: output index. range=[0,3]"ENDL
				"/s get din <index>: Return the general input state"ENDL
					"/p <index>: output index. range=[0,3]"ENDL
                    , &g_App, &AppShell_get       },
/* FIXME redirect to shell in used */
            {"!", "        : OneEye command"                                         , &g_App.primaryShell,   		 &AppShell_oneeyeCmd_0,       },
			{"setp", " : Set parameter"ENDL
				"/s setp <id> <value>: Set a parameter"ENDL
					"/p <id>: Parameter ID"ENDL
					"/p <value>: Parameter value"ENDL
					, &g_App, &AppShell_setParam       },
			{"getp", " : Get a parameter"ENDL
				"/s getp <id> <value>: Get a parameter"ENDL
					"/p <id>: Parameter ID"ENDL
					"/p <value>: Parameter value"ENDL
					, &g_App, &AppShell_getParam       },
   IFX_SHELL_COMMAND_LIST_END
};


/* FIXME add configuration for
 * FIXME check for erroneous/unused parameters and give warning
 * FIXME add confirmation message when command is executed?
 * FIXME add configuration for LB_FileConfiguration.analogInput
 * FIXME add configuration for LB_FileConfiguration.inverter.igbtTempMax
 * FIXME add configuration for LB_FileConfiguration.inverter.igbtTempMin
 *
 **/
/* *INDENT-ON* */
//----------------------------------------------------------------------------------------

typedef struct
{
    Ifx_Shell_Call cmd;
    uint32         modes;
}AppShell_ValidCommandFlags;

const AppShell_ValidCommandFlags AppShell_g_ValidCommandFlags[] = {
    {AppShell_info,      (1 << AppMode_boot) | (1 << AppMode_configuration) | (1 << AppMode_run) | (1 << AppMode_limitedConfiguration) | (1 << AppMode_error)},
    {AppShell_status,    (0 << AppMode_boot) | (1 << AppMode_configuration) | (1 << AppMode_run) | (1 << AppMode_limitedConfiguration) | (1 << AppMode_error)},
    {AppShell_setup,     (0 << AppMode_boot) | (1 << AppMode_configuration) | (0 << AppMode_run) | (0 << AppMode_limitedConfiguration) | (0 << AppMode_error)},
    {AppShell_shutdown,  (0 << AppMode_boot) | (1 << AppMode_configuration) | (1 << AppMode_run) | (1 << AppMode_limitedConfiguration) | (1 << AppMode_error)},
    {AppShell_start,     (0 << AppMode_boot) | (0 << AppMode_configuration) | (1 << AppMode_run) | (0 << AppMode_limitedConfiguration) | (0 << AppMode_error)},
    {AppShell_stop,      (0 << AppMode_boot) | (0 << AppMode_configuration) | (1 << AppMode_run) | (0 << AppMode_limitedConfiguration) | (0 << AppMode_error)},
    {AppShell_reset,     (0 << AppMode_boot) | (0 << AppMode_configuration) | (0 << AppMode_run) | (1 << AppMode_limitedConfiguration) | (1 << AppMode_error)},
    {AppShell_mode,      (0 << AppMode_boot) | (0 << AppMode_configuration) | (1 << AppMode_run) | (0 << AppMode_limitedConfiguration) | (0 << AppMode_error)},
    {AppShell_dump,      (0 << AppMode_boot) | (0 << AppMode_configuration) | (1 << AppMode_run) | (0 << AppMode_limitedConfiguration) | (1 << AppMode_error)},
    {AppShell_config,    (0 << AppMode_boot) | (1 << AppMode_configuration) | (1 << AppMode_run) | (1 << AppMode_limitedConfiguration) | (1 << AppMode_error)},
    {AppShell_speed,     (0 << AppMode_boot) | (0 << AppMode_configuration) | (1 << AppMode_run) | (0 << AppMode_limitedConfiguration) | (0 << AppMode_error)},
    {AppShell_torque,    (0 << AppMode_boot) | (0 << AppMode_configuration) | (1 << AppMode_run) | (0 << AppMode_limitedConfiguration) | (0 << AppMode_error)},
    {AppShell_amplitude, (0 << AppMode_boot) | (0 << AppMode_configuration) | (1 << AppMode_run) | (0 << AppMode_limitedConfiguration) | (0 << AppMode_error)},
    {AppShell_current,   (0 << AppMode_boot) | (0 << AppMode_configuration) | (1 << AppMode_run) | (0 << AppMode_limitedConfiguration) | (0 << AppMode_error)},
    {AppShell_get,       (0 << AppMode_boot) | (0 << AppMode_configuration) | (1 << AppMode_run) | (0 << AppMode_limitedConfiguration) | (0 << AppMode_error)},
    {AppShell_set,       (0 << AppMode_boot) | (0 << AppMode_configuration) | (1 << AppMode_run) | (0 << AppMode_limitedConfiguration) | (0 << AppMode_error)},
    {AppShell_benchmark, (0 << AppMode_boot) | (0 << AppMode_configuration) | (1 << AppMode_run) | (0 << AppMode_limitedConfiguration) | (0 << AppMode_error)},
    {NULL_PTR,           0                                                                                                                                   },
};

boolean AppShell_isValidCommand(IfxStdIf_DPipe *io, Ifx_Shell_Call cmd, AppMode mode)
{
    boolean result = TRUE;
    uint32  i      = 0;

    {
        /* Set the current shell to default */
    	Ifx_Shell       *shell = NULL_PTR;

    	if (io == g_App.primaryShell.io)
    	{
    		shell = &g_App.primaryShell;
    	}
    	else if (io == g_App.secondaryShell.io)
    	{
    		shell = &g_App.secondaryShell;
    	}
    	else
    	{
    	}

    	if (shell)
    	{
        	AppShell_SetDefaultConsole(shell);
        }
    }

    do
    {
        if (AppShell_g_ValidCommandFlags[i].cmd == cmd)
        {
            result = (AppShell_g_ValidCommandFlags[i].modes & (1 << mode)) != 0;
            break;
        }

        i++;
    } while (AppShell_g_ValidCommandFlags[i].cmd);

    if (!result)
    {
//        IfxStdIf_DPipe_print(io, "Command not valid in state: %s"ENDL, AppStateMachine_ModeNames[mode]);
    }

    return result;
}


#include "SysSe/Math/Ifx_crc.h"
/** \brief Handle the 'status' command
 *
 * \par Syntax
 *  - status : Show the application internal state
 *  - status log : Enable/disable printing of periodic status message.
 */
boolean AppShell_status(pchar args, void *data, IfxStdIf_DPipe *io)
{
    if (!AppShell_isValidCommand(io, AppShell_status, g_App.hwState.hwMode))
    {
        return TRUE;
    }

    if (Ifx_Shell_matchToken(&args, "?") != FALSE)
    {
//        IfxStdIf_DPipe_print(io, "Syntax     : status"ENDL);
//        IfxStdIf_DPipe_print(io, "           > Show the application internal state"ENDL);
    }
    else
    {
        Ifx_DateTime rt;
        DateTime_get(&rt);
//        IfxStdIf_DPipe_print(io, "Real-time: %02d:%02d:%02d"ENDL, rt.hours, rt.minutes, rt.seconds);
//        IfxStdIf_DPipe_print(io, "CPU Frequency: %ld Hz"ENDL, (sint32)g_App.info.cpuFreq);
//        IfxStdIf_DPipe_print(io, "SYS Frequency: %ld Hz"ENDL, (sint32)g_App.info.sysFreq);
        if (g_Lb.stdIf.positionSensorX[0])
        {
//            IfxStdIf_DPipe_print(io, "Position sensor 0 actual offset: %d"ENDL, IfxStdIf_Pos_getOffset(g_Lb.stdIf.positionSensorX[0]));
//        	IfxStdIf_Pos_printStatus(IfxStdIf_Pos_getFault(g_Lb.stdIf.positionSensorX[0]), io);
        }
        if (g_Lb.stdIf.positionSensorX[1])
        {
//            IfxStdIf_DPipe_print(io, "Position sensor 1 actual offset: %d"ENDL, IfxStdIf_Pos_getOffset(g_Lb.stdIf.positionSensorX[1]));
//        	IfxStdIf_Pos_printStatus(IfxStdIf_Pos_getFault(g_Lb.stdIf.positionSensorX[1]), io);
        }

        {
            App *app = (App *)data;
//            IfxStdIf_DPipe_print(io, "Main state machine state: %s"ENDL, AppStateMachine_ModeNames[app->hwState.hwMode]);
        }

    	if (g_App.hwState.hwMode >= AppMode_run)
    	{
    		{
    			float32 currents[3];
                ECU_InverterStatus status = ECU_getInverterStatus(0);
                ECU_getMotorPhaseCurrents(0, currents);

//                IfxStdIf_DPipe_print(io, "Vdc: %f V"ENDL, ECU_getInverterVdc(0));
//                IfxStdIf_DPipe_print(io, "Phase currents (If sensor present): U=%f A, V=%f A, W=%f A"ENDL, currents[0], currents[1], currents[2]);
//                IfxStdIf_DPipe_print(io, "Phase currents offset after calibration, raw value (Inverter 0): U=%f, V=%f, W=%f"ENDL, g_Lb.stdIf.currents[0][0]->input.scale.offset, g_Lb.stdIf.currents[0][1]->input.scale.offset, g_Lb.stdIf.currents[0][2]->input.scale.offset);
//                IfxStdIf_DPipe_print(io, "Phase currents offset after calibration, raw value (Inverter 1): U=%f, V=%f, W=%f"ENDL, g_Lb.stdIf.currents[1][0]->input.scale.offset, g_Lb.stdIf.currents[1][1]->input.scale.offset, g_Lb.stdIf.currents[1][2]->input.scale.offset);
//                IfxStdIf_DPipe_print(io, "IGBT temp: U=%f �C, V=%f �C, W=%f �C"ENDL, status.igbtTemp[0], status.igbtTemp[1], status.igbtTemp[2]);
//                IfxStdIf_DPipe_print(io, "Board temp: %f �C"ENDL, status.tempBoard);
//                IfxStdIf_DPipe_print(io, "Mechnical position: %f rad"ENDL, ECU_getMotorMechanicalPosition(0));
//                IfxStdIf_DPipe_print(io, "Speed: %f rpm"ENDL, (ECU_getMotorMechanicalSpeed(0)/2.0/IFX_PI*60.0));

//                IfxStdIf_DPipe_print(io, "Position sensor 0 error: %s"ENDL, (status.positionSensor1Error? "yes":"no"));
//                IfxStdIf_DPipe_print(io, "Position sensor 1 error: %s"ENDL, (status.positionSensor2Error? "yes":"no"));

//                IfxStdIf_DPipe_print(io, "Phase U top error: %s"ENDL, (status.phase1TopDriverError? "yes":"no"));
//                IfxStdIf_DPipe_print(io, "Phase U bottom error: %s"ENDL, (status.phase1BottomDriverError? "yes":"no"));
//                IfxStdIf_DPipe_print(io, "Phase V top error: %s"ENDL, (status.phase2TopDriverError? "yes":"no"));
//                IfxStdIf_DPipe_print(io, "Phase V bottom error: %s"ENDL, (status.phase2BottomDriverError? "yes":"no"));
//                IfxStdIf_DPipe_print(io, "Phase W top error: %s"ENDL, (status.phase2TopDriverError? "yes":"no"));
//                IfxStdIf_DPipe_print(io, "Phase W bottom error: %s"ENDL, (status.phase2BottomDriverError? "yes":"no"));
/* FIXME values are not available
                IfxStdIf_DPipe_print(io, "Phase Current U error: %s"ENDL, (status.currentSensor1Error? "yes":"no"));
                IfxStdIf_DPipe_print(io, "Phase Current V error: %s"ENDL, (status.currentSensor2Error? "yes":"no"));
                IfxStdIf_DPipe_print(io, "Phase Current W error: %s"ENDL, (status.currentSensor3Error? "yes":"no"));
*/
//                IfxStdIf_DPipe_print(io, "Input error: %s"ENDL, (status.faultInput? "yes":"no"));
//                IfxStdIf_DPipe_print(io, "Input limit error: %s"ENDL, (status.faultLimit? "yes":"no"));
//                IfxStdIf_DPipe_print(io, "IOM error: %s"ENDL, (status.faultIom? "yes":"no"));

                if (status.faultMain)
                {
//                    IfxStdIf_DPipe_print(io, "Main error set"ENDL);
                }

    		}

            if (g_App.hwState.specialMode == AppModeSpecial_hardwareDebugMode)
            {
                	boolean runRequest;
                	float32 amplitudeRequest;
                	float32 freqencyRequest;
                	runRequest = !AppLbStdIf_getDigitalInput(&g_Lb.stdIf, 3);
                	amplitudeRequest = AppLbStdIf_getAnalogInput(&g_Lb.stdIf, 0);
                	freqencyRequest = AppLbStdIf_getAnalogInput(&g_Lb.stdIf, 1);

//                    IfxStdIf_DPipe_print(io, "Hardware debug mode: "ENDL);
//                    IfxStdIf_DPipe_print(io, "    - Run request: %s"ENDL, (runRequest ? "requested":"not requested"));
//                    IfxStdIf_DPipe_print(io, "    - Amplitude: %f (duty cycle)"ENDL, amplitudeRequest);
//                    IfxStdIf_DPipe_print(io, "    - Frequency: %f rpm"ENDL, freqencyRequest);
            }

    	}

//        Ifx_MotorControl_Mode_print(io, (Ifx_MotorControl_Mode)g_App.can.inbox.modeCmd);

        if (!g_App.boardVersionLock)
        {
//            IfxStdIf_DPipe_print(io, "Production data unlocked"ENDL);
        }
    }

    return TRUE;
}


boolean AppShell_start(pchar args, void *data, IfxStdIf_DPipe *io)
{
    App *app = (App *)data;

    if (!AppShell_isValidCommand(io, AppShell_start, app->hwState.hwMode))
    {
        return TRUE;
    }

    app->can.inbox.runCmd = TRUE;
//    IfxStdIf_DPipe_print(io, "Motor control start requested");
    return TRUE;
}


boolean AppShell_stop(pchar args, void *data, IfxStdIf_DPipe *io)
{
    App *app = (App *)data;

    if (!AppShell_isValidCommand(io, AppShell_stop, app->hwState.hwMode))
    {
        return TRUE;
    }

    app->can.inbox.runCmd = FALSE;
//    IfxStdIf_DPipe_print(io, "Motor control stop requested");
    return TRUE;
}

boolean AppShell_reset(pchar args, void *data, IfxStdIf_DPipe *io)
{
    App *app = (App *)data;

    if (!AppShell_isValidCommand(io, AppShell_reset, app->hwState.hwMode))
    {
        return TRUE;
    }

    app->can.inbox.clearErrorCmd = TRUE;
//    IfxStdIf_DPipe_print(io, "Error reset requested");
    return TRUE;
}


boolean AppShell_shutdown(pchar args, void *data, IfxStdIf_DPipe *io)
{
    App *app = (App *)data;

    if (!AppShell_isValidCommand(io, AppShell_shutdown, app->hwState.hwMode))
    {
        return TRUE;
    }

    app->hwState.hwModeRequest = AppMode_shutdown;
    return TRUE;
}


boolean AppShell_mode(pchar args, void *data, IfxStdIf_DPipe *io)
{
    boolean result = FALSE;
    App    *app    = (App *)data;

    if (!AppShell_isValidCommand(io, AppShell_mode, app->hwState.hwMode))
    {
        return TRUE;
    }

    if (Ifx_Shell_matchToken(&args, "passive") != FALSE)
    {
        g_App.can.inbox.modeCmd = Ifx_MotorControl_Mode_passive;
        result                  = TRUE;
    }
    else if (Ifx_Shell_matchToken(&args, "current") != FALSE)
    {
        g_App.can.inbox.modeCmd = Ifx_MotorControl_Mode_currentControl;
        result                  = TRUE;
    }
    else if (Ifx_Shell_matchToken(&args, "torque") != FALSE)
    {
        g_App.can.inbox.modeCmd = Ifx_MotorControl_Mode_torqueControl;
        result                  = TRUE;
    }
    else if (Ifx_Shell_matchToken(&args, "speed") != FALSE)
    {
        g_App.can.inbox.modeCmd = Ifx_MotorControl_Mode_speedControl;
        result                  = TRUE;
    }
    else if (Ifx_Shell_matchToken(&args, "open") != FALSE)
    {
        g_App.can.inbox.modeCmd = Ifx_MotorControl_Mode_openLoop;
        result                  = TRUE;
    }
    else if (Ifx_Shell_matchToken(&args, "openpos") != FALSE)
    {
        g_App.can.inbox.modeCmd = Ifx_MotorControl_Mode_openWithPosition;
        result                  = TRUE;
    }

    if (result)
    {
        Ifx_MotorControl_Mode_print(io, (Ifx_MotorControl_Mode)g_App.can.inbox.modeCmd);
    }

    return result;
}


boolean AppShell_config(pchar args, void *data, IfxStdIf_DPipe *io)
{
    boolean result = FALSE;
    App    *app    = (App *)data;

    if (!AppShell_isValidCommand(io, AppShell_config, app->hwState.hwMode))
    {
        return TRUE;
    }

    if (Ifx_Shell_matchToken(&args, "exit") != FALSE)
    {
        if (app->hwState.hwMode == AppMode_configuration)
        {
            app->hwState.hwModeRequest = AppMode_hwInitialization;
        }
        else
        {
            app->hwState.hwModeRequest = AppMode_appInitialization;
        }

        result = TRUE;
    }
    else
    {
        app->hwState.hwModeRequest = AppMode_limitedConfiguration;
        result                     = TRUE;
    }

    return result;
}


boolean AppShell_speed(pchar args, void *data, IfxStdIf_DPipe *io)
{
    boolean result = FALSE;
    App    *app    = (App *)data;
    float32 value;

    if (!AppShell_isValidCommand(io, AppShell_speed, app->hwState.hwMode))
    {
        return TRUE;
    }

    if ((Ifx_Shell_parseFloat32(&args, &value) != FALSE))
    {
        app->can.inbox.speedCmd = value;
//        IfxStdIf_DPipe_print(io, "Speed reference set to %f rpm"ENDL, app->can.inbox.speedCmd);
        result                  = TRUE;
    }
    else
    {
        float32 speed;
        speed  = g_Lb.stdIf.positionSensorX[0] ? IfxStdIf_Pos_radsToRpm(IfxStdIf_Pos_getSpeed(g_Lb.stdIf.positionSensorX[0])) : 0.0;
//        IfxStdIf_DPipe_print(io, "Actual speed %f rpm"ENDL, speed);
        if (g_Lb.stdIf.positionSensorX[1])
        {
            speed  = IfxStdIf_Pos_radsToRpm(IfxStdIf_Pos_getSpeed(g_Lb.stdIf.positionSensorX[1]));
//            IfxStdIf_DPipe_print(io, "Actual speed (second sensor) %f rpm"ENDL, speed);
        }
        result = TRUE;
    }

    return result;
}


boolean AppShell_torque(pchar args, void *data, IfxStdIf_DPipe *io)
{
    boolean result = FALSE;
    App    *app    = (App *)data;
    float32 value;

    if (!AppShell_isValidCommand(io, AppShell_torque, app->hwState.hwMode))
    {
        return TRUE;
    }

    if ((Ifx_Shell_parseFloat32(&args, &value) != FALSE))
    {
        app->can.inbox.torqueCmd = value;
//        IfxStdIf_DPipe_print(io, "Torque reference set to %f Nm"ENDL, app->can.inbox.torqueCmd);
        result                   = TRUE;
    }

    return result;
}


boolean AppShell_amplitude(pchar args, void *data, IfxStdIf_DPipe *io)
{
    boolean result = FALSE;
    App    *app    = (App *)data;
    float32 value;

    if (!AppShell_isValidCommand(io, AppShell_amplitude, app->hwState.hwMode))
    {
        return TRUE;
    }

    if ((Ifx_Shell_parseFloat32(&args, &value) != FALSE))
    {
        app->can.inbox.amplitudeCmd = value;
//        IfxStdIf_DPipe_print(io, "Amplitude set to %f %"ENDL, app->can.inbox.amplitudeCmd);
        result                      = TRUE;
    }
    else
    {
//        IfxStdIf_DPipe_print(io, "Actual Amplitude %f %"ENDL, app->can.inbox.amplitudeCmd);
        result = TRUE;
    }

    return result;
}


boolean AppShell_current(pchar args, void *data, IfxStdIf_DPipe *io)
{
    boolean result = FALSE;
    App    *app    = (App *)data;
    float32 id;
    float32 iq;

    if (!AppShell_isValidCommand(io, AppShell_current, app->hwState.hwMode))
    {
        return TRUE;
    }

    if ((Ifx_Shell_parseFloat32(&args, &iq) != FALSE))
    {
        app->can.inbox.currentIqCmd = iq;
//        IfxStdIf_DPipe_print(io, "Iq current reference set to %f A"ENDL, app->can.inbox.currentIqCmd);

        if ((Ifx_Shell_parseFloat32(&args, &id) != FALSE))
        {
            app->can.inbox.currentIdCmd = id;
        }

//        IfxStdIf_DPipe_print(io, "Id current reference set to %f A"ENDL, app->can.inbox.currentIdCmd);

        result = TRUE;
    }

    return result;
}


boolean AppShell_set(pchar args, void *data, IfxStdIf_DPipe *io)
{
    App    *app    = (App *)data;
    boolean result = FALSE;

    if (!AppShell_isValidCommand(io, AppShell_set, app->hwState.hwMode))
    {
        return TRUE;
    }

    result = AppLbStdIf_shellSet(&g_Lb.stdIf, args, data, io);

    return result;
}


boolean AppShell_get(pchar args, void *data, IfxStdIf_DPipe *io)
{
    App    *app    = (App *)data;
    boolean result = FALSE;

    if (!AppShell_isValidCommand(io, AppShell_get, app->hwState.hwMode))
    {
        return TRUE;
    }

    result = AppLbStdIf_shellGet(&g_Lb.stdIf, args, data, io);

    return result;
}


boolean AppShell_dump(pchar args, void *data, IfxStdIf_DPipe *io)
{
    boolean result = FALSE;
    App    *app    = (App *)data;

    if (!AppShell_isValidCommand(io, AppShell_dump, app->hwState.hwMode))
    {
        return TRUE;
    }

    if (Ifx_Shell_matchToken(&args, "driver") != FALSE)
    {
        boolean diff;
        diff = Ifx_Shell_matchToken(&args, "diff");
        AppDbStdIf_dumpDriverInfo(&g_Lb.driverBoardStdif, io, diff);

        return TRUE;
    }
    else
    {}

    return result;
}


boolean AppShell_benchmark(pchar args, void *data, IfxStdIf_DPipe *io)
{
    boolean result = FALSE;
    App    *app    = (App *)data;

    if (!AppShell_isValidCommand(io, AppShell_benchmark, app->hwState.hwMode))
    {
        return TRUE;
    }

    {
        IfxCpu_ResourceCpu index;

        for (index = IfxCpu_ResourceCpu_0; index <= IfxCpu_ResourceCpu_2; index++)
        {
            Cpu    *cpu;
            cpu = g_Cpus[index];
            float32 cpuFrequency = IfxScuCcu_getCpuFrequency(index);

//            IfxStdIf_DPipe_print(io, "________________________________________________________________________________"ENDL);
//            IfxStdIf_DPipe_print(io, "CPU %d benchmark:"ENDL, index);
//            IfxStdIf_DPipe_print(io, "CPU frequency %f Hz:"ENDL, cpuFrequency);
//            IfxStdIf_DPipe_print(io, "---------------------"ENDL);
            cpu->benchmark.ready = FALSE;
            Lb_raiseIsr(&g_Lb, index);

            while (!cpu->benchmark.ready)
            {}

            /*                       "  N         | I          | DS        | DT         */
//            IfxStdIf_DPipe_print(io, "  Name      | Instruction| Duration  | Duration   |"ENDL);
//            IfxStdIf_DPipe_print(io, "            |            |           |            |"ENDL);
//            IfxStdIf_DPipe_print(io, "            |            | (us)      | (cpu clock)|"ENDL);

            IfxCpu_Perf *benchmarks[10] = {
                &cpu->benchmark.empty,
                &cpu->benchmark.cosSinLookup,
                &cpu->benchmark.clarke,
                &cpu->benchmark.park,
                &cpu->benchmark.reversePark,
                &cpu->benchmark.piController,
                &cpu->benchmark.svm,
                &cpu->benchmark.foc,
                &cpu->benchmark.rdc,
                &cpu->benchmark.gtmpwm,
            };
            pchar        benchmarksNames[10] = {
                "Empty",
                "CosSin lookup",
                "Clarke",
                "Park",
                "Reverse Park",
                "PI controller",
                "SVM",
                "FOC",
                "DSADC RDC",
                "GTM PWM",
            };
            uint32       i;

            for (i = 0; i < 10; i++)
            {
                IfxCpu_Perf *perf = benchmarks[i];
                /*                       "  N         | I          | DT        | DS        */
/*
                IfxStdIf_DPipe_print(io, "  %10.10s| %11d| %10.3f| %11d|"ENDL,
                    benchmarksNames[i],
                    (perf->instruction.counter),
                    (perf->clock.counter / cpuFrequency * 1e6),
                    (perf->clock.counter
                    );
*/
            }
        }
    }

    result = TRUE;

    return result;
}


boolean AppShell_setup(pchar args, void *data, IfxStdIf_DPipe *io)
{
    boolean result = TRUE;

    if (AppLbStdIf_shellSetupCommon(&g_Lb.stdIf, args, data, io))
    {
    }
    else if (AppLbStdIf_shellSetup(&g_Lb.stdIf, args, data, io))
    {
    }
    else if (Ifx_Shell_matchToken(&args, "wizard") != FALSE)
    {
    	AppSetupWizard_run(io);
    }
    else
    {
    	result = FALSE;
    }

    return result;
}


/** \brief Handle the 'info' command.
 *
 * \par Syntax
 * - info : Show application information */
boolean AppShell_info(pchar args, void *data, IfxStdIf_DPipe *io)
{
    App *app = (App *)data;

    if (!AppShell_isValidCommand(io, AppShell_info, app->hwState.hwMode))
    {
        return TRUE;
    }

    app->info.srcRev      = SW_REVISION;
    app->info.compilerVer = SW_COMPILER_VERSION;

//    IfxStdIf_DPipe_print(io, ENDL ""ENDL);
//    IfxStdIf_DPipe_print(io, "******************************************************************************"ENDL);
//    IfxStdIf_DPipe_print(io, "*******                                                                *******"ENDL);
//    IfxStdIf_DPipe_print(io, "*******  Infineon "IFXGLOBAL_DERIVATIVE_NAME " uC                                            *******"ENDL);
//    IfxStdIf_DPipe_print(io, "******************************************************************************"ENDL);
//    IfxStdIf_DPipe_print(io, "*******  Copyright (C) 2015 Infineon Technologies A.G.                 *******"ENDL);
//    IfxStdIf_DPipe_print(io, "*******                                                                *******"ENDL);
//    IfxStdIf_DPipe_print(io, "*******  You can use this program under the terms of the IFX License.  *******"ENDL);
//    IfxStdIf_DPipe_print(io, "*******                                                                *******"ENDL);
//    IfxStdIf_DPipe_print(io, "*******  This program is distributed in the hope that it will be       *******"ENDL);
//    IfxStdIf_DPipe_print(io, "*******  useful, but WITHOUT ANY WARRANTY; without even the            *******"ENDL);
//    IfxStdIf_DPipe_print(io, "*******  implied warranty of MERCHANTABILITY or FITNESS FOR            *******"ENDL);
//    IfxStdIf_DPipe_print(io, "*******  A PARTICULAR PURPOSE. See the IFX License for more            *******"ENDL);
//    IfxStdIf_DPipe_print(io, "*******  details (IFX_License.txt).                                    *******"ENDL);
//    IfxStdIf_DPipe_print(io, "******************************************************************************"ENDL);
//    IfxStdIf_DPipe_print(io, "*******  Software Compile Date : %s                           *******"ENDL, __DATE__);
//    IfxStdIf_DPipe_print(io, "*******  Software Compile Time : %s                              *******"ENDL, __TIME__);
//    IfxStdIf_DPipe_print(io, "*******  Software Version      : %2d.%02d                                 *******"ENDL, (app->info.srcRev >> 8) & 0xFF, (app->info.srcRev >> 0) & 0xFF);
//    IfxStdIf_DPipe_print(io, "*******  "COMPILER_NAME " Compiler      : %ld.%1dr%1d                                 *******"ENDL, (app->info.compilerVer >> 16) & 0xFF, (app->info.compilerVer >> 8) & 0xFF, (app->info.compilerVer >> 0) & 0xFF);
//    IfxStdIf_DPipe_print(io, "******************************************************************************"ENDL);

//    IfxStdIf_DPipe_print(io, ENDL);

//	Ifx_Console_print("Please look for update of the Hybrid kit software on www.myinfineon.com"ENDL);

//    IfxStdIf_DPipe_print(io, ENDL);

    return TRUE;
}


/** OneEye command "!"
 *
 */
boolean AppShell_oneeyeCmd_0(pchar args, void *data, IfxStdIf_DPipe *io)
{
	boolean result;
	boolean echo;
	boolean showPrompt;
	Ifx_Shell       *shell = NULL_PTR;

	if (io == g_App.primaryShell.io)
	{
		shell = &g_App.primaryShell;
	}
	else if (io == g_App.secondaryShell.io)
	{
		shell = &g_App.secondaryShell;
	}
	else
	{
		shell = (Ifx_Shell*) data;
	}

	/* OneEye command detected, switch echo and prompt off */
	echo = shell->control.echo;
	showPrompt = shell->control.showPrompt;

	shell->control.echo = 0;
	shell->control.showPrompt = 0;

	result = Ifx_Shell_execute(shell, args) == 0;

	shell->control.echo = echo ? 1 : 0;
	shell->control.showPrompt = showPrompt ? 1 : 0;
	return result;
}

typedef enum
{
    AppShell_DataType_unknown,
    AppShell_DataType_float32,
    AppShell_DataType_sint32 ,
    AppShell_DataType_uint32 ,
    AppShell_DataType_enum ,
    AppShell_DataType_boolean ,
} AppShell_DataType;

typedef struct
{
	uint32            id;			/**< \brief Parameter ID */
	AppShell_DataType dataType;		/**< \brief Data type */
    pchar               title;
    pchar				unit;
}AppShell_Parameter;


#define PARAMETER_ID_TABLE_END										{0, 0, 0}

/* Logic board configuration */
#define PARAMETER_ID_LB_FIRST										(1)
#define PARAMETER_ID_LB_LAST										(999)
#define PARAMETER_ID_READY					                        (  1)
#define PARAMETER_ID_PRIMARY_CONSOLE		                        (  2)
#define PARAMETER_ID_VERBOSE_LEVEL			                        (  3)

#define PARAMETER_ID_ENCODER_REVERSED		                        (100)
#define PARAMETER_ID_ENCODER_OFFSET			                        (101)
#define PARAMETER_ID_ENCODER_RESOLUTION		                        (102)

#define PARAMETER_ID_AD2S1210_RESOLUTION	     	                (110)
#define PARAMETER_ID_AD2S1210_CARRIER_FREQUENCY		                (111)
#define PARAMETER_ID_AD2S1210_REVERSED				                (112)
#define PARAMETER_ID_AD2S1210_OFFSET				                (113)
#define PARAMETER_ID_AD2S1210_PERIOD_PER_ROTATION	                (114)
#define PARAMETER_ID_AD2S1210_GAIN_CODE				                (115)

#define PARAMETER_ID_TLE5012_REVERSED				                (120)
#define PARAMETER_ID_TLE5012_OFFSET					                (121)

#define PARAMETER_ID_AURIXRDC_CARRIER_FREQUENCY    	                (130)

#define PARAMETER_ID_AURIXRDC0_RESOLUTION	     	                (140)
#define PARAMETER_ID_AURIXRDC0_REVERSED				                (141)
#define PARAMETER_ID_AURIXRDC0_OFFSET				                (142)
#define PARAMETER_ID_AURIXRDC0_PERIOD_PER_ROTATION	                (143)
#define PARAMETER_ID_AURIXRDC0_SIGNAL_AMPLITUDE_MIN	                (144)
#define PARAMETER_ID_AURIXRDC0_SIGNAL_AMPLITUDE_MAX	                (145)
#define PARAMETER_ID_AURIXRDC0_GAIN_CODE			                (146)

#define PARAMETER_ID_AURIXRDC1_RESOLUTION	     	                (150)
#define PARAMETER_ID_AURIXRDC1_REVERSED				                (151)
#define PARAMETER_ID_AURIXRDC1_OFFSET				                (152)
#define PARAMETER_ID_AURIXRDC1_PERIOD_PER_ROTATION	                (153)
#define PARAMETER_ID_AURIXRDC1_SIGNAL_AMPLITUDE_MIN	                (154)
#define PARAMETER_ID_AURIXRDC1_SIGNAL_AMPLITUDE_MAX	                (155)
#define PARAMETER_ID_AURIXRDC1_GAIN_CODE			                (156)

#define PARAMETER_ID_INVERTER0_POSITION_SENSOR0	     	            (200)
#define PARAMETER_ID_INVERTER0_POSITION_SENSOR1	     	            (201)
#define PARAMETER_ID_INVERTER0_PWM_MODULE		     	            (202)
#define PARAMETER_ID_INVERTER0_CURRENT_SENSOR_USE_THREE_SENSORS     (204)
#define PARAMETER_ID_INVERTER0_CURRENT_SENSOR_GAIN    			    (205)
#define PARAMETER_ID_INVERTER0_CURRENT_SENSOR_OFFSET    		    (206)
#define PARAMETER_ID_INVERTER0_CURRENT_SENSOR_MAX    			    (207)
#define PARAMETER_ID_INVERTER0_CURRENT_SENSOR_CUT_OFF_FREQUENCY     (208)

#define PARAMETER_ID_INVERTER1_POSITION_SENSOR0	     	            (250)
#define PARAMETER_ID_INVERTER1_POSITION_SENSOR1	     	            (251)
#define PARAMETER_ID_INVERTER1_PWM_MODULE		     	            (252)
#define PARAMETER_ID_INVERTER1_CURRENT_SENSOR_USE_THREE_SENSORS     (254)
#define PARAMETER_ID_INVERTER1_CURRENT_SENSOR_GAIN    			    (255)
#define PARAMETER_ID_INVERTER1_CURRENT_SENSOR_OFFSET    		    (256)
#define PARAMETER_ID_INVERTER1_CURRENT_SENSOR_MAX    			    (257)
#define PARAMETER_ID_INVERTER1_CURRENT_SENSOR_CUT_OFF_FREQUENCY     (258)

#define PARAMETER_ID_ANALOG_INPUT0_GAIN    						    (300)
#define PARAMETER_ID_ANALOG_INPUT0_OFFSET    					    (301)
#define PARAMETER_ID_ANALOG_INPUT0_MIN    						    (302)
#define PARAMETER_ID_ANALOG_INPUT0_MAX    						    (303)
#define PARAMETER_ID_ANALOG_INPUT0_CUT_OFF_FREQUENCY    		    (304)

#define PARAMETER_ID_ANALOG_INPUT1_GAIN    						    (310)
#define PARAMETER_ID_ANALOG_INPUT1_OFFSET    					    (311)
#define PARAMETER_ID_ANALOG_INPUT1_MIN    						    (312)
#define PARAMETER_ID_ANALOG_INPUT1_MAX    						    (313)
#define PARAMETER_ID_ANALOG_INPUT1_CUT_OFF_FREQUENCY    		    (314)

#define PARAMETER_ID_ANALOG_INPUT2_GAIN    						    (320)
#define PARAMETER_ID_ANALOG_INPUT2_OFFSET    					    (321)
#define PARAMETER_ID_ANALOG_INPUT2_MIN    						    (322)
#define PARAMETER_ID_ANALOG_INPUT2_MAX    						    (323)
#define PARAMETER_ID_ANALOG_INPUT2_CUT_OFF_FREQUENCY    		    (324)

#define PARAMETER_ID_ANALOG_INPUT3_GAIN    						    (330)
#define PARAMETER_ID_ANALOG_INPUT3_OFFSET    					    (331)
#define PARAMETER_ID_ANALOG_INPUT3_MIN    						    (332)
#define PARAMETER_ID_ANALOG_INPUT3_MAX    						    (333)
#define PARAMETER_ID_ANALOG_INPUT3_CUT_OFF_FREQUENCY    		    (334)

#define PARAMETER_ID_WINDOW_WATCHDOG_ENABLED    				    (400)
#define PARAMETER_ID_WINDOW_WATCHDOG_OPENED_WINDOW_TIME    		    (401)
#define PARAMETER_ID_WINDOW_WATCHDOG_CLOSED_WINDOW_TIME    		    (402)
#define PARAMETER_ID_WINDOW_WATCHDOG_USE_WDI_PIN	    		    (403)

#define PARAMETER_ID_FUNCTIONAL_WATCHDOG_ENABLED    			    (410)
#define PARAMETER_ID_FUNCTIONAL_WATCHDOG_HEART_BEAT_TIMER_PERIOD    (411)
#define PARAMETER_ID_FUNCTIONAL_WATCHDOG_SERVICE_PERIOD    		    (412)

#define PARAMETER_ID_FSP_ENABLED					    		    (420)

#define PARAMETER_ID_IOM_ENABLED					    		    (430)
#define PARAMETER_ID_IOM_FSP_ON_FAULT_ENABLED					    (431)
#define PARAMETER_ID_IOM_NMI_ON_FAULT_ENABLED					    (432)
#define PARAMETER_ID_IOM_EVENT_WINDOW_THRESHOLD					    (433)
#define PARAMETER_ID_IOM_FILTER_TIME					    	    (434)

#define PARAMETER_ID_WIZARD_ENABLED					    		    (500)
#define PARAMETER_ID_WIZARD_VDC_NOM					    		    (501)
#define PARAMETER_ID_WIZARD_VF					    			    (502)

#define PARAMETER_ID_SPECIAL_MODE_ENABLED					        (600)
#define PARAMETER_ID_SPECIAL_MODE_PWM_PERIOD					    (601)
#define PARAMETER_ID_SPECIAL_MODE_PULSE_PERIOD					    (602)
#define PARAMETER_ID_SPECIAL_MODE_START_PULSE					    (603)
#define PARAMETER_ID_SPECIAL_MODE_PULSE_INCREMENT				    (604)
#define PARAMETER_ID_SPECIAL_MODE_PULSE_COUNT_PER_PHASE			    (605)
#define PARAMETER_ID_SPECIAL_MODE_SEQUENCE_0_1					    (606)
/* FIXME define other PARAMETER_ID_SPECIAL_MODE_SEQUENCE_*_* */

/* Motor configuration \ref Ifx_MotorModelConfigF32_File */
#define PARAMETER_ID_MOTOR_FIRST				(10000)
#define PARAMETER_ID_MOTOR_LAST					(19999)
#define PARAMETER_ID_MOTOR_TYPE					        	(10000)
#define PARAMETER_ID_MOTOR_I_MAX					        (10001)
#define PARAMETER_ID_MOTOR_ID_KI					        (10002)
#define PARAMETER_ID_MOTOR_ID_KP					        (10003)
#define PARAMETER_ID_MOTOR_IQ_KI					        (10004)
#define PARAMETER_ID_MOTOR_IQ_KP					        (10005)
#define PARAMETER_ID_MOTOR_RS					            (10006)
#define PARAMETER_ID_MOTOR_LD					            (10007)
#define PARAMETER_ID_MOTOR_LQ					            (10008)
#define PARAMETER_ID_MOTOR_KT					            (10009)
#define PARAMETER_ID_MOTOR_I_STALL					        (10010)
#define PARAMETER_ID_MOTOR_POLE_PAIR					    (10011)
#define PARAMETER_ID_MOTOR_STATOR_MAX					    (10012)
#define PARAMETER_ID_MOTOR_RR					            (10013)
#define PARAMETER_ID_MOTOR_LS					            (10014)
#define PARAMETER_ID_MOTOR_LR					            (10015)
#define PARAMETER_ID_MOTOR_LM					            (10016)
#define PARAMETER_ID_MOTOR_FR					            (10017)
#define PARAMETER_ID_MOTOR_SPEED_MAX					    (10018)
#define PARAMETER_ID_MOTOR_SPEED_KP					        (10019)
#define PARAMETER_ID_MOTOR_SPEED_KI					        (10020)
#define PARAMETER_ID_MOTOR_TORQUE_MAX					    (10021)
#define PARAMETER_ID_MOTOR_TORQUE_RATE					    (10022)
#define PARAMETER_ID_MOTOR_FIELD_WEAKNING					(10023)

/* DB HPD Sense \ref DbHPDSense_FileConfiguration */
#define PARAMETER_ID_DBHPSENSE_FIRST				(20000)
#define PARAMETER_ID_DBHPSENSE_LAST					(29999)
#define PARAMETER_ID_DBHPSENSE_DRIVER_SPI_FREQUENCY				(20000)
#define PARAMETER_ID_DBHPSENSE_EEPROM_SPI_FREQUENCY				(20001)
#define PARAMETER_ID_DBHPSENSE_INVERTER_FREQUENCY				(20002)
#define PARAMETER_ID_DBHPSENSE_INVERTER_DEADTIME				(20003)
#define PARAMETER_ID_DBHPSENSE_INVERTER_MIN_PULSE				(20004)
#define PARAMETER_ID_DBHPSENSE_INVERTER_VDC_NOM					(20005)
#define PARAMETER_ID_DBHPSENSE_INVERTER_VDC_MAX_GEN				(20006)
#define PARAMETER_ID_DBHPSENSE_INVERTER_VDC_MIN					(20007)
#define PARAMETER_ID_DBHPSENSE_INVERTER_IGBT_TEMP_MAX		    (20008)
#define PARAMETER_ID_DBHPSENSE_INVERTER_IGBT_TEMP_MIN		    (20009)
#define PARAMETER_ID_DBHPSENSE_VDC_FULL_SCALE_OFFSET		    (20010)
#define PARAMETER_ID_DBHPSENSE_VDC_FULL_SCALE_GAIN				(20011)
#define PARAMETER_ID_DBHPSENSE_VDC_ZOOM_SCALE_OFFSET			(20012)
#define PARAMETER_ID_DBHPSENSE_VDC_ZOOM_SCALE_GAIN				(20013)
#define PARAMETER_ID_DBHPSENSE_IGBT_TEMP_FULL_SCALE_OFFSET		(20014)
#define PARAMETER_ID_DBHPSENSE_IGBT_TEMP_FULL_SCALE_GAIN		(20015)
#define PARAMETER_ID_DBHPSENSE_ACTIVE_CLAMPING_DACLC			(20016)
#define PARAMETER_ID_DBHPSENSE_TOW_LEVEL_VBE_COMPENSATION		(20017)
#define PARAMETER_ID_DBHPSENSE_TURN_ON_LEVEL					(20018)
#define PARAMETER_ID_DBHPSENSE_TURN_OFF_LEVEL					(20019)
#define PARAMETER_ID_DBHPSENSE_TURN_ON_DELAY					(20020)
#define PARAMETER_ID_DBHPSENSE_TURN_OFF_DELAY					(20021)
#define PARAMETER_ID_DBHPSENSE_BLANKING_TIME_OCPBT   			(20022)

/* DB HP2 SIL, DB HPD SIL \ref Db3x_FileConfiguration */
#define PARAMETER_ID_DB3X_FIRST								(30000)
#define PARAMETER_ID_DB3X_LAST								(39999)
#define PARAMETER_ID_DB3X_DRIVER_SPI_FREQUENCY				(30000)
#define PARAMETER_ID_DB3X_EEPROM_SPI_FREQUENCY				(30001)
#define PARAMETER_ID_DB3X_INVERTER_FREQUENCY				(30002)
#define PARAMETER_ID_DB3X_INVERTER_DEADTIME					(30003)
#define PARAMETER_ID_DB3X_INVERTER_MIN_PULSE				(30004)
#define PARAMETER_ID_DB3X_INVERTER_VDC_NOM					(30005)
#define PARAMETER_ID_DB3X_INVERTER_VDC_MAX_GEN				(30006)
#define PARAMETER_ID_DB3X_INVERTER_VDC_MIN					(30007)
#define PARAMETER_ID_DB3X_INVERTER_IGBT_TEMP_MAX		    (30008)
#define PARAMETER_ID_DB3X_INVERTER_IGBT_TEMP_MIN		    (30009)
#define PARAMETER_ID_DB3X_ACTIVE_CLAMPING_AT		    	(30010)
#define PARAMETER_ID_DB3X_ACTIVE_CLAMPING_ACLPM		    	(30011)
#define PARAMETER_ID_DB3X_TOW_LEVEL_VBE_COMPENSATION		(30012)
#define PARAMETER_ID_DB3X_TURN_ON_LEVEL						(30013)
#define PARAMETER_ID_DB3X_TURN_OFF_LEVEL					(30014)
#define PARAMETER_ID_DB3X_TURN_ON_DELAY						(30015)
#define PARAMETER_ID_DB3X_TURN_OFF_DELAY					(30016)



const AppShell_Parameter g_AppShell_parameterTable[] =
{
        {PARAMETER_ID_READY					                        , AppShell_DataType_boolean 	, "Ready"								        , ""    },
        {PARAMETER_ID_PRIMARY_CONSOLE		                        , AppShell_DataType_enum        , "Primary console"						        , ""    },
        {PARAMETER_ID_VERBOSE_LEVEL			                        , AppShell_DataType_enum        , "Verbose level"						        , ""    },

        {PARAMETER_ID_ENCODER_REVERSED		                        , AppShell_DataType_boolean     , "Encoder reversed"					        , ""    },
        {PARAMETER_ID_ENCODER_OFFSET			                    , AppShell_DataType_sint32      , "Encoder offset"						        , "Tick"    },
        {PARAMETER_ID_ENCODER_RESOLUTION		                    , AppShell_DataType_sint32      , "Encoder resolution"					        , "Tick"    },

        {PARAMETER_ID_AD2S1210_RESOLUTION	     	                , AppShell_DataType_sint32      , "AD2S1210 resolution"					        , "Tick"    },
        {PARAMETER_ID_AD2S1210_CARRIER_FREQUENCY		            , AppShell_DataType_sint32      , "AD2S1210 carrier frequency"			        , "Hz"    },
        {PARAMETER_ID_AD2S1210_REVERSED				                , AppShell_DataType_boolean     , "AD2S1210 reversed"					        , ""    },
        {PARAMETER_ID_AD2S1210_OFFSET				                , AppShell_DataType_sint32      , "AD2S1210 offset"						        , "Tick"    },
        {PARAMETER_ID_AD2S1210_PERIOD_PER_ROTATION	                , AppShell_DataType_uint32      , "AD2S1210 period per rotation"		        , ""    },
        {PARAMETER_ID_AD2S1210_GAIN_CODE				            , AppShell_DataType_enum        , "AD2S1210 gain code"					        , ""    },

        {PARAMETER_ID_TLE5012_REVERSED				                , AppShell_DataType_boolean     , "TLE5012 reversed"					        , ""    },
        {PARAMETER_ID_TLE5012_OFFSET					            , AppShell_DataType_sint32      , "TLE5012 offset"						        , "Tick"    },

        {PARAMETER_ID_AURIXRDC_CARRIER_FREQUENCY   	                , AppShell_DataType_sint32 	    , "AURIX RDC carrier frequency"			        , "Hz"    },

        {PARAMETER_ID_AURIXRDC0_RESOLUTION	     	                , AppShell_DataType_sint32 	    , "AURIX RDC0 resolution"				        , "Tick"    },
        {PARAMETER_ID_AURIXRDC0_REVERSED				            , AppShell_DataType_boolean     , "AURIX RDC0 reversed"					        , ""    },
        {PARAMETER_ID_AURIXRDC0_OFFSET				                , AppShell_DataType_sint32      , "AURIX RDC0 offset"					        , "Tick"    },
        {PARAMETER_ID_AURIXRDC0_PERIOD_PER_ROTATION	                , AppShell_DataType_uint32      , "AURIX RDC0 period per rotation"		        , ""    },
        {PARAMETER_ID_AURIXRDC0_SIGNAL_AMPLITUDE_MIN	            , AppShell_DataType_sint32      , "AURIX RDC0 signal amplitude min"		        , ""    },
        {PARAMETER_ID_AURIXRDC0_SIGNAL_AMPLITUDE_MAX	            , AppShell_DataType_sint32      , "AURIX RDC0 signal amplitude max"		        , ""    },
        {PARAMETER_ID_AURIXRDC0_GAIN_CODE			                , AppShell_DataType_enum        , "AURIX RDC0 gain code"				        , ""    },

        {PARAMETER_ID_AURIXRDC1_RESOLUTION	     	                , AppShell_DataType_sint32 	    , "AURIX RDC1 resolution"				        , "Tick"    },
        {PARAMETER_ID_AURIXRDC1_REVERSED				            , AppShell_DataType_boolean     , "AURIX RDC1 reversed"					        , ""    },
        {PARAMETER_ID_AURIXRDC1_OFFSET				                , AppShell_DataType_sint32      , "AURIX RDC1 offset"					        , "Tick"    },
        {PARAMETER_ID_AURIXRDC1_PERIOD_PER_ROTATION	                , AppShell_DataType_uint32      , "AURIX RDC1 period per rotation"		        , ""    },
        {PARAMETER_ID_AURIXRDC1_SIGNAL_AMPLITUDE_MIN	            , AppShell_DataType_sint32      , "AURIX RDC1 signal amplitude min"		        , ""    },
        {PARAMETER_ID_AURIXRDC1_SIGNAL_AMPLITUDE_MAX	            , AppShell_DataType_sint32      , "AURIX RDC1 signal amplitude max"		        , ""    },
        {PARAMETER_ID_AURIXRDC1_GAIN_CODE			                , AppShell_DataType_enum        , "AURIX RDC1 gain code"				        , ""    },

        {PARAMETER_ID_INVERTER0_POSITION_SENSOR0	     	        , AppShell_DataType_enum        , "Inverter 0 position sensor 0"		        , ""    },
        {PARAMETER_ID_INVERTER0_POSITION_SENSOR1	     	        , AppShell_DataType_enum        , "Inverter 0 position sensor 1"		        , ""    },
        {PARAMETER_ID_INVERTER0_PWM_MODULE		     	            , AppShell_DataType_enum        , "Inverter 0 PWM module"				        , ""    },
        {PARAMETER_ID_INVERTER0_CURRENT_SENSOR_USE_THREE_SENSORS    , AppShell_DataType_boolean     , "Inverter 0 current sensor with 3 sensors"	, ""    },	/* FIXME enable 0 sensors */
        {PARAMETER_ID_INVERTER0_CURRENT_SENSOR_GAIN    			    , AppShell_DataType_float32     , "Inverter 0 current sensor gain"				, "A/V"    },
        {PARAMETER_ID_INVERTER0_CURRENT_SENSOR_OFFSET    		    , AppShell_DataType_float32     , "Inverter 0 current sensor offset"			, "V"    },
        {PARAMETER_ID_INVERTER0_CURRENT_SENSOR_MAX    			    , AppShell_DataType_float32     , "Inverter 0 current sensor max"				, "A"    },
        {PARAMETER_ID_INVERTER0_CURRENT_SENSOR_CUT_OFF_FREQUENCY    , AppShell_DataType_float32     , "Inverter 0 current sensor cut off frequency"	, "Hz"    },

        {PARAMETER_ID_INVERTER1_POSITION_SENSOR0	     	        , AppShell_DataType_enum        , "Inverter 1 position sensor 0"		        , ""    },
        {PARAMETER_ID_INVERTER1_POSITION_SENSOR1	     	        , AppShell_DataType_enum        , "Inverter 1 position sensor 1"		        , ""    },
        {PARAMETER_ID_INVERTER1_PWM_MODULE		     	            , AppShell_DataType_enum        , "Inverter 1 PWM module"				        , ""    },
        {PARAMETER_ID_INVERTER1_CURRENT_SENSOR_USE_THREE_SENSORS    , AppShell_DataType_boolean     , "Inverter 1 current sensor with 3 sensors"	, ""    },	/* FIXME enable 0 sensors */
        {PARAMETER_ID_INVERTER1_CURRENT_SENSOR_GAIN    			    , AppShell_DataType_float32     , "Inverter 1 current sensor gain"				, "A/V"    },
        {PARAMETER_ID_INVERTER1_CURRENT_SENSOR_OFFSET    		    , AppShell_DataType_float32     , "Inverter 1 current sensor offset"			, "V"    },
        {PARAMETER_ID_INVERTER1_CURRENT_SENSOR_MAX    			    , AppShell_DataType_float32     , "Inverter 1 current sensor max"				, "A"    },
        {PARAMETER_ID_INVERTER1_CURRENT_SENSOR_CUT_OFF_FREQUENCY    , AppShell_DataType_float32     , "Inverter 1 current sensor cut off frequency"	, "Hz"    },

        {PARAMETER_ID_ANALOG_INPUT0_GAIN    						, AppShell_DataType_float32     , "Analog input 0 gain"							, ""    },
        {PARAMETER_ID_ANALOG_INPUT0_OFFSET    					    , AppShell_DataType_float32     , "Analog input 0 offset"			            , "V"    },
        {PARAMETER_ID_ANALOG_INPUT0_MIN    						    , AppShell_DataType_float32     , "Analog input 0 min"				            , "V"    },
        {PARAMETER_ID_ANALOG_INPUT0_MAX    						    , AppShell_DataType_float32     , "Analog input 0 max"				            , "V"    },
        {PARAMETER_ID_ANALOG_INPUT0_CUT_OFF_FREQUENCY    		    , AppShell_DataType_float32     , "Analog input 0 cut off frequency"            , "Hz"    },

        {PARAMETER_ID_ANALOG_INPUT1_GAIN    						, AppShell_DataType_float32     , "Analog input 1 gain"				            , ""    },
        {PARAMETER_ID_ANALOG_INPUT1_OFFSET    					    , AppShell_DataType_float32     , "Analog input 1 offset"			            , "V"    },
        {PARAMETER_ID_ANALOG_INPUT1_MIN    						    , AppShell_DataType_float32     , "Analog input 1 min"				            , "V"    },
        {PARAMETER_ID_ANALOG_INPUT1_MAX    						    , AppShell_DataType_float32     , "Analog input 1 max"				            , "V"    },
        {PARAMETER_ID_ANALOG_INPUT1_CUT_OFF_FREQUENCY    		    , AppShell_DataType_float32     , "Analog input 1 cut off frequency"            , "Hz"    },

        {PARAMETER_ID_ANALOG_INPUT2_GAIN    						, AppShell_DataType_float32     , "Analog input 2 gain"				            , ""    },
        {PARAMETER_ID_ANALOG_INPUT2_OFFSET    					    , AppShell_DataType_float32     , "Analog input 2 offset"			            , "V"    },
        {PARAMETER_ID_ANALOG_INPUT2_MIN    						    , AppShell_DataType_float32     , "Analog input 2 min"				            , "V"    },
        {PARAMETER_ID_ANALOG_INPUT2_MAX    						    , AppShell_DataType_float32     , "Analog input 2 max"				            , "V"    },
        {PARAMETER_ID_ANALOG_INPUT2_CUT_OFF_FREQUENCY    		    , AppShell_DataType_float32     , "Analog input 2 cut off frequency"            , "Hz"    },

        {PARAMETER_ID_ANALOG_INPUT3_GAIN    						, AppShell_DataType_float32     , "Analog input 3 gain"				            , ""    },
        {PARAMETER_ID_ANALOG_INPUT3_OFFSET    					    , AppShell_DataType_float32     , "Analog input 3 offset"			            , "V"    },
        {PARAMETER_ID_ANALOG_INPUT3_MIN    						    , AppShell_DataType_float32     , "Analog input 3 min"				            , "V"    },
        {PARAMETER_ID_ANALOG_INPUT3_MAX    						    , AppShell_DataType_float32     , "Analog input 3 max"				            , "V"    },
        {PARAMETER_ID_ANALOG_INPUT3_CUT_OFF_FREQUENCY    		    , AppShell_DataType_float32     , "Analog input 3 cut off frequency"            , "Hz"    },

        {PARAMETER_ID_WINDOW_WATCHDOG_ENABLED    				    , AppShell_DataType_boolean     , "Window watchdog enable"			            , ""    },
        {PARAMETER_ID_WINDOW_WATCHDOG_OPENED_WINDOW_TIME    		, AppShell_DataType_float32     , "Window watchdog opened window time"          , "s"    },
        {PARAMETER_ID_WINDOW_WATCHDOG_CLOSED_WINDOW_TIME    		, AppShell_DataType_float32     , "Window watchdog closed window time"          , "s"    },
        {PARAMETER_ID_WINDOW_WATCHDOG_USE_WDI_PIN	    		    , AppShell_DataType_boolean     , "Window watchdog use WDI pin"		            , ""    },

        {PARAMETER_ID_FUNCTIONAL_WATCHDOG_ENABLED    			    , AppShell_DataType_boolean     , "Functional watchdog enabled"		            , ""    },
        {PARAMETER_ID_FUNCTIONAL_WATCHDOG_HEART_BEAT_TIMER_PERIOD   , AppShell_DataType_float32     , "Functional watchdog heart beat timer period"	, "s"    },
        {PARAMETER_ID_FUNCTIONAL_WATCHDOG_SERVICE_PERIOD    		, AppShell_DataType_float32     , "Functional watchdog service period"			, "s"    },

        {PARAMETER_ID_FSP_ENABLED					    		    , AppShell_DataType_boolean     , "FSP enable"						            , ""    },

        {PARAMETER_ID_IOM_ENABLED					    		    , AppShell_DataType_boolean     , "IOM enable"						            , ""    },
        {PARAMETER_ID_IOM_FSP_ON_FAULT_ENABLED					    , AppShell_DataType_boolean     , "IOM FSP on fault"				            , ""    },
        {PARAMETER_ID_IOM_NMI_ON_FAULT_ENABLED					    , AppShell_DataType_boolean     , "IOM NMI on fault"				            , ""    },
        {PARAMETER_ID_IOM_EVENT_WINDOW_THRESHOLD					, AppShell_DataType_float32     , "IOM event window threshold"                  , "us"	},
        {PARAMETER_ID_IOM_FILTER_TIME					    	    , AppShell_DataType_float32     , "IOM filter time"					            , "us"    },

        {PARAMETER_ID_WIZARD_ENABLED					    		, AppShell_DataType_boolean     , "Wizard enabled"					            , ""    },
        {PARAMETER_ID_WIZARD_VDC_NOM					    		, AppShell_DataType_float32     , "Wizard Vdc nom"					            , "V"    },
        {PARAMETER_ID_WIZARD_VF					    			    , AppShell_DataType_float32     , "Wizard V/F constant"				            , "V/Hz"    },

        {PARAMETER_ID_SPECIAL_MODE_ENABLED					        , AppShell_DataType_boolean     , "Special mode enabled"			            , ""    },
        {PARAMETER_ID_SPECIAL_MODE_PWM_PERIOD					    , AppShell_DataType_float32     , "Special mode PWM period"			            , "us"    },
        {PARAMETER_ID_SPECIAL_MODE_PULSE_PERIOD					    , AppShell_DataType_float32     , "Special mode pulse period"		            , "us"    },
        {PARAMETER_ID_SPECIAL_MODE_START_PULSE					    , AppShell_DataType_float32     , "Special mode start pulse"		            , "us"    },
        {PARAMETER_ID_SPECIAL_MODE_PULSE_INCREMENT				    , AppShell_DataType_uint32      , "Special mode pulse increment"		        , "us"    },
        {PARAMETER_ID_SPECIAL_MODE_PULSE_COUNT_PER_PHASE			, AppShell_DataType_uint32      , "Special mode pulse count per phase"	        , ""    },

        /* Motor configuration \ref Ifx_MotorModelConfigF32_File */
        {PARAMETER_ID_MOTOR_TYPE					        	     , AppShell_DataType_enum       , "Type"                                        , ""          },
        {PARAMETER_ID_MOTOR_I_MAX					                 , AppShell_DataType_float32    , "Max phase current"                           , "A"         },
        {PARAMETER_ID_MOTOR_ID_KI					                 , AppShell_DataType_float32    , "Id PI controller KI"                         , ""          },
        {PARAMETER_ID_MOTOR_ID_KP					                 , AppShell_DataType_float32    , "Id PI controller KP"                         , ""          },
        {PARAMETER_ID_MOTOR_IQ_KI					                 , AppShell_DataType_float32    , "Iq PI controller KI"                         , ""          },
        {PARAMETER_ID_MOTOR_IQ_KP					                 , AppShell_DataType_float32    , "Iq PI controller KP"                         , ""          },
        {PARAMETER_ID_MOTOR_RS					                     , AppShell_DataType_float32    , "Stator resistance"                           , "Ohm"       },
        {PARAMETER_ID_MOTOR_LD					                     , AppShell_DataType_float32    , "Stator inductance Ld"                        , "H"         },
        {PARAMETER_ID_MOTOR_LQ					                     , AppShell_DataType_float32    , "Stator inductance Lq"                        , "H"         },
        {PARAMETER_ID_MOTOR_KT					                     , AppShell_DataType_float32    , "Torque constant"                             , "Nm/Arms"   },
        {PARAMETER_ID_MOTOR_I_STALL					                 , AppShell_DataType_float32    , "Stall current"                               , "A"         },
        {PARAMETER_ID_MOTOR_POLE_PAIR					             , AppShell_DataType_uint32     , "Pole pair"                                   , ""          },
        {PARAMETER_ID_MOTOR_STATOR_MAX					             , AppShell_DataType_float32    , "Maximum stator electrical speed"             , "rad/s"     },
        {PARAMETER_ID_MOTOR_RR					                     , AppShell_DataType_float32    , "Rotor default resistance"                    , "Ohm"       },
        {PARAMETER_ID_MOTOR_LS					                     , AppShell_DataType_float32    , "Stator default inductance"                   , "H"         },
        {PARAMETER_ID_MOTOR_LR					                     , AppShell_DataType_float32    , "Rotor default inductance"                    , "H"         },
        {PARAMETER_ID_MOTOR_LM					                     , AppShell_DataType_float32    , "mutual default inductantce"                  , "H"         },
        {PARAMETER_ID_MOTOR_FR					                     , AppShell_DataType_float32    , "Rotor default flux"                          , "V.s"       },
        {PARAMETER_ID_MOTOR_SPEED_MAX					             , AppShell_DataType_float32    , "Max speed"                                   , "rpm"       },
        {PARAMETER_ID_MOTOR_SPEED_KP					             , AppShell_DataType_float32    , "Speed PI controller KP"                      , ""          },
        {PARAMETER_ID_MOTOR_SPEED_KI					             , AppShell_DataType_float32    , "Speed PI controller KI"                      , ""          },
        {PARAMETER_ID_MOTOR_TORQUE_MAX					             , AppShell_DataType_float32    , "Max torque"                                  , "Nm"        },
        {PARAMETER_ID_MOTOR_TORQUE_RATE					             , AppShell_DataType_float32    , "Torque change rate"                          , "Nm/s"      },
        {PARAMETER_ID_MOTOR_FIELD_WEAKNING					         , AppShell_DataType_enum       , "Field weakning"                              , ""          },

        /* DB HPD Sense \ref DbHPDSense_FileConfiguration */
        {PARAMETER_ID_DBHPSENSE_DRIVER_SPI_FREQUENCY			     , AppShell_DataType_float32    , "Eice driver SPI frequency"                   , "Hz"        },
        {PARAMETER_ID_DBHPSENSE_EEPROM_SPI_FREQUENCY			     , AppShell_DataType_float32    , "EEPROM SPI frequency"                        , "Hz"        },
        {PARAMETER_ID_DBHPSENSE_INVERTER_FREQUENCY			         , AppShell_DataType_float32    , "PWM frequency"                               , "Hz"        },
        {PARAMETER_ID_DBHPSENSE_INVERTER_DEADTIME			         , AppShell_DataType_float32    , "Deadtime"                                    , "s"         },
        {PARAMETER_ID_DBHPSENSE_INVERTER_MIN_PULSE			         , AppShell_DataType_float32    , "Minimum pulse"                               , "s"         },
        {PARAMETER_ID_DBHPSENSE_INVERTER_VDC_NOM				     , AppShell_DataType_float32    , "Vdc nominal"                                 , "V"         },
        {PARAMETER_ID_DBHPSENSE_INVERTER_VDC_MAX_GEN			     , AppShell_DataType_float32    , "Maximum allowed DC link voltage"             , "V"         },
        {PARAMETER_ID_DBHPSENSE_INVERTER_VDC_MIN				     , AppShell_DataType_float32    , "Minimum allowed DC link voltage"             , "V"         },
        {PARAMETER_ID_DBHPSENSE_INVERTER_IGBT_TEMP_MAX		         , AppShell_DataType_float32    , "Maximum allowed IGBT temperature"            , "Degree C"  },
        {PARAMETER_ID_DBHPSENSE_INVERTER_IGBT_TEMP_MIN		         , AppShell_DataType_float32    , "Minimum allowed IGBT temperature"            , "Degree C"  },
        {PARAMETER_ID_DBHPSENSE_VDC_FULL_SCALE_OFFSET		         , AppShell_DataType_enum       , "Vdc Full scale offset"                       , ""          },
        {PARAMETER_ID_DBHPSENSE_VDC_FULL_SCALE_GAIN			         , AppShell_DataType_enum       , "Vdc Full scale gain"                         , ""          },
        {PARAMETER_ID_DBHPSENSE_VDC_ZOOM_SCALE_OFFSET		         , AppShell_DataType_enum       , "Vdc Zoom scale offset"                       , ""          },
        {PARAMETER_ID_DBHPSENSE_VDC_ZOOM_SCALE_GAIN			         , AppShell_DataType_enum       , "Vdc Zoom scale gain"                         , ""          },
        {PARAMETER_ID_DBHPSENSE_IGBT_TEMP_FULL_SCALE_OFFSET	         , AppShell_DataType_enum       , "IGBT temperature Full scale offset"          , ""          },
        {PARAMETER_ID_DBHPSENSE_IGBT_TEMP_FULL_SCALE_GAIN	         , AppShell_DataType_enum       , "IGBT temperature Full scale gain"            , ""          },
        {PARAMETER_ID_DBHPSENSE_ACTIVE_CLAMPING_DACLC		         , AppShell_DataType_uint32     , "Active clamping (SCFG.DACLC)"                , ""          },
        {PARAMETER_ID_DBHPSENSE_TOW_LEVEL_VBE_COMPENSATION	         , AppShell_DataType_enum       , "Vbe compensation"                            , ""          },
        {PARAMETER_ID_DBHPSENSE_TURN_ON_LEVEL				         , AppShell_DataType_enum       , "Gate turn on plateau level"                  , ""          },
        {PARAMETER_ID_DBHPSENSE_TURN_OFF_LEVEL				         , AppShell_DataType_enum       , "Gate turn off plateau level"                 , ""          },
        {PARAMETER_ID_DBHPSENSE_TURN_ON_DELAY				         , AppShell_DataType_uint32     , "Turn on delay. range =[0,255]"               , ""          },
        {PARAMETER_ID_DBHPSENSE_TURN_OFF_DELAY				         , AppShell_DataType_uint32     , "Turn on delay. range =[0,255]"               , ""          },
        {PARAMETER_ID_DBHPSENSE_BLANKING_TIME_OCPBT   	             , AppShell_DataType_uint32     , "Blanking time (SOCP.OCPBT). Range ={0, [10,255]}"  , ""          },

        /* DB HP2 SIL, DB HPD SIL \ref Db3x_FileConfiguration */
        {PARAMETER_ID_DB3X_DRIVER_SPI_FREQUENCY				         , AppShell_DataType_float32    , "Eice driver SPI frequency"                   , "Hz"        },
        {PARAMETER_ID_DB3X_EEPROM_SPI_FREQUENCY				         , AppShell_DataType_float32    , "EEPROM SPI frequency"                        , "Hz"        },
        {PARAMETER_ID_DB3X_INVERTER_FREQUENCY				         , AppShell_DataType_float32    , "PWM frequency"                               , "Hz"        },
        {PARAMETER_ID_DB3X_INVERTER_DEADTIME					     , AppShell_DataType_float32    , "Deadtime"                                    , "s"         },
        {PARAMETER_ID_DB3X_INVERTER_MIN_PULSE				         , AppShell_DataType_float32    , "Minimum pulse"                               , "s"         },
        {PARAMETER_ID_DB3X_INVERTER_VDC_NOM					         , AppShell_DataType_float32    , "Vdc nominal"                                 , "V"         },
        {PARAMETER_ID_DB3X_INVERTER_VDC_MAX_GEN				         , AppShell_DataType_float32    , "Maximum allowed DC link voltage"             , "V"         },
        {PARAMETER_ID_DB3X_INVERTER_VDC_MIN					         , AppShell_DataType_float32    , "Minimum allowed DC link voltage"             , "V"         },
        {PARAMETER_ID_DB3X_INVERTER_IGBT_TEMP_MAX		             , AppShell_DataType_float32    , "Maximum allowed IGBT temperature"            , "Degree C"  },
        {PARAMETER_ID_DB3X_INVERTER_IGBT_TEMP_MIN		             , AppShell_DataType_float32    , "Minimum allowed IGBT temperature"            , "Degree C"  },
        {PARAMETER_ID_DB3X_ACTIVE_CLAMPING_AT		    	         , AppShell_DataType_uint32     , "Active clamping activation time (SACLT.AT)"  , ""          },
        {PARAMETER_ID_DB3X_ACTIVE_CLAMPING_ACLPM		    	     , AppShell_DataType_uint32     , "Active clamping mode (SCFG2.ACLPM)"          , ""          },
        {PARAMETER_ID_DB3X_TOW_LEVEL_VBE_COMPENSATION		         , AppShell_DataType_enum       , "Vbe compensation"                            , ""          },
        {PARAMETER_ID_DB3X_TURN_ON_LEVEL						     , AppShell_DataType_enum       , "Gate turn on plateau level"                  , ""          },
        {PARAMETER_ID_DB3X_TURN_OFF_LEVEL					         , AppShell_DataType_enum       , "Gate turn off plateau level"                 , ""          },
        {PARAMETER_ID_DB3X_TURN_ON_DELAY						     , AppShell_DataType_uint32     , "Turn on delay. range =[0,255]"               , ""          },
        {PARAMETER_ID_DB3X_TURN_OFF_DELAY					         , AppShell_DataType_uint32     , "Turn on delay. range =[0,255]"               , ""          },

		PARAMETER_ID_TABLE_END
};

#    define ONEEYE_ENDL                             "\r"


boolean AppShell_isParamValid(const AppShell_Parameter *param)
{
	boolean valid = TRUE;
	if (param)
	{
		if ((param->id >= PARAMETER_ID_LB_FIRST) && (param->id <= PARAMETER_ID_LB_LAST))
		{
		}
		else if ((param->id >= PARAMETER_ID_MOTOR_FIRST) && (param->id <= PARAMETER_ID_MOTOR_LAST))
		{
		}
		else if (
				((AppDbStdIf_getBoardVersion(&g_Lb.driverBoardStdif)->boardType == Lb_BoardType_HybridKit_DriverBoardHpDriveEiceSense)
				|| (AppDbStdIf_getBoardVersion(&g_Lb.driverBoardStdif)->boardType == Lb_BoardType_HybridKit_DriverBoardHpDriveEiceSenseDsc)
						)
				&& ((param->id >= PARAMETER_ID_DBHPSENSE_FIRST) && (param->id <= PARAMETER_ID_DBHPSENSE_LAST)))
		{

		}
		else if(
				((AppDbStdIf_getBoardVersion(&g_Lb.driverBoardStdif)->boardType == Lb_BoardType_HybridKit_DriverBoardHp2)
				||
				(AppDbStdIf_getBoardVersion(&g_Lb.driverBoardStdif)->boardType == Lb_BoardType_HybridKit_DriverBoardHpDrive))
				&& ((param->id >= PARAMETER_ID_DB3X_FIRST) && (param->id <= PARAMETER_ID_DB3X_LAST)))
		{
		}
		else
		{
			valid = FALSE;
		}
	}
	else
	{
		valid = FALSE;
	}

	return valid;

}


boolean AppShell_sendParam(IfxStdIf_DPipe *io, const AppShell_Parameter *param)
{
	boolean result = TRUE;
	LB_FileConfiguration *configurationFile = g_Lb.stdIf.configuration;
	Ifx_MotorModelConfigF32_File* configurationFileMotor = &g_Lb.motorConfiguration.data;
	DbHPDSense_FileConfiguration* configurationFileDBHpdSense = &g_Lb.driverBoards.driverBoard_HPDSense.configuration;
	Db3x_FileConfiguration* configurationFileDb3x = &g_Lb.driverBoards.driverBoard_3x.configuration;

	if (!AppShell_isParamValid(param))
	{
		param = NULL_PTR;
	}

	if (param != NULL_PTR)
	{
	    uint32 id;
	    id = param->id;

	    pchar format;

	    if (param->dataType == AppShell_DataType_float32)
	    {
	    	format = "! p %d %f"ONEEYE_ENDL;
	    }
	    else
	    {
	    	format = "! p %d %d"ONEEYE_ENDL;
	    }

	    	switch (id)
	    	{
			case PARAMETER_ID_READY					                        :
//		        IfxStdIf_DPipe_print(io, format, id, configurationFile->ready);
	    		break;
			case PARAMETER_ID_PRIMARY_CONSOLE		                        :
//		        IfxStdIf_DPipe_print(io, format, id, configurationFile->primaryConsole);
	    		break;
			case PARAMETER_ID_VERBOSE_LEVEL			                        :
//		        IfxStdIf_DPipe_print(io, format, id, configurationFile->verboseLevel);
	    		break;
	    	case PARAMETER_ID_ENCODER_REVERSED:
//		        IfxStdIf_DPipe_print(io, format, id, configurationFile->positionSensors.encoder.reversed);
	    		break;
	    	case PARAMETER_ID_ENCODER_OFFSET:
//		        IfxStdIf_DPipe_print(io, format, id, configurationFile->positionSensors.encoder.offset);
	    		break;
	    	case PARAMETER_ID_ENCODER_RESOLUTION:
//			        IfxStdIf_DPipe_print(io, format, id, configurationFile->positionSensors.encoder.resolution);
	    		break;
			case PARAMETER_ID_AD2S1210_RESOLUTION	     	                :
//		        IfxStdIf_DPipe_print(io, format, id, configurationFile->positionSensors.ad2s1210.resolution);
	    		break;
			case PARAMETER_ID_AD2S1210_CARRIER_FREQUENCY		                :
//		        IfxStdIf_DPipe_print(io, format, id, configurationFile->positionSensors.ad2s1210.carrierFrequency);
	    		break;
			case PARAMETER_ID_AD2S1210_REVERSED				                :
//		        IfxStdIf_DPipe_print(io, format, id, configurationFile->positionSensors.ad2s1210.reversed);
	    		break;
			case PARAMETER_ID_AD2S1210_OFFSET				                :
//		        IfxStdIf_DPipe_print(io, format, id, configurationFile->positionSensors.ad2s1210.offset);
	    		break;
			case PARAMETER_ID_AD2S1210_PERIOD_PER_ROTATION	                :
//		        IfxStdIf_DPipe_print(io, format, id, configurationFile->positionSensors.ad2s1210.periodPerRotation);
	    		break;
			case PARAMETER_ID_AD2S1210_GAIN_CODE				                :
//		        IfxStdIf_DPipe_print(io, format, id, configurationFile->positionSensors.ad2s1210.gainCode);
	    		break;
			case PARAMETER_ID_TLE5012_REVERSED				                :
//		        IfxStdIf_DPipe_print(io, format, id, configurationFile->positionSensors.tle5012.reversed);
	    		break;
			case PARAMETER_ID_TLE5012_OFFSET					                :
//		        IfxStdIf_DPipe_print(io, format, id, configurationFile->positionSensors.tle5012.offset);
	    		break;
			case PARAMETER_ID_AURIXRDC_CARRIER_FREQUENCY    	                :
//		        IfxStdIf_DPipe_print(io, format, id, configurationFile->positionSensors.aurixResolver.carrierFrequency);
	    		break;
			case PARAMETER_ID_AURIXRDC0_RESOLUTION	     	                :
//		        IfxStdIf_DPipe_print(io, format, id, configurationFile->positionSensors.aurixResolver.input[0].resolution);
	    		break;
			case PARAMETER_ID_AURIXRDC0_REVERSED				                :
//		        IfxStdIf_DPipe_print(io, format, id, configurationFile->positionSensors.aurixResolver.input[0].reversed);
	    		break;
			case PARAMETER_ID_AURIXRDC0_OFFSET				                :
//		        IfxStdIf_DPipe_print(io, format, id, configurationFile->positionSensors.aurixResolver.input[0].offset);
	    		break;
			case PARAMETER_ID_AURIXRDC0_PERIOD_PER_ROTATION	                :
//		        IfxStdIf_DPipe_print(io, format, id, configurationFile->positionSensors.aurixResolver.input[0].periodPerRotation);
	    		break;
			case PARAMETER_ID_AURIXRDC0_SIGNAL_AMPLITUDE_MIN	                :
//		        IfxStdIf_DPipe_print(io, format, id, configurationFile->positionSensors.aurixResolver.input[0].signalAmplitudeMin);
	    		break;
			case PARAMETER_ID_AURIXRDC0_SIGNAL_AMPLITUDE_MAX	                :
//		        IfxStdIf_DPipe_print(io, format, id, configurationFile->positionSensors.aurixResolver.input[0].signalAmplitudeMax);
	    		break;
			case PARAMETER_ID_AURIXRDC0_GAIN_CODE			                :
//		        IfxStdIf_DPipe_print(io, format, id, configurationFile->positionSensors.aurixResolver.input[0].gainCode);
	    		break;
			case PARAMETER_ID_AURIXRDC1_RESOLUTION	     	                :
//		        IfxStdIf_DPipe_print(io, format, id, configurationFile->positionSensors.aurixResolver.input[1].resolution);
	    		break;
			case PARAMETER_ID_AURIXRDC1_REVERSED				                :
//		        IfxStdIf_DPipe_print(io, format, id, configurationFile->positionSensors.aurixResolver.input[1].reversed);
	    		break;
			case PARAMETER_ID_AURIXRDC1_OFFSET				                :
//		        IfxStdIf_DPipe_print(io, format, id, configurationFile->positionSensors.aurixResolver.input[1].offset);
	    		break;
			case PARAMETER_ID_AURIXRDC1_PERIOD_PER_ROTATION	                :
//		        IfxStdIf_DPipe_print(io, format, id, configurationFile->positionSensors.aurixResolver.input[1].periodPerRotation);
	    		break;
			case PARAMETER_ID_AURIXRDC1_SIGNAL_AMPLITUDE_MIN	                :
//		        IfxStdIf_DPipe_print(io, format, id, configurationFile->positionSensors.aurixResolver.input[1].signalAmplitudeMin);
	    		break;
			case PARAMETER_ID_AURIXRDC1_SIGNAL_AMPLITUDE_MAX	                :
//		        IfxStdIf_DPipe_print(io, format, id, configurationFile->positionSensors.aurixResolver.input[1].signalAmplitudeMax);
	    		break;
			case PARAMETER_ID_AURIXRDC1_GAIN_CODE			                :
//		        IfxStdIf_DPipe_print(io, format, id, configurationFile->positionSensors.aurixResolver.input[1].gainCode);
	    		break;
			case PARAMETER_ID_INVERTER0_POSITION_SENSOR0	     	            :
//		        IfxStdIf_DPipe_print(io, format, id, configurationFile->inverter[0].positionSensor[0]);
	    		break;
			case PARAMETER_ID_INVERTER0_POSITION_SENSOR1	     	            :
//		        IfxStdIf_DPipe_print(io, format, id, configurationFile->inverter[0].positionSensor[1]);
	    		break;
			case PARAMETER_ID_INVERTER0_PWM_MODULE		     	            :
//		        IfxStdIf_DPipe_print(io, format, id, configurationFile->inverter[0].pwmModule);
	    		break;
			case PARAMETER_ID_INVERTER0_CURRENT_SENSOR_USE_THREE_SENSORS     :
//		        IfxStdIf_DPipe_print(io, format, id, configurationFile->inverter[0].currentSenor.useThreeSensors);
	    		break;
			case PARAMETER_ID_INVERTER0_CURRENT_SENSOR_GAIN    			    :
//		        IfxStdIf_DPipe_print(io, format, id, (configurationFile->inverter[0].currentSenor.gain / CFG_ADC_GAIN));
	    		break;
			case PARAMETER_ID_INVERTER0_CURRENT_SENSOR_OFFSET    		    :
//		        IfxStdIf_DPipe_print(io, format, id, (-configurationFile->inverter[0].currentSenor.offset * CFG_ADC_GAIN));
	    		break;
			case PARAMETER_ID_INVERTER0_CURRENT_SENSOR_MAX    			    :
//		        IfxStdIf_DPipe_print(io, format, id, configurationFile->inverter[0].currentSenor.max);
	    		break;
			case PARAMETER_ID_INVERTER0_CURRENT_SENSOR_CUT_OFF_FREQUENCY     :
//		        IfxStdIf_DPipe_print(io, format, id, configurationFile->inverter[0].currentSenor.cutOffFrequency);
	    		break;
			case PARAMETER_ID_INVERTER1_POSITION_SENSOR0	     	            :
//		        IfxStdIf_DPipe_print(io, format, id, configurationFile->inverter[1].positionSensor[0]);
	    		break;
			case PARAMETER_ID_INVERTER1_POSITION_SENSOR1	     	            :
//		        IfxStdIf_DPipe_print(io, format, id, configurationFile->inverter[1].positionSensor[1]);
	    		break;
			case PARAMETER_ID_INVERTER1_PWM_MODULE		     	            :
//		        IfxStdIf_DPipe_print(io, format, id, configurationFile->inverter[1].pwmModule);
	    		break;
			case PARAMETER_ID_INVERTER1_CURRENT_SENSOR_USE_THREE_SENSORS     :
//		        IfxStdIf_DPipe_print(io, format, id, configurationFile->inverter[1].currentSenor.useThreeSensors);
	    		break;
			case PARAMETER_ID_INVERTER1_CURRENT_SENSOR_GAIN    			    :
//		        IfxStdIf_DPipe_print(io, format, id, (configurationFile->inverter[1].currentSenor.gain / CFG_ADC_GAIN));
	    		break;
			case PARAMETER_ID_INVERTER1_CURRENT_SENSOR_OFFSET    		    :
//		        IfxStdIf_DPipe_print(io, format, id, (-configurationFile->inverter[1].currentSenor.offset * CFG_ADC_GAIN));
	    		break;
			case PARAMETER_ID_INVERTER1_CURRENT_SENSOR_MAX    			    :
//		        IfxStdIf_DPipe_print(io, format, id, configurationFile->inverter[1].currentSenor.max);
	    		break;
			case PARAMETER_ID_INVERTER1_CURRENT_SENSOR_CUT_OFF_FREQUENCY     :
//		        IfxStdIf_DPipe_print(io, format, id, configurationFile->inverter[1].currentSenor.cutOffFrequency);
	    		break;
			case PARAMETER_ID_ANALOG_INPUT0_GAIN    						    :
//		        IfxStdIf_DPipe_print(io, format, id, (configurationFile->analogInput[0].gain / CFG_ADC_GAIN));
	    		break;
			case PARAMETER_ID_ANALOG_INPUT0_OFFSET    					    :
//		        IfxStdIf_DPipe_print(io, format, id, (configurationFile->analogInput[0].offset * CFG_ADC_GAIN));
	    		break;
			case PARAMETER_ID_ANALOG_INPUT0_MIN    						    :
//		        IfxStdIf_DPipe_print(io, format, id, configurationFile->analogInput[0].min);
	    		break;
			case PARAMETER_ID_ANALOG_INPUT0_MAX    						    :
//		        IfxStdIf_DPipe_print(io, format, id, configurationFile->analogInput[0].max);
	    		break;
			case PARAMETER_ID_ANALOG_INPUT0_CUT_OFF_FREQUENCY    		    :
//		        IfxStdIf_DPipe_print(io, format, id, configurationFile->analogInput[0].cutOffFrequency);
	    		break;
			case PARAMETER_ID_ANALOG_INPUT1_GAIN    						    :
//		        IfxStdIf_DPipe_print(io, format, id, (configurationFile->analogInput[1].gain / CFG_ADC_GAIN));
	    		break;
			case PARAMETER_ID_ANALOG_INPUT1_OFFSET    					    :
//		        IfxStdIf_DPipe_print(io, format, id, (-configurationFile->analogInput[1].offset * CFG_ADC_GAIN));
	    		break;
			case PARAMETER_ID_ANALOG_INPUT1_MIN    						    :
//		        IfxStdIf_DPipe_print(io, format, id, configurationFile->analogInput[1].min);
	    		break;
			case PARAMETER_ID_ANALOG_INPUT1_MAX    						    :
//		        IfxStdIf_DPipe_print(io, format, id, configurationFile->analogInput[1].max);
	    		break;
			case PARAMETER_ID_ANALOG_INPUT1_CUT_OFF_FREQUENCY    		    :
//		        IfxStdIf_DPipe_print(io, format, id, configurationFile->analogInput[1].cutOffFrequency);
	    		break;
			case PARAMETER_ID_ANALOG_INPUT2_GAIN    						    :
//		        IfxStdIf_DPipe_print(io, format, id, (configurationFile->analogInput[2].gain / CFG_ADC_GAIN));
	    		break;
			case PARAMETER_ID_ANALOG_INPUT2_OFFSET    					    :
//		        IfxStdIf_DPipe_print(io, format, id, (-configurationFile->analogInput[2].offset * CFG_ADC_GAIN));
	    		break;
			case PARAMETER_ID_ANALOG_INPUT2_MIN    						    :
//		        IfxStdIf_DPipe_print(io, format, id, configurationFile->analogInput[2].min);
	    		break;
			case PARAMETER_ID_ANALOG_INPUT2_MAX    						    :
//		        IfxStdIf_DPipe_print(io, format, id, configurationFile->analogInput[2].max);
	    		break;
			case PARAMETER_ID_ANALOG_INPUT2_CUT_OFF_FREQUENCY    		    :
//		        IfxStdIf_DPipe_print(io, format, id, configurationFile->analogInput[2].cutOffFrequency);
	    		break;
			case PARAMETER_ID_ANALOG_INPUT3_GAIN    						    :
//		        IfxStdIf_DPipe_print(io, format, id, (configurationFile->analogInput[3].gain / CFG_ADC_GAIN));
	    		break;
			case PARAMETER_ID_ANALOG_INPUT3_OFFSET    					    :
//		        IfxStdIf_DPipe_print(io, format, id, (-configurationFile->analogInput[3].offset * CFG_ADC_GAIN));
	    		break;
			case PARAMETER_ID_ANALOG_INPUT3_MIN    						    :
//		        IfxStdIf_DPipe_print(io, format, id, configurationFile->analogInput[3].min);
	    		break;
			case PARAMETER_ID_ANALOG_INPUT3_MAX    						    :
//		        IfxStdIf_DPipe_print(io, format, id, configurationFile->analogInput[3].max);
	    		break;
			case PARAMETER_ID_ANALOG_INPUT3_CUT_OFF_FREQUENCY    		    :
//		        IfxStdIf_DPipe_print(io, format, id, configurationFile->analogInput[3].cutOffFrequency);
	    		break;
			case PARAMETER_ID_WINDOW_WATCHDOG_ENABLED    				    :
//		        IfxStdIf_DPipe_print(io, format, id, configurationFile->safety.windowWatchdog.enabled);
	    		break;
			case PARAMETER_ID_WINDOW_WATCHDOG_OPENED_WINDOW_TIME    		    :
//		        IfxStdIf_DPipe_print(io, format, id, configurationFile->safety.windowWatchdog.openWindowTime);
	    		break;
			case PARAMETER_ID_WINDOW_WATCHDOG_CLOSED_WINDOW_TIME    		    :
//		        IfxStdIf_DPipe_print(io, format, id, configurationFile->safety.windowWatchdog.closeWindowTime);
	    		break;
			case PARAMETER_ID_WINDOW_WATCHDOG_USE_WDI_PIN	    		    :
//		        IfxStdIf_DPipe_print(io, format, id, configurationFile->safety.windowWatchdog.useWdiPin);
	    		break;
			case PARAMETER_ID_FUNCTIONAL_WATCHDOG_ENABLED    			    :
//		        IfxStdIf_DPipe_print(io, format, id, configurationFile->safety.functionalWatchdog.enabled);
	    		break;
			case PARAMETER_ID_FUNCTIONAL_WATCHDOG_HEART_BEAT_TIMER_PERIOD    :
//		        IfxStdIf_DPipe_print(io, format, id, configurationFile->safety.functionalWatchdog.heartbeatTimerPeriod);
	    		break;
			case PARAMETER_ID_FUNCTIONAL_WATCHDOG_SERVICE_PERIOD    		    :
//		        IfxStdIf_DPipe_print(io, format, id, configurationFile->safety.functionalWatchdog.servicePeriod);
	    		break;
			case PARAMETER_ID_FSP_ENABLED					    		    :
//		        IfxStdIf_DPipe_print(io, format, id, configurationFile->safety.fsp.enabled);
	    		break;
			case PARAMETER_ID_IOM_ENABLED					    		    :
//		        IfxStdIf_DPipe_print(io, format, id, configurationFile->safety.iom.enabled);
	    		break;
			case PARAMETER_ID_IOM_FSP_ON_FAULT_ENABLED					    :
//		        IfxStdIf_DPipe_print(io, format, id, configurationFile->safety.iom.fspOnFaultEnabled);
	    		break;
			case PARAMETER_ID_IOM_NMI_ON_FAULT_ENABLED					    :
//		        IfxStdIf_DPipe_print(io, format, id, configurationFile->safety.iom.nmiOnFaultEnabled);
	    		break;
			case PARAMETER_ID_IOM_EVENT_WINDOW_THRESHOLD					    :
//		        IfxStdIf_DPipe_print(io, format, id, (configurationFile->safety.iom.eventWindowThreshold*1e6));
	    		break;
			case PARAMETER_ID_IOM_FILTER_TIME					    	    :
//		        IfxStdIf_DPipe_print(io, format, id, configurationFile->safety.iom.filterTime*1e6);
	    		break;
			case PARAMETER_ID_WIZARD_ENABLED					    		    :
//		        IfxStdIf_DPipe_print(io, format, id, configurationFile->wizard.enabled);
	    		break;
			case PARAMETER_ID_WIZARD_VDC_NOM					    		    :
//		        IfxStdIf_DPipe_print(io, format, id, configurationFile->wizard.vdcNom);
	    		break;
			case PARAMETER_ID_WIZARD_VF					    			    :
//		        IfxStdIf_DPipe_print(io, format, id, configurationFile->wizard.vf);
	    		break;
			case PARAMETER_ID_SPECIAL_MODE_ENABLED					        :
//		        IfxStdIf_DPipe_print(io, format, id, configurationFile->specialMode.hwPulseMode.enabled);
	    		break;
			case PARAMETER_ID_SPECIAL_MODE_PWM_PERIOD					    :
//		        IfxStdIf_DPipe_print(io, format, id, (configurationFile->specialMode.hwPulseMode.pwmPeriod*1e6));
	    		break;
			case PARAMETER_ID_SPECIAL_MODE_PULSE_PERIOD					    :
//		        IfxStdIf_DPipe_print(io, format, id, (configurationFile->specialMode.hwPulseMode.pulsePeriod*1e6));
	    		break;
			case PARAMETER_ID_SPECIAL_MODE_START_PULSE					    :
//		        IfxStdIf_DPipe_print(io, format, id, (configurationFile->specialMode.hwPulseMode.startPulse*1e6));
	    		break;
			case PARAMETER_ID_SPECIAL_MODE_PULSE_INCREMENT				    :
//		        IfxStdIf_DPipe_print(io, format, id, (configurationFile->specialMode.hwPulseMode.pulseIncrement*1e6));
	    		break;
			case PARAMETER_ID_SPECIAL_MODE_PULSE_COUNT_PER_PHASE			    :
//		        IfxStdIf_DPipe_print(io, format, id, configurationFile->specialMode.hwPulseMode.pulseCountPerPhase);
	    		break;

	    		/* Motor configuration \ref Ifx_MotorModelConfigF32_File */
            case PARAMETER_ID_MOTOR_TYPE					        	  :  IfxStdIf_DPipe_print(io, format, id, configurationFileMotor->type);                          break;
            case PARAMETER_ID_MOTOR_I_MAX					              :  IfxStdIf_DPipe_print(io, format, id, configurationFileMotor->iMax);                          break;
            case PARAMETER_ID_MOTOR_ID_KI					              :  IfxStdIf_DPipe_print(io, format, id, configurationFileMotor->idKi);                          break;
            case PARAMETER_ID_MOTOR_ID_KP					              :  IfxStdIf_DPipe_print(io, format, id, configurationFileMotor->idKp);                          break;
            case PARAMETER_ID_MOTOR_IQ_KI					              :  IfxStdIf_DPipe_print(io, format, id, configurationFileMotor->iqKi);                          break;
            case PARAMETER_ID_MOTOR_IQ_KP					              :  IfxStdIf_DPipe_print(io, format, id, configurationFileMotor->iqKp);                          break;
            case PARAMETER_ID_MOTOR_RS					                  :  IfxStdIf_DPipe_print(io, format, id, configurationFileMotor->rs);                            break;
            case PARAMETER_ID_MOTOR_LD					                  :  IfxStdIf_DPipe_print(io, format, id, configurationFileMotor->ld);                            break;
            case PARAMETER_ID_MOTOR_LQ					                  :  IfxStdIf_DPipe_print(io, format, id, configurationFileMotor->lq);                            break;
            case PARAMETER_ID_MOTOR_KT					                  :  IfxStdIf_DPipe_print(io, format, id, configurationFileMotor->kt);                            break;
            case PARAMETER_ID_MOTOR_I_STALL					              :  IfxStdIf_DPipe_print(io, format, id, configurationFileMotor->iStall);                        break;
            case PARAMETER_ID_MOTOR_POLE_PAIR					          :  IfxStdIf_DPipe_print(io, format, id, configurationFileMotor->polePair);                      break;
            case PARAMETER_ID_MOTOR_STATOR_MAX					          :  IfxStdIf_DPipe_print(io, format, id, configurationFileMotor->statorMax);                     break;
            case PARAMETER_ID_MOTOR_RR					                  :  IfxStdIf_DPipe_print(io, format, id, configurationFileMotor->rr);                            break;
            case PARAMETER_ID_MOTOR_LS					                  :  IfxStdIf_DPipe_print(io, format, id, configurationFileMotor->ls);                            break;
            case PARAMETER_ID_MOTOR_LR					                  :  IfxStdIf_DPipe_print(io, format, id, configurationFileMotor->lr);                            break;
            case PARAMETER_ID_MOTOR_LM					                  :  IfxStdIf_DPipe_print(io, format, id, configurationFileMotor->lm);                            break;
            case PARAMETER_ID_MOTOR_FR					                  :  IfxStdIf_DPipe_print(io, format, id, configurationFileMotor->fr);                            break;
            case PARAMETER_ID_MOTOR_SPEED_MAX					          :  IfxStdIf_DPipe_print(io, format, id, configurationFileMotor->speedMax);                      break;
            case PARAMETER_ID_MOTOR_SPEED_KP					          :  IfxStdIf_DPipe_print(io, format, id, configurationFileMotor->speedKp);                       break;
            case PARAMETER_ID_MOTOR_SPEED_KI					          :  IfxStdIf_DPipe_print(io, format, id, configurationFileMotor->speedKi);                       break;
            case PARAMETER_ID_MOTOR_TORQUE_MAX					          :  IfxStdIf_DPipe_print(io, format, id, configurationFileMotor->torqueMax);                     break;
            case PARAMETER_ID_MOTOR_TORQUE_RATE					          :  IfxStdIf_DPipe_print(io, format, id, configurationFileMotor->torqueRate);                    break;
            case PARAMETER_ID_MOTOR_FIELD_WEAKNING					      :  IfxStdIf_DPipe_print(io, format, id, configurationFileMotor->fieldWeakning);                 break;

            /* DB HPD Sense \ref DbHPDSense_FileConfiguration */
            case PARAMETER_ID_DBHPSENSE_DRIVER_SPI_FREQUENCY			  :  IfxStdIf_DPipe_print(io, format, id, configurationFileDBHpdSense->driverSpiFrequency);       break;
            case PARAMETER_ID_DBHPSENSE_EEPROM_SPI_FREQUENCY			  :  IfxStdIf_DPipe_print(io, format, id, configurationFileDBHpdSense->eepromSpiFrequency);       break;
            case PARAMETER_ID_DBHPSENSE_INVERTER_FREQUENCY			      :  IfxStdIf_DPipe_print(io, format, id, configurationFileDBHpdSense->inverter.frequency);       break;
            case PARAMETER_ID_DBHPSENSE_INVERTER_DEADTIME			      :  IfxStdIf_DPipe_print(io, format, id, configurationFileDBHpdSense->inverter.deadtime);        break;
            case PARAMETER_ID_DBHPSENSE_INVERTER_MIN_PULSE			      :  IfxStdIf_DPipe_print(io, format, id, configurationFileDBHpdSense->inverter.minPulse);        break;
            case PARAMETER_ID_DBHPSENSE_INVERTER_VDC_NOM				  :  IfxStdIf_DPipe_print(io, format, id, configurationFileDBHpdSense->inverter.vdcNom);          break;
            case PARAMETER_ID_DBHPSENSE_INVERTER_VDC_MAX_GEN			  :  IfxStdIf_DPipe_print(io, format, id, configurationFileDBHpdSense->inverter.vdcMaxGen);       break;
            case PARAMETER_ID_DBHPSENSE_INVERTER_VDC_MIN				  :  IfxStdIf_DPipe_print(io, format, id, configurationFileDBHpdSense->inverter.vdcMin);          break;
            case PARAMETER_ID_DBHPSENSE_INVERTER_IGBT_TEMP_MAX		      :  IfxStdIf_DPipe_print(io, format, id, configurationFileDBHpdSense->inverter.igbtTempMax);     break;
            case PARAMETER_ID_DBHPSENSE_INVERTER_IGBT_TEMP_MIN		      :  IfxStdIf_DPipe_print(io, format, id, configurationFileDBHpdSense->inverter.igbtTempMin);     break;
            case PARAMETER_ID_DBHPSENSE_VDC_FULL_SCALE_OFFSET		      :  IfxStdIf_DPipe_print(io, format, id, configurationFileDBHpdSense->vdc.fullScaleOffset);      break;
            case PARAMETER_ID_DBHPSENSE_VDC_FULL_SCALE_GAIN			      :  IfxStdIf_DPipe_print(io, format, id, configurationFileDBHpdSense->vdc.fullScaleGain);        break;
            case PARAMETER_ID_DBHPSENSE_VDC_ZOOM_SCALE_OFFSET		      :  IfxStdIf_DPipe_print(io, format, id, configurationFileDBHpdSense->vdc.zoomOffset);           break;
            case PARAMETER_ID_DBHPSENSE_VDC_ZOOM_SCALE_GAIN			      :  IfxStdIf_DPipe_print(io, format, id, configurationFileDBHpdSense->vdc.zoomGain);             break;
            case PARAMETER_ID_DBHPSENSE_IGBT_TEMP_FULL_SCALE_OFFSET	      :  IfxStdIf_DPipe_print(io, format, id, configurationFileDBHpdSense->igbtTemp.fullScaleOffset); break;
            case PARAMETER_ID_DBHPSENSE_IGBT_TEMP_FULL_SCALE_GAIN	      :  IfxStdIf_DPipe_print(io, format, id, configurationFileDBHpdSense->igbtTemp.fullScaleGain);   break;
            case PARAMETER_ID_DBHPSENSE_ACTIVE_CLAMPING_DACLC		      :  IfxStdIf_DPipe_print(io, format, id, configurationFileDBHpdSense->activeClamping.SCFG_DACLC);break;
            case PARAMETER_ID_DBHPSENSE_TOW_LEVEL_VBE_COMPENSATION	      :  IfxStdIf_DPipe_print(io, format, id, configurationFileDBHpdSense->twoLevel.vbeCompensation); break;
            case PARAMETER_ID_DBHPSENSE_TURN_ON_LEVEL				      :  IfxStdIf_DPipe_print(io, format, id, configurationFileDBHpdSense->twoLevel.turnOnLevel);     break;
            case PARAMETER_ID_DBHPSENSE_TURN_OFF_LEVEL				      :  IfxStdIf_DPipe_print(io, format, id, configurationFileDBHpdSense->twoLevel.turnOffLevel);    break;
            case PARAMETER_ID_DBHPSENSE_TURN_ON_DELAY				      :  IfxStdIf_DPipe_print(io, format, id, configurationFileDBHpdSense->twoLevel.turnOnDelay);     break;
            case PARAMETER_ID_DBHPSENSE_TURN_OFF_DELAY				      :  IfxStdIf_DPipe_print(io, format, id, configurationFileDBHpdSense->twoLevel.turnOffDelay);    break;
            case PARAMETER_ID_DBHPSENSE_BLANKING_TIME_OCPBT			      :  IfxStdIf_DPipe_print(io, format, id, configurationFileDBHpdSense->blankingTime.SOCP_OCPBT);  break;

            /* DB HP2 SIL, DB HPD SIL \ref Db3x_FileConfiguration */
            case PARAMETER_ID_DB3X_DRIVER_SPI_FREQUENCY				      : IfxStdIf_DPipe_print(io, format, id, configurationFileDb3x->driverSpiFrequency);              break;
            case PARAMETER_ID_DB3X_EEPROM_SPI_FREQUENCY				      : IfxStdIf_DPipe_print(io, format, id, configurationFileDb3x->eepromSpiFrequency);              break;
            case PARAMETER_ID_DB3X_INVERTER_FREQUENCY				      : IfxStdIf_DPipe_print(io, format, id, configurationFileDb3x->inverter.frequency);              break;
            case PARAMETER_ID_DB3X_INVERTER_DEADTIME					  : IfxStdIf_DPipe_print(io, format, id, configurationFileDb3x->inverter.deadtime);               break;
            case PARAMETER_ID_DB3X_INVERTER_MIN_PULSE				      : IfxStdIf_DPipe_print(io, format, id, configurationFileDb3x->inverter.minPulse);               break;
            case PARAMETER_ID_DB3X_INVERTER_VDC_NOM					      : IfxStdIf_DPipe_print(io, format, id, configurationFileDb3x->inverter.vdcNom);                 break;
            case PARAMETER_ID_DB3X_INVERTER_VDC_MAX_GEN				      : IfxStdIf_DPipe_print(io, format, id, configurationFileDb3x->inverter.vdcMaxGen);              break;
            case PARAMETER_ID_DB3X_INVERTER_VDC_MIN					      : IfxStdIf_DPipe_print(io, format, id, configurationFileDb3x->inverter.vdcMin);                 break;
            case PARAMETER_ID_DB3X_INVERTER_IGBT_TEMP_MAX		          : IfxStdIf_DPipe_print(io, format, id, configurationFileDb3x->inverter.igbtTempMax);            break;
            case PARAMETER_ID_DB3X_INVERTER_IGBT_TEMP_MIN		          : IfxStdIf_DPipe_print(io, format, id, configurationFileDb3x->inverter.igbtTempMin);            break;
            case PARAMETER_ID_DB3X_ACTIVE_CLAMPING_AT		    	      : IfxStdIf_DPipe_print(io, format, id, configurationFileDb3x->activeClamping.SACLT_AT);         break;
            case PARAMETER_ID_DB3X_ACTIVE_CLAMPING_ACLPM		    	  : IfxStdIf_DPipe_print(io, format, id, configurationFileDb3x->activeClamping.SCFG2_ACLPM);      break;
            case PARAMETER_ID_DB3X_TOW_LEVEL_VBE_COMPENSATION		      : IfxStdIf_DPipe_print(io, format, id, configurationFileDb3x->twoLevel.vbeCompensation);        break;
            case PARAMETER_ID_DB3X_TURN_ON_LEVEL						  : IfxStdIf_DPipe_print(io, format, id, configurationFileDb3x->twoLevel.turnOnLevel);            break;
            case PARAMETER_ID_DB3X_TURN_OFF_LEVEL					      : IfxStdIf_DPipe_print(io, format, id, configurationFileDb3x->twoLevel.turnOffLevel);           break;
            case PARAMETER_ID_DB3X_TURN_ON_DELAY						  : IfxStdIf_DPipe_print(io, format, id, configurationFileDb3x->twoLevel.turnOnDelay);            break;
            case PARAMETER_ID_DB3X_TURN_OFF_DELAY					      : IfxStdIf_DPipe_print(io, format, id, configurationFileDb3x->twoLevel.turnOffDelay);           break;

			default:
	    		break;
	    	}
	}
    else
    {
    	result = FALSE;
    }
    return result;
}

boolean AppShell_sendParamWithId(IfxStdIf_DPipe *io, uint32 id)
{
	const AppShell_Parameter *param = NULL_PTR;

	uint32 i = 0;
	while (g_AppShell_parameterTable[i].id != 0)
	{
		if (g_AppShell_parameterTable[i].id == id)
		{
			param = &g_AppShell_parameterTable[i];
			break;
		}
		i++;
	}


	if (param != NULL_PTR)
	{
		return AppShell_sendParam(io, param);
	}
	return FALSE;
}


boolean AppShell_setParam(pchar args, void *data, IfxStdIf_DPipe *io)
{
//    App    *app    = (App *)data;
    App    *app    = &g_App; /* FIXME make use of data*/
	boolean result = FALSE;

	LB_FileConfiguration *configurationFile = g_Lb.stdIf.configuration;
	Ifx_MotorModelConfigF32_File* configurationFileMotor = &g_Lb.motorConfiguration.data;
	DbHPDSense_FileConfiguration* configurationFileDBHpdSense = &g_Lb.driverBoards.driverBoard_HPDSense.configuration;
	Db3x_FileConfiguration* configurationFileDb3x = &g_Lb.driverBoards.driverBoard_3x.configuration;

	LB30 *board = &g_Lb.logicboards.logicBoard_30;/* FIXME point to the logic board in use, Use StdIf */

    uint32 id;
    /* FIXME add save command to store updated parameters to EEPROM */
    /* FIXME add export command to store updated parameters to PC => export in format setp x y */
    if (!AppShell_isValidCommand(io, AppShell_setup, app->hwState.hwMode))
	{
    	result = TRUE;
	}
    else if (Ifx_Shell_matchToken(&args, "save") != FALSE)
    {
//    	IfxStdIf_DPipe_print(io, "Saving configuration..."ENDL);
    	AppLbStdIf_updateLogicBoardConfigFile(&g_Lb.stdIf);
    	AppDbStdIf_saveConfig(&g_Lb.driverBoardStdif);
        Ifx_MotorModelConfigF32_updateFile(&g_Lb.motorConfiguration);
 //   	IfxStdIf_DPipe_print(io, "Done."ENDL);
    	result = TRUE;
    }
    else if ((Ifx_Shell_parseUInt32(&args, &id, FALSE) != FALSE))
    {
    	const AppShell_Parameter *param = NULL_PTR;

		uint32 i = 0;
		while (g_AppShell_parameterTable[i].id != 0)
		{
			if (g_AppShell_parameterTable[i].id == id)
			{
				param = &g_AppShell_parameterTable[i];
				break;
			}
			i++;
		}

		if (!AppShell_isParamValid(param))
		{
			param = NULL_PTR;
		}

		if (param != NULL_PTR)
		{
			uint32                     uint32Value = -1;
			sint32                     sint32Value = -1;
			float32                    float32Value = -1;
			boolean dataValid = FALSE;

			switch (param->dataType)
			{
			case AppShell_DataType_uint32:
			case AppShell_DataType_enum:
			case AppShell_DataType_boolean:
				if (Ifx_Shell_parseUInt32(&args, &uint32Value, FALSE))
				{
					dataValid = TRUE;

				}
				break;
			case AppShell_DataType_sint32:
				if (Ifx_Shell_parseSInt32(&args, &sint32Value))
				{
					dataValid = TRUE;

				}
				break;
			case AppShell_DataType_float32:
				if (Ifx_Shell_parseFloat32(&args, &float32Value))
				{
					dataValid = TRUE;

				}
				break;
			default:
				break;
			}

			if (dataValid)
			{
		    	switch (id)
		    	{
				case PARAMETER_ID_READY					                        :
					configurationFile->ready = uint32Value == 0 ? FALSE : TRUE;
		    		break;
				case PARAMETER_ID_PRIMARY_CONSOLE		                        :
					if ((uint32Value == LB_PrimaryConsole_asc0) || (uint32Value == LB_PrimaryConsole_can0))
					{
						configurationFile->primaryConsole = uint32Value;
					}
		    		break;
				case PARAMETER_ID_VERBOSE_LEVEL			                        :
					if (uint32Value <= IFX_VERBOSE_LEVEL_DEBUG)
					{
						configurationFile->verboseLevel = (uint8)uint32Value;
					}
		    		break;
		    	case PARAMETER_ID_ENCODER_REVERSED:
					configurationFile->positionSensors.encoder.reversed = uint32Value == 0 ? FALSE : TRUE;
		    		break;
		    	case PARAMETER_ID_ENCODER_OFFSET:
					configurationFile->positionSensors.encoder.offset = sint32Value;
		    		break;
		    	case PARAMETER_ID_ENCODER_RESOLUTION:
					configurationFile->positionSensors.encoder.resolution = sint32Value;
		    		break;
				case PARAMETER_ID_AD2S1210_RESOLUTION	     	                :
					configurationFile->positionSensors.ad2s1210.resolution = sint32Value;
		    		break;
				case PARAMETER_ID_AD2S1210_CARRIER_FREQUENCY		                :
					configurationFile->positionSensors.ad2s1210.carrierFrequency = sint32Value;
		    		break;
				case PARAMETER_ID_AD2S1210_REVERSED				                :
					configurationFile->positionSensors.ad2s1210.reversed = uint32Value == 0 ? FALSE : TRUE;
		    		break;
				case PARAMETER_ID_AD2S1210_OFFSET				                :
					configurationFile->positionSensors.ad2s1210.offset = sint32Value;
		    		break;
				case PARAMETER_ID_AD2S1210_PERIOD_PER_ROTATION	                :
					configurationFile->positionSensors.ad2s1210.periodPerRotation = (uint16)uint32Value;
		    		break;
				case PARAMETER_ID_AD2S1210_GAIN_CODE				                :
				{
			    	uint32 min;
			    	uint32 max;

			    	if (board->boardVersion->boardVersion >= LB_BoardVersion_HybridKit_LogicBoard_3_1)
			    	{
			        	min = LB31_ResolverGain_0_75;
			        	max = LB31_ResolverGain_2_63;
			    	}
			    	else
			    	{
			        	min = LB30_ResolverGain_2_08;
			        	max = LB30_ResolverGain_1_60;
			    	}
					if ((uint32Value >= min) && (uint32Value <= max))
					{
						configurationFile->positionSensors.ad2s1210.gainCode = (uint8)uint32Value;
					}
				}
		    		break;
				case PARAMETER_ID_TLE5012_REVERSED				                :
					configurationFile->positionSensors.tle5012.reversed = uint32Value == 0 ? FALSE : TRUE;
		    		break;
				case PARAMETER_ID_TLE5012_OFFSET					                :
					configurationFile->positionSensors.tle5012.offset = sint32Value;
		    		break;
				case PARAMETER_ID_AURIXRDC_CARRIER_FREQUENCY    	                :
					configurationFile->positionSensors.aurixResolver.carrierFrequency = sint32Value;
		    		break;
				case PARAMETER_ID_AURIXRDC0_RESOLUTION	     	                :
					configurationFile->positionSensors.aurixResolver.input[0].resolution = sint32Value;
		    		break;
				case PARAMETER_ID_AURIXRDC0_REVERSED				                :
					configurationFile->positionSensors.aurixResolver.input[0].reversed = uint32Value == 0 ? FALSE : TRUE;
		    		break;
				case PARAMETER_ID_AURIXRDC0_OFFSET				                :
					configurationFile->positionSensors.aurixResolver.input[0].offset = sint32Value;
		    		break;
				case PARAMETER_ID_AURIXRDC0_PERIOD_PER_ROTATION	                :
					configurationFile->positionSensors.aurixResolver.input[0].periodPerRotation = (uint16)uint32Value;
		    		break;
				case PARAMETER_ID_AURIXRDC0_SIGNAL_AMPLITUDE_MIN	                :
					configurationFile->positionSensors.aurixResolver.input[0].signalAmplitudeMin = sint32Value;
		    		break;
				case PARAMETER_ID_AURIXRDC0_SIGNAL_AMPLITUDE_MAX	                :
					configurationFile->positionSensors.aurixResolver.input[0].signalAmplitudeMax = sint32Value;
		    		break;
				case PARAMETER_ID_AURIXRDC0_GAIN_CODE			                :
				{
			    	uint32 min;
			    	uint32 max;

			    	if (board->boardVersion->boardVersion >= LB_BoardVersion_HybridKit_LogicBoard_3_1)
			    	{
			        	min = LB31_ResolverGain_0_75;
			        	max = LB31_ResolverGain_2_63;
			    	}
			    	else
			    	{
			        	min = LB30_ResolverGain_2_08;
			        	max = LB30_ResolverGain_1_60;
			    	}
					if ((uint32Value >= min) && (uint32Value <= max))
					{
						configurationFile->positionSensors.aurixResolver.input[0].gainCode = (uint8)uint32Value;
					}
				}
		    		break;
				case PARAMETER_ID_AURIXRDC1_RESOLUTION	     	                :
					configurationFile->positionSensors.aurixResolver.input[0].resolution = sint32Value;
		    		break;
				case PARAMETER_ID_AURIXRDC1_REVERSED				                :
					configurationFile->positionSensors.aurixResolver.input[1].reversed = uint32Value == 0 ? FALSE : TRUE;
		    		break;
				case PARAMETER_ID_AURIXRDC1_OFFSET				                :
					configurationFile->positionSensors.aurixResolver.input[0].offset = sint32Value;
		    		break;
				case PARAMETER_ID_AURIXRDC1_PERIOD_PER_ROTATION	                :
					configurationFile->positionSensors.aurixResolver.input[1].periodPerRotation = (uint16)uint32Value;
		    		break;
				case PARAMETER_ID_AURIXRDC1_SIGNAL_AMPLITUDE_MIN	                :
					configurationFile->positionSensors.aurixResolver.input[1].signalAmplitudeMin = sint32Value;
		    		break;
				case PARAMETER_ID_AURIXRDC1_SIGNAL_AMPLITUDE_MAX	                :
					configurationFile->positionSensors.aurixResolver.input[1].signalAmplitudeMax = sint32Value;
		    		break;
				case PARAMETER_ID_AURIXRDC1_GAIN_CODE			                :
				{
			    	uint32 min;
			    	uint32 max;

			    	if (board->boardVersion->boardVersion >= LB_BoardVersion_HybridKit_LogicBoard_3_1)
			    	{
			        	min = LB31_ResolverGain_0_75;
			        	max = LB31_ResolverGain_2_63;
			    	}
			    	else
			    	{
			        	min = LB30_ResolverGain_2_08;
			        	max = LB30_ResolverGain_1_60;
			    	}
					if ((uint32Value >= min) && (uint32Value <= max))
					{
						configurationFile->positionSensors.aurixResolver.input[1].gainCode = (uint8)uint32Value;
					}
				}
		    		break;
				case PARAMETER_ID_INVERTER0_POSITION_SENSOR0	     	            :
					if (uint32Value <= ECU_PositionSensor_none)
					{
		        		if (configurationFile->inverter[0].positionSensor[1] == uint32Value)
		        		{
		        			configurationFile->inverter[0].positionSensor[1] = ECU_PositionSensor_none;
//							IfxStdIf_DPipe_print(io, "Redundant sensor must be different than the primary sensor, redundant sensor set to none"ENDL);
					    	AppShell_sendParamWithId(io, PARAMETER_ID_INVERTER0_POSITION_SENSOR1);
		        		}
		        		else if ((uint32Value == ECU_PositionSensor_tle5012)
		        				&& (configurationFile->inverter[0].positionSensor[1] == ECU_PositionSensor_encoder))
		        		{
		        			configurationFile->inverter[0].positionSensor[1] = ECU_PositionSensor_none;
//							IfxStdIf_DPipe_print(io, "Redundant sensor can't be encoder if primary sensor is iGMR"ENDL);
					    	AppShell_sendParamWithId(io, PARAMETER_ID_INVERTER0_POSITION_SENSOR1);
		        		}
		                configurationFile->inverter[0].positionSensor[0] = uint32Value;

					}
		    		break;
				case PARAMETER_ID_INVERTER0_POSITION_SENSOR1	     	            :
					if (uint32Value <= ECU_PositionSensor_none)
					{
		        		if (configurationFile->inverter[0].positionSensor[0] == uint32Value)
		        		{
//							IfxStdIf_DPipe_print(io, "Redundant sensor must be different than the primary sensor"ENDL);
							dataValid = FALSE;
		        		}
		        		else if ((configurationFile->inverter[0].positionSensor[0] == ECU_PositionSensor_tle5012)
		        				&& (uint32Value == ECU_PositionSensor_encoder))
		        		{
//							IfxStdIf_DPipe_print(io, "Redundant sensor can't be iGMR if primary sensor is encoder"ENDL);
							dataValid = FALSE;
		        		}
		        		else
		        		{
		        			configurationFile->inverter[0].positionSensor[1] = uint32Value;
		        		}
					}
		    		break;
				case PARAMETER_ID_INVERTER0_PWM_MODULE		     	            :
					if (uint32Value <= LB_PwmModule_ccu6)
					{
						configurationFile->inverter[0].pwmModule = uint32Value;
					}
		    		break;
				case PARAMETER_ID_INVERTER0_CURRENT_SENSOR_USE_THREE_SENSORS     :
					configurationFile->inverter[0].currentSenor.useThreeSensors = uint32Value == 3;
		    		break;
				case PARAMETER_ID_INVERTER0_CURRENT_SENSOR_GAIN    			    :
					configurationFile->inverter[0].currentSenor.gain = float32Value * CFG_ADC_GAIN;
		    		break;
				case PARAMETER_ID_INVERTER0_CURRENT_SENSOR_OFFSET    		    :
					configurationFile->inverter[0].currentSenor.offset = -float32Value / CFG_ADC_GAIN;
		    		break;
				case PARAMETER_ID_INVERTER0_CURRENT_SENSOR_MAX    			    :
					configurationFile->inverter[0].currentSenor.max = float32Value;
		    		break;
				case PARAMETER_ID_INVERTER0_CURRENT_SENSOR_CUT_OFF_FREQUENCY     :
					configurationFile->inverter[0].currentSenor.cutOffFrequency = float32Value;
		    		break;
				case PARAMETER_ID_INVERTER1_POSITION_SENSOR0	     	            :
					if (uint32Value <= ECU_PositionSensor_none)
					{
		        		if (configurationFile->inverter[1].positionSensor[1] == uint32Value)
		        		{
		        			configurationFile->inverter[1].positionSensor[1] = ECU_PositionSensor_none;
//							IfxStdIf_DPipe_print(io, "Redundant sensor must be different than the primary sensor, redundant sensor set to none"ENDL);
					    	AppShell_sendParamWithId(io, PARAMETER_ID_INVERTER1_POSITION_SENSOR1);
		        		}
		        		else if ((uint32Value == ECU_PositionSensor_tle5012)
		        				&& (configurationFile->inverter[1].positionSensor[1] == ECU_PositionSensor_encoder))
		        		{
		        			configurationFile->inverter[1].positionSensor[1] = ECU_PositionSensor_none;
//							IfxStdIf_DPipe_print(io, "Redundant sensor can't be encoder if primary sensor is iGMR"ENDL);
					    	AppShell_sendParamWithId(io, PARAMETER_ID_INVERTER1_POSITION_SENSOR1);
		        		}
		                configurationFile->inverter[1].positionSensor[0] = uint32Value;

					}
		    		break;
				case PARAMETER_ID_INVERTER1_POSITION_SENSOR1	     	            :
					if (uint32Value <= ECU_PositionSensor_none)
					{
		        		if (configurationFile->inverter[1].positionSensor[0] == uint32Value)
		        		{
//							IfxStdIf_DPipe_print(io, "Redundant sensor must be different than the primary sensor"ENDL);
							dataValid = FALSE;
		        		}
		        		else if ((configurationFile->inverter[1].positionSensor[0] == ECU_PositionSensor_tle5012)
		        				&& (uint32Value == ECU_PositionSensor_encoder))
		        		{
//							IfxStdIf_DPipe_print(io, "Redundant sensor can't be iGMR if primary sensor is encoder"ENDL);
							dataValid = FALSE;
		        		}
		        		else
		        		{
		        			configurationFile->inverter[1].positionSensor[1] = uint32Value;
		        		}
					}
		    		break;
				case PARAMETER_ID_INVERTER1_PWM_MODULE		     	            :
					if (uint32Value <= LB_PwmModule_ccu6)
					{
						configurationFile->inverter[1].pwmModule = uint32Value;
					}
		    		break;
				case PARAMETER_ID_INVERTER1_CURRENT_SENSOR_USE_THREE_SENSORS     :
					configurationFile->inverter[1].currentSenor.useThreeSensors = uint32Value == 3;
		    		break;
				case PARAMETER_ID_INVERTER1_CURRENT_SENSOR_GAIN    			    :
					configurationFile->inverter[1].currentSenor.gain = float32Value * CFG_ADC_GAIN;
		    		break;
				case PARAMETER_ID_INVERTER1_CURRENT_SENSOR_OFFSET    		    :
					configurationFile->inverter[1].currentSenor.offset = -float32Value / CFG_ADC_GAIN;
		    		break;
				case PARAMETER_ID_INVERTER1_CURRENT_SENSOR_MAX    			    :
					configurationFile->inverter[1].currentSenor.max = float32Value;
		    		break;
				case PARAMETER_ID_INVERTER1_CURRENT_SENSOR_CUT_OFF_FREQUENCY     :
					configurationFile->inverter[1].currentSenor.cutOffFrequency = float32Value;
		    		break;
				case PARAMETER_ID_ANALOG_INPUT0_GAIN    						    :
					configurationFile->analogInput[0].gain = float32Value * CFG_ADC_GAIN;
		    		break;
				case PARAMETER_ID_ANALOG_INPUT0_OFFSET    					    :
					configurationFile->analogInput[0].offset = -float32Value / CFG_ADC_GAIN;
		    		break;
				case PARAMETER_ID_ANALOG_INPUT0_MIN    						    :
					configurationFile->analogInput[0].min = float32Value;
		    		break;
				case PARAMETER_ID_ANALOG_INPUT0_MAX    						    :
					configurationFile->analogInput[0].max = float32Value;
		    		break;
				case PARAMETER_ID_ANALOG_INPUT0_CUT_OFF_FREQUENCY    		    :
					configurationFile->analogInput[0].cutOffFrequency = float32Value;
		    		break;
				case PARAMETER_ID_ANALOG_INPUT1_GAIN    						    :
					configurationFile->analogInput[1].gain = float32Value * CFG_ADC_GAIN;
		    		break;
				case PARAMETER_ID_ANALOG_INPUT1_OFFSET    					    :
					configurationFile->analogInput[1].offset = -float32Value / CFG_ADC_GAIN;
		    		break;
				case PARAMETER_ID_ANALOG_INPUT1_MIN    						    :
					configurationFile->analogInput[1].min = float32Value;
		    		break;
				case PARAMETER_ID_ANALOG_INPUT1_MAX    						    :
					configurationFile->analogInput[1].max = float32Value;
		    		break;
				case PARAMETER_ID_ANALOG_INPUT1_CUT_OFF_FREQUENCY    		    :
					configurationFile->analogInput[1].cutOffFrequency = float32Value;
		    		break;
				case PARAMETER_ID_ANALOG_INPUT2_GAIN    						    :
					configurationFile->analogInput[2].gain = float32Value * CFG_ADC_GAIN;
		    		break;
				case PARAMETER_ID_ANALOG_INPUT2_OFFSET    					    :
					configurationFile->analogInput[2].offset = -float32Value / CFG_ADC_GAIN;
		    		break;
				case PARAMETER_ID_ANALOG_INPUT2_MIN    						    :
					configurationFile->analogInput[2].min = float32Value;
		    		break;
				case PARAMETER_ID_ANALOG_INPUT2_MAX    						    :
					configurationFile->analogInput[2].max = float32Value;
		    		break;
				case PARAMETER_ID_ANALOG_INPUT2_CUT_OFF_FREQUENCY    		    :
					configurationFile->analogInput[2].cutOffFrequency = float32Value;
		    		break;
				case PARAMETER_ID_ANALOG_INPUT3_GAIN    						    :
					configurationFile->analogInput[3].gain = float32Value * CFG_ADC_GAIN;
		    		break;
				case PARAMETER_ID_ANALOG_INPUT3_OFFSET    					    :
					configurationFile->analogInput[3].offset = -float32Value / CFG_ADC_GAIN;
		    		break;
				case PARAMETER_ID_ANALOG_INPUT3_MIN    						    :
					configurationFile->analogInput[3].min = float32Value;
		    		break;
				case PARAMETER_ID_ANALOG_INPUT3_MAX    						    :
					configurationFile->analogInput[3].max = float32Value;
		    		break;
				case PARAMETER_ID_ANALOG_INPUT3_CUT_OFF_FREQUENCY    		    :
					configurationFile->analogInput[3].cutOffFrequency = float32Value;
		    		break;
				case PARAMETER_ID_WINDOW_WATCHDOG_ENABLED    				    :
					configurationFile->safety.windowWatchdog.enabled = uint32Value == 0 ? FALSE : TRUE;
		    		break;
				case PARAMETER_ID_WINDOW_WATCHDOG_OPENED_WINDOW_TIME    		    :
					configurationFile->safety.windowWatchdog.openWindowTime = float32Value;
		    		break;
				case PARAMETER_ID_WINDOW_WATCHDOG_CLOSED_WINDOW_TIME    		    :
					configurationFile->safety.windowWatchdog.closeWindowTime = float32Value;
		    		break;
				case PARAMETER_ID_WINDOW_WATCHDOG_USE_WDI_PIN	    		    :
					configurationFile->safety.windowWatchdog.useWdiPin = uint32Value == 0 ? FALSE : TRUE;
		    		break;
				case PARAMETER_ID_FUNCTIONAL_WATCHDOG_ENABLED    			    :
					configurationFile->safety.functionalWatchdog.enabled = uint32Value == 0 ? FALSE : TRUE;
		    		break;
				case PARAMETER_ID_FUNCTIONAL_WATCHDOG_HEART_BEAT_TIMER_PERIOD    :
					configurationFile->safety.functionalWatchdog.heartbeatTimerPeriod = float32Value;
		    		break;
				case PARAMETER_ID_FUNCTIONAL_WATCHDOG_SERVICE_PERIOD    		    :
					configurationFile->safety.functionalWatchdog.servicePeriod = float32Value;
		    		break;
				case PARAMETER_ID_FSP_ENABLED					    		    :
					configurationFile->safety.fsp.enabled = uint32Value == 0 ? FALSE : TRUE;
		    		break;
				case PARAMETER_ID_IOM_ENABLED					    		    :
					configurationFile->safety.iom.enabled = uint32Value == 0 ? FALSE : TRUE;
		    		break;
				case PARAMETER_ID_IOM_FSP_ON_FAULT_ENABLED					    :
					configurationFile->safety.iom.fspOnFaultEnabled = uint32Value == 0 ? FALSE : TRUE;
		    		break;
				case PARAMETER_ID_IOM_NMI_ON_FAULT_ENABLED					    :
					configurationFile->safety.iom.nmiOnFaultEnabled = uint32Value == 0 ? FALSE : TRUE;
		    		break;
				case PARAMETER_ID_IOM_EVENT_WINDOW_THRESHOLD					    :
					configurationFile->safety.iom.eventWindowThreshold = float32Value*1.e-6;
		    		break;
				case PARAMETER_ID_IOM_FILTER_TIME					    	    :
					configurationFile->safety.iom.filterTime = float32Value*1.e-6;
		    		break;
				case PARAMETER_ID_WIZARD_ENABLED					    		    :
					//configurationFile->wizard.enabled = uint32Value == 0 ? FALSE : TRUE;
					dataValid = FALSE; /*  This value should not be updated by the Shell interface directly */
		    		break;
				case PARAMETER_ID_WIZARD_VDC_NOM					    		    :
					configurationFile->wizard.vdcNom = float32Value;
		    		break;
				case PARAMETER_ID_WIZARD_VF					    			    :
					configurationFile->wizard.vf = float32Value;
		    		break;
				case PARAMETER_ID_SPECIAL_MODE_ENABLED					        :
					configurationFile->specialMode.hwPulseMode.enabled = uint32Value == 0 ? FALSE : TRUE;
		    		break;
				case PARAMETER_ID_SPECIAL_MODE_PWM_PERIOD					    :
					configurationFile->specialMode.hwPulseMode.pwmPeriod = float32Value*1.e-6;
		    		break;
				case PARAMETER_ID_SPECIAL_MODE_PULSE_PERIOD					    :
					configurationFile->specialMode.hwPulseMode.pulsePeriod = float32Value*1.e-6;
		    		break;
				case PARAMETER_ID_SPECIAL_MODE_START_PULSE					    :
					configurationFile->specialMode.hwPulseMode.startPulse = float32Value*1.e-6;
		    		break;
				case PARAMETER_ID_SPECIAL_MODE_PULSE_INCREMENT				    :
					configurationFile->specialMode.hwPulseMode.pulseIncrement = float32Value*1.e-6;
		    		break;
				case PARAMETER_ID_SPECIAL_MODE_PULSE_COUNT_PER_PHASE			    :
					configurationFile->specialMode.hwPulseMode.pulseCountPerPhase = uint32Value;
		    		break;

		    		/* Motor configuration \ref Ifx_MotorModelConfigF32_File */
	            case PARAMETER_ID_MOTOR_TYPE					        	  :  configurationFileMotor->type                                      = uint32Value;         break;
	            case PARAMETER_ID_MOTOR_I_MAX					              :  configurationFileMotor->iMax                                      = float32Value;        break;
	            case PARAMETER_ID_MOTOR_ID_KI					              :  configurationFileMotor->idKi                                      = float32Value;        break;
	            case PARAMETER_ID_MOTOR_ID_KP					              :  configurationFileMotor->idKp                                      = float32Value;        break;
	            case PARAMETER_ID_MOTOR_IQ_KI					              :  configurationFileMotor->iqKi                                      = float32Value;        break;
	            case PARAMETER_ID_MOTOR_IQ_KP					              :  configurationFileMotor->iqKp                                      = float32Value;        break;
	            case PARAMETER_ID_MOTOR_RS					                  :  configurationFileMotor->rs                                        = float32Value;        break;
	            case PARAMETER_ID_MOTOR_LD					                  :  configurationFileMotor->ld                                        = float32Value;        break;
	            case PARAMETER_ID_MOTOR_LQ					                  :  configurationFileMotor->lq                                        = float32Value;        break;
	            case PARAMETER_ID_MOTOR_KT					                  :  configurationFileMotor->kt                                        = float32Value;        break;
	            case PARAMETER_ID_MOTOR_I_STALL					              :  configurationFileMotor->iStall                                    = float32Value;        break;
	            case PARAMETER_ID_MOTOR_POLE_PAIR					          :  configurationFileMotor->polePair                                  = (uint8)uint32Value;  break;
	            case PARAMETER_ID_MOTOR_STATOR_MAX					          :  configurationFileMotor->statorMax                                 = float32Value;        break;
	            case PARAMETER_ID_MOTOR_RR					                  :  configurationFileMotor->rr                                        = float32Value;        break;
	            case PARAMETER_ID_MOTOR_LS					                  :  configurationFileMotor->ls                                        = float32Value;        break;
	            case PARAMETER_ID_MOTOR_LR					                  :  configurationFileMotor->lr                                        = float32Value;        break;
	            case PARAMETER_ID_MOTOR_LM					                  :  configurationFileMotor->lm                                        = float32Value;        break;
	            case PARAMETER_ID_MOTOR_FR					                  :  configurationFileMotor->fr                                        = float32Value;        break;
	            case PARAMETER_ID_MOTOR_SPEED_MAX					          :  configurationFileMotor->speedMax                                  = float32Value;        break;
	            case PARAMETER_ID_MOTOR_SPEED_KP					          :  configurationFileMotor->speedKp                                   = float32Value;        break;
	            case PARAMETER_ID_MOTOR_SPEED_KI					          :  configurationFileMotor->speedKi                                   = float32Value;        break;
	            case PARAMETER_ID_MOTOR_TORQUE_MAX					          :  configurationFileMotor->torqueMax                                 = float32Value;        break;
	            case PARAMETER_ID_MOTOR_TORQUE_RATE					          :  configurationFileMotor->torqueRate                                = float32Value;        break;
	            case PARAMETER_ID_MOTOR_FIELD_WEAKNING					      :  configurationFileMotor->fieldWeakning                             = (uint8)uint32Value;  break;

	            /* DB HPD Sense \ref DbHPDSense_FileConfiguration */
	            case PARAMETER_ID_DBHPSENSE_DRIVER_SPI_FREQUENCY			  :  configurationFileDBHpdSense->driverSpiFrequency                   = float32Value;        break;
	            case PARAMETER_ID_DBHPSENSE_EEPROM_SPI_FREQUENCY			  :  configurationFileDBHpdSense->eepromSpiFrequency                   = float32Value;        break;
	            case PARAMETER_ID_DBHPSENSE_INVERTER_FREQUENCY			      :  configurationFileDBHpdSense->inverter.frequency                   = float32Value;        break;
	            case PARAMETER_ID_DBHPSENSE_INVERTER_DEADTIME			      :  configurationFileDBHpdSense->inverter.deadtime                    = float32Value;        break;
	            case PARAMETER_ID_DBHPSENSE_INVERTER_MIN_PULSE			      :  configurationFileDBHpdSense->inverter.minPulse                    = float32Value;        break;
	            case PARAMETER_ID_DBHPSENSE_INVERTER_VDC_NOM				  :  configurationFileDBHpdSense->inverter.vdcNom                      = float32Value;        break;
	            case PARAMETER_ID_DBHPSENSE_INVERTER_VDC_MAX_GEN			  :  configurationFileDBHpdSense->inverter.vdcMaxGen                   = float32Value;        break;
	            case PARAMETER_ID_DBHPSENSE_INVERTER_VDC_MIN				  :  configurationFileDBHpdSense->inverter.vdcMin                      = float32Value;        break;
	            case PARAMETER_ID_DBHPSENSE_INVERTER_IGBT_TEMP_MAX		      :  configurationFileDBHpdSense->inverter.igbtTempMax                 = float32Value;        break;
	            case PARAMETER_ID_DBHPSENSE_INVERTER_IGBT_TEMP_MIN		      :  configurationFileDBHpdSense->inverter.igbtTempMin                 = float32Value;        break;
	            case PARAMETER_ID_DBHPSENSE_VDC_FULL_SCALE_OFFSET		      :  configurationFileDBHpdSense->vdc.fullScaleOffset                  = uint32Value;         break;
	            case PARAMETER_ID_DBHPSENSE_VDC_FULL_SCALE_GAIN			      :  configurationFileDBHpdSense->vdc.fullScaleGain                    = uint32Value;         break;
	            case PARAMETER_ID_DBHPSENSE_VDC_ZOOM_SCALE_OFFSET		      :  configurationFileDBHpdSense->vdc.zoomOffset                       = uint32Value;         break;
	            case PARAMETER_ID_DBHPSENSE_VDC_ZOOM_SCALE_GAIN			      :  configurationFileDBHpdSense->vdc.zoomGain                         = uint32Value;         break;
	            case PARAMETER_ID_DBHPSENSE_IGBT_TEMP_FULL_SCALE_OFFSET	      :  configurationFileDBHpdSense->igbtTemp.fullScaleOffset             = uint32Value;         break;
	            case PARAMETER_ID_DBHPSENSE_IGBT_TEMP_FULL_SCALE_GAIN	      :  configurationFileDBHpdSense->igbtTemp.fullScaleGain               = uint32Value;         break;
	            case PARAMETER_ID_DBHPSENSE_ACTIVE_CLAMPING_DACLC		      :  configurationFileDBHpdSense->activeClamping.SCFG_DACLC            = (uint8)uint32Value;  break;
	            case PARAMETER_ID_DBHPSENSE_TOW_LEVEL_VBE_COMPENSATION	      :  configurationFileDBHpdSense->twoLevel.vbeCompensation             = uint32Value;         break;
	            case PARAMETER_ID_DBHPSENSE_TURN_ON_LEVEL				      :  configurationFileDBHpdSense->twoLevel.turnOnLevel                 = uint32Value;         break;
	            case PARAMETER_ID_DBHPSENSE_TURN_OFF_LEVEL				      :  configurationFileDBHpdSense->twoLevel.turnOffLevel                = uint32Value;         break;
	            case PARAMETER_ID_DBHPSENSE_TURN_ON_DELAY				      :  configurationFileDBHpdSense->twoLevel.turnOnDelay                 = (uint8)uint32Value;  break;
	            case PARAMETER_ID_DBHPSENSE_TURN_OFF_DELAY				      :  configurationFileDBHpdSense->twoLevel.turnOffDelay                = (uint8)uint32Value;  break;
	            case PARAMETER_ID_DBHPSENSE_BLANKING_TIME_OCPBT   		      :  configurationFileDBHpdSense->blankingTime.SOCP_OCPBT              = (uint8)uint32Value;  break;

	            /* DB HP2 SIL, DB HPD SIL \ref Db3x_FileConfiguration */
	            case PARAMETER_ID_DB3X_DRIVER_SPI_FREQUENCY				      : configurationFileDb3x->driverSpiFrequency                          = float32Value;        break;
	            case PARAMETER_ID_DB3X_EEPROM_SPI_FREQUENCY				      : configurationFileDb3x->eepromSpiFrequency                          = float32Value;        break;
	            case PARAMETER_ID_DB3X_INVERTER_FREQUENCY				      : configurationFileDb3x->inverter.frequency                          = float32Value;        break;
	            case PARAMETER_ID_DB3X_INVERTER_DEADTIME					  : configurationFileDb3x->inverter.deadtime                           = float32Value;        break;
	            case PARAMETER_ID_DB3X_INVERTER_MIN_PULSE				      : configurationFileDb3x->inverter.minPulse                           = float32Value;        break;
	            case PARAMETER_ID_DB3X_INVERTER_VDC_NOM					      : configurationFileDb3x->inverter.vdcNom                             = float32Value;        break;
	            case PARAMETER_ID_DB3X_INVERTER_VDC_MAX_GEN				      : configurationFileDb3x->inverter.vdcMaxGen                          = float32Value;        break;
	            case PARAMETER_ID_DB3X_INVERTER_VDC_MIN					      : configurationFileDb3x->inverter.vdcMin                             = float32Value;        break;
	            case PARAMETER_ID_DB3X_INVERTER_IGBT_TEMP_MAX		          : configurationFileDb3x->inverter.igbtTempMax                        = float32Value;        break;
	            case PARAMETER_ID_DB3X_INVERTER_IGBT_TEMP_MIN		          : configurationFileDb3x->inverter.igbtTempMin                        = float32Value;        break;
	            case PARAMETER_ID_DB3X_ACTIVE_CLAMPING_AT		    	      : configurationFileDb3x->activeClamping.SACLT_AT                     = (uint8)uint32Value;  break;
	            case PARAMETER_ID_DB3X_ACTIVE_CLAMPING_ACLPM		    	  : configurationFileDb3x->activeClamping.SCFG2_ACLPM                  = (uint8)uint32Value;  break;
	            case PARAMETER_ID_DB3X_TOW_LEVEL_VBE_COMPENSATION		      : configurationFileDb3x->twoLevel.vbeCompensation                    = uint32Value;         break;
	            case PARAMETER_ID_DB3X_TURN_ON_LEVEL						  : configurationFileDb3x->twoLevel.turnOnLevel                        = uint32Value;         break;
	            case PARAMETER_ID_DB3X_TURN_OFF_LEVEL					      : configurationFileDb3x->twoLevel.turnOffLevel                       = uint32Value;         break;
	            case PARAMETER_ID_DB3X_TURN_ON_DELAY						  : configurationFileDb3x->twoLevel.turnOnDelay                        = (uint8)uint32Value;  break;
	            case PARAMETER_ID_DB3X_TURN_OFF_DELAY					      : configurationFileDb3x->twoLevel.turnOffDelay                       = (uint8)uint32Value;  break;

				default:
		    		dataValid = FALSE;
		    		break;
		    	}
		    	result  = dataValid;
		    	AppShell_sendParam(io, param);
			}
		}
    }

	return result;
}

boolean AppShell_getParam(pchar args, void *data, IfxStdIf_DPipe *io)
{
	boolean result = FALSE;
	uint32 id;

    if (Ifx_Shell_matchToken(&args, "all") != FALSE)
    {
		uint32 i = 0;
		while (g_AppShell_parameterTable[i].id != 0)
		{
			if (AppShell_isParamValid(&g_AppShell_parameterTable[i]))
			{
				AppShell_sendParam(io, &g_AppShell_parameterTable[i]);
			}
			i++;
		}
		result = TRUE;
    }
    else if (Ifx_Shell_matchToken(&args, "list") != FALSE)
    {
		uint32 i = 0;
		while (g_AppShell_parameterTable[i].id != 0)
		{
	    	const AppShell_Parameter *param = NULL_PTR;
			param = &g_AppShell_parameterTable[i];

			pchar dataType;
			pchar dataSize;
			char unit[50];
			switch(param->dataType)
			{
			case AppShell_DataType_float32:
				dataType = "float";
				dataSize = "";
				break;
			case AppShell_DataType_sint32:
				dataType = "sint32";
				dataSize = "";
				break;
			case AppShell_DataType_uint32:
			case AppShell_DataType_enum:
			case AppShell_DataType_boolean:
				dataType = "uint32";
				dataSize = "";
				break;
			default:
				dataType = "";
				dataSize = "";
				break;
			}
			if (param->unit[0])
			{
			    snprintf(unit, 50, " unit=\"%s\"", param->unit);
			}
			else
			{
				unit[0] = 0;
			}


			if ((param->dataType == AppShell_DataType_enum) || (param->dataType == AppShell_DataType_boolean))
			{
				IfxStdIf_DPipe_print(io, "<var name=\"%d\" type=\"%s\"%s%s access=\"read-write\" default=\"0\" enum=\"true\" title=\"%s\">"ONEEYE_ENDL"</var>"ONEEYE_ENDL,
		        		param->id,
		        		dataType,
		        		unit,
		        		dataSize,
		        		param->title
		        		);

			}
			else
			{
				IfxStdIf_DPipe_print(io, "<var name=\"%d\" type=\"%s\"%s%s access=\"read-write\" default=\"0\" title=\"%s\"/>"ONEEYE_ENDL,
		        		param->id,
		        		dataType,
		        		unit,
		        		dataSize,
		        		param->title
		        		);

			}



			i++;
		}
		result = TRUE;
    }
    else if ((Ifx_Shell_parseUInt32(&args, &id, FALSE) != FALSE))
    {
    	const AppShell_Parameter *param = NULL_PTR;

		uint32 i = 0;
		while (g_AppShell_parameterTable[i].id != 0)
		{
			if (g_AppShell_parameterTable[i].id == id)
			{
				param = &g_AppShell_parameterTable[i];
				break;
			}
			i++;
		}

		if (!AppShell_isParamValid(param))
		{
			param = NULL_PTR;
		}

		if (param != NULL_PTR)
		{
			AppShell_sendParam(io, param);

		}
    }

	return result;
}

//----------------------------------------------------------------------------------------
/** \brief Initialise the application shell */
void AppShell_init(Ifx_Shell *shell, IfxStdIf_DPipe *standardIo)
{
    Ifx_Shell_Config config;
    uint32           i;
    Ifx_Shell_initConfig(&config);
    config.standardIo           = standardIo;

    /** Command lists used: */
    i                       = 0;
    config.commandList[i++] = &g_AppShell_commands[0];         /** - \ref g_AppShell_commands */
    config.commandList[i++] = &Ifx_g_MotorModelConfigF32[0]; /** - \ref Ifx_MotorModelConfigF32 commands */
    config.commandList[i++] = &Ifx_g_EfsShell_commands[0];     /** - \ref Ifx_EfsShell commands */
    config.commandList[i++] = &Ifx_g_OsciShell_commands[0];    /** - \ref Ifx_OsciShell commands */
    IFX_ASSERT(IFX_VERBOSE_LEVEL_ERROR, IFX_CFG_SHELL_COMMAND_LISTS >= i);

    Ifx_Shell_init(shell, &config);
}


//----------------------------------------------------------------------------------------
void AppShell_SetDefaultConsole(Ifx_Shell *shell)
{
    Ifx_Assert_setStandardIo(shell->io);	// Assertion output
    Ifx_Console_setIo(shell->io);			// Console output
}

void AppShell_process(Ifx_Shell *shell)
{
    if (Ifx_Shell_process(shell) == IFX_SHELL_KEY_OK)
    {
    }
}
