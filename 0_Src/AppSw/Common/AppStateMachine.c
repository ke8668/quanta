/**
 * \file AppStateMachine.c
 * \brief
 *
 * \copyright Copyright (c) 2015 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * This file may be used, copied, and distributed, with or without modification, provided
 * that all copyright notices are retained; that all modifications to this file are
 * prominently noted in the modified file; and that this paragraph is not modified.
 *
 *
 *
 *
 */

#include "AppStateMachine.h"

#include "Main.h"
#include "AppShell.h"

#include "AppCan.h"
#include "SysSe/EMotor/Ifx_MotorModelConfigF32.h"
#include "LB.h"
#include "Library/AppSpecialMode.h"
#include "SysSe/Bsp/Bsp.h"
#include "LogicBoard3_0/App/LB30.h"

boolean AppStateMachine_initHardware(void);

void AppStateMachine_enterApplicationInitialization(AppStateMachine *stateMachine);
void AppStateMachine_enterBoot(AppStateMachine *stateMachine);
void AppStateMachine_enterConfiguration(AppStateMachine *stateMachine);
void AppStateMachine_enterError(AppStateMachine *stateMachine);
void AppStateMachine_enterHwInitialization(AppStateMachine *stateMachine);
void AppStateMachine_enterLimitedConfiguration(AppStateMachine *stateMachine);
void AppStateMachine_enterRun(AppStateMachine *stateMachine);
void AppStateMachine_enterShutdown(AppStateMachine *stateMachine);

void AppStateMachine_processApplicationInitialization(AppStateMachine *stateMachine);
void AppStateMachine_processBoot(AppStateMachine *stateMachine);
void AppStateMachine_processConfiguration(AppStateMachine *stateMachine);
void AppStateMachine_processError(AppStateMachine *stateMachine);
void AppStateMachine_processHwInitialization(AppStateMachine *stateMachine);
void AppStateMachine_processLimitedConfiguration(AppStateMachine *stateMachine);
void AppStateMachine_processRun(AppStateMachine *stateMachine);
void AppStateMachine_processShutdown(AppStateMachine *stateMachine);
#if 1
void LED_Indicator_0(boolean status);
void LED_Indicator_1(boolean status);
void LED_Indicator_2(boolean status);
void LED_Indicator_0_Flash(void);
void LED_Indicator_1_Flash(void);
void LED_Indicator_2_Flash(void);
#endif
void LED_Indicator_Process(AppStateMachine *stateMachine);

boolean AppStateMachine_postInit(void);
boolean AppStateMachine_selftTest(void);

extern uint32 TIME_CNT_10ms;

const pchar AppStateMachine_ModeNames[9] =
{
    "OFF",
    "Boot",
    "Configuration",
    "Hardware initialization",
    "Application initialization",
    "Run",
    "Limited configuration",
    "Error",
    "Shutdown"
};

void AppStateMachine_process(AppStateMachine *stateMachine)
{
	LED_Indicator_Process(stateMachine);
    if (stateMachine->hwModeRequest != stateMachine->hwMode)
    {
        AppMode hwMode = stateMachine->hwMode;

        /* State transition table */
        switch (stateMachine->hwModeRequest)
        {
            case AppMode_run:
                AppStateMachine_enterRun(stateMachine);
                break;
            case AppMode_appInitialization:
                AppStateMachine_enterApplicationInitialization(stateMachine);
                break;
            case AppMode_hwInitialization:
                AppStateMachine_enterHwInitialization(stateMachine);
                break;
            case AppMode_shutdown:
                AppStateMachine_enterShutdown(stateMachine);
                break;
            case AppMode_configuration:
                AppStateMachine_enterConfiguration(stateMachine);
                break;
            case AppMode_limitedConfiguration:
                AppStateMachine_enterLimitedConfiguration(stateMachine);
                break;
            case AppMode_boot:
                AppStateMachine_enterBoot(stateMachine);
                break;
            case AppMode_error:
                AppStateMachine_enterError(stateMachine);
                break;
            case AppMode_off:
                break;
        }

        if (stateMachine->hwMode == hwMode)
        {
            /* If request could not be handled, then canceled the request */
//            Ifx_Console_print(ENDL "Main State machine error: invalid state transition '%s'->'%s'"ENDL, (AppStateMachine_ModeNames[hwMode]), (AppStateMachine_ModeNames[stateMachine->hwModeRequest]));
            stateMachine->hwModeRequest = hwMode;
        }
        else
        {
            if (Ifx_g_console.standardIo != NULL_PTR)
            {
//                Ifx_Console_print(ENDL "Main State machine: '%s' state entered"ENDL, AppStateMachine_ModeNames[stateMachine->hwMode]);
            }
        }
    }
    else
    {
        /* State action table */
        switch (stateMachine->hwModeRequest)
        {
            case AppMode_appInitialization:
                AppStateMachine_processApplicationInitialization(stateMachine);
                break;
            case AppMode_boot:
                AppStateMachine_processBoot(stateMachine);
                break;
            case AppMode_run:
                AppStateMachine_processRun(stateMachine);
                break;
            case AppMode_hwInitialization:
                AppStateMachine_processHwInitialization(stateMachine);
                break;
            case AppMode_shutdown:
                AppStateMachine_processShutdown(stateMachine);
                break;
            case AppMode_configuration:
                AppStateMachine_processConfiguration(stateMachine);
                break;
            case AppMode_limitedConfiguration:
                AppStateMachine_processLimitedConfiguration(stateMachine);
                break;
            case AppMode_error:
                AppStateMachine_processError(stateMachine);
                break;
            case AppMode_off:
                break;
        }
    }
}


void AppStateMachine_enterBoot(AppStateMachine *stateMachine)
{
    if (stateMachine->hwMode == AppMode_off)
    {
        stateMachine->hwMode = stateMachine->hwModeRequest;
    }
}


void AppStateMachine_enterApplicationInitialization(AppStateMachine *stateMachine)
{
    if ((stateMachine->hwMode == AppMode_hwInitialization) || (stateMachine->hwMode == AppMode_limitedConfiguration) || (stateMachine->hwMode == AppMode_error))
    {
        stateMachine->hwMode = stateMachine->hwModeRequest;
    }
}


void AppStateMachine_enterConfiguration(AppStateMachine *stateMachine)
{
    if (stateMachine->hwMode == AppMode_boot)
    {
        stateMachine->hwMode = stateMachine->hwModeRequest;

        if (AppLbStdIf_isMinimalSetup(&g_Lb.stdIf, Ifx_g_console.standardIo))
        {
            Ifx_Efs *efs = NULL_PTR;

            protectEeprom(&g_Lb.driver.eeprom);
            protectEeprom(AppDbStdIf_getBoardVersionEfs(&g_Lb.driverBoardStdif)->eeprom);

            efs = &g_Lb.driver.efsConfig;

            if (g_Lb.stdIf.configuration->wizard.enabled)
            {
                g_Lb.stdIf.configuration->wizard.enabled = FALSE; /* Disable wizard mode after boot */
                //Ifx_Console_print("Wizard flag removed"ENDL);
                AppLbStdIf_updateLogicBoardConfigFile(&g_Lb.stdIf);
            }


            {
                Ifx_MotorModelConfigF32_Config config;
                Ifx_MotorModelConfigF32_initConfig(&config, efs, EFS_CFG_FILE_NAME_MOTOR_CONFIGURATION);
                Ifx_MotorModelConfigF32_init(&g_Lb.motorConfiguration, &config);
            }

            if (!Ifx_MotorModelConfigF32_loadFile(&g_Lb.motorConfiguration))
            {
                /* Create the default configuration file if it dose not exist*/
//                Ifx_Console_print("Motor configuration file does not exist"ENDL);
//                Ifx_Console_print("Creating default file..."ENDL);

                Ifx_MotorModelConfigF32_setupDefaultValue(&g_Lb.motorConfiguration);

                Ifx_MotorModelConfigF32_deleteFile(&g_Lb.motorConfiguration);
                if (Ifx_MotorModelConfigF32_createFile(&g_Lb.motorConfiguration))
                {
//                    Ifx_Console_print("Success"ENDL);
                }
                else
                {
//                    Ifx_Console_print("Error"ENDL);
                }
            }
#if BYPASSDRVBD
            stateMachine->specialMode = AppModeSpecial_normal;
#else if
            stateMachine->specialMode = getSpecialMode();
#endif
        }
    }
}


void AppStateMachine_enterError(AppStateMachine *stateMachine)
{
    if ((stateMachine->hwMode == AppMode_hwInitialization)
            || (stateMachine->hwMode == AppMode_appInitialization)
            || (stateMachine->hwMode == AppMode_run))
    {
        stateMachine->hwMode = stateMachine->hwModeRequest;
    }
}


void AppStateMachine_enterHwInitialization(AppStateMachine *stateMachine)
{
    if (stateMachine->hwMode == AppMode_configuration)
    {
        if (AppLbStdIf_isMinimalSetup(&g_Lb.stdIf, Ifx_g_console.standardIo) && AppLbStdIf_isConfigValid(&g_Lb.stdIf))
        {
            stateMachine->hwMode = stateMachine->hwModeRequest;
        }
        else
        {
//            Ifx_Console_print("ERROR: Invalid configuration."ENDL);
        }
    }
}


void AppStateMachine_enterLimitedConfiguration(AppStateMachine *stateMachine)
{
    if ((stateMachine->hwMode == AppMode_run) || (stateMachine->hwMode == AppMode_error))
    {
        stateMachine->hwMode = stateMachine->hwModeRequest;
    }
}


void AppStateMachine_enterRun(AppStateMachine *stateMachine)
{
    if (stateMachine->hwMode == AppMode_appInitialization)
    {
        stateMachine->hwMode = stateMachine->hwModeRequest;
    }
}


void AppStateMachine_enterShutdown(AppStateMachine *stateMachine)
{
    if ((stateMachine->hwMode == AppMode_configuration)
            || (stateMachine->hwMode == AppMode_error)
            || (stateMachine->hwMode == AppMode_run)
            || (stateMachine->hwMode == AppMode_limitedConfiguration))
    {
        stateMachine->hwMode = stateMachine->hwModeRequest;
        ECU_slotShutdown();
    }
}


void AppStateMachine_processApplicationInitialization(AppStateMachine *stateMachine)
{
    boolean status;
    if(g_App.hwState.specialMode == AppModeSpecial_normal)
    {
        status = ECU_slotInit();	// 20191224 Sync with eeprom pararmeters //
    }
    else
    {
        status = HandleSpecialModeInit();
    }

    if (status)
    {
        g_App.initialized           = TRUE;
        stateMachine->hwModeRequest = AppMode_run;
    }
    else
    {
//        Ifx_Console_print("ERROR: Failure during ECU initialization"ENDL);
        stateMachine->hwModeRequest = AppMode_error;
    }
}


boolean AppStateMachine_initConsole(void)
{
    boolean logicBoardOk         = TRUE;
    boolean primaryConsoleIsAsc0 = TRUE;

    /* Initialize the configuration interface */
    primaryConsoleIsAsc0 = g_Lb.stdIf.configuration->primaryConsole == LB_PrimaryConsole_asc0;
    logicBoardOk         = AppLbStdIf_appInitConsole(&g_Lb.stdIf, primaryConsoleIsAsc0);
    AppCanConsole_init(&g_App.can.canConsole, !primaryConsoleIsAsc0);

    // look around for TC397
    logicBoardOk = 1;
    primaryConsoleIsAsc0 = 1;
    if (logicBoardOk)
    {
        /* Console initialisation */

        AppCan_init();  /* FIXME do only initialization of the console here */

        /* Enable 1ms timer */
        logicBoardOk         = AppLbStdIf_appInit1MsTimer(&g_Lb.stdIf);

        if (primaryConsoleIsAsc0)
        {
            /* Console initialisation */
            Ifx_Console_init(g_Lb.stdIf.asc0);
        }
        else
        {
            Ifx_Console_init(&g_App.can.canConsole.stdIf);
        }

        /* Assert initialisation */
//        Ifx_Assert_setStandardIo(Ifx_g_console.standardIo);

//        Ifx_Console_print(ENDL);

        /** - Initialise the shell command interface  */
        // look around by Jimmy
//        AppShell_init(&g_App.primaryShell, (!primaryConsoleIsAsc0 ? &g_App.can.canConsole.stdIf : g_Lb.stdIf.asc0));
//        AppShell_init(&g_App.secondaryShell, (!primaryConsoleIsAsc0 ? g_Lb.stdIf.asc0 : &g_App.can.canConsole.stdIf));
        /* Initialize the EFS shell */
        Ifx_EfsShell_Config config;
        Ifx_EfsShell_initConfig(&config, &g_Lb.driver.efsConfig);
        logicBoardOk &= Ifx_EfsShell_init(&g_Lb.driver.efsShell, &config);
    }

    return logicBoardOk;
}


void AppStateMachine_processBoot(AppStateMachine *stateMachine)
{
    boolean           logicBoardOk   = TRUE;
    LB_PrimaryConsole primaryConsole = LB_PrimaryConsole_asc0;

    /** - Enable program cache */
    IfxCpu_setProgramCache(TRUE);

    memset(&g_Lb.stdIf, 0, sizeof(g_Lb.stdIf));
    g_App.boardVersionLock = TRUE;
    g_App.hwState.hwEmergency = FALSE;
    g_App.hwState.vdcMeasurementEnabled = TRUE;

    /** - Internal clocks: CPU, PCP, SPB */
    /** \brief Initialize the microcontroller's clock system */
    g_App.info.pllFreq = IfxScuCcu_getPllFrequency();
    g_App.info.cpuFreq = IfxScuCcu_getCpuFrequency(IfxCpu_getCoreIndex());
    IfxScuCcu_setSpbFrequency(g_App.info.cpuFreq / 2);
    g_App.info.sysFreq = IfxScuCcu_getSpbFrequency();
    g_App.info.stmFreq = IfxStm_getFrequency(&MODULE_STM0);

    /** - Time constants */
    initTime();

    /** - GTM clocks */
    Ifx_GTM *gtm = &MODULE_GTM;
    IfxGtm_enable(gtm);
    g_App.info.gtmFreq = IfxGtm_Cmu_getModuleFrequency(gtm);

    /* Set the global clock frequencies */
    IfxGtm_Cmu_setGclkFrequency(gtm, g_App.info.gtmFreq);
    g_App.info.gtmGclkFreq = IfxGtm_Cmu_getGclkFrequency(gtm);

    IfxGtm_Cmu_setClkFrequency(gtm, IfxGtm_Cmu_Clk_0, 100e6); /* PWM timer */
    IfxGtm_Cmu_setClkFrequency(gtm, IfxGtm_Cmu_Clk_1, 50e6);  /* 1ms and 10ms timers */
    IfxGtm_Cmu_setClkFrequency(gtm, IfxGtm_Cmu_Clk_3, 10e3);  /* IGBT temp timeouts  */
    g_App.info.gtmCmuClk1Freq = IfxGtm_Cmu_getClkFrequency(gtm, IfxGtm_Cmu_Clk_1, TRUE);

    /* FIXME the delay has been reduced from 1s to 400ms to enable TLF35584. Check value on HP2 driver board */
    wait(TimeConst_100ms*4); /* Wait for all power supply to be on (HP2 Driver board delay). Care for the TLF35584 timeout of 550ms  */

    /* Enable the interrupts, required for the EEPROM access and shell */
    enableInterrupts();

    /* Get the logic board version from the EEPROM, or auto-detect  */
    LB_BoardVersion lbVersion = Lb_discoverLogicBoardVersion(&g_Lb);

    /* Mount the file system  for logic board configuration */
    Ifx_Efs_Init(&g_Lb.driver.efsConfig, &g_Lb.driver.eeprom);
    Ifx_Efs_tableOffsetSet(&g_Lb.driver.efsConfig, CFG_LB_CONFIG_EFS_OFFSET);
    logicBoardOk &= Ifx_Efs_mount(&g_Lb.driver.efsConfig);
#if BYPASSEEPROM
    logicBoardOk = 1;
#endif
    /* Initialize logic board dependent variables, initialize the driver board eeprom  */
    if (App_getBoardVersion(lbVersion) == 3)
    {
        g_Lb.logicboards.logicBoard_30.boardVersion = &g_Lb.boardVersion;
        g_Lb.logicboards.logicBoard_30.driver.qspi0 = &g_Lb.driver.qspi0;
        g_Lb.logicboards.logicBoard_30.driver.efsConfig = &g_Lb.driver.efsConfig;

        LB30_init(&g_Lb.logicboards.logicBoard_30);
        g_Lb.logicboards.logicBoard_30.motorConfiguration = &g_Lb.motorConfiguration.data;
        LB30_LbStdIfInit(&g_Lb.stdIf, &g_Lb.logicboards.logicBoard_30);
        /* Use 3x board by default to get access to the eeprom. This might get reconfigured afterwards depending on the board version */
        Db3x_DbStdIfInit(&g_Lb.driverBoardStdif, &g_Lb.driverBoards.driverBoard_3x);
        g_Lb.logicboards.logicBoard_30.driverBoardStdif = &g_Lb.driverBoardStdif;
    }
    else
    {
        /* FIXME handle the case where no board is detected */
        while (1)
        {}
    }

    /* FIXME make use of logicBoardOk */

    logicBoardOk &= AppStateMachine_initConsole();

    // "LB30_AppPreInit" //
    AppLbStdIf_appPreInit(&g_Lb.stdIf); /* FIXME avoid start of the application if pre init show error, enable up to configuration mode */

    /** - Show application info */
//    AppShell_info("", &g_App, Ifx_g_console.standardIo);

    /* Initialize the driver board depending on the board version */
    LB_BoardVersion driverBoardVersion= AppDbStdIf_getBoardVersion(&g_Lb.driverBoardStdif)->boardVersion;
    Lb_BoardType driverBoardType= AppDbStdIf_getBoardVersion(&g_Lb.driverBoardStdif)->boardType;
    // Lookaround with Driver Board //
#if BYPASSDRVBD
    driverBoardVersion = LB_BoardVersion_HybridKit_DriverBoardHpDriveSense_1_2;
    driverBoardType= 5;
#endif
    if (
        ((driverBoardType == Lb_BoardType_HybridKit_DriverBoardHp2)
         &&
         (
             (driverBoardVersion == LB_BoardVersion_HybridKit_DriverBoardHp2_3_1)
             || (driverBoardVersion == LB_BoardVersion_HybridKit_DriverBoardHp2_3_2)
         )
        )
        || ((driverBoardType == Lb_BoardType_HybridKit_DriverBoardHpDrive)
            &&
            (
                (driverBoardVersion == LB_BoardVersion_HybridKit_DriverBoardHpDrive_1_3)
            )
           )
    )
    {
        /* Nothing to do */

    }
    else if (
        ((driverBoardType == Lb_BoardType_HybridKit_DriverBoardHpDriveEiceSense)
         &&
         (
             (driverBoardVersion == LB_BoardVersion_HybridKit_DriverBoardHpDriveSense_1_0)
             || (driverBoardVersion == LB_BoardVersion_HybridKit_DriverBoardHpDriveSense_1_1)
             || (driverBoardVersion == LB_BoardVersion_HybridKit_DriverBoardHpDriveSense_1_2)
         )
        )
        || ((driverBoardType == Lb_BoardType_HybridKit_DriverBoardHpDriveEiceSenseDsc)
            &&
            (
                (driverBoardVersion == LB_BoardVersion_HybridKit_DriverBoardHpDriveSenseDsc_1_0)
                ||  (driverBoardVersion == LB_BoardVersion_HybridKit_DriverBoardHpDriveSenseDsc_1_1)
            )
           )
    )
    {
        /* Re-initialize the driver board object
         * This will also re-initialize the EEPROM QSPI channel */
//        Ifx_Console_printAlign("HP Drive Sense board selected"ENDL);

        DbHPDSense_DbStdIfInit(&g_Lb.driverBoardStdif, &g_Lb.driverBoards.driverBoard_HPDSense);
        logicBoardOk &= AppDbStdIf_init(&g_Lb.driverBoardStdif, &g_Lb.driver.qspi0, NULL_PTR);

    }
    else
    {
        /* Unknown board */
    }

    boolean isBoardConfiguration;
    isBoardConfiguration = AppDbStdIf_loadConfigFile(&g_Lb.driverBoardStdif);
    logicBoardOk &= isBoardConfiguration;

#if CFG_AUTO_CONGIF_SETUP
    if (!isBoardConfiguration)
    {
        AppDbStdIf_createConfigFile(&g_Lb.driverBoardStdif, FALSE);
    }
#endif


    //LB_printBoardConfiguration(Ifx_g_console.standardIo, g_Lb.stdIf.configuration, &g_Lb.boardVersion, FALSE);
    primaryConsole = g_Lb.stdIf.configuration->primaryConsole;

    // AppDbStdIf_printBoardConfiguration(&g_Lb.driverBoardStdif, Ifx_g_console.standardIo);

//    LB_printShellInfo(&g_App.can.canConsole.stdIf, primaryConsole, LB_PrimaryConsole_can0);
//    LB_printShellInfo(g_Lb.stdIf.asc0, primaryConsole, LB_PrimaryConsole_asc0);

    /* Enable clock for 1ms interrupt */
    IfxGtm_Cmu_enableClocks(gtm, IFXGTM_CMU_CLKEN_CLK1);

    Lb_initBenchmark(&g_Lb);
    g_App.can.outbox.ecuReset = TRUE;
    stateMachine->hwModeRequest = AppMode_configuration;
}


void AppStateMachine_processConfiguration(AppStateMachine *stateMachine)
{
#if BYPASSSHELL
	g_Lb.stdIf.configuration->inverter[0].pwmModule = LB_PwmModule_gtmAtom;
	g_Lb.stdIf.configuration->inverter[0].positionSensor[0] = ECU_PositionSensor_ad2s1210;
#else if
    AppShell_process(&g_App.primaryShell);
    AppShell_process(&g_App.secondaryShell);
#endif
    if (g_Lb.stdIf.configuration->ready || (stateMachine->specialMode != AppModeSpecial_normal))
    {
        stateMachine->hwModeRequest = AppMode_hwInitialization;
    }
    // look around for bypass OneEye
#if BYPASSONEEYE
    g_App.can.inbox.configExit = 1;		// auto configExig
#endif
}


void AppStateMachine_processError(AppStateMachine *stateMachine)
{
#if BYPASSSHELL
#else if
    AppShell_process(&g_App.primaryShell);
    AppShell_process(&g_App.secondaryShell);
#endif
    if (g_App.initialized)
    {
        IfxStdIf_Inverter_showMessage(g_Lb.stdIf.inverter);
    }

}


void AppStateMachine_processHwInitialization(AppStateMachine *stateMachine)
{
    boolean status;

    disableInterrupts();
    status = AppStateMachine_initHardware();
    enableInterrupts();

    if (status)
    {
        status &= AppStateMachine_postInit();		// will detect "Ad2s1210" or "Tle5012" look around //
    }

    if (status)
    {
        g_App.hwState.hwInitReady = TRUE;
        wait(TimeConst_100ms); /* Wait for input signals to settle */

        AppLbStdIf_currentOffsetsCalibrationStep(&g_Lb.stdIf);
		
#if  BYPASSDRVBD
#else if
        ECU_setInverterEnabled(TRUE);		// Initial 1edi20xx gate driver //
#endif

        status &= AppStateMachine_selftTest();		// will detect "tlf35584" look around //
        
#if  ANALOGSIGNALFLOATING
        status = TRUE;
#else if

#endif

    }

    if (status)
    {
        stateMachine->hwModeRequest = AppMode_appInitialization;
    }
    else
    {
//        Ifx_Console_print("ERROR: Failure during hardware initialization"ENDL);
        stateMachine->hwModeRequest = AppMode_error;
    }
}


void AppStateMachine_processLimitedConfiguration(AppStateMachine *stateMachine)
{
#if BYPASSSHELL
#else if
    AppShell_process(&g_App.primaryShell);
    AppShell_process(&g_App.secondaryShell);
#endif
    if (g_App.initialized)
    {
        IfxStdIf_Inverter_showMessage(g_Lb.stdIf.inverter);
    }
}


void AppStateMachine_processRun(AppStateMachine *stateMachine)
{
#if BYPASSSHELL
#else if
    AppShell_process(&g_App.primaryShell);
    AppShell_process(&g_App.secondaryShell);
#endif
//    IfxStdIf_Inverter_showMessage(g_Lb.stdIf.inverter);
    if(g_App.hwState.specialMode == AppModeSpecial_normal)
    {
        ECU_slotBackground();
    }
    else
    {
        HandleSpecialModeBackground();
    }

    // look around for bypass OneEye
#if AUTORUN
    g_App.can.inbox.modeCmd = 2;	// = Ifx_MotorControl_Mode_torqueControl;		// Torque mode
	//g_App.can.inbox.modeCmd = 4;	//Ifx_MotorControl_Mode_openLoop;				// Open loop mode
    g_App.can.inbox.runCmd = 1;		// auto-run
#endif
}


void AppStateMachine_processShutdown(AppStateMachine *stateMachine)
{}

void AppStateMachine_initOsci(float32 samplePeriod)
{
    {
        Ifx_Osci_Config osciConfig;
        Ifx_Osci_initConfig(&osciConfig);
        osciConfig.samplePeriod = samplePeriod;
        Ifx_Osci_init(&g_App.osci, &osciConfig);

//        Ifx_Osci_setSignalsConfig(&g_App.osci, appTest_OsciSignalConfig);
//        Ifx_Osci_setChannelSignal(&g_App.osci, 0, App_OsciSignal_current0U);
    }

    {
        Ifx_OsciShell_Config config;
        Ifx_OsciShell_initConfig(&config, &g_App.osci);
        Ifx_OsciShell_init(&g_App.osciShell, &config);
    }

//    Ifx_Osci_run(&g_App.osci);
}


boolean AppStateMachine_initHardware(void)
{
    boolean  result = TRUE;
    Ifx_GTM *gtm    = &MODULE_GTM;

    result &= AppLbStdIf_appInit(&g_Lb.stdIf);		// "LB30_AppInit" //

    /* Connect to logic board */
    /* FIXME merge with ECU_selectPositionSensor() */
    switch (g_Lb.stdIf.configuration->inverter[0].positionSensor[0])
    {
        case ECU_PositionSensor_ad2s1210:
            g_Lb.stdIf.positionSensorX[0] = g_Lb.stdIf.ad2s1210->driver ?  g_Lb.stdIf.ad2s1210 : NULL_PTR;
            break;
        case ECU_PositionSensor_encoder:
            g_Lb.stdIf.positionSensorX[0] = g_Lb.stdIf.encoder->driver ?  g_Lb.stdIf.encoder : NULL_PTR;
            break;
        case ECU_PositionSensor_internalResolver0:
            g_Lb.stdIf.positionSensorX[0] = g_Lb.stdIf.dsadcRdc[0]->driver ?  g_Lb.stdIf.dsadcRdc[0] : NULL_PTR;
            break;
        case ECU_PositionSensor_internalResolver1:
            g_Lb.stdIf.positionSensorX[0] = g_Lb.stdIf.dsadcRdc[1]->driver ?  g_Lb.stdIf.dsadcRdc[1] : NULL_PTR;
            break;
        case ECU_PositionSensor_tle5012:
            g_Lb.stdIf.positionSensorX[0] = g_Lb.stdIf.encoder->driver ?  g_Lb.stdIf.encoder : NULL_PTR; /* iGMR encoder signal is used at run-time */
            break;
        default:
            g_Lb.stdIf.positionSensorX[0] = NULL_PTR;
            break;
    }
    switch (g_Lb.stdIf.configuration->inverter[0].positionSensor[1])
    {
        case ECU_PositionSensor_ad2s1210:
            g_Lb.stdIf.positionSensorX[1] = g_Lb.stdIf.ad2s1210->driver ?  g_Lb.stdIf.ad2s1210 : NULL_PTR;
            break;
        case ECU_PositionSensor_encoder:
            g_Lb.stdIf.positionSensorX[1] = g_Lb.stdIf.encoder->driver ?  g_Lb.stdIf.encoder : NULL_PTR;
            break;
        case ECU_PositionSensor_internalResolver0:
            g_Lb.stdIf.positionSensorX[1] = g_Lb.stdIf.dsadcRdc[0]->driver ?  g_Lb.stdIf.dsadcRdc[0] : NULL_PTR;
            break;
        case ECU_PositionSensor_internalResolver1:
            g_Lb.stdIf.positionSensorX[1] = g_Lb.stdIf.dsadcRdc[1]->driver ?  g_Lb.stdIf.dsadcRdc[1] : NULL_PTR;
            break;
        case ECU_PositionSensor_tle5012:
            g_Lb.stdIf.positionSensorX[1] = g_Lb.stdIf.encoder->driver ?  g_Lb.stdIf.encoder : NULL_PTR; /* iGMR encoder signal is used at run-time */
            break;
        default:
            g_Lb.stdIf.positionSensorX[1] = NULL_PTR;
            break;
    }

    AppStateMachine_initOsci(IfxStdIf_Inverter_getPeriod(g_Lb.stdIf.inverter)); /* FIXME osci shell is only available once initialized, deny shell access if not initialied */

    /** - Enable all timer clocks: CCU6x, GTM */
    // "LB30_Inverter_startPwmTimer", Enable IOM Module //
    IfxStdIf_Inverter_startPwmTimer(g_Lb.stdIf.inverter);
//#if BYPASSIOM
//#else if
    IfxGtm_Cmu_enableClocks(gtm, IFXGTM_CMU_CLKEN_FXCLK | IFXGTM_CMU_CLKEN_CLK0 | IFXGTM_CMU_CLKEN_CLK1 | IFXGTM_CMU_CLKEN_CLK3);
//#endif
    /** - Reset the position interfaces */

    return result;
}


boolean AppStateMachine_postInit(void)
{
    boolean result = TRUE;

    result &= AppLbStdIf_appPostInit(&g_Lb.stdIf);		// "LB30_AppPostInit" //

    return result;
}


boolean AppStateMachine_selftTest(void)
{
    boolean result = TRUE;

    result &= AppLbStdIf_appSelfTest(&g_Lb.stdIf);		// "LB30_AppSelfTest" //

    return result;
}

// 20191220 Jimmy created //
void LED_Indicator_Process(AppStateMachine *stateMachine)
{
	switch (stateMachine->hwModeRequest)
	{
		case AppMode_run:
			LED_Indicator_0(1);
			LED_Indicator_1_Flash();
			LED_Indicator_2(0);
	        break;
	    case AppMode_appInitialization:
	    	LED_Indicator_0_Flash();
	    	LED_Indicator_1(0);
	    	LED_Indicator_2(0);
	        break;
	    case AppMode_hwInitialization:
	    	LED_Indicator_0_Flash();
	    	LED_Indicator_1(0);
	    	LED_Indicator_2(0);
	        break;
	    case AppMode_shutdown:
	    	LED_Indicator_0(0);
	    	LED_Indicator_1(0);
	    	LED_Indicator_2_Flash();
	        break;
	    case AppMode_configuration:
	    	LED_Indicator_0_Flash();
	    	LED_Indicator_1(0);
	    	LED_Indicator_2(0);
	        break;
	    case AppMode_limitedConfiguration:
	    	LED_Indicator_0_Flash();
	    	LED_Indicator_1(0);
	    	LED_Indicator_2(0);
	        break;
	    case AppMode_boot:
//	    	LED_Indicator_0(0);
//	    	LED_Indicator_1(0);
//	    	LED_Indicator_2(0);
	        break;
	    case AppMode_error:
	    	LED_Indicator_0(0);
	    	LED_Indicator_1(0);
	    	LED_Indicator_2_Flash();
	        break;
	    case AppMode_off:
//	    	LED_Indicator_0(0);
//	    	LED_Indicator_1(0);
//	    	LED_Indicator_2(0);
	        break;
	}
}

void LED_Indicator_0(boolean status)
{
	if(status)
	{
		IfxPort_setPinState(&MODULE_P10, 1, IfxPort_State_high);
	}
	else
	{
		IfxPort_setPinState(&MODULE_P10, 1, IfxPort_State_low);
	}
}

void LED_Indicator_1(boolean status)
{
	if(status)
	{
		IfxPort_setPinState(&MODULE_P10, 3, IfxPort_State_high);
	}
	else
	{
		IfxPort_setPinState(&MODULE_P10, 3, IfxPort_State_low);
	}
}

void LED_Indicator_2(boolean status)
{
	if(status)
	{
		IfxPort_setPinState(&MODULE_P10, 4, IfxPort_State_high);
	}
	else
	{
		IfxPort_setPinState(&MODULE_P10, 4, IfxPort_State_low);
	}
}

void LED_Indicator_0_Flash(void)
{
	static uint32 Pre_TIME_CNT_10ms = 0;

	if(Pre_TIME_CNT_10ms == 0)
	{
		Pre_TIME_CNT_10ms = TIME_CNT_10ms;
	}

	if(TIME_CNT_10ms > Pre_TIME_CNT_10ms)
	{
		if((TIME_CNT_10ms - Pre_TIME_CNT_10ms) >= 50)
		{
			if(IfxPort_getPinState(&MODULE_P10, 1))
			{
				LED_Indicator_0(0);
			}
			else
			{
				LED_Indicator_0(1);
			}
			Pre_TIME_CNT_10ms = TIME_CNT_10ms;
		}
	}
	else if(TIME_CNT_10ms <= 50)
	{
		if((0x100000000 - Pre_TIME_CNT_10ms + TIME_CNT_10ms) >= 50)
		{
			if(IfxPort_getPinState(&MODULE_P10, 1))
			{
				LED_Indicator_0(0);
			}
			else
			{
				LED_Indicator_0(1);
			}
			Pre_TIME_CNT_10ms = TIME_CNT_10ms;
		}
	}
	else
	{
		Pre_TIME_CNT_10ms = TIME_CNT_10ms;
	}
}

void LED_Indicator_1_Flash(void)
{
	static uint32 Pre_TIME_CNT_10ms = 0;

	if(Pre_TIME_CNT_10ms == 0)
	{
		Pre_TIME_CNT_10ms = TIME_CNT_10ms;
	}

	if(TIME_CNT_10ms > Pre_TIME_CNT_10ms)
	{
		if((TIME_CNT_10ms - Pre_TIME_CNT_10ms) >= 50)
		{
			if(IfxPort_getPinState(&MODULE_P10, 3))
			{
				LED_Indicator_1(0);
			}
			else
			{
				LED_Indicator_1(1);
			}
			Pre_TIME_CNT_10ms = TIME_CNT_10ms;
		}
	}
	else if(TIME_CNT_10ms <= 50)
	{
		if((0x100000000 - Pre_TIME_CNT_10ms + TIME_CNT_10ms) >= 50)
		{
			if(IfxPort_getPinState(&MODULE_P10, 3))
			{
				LED_Indicator_1(0);
			}
			else
			{
				LED_Indicator_1(1);
			}
			Pre_TIME_CNT_10ms = TIME_CNT_10ms;
		}
	}
	else
	{
		Pre_TIME_CNT_10ms = TIME_CNT_10ms;
	}
}

void LED_Indicator_2_Flash(void)
{
	static uint32 Pre_TIME_CNT_10ms = 0;

	if(Pre_TIME_CNT_10ms == 0)
	{
		Pre_TIME_CNT_10ms = TIME_CNT_10ms;
	}

	if(TIME_CNT_10ms > Pre_TIME_CNT_10ms)
	{
		if((TIME_CNT_10ms - Pre_TIME_CNT_10ms) >= 50)
		{
			if(IfxPort_getPinState(&MODULE_P10, 4))
			{
				LED_Indicator_2(0);
			}
			else
			{
				LED_Indicator_2(1);
			}
			Pre_TIME_CNT_10ms = TIME_CNT_10ms;
		}
	}
	else if(TIME_CNT_10ms <= 50)
	{
		if((0x100000000 - Pre_TIME_CNT_10ms + TIME_CNT_10ms) >= 50)
		{
			if(IfxPort_getPinState(&MODULE_P10, 4))
			{
				LED_Indicator_2(0);
			}
			else
			{
				LED_Indicator_2(1);
			}
			Pre_TIME_CNT_10ms = TIME_CNT_10ms;
		}
	}
	else
	{
		Pre_TIME_CNT_10ms = TIME_CNT_10ms;
	}
}


