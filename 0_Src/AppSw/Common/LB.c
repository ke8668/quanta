/**
 * \file LB.c
 * \brief
 *
 * \copyright Copyright (c) 2015 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * This file may be used, copied, and distributed, with or without modification, provided
 * that all copyright notices are retained; that all modifications to this file are
 * prominently noted in the modified file; and that this paragraph is not modified.
 */

#include "LB.h"
#include "SysSe/Ext/At25xxx/Ifx_Efs.h"
#include "SysSe/Ext/At25xxx/Ifx_EfsShell.h"
#include "SysSe/Comm/Ifx_Shell.h"
#include "AppShell.h"

#include "Ifx_Assert.h"

#include "main.h"
#include "AppCan.h"

//________________________________________________________________________________________
// GLOBAL VARIABLES
LB   g_Lb;   /**< \brief Logic board HAL \ingroup main_sourceCodeGlobals */
Cpu  g_Cpu0; /**< \brief Cpu specific data \ingroup main_sourceCodeGlobals */
Cpu  g_Cpu1; /**< \brief Cpu specific data \ingroup main_sourceCodeGlobals */
Cpu  g_Cpu2; /**< \brief Cpu specific data \ingroup main_sourceCodeGlobals */

Cpu *g_Cpus[3] = {&g_Cpu0, &g_Cpu1, &g_Cpu2};

#define logicBoard g_Lb

void LB_printShellInfo(IfxStdIf_DPipe *io, LB_PrimaryConsole primaryConsole, LB_PrimaryConsole currentConsole)
{
    if (primaryConsole != currentConsole)
    {
        IfxStdIf_DPipe_print(io, ENDL "Primary console is %s"ENDL, (primaryConsole == LB_PrimaryConsole_can0 ? "CAN0" : "ASC0"));
        IfxStdIf_DPipe_print(io, "To set this console as primary console enter the command 'setup console %s'"ENDL,
            (primaryConsole == LB_PrimaryConsole_can0 ? "asc0" : "can0"));
    }
    else
    {
        IfxStdIf_DPipe_print(io, ENDL "This console is the primary console"ENDL);
    }
}



void Lb_initBenchmark(LB *board)
{
    /* FIXME add CPU int support in iLLD */
	/* 20191212 Jimmy Modified */
    board->srcCpu[0] = &MODULE_SRC.CPU.CPU[0].SB;// IfxCpu_getSrcPointer();
    IfxSrc_init(board->srcCpu[0], ISR_PROVIDER(INTERRUPT_CPU0), ISR_PRIORITY(INTERRUPT_CPU0));
    IfxSrc_enable(board->srcCpu[0]);

    board->srcCpu[1] = &MODULE_SRC.CPU.CPU[1].SB;// IfxCpu_getSrcPointer();
    IfxSrc_init(board->srcCpu[1], ISR_PROVIDER(INTERRUPT_CPU1), ISR_PRIORITY(INTERRUPT_CPU1));
    IfxSrc_enable(board->srcCpu[1]);

    board->srcCpu[2] = &MODULE_SRC.CPU.CPU[2].SB;// IfxCpu_getSrcPointer();
    IfxSrc_init(board->srcCpu[2], ISR_PROVIDER(INTERRUPT_CPU2), ISR_PRIORITY(INTERRUPT_CPU2));
    IfxSrc_enable(board->srcCpu[2]);
}

static const uint8 LB_hwPulseMode_sequence1[6][6] =
{
		{ 1, 1, 0, 0, 0, 2},
		{ 0, 0, 2, 1, 1, 0},
		{ 1, 0, 1, 0, 2, 0},
		{ 0, 2, 0, 1, 0, 1},
		{ 0, 1, 1, 2, 0, 0},
		{ 2, 0, 0, 0, 1, 1}
};


void LB_setupDefaultValue(LB_FileConfiguration *configuration)
{
    configuration->valid                                    = TRUE;
    configuration->ready                                    = FALSE;
    configuration->primaryConsole                           = CFG_CONSOLE_PRIMARY;

    configuration->verboseLevel                             = IFX_VERBOSE_LEVEL_DEBUG;
    configuration->inverter[0].positionSensor[0]            = CFG_POSITION_SENSOR;
    configuration->inverter[0].positionSensor[1]            = ECU_PositionSensor_none;
    configuration->positionSensors.encoder.reversed             = CFG_POS_SENSOR_ENCODER_REVERSED;
    configuration->positionSensors.encoder.offset             = CFG_POS_SENSOR_ENCODER_OFFSET;
    configuration->positionSensors.encoder.resolution             = CFG_POS_SENSOR_ENCODER_RESOLUTION;
    configuration->positionSensors.ad2s1210.resolution          = CFG_POS_SENSOR_AD2S1210_RESOLUTION;
    configuration->positionSensors.ad2s1210.reversed            = CFG_POS_SENSOR_AD2S1210_REVERSED;
    configuration->positionSensors.ad2s1210.offset            = CFG_POS_SENSOR_AD2S1210_OFFSET;
    configuration->positionSensors.ad2s1210.carrierFrequency    = CFG_POS_SENSOR_AD2S1210_CARRIER_FREQUENCY;
    configuration->positionSensors.ad2s1210.periodPerRotation   = CFG_POS_SENSOR_AD2S1210_PERIOD_PER_ROTATION;
    configuration->positionSensors.ad2s1210.gainCode            = CFG_POS_SENSOR_AD2S1210_GAIN_CODE;
    configuration->positionSensors.tle5012.reversed             = CFG_POS_SENSOR_TLE5012_REVERSED;
    configuration->positionSensors.tle5012.offset             = CFG_POS_SENSOR_TLE5012_OFFSET;
    configuration->positionSensors.aurixResolver.carrierFrequency    = CFG_POS_SENSOR_AURIX_RESOLVER_CARRIER_FREQUENCY;

    configuration->positionSensors.aurixResolver.input[0].resolution     = CFG_POS_SENSOR_AURIX_RESOLVER_RESOLUTION;
    configuration->positionSensors.aurixResolver.input[0].reversed       = CFG_POS_SENSOR_AURIX_RESOLVER_REVERSED;
    configuration->positionSensors.aurixResolver.input[0].offset       = CFG_POS_SENSOR_AURIX_RESOLVER_OFFSET;
    configuration->positionSensors.aurixResolver.input[0].periodPerRotation   = CFG_POS_SENSOR_AURIX_RESOLVER_PERIOD_PER_ROTATION;
    configuration->positionSensors.aurixResolver.input[0].signalAmplitudeMax   = CFG_POS_SENSOR_AURIX_RESOLVER_SIGNAL_AMPLITUDE_MAX;
    configuration->positionSensors.aurixResolver.input[0].signalAmplitudeMin   = CFG_POS_SENSOR_AURIX_RESOLVER_SIGNAL_AMPLITUDE_MIN;
    configuration->positionSensors.aurixResolver.input[0].gainCode       = CFG_POS_SENSOR_AURIX_RESOLVER_GAIN_CODE;

    configuration->positionSensors.aurixResolver.input[1].resolution     = CFG_POS_SENSOR_AURIX_RESOLVER_RESOLUTION;
    configuration->positionSensors.aurixResolver.input[1].reversed       = CFG_POS_SENSOR_AURIX_RESOLVER_REVERSED;
    configuration->positionSensors.aurixResolver.input[1].offset       = CFG_POS_SENSOR_AURIX_RESOLVER_OFFSET;
    configuration->positionSensors.aurixResolver.input[1].periodPerRotation   = CFG_POS_SENSOR_AURIX_RESOLVER_PERIOD_PER_ROTATION;
    configuration->positionSensors.aurixResolver.input[1].signalAmplitudeMax   = CFG_POS_SENSOR_AURIX_RESOLVER_SIGNAL_AMPLITUDE_MAX;
    configuration->positionSensors.aurixResolver.input[1].signalAmplitudeMin   = CFG_POS_SENSOR_AURIX_RESOLVER_SIGNAL_AMPLITUDE_MIN;
    configuration->positionSensors.aurixResolver.input[1].gainCode       = CFG_POS_SENSOR_AURIX_RESOLVER_GAIN_CODE;

    configuration->inverter[0].pwmModule                    = CFG_INVERTER_PWM;
    configuration->inverter[0].currentSenor.useThreeSensors = TRUE;
    configuration->inverter[0].currentSenor.gain            = CFG_AN_I_GAIN;
    configuration->inverter[0].currentSenor.offset          = CFG_AN_I_OFFSET;
    configuration->inverter[0].currentSenor.max             = CFG_LIMIT_MAX_MOTOR_PHASE_CURRENT;
    configuration->inverter[0].currentSenor.cutOffFrequency = 1000;
    configuration->inverter[1].pwmModule                    = LB_PwmModule_gtmAtom;			/* Not configurable */
    configuration->inverter[1].currentSenor.useThreeSensors = TRUE;
    configuration->inverter[1].currentSenor.gain            = CFG_AN_I_GAIN;
    configuration->inverter[1].currentSenor.offset          = CFG_AN_I_OFFSET;
    configuration->inverter[1].currentSenor.max             = 0;
    configuration->inverter[1].currentSenor.cutOffFrequency = 1000;
	configuration->analogInput[0].cutOffFrequency 	= CFG_AN0_CUTOFF_FREQUENCY;
	configuration->analogInput[0].gain 				= CFG_AN0_GAIN;
	configuration->analogInput[0].offset 			= CFG_AN0_OFFSET;
	configuration->analogInput[0].max 				= CFG_AN0_MAX;
	configuration->analogInput[0].min 				= CFG_AN0_MIN;
	configuration->analogInput[1].cutOffFrequency 	= CFG_AN1_CUTOFF_FREQUENCY;
	configuration->analogInput[1].gain 				= CFG_AN1_GAIN;
	configuration->analogInput[1].offset 			= CFG_AN1_OFFSET;
	configuration->analogInput[1].max 				= CFG_AN1_MAX;
	configuration->analogInput[1].min 				= CFG_AN1_MIN;
	configuration->analogInput[2].cutOffFrequency 	= CFG_AN2_CUTOFF_FREQUENCY;
	configuration->analogInput[2].gain 				= CFG_AN2_GAIN;
	configuration->analogInput[2].offset 			= CFG_AN2_OFFSET;
	configuration->analogInput[2].max 				= CFG_AN2_MAX;
	configuration->analogInput[2].min 				= CFG_AN2_MIN;
	configuration->analogInput[3].cutOffFrequency 	= CFG_AN3_CUTOFF_FREQUENCY;
	configuration->analogInput[3].gain 				= CFG_AN3_GAIN;
	configuration->analogInput[3].offset 			= CFG_AN3_OFFSET;
	configuration->analogInput[3].max 				= CFG_AN3_MAX;
	configuration->analogInput[3].min 				= CFG_AN3_MIN;
	configuration->safety.fsp.enabled                 = CFG_SAFETY_FSP_ENABLED;
	configuration->safety.functionalWatchdog.enabled  = CFG_SAFETY_FUNCIONAL_WATCHDOG_ENABLED;
	configuration->safety.functionalWatchdog.heartbeatTimerPeriod  = CFG_SAFETY_FUNCIONAL_WATCHDOG_HEARTBEAT_TIMER_PERIOD;
	configuration->safety.functionalWatchdog.servicePeriod  = CFG_SAFETY_FUNCIONAL_WATCHDOG_SERVICE_PERIOD;

	configuration->safety.windowWatchdog.closeWindowTime      = CFG_SAFETY_WINDOW_WATCHDOG_CLOSED_WINDOW_TIME;
	configuration->safety.windowWatchdog.enabled      = CFG_SAFETY_WINDOW_WATCHDOG_ENABLED;
	configuration->safety.windowWatchdog.openWindowTime      = CFG_SAFETY_WINDOW_WATCHDOG_OPENED_WINDOW_TIME;
	configuration->safety.windowWatchdog.useWdiPin      = CFG_SAFETY_WINDOW_WATCHDOG_USE_WDI_PIN;

	configuration->safety.iom.enabled		= CFG_SAFETY_IOM_ENABLED;
	configuration->safety.iom.fspOnFaultEnabled		= CFG_SAFETY_IOM_FSP_ON_FAULT;
	configuration->safety.iom.nmiOnFaultEnabled		= CFG_SAFETY_IOM_NMI_ON_FAULT;
	configuration->safety.iom.eventWindowThreshold		= CFG_SAFETY_IOM_EVENT_WINDOW_THRESHOLD;
	configuration->safety.iom.filterTime		= CFG_SAFETY_IOM_PIN_FILTER_TIME;

	configuration->wizard.enabled  = FALSE;
	configuration->wizard.vdcNom  = CFG_WIZARD_VDCNOM;
	configuration->wizard.vf  = CFG_WIZARD_VF;

	{   /* Special Mode: HwPulseMode */
		uint8 i,j;
		configuration->specialMode.hwPulseMode.pwmPeriod = 500e-6;
		configuration->specialMode.hwPulseMode.pulsePeriod = 1;
		configuration->specialMode.hwPulseMode.startPulse = 10e-6;
		configuration->specialMode.hwPulseMode.pulseIncrement = 10e-6;
		configuration->specialMode.hwPulseMode.pulseCountPerPhase = 25;
		configuration->specialMode.hwPulseMode.enabled = FALSE;
		for (i=0;i<6;i++)
		{
			for (j=0;j<6;j++)
			{
				configuration->specialMode.hwPulseMode.sequence[i][j] = LB_hwPulseMode_sequence1[i][j];
			}
		}
	}
}


void LB_printBoardConfiguration(IfxStdIf_DPipe *io, LB_FileConfiguration *configuration, LB_FileBoardVersion *boardVersion, boolean all)
{
    IfxStdIf_DPipe_print(io, ENDL "Logic board information:"ENDL);

    if (boardVersion->fileVersion != Lb_FileVersion_undefined)
    {
        boolean boardSupported = FALSE;
        App_printBoardVersion(boardVersion, io);

        switch (boardVersion->boardType)
        {   /* FIXME should match isMinimalSetup test*/
        case Lb_BoardType_HybridKit_LogicBoard:
            boardSupported = TRUE;
            break;
        default:
            break;
        }

		if (configuration->valid)
		{
			/* Print configuration file version */
			IfxStdIf_DPipe_print(io, "- Configuration file version: %d.%d"ENDL, ((configuration->fileVersion >> 8) & 0xFF), ((configuration->fileVersion >> 0) & 0xFF));

			if (boardSupported)
			{
				IfxStdIf_DPipe_print(io, "- ");
				Ifx_Assert_printLevel(configuration->verboseLevel, io);

				/* Print primary console */
				IfxStdIf_DPipe_print(io, "- Primary console: %s"ENDL, (configuration->primaryConsole == LB_PrimaryConsole_asc0 ? "ASC0" : "CAN0"));

				/* Print the ready flag */

				IfxStdIf_DPipe_print(io, "- Configuration ready: %s"ENDL, (configuration->ready ? "YES" : "NO"));


				if (configuration->inverter[0].positionSensor[0] == ECU_PositionSensor_encoder)
				{
					IfxStdIf_DPipe_print(io, "- Motor 0 position sensor: encoder"ENDL);
				}
				else if (configuration->inverter[0].positionSensor[0] == ECU_PositionSensor_ad2s1210)
				{
					IfxStdIf_DPipe_print(io, "- Motor 0 position sensor: AD2S1210"ENDL);
				}
				else if (configuration->inverter[0].positionSensor[0] == ECU_PositionSensor_internalResolver0)
				{
					IfxStdIf_DPipe_print(io, "- Motor 0 position sensor: AURIX resolver 0"ENDL);
				}
				else if (configuration->inverter[0].positionSensor[0] == ECU_PositionSensor_internalResolver1)
				{
					IfxStdIf_DPipe_print(io, "- Motor 0 position sensor: AURIX resolver 1"ENDL);
				}
				else if (configuration->inverter[0].positionSensor[0] == ECU_PositionSensor_tle5012)
				{
					IfxStdIf_DPipe_print(io, "- Motor 0 position sensor: iGMR + encoder"ENDL);
				}

				if (configuration->inverter[0].positionSensor[1] == ECU_PositionSensor_encoder)
				{
					IfxStdIf_DPipe_print(io, "- Motor 0 redundant position sensor: encoder"ENDL);
				}
				else if (configuration->inverter[0].positionSensor[1] == ECU_PositionSensor_ad2s1210)
				{
					IfxStdIf_DPipe_print(io, "- Motor 0 redundant  position sensor: AD2S1210"ENDL);
				}
				else if (configuration->inverter[0].positionSensor[1] == ECU_PositionSensor_internalResolver0)
				{
					IfxStdIf_DPipe_print(io, "- Motor 0 redundant  position sensor: AURIX resolver 0"ENDL);
				}
				else if (configuration->inverter[0].positionSensor[1] == ECU_PositionSensor_internalResolver1)
				{
					IfxStdIf_DPipe_print(io, "- Motor 0 redundant  position sensor: AURIX resolver 1"ENDL);
				}
				else if (configuration->inverter[0].positionSensor[1] == ECU_PositionSensor_tle5012)
				{
					IfxStdIf_DPipe_print(io, "- Motor 0 redundant  position sensor: iGMR + encoder"ENDL);
				}

				IfxStdIf_DPipe_print(io, "- Encoder: resolution=%d, offset=%d%s"ENDL,
						configuration->positionSensors.encoder.resolution,
						configuration->positionSensors.encoder.offset,
						(configuration->positionSensors.encoder.reversed ? ", reversed":"")
						);
				IfxStdIf_DPipe_print(io, "- AD2S1210: resolution=%d, offset=%d, carrier frequency=%d, gain code=%d, period per rotation=%d%s"ENDL,
						configuration->positionSensors.ad2s1210.resolution,
						configuration->positionSensors.ad2s1210.offset,
						configuration->positionSensors.ad2s1210.carrierFrequency,
						configuration->positionSensors.ad2s1210.gainCode,
						configuration->positionSensors.ad2s1210.periodPerRotation,
						(configuration->positionSensors.ad2s1210.reversed ? ", reversed":"")
						);
				IfxStdIf_DPipe_print(io, "- AURIX resolver 0: resolution=%d, offset=%d, carrier frequency=%d, gain code=%d, period per rotation=%d, Max signal amplitude=%d, Min signal amplitude=%d%s"ENDL,
						configuration->positionSensors.aurixResolver.input[0].resolution,
						configuration->positionSensors.aurixResolver.input[0].offset,
						configuration->positionSensors.aurixResolver.carrierFrequency,
						configuration->positionSensors.aurixResolver.input[0].gainCode,
						configuration->positionSensors.aurixResolver.input[0].periodPerRotation,
						configuration->positionSensors.aurixResolver.input[0].signalAmplitudeMax,
						configuration->positionSensors.aurixResolver.input[0].signalAmplitudeMin,
						(configuration->positionSensors.aurixResolver.input[0].reversed ? ", reversed":"")
						);
				IfxStdIf_DPipe_print(io, "- AURIX resolver 1: resolution=%d, offset=%d, carrier frequency=%d, gain code=%d, period per rotation=%d, Max signal amplitude=%d, Min signal amplitude=%d%s"ENDL,
						configuration->positionSensors.aurixResolver.input[1].resolution,
						configuration->positionSensors.aurixResolver.input[1].offset,
						configuration->positionSensors.aurixResolver.carrierFrequency,
						configuration->positionSensors.aurixResolver.input[1].gainCode,
						configuration->positionSensors.aurixResolver.input[1].periodPerRotation,
						configuration->positionSensors.aurixResolver.input[1].signalAmplitudeMax,
						configuration->positionSensors.aurixResolver.input[1].signalAmplitudeMin,
						(configuration->positionSensors.aurixResolver.input[1].reversed ? ", reversed":"")
						);
				IfxStdIf_DPipe_print(io, "- TLE2015: offset=%d%s"ENDL,
						configuration->positionSensors.tle5012.offset,
						(configuration->positionSensors.tle5012.reversed ? ", reversed":"")
						);

				IfxStdIf_DPipe_print(io, "- Current sensors: %d sensors, gain=%f A/V, offset=%f V, digital filter: %f Hz, error threshold=%fA"ENDL,
					(configuration->inverter[0].currentSenor.useThreeSensors ? 3 : 2),
					(configuration->inverter[0].currentSenor.gain / CFG_ADC_GAIN),
					(-configuration->inverter[0].currentSenor.offset * CFG_ADC_GAIN),
					(configuration->inverter[0].currentSenor.cutOffFrequency),
					(configuration->inverter[0].currentSenor.max)
					);

				switch (configuration->inverter[0].pwmModule)
				{
				case LB_PwmModule_gtmAtom:
					IfxStdIf_DPipe_print(io, "- PWM Module: GTM ATOM"ENDL);
					break;
				case LB_PwmModule_gtmTom:
					IfxStdIf_DPipe_print(io, "- PWM Module: GTM TOM"ENDL);
					break;
				case LB_PwmModule_ccu6:
					IfxStdIf_DPipe_print(io, "- PWM Module: CCU6"ENDL);
					break;
				}

				{
					uint8 i;
					for (i=0;i<4;i++)
					{
						IfxStdIf_DPipe_print(io, "- Analog input %d: Gain=%f, Offset=%f V"ENDL, i, configuration->analogInput[i].gain * CFG_ADC_GAIN, configuration->analogInput[i].offset * CFG_ADC_GAIN);

						if (configuration->analogInput[i].cutOffFrequency > 0.0)
						{
							Ifx_Console_printAlign("    - Low pass filter: gain=%f, F=%f Hz"ENDL, configuration->analogInput[i].cutOffFrequency);
						}
						if (configuration->analogInput[i].min < configuration->analogInput[i].max)
						{
							Ifx_Console_printAlign("    - Limit: min=%f, max=%f"ENDL, configuration->analogInput[i].min, configuration->analogInput[i].max);
						}

					}
				}

				IfxStdIf_DPipe_print(io, "- Safety items:"ENDL);
				IfxStdIf_DPipe_print(io, "    - Window watchdog %s, closed window time = %fms, opened window time = %fms, %s"ENDL,
					(configuration->safety.windowWatchdog.enabled ? "enabled" : "disabled"),
					(configuration->safety.windowWatchdog.closeWindowTime *1e3),
					(configuration->safety.windowWatchdog.openWindowTime *1e3),
					(configuration->safety.windowWatchdog.useWdiPin ? "WDI pin trigger" : "SPI trigger")
					);
				IfxStdIf_DPipe_print(io, "    - Functional watchdog %s, heartbeat timer period %fms, service period %fms"ENDL,
					(configuration->safety.functionalWatchdog.enabled ? "enabled" : "disabled"),
					(configuration->safety.functionalWatchdog.heartbeatTimerPeriod *1e3),
					(configuration->safety.functionalWatchdog.servicePeriod *1e3)
					);
				IfxStdIf_DPipe_print(io, "    - Fault signaling protocol %s"ENDL,
					(configuration->safety.fsp.enabled ? "enabled" : "disabled")
					);
				IfxStdIf_DPipe_print(io, "    - IOM monitoring %s, FSP triggering %s, NMI triggering %s"ENDL,
					(configuration->safety.iom.enabled ? "enabled" : "disabled"),
					(configuration->safety.iom.fspOnFaultEnabled ? "enabled" : "disabled"),
					(configuration->safety.iom.nmiOnFaultEnabled ? "enabled" : "disabled")
					);
				IfxStdIf_DPipe_print(io, "    - IOM event window threshold=%fus"ENDL,
					(configuration->safety.iom.eventWindowThreshold *1e6)
					);
				IfxStdIf_DPipe_print(io, "    - IOM pin delay debounce filter  =%fus"ENDL,
					(configuration->safety.iom.filterTime *1e6)
					);



				if (all)
				{   /* Special Mode: HwPulseMode */
					uint8 i,j;
					IfxStdIf_DPipe_print(io, "- Special mode: Hardware pulse mode:"ENDL);
					IfxStdIf_DPipe_print(io, "    - %s"ENDL, (configuration->specialMode.hwPulseMode.enabled ? "Enabled":"Disabled"));
					IfxStdIf_DPipe_print(io, "    - PWM period               : %3.2f us"ENDL, (configuration->specialMode.hwPulseMode.pwmPeriod * 1e6));
					IfxStdIf_DPipe_print(io, "    - Pulse period             : %3.2f us"ENDL, (configuration->specialMode.hwPulseMode.pulsePeriod * 1e6));
					IfxStdIf_DPipe_print(io, "    - 1st pulse width          : %3.2f us"ENDL, (configuration->specialMode.hwPulseMode.startPulse * 1e6));
					IfxStdIf_DPipe_print(io, "    - Pulse width increment    : %3.2f us"ENDL, (configuration->specialMode.hwPulseMode.pulseIncrement * 1e6));
					IfxStdIf_DPipe_print(io, "    - Number of pulse per phase: %d"ENDL, (configuration->specialMode.hwPulseMode.pulseCountPerPhase));
					IfxStdIf_DPipe_print(io, "    - Pulse sequence: UT, VT, WT, UB, VB, WB"ENDL);
					for (i=0;i<6;i++)
					{
						IfxStdIf_DPipe_print(io, "    - Sequence %d: ", i);
						for (j=0;j<6;j++)
						{
							if (configuration->specialMode.hwPulseMode.sequence[i][j] == 0)
							{
								IfxStdIf_DPipe_print(io, "  OFF, ");
							}
							else if (configuration->specialMode.hwPulseMode.sequence[i][j] == 1)
							{
								IfxStdIf_DPipe_print(io, "   ON, ");

							}
							else if (configuration->specialMode.hwPulseMode.sequence[i][j] == 2)
							{
								IfxStdIf_DPipe_print(io, "PULSE, ");

							}
							else
							{
								IfxStdIf_DPipe_print(io, "ERROR, ");
							}
						}
						IfxStdIf_DPipe_print(io, ENDL);
					}
				}

			}
		}
		else
		{
			IfxStdIf_DPipe_print(io, "- Configuration invalid"ENDL);
		}
    }
    else
    {
        IfxStdIf_DPipe_print(io, "- Board version invalid"ENDL);
    }
}

boolean Lb_initQspi0(LB *board)
{
    IfxQspi_SpiMaster_Config spiConfig;
    IfxQspi_SpiMaster_Pins   spiPins;

    spiPins.sclk      = &IfxQspi0_SCLK_P20_11_OUT;
    spiPins.sclkMode  = IfxPort_OutputMode_pushPull;
    spiPins.mtsr      = &IfxQspi0_MTSR_P20_14_OUT;
    spiPins.mtsrMode  = IfxPort_OutputMode_pushPull;
    spiPins.mrst      = &IfxQspi0_MRSTA_P20_12_IN;
    spiPins.mrstMode  = IfxPort_InputMode_noPullDevice;
    spiPins.pinDriver = IfxPort_PadDriver_cmosAutomotiveSpeed1;

    IfxQspi_SpiMaster_initModuleConfig(&spiConfig, &MODULE_QSPI0);
    spiConfig.base.rxPriority      = ISR_PRIORITY(INTERRUPT_QSPI0_RX);
    spiConfig.base.txPriority      = ISR_PRIORITY(INTERRUPT_QSPI0_TX);
    spiConfig.base.erPriority      = ISR_PRIORITY(INTERRUPT_QSPI0_ERR);
    spiConfig.base.bufferSize      = 0;
    spiConfig.base.buffer          = NULL_PTR;
    spiConfig.base.maximumBaudrate = 10.0e6;
    spiConfig.pins                 = &spiPins;
    IfxQspi_SpiMaster_initModule(&board->driver.qspi0, &spiConfig);

    App_printQspiInitializationStatus(&spiConfig);
    return TRUE;
}


LB_BoardVersion Lb_discoverLogicBoardVersion(LB *board)
{
	board->boardVersion.boardType    = Lb_BoardType_undefined;
	board->boardVersion.boardVersion = LB_BoardVersion_undefined;
	board->boardVersion.fileVersion  = Lb_FileVersion_undefined;
    /* Initialize the QSPI required for the EEPROM */
    Lb_initQspi0(board);

    /** Detect EEPROM */
    /* 1st test on CS8: LB 3.0, LB3.1 */
	/* No side effect expected on logic board 2.3 because the CS8 connects to a 1EDI2002/ 1EDI2010 CS, command will be ignored */
    if (App_initEeprom(&board->driver.eeprom, &board->driver.qspi0, &board->driver.sscEeprom, &IfxQspi0_SLSO8_P20_6_OUT, 3e6))
    {
        Ifx_Efs_Init(&board->driver.efsBoard, &board->driver.eeprom);
        Ifx_Efs_tableOffsetSet(&board->driver.efsBoard, CFG_LB30_BOARD_EFS_OFFSET);

        /* Try to mount the file system */
        if (Ifx_Efs_mount(&board->driver.efsBoard))
        {
            board->boardVersion.boardType    = Lb_BoardType_undefined;
            board->boardVersion.boardVersion = LB_BoardVersion_undefined;
            board->boardVersion.fileVersion  = Lb_FileVersion_undefined;
        	App_loadBoardVersionFile(&board->driver.efsBoard, &board->boardVersion);
        }

        if  (
        		(board->boardVersion.boardType == Lb_BoardType_HybridKit_LogicBoard)
        		&& (board->boardVersion.boardVersion != LB_BoardVersion_undefined))
		{
			return board->boardVersion.boardVersion;
		}
	}

    /* 2nd test on CS0: LB 3.3 */
    if (App_initEeprom(&board->driver.eeprom, &board->driver.qspi0, &board->driver.sscEeprom, &IfxQspi0_SLSO0_P20_8_OUT, 3e6))
    {
    	/* LB3.3 */
        Ifx_Efs_Init(&board->driver.efsBoard, &board->driver.eeprom);
        Ifx_Efs_tableOffsetSet(&board->driver.efsBoard, CFG_LB30_BOARD_EFS_OFFSET);

        /* Try to mount the file system */
        if (Ifx_Efs_mount(&board->driver.efsBoard))
        {
            board->boardVersion.boardType    = Lb_BoardType_undefined;
            board->boardVersion.boardVersion = LB_BoardVersion_undefined;
            board->boardVersion.fileVersion  = Lb_FileVersion_undefined;
        	App_loadBoardVersionFile(&board->driver.efsBoard, &board->boardVersion);
        }

        if  (
        		(board->boardVersion.boardType == Lb_BoardType_HybridKit_LogicBoard)
        		&& (board->boardVersion.boardVersion != LB_BoardVersion_undefined))
		{
			return board->boardVersion.boardVersion;
		}
	}

    board->boardVersion.boardType    = Lb_BoardType_HybridKit_LogicBoard;
    board->boardVersion.boardVersion = LB_BoardVersion_HybridKit_LogicBoard_3_x;

    /* FIXME uninitialized the unused CS */

	return board->boardVersion.boardVersion;
}
