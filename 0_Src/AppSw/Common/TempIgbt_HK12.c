/**
 * \file TempIgbt_HK12.c
 * \brief Lookup functions for IGBT temperature
 *
 * \copyright Copyright (c) 2015 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * This file may be used, copied, and distributed, with or without modification, provided
 * that all copyright notices are retained; that all modifications to this file are
 * prominently noted in the modified file; and that this paragraph is not modified.
 */

//////////////////////////////////////////////////////////////////////////////////////////
// TEMPERATURE SENSOR  LOOK-UP TABLE /////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
#include "Configuration.h"
#include "TempIgbt_HK12.h"

#define ADC_GAIN          (CFG_ADC_RESOLUTION / CFG_ADC_FULL_VOLTAGE)

#define TEMP_LOOKUP_ITEMS (26)

#define TEMP_0            (-40.0)
#define TEMP_1            (-30.0)
#define TEMP_2            (-10.0)
#define TEMP_3            (0.0)
#define TEMP_4            (20.0)
#define TEMP_5            (30.0)
#define TEMP_6            (40.0)
#define TEMP_7            (50.0)
#define TEMP_8            (60.0)
#define TEMP_9            (70.0)
#define TEMP_10           (75.0)
#define TEMP_11           (80.0)
#define TEMP_12           (85.0)
#define TEMP_13           (90.0)
#define TEMP_14           (95.0)
#define TEMP_15           (100.0)
#define TEMP_16           (105.0)
#define TEMP_17           (110.0)
#define TEMP_18           (115.0)
#define TEMP_19           (120.0)
#define TEMP_20           (125.0)
#define TEMP_21           (130.0)
#define TEMP_22           (135.0)
#define TEMP_23           (140.0)
#define TEMP_24           (145.0)
#define TEMP_25           (150.0)

/* NOTE: This part is generated from TempIgbt.xlsx
 * ADC_GAIN * V_AIN */
#define Adc_RAW_0         (ADC_GAIN * 4.562827879)
#define Adc_RAW_1         (ADC_GAIN * 4.331550802)
#define Adc_RAW_2         (ADC_GAIN * 3.764124294)
#define Adc_RAW_3         (ADC_GAIN * 3.487903226)
#define Adc_RAW_4         (ADC_GAIN * 3.059006211)
#define Adc_RAW_5         (ADC_GAIN * 2.913022738)
#define Adc_RAW_6         (ADC_GAIN * 2.804633269)
#define Adc_RAW_7         (ADC_GAIN * 2.725628485)
#define Adc_RAW_8         (ADC_GAIN * 2.668221186)
#define Adc_RAW_9         (ADC_GAIN * 2.626582278)
#define Adc_RAW_10        (ADC_GAIN * 2.610192869)
#define Adc_RAW_11        (ADC_GAIN * 2.596197872)
#define Adc_RAW_12        (ADC_GAIN * 2.584196504)
#define Adc_RAW_13        (ADC_GAIN * 2.57390352)
#define Adc_RAW_14        (ADC_GAIN * 2.565025416)
#define Adc_RAW_15        (ADC_GAIN * 2.557378346)
#define Adc_RAW_16        (ADC_GAIN * 2.550757632)
#define Adc_RAW_17        (ADC_GAIN * 2.545019663)
#define Adc_RAW_18        (ADC_GAIN * 2.540038797)
#define Adc_RAW_19        (ADC_GAIN * 2.535697186)
#define Adc_RAW_20        (ADC_GAIN * 2.531897289)
#define Adc_RAW_21        (ADC_GAIN * 2.528574388)
#define Adc_RAW_22        (ADC_GAIN * 2.525662416)
#define Adc_RAW_23        (ADC_GAIN * 2.523105884)
#define Adc_RAW_24        (ADC_GAIN * 2.520836777)
#define Adc_RAW_25        (ADC_GAIN * 2.518844963)

#define TEMP_GAIN(a, b)       ((TEMP_##a - TEMP_##b) / (Adc_RAW_##a - Adc_RAW_##b))
#define TEMP_OFFSET(a, b)     (TEMP_##a - (TEMP_GAIN(a, b) * Adc_RAW_##a))
#define TEMP_ADC_RAW_LU(a, b) {TEMP_GAIN(a, b), TEMP_OFFSET(a, b), Adc_RAW_##a}

static const Ifx_LutLinearF32_Item g_TempIgbtLutItems[TEMP_LOOKUP_ITEMS] = {
    {0, TEMP_0, Adc_RAW_0},
    TEMP_ADC_RAW_LU(1, 0),
    TEMP_ADC_RAW_LU(2, 1),
    TEMP_ADC_RAW_LU(3, 2),
    TEMP_ADC_RAW_LU(4, 3),
    TEMP_ADC_RAW_LU(5, 4),
    TEMP_ADC_RAW_LU(6, 5),
    TEMP_ADC_RAW_LU(7, 6),
    TEMP_ADC_RAW_LU(8, 7),
    TEMP_ADC_RAW_LU(9, 8),
    TEMP_ADC_RAW_LU(10, 9),
    TEMP_ADC_RAW_LU(11, 10),
    TEMP_ADC_RAW_LU(12, 11),
    TEMP_ADC_RAW_LU(13, 12),
    TEMP_ADC_RAW_LU(14, 13),
    TEMP_ADC_RAW_LU(15, 14),
    TEMP_ADC_RAW_LU(16, 15),
    TEMP_ADC_RAW_LU(17, 16),
    TEMP_ADC_RAW_LU(18, 17),
    TEMP_ADC_RAW_LU(19, 18),
    TEMP_ADC_RAW_LU(20, 19),
    TEMP_ADC_RAW_LU(21, 20),
    TEMP_ADC_RAW_LU(22, 21),
    TEMP_ADC_RAW_LU(23, 22),
    TEMP_ADC_RAW_LU(24, 23),
    TEMP_ADC_RAW_LU(25, 24),
};

const Ifx_LutLinearF32             g_TempIgbtLut = {
    TEMP_LOOKUP_ITEMS,
    g_TempIgbtLutItems
};
