/**
 * \file TempIgbt_R2fV1.c
 * \brief Lookup functions for hybrid kit IGBT temperature (R2F)
 *
 * \copyright Copyright (c) 2015 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * This file may be used, copied, and distributed, with or without modification, provided
 * that all copyright notices are retained; that all modifications to this file are
 * prominently noted in the modified file; and that this paragraph is not modified.
 */

//////////////////////////////////////////////////////////////////////////////////////////
// TEMPERATURE SENSOR  LOOK-UP TABLE /////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
#include "Configuration.h"
#include "TempIgbt_R2fV1.h"

#define LU_ITEM_COUNT (22)

/** Fit function T=57.3*f^0.21-80
 * Valid for:
 * - HP2 3.2
 * - HP2 3.1
 * - HPD 1.3
 */

static const Ifx_LutLinearF32_Item g_TempIgbtR2fV1LutItems[LU_ITEM_COUNT] = {
		{-2759.45011985857,-24.7197551622305,0.00191351342057966},
		{-9005.77983558995,-12.7673194218129,0.000803115411461009},
		{-23943.4073385157,-0.770680563549387,0.000385463911045116},
		{-55136.1310245781,11.2529887046299,0.0002040946380444},
		{-114135.0583635,23.294353424875,0.000116479139849693},
		{-217617.669814641,35.3479189961039,7.05269889580964E-05},
		{-388649.033884089,47.410246121318,4.47968336556072E-05},
		{-658061.559963699,59.4790742368432,2.96006869599217E-05},
		{-1065950.17275631,71.552857378634,2.02193854173344E-05},
		{-1663280.17744871,83.6305029648479,1.42071692341662E-05},
		{-2513605.39153331,95.7112171854263,1.02288200335783E-05},
		{-3694894.36863645,107.794409539864,7.5223827170251E-06},
		{-5301462.74621082,119.879631737049,5.63611085608494E-06},
		{-7446009.92245022,131.966537358438,4.29310969114567E-06},
		{-10261758.4154263,144.054854501462,3.31861783554253E-06},
		{-13904694.38446,156.144366782037,2.59943626106822E-06},
		{-18555907.9040639,168.234899862866,2.06052433869281E-06},
		{-24424031.6774213,180.326311720331,1.65109152546712E-06},
		{-31747776.961634,192.418485493774,0.000001336108841417},
		{-40798565.5527221,204.511324152123,1.09100218473621E-06},
		{-51883256.7460577,216.604746461179,8.98261778154853E-07},
		{-65346968.2487168,228.698683896121,7.45232490523038E-07},
};

const Ifx_LutLinearF32             g_TempIgbtR2fV1Lut = {
    LU_ITEM_COUNT,
    g_TempIgbtR2fV1LutItems
};
