/**
 * \file TempIgbt_R2fV2.c
 * \brief Lookup functions for hybrid kit IGBT temperature (R2F)
 *
 * \copyright Copyright (c) 2015 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * This file may be used, copied, and distributed, with or without modification, provided
 * that all copyright notices are retained; that all modifications to this file are
 * prominently noted in the modified file; and that this paragraph is not modified.
 */

//////////////////////////////////////////////////////////////////////////////////////////
// TEMPERATURE SENSOR  LOOK-UP TABLE /////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
#include "Configuration.h"
#include "TempIgbt_R2fV2.h"

#define LU_ITEM_COUNT (22)

/** Fit function T=195*f^0.11-205
 * Valid for:
 * - HPD Sense 1.0
 * - HPD Sense 1.1
 */

static const Ifx_LutLinearF32_Item g_TempIgbtR2fV2LutItems[LU_ITEM_COUNT] = {
		{-5286.33132599315,-15.8617434390241,0.00267449308208462},
		{-9427.62216329639,-4.78588974375614,0.00161378022927938},
		{-16292.4765624018,6.29247656240184,0.001},
		{-27372.9839336518,17.3729839336518,0.000634676291622479},
		{-44834.3995175219,28.4553304229014,0.000411633268684436},
		{-71761.1291557351,39.5392681588612,0.000272282061176286},
		{-112473.776852023,50.6245917895503,0.000183372447932333},
		{-172932.903053696,61.711129761001,0.000125546552319541},
		{-261247.617297191,72.7987376533573,8.72686912486625E-05},
		{-388309.857130277,83.8872930307144,6.15160614444569E-05},
		{-568578.198890485,94.9766914189257,4.39283311735569E-05},
		{-821038.317875521,106.066843133816,3.17486316610231E-05},
		{-1170370.77862888,117.157670757513,2.32043308440502E-05},
		{-1648360.70347277,128.249107113714,1.71376975040707E-05},
		{-2295588.05208732,139.341093630631,1.27815152217543E-05},
		{-3163441.75994122,150.433579007822,9.62040123298725E-06},
		{-4316505.84185002,161.526518123131,7.3037126041786E-06},
		{-5835370.78203845,172.61987113083,5.5900254412685E-06},
		{-7819929.11702571,183.71360271309,4.31124147144609E-06},
		{-10393220.0856739,194.807681455224,3.34907575980258E-06},
		{-13705894.58514,205.902079321502,2.61946267706065E-06},
		{-17941378.4455579,216.996771213159,2.0620919025494E-06},
};

const Ifx_LutLinearF32             g_TempIgbtR2fV2Lut = {
    LU_ITEM_COUNT,
    g_TempIgbtR2fV2LutItems
};
