/**
 * \file TempIgbt_R2fV3.c
 * \brief Lookup functions for hybrid kit IGBT temperature (R2F)
 *
 * \copyright Copyright (c) 2015 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * This file may be used, copied, and distributed, with or without modification, provided
 * that all copyright notices are retained; that all modifications to this file are
 * prominently noted in the modified file; and that this paragraph is not modified.
 */

//////////////////////////////////////////////////////////////////////////////////////////
// TEMPERATURE SENSOR  LOOK-UP TABLE /////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
#include "Configuration.h"
#include "TempIgbt_R2fV3.h"

#define LU_ITEM_COUNT (22)

/** Fit function T=120*f^0.173-133
 * Valid for:
 * - HPD Sense 1.2
 * - HPD Sense DSC 1.0
 */

static const Ifx_LutLinearF32_Item g_TempIgbtR2fV3LutItems[LU_ITEM_COUNT] = {
		{-5139.53355142981,-17.5716018292803,0.00241819574604433},
		{-9972.25208552114,-5.88514242831109,0.001415413233705},
		{-18233.9764292087,5.80861154096699,0.000866986507432547},
		{-31728.260543071,17.507973795147,0.000551810073904934},
		{-52938.0319604738,29.2117393284908,0.000362909965048101},
		{-85197.5076684601,40.9190245301461,0.000245535639511322},
		{-132889.738547605,52.6291669387785,0.000170285284523095},
		{-201671.332580332,64.3416602485936,0.0001206996549145},
		{-298725.882473278,76.0561109285543,8.72241491524762E-05},
		{-433047.600796135,87.772208521964,6.41319995097683E-05},
		{-615756.644932991,99.4897048509792,4.78918174795959E-05},
		{-860447.594843006,111.208399162979,3.62699592049795E-05},
		{-1183572.5286475,122.928127330179,0.000027820962833437},
		{-1604860.12424439,134.648753869468,2.15898902004197E-05},
		{-2147772.19936821,146.370165959874,1.69339029393212E-05},
		{-2839999.08765437,158.0922688981,1.34127750476009E-05},
		{-3713995.23423605,169.81498260467,1.07202567837596E-05},
		{-4807556.38112066,181.538238908016,8.64019797482495E-06},
		{-6164439.69998976,193.261979411782,7.01799052586302E-06},
		{-7835028.21808346,204.986153804379,5.74167093623861E-06},
		{-9877040.87140825,216.710718507406,4.72922195175099E-06},
		{-12356289.5086097,228.435635586308,3.91991750861439E-06},
};

const Ifx_LutLinearF32             g_TempIgbtR2fV3Lut = {
    LU_ITEM_COUNT,
    g_TempIgbtR2fV3LutItems
};
