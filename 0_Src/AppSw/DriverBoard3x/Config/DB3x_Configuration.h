/**
 * \file Db3x_Configuration.h
 * \brief Driver board version 3.x configuration
 *
 *
 * \version disabled
 * \copyright Copyright (c) 2013 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * \defgroup sourceCodeApplication_db3x_configuratiuon Driver board version 3.x configuration
 * \ingroup sourceCodeApplication_db3x
 *
 */
#ifndef DB3X_CONFIGURATION_H_
#define DB3X_CONFIGURATION_H_

#define CFG_DB3X_DRIVER_SPI_FREQUENCY      (200000)         /**< \brief Define SPI frequency for the 1EDI2002AS drivers. THis value is overwritten by the EEPROM settings */
/* HP Drive */
#define CFG_DBHPDRIVE_VDC_NOM                    (400)                                /**< \brief Define the nominal DC link voltage in V FIXME document usage */
#define CFG_DBHPDRIVE_VDC_MAX                    (500)                                /**< \brief Maximum allowed DC-link voltage in V  FIXME document usage  */
#define CFG_DBHPDRIVE_VDC_MIN                    (0)                                /**< \brief Minimum allowed DC-link voltage in V  FIXME document usage */
#define CFG_DBHPDRIVE_FREQUENCY                  (8000)                             /**< \brief Define the PWM frequency in Hz  */
#define CFG_DBHPDRIVE_DEAD_TIME                  (2.0e-6)                            /**< \brief Define the dead time between the top and bottom PWM in s. */
#define CFG_DBHPDRIVE_MIN_PULSE                  (0) /**< \brief Define the smallest PWM pulse in s. */
#define CFG_DBHPDRIVE_MIN_IGBT_TEMP                 (-40.0) /**< \brief Minimal IGBT temperature in �C */
#define CFG_DBHPDRIVE_MAX_IGBT_TEMP                 (125.0) /**< \brief Maximal IGBT temperature in �C */

/* HP2 */
#define CFG_DBHP2_VDC_NOM                    (350)                                /**< \brief Define the nominal DC link voltage in V FIXME document usage */
#define CFG_DBHP2_VDC_MAX                    (400)                                /**< \brief Maximum allowed DC-link voltage in V  FIXME document usage  */
#define CFG_DBHP2_VDC_MIN                    (0)                                /**< \brief Minimum allowed DC-link voltage in V  FIXME document usage */
#define CFG_DBHP2_FREQUENCY                  (8000)                             /**< \brief Define the PWM frequency in Hz  */
#define CFG_DBHP2_DEAD_TIME                  (3.0e-6)                            /**< \brief Define the dead time between the top and bottom PWM in s. */
#define CFG_DBHP2_MIN_PULSE                  (0) /**< \brief Define the smallest PWM pulse in s. */
#define CFG_DBHP2_MIN_IGBT_TEMP                 (-40.0) /**< \brief Minimal IGBT temperature in �C */
#define CFG_DBHP2_MAX_IGBT_TEMP                 (125.0) /**< \brief Maximal IGBT temperature in �C */


#define CFG_DB3X_DRIVER_ACTIVE_CLAMPING_ACTIVATION_TIME    (0x26)                                               /**< \brief Active clamping activation time see SACLT.AT*/
#define CFG_DB3X_DRIVER_ACTIVE_CLAMPING_MODE               (0)                                                  /**< \brief Active clamping mode see SCFG2.ACLPM*/

#define CFG_DB3X_DRIVER_TWO_LEVEL_TURN_ON_DELAY    (0)                                                          /**< \brief Two level turn on delay */
#define CFG_DB3X_DRIVER_TWO_LEVEL_TURN_ON_LEVEL    (Ifx1edi2002as_GateTurnOnPlateauLevel_9Dot25Volt)            /**< \brief Two level turn on level */
#define CFG_DB3X_DRIVER_TWO_LEVEL_TURN_OFF_DELAY   (0)                                                          /**< \brief Two level turn off delay */
#define CFG_DB3X_DRIVER_TWO_LEVEL_TURN_OFF_LEVEL   (Ifx1edi2002as_GateTurnOffPlateauLevel_15)                   /**< \brief Two level turn off level */
#define CFG_DB3X_DRIVER_TWO_LEVEL_VBE_COMPENSATION (Ifx1edi2002as_VbeCompensation_enabled)                      /**< \brief Two level turn VBE compensation */

#endif /* DB3X_CONFIGURATION_H_ */
