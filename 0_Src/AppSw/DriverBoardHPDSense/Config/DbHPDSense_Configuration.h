/**
 * \file DbHPDSense_Configuration.h
 * \brief Driver board HPD Sense configuration
 *
 *
 * \version disabled
 * \copyright Copyright (c) 2013 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * \defgroup sourceCodeApplication_dbhpdsense_configuratiuon Driver board HPD Sense configuration
 * \ingroup sourceCodeApplication_dbhpdsense
 *
 */
#ifndef DBHPDSENSE_CONFIGURATION_H_
#define DBHPDSENSE_CONFIGURATION_H_

#define CFG_DBHPDSENSE_DRIVER_SPI_FREQUENCY      (1.8e6)         /**< \brief Define SPI frequency for the 1EDI2010AS drivers. THis value is overwritten by the EEPROM settings */
#define CFG_DBHPDRIVE_VDC_NOM                    (400)                                /**< \brief Define the nominal DC link voltage in V FIXME document usage */
#define CFG_DBHPDRIVE_VDC_MAX                    (500)                                /**< \brief Maximum allowed DC-link voltage in V  FIXME document usage  */
#define CFG_DBHPDRIVE_VDC_MIN                    (0)                                /**< \brief Minimum allowed DC-link voltage in V  FIXME document usage */
#define CFG_DBHPDRIVE_FREQUENCY                  (8000)                             /**< \brief Define the PWM frequency in Hz  */
#define CFG_DBHPDRIVE_DEAD_TIME                  (2.0e-6)                            /**< \brief Define the dead time between the top and bottom PWM in s. */
#define CFG_DBHPDRIVE_MIN_PULSE                  (0) /**< \brief Define the smallest PWM pulse in s. */
#define CFG_DBHPDRIVE_MIN_IGBT_TEMP                 (-40.0) /**< \brief Minimal IGBT temperature in �C */
#define CFG_DBHPDRIVE_MAX_IGBT_TEMP                 (125.0) /**< \brief Maximal IGBT temperature in �C */

#define CFG_DBHPDSENSE_DRIVER_VDC_FULL_SCALE_GAIN    (Ifx1edi2010as_AdcGain_gain0)     /**< \brief Define the full scale gain for Vdc measured on LS V. This value is overwritten by the EEPROM settings */
#define CFG_DBHPDSENSE_DRIVER_VDC_FULL_SCALE_OFFSET  (Ifx1edi2010as_AdcOffset_ofst0)         /**< \brief Define the full scale offset for Vdc measured on LS V. This value is overwritten by the EEPROM settings */
#define CFG_DBHPDSENSE_DRIVER_VDC_ZOOM_GAIN          (Ifx1edi2010as_AdcGain_gain0)     /**< \brief Define the zoom gain for Vdc measured on LS W. This value is overwritten by the EEPROM settings */
#define CFG_DBHPDSENSE_DRIVER_VDC_ZOOM_OFFSET        (Ifx1edi2010as_AdcOffset_ofst0)         /**< \brief Define the zoom offset for Vdc measured on LS W. This value is overwritten by the EEPROM settings */

#define CFG_DBHPDSENSE_DRIVER_IGBT_TEMP_FULL_SCALE_GAIN    (Ifx1edi2010as_AdcGain_gain2)     /**< \brief Define the full scale gain for temp measurement. This value is overwritten by the EEPROM settings */
#define CFG_DBHPDSENSE_DRIVER_IGBT_TEMP_FULL_SCALE_OFFSET  (Ifx1edi2010as_AdcOffset_ofst4)         /**< \brief Define the full scale offset for temp measurement. This value is overwritten by the EEPROM settings */

#define CFG_DBHPDSENSE_DRIVER_ACTIVE_CLAMPING    (0x1)                                               /**< \brief Active clamping configuration SCFG.DACLC*/
#define CFG_DBHPDSENSE_DRIVER_BLANKING_TIME      (0x0)                                               /**< \brief Blanking time configuration SOCP.OCPBT */
#define CFG_DBHPDSENSE_DRIVER_BLANKING_TIME_DSC  (0x32)                                               /**< \brief Blanking time configuration SOCP.OCPBT for the DSC version */

#define CFG_DBHPDSENSE_DRIVER_TWO_LEVEL_TURN_ON_DELAY    (0)                                                          /**< \brief Two level turn on delay */
#define CFG_DBHPDSENSE_DRIVER_TWO_LEVEL_TURN_ON_LEVEL    (Ifx1edi2010as_GateTtonPlateauLevel_gpon6WtoOrHardSwitching) /**< \brief Two level turn on level */
#define CFG_DBHPDSENSE_DRIVER_TWO_LEVEL_TURN_OFF_DELAY   (0)                                                          /**< \brief Two level turn off delay */
#define CFG_DBHPDSENSE_DRIVER_TWO_LEVEL_TURN_OFF_LEVEL   (Ifx1edi2010as_GateRegularTtoffPlateauLevel_gpof5)               /**< \brief Two level turn off level */
#define CFG_DBHPDSENSE_DRIVER_TWO_LEVEL_VBE_COMPENSATION (Ifx1edi2010as_VbeCompensationEnable_enabled)                      /**< \brief Two level turn VBE compensation */

#endif /* DBHPDSENSE_CONFIGURATION_H_ */
