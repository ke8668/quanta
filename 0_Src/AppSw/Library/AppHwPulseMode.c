/**
 * \file AppHwPulseMode.c
 * \brief Hardware pulse mode
 *
 * \copyright Copyright (c) 2015 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * This file may be used, copied, and distributed, with or without modification, provided
 * that all copyright notices are retained; that all modifications to this file are
 * prominently noted in the modified file; and that this paragraph is not modified.
 */

#include  "Library/AppHwPulseMode.h"

#include "Common/Main.h"
AppHwPulseMode g_appHwPulseMode; /**< \brief General system information \ingroup main_sourceCodeGlobals */



void AppHwPulseMode_Background(void)
{
}


void AppHwPulseMode_sequenceInit(void)
{
	uint32 i;

	if (g_appHwPulseMode.sequenceState.nextPulseCountDownReload == 0)
	{
		g_appHwPulseMode.sequenceState.nextPulseCountDownReload = 1;
	}

	g_appHwPulseMode.sequenceState.nextPulseCountDownReload = g_appHwPulseMode.sequenceParameter.pulsePeriod / IfxStdIf_Inverter_getPeriod(g_appHwPulseMode.stdIfInverter);

	g_appHwPulseMode.sequenceState.nextPulseCountDownReload = __maxX(g_appHwPulseMode.sequenceState.nextPulseCountDownReload, 1);
	g_appHwPulseMode.sequenceState.nextPulseCountDown = g_appHwPulseMode.sequenceState.nextPulseCountDownReload;

	g_appHwPulseMode.sequenceState.phaseIndex = 0;
	g_appHwPulseMode.sequenceState.pulseIndex = 0;
	g_appHwPulseMode.sequenceState.started = FALSE;
	g_appHwPulseMode.sequenceState.finished = FALSE;

	for (i=0;i<6;i++)
	{
		g_appHwPulseMode.sequenceState.pulseOn[i] = i < 3 ? 0 : g_appHwPulseMode.sequenceParameter.pwmPeriod;
		g_appHwPulseMode.sequenceState.pulseOffset[i] = 0;
	}
}

void AppHwPulseMode_sequenceEnd(void)
{
	g_appHwPulseMode.sequenceState.nextPulseCountDown = 1;
	g_appHwPulseMode.sequenceState.pulseIndex = g_appHwPulseMode.sequenceParameter.pulseCountPerPhase;
}
void AppHwPulseMode_sequenceNext(void)
{
	uint32 i;
	boolean pulse = FALSE;


	g_appHwPulseMode.sequenceState.nextPulseCountDown--;
	if (g_appHwPulseMode.sequenceState.nextPulseCountDown == 0)
	{
		g_appHwPulseMode.sequenceState.nextPulseCountDown = g_appHwPulseMode.sequenceState.nextPulseCountDownReload;

		if (g_appHwPulseMode.sequenceState.pulseIndex >= g_appHwPulseMode.sequenceParameter.pulseCountPerPhase)
		{
			g_appHwPulseMode.sequenceState.pulseIndex = -1; /* While pulse index is <0 all switch are inactive, this avoid shorts while moving from 1 phase to an other */
			g_appHwPulseMode.sequenceState.phaseIndex++;
			if (g_appHwPulseMode.sequenceState.phaseIndex >= 6)
			{
				g_appHwPulseMode.sequenceState.finished = TRUE;
			}

		}
		if (!g_appHwPulseMode.sequenceState.finished)
		{
			pulse = TRUE;
		}
	}

	if (!g_appHwPulseMode.sequenceState.finished)
	{
		uint8 i;
		if (g_appHwPulseMode.sequenceState.pulseIndex < 0)
		{
			for (i=0;i<6;i++)
			{
				g_appHwPulseMode.sequenceState.pulseOn[i] = i < 3 ? 0 : g_appHwPulseMode.sequenceParameter.pwmPeriod;
				g_appHwPulseMode.sequenceState.pulseOffset[i] = 0;
			}
		}
		else
		{
			for (i=0;i<6;i++)
			{
				if (g_appHwPulseMode.sequenceParameter.sequence[g_appHwPulseMode.sequenceState.phaseIndex][i] == 2)
				{
					if (pulse)
					{
						g_appHwPulseMode.sequenceState.pulseOn[i] = g_appHwPulseMode.sequenceParameter.startPulse + g_appHwPulseMode.sequenceParameter.pulseIncrement * g_appHwPulseMode.sequenceState.pulseIndex;
						if (g_appHwPulseMode.sequenceState.pulseOn[i] > g_appHwPulseMode.sequenceParameter.pwmPeriod)
						{
							g_appHwPulseMode.sequenceState.pulseOn[i] = g_appHwPulseMode.sequenceParameter.pwmPeriod;
						}

					}
					else
					{
						g_appHwPulseMode.sequenceState.pulseOn[i] = 0;
					}

				}
				else if (g_appHwPulseMode.sequenceParameter.sequence[g_appHwPulseMode.sequenceState.phaseIndex][i] == 1)
				{
					g_appHwPulseMode.sequenceState.pulseOn[i] = g_appHwPulseMode.sequenceParameter.pwmPeriod;
				}
				else
				{
					g_appHwPulseMode.sequenceState.pulseOn[i] = 0;
				}

				g_appHwPulseMode.sequenceState.pulseOn[i] = i < 3 ? g_appHwPulseMode.sequenceState.pulseOn[i] : g_appHwPulseMode.sequenceParameter.pwmPeriod - g_appHwPulseMode.sequenceState.pulseOn[i];
			}
		}
		if (pulse)
		{
			g_appHwPulseMode.sequenceState.pulseIndex++;
		}
	}
	else
	{
		for (i=0;i<6;i++)
		{
			g_appHwPulseMode.sequenceState.pulseOn[i] = i < 3 ? 0 : g_appHwPulseMode.sequenceParameter.pwmPeriod;
			g_appHwPulseMode.sequenceState.pulseOffset[i] = 0;
		}

	}
}


void AppHwPulseMode_OneMs(void)
{
	boolean runRequest;

	runRequest = AppLbStdIf_getDigitalInput(&g_Lb.stdIf, 3);
    ECU_InverterStatus status = ECU_getInverterStatus(0);


    /* Wait for a start pulse of at least 100ms */
    if (runRequest != g_appHwPulseMode.control.lastRunRequest)
    {
    	g_appHwPulseMode.control.lastRunRequest = runRequest;
    	g_appHwPulseMode.control.runRequestStateCount = 0;
    }
    else if (g_appHwPulseMode.control.runRequestStateCount >= 100)
    {
    	if (!g_appHwPulseMode.control.running && !status.faultMain)
    	{
    		AppHwPulseMode_sequenceInit();
    		AppLbStdIf_setDigitalOutput(&g_Lb.stdIf, 3, TRUE);
    		g_appHwPulseMode.control.running = TRUE;
    	}
    }
    else if (runRequest  && !g_appHwPulseMode.control.running)
    {
    	g_appHwPulseMode.control.runRequestStateCount++;
    }

    if (g_appHwPulseMode.control.running && g_appHwPulseMode.sequenceState.finished)
    {
		g_appHwPulseMode.control.running = FALSE;
		AppLbStdIf_setDigitalOutput(&g_Lb.stdIf, 3, FALSE);
    }



    {   /* Update UI */


        g_App.can.outbox.mode            = 7; /* FIXME use dedicated enum value ?  */
        g_App.can.outbox.error           = status.faultMain;
        g_App.can.outbox.run             = g_appHwPulseMode.control.running;
        g_App.can.outbox.tempBoardMeas   = status.tempBoard;
        g_App.can.outbox.tempIgbtMeas[0] = status.igbtTemp[0];
        g_App.can.outbox.tempIgbtMeas[1] = status.igbtTemp[1];
        g_App.can.outbox.tempIgbtMeas[2] = status.igbtTemp[2];
        g_App.can.outbox.voltageDcMeas   = ECU_getInverterVdc(0);
    }

}

void AppHwPulseMode_TenMs(void)
{

    if (g_appHwPulseMode.control.error == 2)
    {
		if (IfxStdIf_Inverter_clearFlags(g_appHwPulseMode.stdIfInverter))
		{
	    	g_appHwPulseMode.control.error = 1;
		}
    }
    else if (g_appHwPulseMode.control.error == 1)
	{
    	AppHwPulseMode_sequenceEnd();
    	g_appHwPulseMode.control.error = 0;
	}

}


void AppHwPulseMode_EndOfPhaseCurrentConversion(void)
{
    IfxStdIf_Inverter_Status status = IfxStdIf_Inverter_getStatus(g_appHwPulseMode.stdIfInverter);

    /* if error is set, it never get out of the error loop. Seems to be set again from status.error */
    if (g_appHwPulseMode.control.error > 0)
    {
        if (g_appHwPulseMode.control.error == 3)
        {  /* Make sure the PWM is properly switched off */
        	g_appHwPulseMode.control.error = 2;
        }
        else
        {/* Do nothing here */

        }
    }
    else if (status.error)
    {
		ECU_setMotorPwmEnabled(0, Ifx_Pwm_Mode_off);
    	g_appHwPulseMode.control.error = 3;
    }
    else if (g_appHwPulseMode.control.running)
	{

		if (!g_appHwPulseMode.sequenceState.started)
		{
			g_appHwPulseMode.sequenceState.started = TRUE;

			IfxStdIf_Inverter_setPulse(g_appHwPulseMode.stdIfInverter, &g_appHwPulseMode.sequenceState.pulseOn[0], &g_appHwPulseMode.sequenceState.pulseOffset[0]);
		}
		else if (!g_appHwPulseMode.sequenceState.finished)
		{
			AppHwPulseMode_sequenceNext();
			if (g_appHwPulseMode.sequenceState.finished)
			{
				ECU_setMotorPwmEnabled(0, Ifx_Pwm_Mode_off);
			}
			else
			{
				IfxStdIf_Inverter_setPulse(g_appHwPulseMode.stdIfInverter, &g_appHwPulseMode.sequenceState.pulseOn[0], &g_appHwPulseMode.sequenceState.pulseOffset[0]);
			}
		}
	}

}

boolean AppHwPulseMode_Init(void)
{
	g_appHwPulseMode.stdIfInverter = g_Lb.stdIf.inverter;

	g_appHwPulseMode.control.runRequestStateCount = 0;
	g_appHwPulseMode.control.running = FALSE;
	g_appHwPulseMode.control.error = FALSE;
	g_appHwPulseMode.control.lastRunRequest = AppLbStdIf_getDigitalInput(&g_Lb.stdIf, 3);

	g_appHwPulseMode.sequenceParameter.pulsePeriod = g_Lb.stdIf.configuration->specialMode.hwPulseMode.pulsePeriod;
	g_appHwPulseMode.sequenceParameter.startPulse = g_Lb.stdIf.configuration->specialMode.hwPulseMode.startPulse;
	g_appHwPulseMode.sequenceParameter.pulseIncrement = g_Lb.stdIf.configuration->specialMode.hwPulseMode.pulseIncrement;
	g_appHwPulseMode.sequenceParameter.pulseCountPerPhase = g_Lb.stdIf.configuration->specialMode.hwPulseMode.pulseCountPerPhase;

	g_appHwPulseMode.sequenceParameter.pwmPeriod = IfxStdIf_Inverter_getPeriod(g_appHwPulseMode.stdIfInverter);

	uint8 i,j;
	for (i=0;i<6;i++)
	{
		for (j=0;j<6;j++)
		{
			g_appHwPulseMode.sequenceParameter.sequence[i][j] = g_Lb.stdIf.configuration->specialMode.hwPulseMode.sequence[i][j];
		}
	}

	AppHwPulseMode_sequenceInit();


    return TRUE;

}

