/**
 * \file AppLibrary.c
 * \brief Motor control block
 *
 * \copyright Copyright (c) 2015 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * This file may be used, copied, and distributed, with or without modification, provided
 * that all copyright notices are retained; that all modifications to this file are
 * prominently noted in the modified file; and that this paragraph is not modified.
 */

#include "Common/AppInterface.h"
#include "Library/AppLibrary.h"
#include "Common/Main.h"
#include "Common/AppCan.h"
#include "SysSe/EMotor/Ifx_SvmF32.h"

#if CFG_SETUP == CFG_SETUP_LIBRARY
AppLibrary g_appLibrary; /**< \brief General system information \ingroup main_sourceCodeGlobals */

void ECU_slotBackground(void)
{
    Ifx_MotorControlF32_backgroundTask(&g_appLibrary.motorControl);
}


void ECU_slotOneMs(void)
{
	if (!g_Lb.stdIf.configuration->wizard.enabled)
    {   /* Update according to UI */
        Ifx_MotorControl_Mode mode;
        mode = Ifx_MotorControlF32_getMode(&g_appLibrary.motorControl);


        if (!Ifx_MotorControlF32_isRunning(&g_appLibrary.motorControl))
        {   /* Allow to change mode */
            Ifx_MotorControlF32_setMode(&g_appLibrary.motorControl, g_App.can.inbox.modeCmd);
#if 0
            /* 20200224 Jimmy Created */
            if(mode == Ifx_MotorControl_Mode_off)
            	g_App.can.inbox.runCmd = 0;
            else
            	g_App.can.inbox.runCmd = 1;
#endif
        }

        if (Ifx_MotorControlF32_isRunning(&g_appLibrary.motorControl))
        {   /* Set command values */
            if (g_App.can.inbox.runCmd)
            {
                switch (mode)
                {
                case Ifx_MotorControl_Mode_passive:
                    break;
                case Ifx_MotorControl_Mode_currentControl:
                {
                	cfloat32 current;
                	current.real = g_App.can.inbox.currentIdCmd;
                	current.imag = g_App.can.inbox.currentIqCmd;
                    Ifx_MotorControlF32_setCurrent(&g_appLibrary.motorControl, &current);

                }
                    break;
                case Ifx_MotorControl_Mode_torqueControl:
                    Ifx_MotorControlF32_setTorque(&g_appLibrary.motorControl, g_App.can.inbox.torqueCmd);
                    break;
                case Ifx_MotorControl_Mode_speedControl:
                    Ifx_MotorControlF32_setSpeed(&g_appLibrary.motorControl, g_App.can.inbox.speedCmd);
                    break;
                case Ifx_MotorControl_Mode_openLoop:

                    if (g_App.can.inbox.pulseMode)
                    {}
                    else
                    {
                        Ifx_MotorControlF32_setOpenLoopAmplitude(&g_appLibrary.motorControl, g_App.can.inbox.amplitudeCmd / 100.0);
                        Ifx_MotorControlF32_setSpeed(&g_appLibrary.motorControl, g_App.can.inbox.speedCmd);
                    }

                    break;
                case Ifx_MotorControl_Mode_openWithPosition:
                    Ifx_MotorControlF32_setOpenLoopAmplitude(&g_appLibrary.motorControl, g_App.can.inbox.amplitudeCmd / 100.0);
                    break;
                case Ifx_MotorControl_Mode_off:
                	g_App.can.inbox.runCmd = 0;
                    break;
                }
            }
            else
            {
                Ifx_MotorControlF32_stop(&g_appLibrary.motorControl);
            }
        }
        else
        {
            if (g_App.can.inbox.runCmd)
            {
                Ifx_MotorControlF32_start(&g_appLibrary.motorControl);
            }
            else
            {
                if (g_App.can.inbox.modeCmd == Ifx_MotorControl_Mode_passive)
                {
                    Ifx_MotorControlF32_setMode(&g_appLibrary.motorControl, Ifx_MotorControl_Mode_passive);
                }
                else
                {}
            }
        }
    }
	else
	{ /* Wizard mode */

	}

    Ifx_MotorControlF32_speedStep(&g_appLibrary.motorControl);
   {   /* Update UI */
#if  BYPASSDRVBD
#else if
    	ECU_InverterStatus status = ECU_getInverterStatus(0);

		if (status.faultMain)
		{   /* FIXME move this to ECU_slotEndOfPhaseCurrentConversion() for better reaction time, use Ifx_MotorControlF32_getMode() == Ifx_MotorControl_Mode_error (to be implemented)*/
			g_App.hwState.hwModeRequest = AppMode_error;
		}
#endif
		g_App.can.outbox.mode            = Ifx_MotorControlF32_getMode(&g_appLibrary.motorControl);
		g_App.can.outbox.run             = Ifx_MotorControlF32_isRunning(&g_appLibrary.motorControl);
		//g_App.can.outbox.speedMeas       = Ifx_MotorControlF32_getSpeed(&g_appLibrary.motorControl);
		g_App.can.outbox.speedMeas       = Ifx_MotorControlF32_getSensorSpeed(&g_appLibrary.motorControl);
		g_App.can.outbox.torqueMeas      = Ifx_MotorControlF32_getTorque(&g_appLibrary.motorControl);
		//g_App.can.outbox.mechPos   = Ifx_MotorControlF32_getPosition(&g_appLibrary.motorControl); /* This will be virtual sensor in open loop mode */
		g_App.can.outbox.mechPos   = Ifx_MotorControlF32_getSensorPosition(&g_appLibrary.motorControl);
		g_App.can.outbox.elecPos   = 0; /* FIXME todo*/
#if  BYPASSDRVBD
#else if
		/* 20200221 be created for fault alarms */
		g_App.can.outbox.fault = (uint32)(status & 0x1FFFFF);	/* 21-bits for error flag */
		/* 20200221 end of created*/
#endif
	}
 }


void ECU_slotShutdown(void)
{
    Ifx_MotorControlF32_stop(&g_appLibrary.motorControl);
}
void ECU_slotTenMs(void)
{}

void ECU_slotPwmPeriodStart(ECU_MotorIndex motorIndex)
{}

void ECU_slotTriggerPoint(ECU_MotorIndex motorIndex)
{}

void ECU_slotEndOfPhaseCurrentConversion(ECU_MotorIndex motorIndex)
{
    Ifx_MotorControl_Mode mode;
    uint32 temp;

    ECU_getMotorPhaseCurrents(0, g_appLibrary.AppState.currents);
    g_appLibrary.AppState.vDc      = ECU_getInverterVdc(motorIndex);
    g_appLibrary.AppState.status   = ECU_getInverterStatus(motorIndex); /* FIXME optimization: is it required to update at that rate ?*/
    g_appLibrary.AppState.position = ECU_getMotorMechanicalPosition(motorIndex);
    g_appLibrary.AppState.speed    = ECU_getMotorMechanicalSpeed(motorIndex);

    mode                           = Ifx_MotorControlF32_getMode(&g_appLibrary.motorControl);

    IfxStdIf_Inverter_Status status = IfxStdIf_Inverter_getStatus(g_appLibrary.motorControl.inverter);

    if ((mode == Ifx_MotorControl_Mode_openLoop) && (g_App.can.inbox.pulseMode) && (status.error == 0))
    {
        uint32  i;
        float32 pulseOffset[6];
        float32 pulseOn[6];

        for (i = 0; i < 6; i++)
        {
            pulseOn[i]     = g_App.can.inbox.pulseOn[i] * 1e-6;
            pulseOffset[i] = g_App.can.inbox.pulseOffset[i] * 1e-6;
        }

        IfxStdIf_Inverter_setPulse(g_appLibrary.motorControl.inverter, &pulseOn[0], &pulseOffset[0]);
    }
    else
    {
        Ifx_MotorControlF32_execute(&g_appLibrary.motorControl);
    }
}


typedef enum
{
    App_OsciSignal_current0U,
    App_OsciSignal_current0V,
    App_OsciSignal_current0W,
    App_OsciSignal_Vdc,
    App_OsciSignal_Position,
    App_OsciSignal_Speed,
    App_OsciSignal_ElectricalAngle,
    App_OsciSignal_pwmU,
    App_OsciSignal_pwmV,
    App_OsciSignal_pwmW,
    App_OsciSignal_pwmUFoc,
    App_OsciSignal_pwmVFoc,
    App_OsciSignal_pwmWFoc,
    App_OsciSignal_igbtTempU,
    App_OsciSignal_igbtTempV,
    App_OsciSignal_igbtTempW,

    App_OsciSignal_FocPiIdIk,
    App_OsciSignal_FocPiIdIu,
    App_OsciSignal_FocPiIqIk,
    App_OsciSignal_FocPiIqIu,
    App_OsciSignal_FocNfoIabReal,
    App_OsciSignal_FocNfoIabImag,
    App_OsciSignal_FocNfoMabReal,
    App_OsciSignal_FocNfoMabImag,
    App_OsciSignal_FocNfoMdqReal,
    App_OsciSignal_FocNfoMdqImag,
    App_OsciSignal_FocNfoPhaseCurrentsU,
    App_OsciSignal_FocNfoPhaseCurrentsV,
    App_OsciSignal_FocNfoPhaseCurrentsW,

    App_OsciSignal_SpeedOut,

    App_OsciSignal_TorqueOut,
    App_OsciSignal_RdcCosIn,
    App_OsciSignal_RdcSinIn,
    App_OsciSignal_RdcAngleErr,
    App_OsciSignal_current1U,
    App_OsciSignal_current1V,
    App_OsciSignal_current1W,
}App_OsciSignal;

const Ifx_Osci_Signal_Config appTest_OsciSignalConfig[] = {
    {App_OsciSignal_current0U,            {&g_appLibrary.AppState.currents[0],                     Ifx_Osci_DataType_Float32, 0}, "Current M0 U (A)"              },
    {App_OsciSignal_current0V,            {&g_appLibrary.AppState.currents[1],                     Ifx_Osci_DataType_Float32, 0}, "Current M0 V (A)"              },
    {App_OsciSignal_current0W,            {&g_appLibrary.AppState.currents[2],                     Ifx_Osci_DataType_Float32, 0}, "Current M0 W (A)"              },
    {App_OsciSignal_Vdc,                  {&g_appLibrary.AppState.vDc,                             Ifx_Osci_DataType_Float32, 0}, "Vdc (V)"                    },
    {App_OsciSignal_Position,             {&g_appLibrary.AppState.position,                        Ifx_Osci_DataType_Float32, 0}, "Mechanical position (rad)"  },
    {App_OsciSignal_Speed,                {&g_appLibrary.AppState.speed,                           Ifx_Osci_DataType_Float32, 0}, "Speed (rad/s)"              },
    {App_OsciSignal_SpeedOut,             {&g_appLibrary.motorControl.velocity.pic.uk,             Ifx_Osci_DataType_Float32, 0}, "Speed out (MC)"             },

    {App_OsciSignal_TorqueOut,            {&g_appLibrary.motorModel.pmsm.mtpa.imag,                Ifx_Osci_DataType_Float32, 0}, "Torque out (MC)"            },
    {App_OsciSignal_ElectricalAngle,      {&g_appLibrary.motorModel.pmsm.foc.elAngle.input,        Ifx_Osci_DataType_SInt16,  0}, "Electrical angle (FxpAngle)"},
    {App_OsciSignal_FocPiIdIk,            {&g_appLibrary.motorModel.pmsm.foc.piD.ik,               Ifx_Osci_DataType_Float32, 0}, "FOC PI Id ik"               },
    {App_OsciSignal_FocPiIdIu,            {&g_appLibrary.motorModel.pmsm.foc.piD.uk,               Ifx_Osci_DataType_Float32, 0}, "FOC PI Id uk"               },
    {App_OsciSignal_FocPiIqIk,            {&g_appLibrary.motorModel.pmsm.foc.piQ.ik,               Ifx_Osci_DataType_Float32, 0}, "FOC PI Iq ik"               },
    {App_OsciSignal_FocPiIqIu,            {&g_appLibrary.motorModel.pmsm.foc.piQ.uk,               Ifx_Osci_DataType_Float32, 0}, "FOC PI Iq uk"               },
    {App_OsciSignal_FocNfoMdqReal,        {&g_appLibrary.motorModel.pmsm.foc.idq.real,             Ifx_Osci_DataType_Float32, 0}, "FOC Mdq real"               },
    {App_OsciSignal_FocNfoMdqImag,        {&g_appLibrary.motorModel.pmsm.foc.idq.imag,             Ifx_Osci_DataType_Float32, 0}, "FOC Mdq imag"               },
    {App_OsciSignal_igbtTempU,        	  {&g_appLibrary.AppState.status.igbtTemp[0],              Ifx_Osci_DataType_Float32, 0}, "IGBT temp U (�C)"           },
    {App_OsciSignal_igbtTempV,        	  {&g_appLibrary.AppState.status.igbtTemp[1],              Ifx_Osci_DataType_Float32, 0}, "IGBT temp V (�C)"           },
    {App_OsciSignal_igbtTempW,        	  {&g_appLibrary.AppState.status.igbtTemp[2],              Ifx_Osci_DataType_Float32, 0}, "IGBT temp W (�C)"           },

#if CFG_CURRENT_SENSOR_TEST == 1
    {App_OsciSignal_current1U,          {&g_Lb.logicboards.logicBoard_30.analogInput.currents[ECU_MOTOR_INDEX_1][0].input,                     Ifx_Osci_DataType_Float32, 0}, "Current M1 U (A)"              },
    {App_OsciSignal_current1V,          {&g_Lb.logicboards.logicBoard_30.analogInput.currents[ECU_MOTOR_INDEX_1][1].input,                     Ifx_Osci_DataType_Float32, 0}, "Current M1 V (A)"              },
    {App_OsciSignal_current1W,          {&g_Lb.logicboards.logicBoard_30.analogInput.currents[ECU_MOTOR_INDEX_1][2].input,                     Ifx_Osci_DataType_Float32, 0}, "Current M1 W (A)"              },
#endif

#if 0
    /* Only enable if logic board version is 3.x */
    {App_OsciSignal_RdcCosIn,             {&g_Lb.logicboards.logicBoard_30.driver.dsadcRdc[0].cosIn, Ifx_Osci_DataType_SInt16,  0}, "RDC Cos IN"                 },
    {App_OsciSignal_RdcSinIn,             {&g_Lb.logicboards.logicBoard_30.driver.dsadcRdc[0].sinIn, Ifx_Osci_DataType_SInt16,  0}, "RDC Sin IN"                 },
    {App_OsciSignal_RdcAngleErr,          {&g_Lb.logicboards.logicBoard_30.driver.dsadcRdc[0].angleTrk.angleErr, Ifx_Osci_DataType_Float32,  0}, "RDC angle error"                 },
#endif
#if IFX_FOCF32_DEBUG
    {App_OsciSignal_FocNfoIabReal,        {&g_appLibrary.motorModel.pmsm.foc.iab.real,             Ifx_Osci_DataType_Float32, 0}, "FOC Iab real"               },
    {App_OsciSignal_FocNfoIabImag,        {&g_appLibrary.motorModel.pmsm.foc.iab.imag,             Ifx_Osci_DataType_Float32, 0}, "FOC Iab imag"               },
    {App_OsciSignal_FocNfoMabReal,        {&g_appLibrary.motorModel.pmsm.foc.mab.real,             Ifx_Osci_DataType_Float32, 0}, "FOC Mab real (MC)"          },
    {App_OsciSignal_FocNfoMabImag,        {&g_appLibrary.motorModel.pmsm.foc.mab.imag,             Ifx_Osci_DataType_Float32, 0}, "FOC Mab imag (MC)"          },
    {App_OsciSignal_FocNfoPhaseCurrentsU, {&g_appLibrary.motorModel.pmsm.foc.iuvw[0],              Ifx_Osci_DataType_Float32, 0}, "FOC U"                      },
    {App_OsciSignal_FocNfoPhaseCurrentsV, {&g_appLibrary.motorModel.pmsm.foc.iuvw[1],              Ifx_Osci_DataType_Float32, 0}, "FOC V"                      },
    {App_OsciSignal_FocNfoPhaseCurrentsW, {&g_appLibrary.motorModel.pmsm.foc.iuvw[2],              Ifx_Osci_DataType_Float32, 0}, "FOC W"                      },
#endif
    IFX_OSCI_SIGNAL_CONFIG_TABLE_TERMINATOR
};

boolean ECU_slotInit(void)
{
    Ifx_MotorModelConfigF32_File motorConfiguration;
    Ifx_SvmF32_init();
    float32                       vdcNom;
    float32                       vdcMaxGen;
    AppDbStdIf_MainConfig *driverBoardConfig = AppDbStdIf_getMainConfig(&g_Lb.driverBoardStdif);

    g_appLibrary.stdIfInverter = g_Lb.stdIf.inverter;

    vdcNom    = driverBoardConfig->vdcNom;
    vdcMaxGen = driverBoardConfig->vdcMaxGen;

    Ifx_Osci_setSignalsConfig(&g_App.osci, appTest_OsciSignalConfig);
    Ifx_Osci_setChannelSignal(&g_App.osci, 0, App_OsciSignal_current0U);
    Ifx_Osci_setChannelSignal(&g_App.osci, 1, App_OsciSignal_current0V);
    Ifx_Osci_setChannelSignal(&g_App.osci, 2, App_OsciSignal_current0W);
    Ifx_Osci_setChannelSignal(&g_App.osci, 3, App_OsciSignal_Position);
    Ifx_Osci_setChannelSignal(&g_App.osci, 4, App_OsciSignal_Speed);

    Ifx_Osci_setChannelSignal(&g_App.osci, 5, App_OsciSignal_SpeedOut);

    Ifx_Osci_setChannelSignal(&g_App.osci, 6, App_OsciSignal_TorqueOut);
    Ifx_Osci_setChannelSignal(&g_App.osci, 7, App_OsciSignal_TorqueOut);

    Ifx_Osci_setChannelSignal(&g_App.osci, 8, App_OsciSignal_ElectricalAngle);
    Ifx_Osci_setChannelSignal(&g_App.osci, 9, App_OsciSignal_FocPiIdIk);
    Ifx_Osci_setChannelSignal(&g_App.osci, 10, App_OsciSignal_FocPiIqIk);
    Ifx_Osci_setChannelSignal(&g_App.osci, 11, App_OsciSignal_igbtTempU);
    Ifx_Osci_run(&g_App.osci);

    g_App.osci.trigger.mode    = Ifx_Osci_TriggerMode_normal;
    g_App.osci.trigger.source  = 5;
    g_App.osci.trigger.level   = 5000;
    g_App.osci.trigger.enabled = TRUE;

    float32 controlPeriod = IfxStdIf_Inverter_getPeriod(g_appLibrary.stdIfInverter);
    ECU_getMotorConfiguration(0, &motorConfiguration);
{	// Manual setup motor control parameters by Jimmy //
	motorConfiguration.type = Ifx_F32_MotorModel_Type_pmsmStandard;
	controlPeriod = (1.25E-4);
	motorConfiguration.iMax = (5000.0);
	motorConfiguration.polePair = (4);
	vdcMaxGen = (800);
	motorConfiguration.fieldWeakning = 0;
	motorConfiguration.idKp = (0.01);
	motorConfiguration.idKi = (1.0);
	motorConfiguration.iqKp = (0.01);
	motorConfiguration.iqKi = (1.0);
	vdcNom = (400.0);
	motorConfiguration.rs = (0.05);
	motorConfiguration.ld = (7.95E-4);
	motorConfiguration.lq = (7.95E-4);
	motorConfiguration.iStall = (53.0);
	motorConfiguration.kt = (2.09);

	motorConfiguration.speedKp = (0.8);
	motorConfiguration.speedKi = (1.0);
	motorConfiguration.speedMax = (3000.0);
	motorConfiguration.torqueMax = (111.0);
	motorConfiguration.torqueRate = (500.0);
}

    if ((motorConfiguration.type == Ifx_F32_MotorModel_Type_pmsmStandard) || (motorConfiguration.type == Ifx_F32_MotorModel_Type_pmsmSalient))
    {
        Ifx_MotorModelPmsmF32_Config config;
        Ifx_MotorModelPmsmF32_initConfig(&config);
        config.base.controlPeriod        = controlPeriod;
        config.base.currentMax           = motorConfiguration.iMax;
        config.base.polePair             = motorConfiguration.polePair;

        config.voltageGenMax             = vdcMaxGen;
        config.fieldWeakeningEnabled     = motorConfiguration.fieldWeakning;
        config.foc.piD.kp                = motorConfiguration.idKp;
        config.foc.piD.ki                = motorConfiguration.idKi;
        config.foc.piQ.kp                = motorConfiguration.iqKp;
        config.foc.piQ.ki                = motorConfiguration.iqKi;
        config.foc.vdcNom                = vdcNom;
        config.foc.fieldWeakeningEnabled = config.fieldWeakeningEnabled;
        config.rs                        = motorConfiguration.rs;
        config.ld                        = motorConfiguration.ld;
        config.lq                        = motorConfiguration.lq;
        config.iStall                    = motorConfiguration.iStall;
        config.fluxM                     = motorConfiguration.kt * IFX_SQRT_TWO / (3 * motorConfiguration.polePair);
        config.torqueControlEnabled      = TRUE;
        config.feedForwardEnabled        = FALSE;

        Ifx_MotorModelPmsmF32_init(&g_appLibrary.motorModel.pmsm, &config);
        Ifx_MotorModelPmsmF32_stdIfMotorModelInit(&g_appLibrary.stdifMotorModel, &g_appLibrary.motorModel.pmsm);
    }
    else if (motorConfiguration.type == Ifx_F32_MotorModel_Type_acim)
    {
        Ifx_MotorModelAcimF32_Config config;
        Ifx_MotorModelAcimF32_initConfig(&config);
        config.base.controlPeriod        = controlPeriod;
        config.base.currentMax           = motorConfiguration.iMax;
        config.base.polePair             = motorConfiguration.polePair;

        config.voltageGenMax             = vdcMaxGen;
        config.fieldWeakeningEnabled     = motorConfiguration.fieldWeakning;
        config.foc.piD.kp                = motorConfiguration.idKp;
        config.foc.piD.ki                = motorConfiguration.idKi;
        config.foc.piQ.kp                = motorConfiguration.iqKp;
        config.foc.piQ.ki                = motorConfiguration.iqKi;
        config.foc.vdcNom                = vdcNom;
        config.foc.fieldWeakeningEnabled = config.fieldWeakeningEnabled;
        config.rs                        = motorConfiguration.rs;
        config.statorMax                 = motorConfiguration.statorMax;
        config.rr                        = motorConfiguration.rr;
        config.ls                        = motorConfiguration.ls;
        config.lr                        = motorConfiguration.lr;
        config.lm                        = motorConfiguration.lm;
        config.fr                        = motorConfiguration.fr;
        config.iStall                    = motorConfiguration.iStall;
        config.torqueControlEnabled      = TRUE;
        config.feedForwardEnabled        = FALSE;

        Ifx_MotorModelAcimF32_init(&g_appLibrary.motorModel.acim, &config);
        Ifx_MotorModelAcimF32_stdIfMotorModelInit(&g_appLibrary.stdifMotorModel, &g_appLibrary.motorModel.acim);
    }
    else
    {
        /* FIXME to be implemented */
        IFX_ASSERT(IFX_VERBOSE_LEVEL_ERROR, FALSE);

        while (1)
        {}
    }

    {
        Ifx_MotorControlF32_Config config;
        Ifx_MotorControlF32_initConfig(&config);
        config.piSpeed.kp            = motorConfiguration.speedKp;
        config.piSpeed.ki            = motorConfiguration.speedKi;
        config.speedControlPeriod    = 1e-3;
        config.speedMax              = motorConfiguration.speedMax;
        config.torqueMax             = motorConfiguration.torqueMax;
        config.controlPeriod         = controlPeriod;
        config.torqueRate            = motorConfiguration.torqueRate;
        config.motorModel            = &g_appLibrary.stdifMotorModel;
        config.openLoopAmplitudeRate = 1;
        config.inverter              = g_appLibrary.stdIfInverter;
        config.positionSensor        = ECU_getPositionSensorAddress(0, g_Lb.stdIf.configuration->inverter[0].positionSensor[0]);
        config.mode                  = Ifx_MotorControl_Mode_passive;
        Ifx_MotorControlF32_init(&g_appLibrary.motorControl, &config);
    }
    ECU_selectPositionSensor(0, g_Lb.stdIf.configuration->inverter[0].positionSensor[0]);


    return TRUE;
}


#endif
