/**
 * \file AppCanConsole.c
 * \brief
 *
 * \copyright Copyright (c) 2015 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * This file may be used, copied, and distributed, with or without modification, provided
 * that all copyright notices are retained; that all modifications to this file are
 * prominently noted in the modified file; and that this paragraph is not modified.
 */

#include "LogicBoard3_0/App/AppCanConsole.h"
#include "Configuration.h"
#include "string.h"
#include "SysSe/Comm/Ifx_Console.h"

boolean AppCanConsole_canReadCount(AppCanConsole *console, Ifx_SizeT count, Ifx_TickTime timeout)
{
    return Ifx_Fifo_canReadCount(console->rx, count, timeout);
}


boolean AppCanConsole_canWriteCount(AppCanConsole *console, Ifx_SizeT count, Ifx_TickTime timeout)
{
    return Ifx_Fifo_canWriteCount(console->tx, count, timeout);
}


void AppCanConsole_clearRx(AppCanConsole *console)
{
//    IfxAsclin_flushRxFifo(console->console);
    Ifx_Fifo_clear(console->rx);
}


void AppCanConsole_clearTx(AppCanConsole *console)
{
    Ifx_Fifo_clear(console->tx);
//    IfxAsclin_flushTxFifo(console->console);
}


boolean AppCanConsole_flushTx(AppCanConsole *console, Ifx_TickTime timeout)
{
//    Ifx_TickTime deadline = getDeadLine(timeout);
    boolean      result;

    /* Flush the software FIFO */
    result = Ifx_Fifo_flush(console->tx, timeout);

//    if (result)
//    {
        /* Flush the hardware FIFO (wait until all bytes have been transmitted) */
//        do
//        {
//            result = IfxAsclin_getTxFifoFillLevel(console->console) == 0;
//        } while (!result && !isDeadLine(deadline));
//    }

    return result;
}


sint32 AppCanConsole_getReadCount(AppCanConsole *console)
{
    return Ifx_Fifo_readCount(console->rx);
}


IfxStdIf_DPipe_ReadEvent AppCanConsole_getReadEvent(AppCanConsole *console)
{
    return &console->rx->eventWriter;
}


sint32 AppCanConsole_getWriteCount(AppCanConsole *console)
{
    return Ifx_Fifo_writeCount(console->tx);
}


IfxStdIf_DPipe_WriteEvent AppCanConsole_getWriteEvent(AppCanConsole *console)
{
    return &console->tx->eventWriter;
}


boolean AppCanConsole_write(AppCanConsole *console, void *data, Ifx_SizeT *count, Ifx_TickTime timeout)
{
    Ifx_SizeT left;
    boolean   result = TRUE;

    if (*count != 0)
    {
        left = Ifx_Fifo_write(console->tx, data, *count, timeout);
        //AppCanConsole_initiateTransmission(console);

        *count -= left;
        result  = left == 0;
    }

    return result;
}


boolean AppCanConsole_read(AppCanConsole *console, void *data, Ifx_SizeT *count, Ifx_TickTime timeout)
{
    Ifx_SizeT left = Ifx_Fifo_read(console->rx, data, *count, timeout);

    *count -= left;

    return left == 0;
}


void AppCanConsole_init(AppCanConsole *console, boolean primaryConsole)
{   /* CAN console */
    console->rx = Ifx_Fifo_create(CFG_CAN_CONSOLE_RX_BUFFER_SIZE, 1);
    console->tx = Ifx_Fifo_create(CFG_CAN_CONSOLE_TX_BUFFER_SIZE + CFG_CONSOLE_TX_BUFFER_SIZE/*(primaryConsole ? CFG_CONSOLE_TX_BUFFER_SIZE : 0)*/, 1);

    {   /* STDIF */
        IfxStdIf_DPipe *stdif;
        stdif = &console->stdIf;
        memset(stdif, 0, sizeof(IfxStdIf_DPipe));

        /* Set the API link */
        stdif->driver         = console;
        stdif->write          = (IfxStdIf_DPipe_Write) & AppCanConsole_write;
        stdif->read           = (IfxStdIf_DPipe_Read) & AppCanConsole_read;
        stdif->getReadCount   = (IfxStdIf_DPipe_GetReadCount) & AppCanConsole_getReadCount;
        stdif->getReadEvent   = (IfxStdIf_DPipe_GetReadEvent) & AppCanConsole_getReadEvent;
        stdif->getWriteCount  = (IfxStdIf_DPipe_GetWriteCount) & AppCanConsole_getWriteCount;
        stdif->getWriteEvent  = (IfxStdIf_DPipe_GetWriteEvent) & AppCanConsole_getWriteEvent;
        stdif->canReadCount   = (IfxStdIf_DPipe_CanReadCount) & AppCanConsole_canReadCount;
        stdif->canWriteCount  = (IfxStdIf_DPipe_CanWriteCount) & AppCanConsole_canWriteCount;
        stdif->flushTx        = (IfxStdIf_DPipe_FlushTx) & AppCanConsole_flushTx;
        stdif->clearTx        = (IfxStdIf_DPipe_ClearTx) & AppCanConsole_clearTx;
        stdif->clearRx        = (IfxStdIf_DPipe_ClearRx) & AppCanConsole_clearRx;
        stdif->onReceive      = (IfxStdIf_DPipe_OnReceive)NULL_PTR;
        stdif->onTransmit     = (IfxStdIf_DPipe_OnTransmit)NULL_PTR;
        stdif->onError        = (IfxStdIf_DPipe_OnError)NULL_PTR;
        stdif->getSendCount   = (IfxStdIf_DPipe_GetSendCount)NULL_PTR;
        stdif->getTxTimeStamp = (IfxStdIf_DPipe_GetTxTimeStamp)NULL_PTR;
        stdif->resetSendCount = (IfxStdIf_DPipe_ResetSendCount)NULL_PTR;
        stdif->txDisabled     = FALSE;
    }
}
