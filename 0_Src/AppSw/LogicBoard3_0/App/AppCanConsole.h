/**
 * \file AppCanConsole.h
 * \brief
 *
 * \copyright Copyright (c) 2015 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * This file may be used, copied, and distributed, with or without modification, provided
 * that all copyright notices are retained; that all modifications to this file are
 * prominently noted in the modified file; and that this paragraph is not modified.
 */

#ifndef LB30_APPCANCONSOLE_H_
#define LB30_APPCANCONSOLE_H_

#include "_Lib/DataHandling/Ifx_Fifo.h"
#include "SysSe/Bsp/Bsp.h"
#include "StdIf/IfxStdIf_DPipe.h"

typedef struct
{
    Ifx_Fifo      *tx;                        /**< \brief Transmit FIFO buffer */
    Ifx_Fifo      *rx;                        /**< \brief Receive FIFO buffer */
    IfxStdIf_DPipe stdIf;
}AppCanConsole;

void AppCanConsole_init(AppCanConsole *console, boolean primaryConsole);

#endif /* LB30_APPCANCONSOLE_H_ */
