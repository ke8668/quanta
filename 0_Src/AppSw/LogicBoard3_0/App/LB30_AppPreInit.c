/**
 * \file LB30_AppPreInit.c
 * \brief Logic board 3.0 initialization
 *
 * \copyright Copyright (c) 2015 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * This file may be used, copied, and distributed, with or without modification, provided
 * that all copyright notices are retained; that all modifications to this file are
 * prominently noted in the modified file; and that this paragraph is not modified.
 */

#include "Configuration.h"
#include "LogicBoard3_0/App/LB30_AppPreInit.h"
#include "SysSe/Comm/Ifx_Console.h"
#include "LogicBoard3_0/App/LB30.h"
#include "LogicBoard3_0/App/LB30_AppInit.h"

boolean LB30_AppPreInit(LB30 *board)
{
    boolean result = TRUE;

//    Ifx_Console_printAlign("****************************"ENDL);
//    Ifx_Console_printAlign("* Board pre-initialization *"ENDL);
//    Ifx_Console_printAlign("****************************"ENDL);
    /* Initialize the TLF35584 */
    result 						   &= LB30_AppInit_Dio(board);	// Dio is used for debugging and mode selection purpose
    result                         &= LB30_AppInit_Qspi2(board);
//    result                         &= LB30_AppInit_smu(board);
    result                         &= LB30_AppInit_tlf35584(board);
    result                         &= AppDbStdIf_init(board->driverBoardStdif, board->driver.qspi0, NULL_PTR);

//    Ifx_Console_printAlign("Board pre-initialization");
//    App_printStatus(result);

    return result;
}
