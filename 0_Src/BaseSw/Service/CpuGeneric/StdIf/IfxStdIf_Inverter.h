/**
 * \file IfxStdIf_Inverter.h
 * \brief Standard interface: Timer
 * \ingroup IfxStdIf
 *
 * \version disabled
 * \copyright Copyright (c) 2013 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 *
 * \defgroup library_srvsw_stdif_inverter Standard interface: Inverter
 * \ingroup library_srvsw_stdif
 *
 * The standard interface inverter (Inverter) abstract the hardware used for inverter feature. It provide, after proper initialization an hardware
 * independant way to interact with the inverter functionality like setting duty cycles, deadtime, ...
 *
 */

#ifndef IFXSTDIF_INVERTER_H
#define IFXSTDIF_INVERTER_H 1

#include "Cpu/Std/Ifx_Types.h"
#include "IfxStdIf.h"
#include "SysSe/Math/IFX_Cf32.h"

/** \brief Timer increment direction */
typedef enum
{
    IfxStdIf_Inverter_CountDir_up,                      /**< \brief Timer is counting up */
    IfxStdIf_Inverter_CountDir_upAndDown,               /**< \brief Timer is counting up and down */
    IfxStdIf_Inverter_CountDir_down                     /**< \brief Timer is counting down */
} IfxStdIf_Inverter_CountDir;

/** \brief Motor status
 *
 */
typedef struct
{
    uint32 error : 1;                                 /**<\brief  Inverter main fault, 1 if any fault */
    uint32 faultInput : 1;							  /**<\brief  Inverter input fault */
    uint32 faultLimit : 1;                            /**<\brief  Inverter limit fault */
    uint32 faultIom : 1;                              /**<\brief  Inverter IOM fault */
    uint32 faultGDEN : 1;							  /* Jimmy create Gate Driver En status */
    uint32 faultPBEN : 1;							  /* Jimmy created for Power board enable pin */
    uint32 reserver_1 : 3;							  /* Jimmy Change to 3 from 4 */

    uint32 phase1TopDriverError : 1;                  /**<\brief  Phase 1 top driver error. 0: no fault, 1: Fault */
    uint32 phase1BottomDriverError : 1;               /**<\brief  Phase 1 bottom driver error */
    uint32 phase1SwitchTempError : 1;                 /**<\brief  Switch temperature error */
    uint32 phase1CurrentSensorError : 1;              /**<\brief  Current sensor 1 error  */
    uint32 phase1Overcurrent : 1;                     /**<\brief  Phase 1 overcurrent error. 0: no fault, 1: Fault */
    uint32 reserved_C : 3;

    uint32 phase2TopDriverError : 1;                  /**<\brief  Phase 2 top driver error */
    uint32 phase2BottomDriverError : 1;               /**<\brief  Phase 2 bottom driver error */
    uint32 phase2SwitchTempError : 1;                 /**<\brief  Phase 2 Switch temperature error */
    uint32 phase2CurrentSensorError : 1;              /**<\brief  Phase 2 Current sensor 1 error  */
    uint32 phase2Overcurrent : 1;                     /**<\brief  Phase 2 overcurrent error. 0: no fault, 1: Fault */
    uint32 reserved_14 : 3;

    uint32 phase3TopDriverError : 1;                  /**<\brief  Phase 3 top driver error */
    uint32 phase3BottomDriverError : 1;               /**<\brief  Phase 3 bottom driver error */
    uint32 phase3SwitchTempError : 1;                 /**<\brief  Phase 3 Switch temperature error */
    uint32 phase3CurrentSensorError : 1;              /**<\brief  Phase 3 Current sensor 1 error  */
    uint32 phase3Overcurrent : 1;                     /**<\brief  Phase 3 overcurrent error. 0: no fault, 1: Fault */
    uint32 reserved_1C : 3;
} IfxStdIf_Inverter_Status;

/** \brief Forward declaration */
typedef struct IfxStdIf_Inverter_ IfxStdIf_Inverter;

/** \brief Clear the flags
 * \param driver Pointer to the interface driver object
 * \return return TRUE in case all flags could be cleared else FALSE
 */
typedef boolean                     (*IfxStdIf_Inverter_ClearFlags)(IfxStdIf_InterfaceDriver driver);
/** \brief Disable the inverter
 * \param driver Pointer to the interface driver object
 * \return None
 */
typedef void                     (*IfxStdIf_Inverter_Disable)(IfxStdIf_InterfaceDriver driver);
/** \brief Enable the inverter
 * \param driver Pointer to the interface driver object
 * \return None
 */
typedef void                     (*IfxStdIf_Inverter_Enable)(IfxStdIf_InterfaceDriver driver);
/** \brief Return the dead time
 * \param driver Pointer to the interface driver object
 * \return Return the dead time in seconds
 */
typedef float32                  (*IfxStdIf_Inverter_GetDeadtime)(IfxStdIf_InterfaceDriver driver);
/** \brief Return the minimal allowed pulse
 * \param driver Pointer to the interface driver object
 * \return Return the minimal allowed pulse in seconds
 */
typedef float32                  (*IfxStdIf_Inverter_GetMinPulse)(IfxStdIf_InterfaceDriver driver);
/** \brief Return the period
 * \param driver Pointer to the interface driver object
 * \return Return the period in seconds
 */
typedef float32                  (*IfxStdIf_Inverter_GetPeriod)(IfxStdIf_InterfaceDriver driver);
/** \brief Return the period
 * \param driver Pointer to the interface driver object
 * \return Return the period in ticks
 */
typedef Ifx_TimerValue           (*IfxStdIf_Inverter_GetPeriodTicks)(IfxStdIf_InterfaceDriver driver);
/** \brief Return the phase currents
 * \param driver Pointer to the interface driver object
 * \param currents Phases currents. Point to an array of size equal to the number of phases
 * \return Return the phase currents in A
 */
typedef void                     (*IfxStdIf_Inverter_GetPhaseCurrents)(IfxStdIf_InterfaceDriver driver, float32 *currents);
/** \brief Get the PWM duty cycles
 * \param driver Pointer to the interface driver object
 * \param duty PWM duty cycles. Point to an array of size aqual to the number of phases
 * \return Return None
 */
typedef void                     (*IfxStdIf_Inverter_GetPwm)(IfxStdIf_InterfaceDriver driver, Ifx_TimerValue *duty);
/** \brief Return the inverter status
 * \param driver Pointer to the interface driver object
 * \return Return the status
 */
typedef IfxStdIf_Inverter_Status (*IfxStdIf_Inverter_GetStatus)(IfxStdIf_InterfaceDriver driver);
/** \brief Return the inverter switch temperatures
 * \param driver Pointer to the interface driver object
 * \param temps Switch temperatures. Point to an array of size aqual to the number of phases
 * \return Return the inverter switch temperature in �C
 */
typedef void                     (*IfxStdIf_Inverter_GetSwitchTemps)(IfxStdIf_InterfaceDriver driver, float32 *temps);
/** \brief Return the inverter DC link voltage
 * \param driver Pointer to the interface driver object
 * \return Return the inverter DC link voltage in V
 */
typedef float32                  (*IfxStdIf_Inverter_GetVdc)(IfxStdIf_InterfaceDriver driver);
/** \brief Start the PWM timer
 * \param driver Pointer to the interface driver object
 * \return Return None
 */
typedef void                     (*IfxStdIf_Inverter_StartPwmTimer)(IfxStdIf_InterfaceDriver driver);
/** \brief Set the dead time
 * \param driver Pointer to the interface driver object
 * \param deadtime Dead time in seconds
 * \return Return None
 */
typedef void                     (*IfxStdIf_Inverter_SetDeadtime)(IfxStdIf_InterfaceDriver driver, float32 deadtime);
/** \brief Set the minimal allowed pulse
 * \param driver Pointer to the interface driver object
 * \param minPulse Minimal allowed pulse in seconds
 * \return Return None
 */
typedef void                     (*IfxStdIf_Inverter_SetMinPulse)(IfxStdIf_InterfaceDriver driver, float32 minPulse);
/** \brief Set the PWM duty cycles
 * \param driver Pointer to the interface driver object
 * \param duty PWM duty cycles
 * \return Return None
 */
typedef void                     (*IfxStdIf_Inverter_SetPwm)(IfxStdIf_InterfaceDriver driver, Ifx_TimerValue *duty);
/** \brief Set the pwm pulse of all switched independently
 * \param driver Pointer to the interface driver object
 * \param tOn Pointer to an array of ON times in s. The array size must be equal to the number of PWM channels times 2. Parameters order is  Phase 0 top, phase 1 top, ... phase 0 bottom, phase 1 botteom, ...
 * \param offset Pointer to an array of offset values in s. The array size must be equal to the number of PWM channels times 2. Parameters order is  Phase 0 top, phase 1 top, ... phase 0 bottom, phase 1 botteom, ...
 * \return none
 */
typedef void                     (*IfxStdIf_Inverter_SetPulse)(IfxStdIf_InterfaceDriver driver, float32 *tOn, float32 *offset);

/** \brief Set the PWM to OFF
 * Switch the PWM to the inactive state in the next period
 *
 * \param driver Pointer to the interface driver object
 * \return Return None
 */
typedef void (*IfxStdIf_Inverter_SetPwmOff)(IfxStdIf_InterfaceDriver driver);
/** \brief Set the PWM to ON
 * Enable the PWM in the next period, top channels are set to inactive, and bottom channels to active
 *
 * \param driver Pointer to the interface driver object
 * \param mode Set the PWM mode, if mode is Ifx_Pwm_Mode_off, the PWM is disabled. when disabled, all PWM output are set to inactive
 * \return Return None
 */
typedef void (*IfxStdIf_Inverter_SetPwmOn)(IfxStdIf_InterfaceDriver driver, Ifx_Pwm_Mode mode);
/** \brief set the period
 * \param driver Pointer to the interface driver object
 * \param period in seconds
 * \return Return None
 */
typedef void (*IfxStdIf_Inverter_SetPeriod)(IfxStdIf_InterfaceDriver driver, float32 period);
/** \brief set the period
 * \param driver Pointer to the interface driver object
 * \param period Period in timer ticks
 * \return Return None
 */
typedef void (*IfxStdIf_Inverter_SetPeriodTicks)(IfxStdIf_InterfaceDriver driver, Ifx_TimerValue period);
/** \brief set the output voltage
 * \param driver Pointer to the interface driver object
 * \param period Period in timer ticks
 * \return Return None
 */
typedef void (*IfxStdIf_Inverter_SetVoltage)(IfxStdIf_InterfaceDriver driver, cfloat32 mab);
/** \brief Show the raised messages
 * \param driver Pointer to the interface driver object
 * \return Return None
 */
typedef void (*IfxStdIf_Inverter_ShowMessage)(IfxStdIf_InterfaceDriver driver);

/** \brief Standard interface object
 */
struct IfxStdIf_Inverter_
{
    IfxStdIf_InterfaceDriver           driver;                /**< \brief Interface driver object                  */
    IfxStdIf_Inverter_ClearFlags       clearFlags;            /**< \brief \see IfxStdIf_Inverter_ClearFlags         */
    IfxStdIf_Inverter_Disable          disable;               /**< \brief \see IfxStdIf_Inverter_Disable         */
    IfxStdIf_Inverter_Enable           enable;                /**< \brief \see IfxStdIf_Inverter_Enable         */
    IfxStdIf_Inverter_GetDeadtime      getDeadtime;           /**< \brief \see IfxStdIf_Inverter_GetDeadtime         */
    IfxStdIf_Inverter_GetMinPulse      getMinPulse;           /**< \brief \see IfxStdIf_Inverter_GetMinPulse         */
    IfxStdIf_Inverter_GetPeriod        getPeriod;             /**< \brief \see IfxStdIf_Inverter_GetPeriod     FIXME rename getPeriodSecond to avoid confusion with getPeriodTicks, do this for all API of StdIf???    */
    IfxStdIf_Inverter_GetPeriodTicks   getPeriodTicks;        /**< \brief \see IfxStdIf_Inverter_GetPeriodTicks         */
    IfxStdIf_Inverter_GetPhaseCurrents getPhaseCurrents;      /**< \brief \see IfxStdIf_Inverter_GetPhaseCurrents         */
    IfxStdIf_Inverter_GetPwm           getPwm;                /**< \brief \see IfxStdIf_Inverter_GetPwm         */
    IfxStdIf_Inverter_GetStatus        getStatus;             /**< \brief \see IfxStdIf_Inverter_GetStatus         */
    IfxStdIf_Inverter_GetSwitchTemps   getSwitchTemps;        /**< \brief \see IfxStdIf_Inverter_GetSwitchTemps         */
    IfxStdIf_Inverter_GetVdc           getVdc;                /**< \brief \see IfxStdIf_Inverter_GetVdc         */
    IfxStdIf_Inverter_StartPwmTimer    startPwmTimer;         /**< \brief \see IfxStdIf_Inverter_StartPwmTimer        */
    IfxStdIf_Inverter_SetDeadtime      setDeadtime;           /**< \brief \see IfxStdIf_Inverter_SetDeadtime         */
    IfxStdIf_Inverter_SetMinPulse      setMinPulse;           /**< \brief \see IfxStdIf_Inverter_SetMinPulse         */
    IfxStdIf_Inverter_SetPwm           setPwm;                /**< \brief \see IfxStdIf_Inverter_SetPwm         */
    IfxStdIf_Inverter_SetPulse         setPulse;              /**< \brief \see IfxStdIf_Inverter_SetPulse         */
    IfxStdIf_Inverter_SetPwmOff        setPwmOff;             /**< \brief \see IfxStdIf_Inverter_SetPwmOff         */
    IfxStdIf_Inverter_SetPwmOn         setPwmOn;              /**< \brief \see IfxStdIf_Inverter_SetPwmOn         */
    IfxStdIf_Inverter_SetPeriod        setPeriod;             /**< \brief \see IfxStdIf_Inverter_SetPeriod         */
    IfxStdIf_Inverter_SetPeriodTicks   setPeriodTicks;        /**< \brief \see IfxStdIf_Inverter_SetPeriodTicks         */
    IfxStdIf_Inverter_SetVoltage       setVoltage;            /**< \brief \see IfxStdIf_Inverter_SetVoltage         */
    IfxStdIf_Inverter_ShowMessage      showMessage;           /**< \brief \see IfxStdIf_Inverter_ShowMessage         */
};

/** \brief Timer configuration */
typedef struct
{
    float32 deadtimeComp;               /**< \brief Deadtime compensation value, 0 disables the deadtime conpensation  */
} IfxStdIf_Inverter_Config;

/** \addtogroup library_srvsw_stdif_inverter
 *  \{
 */

/** \copydoc IfxStdIf_Inverter_ClearFlags
 * \param stdif Standard interface pointer
 */
IFX_INLINE boolean IfxStdIf_Inverter_clearFlags(IfxStdIf_Inverter *stdIf)
{
    return stdIf->clearFlags(stdIf->driver);
}


/** \copydoc IfxStdIf_Inverter_Disable
 * \param stdif Standard interface pointer
 */
IFX_INLINE void IfxStdIf_Inverter_disable(IfxStdIf_Inverter *stdIf)
{
    stdIf->disable(stdIf->driver);
}


/** \copydoc IfxStdIf_Inverter_Enable
 * \param stdif Standard interface pointer
 */
IFX_INLINE void IfxStdIf_Inverter_enable(IfxStdIf_Inverter *stdIf)
{
    stdIf->enable(stdIf->driver);
}


/** \copydoc IfxStdIf_Inverter_GetDeadtime
 * \param stdif Standard interface pointer
 */
IFX_INLINE float32 IfxStdIf_Inverter_getDeadtime(IfxStdIf_Inverter *stdIf)
{
    return stdIf->getDeadtime(stdIf->driver);
}


/** \copydoc IfxStdIf_Inverter_GetMinPulse
 * \param stdif Standard interface pointer
 */
IFX_INLINE float32 IfxStdIf_Inverter_getMinPulse(IfxStdIf_Inverter *stdIf)
{
    return stdIf->getMinPulse(stdIf->driver);
}


/** \copydoc IfxStdIf_Inverter_GetPeriod
 * \param stdif Standard interface pointer
 */
IFX_INLINE float32 IfxStdIf_Inverter_getPeriod(IfxStdIf_Inverter *stdIf)
{
    return stdIf->getPeriod(stdIf->driver);
}


/** \copydoc IfxStdIf_Inverter_GetPeriodTicks
 * \param stdif Standard interface pointer
 */
IFX_INLINE Ifx_TimerValue IfxStdIf_Inverter_getPeriodTicks(IfxStdIf_Inverter *stdIf)
{
    return stdIf->getPeriodTicks(stdIf->driver);
}


/** \copydoc IfxStdIf_Inverter_GetPhaseCurrents
 * \param stdif Standard interface pointer
 */
IFX_INLINE void IfxStdIf_Inverter_getPhaseCurrents(IfxStdIf_Inverter *stdIf, float32 *currents)
{
    stdIf->getPhaseCurrents(stdIf->driver, currents);
}


/** \copydoc IfxStdIf_Inverter_GetPwm
 * \param stdif Standard interface pointer
 */
IFX_INLINE void IfxStdIf_Inverter_getPwm(IfxStdIf_Inverter *stdIf, Ifx_TimerValue *duty)
{
    stdIf->getPwm(stdIf->driver, duty);
}


/** \copydoc IfxStdIf_Inverter_GetStatus
 * \param stdif Standard interface pointer
 */
IFX_INLINE IfxStdIf_Inverter_Status IfxStdIf_Inverter_getStatus(IfxStdIf_Inverter *stdIf)
{
    return stdIf->getStatus(stdIf->driver);
}


/** \copydoc IfxStdIf_Inverter_GetSwitchTemps
 * \param stdif Standard interface pointer
 */
IFX_INLINE void IfxStdIf_Inverter_getSwitchTemps(IfxStdIf_Inverter *stdIf, float32 *temps)
{
    stdIf->getSwitchTemps(stdIf->driver, temps);
}


/** \copydoc IfxStdIf_Inverter_GetVdc
 * \param stdif Standard interface pointer
 */
IFX_INLINE float32 IfxStdIf_Inverter_getVdc(IfxStdIf_Inverter *stdIf)
{
    return stdIf->getVdc(stdIf->driver);
}


/** \copydoc IfxStdIf_Inverter_StartPwmTimer
 * \param stdif Standard interface pointer
 */
IFX_INLINE void IfxStdIf_Inverter_startPwmTimer(IfxStdIf_Inverter *stdIf)
{
    stdIf->startPwmTimer(stdIf->driver);
}


/** \copydoc IfxStdIf_Inverter_SetDeadtime
 * \param stdif Standard interface pointer
 */
IFX_INLINE void IfxStdIf_Inverter_setDeadtime(IfxStdIf_Inverter *stdIf, float32 deadtime)
{
    stdIf->setDeadtime(stdIf->driver, deadtime);
}


/** \copydoc IfxStdIf_Inverter_SetMinPulse
 * \param stdif Standard interface pointer
 */
IFX_INLINE void IfxStdIf_Inverter_setMinPulse(IfxStdIf_Inverter *stdIf, float32 minPulse)
{
    stdIf->setMinPulse(stdIf->driver, minPulse);
}


/** \copydoc IfxStdIf_Inverter_SetPwm
 * \param stdif Standard interface pointer
 */
IFX_INLINE void IfxStdIf_Inverter_setPwm(IfxStdIf_Inverter *stdIf, Ifx_TimerValue *duty)
{
    stdIf->setPwm(stdIf->driver, duty);
}


/** \copydoc IfxStdIf_Inverter_SetPulse
 * \param stdif Standard interface pointer
 */
IFX_INLINE void IfxStdIf_Inverter_setPulse(IfxStdIf_Inverter *stdIf, float32 *tOn, float32 *offset)
{
    stdIf->setPulse(stdIf->driver, tOn, offset);
}


/** \copydoc IfxStdIf_Inverter_SetPwmOff
 * \param stdif Standard interface pointer
 */
IFX_INLINE void IfxStdIf_Inverter_setPwmOff(IfxStdIf_Inverter *stdIf)
{
    stdIf->setPwmOff(stdIf->driver);
}


/** \copydoc IfxStdIf_Inverter_SetPwmOn
 * \param stdif Standard interface pointer
 */
IFX_INLINE void IfxStdIf_Inverter_setPwmOn(IfxStdIf_Inverter *stdIf, Ifx_Pwm_Mode mode)
{
    stdIf->setPwmOn(stdIf->driver, mode);
}


/** \copydoc IfxStdIf_Inverter_SetPeriod
 * \param stdif Standard interface pointer
 */
IFX_INLINE void IfxStdIf_Inverter_setPeriod(IfxStdIf_Inverter *stdIf, float32 period)
{
    stdIf->setPeriod(stdIf->driver, period);
}


/** \copydoc IfxStdIf_Inverter_SetPeriodTicks
 * \param stdif Standard interface pointer
 */
IFX_INLINE void IfxStdIf_Inverter_setPeriodTicks(IfxStdIf_Inverter *stdIf, Ifx_TimerValue period)
{
    stdIf->setPeriodTicks(stdIf->driver, period);
}


/** \copydoc IfxStdIf_Inverter_SetVoltages
 * \param stdif Standard interface pointer
 */
IFX_INLINE void IfxStdIf_Inverter_setVoltage(IfxStdIf_Inverter *stdIf, cfloat32 mab)
{
    stdIf->setVoltage(stdIf->driver, mab);
}


/** \copydoc IfxStdIf_Inverter_ShowMessage
 * \param stdif Standard interface pointer
 */
IFX_INLINE void IfxStdIf_Inverter_showMessage(IfxStdIf_Inverter *stdIf)
{
    stdIf->showMessage(stdIf->driver);
}


/** \} */

/** Initialize the configuration structure to default
 *
 * \param config Timer configuration. This parameter is initialised by the function
 *
 */
void IfxStdIf_Inverter_initConfig(IfxStdIf_Inverter_Config *config);

#endif /* IFXSTDIF_INVERTER_H */
