/**
 * \file IfxStdIf_MotorModelF32.c
 * \brief Standard interface: Motor model
 * \ingroup IfxStdIf
 *
 * \version disabled
 * \copyright Copyright (c) 2013 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 */

#include "IfxStdIf_MotorModelF32.h"

void IfxStdIf_MotorModelF32_initConfig(IfxStdIf_MotorModelF32_Config *config)
{
    config->controlPeriod            = 10000.0;
    config->currentMax               = 0.0;
    config->polePair                 = 1;
    config->positionSensorResolution = 0;
}

void Ifx_F32_MotorModel_printType(IfxStdIf_DPipe *io, Ifx_F32_MotorModel_Type type)
{
	switch (type)
	{
	case Ifx_F32_MotorModel_Type_pmsmStandard:
		IfxStdIf_DPipe_print(io, "PMSM standard");
	break;
	case Ifx_F32_MotorModel_Type_pmsmSalient:
		IfxStdIf_DPipe_print(io, "PMSM silent");
	break;
	case Ifx_F32_MotorModel_Type_acim:
		IfxStdIf_DPipe_print(io, "ACIM");
	break;
	case Ifx_F32_MotorModel_Type_openLoop:
		IfxStdIf_DPipe_print(io, "open loop");
	break;
	}
}
