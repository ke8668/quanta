/**
 * \file IfxStdIf_MotorModelF32.h
 * \brief Standard interface: Motor model
 * \ingroup IfxStdIf
 *
 * \version disabled
 * \copyright Copyright (c) 2013 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 *
 * \defgroup library_srvsw_stdif_MotorModel Standard interface: Motor model
 * \ingroup library_srvsw_stdif
 *
 * The standard interface motor model (\ref IfxStdIf_MotorModelF32) abstract the motor. It provide an
 * independent way to interact with the motor like PMSM, ACIM, ...
 *
 * It implement the below functionality:
 *
 * \image html "SrvSw.StdIf.IfxStdIf_F32_MotorModel.png"
 *
 */

#ifndef IFXSTDIF_F32_MOTORMODEL_H
#define IFXSTDIF_F32_MOTORMODEL_H 1

#include "Cpu/Std/Ifx_Types.h"
#include "IfxStdIf.h"
#include "SysSe/Math/IFX_Cf32.h"
#include "IfxStdIf_Pos.h"
#include "SysSe/Math/Ifx_LutLSincosF32.h"
#include "IfxStdIf_DPipe.h"
#include "SysSe/EMotor/Ifx_FocF32.h"

typedef struct
{
    struct
    {
        float32  torque;
        cfloat32 current;
        cfloat32 voltage;
    }ref;
    struct
    {
        float32 mechanicalSpeed;
        float32 vdc;
        sint32  rawMechanicalAngle;
        float32 currents[3];
    }state;
}Ifx_F32_MotorModel_input;

typedef struct
{
    float32  torque;
    cfloat32 modulationIndex;
}Ifx_F32_MotorModel_output;

typedef enum
{
    Ifx_F32_MotorModel_Type_pmsmStandard,
    Ifx_F32_MotorModel_Type_pmsmSalient,
    Ifx_F32_MotorModel_Type_acim,
    Ifx_F32_MotorModel_Type_openLoop,
}Ifx_F32_MotorModel_Type;

typedef struct
{                     /* FIXME force all motor implementation to use this struct instead of custom ones */
    uint8   polePair; /**< \brief PMSM: Motor pole pair count */
    float32 rs;       /**< \brief PMSM: Motor stator resistance in Ohm */
    float32 ld;       /**< \brief PMSM: Motor phase D inductance in H */
    float32 lq;       /**< \brief PMSM: Motor phase Q inductance in H */
    float32 fluxM;    /**< \brief PMSM: Motor flux magnetic linkage in V.s */
    float32 iStall;   /**< \brief PMSM: Motor nominal current in A */
}IfxStdIf_MotorModelF32_Param;

/** \brief Forward declaration */
typedef struct IfxStdIf_MotorModelF32_ IfxStdIf_MotorModelF32;

/** \brief Get the motor parameters
 * \param driver Pointer to the interface driver object
 * \return None
 */
typedef IfxStdIf_MotorModelF32_Param (*IfxStdIf_MotorModelF32_GetParam)(IfxStdIf_InterfaceDriver driver);
/** \brief Enable / disable the torque control
 * \param driver Pointer to the interface driver object
 * \return None
 */
typedef void                          (*IfxStdIf_MotorModelF32_SetTorqueControl)(IfxStdIf_InterfaceDriver driver, boolean enabled);
/** \brief Return the status of the torque control enable / disable state
 * \param driver Pointer to the interface driver object
 * \return Return the status of the torque control enable / disable state
 */
typedef boolean                       (*IfxStdIf_MotorModelF32_IsTorqueControlEnabled)(IfxStdIf_InterfaceDriver driver);
/** \brief Execute the model
 * \param driver Pointer to the interface driver object
 * \return None
 */
typedef void                          (*IfxStdIf_MotorModelF32_Execute)(IfxStdIf_InterfaceDriver driver, Ifx_F32_MotorModel_input *input, Ifx_F32_MotorModel_output *output);
/** \brief Reset the model
 * \param driver Pointer to the interface driver object
 * \return None
 */
typedef void                          (*IfxStdIf_MotorModelF32_Reset)(IfxStdIf_InterfaceDriver driver);
/** \brief Set the control period
 * \param driver Pointer to the interface driver object
 * \return None
 */
typedef void                          (*IfxStdIf_MotorModelF32_SetControlPeriod)(IfxStdIf_InterfaceDriver driver, float32 controlPeriod);

/** \brief Set the current controllers Kp and Ki
 * \param driver Pointer to the interface driver object
 * \param piD Kp and Ki for the D axis
 * \param piQ Kp and Ki for the Q axis
 * \return None
 */
typedef void                          (*IfxStdIf_MotorModelF32_SetCurrentControllers)(IfxStdIf_InterfaceDriver driver, Ifx_FocF32_Pic_Config *piD, Ifx_FocF32_Pic_Config*piQ);
/** \brief Set the position sensor resolution
 * This is equal to the number of pulse per mechanical rotation
 * \param driver Pointer to the interface driver object
 * \return None
 */
typedef void                          (*IfxStdIf_MotorModelF32_SetPositionSensorResolution)(IfxStdIf_InterfaceDriver driver, IfxStdIf_Pos_RawAngle resolution);

/** \brief Standard interface object
 */
struct IfxStdIf_MotorModelF32_
{
    IfxStdIf_InterfaceDriver                            driver;                      /**< \brief Interface driver object                  */
    IfxStdIf_MotorModelF32_GetParam                    getParam;                    /**< \brief \see IfxStdIf_MotorModelF32_GetParam           */
    IfxStdIf_MotorModelF32_SetTorqueControl            setTorqueControl;            /**< \brief \see IfxStdIf_MotorModelF32_SetTorqueControl           */
    IfxStdIf_MotorModelF32_IsTorqueControlEnabled      isTorqueControlEnable;       /**< \brief \see IfxStdIf_MotorModelF32_IsTorqueControlEnabled           */
    IfxStdIf_MotorModelF32_Execute                     execute;                     /**< \brief \see IfxStdIf_MotorModelF32_Execute           */
    IfxStdIf_MotorModelF32_Reset                       reset;                       /**< \brief \see IfxStdIf_MotorModelF32_Reset           */
    IfxStdIf_MotorModelF32_SetControlPeriod            setControlPeriod;            /**< \brief \see IfxStdIf_MotorModelF32_SetControlPeriod           */
    IfxStdIf_MotorModelF32_SetCurrentControllers       setCurrentControllers;       /**< \brief \see IfxStdIf_MotorModelF32_SetCurrentControllers           */
    IfxStdIf_MotorModelF32_SetPositionSensorResolution setPositionSensorResolution; /**< \brief \see IfxStdIf_MotorModelF32_SetPositionSensorResolution           */
};

/** \brief Motor model base configuration */
typedef struct
{
    float32               controlPeriod;            /**< \brief Control period in s */
    float32               currentMax;               /**< \brief Maximum allowed phase current in A */
    uint8                 polePair;                 /**< \brief Number of motor pole pairs */
    IfxStdIf_Pos_RawAngle positionSensorResolution; /**< \brief position sensor resolution: number of pulse per mechanical rotation FIXME this parameter should not be part of the motor config? */
} IfxStdIf_MotorModelF32_Config;

/** \addtogroup library_srvsw_stdif_MotorModel
 *  \{
 */

/** \copydoc IfxStdIf_MotorModelF32_GetParam
 * \param stdif Standard interface pointer
 */
IFX_INLINE IfxStdIf_MotorModelF32_Param IfxStdIf_MotorModelF32_getParam(IfxStdIf_MotorModelF32 *stdIf)
{
    return stdIf->getParam(stdIf->driver);
}


/** \copydoc IfxStdIf_MotorModelF32_SetTorqueControl
 * \param stdif Standard interface pointer
 */
IFX_INLINE void IfxStdIf_MotorModelF32_setTorqueControl(IfxStdIf_MotorModelF32 *stdIf, boolean enabled)
{
    stdIf->setTorqueControl(stdIf->driver, enabled);
}


/** \copydoc IfxStdIf_MotorModelF32_IsTorqueControlEnable
 * \param stdif Standard interface pointer
 */
IFX_INLINE boolean IfxStdIf_MotorModelF32_isTorqueControlEnable(IfxStdIf_MotorModelF32 *stdIf)
{
    return stdIf->isTorqueControlEnable(stdIf->driver);
}


/** \copydoc IfxStdIf_MotorModelF32_Execute
 * \param stdif Standard interface pointer
 */
IFX_INLINE void IfxStdIf_MotorModelF32_execute(IfxStdIf_MotorModelF32 *stdIf, Ifx_F32_MotorModel_input *input, Ifx_F32_MotorModel_output *output)
{
    stdIf->execute(stdIf->driver, input, output);
}


/** \copydoc IfxStdIf_MotorModelF32_Reset
 * \param stdif Standard interface pointer
 */
IFX_INLINE void IfxStdIf_MotorModelF32_reset(IfxStdIf_MotorModelF32 *stdIf)
{
    stdIf->reset(stdIf->driver);
}


/** \copydoc IfxStdIf_MotorModelF32_SetControlPeriod
 * \param stdif Standard interface pointer
 */
IFX_INLINE void IfxStdIf_MotorModelF32_setControlPeriod(IfxStdIf_MotorModelF32 *stdIf, float32 controlPeriod)
{
    stdIf->setControlPeriod(stdIf->driver, controlPeriod);
}


/** \copydoc IfxStdIf_MotorModelF32_SetCurrentControllers
 * \param stdif Standard interface pointer
 */
IFX_INLINE void IfxStdIf_MotorModelF32_setCurrentControllers(IfxStdIf_MotorModelF32 *stdIf, Ifx_FocF32_Pic_Config *piD, Ifx_FocF32_Pic_Config*piQ)
{
    stdIf->setCurrentControllers(stdIf->driver, piD, piQ);
}


/** \copydoc IfxStdIf_MotorModelF32_SetPositionSensorResolution
 * \param stdif Standard interface pointer
 */
IFX_INLINE void IfxStdIf_MotorModelF32_setPositionSensorResolution(IfxStdIf_MotorModelF32 *stdIf, IfxStdIf_Pos_RawAngle resolution)
{
    stdIf->setPositionSensorResolution(stdIf->driver, resolution);
}


/** Initialize the configuration structure to default
 *
 * \param config Motor model configuration. This parameter is initialized by the function
 *
 */
IFX_EXTERN void IfxStdIf_MotorModelF32_initConfig(IfxStdIf_MotorModelF32_Config *config);

/** \brief Return the rotor electrical speed in rad/s (float32) */
IFX_INLINE float32 Ifx_F32_MotorModel_getElectricalSpeed(uint8 polePair, float32 mechanicalSpeed)
{
    return mechanicalSpeed * polePair;
}


/** \brief Get the electrical angle used as output */
IFX_INLINE IfxStdIf_Pos_RawAngle Ifx_F32_MotorModel_getElectricalAngle(float32 electricalAngleConst, IfxStdIf_Pos_RawAngle mechanicalAngle)
{
    return (IfxStdIf_Pos_RawAngle)(electricalAngleConst * mechanicalAngle) & (IFX_LUT_ANGLE_RESOLUTION - 1);
}


IFX_INLINE IfxStdIf_Pos_RawAngle Ifx_F32_MotorModel_getElectricalAngleDelta(float32 electricalSpeed, float32 controlPeriod)
{
    return electricalSpeed * controlPeriod * (IFX_LUT_ANGLE_RESOLUTION / (IFX_PI * 2.0));
}

void Ifx_F32_MotorModel_printType(IfxStdIf_DPipe *io, Ifx_F32_MotorModel_Type type);

/*\}*/

#endif /* IFXSTDIF_F32_MOTORMODEL_H */
