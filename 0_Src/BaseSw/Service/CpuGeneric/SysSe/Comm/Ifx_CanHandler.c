
#include "Common/AppCan.h"
#include "Ifx_CanHandler.h"
#include "Ifx_Console.h"
#include "_Lib/DataHandling/Ifx_Fifo.h"


static void Ifx_CanHandler_releaseMessage(Ifx_CanHandler *canHandler, Ifx_CanHandler_MessagePointer message);
static void Ifx_CanHandler_addMessageToQueue(Ifx_CanHandler_MessagePointer message, Ifx_CanHandler_MessagePointer *queue);
static void Ifx_CanHandler_dumpQueue(Ifx_CanHandler *canHandler, Ifx_CanHandler_MessagePointer queue);

static void Ifx_CanHandler_pack8(uint64 *buffer, Ifx_CanHandler_MessageItemPointer item);
static void Ifx_CanHandler_pack16(uint64 *buffer, Ifx_CanHandler_MessageItemPointer item);
static void Ifx_CanHandler_pack32(uint64 *buffer, Ifx_CanHandler_MessageItemPointer item);
static void Ifx_CanHandler_pack64(uint64 *buffer, Ifx_CanHandler_MessageItemPointer item);
static void Ifx_CanHandler_packBoolean(uint64 *buffer, Ifx_CanHandler_MessageItemPointer item);
static void Ifx_CanHandler_packBitfield32(uint64 *buffer, Ifx_CanHandler_MessageItemPointer item);
static void Ifx_CanHandler_unpack8(uint64 *buffer, Ifx_CanHandler_MessageItemPointer item);
static void Ifx_CanHandler_unpack16(uint64 *buffer, Ifx_CanHandler_MessageItemPointer item);
static void Ifx_CanHandler_unpack32(uint64 *buffer, Ifx_CanHandler_MessageItemPointer item);
static void Ifx_CanHandler_unpack64(uint64 *buffer, Ifx_CanHandler_MessageItemPointer item);
static void Ifx_CanHandler_unpackBoolean(uint64 *buffer, Ifx_CanHandler_MessageItemPointer item);
static void Ifx_CanHandler_unpackBitfield32(uint64 *buffer, Ifx_CanHandler_MessageItemPointer item);

boolean Ifx_CanHandler_init(Ifx_CanHandler *canHandler, Ifx_CanHandler_Config *config)
{
    uint32 i;
    Ifx_Console_debug("Initializing Ifx_CanHandler"ENDL);
    canHandler->rx              = IFX_CANHANDLER_NO_MESSAGE;
    canHandler->tx              = IFX_CANHANDLER_NO_MESSAGE;
    canHandler->freeItem        = &canHandler->itemPool[0];
    canHandler->freeMessage     = &canHandler->messagePool[0];
    canHandler->inboxTime       = 0;
    canHandler->outboxTime      = 0;
    canHandler->processInterval = config->processInterval;

    for (i = 0; i < IFX_CANHANDLER_MESSAGE_MAX; i++)
    {
        Ifx_CanHandler_MessagePointer message;
        message                = &canHandler->messagePool[i];
        message->id            = 0;
        message->dlc           = 0;
        message->items         = IFX_CANHANDLER_NO_ITEM;
        message->interval      = 0;
        message->eventTime     = 0;
        message->rxTxCount  = 0;
        message->missingCount  = 0;
        message->flags.type    = IFX_CANHANDLER_MESSAGE_TYPE_TRANSMIT;
        message->flags.newData = 0;
        message->flags.single  = 0;
        message->protocol      = Ifx_CanHandler_Protocol_none;

        if (i == 0)
        {
            message->prev = &canHandler->messagePool[IFX_CANHANDLER_MESSAGE_MAX - 1];
            message->next = &canHandler->messagePool[i + 1];
        }
        else if (i == IFX_CANHANDLER_MESSAGE_MAX - 1)
        {
            message->prev = &canHandler->messagePool[i - 1];
            message->next = &canHandler->messagePool[0];
        }
        else
        {
            message->prev = &canHandler->messagePool[i - 1];
            message->next = &canHandler->messagePool[i + 1];
        }
    }

    for (i = 0; i < IFX_CANHANDLER_MESSAGE_ITEM_MAX; i++)
    {
        canHandler->itemPool[i].data   = NULL_PTR;
        canHandler->itemPool[i].offset = 0;
        canHandler->itemPool[i].size   = 0;
        canHandler->itemPool[i].pack   = NULL_PTR;

        if (i == IFX_CANHANDLER_MESSAGE_ITEM_MAX - 1)
        {
            canHandler->itemPool[i].next = IFX_CANHANDLER_NO_ITEM;
        }
        else
        {
            canHandler->itemPool[i].next = &canHandler->itemPool[i + 1];
        }
    }

    Ifx_CanHandler_dumpQueues(canHandler);

    return TRUE;
}


void Ifx_CanHandler_initConfig(Ifx_CanHandler_Config *config)
{
    config->processInterval = 1;
}


const uint64 Ifx_CanHandler_mask[65] =
{
    0x0000000000000000,
    0x0000000000000001,
    0x0000000000000003,
    0x0000000000000007,
    0x000000000000000F,
    0x000000000000001F,
    0x000000000000003F,
    0x000000000000007F,
    0x00000000000000FF,
    0x00000000000001FF,
    0x00000000000003FF,
    0x00000000000007FF,
    0x0000000000000FFF,
    0x0000000000001FFF,
    0x0000000000003FFF,
    0x0000000000007FFF,
    0x000000000000FFFF,
    0x000000000001FFFF,
    0x000000000003FFFF,
    0x000000000007FFFF,
    0x00000000000FFFFF,
    0x00000000001FFFFF,
    0x00000000003FFFFF,
    0x00000000007FFFFF,
    0x0000000000FFFFFF,
    0x0000000001FFFFFF,
    0x0000000003FFFFFF,
    0x0000000007FFFFFF,
    0x000000000FFFFFFF,
    0x000000001FFFFFFF,
    0x000000003FFFFFFF,
    0x000000007FFFFFFF,
    0x00000000FFFFFFFF,
    0x00000001FFFFFFFF,
    0x00000003FFFFFFFF,
    0x00000007FFFFFFFF,
    0x0000000FFFFFFFFF,
    0x0000001FFFFFFFFF,
    0x0000003FFFFFFFFF,
    0x0000007FFFFFFFFF,
    0x000000FFFFFFFFFF,
    0x000001FFFFFFFFFF,
    0x000003FFFFFFFFFF,
    0x000007FFFFFFFFFF,
    0x00000FFFFFFFFFFF,
    0x00001FFFFFFFFFFF,
    0x00003FFFFFFFFFFF,
    0x00007FFFFFFFFFFF,
    0x0000FFFFFFFFFFFF,
    0x0001FFFFFFFFFFFF,
    0x0003FFFFFFFFFFFF,
    0x0007FFFFFFFFFFFF,
    0x000FFFFFFFFFFFFF,
    0x001FFFFFFFFFFFFF,
    0x003FFFFFFFFFFFFF,
    0x007FFFFFFFFFFFFF,
    0x00FFFFFFFFFFFFFF,
    0x01FFFFFFFFFFFFFF,
    0x03FFFFFFFFFFFFFF,
    0x07FFFFFFFFFFFFFF,
    0x0FFFFFFFFFFFFFFF,
    0x1FFFFFFFFFFFFFFF,
    0x3FFFFFFFFFFFFFFF,
    0x7FFFFFFFFFFFFFFF,
    0xFFFFFFFFFFFFFFFF,
};

void Ifx_CanHandler_sendData(IfxCan_Can_MsgObj *msgObj, uint32 id, uint64 data, uint8 dlc)
{
    IfxCan_Message msg;
    msg.dataLengthCode = dlc;
    msg.messageId         = id;
//    msg.data[0]    = (uint32)(data);
//    msg.data[1]    = (uint32)(data >> 32);
//    IfxCan_Can_sendMessage(msgObj, &msg);
}


boolean Ifx_CanHandler_readData(IfxCan_Can_MsgObj *msgObj, uint64 *data, uint8 *dlc)
{
    IfxCan_Message msg;
    IfxCan_Status  status;
    // 20191214 Jimmy should be correct
#if 0
    status = IfxCan_Can_readMessage(msgObj, &msg);

    if ((status & IfxMultican_Status_newData) != 0)
    {
        *data  = (((uint64)msg.data[1]) << 32) | msg.data[0];
        *data &= Ifx_CanHandler_mask[msg.lengthCode << 3];
        *dlc   = msg.lengthCode;
        return TRUE;
    }
    else
    {
        return FALSE;
    }
#endif
}


static void Ifx_CanHandler_pack8(uint64 *buffer, Ifx_CanHandler_MessageItemPointer item)
{
    ((uint8 *)buffer)[item->offset >> 3] = *((uint8 *)item->data);
}


static void Ifx_CanHandler_pack16(uint64 *buffer, Ifx_CanHandler_MessageItemPointer item)
{
    ((uint16 *)buffer)[item->offset >> 4] = *((uint16 *)item->data);
}


static void Ifx_CanHandler_pack32(uint64 *buffer, Ifx_CanHandler_MessageItemPointer item)
{
    ((uint32 *)buffer)[item->offset >> 5] = *((uint32 *)item->data);
}


static void Ifx_CanHandler_pack64(uint64 *buffer, Ifx_CanHandler_MessageItemPointer item)
{
    *((uint64 *)buffer) = *((uint64 *)item->data);
}


static void Ifx_CanHandler_packBoolean(uint64 *buffer, Ifx_CanHandler_MessageItemPointer item)
{
    uint32 offset = item->offset;

    *buffer &= ~(1 << offset);
    *buffer |= ((uint64)(*((boolean *)item->data) ? 1 : 0)) << offset;
}


static void Ifx_CanHandler_packBitfield32(uint64 *buffer, Ifx_CanHandler_MessageItemPointer item)
{
    uint32 offset = item->offset;
    uint64 mask   = Ifx_CanHandler_mask[item->size];
    uint64 data   = *(uint32 *)item->data;

    *buffer &= ~(mask << offset);
    *buffer |= (data) << offset;
}


static void Ifx_CanHandler_unpack8(uint64 *buffer, Ifx_CanHandler_MessageItemPointer item)
{
    *((uint8 *)item->data) = ((uint8 *)buffer)[item->offset >> 3];
}


static void Ifx_CanHandler_unpack16(uint64 *buffer, Ifx_CanHandler_MessageItemPointer item)
{
    *((uint16 *)item->data) = ((uint16 *)buffer)[item->offset >> 4];
}


static void Ifx_CanHandler_unpack32(uint64 *buffer, Ifx_CanHandler_MessageItemPointer item)
{
    *((uint32 *)item->data) = ((uint32 *)buffer)[item->offset >> 5];
}


static void Ifx_CanHandler_unpack64(uint64 *buffer, Ifx_CanHandler_MessageItemPointer item)
{
    *((uint64 *)item->data) = *((uint64 *)buffer);
}


static void Ifx_CanHandler_unpackBoolean(uint64 *buffer, Ifx_CanHandler_MessageItemPointer item)
{
    *((boolean *)item->data) = ((*buffer) >> item->offset) & 0x1;
}


static void Ifx_CanHandler_unpackBitfield32(uint64 *buffer, Ifx_CanHandler_MessageItemPointer item)
{
    uint32  offset = item->offset;
    uint64  mask   = Ifx_CanHandler_mask[item->size];

    *((uint32 *)item->data) = (uint32)(((*buffer) >> offset) & mask);
}


static void Ifx_CanHandler_send(Ifx_CanHandler *canHandler, Ifx_CanHandler_MessagePointer message)
{
    uint64  data;
    boolean send = TRUE;
//    Ifx_Console_debug("Ifx_CanHandler.send(message.id=%d)"ENDL, message->id);

    data = 0;

    switch (message->protocol)
    {
        case Ifx_CanHandler_Protocol_none:
        {
            Ifx_CanHandler_MessageItem *item;
            item = message->items;

            while (item != IFX_CANHANDLER_NO_ITEM)
            {
                /* FIXME add call back to enable "scaling" of data */
                item->pack(&data, item);
                item = item->next;
            }
        }
        break;
        case Ifx_CanHandler_Protocol_streamBasic:
        {
            Ifx_CanHandler_MessageItem *item;
//	20191214 Jimmy Should be correct
//            if (IfxCan_MsgObj_getPointer(message->msgObj->node->mcan, message->msgObj->msgObjId)->STAT.B.TXRQ != 0)
            {
                /* CAN MO is busy */
                send = FALSE;
                break;
            }

            item = message->items;

            while (item != IFX_CANHANDLER_NO_ITEM)
            {
                /* FIXME add support for uint16 and uint32 */
                message->dlc = 8 - Ifx_Fifo_read(item->data, &data, 8, TIME_NULL);

                if (message->dlc == 0)
                {
                    /* Transmit buffer is empty */
                    send = FALSE;
                }

                break;         /* Allow only one item */
            }
        }
        break;
    }

    /* FIXME take the return status in account */
    if (send)
    {
        Ifx_CanHandler_sendData(message->msgObj, message->id, data, message->dlc);
        message->rxTxCount++;
    }
}


static void Ifx_CanHandler_receive(Ifx_CanHandler *canHandler, Ifx_CanHandler_MessagePointer message)
{
    uint64 data;
    uint8  dlc = 0;
//    Ifx_Console_debug("Ifx_CanHandler.receive(message.id=%d)"ENDL, message->id);

    if (Ifx_CanHandler_readData(message->msgObj, &data, &dlc))
    {
        /* FIXME handle queue with more than one message */
        switch (message->protocol)
        {
            case Ifx_CanHandler_Protocol_none:
            {
                Ifx_CanHandler_MessageItem *item;

                item = message->items;

                while (item != IFX_CANHANDLER_NO_ITEM)
                {
                    /* FIXME add call back to enable "scaling" of data */
                    item->pack(&data, item);

                    item = item->next;
                }
            }
            break;
            case Ifx_CanHandler_Protocol_streamBasic:
            {
                Ifx_CanHandler_MessageItem *item;

                item = message->items;

                while (item != IFX_CANHANDLER_NO_ITEM)
                {
                    if (Ifx_Fifo_write(item->data, &data, dlc, TIME_NULL) != 0)
                    {
                        /* Receive buffer is full, data is discard */
                        /* FIXME set flag */
                    }

                    break;         /* Allow only one item */
                }
            }
            break;
        }

        message->rxTxCount++;
    }
    else
    {
        message->missingCount++;
        /* FIXME option for action, on missed message */
    }
}


static Ifx_CanHandler_MessagePointer Ifx_CanHandler_reorderQueue(Ifx_CanHandler *canHandler, Ifx_CanHandler_MessagePointer first, Ifx_CanHandler_MessagePointer last)
{
    Ifx_CanHandler_MessagePointer message;
    Ifx_CanHandler_MessagePointer index;
    Ifx_CanHandler_MessagePointer result;

    message = first->prev;
    result  = last->next;

    do
    {
        message = message->next;

        if (message->eventTime < result->eventTime)
        {
            result = message;
        }

        index = last->next;

        while ((index != message) && (message->eventTime >= index->eventTime))
        {
            index = index->next;
        }

        if (index != message)
        {
            /* Remove messages from the list */
            message->prev->next = message->next;
            message->next->prev = message->prev;

            /* Insert messages before index */
            message->next       = index;
            message->prev       = index->prev;
            index->prev         = message;
            message->prev->next = message;
        }
    }
    while (message != last);

    return result;
}


boolean Ifx_CanHandler_processOutbox(Ifx_CanHandler *canHandler)
{
    Ifx_CanHandler_MessagePointer message;

//    Ifx_Console_debug("Ifx_CanHandler.processOutbox(time=%d)"ENDL, canHandler->outboxTime);

    /* Handle TX */
    message = canHandler->tx;

    if (message != IFX_CANHANDLER_NO_MESSAGE)
    {
        Ifx_CanHandler_MessagePointer first;

        /* Send message */
        first = message;

        while (message->eventTime <= canHandler->outboxTime)
        {
            /* FIXME check max outboxTime before overflow. Hint about it in the doc */
            if (message->flags.newData)
            {
                Ifx_CanHandler_send(canHandler, message);
                message->eventTime += message->interval;

                if (message->flags.single)
                {
                    message->flags.newData = 0;
                }
            }
            else
            {
                message->eventTime += canHandler->processInterval;
            }

            message = message->next;
        }

        if (first->eventTime >= canHandler->outboxTime)
        {
            canHandler->tx = Ifx_CanHandler_reorderQueue(canHandler, first, message->prev);
        }
    }

    canHandler->outboxTime += canHandler->processInterval;

//    Ifx_Console_debug("Tx queue:"ENDL);
    Ifx_CanHandler_dumpQueue(canHandler, canHandler->tx);
    return TRUE;
}


boolean Ifx_CanHandler_processInbox(Ifx_CanHandler *canHandler)
{
    Ifx_CanHandler_MessagePointer message;

    uint32 rxData[50];

//    Ifx_Console_debug("Ifx_CanHandler.processInbox(time=%d)"ENDL, canHandler->inboxTime);

#if 1
    /* Handle RX */
    message = canHandler->rx;

    if (message != IFX_CANHANDLER_NO_MESSAGE)
    {
        Ifx_CanHandler_MessagePointer first;

        /* receive message */
        first = message;

        while (message->eventTime <= canHandler->inboxTime)
        {
            Ifx_CanHandler_receive(canHandler, message);
            message->eventTime += message->interval;
            message             = message->next;
        }

        if (first->eventTime >= canHandler->inboxTime)
        {
            canHandler->rx = Ifx_CanHandler_reorderQueue(canHandler, first, message->prev);
        }
    }

    canHandler->inboxTime += canHandler->processInterval;
//    Ifx_Console_debug("Rx queue:"ENDL);
    Ifx_CanHandler_dumpQueue(canHandler, canHandler->rx);
#endif
    return TRUE;
}


void Ifx_CanHandler_initMessageConfig(Ifx_CanHandler_MessageConfig *config)
{
    config->id        = 0;
    config->msgObj    = NULL_PTR;
    config->interval  = 10;
    config->eventTime = 0;
    config->type      = Ifx_CanHandler_MessageType_receive;
    config->protocol  = Ifx_CanHandler_Protocol_none;
}


/** Remove a message from a list
 *
 */
static void Ifx_CanHandler_popMessage(Ifx_CanHandler *canHandler, Ifx_CanHandler_MessagePointer message)
{
    IFX_ASSERT(IFX_VERBOSE_LEVEL_ERROR, message != IFX_CANHANDLER_NO_MESSAGE);

    /* Update queue pointers */
    if (message == canHandler->tx)
    {
        if (canHandler->tx != canHandler->tx->next)
        {
            canHandler->tx = canHandler->tx->next;
        }
        else
        {
            canHandler->tx = IFX_CANHANDLER_NO_MESSAGE;
        }
    }
    else if (message == canHandler->rx)
    {
        if (canHandler->rx != canHandler->rx->next)
        {
            canHandler->rx = canHandler->rx->next;
        }
        else
        {
            canHandler->rx = IFX_CANHANDLER_NO_MESSAGE;
        }
    }

    message->next->prev = message->prev;
    message->prev->next = message->next;

    message->next       = IFX_CANHANDLER_NO_MESSAGE;
    message->prev       = IFX_CANHANDLER_NO_MESSAGE;
}


/** Add a message to the free message list
 *
 */
static void Ifx_CanHandler_releaseMessage(Ifx_CanHandler *canHandler, Ifx_CanHandler_MessagePointer message)
{
    IFX_ASSERT(IFX_VERBOSE_LEVEL_ERROR, message != IFX_CANHANDLER_NO_MESSAGE);

    switch (message->flags.type)
    {
        case IFX_CANHANDLER_MESSAGE_TYPE_TRANSMIT:
            Ifx_CanHandler_popMessage(canHandler, message);
//            Ifx_Console_debug("Ifx_CanHandler.releaseMessage(transmit, id=%d)"ENDL, message->id);

            break;
        case IFX_CANHANDLER_MESSAGE_TYPE_RECEIVE:
            Ifx_CanHandler_popMessage(canHandler, message);
//            Ifx_Console_debug("Ifx_CanHandler.releaseMessage(receive, id=%d)"ENDL, message->id);
            break;
    }

    message->eventTime = 0;
    Ifx_CanHandler_addMessageToQueue(message, &canHandler->freeMessage);
}


/** Add a message at in the queue, sorted by eventTime
 * If the message is already in a queue, it is first removed from its queue
 * The message must not be already in a queue
 */
static void Ifx_CanHandler_addMessageToQueue(Ifx_CanHandler_MessagePointer message, Ifx_CanHandler_MessagePointer *queue)
{
    IFX_ASSERT(IFX_VERBOSE_LEVEL_ERROR, message != IFX_CANHANDLER_NO_MESSAGE);
    Ifx_CanHandler_MessagePointer prev;

    prev = *queue;

    if (prev == IFX_CANHANDLER_NO_MESSAGE)
    {
        /* Queue is empty */
        /* Build circular buffer */
        message->prev = message;
        message->next = message;
        *queue        = message;
    }
    else
    {
        while ((prev->eventTime <= message->eventTime) && (prev != (*queue)->prev))
        {
            prev = prev->next;
        }

        if (prev->eventTime > message->eventTime)
        {
            prev = prev->prev;
        }

        /* Add message after prev */
        message->prev       = prev;
        message->next       = prev->next;
        message->next->prev = message;
        prev->next          = message;
    }
}


/** Remove the next free message from the list
 *
 */
static Ifx_CanHandler_MessagePointer Ifx_CanHandler_popFreeMessage(Ifx_CanHandler *canHandler)
{
    Ifx_CanHandler_MessagePointer message;
    message = canHandler->freeMessage;

    if (message != IFX_CANHANDLER_NO_MESSAGE)
    {
        if (canHandler->freeMessage != canHandler->freeMessage->next)
        {
            message->next->prev     = message->prev;
            message->prev->next     = message->next;
            canHandler->freeMessage = canHandler->freeMessage->next;
        }
        else
        {
            canHandler->freeMessage = IFX_CANHANDLER_NO_MESSAGE;
        }

        message->next = IFX_CANHANDLER_NO_MESSAGE;
        message->prev = IFX_CANHANDLER_NO_MESSAGE;
    }
    else
    {}

    return message;
}


Ifx_CanHandler_MessagePointer Ifx_CanHandler_registerMessage(Ifx_CanHandler *canHandler, Ifx_CanHandler_MessageConfig *config)
{
    Ifx_CanHandler_MessagePointer message;

    if ((config->msgObj != NULL_PTR) || TRUE)
    {
        message = Ifx_CanHandler_popFreeMessage(canHandler);

        if (message != IFX_CANHANDLER_NO_MESSAGE)
        {
            message->id            = config->id;
            message->dlc           = 0;
            message->msgObj        = config->msgObj;
            message->interval      = config->interval;
            message->eventTime     = config->eventTime;

            message->items         = IFX_CANHANDLER_NO_ITEM;
            message->rxTxCount  = 0;
            message->missingCount  = 0;
            message->flags.newData = 1;
            message->flags.single  = 0;
            message->protocol      = config->protocol;

            switch (config->type)
            {
                case Ifx_CanHandler_MessageType_transmit:
                    message->flags.type = IFX_CANHANDLER_MESSAGE_TYPE_TRANSMIT;
                    Ifx_CanHandler_addMessageToQueue(message, &canHandler->tx);
//				Ifx_Console_debug("Ifx_CanHandler.registerMessage(transmit, id=%d, node=%d, period=%d)"ENDL, message->id, message->msgObj->node->nodeId, message->interval);

                    break;
                case Ifx_CanHandler_MessageType_receive:
                    message->flags.type = IFX_CANHANDLER_MESSAGE_TYPE_RECEIVE;
                    Ifx_CanHandler_addMessageToQueue(message, &canHandler->rx);
//				Ifx_Console_debug("Ifx_CanHandler.registerMessage(receive, id=%d, node=%d, period=%d)"ENDL, message->id, message->msgObj->node->nodeId, message->interval);
                    break;
            }
        }
        else
        {}
    }
    else
    {
        message = IFX_CANHANDLER_NO_MESSAGE;
    }

    return message;
}


void Ifx_CanHandler_initFieldConfig(Ifx_CanHandler_FieldConfig *config)
{
    config->data   = NULL_PTR;
    config->offset = 0;
    config->size   = 0;
}


boolean Ifx_CanHandler_addMessageField(Ifx_CanHandler *canHandler, Ifx_CanHandler_MessagePointer message, Ifx_CanHandler_FieldConfig *config)
{
    boolean                           result = TRUE;
    Ifx_CanHandler_MessageItemPointer item;

    item = canHandler->freeItem;

    if (item != IFX_CANHANDLER_NO_ITEM)
    {
        uint8 size;
        uint8 offset;
        uint8 type;
        type = message->flags.type;

        /* Remove item from the free pool */
        canHandler->freeItem = item->next;

        /* Configure item */
        item->data   = config->data;
        IFX_ASSERT(IFX_VERBOSE_LEVEL_ERROR, item->data != NULL_PTR);
        item->next   = IFX_CANHANDLER_NO_ITEM;
        item->offset = config->offset;
        item->size   = config->size;

        size         = item->size;
        offset       = item->offset;

        if (size == 0)
        {
            item->pack = type == IFX_CANHANDLER_MESSAGE_TYPE_TRANSMIT ? &Ifx_CanHandler_packBoolean : &Ifx_CanHandler_unpackBoolean;
        }
        else if ((size == 8) && ((offset & 0x3) == 0))
        {
            item->pack = type == IFX_CANHANDLER_MESSAGE_TYPE_TRANSMIT ? &Ifx_CanHandler_pack8 : &Ifx_CanHandler_unpack8;
        }
        else if ((size == 16) && ((offset & 0x4) == 0))
        {
            item->pack = type == IFX_CANHANDLER_MESSAGE_TYPE_TRANSMIT ? &Ifx_CanHandler_pack16 : &Ifx_CanHandler_unpack16;
        }
        else if ((size == 32) && ((offset & 0x5) == 0))
        {
            item->pack = type == IFX_CANHANDLER_MESSAGE_TYPE_TRANSMIT ? &Ifx_CanHandler_pack32 : &Ifx_CanHandler_unpack32;
        }
        else if ((size == 64) && ((offset & 0x6) == 0))
        {
            item->pack = type == IFX_CANHANDLER_MESSAGE_TYPE_TRANSMIT ? &Ifx_CanHandler_pack64 : &Ifx_CanHandler_unpack64;
        }
        else
        {
            item->pack = type == IFX_CANHANDLER_MESSAGE_TYPE_TRANSMIT ? &Ifx_CanHandler_packBitfield32 : &Ifx_CanHandler_unpackBitfield32;
        }

        uint8 dlc;
        dlc = (item->offset + item->size + 7) / 8;

        if (dlc > message->dlc)
        {
            message->dlc = dlc;
        }

        /* Append item to the message field list */
        Ifx_CanHandler_MessageItemPointer index;
        index = message->items;

        if (index != IFX_CANHANDLER_NO_ITEM)
        {
            while (index->next != IFX_CANHANDLER_NO_ITEM)
            {
                index = index->next;
            }

            index->next = item;
        }
        else
        {
            message->items = item;
        }
    }
    else
    {
        result = FALSE;
        IFX_ASSERT(IFX_VERBOSE_LEVEL_WARNING, result);
    }

    return result;
}


void Ifx_CanHandler_unregisterMessage(Ifx_CanHandler *canHandler, Ifx_CanHandler_MessagePointer message)
{
    if (message != IFX_CANHANDLER_NO_MESSAGE)
    {
        Ifx_CanHandler_releaseMessage(canHandler, message);
    }
    else
    {}
}


static void Ifx_CanHandler_dumpQueue(Ifx_CanHandler *canHandler, Ifx_CanHandler_MessagePointer queue)
{
    Ifx_CanHandler_MessagePointer message;
    uint32                        count = 0;
    message = queue;

    if (message != IFX_CANHANDLER_NO_MESSAGE)
    {
        do
        {
            count++;
//            Ifx_Console_debug("    id=%d, time=%d, interval=%d, dir=", message->id, message->eventTime, message->interval);

            switch (message->flags.type)
            {
                case IFX_CANHANDLER_MESSAGE_TYPE_TRANSMIT:
//                    Ifx_Console_debug("transmit"ENDL, message->id);

                    break;
                case IFX_CANHANDLER_MESSAGE_TYPE_RECEIVE:
//                    Ifx_Console_debug("receive"ENDL, message->id);
                    break;
            }

            message = message->next;
        }
        while ((message != queue) && (message != IFX_CANHANDLER_NO_MESSAGE));

//        Ifx_Console_debug("    Number of messages: %d"ENDL, count);
    }
    else
    {
//        Ifx_Console_debug("    empty"ENDL);
    }
}


void Ifx_CanHandler_dumpQueues(Ifx_CanHandler *canHandler)
{
//    Ifx_Console_debug("Free queue:"ENDL);
    Ifx_CanHandler_dumpQueue(canHandler, canHandler->freeMessage);
//    Ifx_Console_debug("Tx queue:"ENDL);
    Ifx_CanHandler_dumpQueue(canHandler, canHandler->tx);
//    Ifx_Console_debug("Rx queue:"ENDL);
    Ifx_CanHandler_dumpQueue(canHandler, canHandler->rx);
}
