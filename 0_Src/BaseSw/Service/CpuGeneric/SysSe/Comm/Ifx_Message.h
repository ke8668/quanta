/**
 * \file Ifx_Message.h
 * \brief Internal messaging functions
 *
 *
 * \version disabled
 * \copyright Copyright (c) 2013 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 */

#ifndef IFX_MESSAGE_H
#define IFX_MESSAGE_H

#include "SysSe/Bsp/Bsp.h"
#include "StdIf/IfxStdIf_DPipe.h"

//-----------------------------------------------------------------------------

typedef struct
{
    boolean single;             /**< \brief If TRUE, the message is only showed once */
    pchar   string;             /**< \brief Message text */
} Ifx_Message_Entry;

typedef struct
{
    uint32                   owner;     /**< \brief Identifier of the owner */
    pchar                    ownerName; /**< \brief Owner name */
    uint32                   flags;     /**< \brief Error and status message flags */
    uint32                   displayed; /**< \brief Error and status message displayed flags */
    uint32                   count;     /**< \brief Number of flag bits */
    const Ifx_Message_Entry *entries;   /**< \brief Pointer to the message entries */
    IfxStdIf_DPipePointer   *standardIo;
} Ifx_Message;

typedef struct
{
    uint32                   owner;      /**< \brief Identifier of the owner */
    pchar                    ownerName;  /**< \brief Owner name */
    uint32                   count;      /**< \brief Number of flag bits*/
    const Ifx_Message_Entry *entries;    /**< \brief Pointer to the message entries */
    IfxStdIf_DPipePointer   *standardIo; /**< \brief Pipe used for displaying the messages */
}Ifx_Message_Config;

IFX_EXTERN boolean Ifx_Message_isSet(Ifx_Message *message, uint32 index, boolean single);

IFX_INLINE void Ifx_Message_clear(Ifx_Message *message)
{
    message->flags     = 0;
    message->displayed = 0;
}


IFX_EXTERN void Ifx_Message_initConfig(Ifx_Message_Config *config);

IFX_EXTERN void Ifx_Message_init(Ifx_Message *message, Ifx_Message_Config *config);

IFX_INLINE void Ifx_Message_set(Ifx_Message *message, uint32 index)
{
    uint32  mask   = 0x1UL << index;
    boolean istate = disableInterrupts();
    message->flags |= mask;
    restoreInterrupts(istate);
}
IFX_INLINE void Ifx_Message_setFromTrap(Ifx_Message *message, uint32 index)
{
    uint32  mask   = 0x1UL << index;
    message->flags |= mask;
}


IFX_INLINE void Ifx_Message_enable(Ifx_Message *message, uint32 index)
{
    uint32  mask   = 0x1 << index;
    boolean istate = disableInterrupts();
    message->displayed &= ~mask;
    restoreInterrupts(istate);
}


IFX_INLINE void Ifx_Message_enableAll(Ifx_Message *message)
{
    message->displayed = 0;
}


IFX_EXTERN void Ifx_Message_show(Ifx_Message *message);

#endif /* IFX_MESSAGE_H */
