/**
 * \file Ifx_Osci.c
 * \brief Oscilloscope functionality
 *
 * \version disabled
 * \copyright Copyright (c) 2013 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 */
//----------------------------------------------------------------------------------------
#include "Ifx_Osci.h"
#include "_Utilities/Ifx_Assert.h"
#include "SysSe/Math/Ifx_FftF32.h"
#include "SysSe/Math/Ifx_WndF32.h"
#include "SysSe/Math/Ifx_LutLSincosF32.h"
#include "SysSe/Math/Ifx_LutAtan2F32.h"

//----------------------------------------------------------------------------------------

/** \brief Returns the signal name referenced by source */
pchar Ifx_Osci_getSignalName(Ifx_Osci *osci, const void *source)
{
    sint16 i;
    pchar  result = "Unknown";

    for (i = 0; i < osci->signals.count; i++)
    {
        if (source == osci->signals.config[i].config.source)
        {
            result = osci->signals.config[i].name;
            break;
        }
    }

    return result;
}


/** \brief Returns null-terminated string name of the requested type */
pchar Ifx_Osci_getDataTypeName(Ifx_Osci_DataType type)
{
    pchar result;

    switch (type)
    {
    case Ifx_Osci_DataType_Float32:      result   = "float32";     break;
    case Ifx_Osci_DataType_SInt16:       result   = "sint16";      break;
    case Ifx_Osci_DataType_SInt32:       result   = "sint32";      break;
    case Ifx_Osci_DataType_FixPoint16:   result   = "FixPoint16";  break;
    case Ifx_Osci_DataType_FixPoint32:   result   = "FixPoint32";  break;
    case Ifx_Osci_DataType_SInt8:        result   = "sint8";  break;
    case Ifx_Osci_DataType_UInt8:        result   = "uint8";  break;
    case Ifx_Osci_DataType_boolean:        result = "boolean";  break;
    case Ifx_Osci_DataType_SInt64:        result  = "sint64";  break;
    case Ifx_Osci_DataType_UInt64:        result  = "uint64";  break;
    default:                        result        = "Unknown";     break;
    }

    return result;
}


float32 Ifx_Osci_valueGet(Ifx_Osci_Channel *channel)
{
    float32           value;
    Ifx_Osci_Channel *config = channel;

    switch (config->type)
    {
    case Ifx_Osci_DataType_Float32:
        value = *((const float32 *)config->source);
        break;
    case Ifx_Osci_DataType_SInt16:
        value = (float32)(*(const sint16 *)config->source);
        break;
    case Ifx_Osci_DataType_UInt16:
        value = (float32)(*(const uint16 *)config->source);
        break;
    case Ifx_Osci_DataType_SInt32:
        value = (float32)(*(const sint32 *)config->source);
        break;
    case Ifx_Osci_DataType_UInt32:
        value = (float32)(*(const uint32 *)config->source);
        break;
    case Ifx_Osci_DataType_FixPoint16:
        value = __fixpoint_to_float32((*(const sfract *)config->source), config->QFormatShift);
        break;
    case Ifx_Osci_DataType_FixPoint32:
        value = __fixpoint_to_float32((*(const fract *)config->source), config->QFormatShift);
        break;
    case Ifx_Osci_DataType_SInt8:
        value = (float32)(*(const sint8 *)config->source);
        break;
    case Ifx_Osci_DataType_UInt8:
        value = (float32)(*(const uint8 *)config->source);
        break;
    case Ifx_Osci_DataType_boolean:
        value = (float32)(*(boolean *)config->source);
        break;
    case Ifx_Osci_DataType_SInt64:
        value = (float32)(*(const sint64 *)config->source);
        break;
    case Ifx_Osci_DataType_UInt64:
        value = (float32)(*(const uint64 *)config->source);
        break;
    default:
        value = 0;
        break;
    }

    return value;
}


IFX_INLINE boolean risingEdgeDetected(Ifx_Osci_Trigger *trigger, float32 value)
{
    boolean risingEdge = (trigger->risingEdge != FALSE);
    risingEdge &= ((trigger->lastLevel < trigger->level) && (value >= trigger->level));
    return (risingEdge != FALSE) ? TRUE : FALSE;
}


IFX_INLINE boolean fallingEdgeDetected(Ifx_Osci_Trigger *trigger, float32 value)
{
    boolean fallingEdge = (trigger->risingEdge == FALSE);
    fallingEdge &= ((trigger->lastLevel > trigger->level) && (value <= trigger->level));
    return (fallingEdge != FALSE) ? TRUE : FALSE;
}


IFX_INLINE boolean activeEdgeDetected(Ifx_Osci_Trigger *trigger, float32 value)
{
    return fallingEdgeDetected(trigger, value) || risingEdgeDetected(trigger, value);
}


IFX_INLINE uint16 Ifx_Osci_getSamples(Ifx_Osci *osci, uint16 writeIndex)
{
    uint32 i;

    for (i = 0; i < osci->channelCount; i++)
    {
        Ifx_Osci_Channel *channel = &osci->channels[i];

        if (channel->source != NULL_PTR)
        {
            channel->values[writeIndex] = Ifx_Osci_valueGet(channel);
        }
    }

    return writeIndex + 1;
}


IFX_INLINE boolean Ifx_Osci_isActiveEdge(Ifx_Osci *osci)
{
    boolean           activeEdge = FALSE;
    Ifx_Osci_Channel *channel    = &osci->channels[osci->trigger.source];

    if (channel->source != NULL_PTR)
    {
        float32 value = Ifx_Osci_valueGet(channel);
        activeEdge              = activeEdgeDetected(&osci->trigger, value);
        osci->trigger.lastLevel = value;
    }

    return activeEdge;
}


static void Ifx_Osci_stepEdgeTriggered(Ifx_Osci *osci)
{
    // Process channel trigger when osci is idle
    if (osci->writeIndex == osci->dataLength)
    {
        /** In triggered mode, acquisition is restarted if:
         *  - Ifx_Osci_forceTrigger() is called, or
         *  - An active edge is detected */
        boolean restart = (osci->trigger.force == TRUE) || ((osci->trigger.enabled == TRUE) && (Ifx_Osci_isActiveEdge(osci) != FALSE));

        if (restart != FALSE)
        {
            osci->intervalTmp = 1; // start immediately
            osci->startTime   = now();
            osci->writeIndex  = 0;
            osci->readIndex   = 0;

            if (osci->trigger.single)
            {
                osci->trigger.enabled = FALSE;
            }

            if (osci->trigger.force)
            {
                osci->trigger.enabled = FALSE;
                osci->trigger.force   = FALSE;
            }
        }
    }

    // Acquire all channels
    if (osci->writeIndex < osci->dataLength)
    {
        osci->intervalTmp--;

        if (osci->intervalTmp == 0)
        {
            osci->intervalTmp = osci->interval;
            boolean istate = disableInterrupts();
            osci->writeIndex  = Ifx_Osci_getSamples(osci, osci->writeIndex);
            restoreInterrupts(istate);
        }
    }
}


void Ifx_Osci_stepForced(Ifx_Osci *osci)
{
    if (osci->trigger.force)
    {
        osci->trigger.mode    = Ifx_Osci_TriggerMode_normal;
        osci->trigger.enabled = FALSE;
        osci->trigger.force   = FALSE;
        osci->writeIndex      = 0;
        osci->startTime       = now();
        osci->readIndex       = 0;
    }

    if (osci->writeIndex < osci->dataLength)
    {
        boolean istate = disableInterrupts();
        osci->writeIndex = Ifx_Osci_getSamples(osci, osci->writeIndex);
        restoreInterrupts(istate);
    }
}


/**
 * \brief Step function, which shall be called every sampling period
 * The period was previously set by Ifx_Osci_init() function.
 *
 * \param osci Pointer to the oscilloscope object
 */
void Ifx_Osci_step(Ifx_Osci *osci)
{
    /** The Ifx_Osci_step() function is only active if oscilloscope was enabled by Ifx_Osci_run() function */
    if (osci->enabled != FALSE)
    {
        if (osci->trigger.mode == Ifx_Osci_TriggerMode_automatic)
        {
            osci->intervalTmp--;

            if (osci->intervalTmp == 0)
            {
                osci->intervalTmp = osci->interval;
                boolean istate = disableInterrupts();
                osci->writeIndex  = osci->writeIndex % osci->dataLength;

                if (osci->writeIndex == 0)
                {
                    osci->startTime = now();
                }

                if (osci->writeIndex == osci->readIndex)
                {
                    osci->readIndex++;
                    osci->readIndex = osci->readIndex % osci->dataLength;
                }

                osci->writeIndex = Ifx_Osci_getSamples(osci, osci->writeIndex);
                restoreInterrupts(istate);
            }
        }
        else
        {
            Ifx_Osci_stepEdgeTriggered(osci);
        }
    }
}


/**
 * \brief Set a channel using the given config
 *
 * NOTE: As alternative, use Ifx_Osci_setChannelSignal()
 *
 * \param osci Pointer to the oscilloscope object
 * \param channelIndex Index to the channel
 * \param config Pointer to a channel configuration
 */
boolean Ifx_Osci_setChannel(Ifx_Osci *osci, uint16 channelIndex, const Ifx_Osci_Channel_Config *config)
{
    Ifx_Osci_Channel *channel = &osci->channels[channelIndex];
    boolean           result;

    if (channel == NULL_PTR)
    {
        result = FALSE;
    }
    else
    {
        channel->source       = config->source;
        channel->type         = config->type;
        channel->QFormatShift = config->QFormatShift;
        result                = TRUE;
    }

    return result;
}


/**
 * \brief Initialize an oscilloscope object.
 * \param osci Pointer to the oscilloscope object
 * \param config Oscilloscope configuration
 */
boolean Ifx_Osci_init(Ifx_Osci *osci, Ifx_Osci_Config *config)
{
    uint32  i;
    boolean result;

    result = TRUE;

    Ifx_Osci_setSignalsConfig(osci, NULL_PTR);

    /* Channels initialisation */
    for (i = 0; i < IFX_CFG_OSCI_CHANNEL_COUNT; i++)
    {
        Ifx_Osci_Channel *channel = &osci->channels[i];
        channel->values       = &osci->values[i][0];
        channel->source       = NULL_PTR;
        channel->type         = Ifx_Osci_DataType_Unknown;
        channel->QFormatShift = 0;
        channel->osci         = osci;
    }

    osci->channelCount = config->channelCount;

    result            &= IFX_VALIDATE(IFX_VERBOSE_LEVEL_ERROR, config->dataLength <= IFX_CFG_OSCI_DATA_LENGTH);
    osci->dataLength   = config->dataLength;

    osci->samplePeriod = config->samplePeriod;

    {   /* Trigger initialisation */
        Ifx_Osci_Trigger *trigger = &osci->trigger;
        trigger->groupIndex = 0;
        trigger->itemIndex  = 0;
        trigger->mode       = config->triggerMode;
        trigger->risingEdge = TRUE;
        trigger->level      = 0;
        trigger->source     = 0;
        trigger->force      = FALSE;
        trigger->enabled    = FALSE;
        trigger->single     = FALSE;
        trigger->autoNext   = 0;
        trigger->baseTime   = osci->samplePeriod * TimeConst_1s * osci->dataLength * 2;
    }

    osci->enabled = FALSE;

    Ifx_Osci_setInterval(osci, 1);
    osci->startTime   = 0;
    osci->readIndex   = 0;
    osci->writeIndex  = osci->dataLength;
    osci->intervalTmp = 1;

#if IFX_CFG_OSCI_FFT_ENABLED
    result             &= IFX_VALIDATE(IFX_VERBOSE_LEVEL_ERROR, config->fftLength <= IFX_OSCI_FFT_LENGTH);
    result             &= IFX_VALIDATE(IFX_VERBOSE_LEVEL_ERROR, config->fftLength <= config->dataLength);

    osci->fft.enabled   = result;
    osci->fft.length    = config->fftLength;
    osci->fft.padLength = osci->fft.length - osci->dataLength;

#else
    osci->fft.enabled   = FALSE;
    osci->fft.fftLength = 0;
    osci->fft.padLength = 0;
#endif

    return result;
}


void Ifx_Osci_initConfig(Ifx_Osci_Config *config)
{
    config->samplePeriod = 0.001;
    config->dataLength   = IFX_CFG_OSCI_DATA_LENGTH;
    config->channelCount = IFX_CFG_OSCI_CHANNEL_COUNT;
    config->triggerMode  = Ifx_Osci_TriggerMode_automatic;
#if IFX_CFG_OSCI_FFT_ENABLED
    config->fftLength    = config->dataLength;
#else
    config->fftLength    = 2;
#endif
}


/**
 * \brief Set oscilloscope list of input signals configuration
 * \param osci Pointer to the oscilloscope object
 * \param config Pointer to input signal configuration table.
 * NOTE: It must be pointer to a global variable, not stack.
 */
void Ifx_Osci_setSignalsConfig(Ifx_Osci *osci, const Ifx_Osci_Signal_Config *config)
{
    /* Input signals configuration */
    osci->signals.count  = 0;
    osci->signals.config = config;

    while ((config != NULL_PTR) && (config->name != NULL_PTR))
    {
        osci->signals.count++;
        config = &config[1];
    }
}


/**
 * \brief Assign a signal into an oscilloscope channel
 * The signal configuration was set by calling Ifx_Osci_setSignalsConfig()
 *
 * \param osci Pointer to the oscilloscope object
 * \param osciChannel Oscilloscope channel number 0..(osci->channelCount-1)
 * \param signalIndex Signal index in the Ifx_Osci::signals::config table
 */
boolean Ifx_Osci_setChannelSignal(Ifx_Osci *osci, uint16 osciChannel, uint16 signalIndex)
{
    boolean result = FALSE;
    sint16  i;

    if (osciChannel < osci->channelCount)
    {
        for (i = 0; i < osci->signals.count; i++)
        {
            if (osci->signals.config[i].id == signalIndex)
            {
                result = Ifx_Osci_setChannel(osci, osciChannel, &osci->signals.config[i].config);
            }
        }
    }

    return result;
}


/** \brief Get the next data since the last reading
 * \param osci Pointer to the Ifx_Osci object
 * \param data Pointer to buffer which shall fits (sizeof(float32) * osci->channelCount) */
boolean Ifx_Osci_getNextData(Ifx_Osci *osci, float32 *data)
{
    boolean result;
    boolean istate = disableInterrupts();

    if (osci->trigger.mode == Ifx_Osci_TriggerMode_normal)
    {   /* triggered mode, can't dump except that the osci is idle */
        result = (osci->writeIndex == osci->dataLength) && (osci->writeIndex != osci->readIndex);

        if (result != FALSE)
        {
            int i;

            for (i = 0; i < osci->channelCount; i++)
            {
                data[i] = osci->values[i][osci->readIndex];
            }

            osci->readIndex++;
        }
    }
    else
    {   /* continuous mode, dump only when stopped */
        result = (osci->enabled == FALSE) && (osci->writeIndex != osci->dataLength);

        if (result != FALSE)
        {
            int i;

            for (i = 0; i < osci->channelCount; i++)
            {
                data[i] = osci->values[i][osci->readIndex];
            }

            osci->readIndex++;
            osci->readIndex = osci->readIndex % osci->dataLength;
            result         &= osci->writeIndex != osci->readIndex;
        }
    }

    restoreInterrupts(istate);
    return result;
}


/**
 * \brief Square wave lookup function with 50% duty cycle
 * \param angle in radian
 * \return square(angle)
 */
IFX_INLINE float32 Ifx_Lut_squareWave(float32 angle)
{   /* FIXME API NAME */
    return (angle < IFX_PI) ? 1.0f : -1.0f;
}


void Ifx_Osci_doSignalOperation(Ifx_Osci_Channel *result, Ifx_Osci_Channel *a, Ifx_Osci_Operation op, Ifx_Osci_SignalType type, float32 freq, float32 ampl, float32 offset, float32 phase)
{
    Ifx_Osci *osci = result->osci;
    sint16    i;

    freq = freq * osci->samplePeriod;

    for (i = 0; i < osci->dataLength; i++)
    {
        float32 value;

        if (type == Ifx_Osci_SignalType_square)
        {
            value = Ifx_Lut_squareWave(2.0 * IFX_PI * freq * i);
        }
        else
        {
            //value = sin((2.0 * IFX_PI * freq * i) + (phase / 180 * IFX_PI));
            value = (2.0 * IFX_LUT_ANGLE_PI * freq * i) + (phase / 180 * IFX_LUT_ANGLE_PI);
            value = Ifx_LutLSincosF32_cos(__roundf(value));
        }

        value = (ampl * value) + offset;

        switch (op)
        {
        case Ifx_Osci_Operation_add: result->values[i] = a->values[i] + value; break;
        case Ifx_Osci_Operation_sub: result->values[i] = a->values[i] - value; break;
        case Ifx_Osci_Operation_mul: result->values[i] = a->values[i] * value; break;
        case Ifx_Osci_Operation_div: result->values[i] = a->values[i] / value; break;
        case Ifx_Osci_Operation_clr: result->values[i] = 0; break;
        case Ifx_Osci_Operation_set:
        default:                     result->values[i] = value; break;
        }
    }
}


void Ifx_Osci_doChannelOperation(Ifx_Osci_Channel *result, Ifx_Osci_Channel *a, Ifx_Osci_Operation op, Ifx_Osci_Channel *b)
{
    Ifx_Osci *osci = result->osci;
    sint16    i;

    switch (op)
    {
    case Ifx_Osci_Operation_atan2:

        for (i = 0; i < osci->dataLength; i++)
        {
            result->values[i] = (float32)Ifx_LutAtan2F32_fxpAngle(a->values[i], b->values[i]);
        }

        break;
    case Ifx_Osci_Operation_add:

        for (i = 0; i < osci->dataLength; i++)
        {
            result->values[i] = a->values[i] + b->values[i];
        }

        break;
    case Ifx_Osci_Operation_sub:

        for (i = 0; i < osci->dataLength; i++)
        {
            result->values[i] = a->values[i] - b->values[i];
        }

        break;
    case Ifx_Osci_Operation_mul:

        for (i = 0; i < osci->dataLength; i++)
        {
            result->values[i] = a->values[i] * b->values[i];
        }

        break;
    case Ifx_Osci_Operation_div:

        for (i = 0; i < osci->dataLength; i++)
        {
            result->values[i] = a->values[i] / b->values[i];
        }

        break;
    case Ifx_Osci_Operation_clr:

        for (i = 0; i < osci->dataLength; i++)
        {
            result->values[i] = 0;
        }

        break;
    case Ifx_Osci_Operation_set:

        for (i = 0; i < osci->dataLength; i++)
        {
            result->values[i] = a->values[i];
        }

        break;
    default:
        break;
    }
}


#define PWR_DB(p) (20.0 * log10f(p))

Ifx_Osci_analyzeResult Ifx_Osci_analyze(Ifx_Osci_Channel *channel, sint32 span)
{
    Ifx_Osci              *osci = channel->osci;

    Ifx_Osci_analyzeResult result;

    /* Compensate DC-offset */
    result.dcOffset = VecAvg_f32(channel->values, osci->dataLength);
    VecOfs_f32(channel->values, result.dcOffset, osci->dataLength);

    if (osci->fft.enabled)
    {
        sint16 i;
        sint16 lowerIdx, upperIdx, peakIdx;

        /* Copy data buffer */
        /* Initializing buffer with osci->dataLength data */
        for (i = 0; i < osci->dataLength; i++)
        {   /* set with values from oscilloscope */
            IFX_Cf32_set(&osci->fft.in[i], channel->values[i], 0);
        }

        if (osci->fft.padLength > 0)
        {
            /* Zero padding */
            /* Zero-padding last osci->fft.padLength data */
            for ( ; i < osci->fft.length; i++)
            {   /* zero padding */
                IFX_Cf32_reset(&osci->fft.in[i]);
            }
        }

        /* Windowing */
        /* Applying data window osci->dataLength */
        Ifx_WndF32_apply(osci->fft.in, Ifx_g_WndF32_blackmanHarrisTable, osci->dataLength);

        /* Fourier transform */
        /* Executing FFT-2 osci->fft.length */
        Ifx_FftF32_radix2(osci->fft.out.X, osci->fft.in, osci->fft.length);

        CplxVecPwr_f32(osci->fft.out.X, osci->fft.length / 2);

        /* Total power at half spectrum */
        //VecGain_f32(osci->fft.out.pwr, 2.0,  osci->fft.length/2);
        result.pTot = VecSum_f32(osci->fft.out.pwr, osci->fft.length / 2);

        /* Peak power and frequency */
        result.pPeak = VecMaxIdx_f32(osci->fft.out.pwr, osci->fft.length / 2, &lowerIdx, &upperIdx);
        peakIdx      = (upperIdx + lowerIdx) / 2;
        result.fPeak = peakIdx * 1.0 / (osci->fft.length * osci->samplePeriod);

        /* Signal power */
        result.pSig = VecSum_f32(&osci->fft.out.pwr[peakIdx - span], 2 * (sint16)span + 1);

        /* Noise and Distortion */
        result.pnd = result.pTot - result.pSig;

        VecPwrdB_f32(osci->fft.out.pwr, osci->fft.length / 2);

        result.signalPeakDb            = PWR_DB(result.pPeak);
        result.signalNoiseDistortionDb = PWR_DB(result.pTot);
        result.noiseDistortionDb       = PWR_DB(result.pnd);
        result.sinad                   = PWR_DB(result.pTot / result.pnd);
        result.enob                    = (result.sinad - 1.76) / 6.02;
    }

    return result;
}
