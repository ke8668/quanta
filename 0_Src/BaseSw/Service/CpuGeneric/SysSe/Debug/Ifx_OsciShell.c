/**
 * \file Ifx_OsciShell.c
 * \brief Oscilloscope interface using Shell
 *
 *
 * \version disabled
 * \copyright Copyright (c) 2013 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 */

#include "Ifx_OsciShell.h"
#include "Ifx_Osci.h"
#include "SysSe/Math/Ifx_FftF32.h"
#include "SysSe/Math/Ifx_WndF32.h"
#include "SysSe/Bsp/Bsp.h"

#define BOOLEAN_STR(v) (((v) != FALSE) ? "TRUE" : "FALSE")

static boolean Ifx_OsciShell_run(pchar arguments, void *data, IfxStdIf_DPipe *io);
static boolean Ifx_OsciShell_stop(pchar arguments, void *data, IfxStdIf_DPipe *io);
static boolean Ifx_OsciShell_trigger(pchar arguments, void *data, IfxStdIf_DPipe *io);
static boolean Ifx_OsciShell_status(pchar arguments, void *data, IfxStdIf_DPipe *io);
static boolean Ifx_OsciShell_channel(pchar arguments, void *data, IfxStdIf_DPipe *io);
static boolean Ifx_OsciShell_analyze(pchar arguments, void *data, IfxStdIf_DPipe *io);
static boolean Ifx_OsciShell_simulate(pchar arguments, void *data, IfxStdIf_DPipe *io);
static boolean Ifx_OsciShell_dump(pchar arguments, void *data, IfxStdIf_DPipe *io);

/* *INDENT-OFF* */
Ifx_Shell_Command Ifx_g_OsciShell_commands[] =
{
    {IFX_OSCI_SHELL_PREFIX, "     : Oscilloscope functions"
            , NULL_PTR, NULL_PTR               },
    {"run", "      : run the oscilloscope"ENDL
        "/s run [single|auto|normal]"
            , NULL_PTR, &Ifx_OsciShell_run     },
    {"stop", "     : stop oscilloscope"
            , NULL_PTR, &Ifx_OsciShell_stop    },
    {"trigger", "  : trigger settings"ENDL
        "/s trigger [force|single|level <value>|raising|falling|source <chNum>|interval <value>]"ENDL
            "/p force: Force the trigger"ENDL
            "/p single: Single the trigger"ENDL
            "/p level <value>: Set trigger level to value"ENDL
            "/p [raising|falling]: Set trigger to raising or falling edges"ENDL
            "/p source <chNum>: Set trigger channel number"ENDL
            "<chNum> can be obtained with 'channel' command"ENDL
            "/p interval <value>: Set sampling interval to value"

            , NULL_PTR, &Ifx_OsciShell_trigger },
    {"status", "   : display status"
            , NULL_PTR, &Ifx_OsciShell_status  },
    {"channel", "  : channel information"ENDL
            "/s channel: Show the channels information"ENDL
            "/s channel <ch> <sig>: Assign a signal to a channel"
            , NULL_PTR, &Ifx_OsciShell_channel },
    {"analyze", "  : signal analysis of a ch"ENDL
        "/s analyze <ch>: analyze the input channel <ch>"
            , NULL_PTR, &Ifx_OsciShell_analyze },
    {"simulate", " : simulate a signal"ENDL
        "/s simulate <ch> <op> <wave> <freq> <ampl> <offs> <phase> : Simulate a signal generation"ENDL
            "/p <ch>   : {0 .. Channel count}"ENDL
            "/p <op>   : {add,sub,mul,clr}"ENDL
            "/p <wave> : {sine,square}"ENDL
            "/p <freq> : -INF .. INF"ENDL
            "/p <offs> : -INF .. INF"ENDL
            "/p <ampl> : -INF .. INF"ENDL
            "/p <phase>: 0 .. 360 [degree]"ENDL
        "/s simulate <ch> atan <ych> <xch>: Compute atan(ych/xch) and store in channel <ch>"
            , NULL_PTR, &Ifx_OsciShell_simulate},
    {"dump", "     : dump values (all channels)"ENDL
        "/s dump <ch> [hex]"ENDL
            "/p <ch>: Dump the values of the channel <ch>"ENDL
            "/p hex: If hex is specified, values will be in hexadecimal"
            , NULL_PTR, &Ifx_OsciShell_dump    },
    IFX_SHELL_COMMAND_LIST_END,
};
/* *INDENT-ON* */

//, (osci->channelCount-1));
static boolean Ifx_OsciShell_run(pchar arguments, void *data, IfxStdIf_DPipe *io)
{
    Ifx_OsciShell *osciShell;
    osciShell = (Ifx_OsciShell *)data;
    Ifx_Osci      *osci = osciShell->osci;

    Ifx_Osci_run(osci);

    if (Ifx_Shell_matchToken(&arguments, "single") != FALSE)
    {
        Ifx_Osci_enableSingleTrigger(osci);
    }
    else if (Ifx_Shell_matchToken(&arguments, "auto") != FALSE)
    {
        Ifx_Osci_enableModeAuto(osci, NULL_PTR);
    }
    else if (Ifx_Shell_matchToken(&arguments, "normal") != FALSE)
    {
        Ifx_Osci_enableEdgeTrigger(osci);
    }
    else
    {
        Ifx_Osci_run(osci);
    }

    return TRUE;
}


static boolean Ifx_OsciShell_trigger(pchar arguments, void *data, IfxStdIf_DPipe *io)
{
    Ifx_OsciShell *osciShell;
    osciShell = (Ifx_OsciShell *)data;
    Ifx_Osci      *osci = osciShell->osci;

    if (Ifx_Shell_matchToken(&arguments, "interval") != FALSE)
    {
        sint32 interval;

        if (Ifx_Shell_parseSInt32(&arguments, &interval) != FALSE)
        {
            Ifx_Osci_setInterval(osci, (uint16)interval);
        }
    }
    else if (Ifx_Shell_matchToken(&arguments, "level") != FALSE)
    {
        float32 level;

        if (Ifx_Shell_parseFloat32(&arguments, &level) != FALSE)
        {
            Ifx_Osci_setEdgeTriggerLevel(osci, level);
        }
    }
    else if (Ifx_Shell_matchToken(&arguments, "source") != FALSE)
    {
        sint32 source;

        if (Ifx_Shell_parseSInt32(&arguments, &source) != FALSE)
        {
            Ifx_Osci_setEdgeTriggerSource(osci, source);
        }
    }
    else if (Ifx_Shell_matchToken(&arguments, "force") != FALSE)
    {
        Ifx_Osci_forceTrigger(osci);
    }
    else
    {
        IfxStdIf_DPipe_print(io, "Trigger settings:"ENDL);
        IfxStdIf_DPipe_print(io, " * mode: %s"ENDL, (osci->trigger.mode == Ifx_Osci_TriggerMode_automatic ? "Auto" : "Normal"));
        IfxStdIf_DPipe_print(io, " * single: %s"ENDL, (osci->trigger.single ? "TRUE" : "FALSE"));
        IfxStdIf_DPipe_print(io, " * edge: %s"ENDL, (osci->trigger.risingEdge ? "Rising" : "Falling"));
        IfxStdIf_DPipe_print(io, " * level: %f"ENDL, osci->trigger.level);
        IfxStdIf_DPipe_print(io, " * interval: %d"ENDL, osci->interval);
    }

    return TRUE;
}


boolean Ifx_OsciShell_init(Ifx_OsciShell *osciShell, Ifx_OsciShell_Config *config)
{
    uint32 index;

    index           = 0;
    osciShell->osci = config->osci;

    while (Ifx_g_OsciShell_commands[index].commandLine != NULL_PTR)
    {
        Ifx_g_OsciShell_commands[index].data = osciShell;
        index++;
    }

    return TRUE;
}


void Ifx_OsciShell_initConfig(Ifx_OsciShell_Config *config, Ifx_Osci *osci)
{
    config->osci = osci;
}


static boolean Ifx_OsciShell_stop(pchar arguments, void *data, IfxStdIf_DPipe *io)
{
    Ifx_OsciShell *osciShell;
    osciShell = (Ifx_OsciShell *)data;
    Ifx_Osci      *osci = osciShell->osci;

    Ifx_Osci_stop(osci);
    return TRUE;
}


#define PWR_DB(p) (20.0 * log10f(p))
//#define PWR_DB(p) (p)

#define SIG_SPAN (5)

static boolean Ifx_OsciShell_analyze(pchar arguments, void *data, IfxStdIf_DPipe *io)
{
    Ifx_OsciShell *osciShell;
    osciShell = (Ifx_OsciShell *)data;
    Ifx_Osci      *osci = osciShell->osci;

    sint32         chnr, span;

    if ((Ifx_Shell_parseSInt32(&arguments, &chnr) != FALSE) && (chnr < osci->channelCount))
    {
        Ifx_Osci_Channel *channel = &(osci->channels[chnr]);
        float32           offset;

        if (Ifx_Shell_parseSInt32(&arguments, &span) == FALSE)
        {
            span = 5;
        }

        IfxStdIf_DPipe_print(io, "Analyzing channel %d"ENDL, chnr);

        /* Compensate DC-offset */
        offset = VecAvg_f32(channel->values, osci->dataLength);
        IfxStdIf_DPipe_print(io, " * DC offset is %f (estimated) "ENDL, offset);
        VecOfs_f32(channel->values, offset, osci->dataLength);

        if (osci->fft.enabled)
        {
            sint16  i;
            sint16  lowerIdx, upperIdx, peakIdx;
            float32 Ptot, Ppeak, Psig, Pnd, freq0, sinad;

            /* Copy data buffer */
            IfxStdIf_DPipe_print(io, " * Initializing buffer with %d data"ENDL, osci->dataLength);

            for (i = 0; i < osci->dataLength; i++)
            {   /* set with values from oscilloscope */
                IFX_Cf32_set(&osci->fft.in[i], channel->values[i], 0);
            }

            if (osci->fft.padLength > 0)
            {
                /* Zero padding */
                IfxStdIf_DPipe_print(io, " * Zero-padding last %d data"ENDL, osci->fft.padLength);

                for ( ; i < osci->fft.length; i++)
                {   /* zero padding */
                    IFX_Cf32_reset(&osci->fft.in[i]);
                }
            }

            /* Windowing */
            IfxStdIf_DPipe_print(io, " * Applying data window %d"ENDL, osci->dataLength);
            Ifx_WndF32_apply(osci->fft.in, Ifx_g_WndF32_blackmanHarrisTable, osci->dataLength);

            /* Fourier transform */
            IfxStdIf_DPipe_print(io, " * Executing FFT-2 %d"ENDL, osci->fft.length);
            Ifx_FftF32_radix2(osci->fft.out.X, osci->fft.in, osci->fft.length);

            CplxVecPwr_f32(osci->fft.out.X, osci->fft.length / 2);

            /* Total power at half spectrum */
            //VecGain_f32(osci->fft.out.pwr, 2.0,  osci->fft.length/2);
            Ptot = VecSum_f32(osci->fft.out.pwr, osci->fft.length / 2);

            /* Peak power and frequency */
            Ppeak   = VecMaxIdx_f32(osci->fft.out.pwr, osci->fft.length / 2, &lowerIdx, &upperIdx);
            peakIdx = (upperIdx + lowerIdx) / 2;
            freq0   = peakIdx * 1.0 / (osci->fft.length * osci->samplePeriod);

            /* Signal power */
            Psig = VecSum_f32(&osci->fft.out.pwr[peakIdx - span], 2 * (sint16)span + 1);

            /* Noise and Distortion */
            Pnd = Ptot - Psig;

            VecPwrdB_f32(osci->fft.out.pwr, osci->fft.length / 2);

            /* Report results */
            IfxStdIf_DPipe_print(io, " * Signal peak = %f dB at %f Hz"ENDL, PWR_DB(Ppeak), freq0);
            IfxStdIf_DPipe_print(io, " * Signal + Noise + Distortion = %f dB"ENDL, PWR_DB(Ptot));
            IfxStdIf_DPipe_print(io, " * Noise + Distortion = %f dB"ENDL, PWR_DB(Pnd));
            IfxStdIf_DPipe_print(io, " * SINAD = %f dB"ENDL, (sinad = PWR_DB(Ptot / Pnd)));
            IfxStdIf_DPipe_print(io, " * ENOB = %f bits"ENDL, ((sinad - 1.76) / 6.02));
        }
    }
    else
    {
        IfxStdIf_DPipe_print(io, "ERROR: channel number is out of range"ENDL);
    }

    return TRUE;
}


static boolean Ifx_OsciShell_channel(pchar arguments, void *data, IfxStdIf_DPipe *io)
{
    Ifx_OsciShell *osciShell;
    osciShell = (Ifx_OsciShell *)data;
    Ifx_Osci      *osci = osciShell->osci;

    sint16         i;

    sint32         channel, isignal;

    if (Ifx_Shell_parseSInt32(&arguments, &channel) != FALSE)
    {
        if (channel < osci->channelCount)
        {
            if (Ifx_Shell_parseSInt32(&arguments, &isignal) != FALSE)
            {
                if (Ifx_Osci_setChannelSignal(osci, (sint16)channel, (sint16)isignal) != FALSE)
                {
                    IfxStdIf_DPipe_print(io, "Signal %d is assigned to channel %d"ENDL, isignal, channel);
                }
                else
                {
                    IfxStdIf_DPipe_print(io, "ERROR: signal number is unknown"ENDL);
                }
            }
            else
            {
                IfxStdIf_DPipe_print(io, "ERROR: please provide signal number"ENDL);
            }
        }
        else
        {
            IfxStdIf_DPipe_print(io, "ERROR: channel number is out of range"ENDL);
        }
    }
    else
    {
        IfxStdIf_DPipe_print(io, "Available channels:"ENDL);

        for (i = 0; i < osci->channelCount; i++)
        {
            Ifx_Osci_DataType type   = osci->channels[i].type;
            const void       *source = osci->channels[i].source;
            IfxStdIf_DPipe_print(io, "Channel %d:", i);
            IfxStdIf_DPipe_print(io, " source = %08X (%s),", source, Ifx_Osci_getSignalName(osci, source));
            IfxStdIf_DPipe_print(io, " type = %d (%s)"ENDL, type, Ifx_Osci_getDataTypeName(type));
        }

        IfxStdIf_DPipe_print(io, ENDL "Available signals:"ENDL);

        for (i = 0; i < osci->signals.count; i++)
        {
            IfxStdIf_DPipe_print(io, "Signal %2d: %s"ENDL, osci->signals.config[i].id, osci->signals.config[i].name);
        }
    }

    return TRUE;
}


static boolean Ifx_OsciShell_simulate(pchar arguments, void *data, IfxStdIf_DPipe *io)
{
    Ifx_OsciShell      *osciShell;
    osciShell = (Ifx_OsciShell *)data;
    Ifx_Osci           *osci = osciShell->osci;

    sint32              chnr;
    float32             freq, ampl, offset, phase;
    Ifx_Osci_Operation  op;
    Ifx_Osci_SignalType type;

    /* first argument is oscilloscope channel number */
    Ifx_Shell_parseSInt32(&arguments, &chnr);

    /* second argument is [add,sub,mul] which indicates signal operation */
    if (Ifx_Shell_matchToken(&arguments, "atan") != FALSE)
    {
        sint32 ych, xch;

        if (Ifx_Shell_parseSInt32(&arguments, &ych) == FALSE)
        {
            ych = 1;
        }

        if (Ifx_Shell_parseSInt32(&arguments, &xch) == FALSE)
        {
            xch = 0;
        }

        Ifx_Osci_doChannelOperation(&osci->channels[chnr], &osci->channels[xch], Ifx_Osci_Operation_atan2, &osci->channels[ych]);
    }
    else
    {
        if (Ifx_Shell_matchToken(&arguments, "add") != FALSE)
        {
            op = Ifx_Osci_Operation_add;
        }
        else if (Ifx_Shell_matchToken(&arguments, "sub") != FALSE)
        {
            op = Ifx_Osci_Operation_sub;
        }
        else if (Ifx_Shell_matchToken(&arguments, "mul") != FALSE)
        {
            op = Ifx_Osci_Operation_mul;
        }
        else if (Ifx_Shell_matchToken(&arguments, "div") != FALSE)
        {
            op = Ifx_Osci_Operation_div;
        }
        else if (Ifx_Shell_matchToken(&arguments, "clr") != FALSE)
        {
            op = Ifx_Osci_Operation_clr;
        }
        else if (Ifx_Shell_matchToken(&arguments, "set") != FALSE)
        {
            op = Ifx_Osci_Operation_set;
        }
        else
        {
            op = Ifx_Osci_Operation_clr;
        }

        /* third argument is [sine,square] which indicates the signal type */
        if (Ifx_Shell_matchToken(&arguments, "sine") != FALSE)
        {
            type = Ifx_Osci_SignalType_sine;
        }
        else if (Ifx_Shell_matchToken(&arguments, "square") != FALSE)
        {
            type = Ifx_Osci_SignalType_square;
        }
        else
        {
            type = Ifx_Osci_SignalType_sine;
        }

        /* last arguments are <freq> <ampl> <offs> which are amplitude and frequency */
        if (Ifx_Shell_parseFloat32(&arguments, &freq) == FALSE)
        {
            freq = 1.0;
        }

        if (Ifx_Shell_parseFloat32(&arguments, &ampl) == FALSE)
        {
            ampl = 1.0;
        }

        if (Ifx_Shell_parseFloat32(&arguments, &offset) == FALSE)
        {
            offset = 0.0;
        }

        if (Ifx_Shell_parseFloat32(&arguments, &phase) == FALSE)
        {
            phase = 0.0;
        }

        chnr = __max(__min(chnr, osci->channelCount - 1), 0);
        {
            Ifx_Osci_doSignalOperation(&osci->channels[chnr], &osci->channels[chnr], op, type, freq, ampl, offset, phase);
        }
    }

    return TRUE;
}


static boolean Ifx_OsciShell_dump(pchar arguments, void *data, IfxStdIf_DPipe *io)
{
    Ifx_OsciShell *osciShell;
    osciShell = (Ifx_OsciShell *)data;
    Ifx_Osci      *osci    = osciShell->osci;

    boolean        result  = TRUE;
    float32        values[osci->channelCount];
    sint32         startCh = 0, endCh = osci->channelCount;

    if (Ifx_Shell_parseSInt32(&arguments, &startCh) != FALSE)
    {
        result = (startCh < osci->channelCount) ? TRUE : FALSE;
        endCh  = startCh;
    }

    boolean hexMode = (Ifx_Shell_matchToken(&arguments, "hex") != FALSE);

    if (result != FALSE)
    {
        sint32 i;

        for (i = startCh; i < endCh; i++)
        {
            IfxStdIf_DPipe_print(io, "%s\t", Ifx_Osci_getSignalName(osci, osci->channels[i].source));
        }

        IfxStdIf_DPipe_print(io, ENDL);
        uint32 checksum = 0;

        while (Ifx_Osci_getNextData(osci, values) != FALSE)
        {
            for (i = startCh; i < endCh; i++)
            {
                uint32 asInteger = (*((uint32 *)&(values[i])));

                if (hexMode)
                {
                    IfxStdIf_DPipe_print(io, "%08X\t", asInteger);
                }
                else
                {
                    IfxStdIf_DPipe_print(io, "%e\t", values[i]);
                }

                checksum = checksum + asInteger;
            }

            wait(TimeConst_1ms * 4);

            IfxStdIf_DPipe_print(io, ENDL);
            i++;

            if (i > osci->dataLength)
            {
                IfxStdIf_DPipe_print(io, "Overflow error"ENDL);
                break;
            }
        }

        if (hexMode)
        {
            IfxStdIf_DPipe_print(io, "Checksum = %08X"ENDL, checksum);
        }
    }
    else
    {
        IfxStdIf_DPipe_print(io, "ERROR: invalid channel number %d, range is 0 .. %d", startCh, osci->channelCount - 1);
    }

    return TRUE;
}


static boolean Ifx_OsciShell_status(pchar arguments, void *data, IfxStdIf_DPipe *io)
{
    Ifx_OsciShell    *osciShell;
    osciShell = (Ifx_OsciShell *)data;
    Ifx_Osci         *osci    = osciShell->osci;

    Ifx_Osci_Trigger *trigger = &osci->trigger;
    IfxStdIf_DPipe_print(io, " * Sampling freq: %f Hz"ENDL, (1.0 / osci->samplePeriod));
    IfxStdIf_DPipe_print(io, " * Enabled: %s"ENDL, BOOLEAN_STR(osci->enabled));
    IfxStdIf_DPipe_print(io, " * Triggered: %s"ENDL, BOOLEAN_STR(trigger->enabled));
    IfxStdIf_DPipe_print(io, " * Automatic: %s"ENDL, BOOLEAN_STR(trigger->mode == Ifx_Osci_TriggerMode_automatic));
    IfxStdIf_DPipe_print(io, " * Rising-edge: %s"ENDL, BOOLEAN_STR(trigger->risingEdge));

    return TRUE;
}
