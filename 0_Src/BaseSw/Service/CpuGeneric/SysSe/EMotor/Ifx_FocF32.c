/**
 * \file Ifx_FocF32.c
 * \brief Field Oriented Control.
 *
 *
 * \version disabled
 * \copyright Copyright (c) 2013 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 */

//------------------------------------------------------------------------------
#include "SysSe/EMotor/Ifx_FocF32.h"
#include "SysSe/EMotor/Ifx_SvmF32.h"
#include "SysSe/EMotor/Ifx_ClarkeF32.h"
#include "SysSe/EMotor/Ifx_ParkF32.h"
#include "SysSe/Math/Ifx_LutAtan2F32.h"
//------------------------------------------------------------------------------

IFX_INLINE void Ifx_FocF32_resetPic(Pic_Signals *pic)
{
    pic->ik = 0.0;
    pic->uk = 0.0;
}


/** \brief Calculate FOC runtime parameters */
void Ifx_FocF32_calcParams(Ifx_FocF32_Params *params, const Ifx_FocF32_Config *config, float32 Ts)
{
    params->piD.kp                = config->piD.kp;
    params->piD.KiDt              = config->piD.ki * Ts / 2.0;

    params->piQ.kp                = config->piQ.kp;
    params->piQ.KiDt              = config->piQ.ki * Ts / 2.0;

    params->vdcNom                = config->vdcNom;

    params->fwFbMax               = config->fwFbMax;
#if IFX_FOCF32_VOLTAGE_IN_VOLTS == 0
    params->kiFwDt                = config->kiFw * Ts;
#else
    params->kiFwDt                = config->kiFw * Ts / (config->vdcNom * IFX_ONE_OVER_SQRT_THREE) * IFX_SVMF32_MAX_AMPLITUDE;
#endif
    params->fieldWeakeningEnabled = config->fieldWeakeningEnabled;
}


/** \brief Get actual FOC configuration */
void Ifx_FocF32_getConfig(Ifx_FocF32 *self, Ifx_FocF32_Config *config, float32 Ts)
{
    const Ifx_FocF32_Params *p = self->params;

    if (p != NULL_PTR)
    {
        config->piD.kp  = p->piD.kp;
        config->piD.ki  = p->piD.KiDt * 2.0 / Ts;

        config->piQ.kp  = p->piQ.kp;
        config->piQ.ki  = p->piQ.KiDt * 2.0 / Ts;

        config->vdcNom  = self->vdcNom;
        config->fwFbMax = p->fwFbMax;
#if IFX_FOCF32_VOLTAGE_IN_VOLTS == 0
        config->kiFw    = p->kiFwDt / Ts;
#else
        config->kiFw    = p->kiFwDt / (Ts / (config->vdcNom * IFX_ONE_OVER_SQRT_THREE) * IFX_SVMF32_MAX_AMPLITUDE);
#endif
    }
    else
    {
        config->piD.ki                                           = config->piD.kp =
                                                  config->piQ.ki = config->piQ.kp = 0.0;
        config->vdcNom                                           = 0;
        config->fwFbMax                                          = 1.0;
#if IFX_FOCF32_VOLTAGE_IN_VOLTS == 0
        config->kiFw                                             = 2.5;
#else
        config->kiFw                                             = 2.5;
#endif
    }

    config->fieldWeakeningEnabled = p->fieldWeakeningEnabled;
}


/** \brief Set the runtime parameters */
void Ifx_FocF32_setParams(Ifx_FocF32 *self, const Ifx_FocF32_Params *params)
{
    self->params = params;
}


/** \brief Initialize the Ifx_FocF32 object.
 *
 * This function initialises the Ifx_FocF32 object:
 *    - piD and piQ parameters are set to default values as done by Pic_init()
 *    - the Id and Iq references and limits are set to 0
 *    - internal signals are set to 0
 *
 * \param self Specifies the field oriented control object.
 * \param params Pointer to the Ifx_FocF32_Params object, e.g. calculated by Ifx_FocF32_calcParams()
 *
 * \return none
 * \note The kp, ki and limits of the PI controllers must be set separately.
 */
boolean Ifx_FocF32_init(Ifx_FocF32 *self, const Ifx_FocF32_Params *params)
{
    Ifx_FocF32_setParams(self, params);

    IFX_Cf32_reset(&self->mdqFf);
    IFX_Cf32_reset(&self->ref);
#if IFX_FOCF32_DEBUG
    IFX_Cf32_reset(&self->iab);
#endif
    Ifx_FocF32_reset(self);

    self->idqLimit       = 0.0;
#if IFX_FOCF32_DEBUG
    self->iuvw[0]        = 0.0;
    self->iuvw[1]        = 0.0;
    self->iuvw[2]        = 0.0;
#endif
    self->elAngle.input  = 0;
    self->elAngle.output = 0;
    self->elAngle.offset = 0;

#if IFX_FOCF32_VOLTAGE_IN_VOLTS == 0
    self->fw.mdqThres = IFX_FOCF32_VDQ_THRESHOLD * IFX_SVMF32_MAX_AMPLITUDE;
#else
    self->fw.mdqThres = IFX_FOCF32_VDQ_THRESHOLD * params->vdcNom * IFX_ONE_OVER_SQRT_THREE;
#endif

    return TRUE;
}


/** \brief Reset the FOC PI controllers.
 *
 * The piD and piQ parameters are set to default values by Pic_reset()
 *
 * \param self Specifies the field oriented control object.
 *
 * \return None.
 */
void Ifx_FocF32_reset(Ifx_FocF32 *self)
{
    Ifx_FocF32_resetPic(&self->piD);
    Ifx_FocF32_resetPic(&self->piQ);
    IFX_Cf32_reset(&self->mdq);
    self->fw.fbValue     = 0.0;
    self->elAngle.input  = 0;
    self->elAngle.output = 0;
    self->elAngle.offset = 0;

#if IFX_FOCF32_DEBUG
    IFX_Cf32_reset(&self->mab);
#endif
}


/** \brief Field Oriented Control input step function.
 *
 * This function performs input step of the FOC calculations and returns the output of
 * the reverse park transformation.
 *
 * \param self Specifies the field oriented control object.
 * \param elAngle Electrical angle for forward park transformation
 * \param currents Pointer to currents array
 */
void Ifx_FocF32_stepIn(Ifx_FocF32 *self, Ifx_Lut_FxpAngle elAngle, const float32 *currents)
{
    cfloat32 cossin, iab;

    /* Clarke transformation */
    iab = Ifx_ClarkeF32(currents);

    /* save for informational purpose */
#if IFX_FOCF32_DEBUG
    self->iab           = iab;
    self->iuvw[0]       = currents[0];
    self->iuvw[1]       = currents[1];
    self->iuvw[2]       = currents[2];
#endif
    self->elAngle.input = elAngle;

    /* Lookup the cos and sin values */
    cossin = Ifx_LutLSincosF32_cossin(elAngle);

    /* Park transformation */
    self->idq = Ifx_ParkF32_forward(&iab, &cossin);
}


#if IFX_FOCF32_VOLTAGE_IN_VOLTS == 0
#define IFX_FOCF32_VOLTAGE_LIMIT (IFX_SVMF32_MAX_AMPLITUDE)
#else
#define IFX_FOCF32_VOLTAGE_LIMIT (self->vdcNom * IFX_ONE_OVER_SQRT_THREE)
#endif

/** \brief Field Oriented Control output step function.
 *
 * This function performs output step of the FOC calculations and returns the output of
 * the reverse park transformation.
 *
 * \param self Specifies the field oriented control object.
 * \param elAngle Electrical angle for inverse park transformation
 * \return mab (AB voltage).
 */
cfloat32 Ifx_FocF32_stepOut(Ifx_FocF32 *self, Ifx_Lut_FxpAngle elAngle)
{
    cfloat32                  mab;
    cfloat32                  mdqLim, cossin;
    cfloat32                  mdq, ierr;
    const Ifx_FocF32_Params *p = self->params;

    /* Error value -----------------------------------------------------------------*/
    ierr = IFX_Cf32_sub(&self->ref, &self->idq);

    /* Integration with Trapezoidal approximation ----------------------------------*/
    if (self->vdqLimitHit == FALSE)
    {   /* integrate only when limit is not hit */
        self->piD.uk += (p->piD.KiDt * (ierr.real + self->piD.ik));
        self->piQ.uk += (p->piQ.KiDt * (ierr.imag + self->piQ.ik));
    }

    /* Keep previous error value ---------------------------------------------------*/
    self->piD.ik = ierr.real;
    self->piQ.ik = ierr.imag;

    /* Proportional and Integrated -------------------------------------------------*/
    mdq.real = (p->piD.kp * ierr.real) + self->piD.uk;
    mdq.imag = (p->piQ.kp * ierr.imag) + self->piQ.uk;

    /* Add the feed-forward voltage ------------------------------------------------*/
    mdq.real += self->mdqFf.real;
    mdq.imag += self->mdqFf.imag;

    /* Magnitude of DQ voltage */
    self->mdqMag = IFX_Cf32_mag(&self->mdq);

    /* Limit the voltage -----------------------------------------------------------*/
    {   // The voltage amplitude is saturated to a maximum while the angle is kept.
        float32 limit = IFX_FOCF32_VOLTAGE_LIMIT;
        self->vdqLimitHit = !(self->mdqMag < limit);

        if (!p->fieldWeakeningEnabled)
        {
            if (!(self->mdqMag < limit))
            {   /** scale the voltage when hit limit */
                float32 scale = limit / self->mdqMag;
                mdqLim.imag = mdq.imag * scale;
                mdqLim.real = mdq.real * scale;
            }
            else
            {
                mdqLim = mdq;
            }
        }
        else
        {
            mdqLim = mdq;

            /* Feedback for field-weakening ------------------------------------------------*/
            {   // it's an integrating function which can be used to reduce the flux reference
                // or d-axis current.
                float32 margin     = self->fw.mdqThres - self->mdqMag;
                float32 fwFeedback = self->fw.fbValue + (margin * p->kiFwDt);
                self->fw.mdqMargin = margin;
                self->fw.fbValue   = __saturatef(fwFeedback, -p->fwFbMax, 0);
            }
        }

        self->mdq = mdqLim;
    }

    /* Save for informational purpose --------------------------------------------- */
    self->elAngle.output = elAngle;
#if IFX_FOCF32_DEBUG_2
    self->marg           = Ifx_LutAtan2F32_fxpAngle(mdqLim.real, mdqLim.imag);
#endif

    /* Reverse Park transformation ------------------------------------------------ */
    cossin    = Ifx_LutLSincosF32_cossin(elAngle);
    mab       = Ifx_ParkF32_reverse(&mdqLim, &cossin);
#if IFX_FOCF32_DEBUG
    self->mab = mab;
#endif
    return mab;
}


/** \brief Get the actual Idq values.
 *
 * \param self Specifies the field oriented control object.
 *
 * \return actual DQ current values in Ampere
 */
cfloat32 Ifx_FocF32_getIdq(Ifx_FocF32 *self)
{
    return self->idq;
}


/** \brief Get the DQ current reference.
 *
 * \param self Specifies the field oriented control object.
 *
 * \return DQ current reference in Ampere
 * \ingroup library_srvsw_sysse_emotor_foc
 */
cfloat32 Ifx_FocF32_getIdqRef(Ifx_FocF32 *self)
{
    return self->ref;
}


/** \brief Set the Idq current reference.
 *
 * This function sets the Idq current reference. The Idq reference value is checked against the Idq limit.
 *
 * \param self Specifies the field oriented control object.
 * \param ref Specifies Idq current reference.
 *
 * \return none
 * \ingroup library_srvsw_sysse_emotor_foc
 */
void Ifx_FocF32_setIdqRef(Ifx_FocF32 *self, cfloat32 ref)
{
    float32 ampl;
    self->ref = IFX_Cf32_saturate(&ref, &ampl, self->idqLimit);
}


/** \brief Set the Idq current limit.
 *
 * This function sets the Iq current limit.
 *
 * \param self Specifies the field oriented control object.
 * \param limit Specifies Idq current limit.
 *
 * \return none
 */
void Ifx_FocF32_setIdqLimit(Ifx_FocF32 *self, float32 limit)
{
    self->idqLimit = limit;
}


/** \brief Return the Idq current limit.
 *
 * This function returns the Idq current limit.
 *
 * \param self Specifies the field oriented control object.
 *
 * \return Returns the Idq current limit
 */
float32 Ifx_FocF32_getIdqLimit(Ifx_FocF32 *self)
{
    return self->idqLimit;
}
