/**
 * \file Ifx_MotorControl.h
 * \brief Electrical motor control global definition.
 *
 *
 * \version disabled
 * \copyright Copyright (c) 2013 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * \defgroup library_srvsw_sysse_emotor_motorControl Electrical motor control global definition.
 * \ingroup library_srvsw_sysse_emotor
 *
 */
#ifndef IFX_MOTORCONTROL_H_
#define IFX_MOTORCONTROL_H_

#include "StdIf/IfxStdIf_DPipe.h"

/** \brief Motor control modes */
typedef enum
{
    Ifx_MotorControl_Mode_passive          = 0, /**< \brief Passive mode (control is stopped) */
    Ifx_MotorControl_Mode_currentControl   = 1, /**< \brief Current control mode */
    Ifx_MotorControl_Mode_torqueControl    = 2, /**< \brief Torque control mode */
    Ifx_MotorControl_Mode_speedControl     = 3, /**< \brief Speed control mode */
    Ifx_MotorControl_Mode_openLoop         = 4, /**< \brief Open-loop mode */
    Ifx_MotorControl_Mode_openWithPosition = 5, /**< \brief Open-loop mode using position sensor*/
    Ifx_MotorControl_Mode_testPI           = 6, /**< \brief PI-controller test mode */
	Ifx_MotorControl_Mode_off			   = 0xF,
    /* FIXME add mode Ifx_MotorControl_Mode_error, that is entered in case of error detected, and exit on request only if error is cleared */
} Ifx_MotorControl_Mode;

void Ifx_MotorControl_Mode_print(IfxStdIf_DPipe *io, Ifx_MotorControl_Mode mode);

#endif /* IFX_MOTORCONTROL_H_ */
