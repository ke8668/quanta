/**
 * \file Ifx_MotorModelConfigF32.h
 * \brief Motor model configuration
 *
 *
 * \version disabled
 * \copyright Copyright (c) 2013 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * \defgroup library_srvsw_sysse_emotor_motormodelconfig Motor model configuration
 * \ingroup library_srvsw_sysse_emotor
 *
 */
#ifndef IFX_MOTORMODELCONFIGF32_H
#define IFX_MOTORMODELCONFIGF32_H

#include "SysSe/Ext/At25xxx/Ifx_Efs.h"
#include "StdIf/IfxStdIf_MotorModelF32.h"
#include "SysSe/Comm/Ifx_Shell.h"

/** \brief File version
 * Format: \<00\>\<00\>\<version\>\<revision\>
 */
typedef enum
{
    Ifx_MotorModelConfigF32_File_Version_unknown  = 0x0000,      /**<\brief Undefined version */
    Ifx_MotorModelConfigF32_File_Version_01_00    = 0x0100,      /**<\brief Motor configuration file version 1.00 */
    Ifx_MotorModelConfigF32_File_Version_01_01    = 0x0101,      /**<\brief Motor configuration file version 1.01 */
    Ifx_MotorModelConfigF32_File_Version_01_02    = 0x0102,      /**<\brief Motor configuration file version 1.02 */
    Ifx_MotorModelConfigF32_File_Version_DataSize = 0x7FFFFFFF   /**<\brief Ensure that sizeof(Ifx_MotorModelConfigF32_File_Version) is at least 4 byte  for file in EEPROM */
}Ifx_MotorModelConfigF32_File_Version;

typedef struct
{
    Ifx_MotorModelConfigF32_File_Version fileVersion;                                        /**< \brief File version */
    boolean                               valid;                                              /**< \brief Valid flag that indicates if the configuration is valid */
    Ifx_F32_MotorModel_Type               type;                                               /**< \brief Motor type */
    float32                               iMax;                                               /**< \brief Max phase current */
    float32                               idKi;                                               /**< \brief Id PI controller, integrator gain */
    float32                               idKp;                                               /**< \brief Id PI controller, proportional gain */
    float32                               iqKi;                                               /**< \brief Iq PI controller, integrator gain */
    float32                               iqKp;                                               /**< \brief Iq PI controller, proportional gain */
    float32                               rs;                                                 /**< \brief Stator resistance in Ohm */
    float32                               ld;                                                 /**< \brief (PMSM only) Stator inductance Ld in H */
    float32                               lq;                                                 /**< \brief (PMSM only) Stator inductance Lq in H */
    float32                               kt;                                                 /**< \brief (PMSM only) Torque constant in Nm/Arms */
    float32                               iStall;                                             /**< \brief Stall current */
    uint8                                 polePair;                                           /**< \brief number of pole pairs */
    float32                               statorMax;                                          /**< \brief (ACIM only) maximum stator electrical speed (e.g. used at start-up) */
    float32                               rr;                                                 /**< \brief (ACIM only) Motor rotor default resistance in Ohm */
    float32                               ls;                                                 /**< \brief (ACIM only) Motor stator default inductance in H */
    float32                               lr;                                                 /**< \brief (ACIM only) Motor rotor default inductance in H */
    float32                               lm;                                                 /**< \brief (ACIM only) Motor mutual default inductance in H */
    float32                               fr;                                                 /**< \brief (ACIM only) Motor rotor default flux in V.s */

    float32                               speedMax;                                           /**< \brief Max speed in rpm */
    float32                               speedKp;                                            /**< \brief Speed PI controller, proportional gain */
    float32                               speedKi;                                            /**< \brief Speed PI controller, integrator gain */
    float32                               torqueMax;                                          /**< \brief Max torque in Nm */
    float32                               torqueRate;                                         /**< \brief Torque change rate in Nm/s */

    boolean                               fieldWeakning;                                      /**< \brief Specifies if the field weakning is enabled (TRUE) or disabled (FALSE) */

}Ifx_MotorModelConfigF32_File;

#define IFX_MOTORMODELCONFIGF32_FILENAME_SIZE (16)                                          /* Size of the filename field in byte */
#define IFX_MOTORMODELCONFIGF32_FILE_VERSION  (Ifx_MotorModelConfigF32_File_Version_01_02) /* Current file version */

typedef struct
{
    Ifx_MotorModelConfigF32_File data;
    Ifx_Efs                      *efs;
    char                          filename[IFX_MOTORMODELCONFIGF32_FILENAME_SIZE];
}Ifx_MotorModelConfigF32;

typedef struct
{
    Ifx_Efs *efs;
    pchar    filename;
}Ifx_MotorModelConfigF32_Config;

IFX_EXTERN Ifx_Shell_Command Ifx_g_MotorModelConfigF32[];
void    Ifx_MotorModelConfigF32_initConfig(Ifx_MotorModelConfigF32_Config *config, Ifx_Efs *efs, pchar filename);
boolean Ifx_MotorModelConfigF32_init(Ifx_MotorModelConfigF32 *driver, Ifx_MotorModelConfigF32_Config *config);

void    Ifx_MotorModelConfigF32_setupDefaultValue(Ifx_MotorModelConfigF32 *driver);
boolean Ifx_MotorModelConfigF32_deleteFile(Ifx_MotorModelConfigF32 *driver);
boolean Ifx_MotorModelConfigF32_createFile(Ifx_MotorModelConfigF32 *driver);
boolean Ifx_MotorModelConfigF32_updateFile(Ifx_MotorModelConfigF32 *driver);
boolean Ifx_MotorModelConfigF32_loadFile(Ifx_MotorModelConfigF32 *driver);
void    Ifx_MotorModelConfigF32_printConfig(Ifx_MotorModelConfigF32 *driver, IfxStdIf_DPipe *io);
void    Ifx_MotorModelConfigF32_setFileName(Ifx_MotorModelConfigF32 *driver, pchar filename);

#endif /* IFX_MOTORMODELCONFIGF32_H */
