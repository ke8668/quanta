/**
 * \file Ifx_ParkF32.h
 * \brief Park transformation.
 * \ingroup library_srvsw_sysse_emotor_park
 *
 *
 *
 * \version disabled
 * \copyright Copyright (c) 2013 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * \defgroup library_srvsw_sysse_emotor_park Park Transformation
 * \ingroup library_srvsw_sysse_emotor
 *
 * The Park transformation converts the rotating two-phase signals into two
 * rotating axis references. The real (d) axis is in the same direction as the
 * rotor magnetic flux and the quadrature (q) axis is perpendicular, where
 * \f$\varphi\f$ is the angle between the \f$\alpha\f$ axis and d axis.
 * This transformation can be written as:
 *
 * \f$\begin{bmatrix}x_d\\x_q\end{bmatrix}=\begin{bmatrix}cos(\varphi)&sin(\varphi)\\-sin(\varphi)&cos(\varphi)\end{bmatrix}\begin{bmatrix}x_\alpha\\x_\beta\end{bmatrix}\f$	\eq{5,library_srvsw_sysse_emotor_park_eq5}
 *
 * The inverse of the transformation is written as:
 *
 * \f$\begin{bmatrix}x_\alpha\\x_\beta\end{bmatrix}=\begin{bmatrix}cos(\varphi)&-sin(\varphi)\\sin(\varphi)&cos(\varphi)\end{bmatrix}\begin{bmatrix}x_d\\x_q\end{bmatrix}\f$	\eq{6,library_srvsw_sysse_emotor_park_eq6}
 *
 * \image html "SrvSw.SysSe.EMotor.Park[Transforms].png" "Park transform"
 *
 */

#if !defined(IFX_PARKF32_H)
#define IFX_PARKF32_H
//------------------------------------------------------------------------------
#include "SysSe/Math/IFX_Cf32.h"
//------------------------------------------------------------------------------

cfloat32 Ifx_ParkF32_forward(const cfloat32 *mab, const cfloat32 *CosSin);
cfloat32 Ifx_ParkF32_reverse(const cfloat32 *mdq, const cfloat32 *CosSin);
#endif
