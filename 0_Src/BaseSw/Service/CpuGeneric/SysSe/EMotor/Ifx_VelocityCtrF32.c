/**
 * \file Ifx_VelocityCtrF32.c
 * \brief Velocity control implementation
 *
 *
 * \version disabled
 * \copyright Copyright (c) 2013 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 */

//---------------------------------------------------------------------------
#include "Ifx_VelocityCtrF32.h"
//---------------------------------------------------------------------------
/** \brief Execute the speed PI controller
 *
 * \param vc Specifies pointer to the velocity control (\ref Ifx_VelocityCtrF32) object.
 * \param speed Specifies the current speed.
 *
 * \return Returns the velocity controller output.
 * \see Ifx_PicF32()
 */
float32 Ifx_VelocityCtrF32_step(Ifx_VelocityCtrF32 *vc, float32 speed)
{
    float32 result;
    vc->value = speed;

    if (vc->enabled != FALSE)
    {
        result = Ifx_PicF32_step(&vc->pic, vc->ref - speed);
    }
    else
    {
        result = vc->pic.uk;
    }

    return result;
}


/** \brief Initialize the velocity control object
 *
 * \param vc Specifies pointer to the velocity control (\ref Ifx_VelocityCtrF32) object.
 *
 * \return None.
 * \see Ifx_PicF32_init()
 */
void Ifx_VelocityCtrF32_init(Ifx_VelocityCtrF32 *vc)
{
    vc->ref = 0;
    Ifx_PicF32_init(&vc->pic);
}
