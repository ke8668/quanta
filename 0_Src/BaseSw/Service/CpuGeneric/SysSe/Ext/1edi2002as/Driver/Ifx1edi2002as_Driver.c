/**
 * \file Ifx1edi2002as_Driver.c
 * \brief
 *
 * \copyright Copyright (c) 2015 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * This file may be used, copied, and distributed, with or without modification, provided
 * that all copyright notices are retained; that all modifications to this file are
 * prominently noted in the modified file; and that this paragraph is not modified.
 *
 */

#include "SysSe/Ext/1edi2002as/Driver/Ifx1edi2002as_Driver.h"
#include "SysSe/Ext/1edi2002as/Hal/Ifx1edi2002as_Hal.h"
#include "string.h"
#include "SysSe/Ext/1edi2002as/If/Ifx1edi2002as_If.h"


#if IFX_CFG_1EDI2002AS_DRIVER_CACHE_ENABLE
#pragma message  "WARNING: 1EDI2002AS Cache feature is experimental"
#endif

void Ifx1edi2002as_Driver_disableAllDevicesCache(Ifx1edi2002as_Driver *driver)
{
#if IFX_CFG_1EDI2002AS_DRIVER_CACHE_ENABLE
    /* Ensure all cached data are written to the device before disabling the cache */
	Ifx1edi2002as_Driver_writeSyncAllDevices(driver);
    driver = driver->daisyChain.first;

    do
    {
        driver->cache.enabled = FALSE;
        driver                = driver->daisyChain.next;
    } while (driver);
#else
    (void)driver;
#endif
}


void Ifx1edi2002as_Driver_enableAllDevicesCache(Ifx1edi2002as_Driver *driver)
{
#if IFX_CFG_1EDI2002AS_DRIVER_CACHE_ENABLE
    driver = driver->daisyChain.first;
    do
    {
        driver->cache.enabled = TRUE;
        Ifx_DeviceCache_invalidate(&driver->cache.engine);
        driver                = driver->daisyChain.next;
    } while (driver);
#else
    (void)driver;
#endif
}


boolean Ifx1edi2002as_init(Ifx1edi2002as_Driver *driver, Ifx1edi2002as_Driver_Config *config)
{
    boolean result = TRUE;

    Ifx1edi2002as_Hal_init(config);

    driver->channel       = config->channel;
    driver->pins.en       = config->en;
    driver->pins.fltAN    = config->fltAN;
    driver->pins.fltBN    = config->fltBN;
    driver->pins.rstN     = config->rstN;
    driver->daisyChain.next = NULL_PTR;

    if (config->previousDevice)
    {
        driver->daisyChain.first                = config->previousDevice->daisyChain.first;
        config->previousDevice->daisyChain.next = driver;
        uint8          length, index;
        Ifx1edi2002as_Driver *device;
        device = driver->daisyChain.first;
        length = 0;
        index  = 0;

        do
        {
            length++;
            device = device->daisyChain.next;
        } while (device);

        device = driver->daisyChain.first;

        do
        {
            device->daisyChain.length = length;
            device->daisyChain.index  = index++;
            device                    = device->daisyChain.next;
        } while (device);
    }
    else
    {
        driver->daisyChain.first  = driver;
        driver->daisyChain.length = 1;
    }


#if IFX_CFG_1EDI2002AS_DRIVER_CACHE_ENABLE
    Ifx_DeviceCache_Config cacheConfig;
    Ifx_DeviceCache_initConfig(&cacheConfig);

    driver->cache.enabled = TRUE;
    cacheConfig.ItemCount         = sizeof(Ifx_1EDI2002AS) / IFX1EDI2002AS_ADDRESS_UNIT;
    cacheConfig.buffer            = (void*)&driver->cache.cache;
    cacheConfig.modificationMask  = &driver->cache.modificationMask[0];
    cacheConfig.validAddress      = (uint32 *)&Ifx_g_1EDI2002AS_validAddress[0];
    cacheConfig.volatileRegisters = (uint32 *)&Ifx_g_1EDI2002AS_volatileRegisters[0];
    cacheConfig.actionReadRegisters  = (uint32 *)&Ifx_g_1EDI2002AS_actionReadRegisters[0];
    cacheConfig.actionWriteRegisters = (uint32 *)&Ifx_g_1EDI2002AS_actionWriteRegisters[0];
    cacheConfig.valid             = &driver->cache.cacheValid[0];
    cacheConfig.flagArraySize     = sizeof(Ifx_g_1EDI2002AS_validAddress) / 4;
    cacheConfig.read              = (Ifx_DeviceCache_ReadFromDevice)Ifx1edi2002as_If_readRegister;
    cacheConfig.write             = (Ifx_DeviceCache_WriteToDevice)Ifx1edi2002as_If_writeRegister;
    cacheConfig.deviceDriver      = driver;
    cacheConfig.registerWidth     = IFX1EDI2002AS_ADDRESS_UNIT;

    if (config->previousDevice)
    {
        cacheConfig.previousCache = &config->previousDevice->cache.engine;
    }
    else
    {
        cacheConfig.previousCache = NULL_PTR;
    }

    cacheConfig.tempCacheLine = driver->cache.tempCacheLine;

    result                   &= Ifx_DeviceCache_init(&driver->cache.engine, &cacheConfig);
    Ifx_DeviceCache_setVolatileCache(&driver->cache.engine, TRUE);
#endif
    return result;
}


void Ifx1edi2002as_initConfig(Ifx1edi2002as_Driver_Config *config)
{
    memset(config, 0, sizeof(Ifx1edi2002as_Driver_Config));
}


boolean Ifx1edi2002as_Driver_readRegister(Ifx1edi2002as_Driver *driver, Ifx1edi2002as_Address address, uint16 *data)
{
    boolean result = TRUE;
#if IFX_CFG_1EDI2002AS_DRIVER_CACHE_ENABLE

    if (driver->cache.enabled)
    {
        result = Ifx_DeviceCache_getValue16(&driver->cache.engine, address, data);
    }
    else
#endif
    {
        uint16 temp[IFX1EDI2002AS_MAX_DAISYCHAIN_LENGTH];

        result = Ifx1edi2002as_If_readRegister(driver, address, temp);
        *data  = temp[driver->daisyChain.index];
    }

    return result;
}


boolean Ifx1edi2002as_Driver_readSync(Ifx1edi2002as_Driver *driver)
{
    boolean result = TRUE;
#if IFX_CFG_1EDI2002AS_DRIVER_CACHE_ENABLE

    if (driver->cache.enabled)
    {
        result = Ifx_DeviceCache_synchronize16(&driver->cache.engine);
    }
    else
#else
    (void)driver;
#endif
    {
        /* Nothing to do */
    }

    return result;
}


boolean Ifx1edi2002as_Driver_writeRegister(Ifx1edi2002as_Driver *driver, Ifx1edi2002as_Address address, uint16 data)
{
    boolean result = TRUE;

#if IFX_CFG_1EDI2002AS_DRIVER_CACHE_ENABLE
    if (driver->cache.enabled)
    {
        Ifx_DeviceCache_setValue16(&driver->cache.engine, address, data);
    }
    else
#endif
    {
        uint16 temp[IFX1EDI2002AS_MAX_DAISYCHAIN_LENGTH];
        temp[driver->daisyChain.index] = data;
        result                         = Ifx1edi2002as_If_writeRegister(driver, 1 << driver->daisyChain.index, address, temp);
    }

    return result;
}


boolean Ifx1edi2002as_Driver_writeSyncAllDevices(Ifx1edi2002as_Driver *driver)
{
    boolean result = TRUE;

#if IFX_CFG_1EDI2002AS_DRIVER_CACHE_ENABLE
    if (driver->cache.enabled)
    {
        result = Ifx_DeviceCache_writeBack16(&driver->cache.engine);
    }
    else
#else
    (void)driver;
#endif
    {
        /* Nothing to do */
    }

    return result;
}


void Ifx1edi2002as_Driver_disable(Ifx1edi2002as_Driver *driver, uint32 mask)
{
    uint32 i = 1;
    driver = driver->daisyChain.first;

    do
    {
        if ((i & mask) && (driver->pins.en.port != NULL_PTR))
        {
            Ifx1edi2002as_Hal_setPinLow(&driver->pins.en);
        }

        i    <<= 1;
        driver = driver->daisyChain.next;
    } while (driver);
}


void Ifx1edi2002as_Driver_enable(Ifx1edi2002as_Driver *driver, uint32 mask)
{
    uint32 i = 1;
    driver = driver->daisyChain.first;

    do
    {
        if ((i & mask) && (driver->pins.en.port != NULL_PTR))
        {
            Ifx1edi2002as_Hal_setPinHigh(&driver->pins.en);
        }

        i    <<= 1;
        driver = driver->daisyChain.next;
    } while (driver);
}


void Ifx1edi2002as_Driver_reset(Ifx1edi2002as_Driver *driver, uint32 mask)
{
    Ifx1edi2002as_Driver_switchOff(driver, mask);
    wait(TimeConst_1s);/* FIXME find out the correct timing. Is 10us ok having the external circuit with RC?*/
    Ifx1edi2002as_Driver_switchOn(driver, mask);
}


void Ifx1edi2002as_Driver_switchOff(Ifx1edi2002as_Driver *driver, uint32 mask)
{
    uint32 i = 1;
    driver = driver->daisyChain.first;

    do
    {
        if ((i & mask) && (driver->pins.rstN.port != NULL_PTR))
        {
            Ifx1edi2002as_Hal_setPinLow(&driver->pins.rstN);
        }

        i    <<= 1;
        driver = driver->daisyChain.next;
    } while (driver);
}


void Ifx1edi2002as_Driver_switchOn(Ifx1edi2002as_Driver *driver, uint32 mask)
{
    uint32 i = 1;
    driver = driver->daisyChain.first;

    do
    {
        if ((i & mask) && (driver->pins.rstN.port != NULL_PTR))
        {
            Ifx1edi2002as_Hal_setPinHigh(&driver->pins.rstN);
        }

        i    <<= 1;
        driver = driver->daisyChain.next;
    } while (driver);
}


uint32 Ifx1edi2002as_Driver_isAnyDeviceNotReady(Ifx1edi2002as_Driver *driver)
{
    uint32 result = 0;
    uint32 i      = 1;
    driver = driver->daisyChain.first;

    do
    {
        if (driver->pins.rstN.port != NULL_PTR)
        {
            result |= Ifx1edi2002as_Hal_getPinState(&driver->pins.rstN) == FALSE ? i : 0;
        }

        i    <<= 1;
        driver = driver->daisyChain.next;
    } while (driver);

    return result;
}


uint32 Ifx1edi2002as_Driver_isAnyDeviceFault(Ifx1edi2002as_Driver *driver)
{
    uint32 result = 0;
    uint32 i      = 1;
    driver = driver->daisyChain.first;

    do
    {
        if (driver->pins.fltAN.port != NULL_PTR)
        {
            result |= Ifx1edi2002as_Hal_getPinState(&driver->pins.fltAN) == FALSE ? i : 0;
        }

        if (driver->pins.fltBN.port != NULL_PTR)
        {
            result |= Ifx1edi2002as_Hal_getPinState(&driver->pins.fltBN) == FALSE ? i << 16 : 0;
        }

        i    <<= 1;
        driver = driver->daisyChain.next;
    } while (driver);

    /* FIXME missing main fault flag bit 15 */
    return result;
}


boolean Ifx1edi2002as_Driver_enterConfigurationMode(Ifx1edi2002as_Driver *driver, uint32 mask)
{
    boolean result = TRUE;
#if IFX_CFG_1EDI2002AS_DRIVER_CACHE_ENABLE
    if (driver->cache.enabled)
    {
		// Ensure register values are transfered before mode change
		result &= Ifx_DeviceCache_writeBack16(&driver->cache.engine);
    }
#endif
    result &= Ifx1edi2002as_If_modeTransition(driver, mask, Ifx1edi2002as_modeTransition_enterCmode);
#if IFX_CFG_1EDI2002AS_DRIVER_CACHE_ENABLE
    if (driver->cache.enabled)
    {
        Ifx_DeviceCache_invalidateVolatile(&driver->cache.engine);
    }
#endif
    return result;
}


boolean Ifx1edi2002as_Driver_exitConfigurationMode(Ifx1edi2002as_Driver *driver, uint32 mask)
{
    boolean result = TRUE;
#if IFX_CFG_1EDI2002AS_DRIVER_CACHE_ENABLE
    if (driver->cache.enabled)
    {
		// Ensure register values are transfered before mode change
		result &= Ifx_DeviceCache_writeBack16(&driver->cache.engine);
    }
#endif
    result &= Ifx1edi2002as_If_modeTransition(driver, mask, Ifx1edi2002as_modeTransition_exitCmode);
#if IFX_CFG_1EDI2002AS_DRIVER_CACHE_ENABLE
    if (driver->cache.enabled)
    {
        Ifx_DeviceCache_invalidateVolatile(&driver->cache.engine);
    }
#endif
    return result;
}


boolean Ifx1edi2002as_Driver_enterVerificationMode(Ifx1edi2002as_Driver *driver, uint32 mask)
{
    boolean result = TRUE;
#if IFX_CFG_1EDI2002AS_DRIVER_CACHE_ENABLE
    if (driver->cache.enabled)
    {
		// Ensure register values are transfered before mode change
		result &= Ifx_DeviceCache_writeBack16(&driver->cache.engine);
    }
#endif
    result &= Ifx1edi2002as_If_modeTransition(driver, mask, Ifx1edi2002as_modeTransition_enterVmode);
#if IFX_CFG_1EDI2002AS_DRIVER_CACHE_ENABLE
    if (driver->cache.enabled)
    {
        Ifx_DeviceCache_invalidateVolatile(&driver->cache.engine);
    }
#endif
    return result;
}
