/**
 * \file Ifx1edi2002as_Hal.c
 * \brief
 * Implementation of the 1EDI2002AS HAL. This file is not part of the driver and must be adapted to match the application framework
 *
 * \copyright Copyright (c) 2015 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * This file may be used, copied, and distributed, with or without modification, provided
 * that all copyright notices are retained; that all modifications to this file are
 * prominently noted in the modified file; and that this paragraph is not modified.
 *
 */

#include "Ifx1edi2002as_Hal.h"
#include "If/SpiIf.h"

#if IFX1EDI2002AS_DEBUG
#include "Ifx_Console.h"
#endif

boolean Ifx1edi2002as_Hal_exchangeSpi(void *spiChannel, const void *src, void *dest, uint8 length)
{
#if IFX1EDI2002AS_DEBUG
    {
    	uint32 i;
        Ifx_Console_print("1EDI2002AS Exchange:");
    	for (i=0;i<length;i++)
    	{
            uint16 d = ((uint16 *)src)[i];
            Ifx_Console_print(" (0x%04X)", d);
    	}
    }
#endif

    SpiIf_Ch *spi = (SpiIf_Ch *)spiChannel;
    SpiIf_exchange(spi, src, dest, length);
    SpiIf_wait(spi);
#if IFX1EDI2002AS_DEBUG
    {
    	uint32 i;
        Ifx_Console_print("->");
    	for (i=0;i<length;i++)
    	{
            uint16 d = ((uint16 *)dest)[i];
            Ifx_Console_print(" (0x%04X)", d);
    	}
        Ifx_Console_print(ENDL);
    }
#endif
    return TRUE; /* FIXME handle intermediate status? */
}


void Ifx1edi2002as_Hal_initPinOut(IfxPort_Pin *pin, IfxPort_Mode mode, IfxPort_PadDriver driver, IfxPort_State state)
{
    Pin_setMode(pin, mode);
    Pin_setDriver(pin, driver);
    Pin_setState(pin, state);
}


void Ifx1edi2002as_Hal_initPinInp(IfxPort_Pin *pin, IfxPort_Mode mode)
{
    Pin_setMode(pin, mode);
}


boolean Ifx1edi2002as_Hal_init(Ifx1edi2002as_Driver_Config *config)
{
    boolean result = TRUE;

    if (config->rstN.port != NULL_PTR)
    {
        Ifx1edi2002as_Hal_initPinOut(&config->rstN, IfxPort_Mode_outputOpenDrainGeneral, IfxPort_PadDriver_cmosAutomotiveSpeed1, IfxPort_State_low);
    }

    if (config->en.port != NULL_PTR)
    {
        Ifx1edi2002as_Hal_initPinOut(&config->en, IfxPort_Mode_outputPushPullGeneral, IfxPort_PadDriver_cmosAutomotiveSpeed1, IfxPort_State_low);
    }

    if (config->fltAN.port != NULL_PTR)
    {
        Ifx1edi2002as_Hal_initPinInp(&config->fltAN, IfxPort_Mode_inputNoPullDevice);
    }

    if (config->fltBN.port != NULL_PTR)
    {
        Ifx1edi2002as_Hal_initPinInp(&config->fltBN, IfxPort_Mode_inputNoPullDevice);
    }

    return result;
}
