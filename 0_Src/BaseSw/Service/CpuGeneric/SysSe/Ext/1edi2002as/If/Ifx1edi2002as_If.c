/**
 * \file Ifx1edi2002as_If.c
 * \brief
 *
 * \copyright Copyright (c) 2015 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * This file may be used, copied, and distributed, with or without modification, provided
 * that all copyright notices are retained; that all modifications to this file are
 * prominently noted in the modified file; and that this paragraph is not modified.
 *
 */

#include "SysSe/Ext/1edi2002as/If/Ifx1edi2002as_If.h"

#include "SysSe/Ext/1edi2002as/Hal/Ifx1edi2002as_Hal.h"

#if IFX1EDI2002AS_DEBUG
#include "Ifx_Console.h"
#endif

/** Message command field*/
typedef enum
{
    Ifx1edi2002as_If_Cmd_enterCmode = 0x1,
    Ifx1edi2002as_If_Cmd_enterVmode = 0x1,
    Ifx1edi2002as_If_Cmd_exitCmode  = 0x1,
    Ifx1edi2002as_If_Cmd_nop        = 0x1,
    Ifx1edi2002as_If_Cmd_read       = 0x0,
    Ifx1edi2002as_If_Cmd_writeh     = 0x4,
    Ifx1edi2002as_If_Cmd_writel     = 0xA
}Ifx1edi2002as_If_Cmd;

/** Message command key (fixed value of data)*/
typedef enum
{
    Ifx1edi2002as_If_CmdKey_enterCmode = 0x440,
    Ifx1edi2002as_If_CmdKey_enterVmode = 0x0A0,
    Ifx1edi2002as_If_CmdKey_exitCmode  = 0x110,
    Ifx1edi2002as_If_CmdKey_nop        = 0x208,
    Ifx1edi2002as_If_CmdKey_read       = 0x15,
    Ifx1edi2002as_If_CmdKey_writeh     = 0x2,
    Ifx1edi2002as_If_CmdKey_writel     = 0x0
}Ifx1edi2002as_If_CmdKey;

/** Message command data*/
typedef enum
{
    Ifx1edi2002as_If_CmdData_enterCmode = (Ifx1edi2002as_If_Cmd_enterCmode << 12) | (Ifx1edi2002as_If_CmdKey_enterCmode << 1),
    Ifx1edi2002as_If_CmdData_enterVmode = (Ifx1edi2002as_If_Cmd_enterVmode << 12) | (Ifx1edi2002as_If_CmdKey_enterVmode << 1),
    Ifx1edi2002as_If_CmdData_exitCmode  = (Ifx1edi2002as_If_Cmd_exitCmode << 12) | (Ifx1edi2002as_If_CmdKey_exitCmode << 1),
    Ifx1edi2002as_If_CmdData_nop        = (Ifx1edi2002as_If_Cmd_nop << 12) | (Ifx1edi2002as_If_CmdKey_nop << 1)
}Ifx1edi2002as_If_CmdData;

/** General MTSR frame*/
typedef uint16 Ifx1edi2002as_If_Mtsr;
/**
 * MRST frame definition (Message: Enter CMODE, Enter VMODE, Exit CMODE, NOP)
 */
typedef union
{
    struct
    {
        uint16 p : 1;
        uint16 data : 11;
        uint16 cmd : 4;
    }      fields;
    uint16 data;
}Ifx1edi2002as_If_MtsrMode;

/**
 * MRST frame definition (Message: READ)
 */
typedef union
{
    struct
    {
        uint16 p : 1;             /**< \brief parity bit */
        uint16 key : 6;           /**< \brief fixed data */
        uint16 address : 5;       /**< \brief register address */
        uint16 cmd : 4;           /**< \brief command */
    }      fields;
    uint16 data;
}Ifx1edi2002as_If_MtsrRead;

/**
 * MRST frame definition (Message: WRITEH)
 */
typedef union
{
    struct
    {
        uint16 p : 1;             /**< \brief parity bit */
        uint16 data : 8;          /**< \brief data */
        uint16 key : 3;           /**< \brief fixed data */
        uint16 cmd : 4;           /**< \brief command */
    }      fields;
    uint16 data;
}Ifx1edi2002as_If_MtsrWriteH;

/**
 * MRST frame definition (Message: WRITEL)
 */
typedef union
{
    struct
    {
        uint16 p : 1;             /**< \brief parity bit */
        uint16 data : 6;          /**< \brief data */
        uint16 address : 5;       /**< \brief register address */
        uint16 cmd : 4;           /**< \brief command */
    }      fields;
    uint16 data;
}Ifx1edi2002as_If_MtsrWriteL;

/**
 * MTSR frame definition
 */
typedef union
{
    struct
    {
        uint16 p : 1;
        uint16 lmi : 1;
        uint16 data : 14;
    }      fields;
    uint16 data;
}Ifx1edi2002as_If_Mrst;

uint8 Ifx1edi2002as_If_calcualteParity(uint16 data)
{
    uint8 p = 1;
    uint8 i = 15;

    while (i)
    {
        data = data >> 1;
        p    = p ^ (data & 1);
        i--;
    }

    return p;
}


/** Checks the parity
 */
boolean Ifx1edi2002as_If_checkCmdResponse(Ifx1edi2002as_If_Mrst *frame)
{
    boolean status = TRUE;

    if ((frame->fields.p == Ifx1edi2002as_If_calcualteParity(frame->data)) && (frame->fields.lmi == 0))
    {
        status = TRUE;
    }
    else
    {
        status = FALSE;
    }

    return status;
}


/** Checks the parity
 */
boolean Ifx1edi2002as_If_checkReadResponse(Ifx1edi2002as_If_Mrst *frame)
{
    boolean status = TRUE;

    if ((frame->fields.p == Ifx1edi2002as_If_calcualteParity(frame->data)) && (frame->fields.lmi == 0))
    {
        status = TRUE;
    }
    else
    {
        status = FALSE;
    }

    return status;
}


/** Checks the parity and lmi
 */
boolean Ifx1edi2002as_If_checkWriteResponse(Ifx1edi2002as_If_Mrst *frame)
{
    boolean status = TRUE;

    /* FIXME check PSTAT values in frame[0] and frame[1] */
    if (
        (frame->fields.p == Ifx1edi2002as_If_calcualteParity(frame->data)) && (frame->fields.lmi == 0)
        )
    {
        status = TRUE;
    }
    else
    {
        status = FALSE;
    }

    return status;
}


/** Initialize the MTSR frame (Message: Enter CMODE)
 * \param frame frame to be initialized
 */
void Ifx1edi2002as_If_initMtsrEnterCmode(Ifx1edi2002as_If_MtsrMode *frame)
{
    frame->data = (uint16)Ifx1edi2002as_If_CmdData_enterCmode;
}


/** Initialize the MTSR frame (Message: Enter VMODE)
 * \param frame frame to be initialized
 */
void Ifx1edi2002as_If_initMtsrEnterVmode(Ifx1edi2002as_If_MtsrMode *frame)
{
    frame->data = (uint16)Ifx1edi2002as_If_CmdData_enterVmode;
}


/** Initialize the MTSR frame (Message: Exit CMODE)
 * \param frame frame to be initialized
 */
void Ifx1edi2002as_If_initMtsrExitCmode(Ifx1edi2002as_If_MtsrMode *frame)
{
    frame->data = (uint16)Ifx1edi2002as_If_CmdData_exitCmode;
}


/** Initialize the MTSR frame (Message: NOP)
 * \param frame frame to be initialized
 */
void Ifx1edi2002as_If_initMtsrNop(Ifx1edi2002as_If_MtsrMode *frame)
{
    frame->data = (uint16)Ifx1edi2002as_If_CmdData_nop;
}


/** Initialize the MTSR frame (Message: READ)
 * \param frame frame to be initialized
 * \param address Register address
 */
void Ifx1edi2002as_If_initMtsrRead(Ifx1edi2002as_If_MtsrRead *frame, uint8 address)
{
    frame->fields.key     = Ifx1edi2002as_If_CmdKey_read;
    frame->fields.address = address;
    frame->fields.cmd     = Ifx1edi2002as_If_Cmd_read;
    frame->fields.p       = Ifx1edi2002as_If_calcualteParity(frame->data);
}


/** Initialize the MTSR frame (Message: WRITEH)
 * \param frameH frame to be initialized (High)
 * \param frameL frame to be initialized (Low)
 * \param address Register address
 * \param data Data to be written to the register
 */
void Ifx1edi2002as_If_initMtsrWrite(Ifx1edi2002as_If_MtsrWriteH *frameH, Ifx1edi2002as_If_MtsrWriteL *frameL, uint8 address, uint16 data)
{
    frameH->fields.key     = Ifx1edi2002as_If_CmdKey_writeh;
    frameH->fields.data    = data >> 8;
    frameH->fields.cmd     = Ifx1edi2002as_If_Cmd_writeh;
    frameH->fields.p       = Ifx1edi2002as_If_calcualteParity(frameH->data);

    frameL->fields.address = address;
    frameL->fields.data    = data >> 2;
    frameL->fields.cmd     = Ifx1edi2002as_If_Cmd_writel;
    frameL->fields.p       = Ifx1edi2002as_If_calcualteParity(frameL->data);
}


boolean Ifx1edi2002as_If_readRegister(Ifx1edi2002as_Driver *driver, Ifx1edi2002as_Address address, uint16 *data)
{
    boolean                    status = TRUE;
    Ifx1edi2002as_If_MtsrRead requestRead[IFX1EDI2002AS_MAX_DAISYCHAIN_LENGTH];
    Ifx1edi2002as_If_MtsrMode requestNop[IFX1EDI2002AS_MAX_DAISYCHAIN_LENGTH];
    Ifx1edi2002as_If_Mrst     response[IFX1EDI2002AS_MAX_DAISYCHAIN_LENGTH];

    uint8                      i;

    Ifx1edi2002as_If_initMtsrRead(&requestRead[0], address);
    Ifx1edi2002as_If_initMtsrNop(&requestNop[0]);

    for (i = 1; i < driver->daisyChain.length; i++)
    {
        requestRead[i] = requestRead[0];
        requestNop[i]  = requestNop[0];
    }

#if IFX1EDI2002AS_DEBUG
	Ifx_Console_print(ENDL);
#endif

    wait(TimeConst_10us);
    status = Ifx1edi2002as_Hal_exchangeSpi(driver->channel, requestRead, response, driver->daisyChain.length);
    wait(TimeConst_10us);

    if (status)
    {
        status = Ifx1edi2002as_Hal_exchangeSpi(driver->channel, requestNop, response, driver->daisyChain.length);

        if (status)
        {
#if IFX1EDI2002AS_DEBUG
        	Ifx_Console_print(" 1EDI2002AS read @ 0x%02X", address);
#endif
            for (i = 0; i < driver->daisyChain.length; i++)
            {
#if IFX1EDI2002AS_DEBUG
            	Ifx_Console_print(" 0x%02X", response[i].data);
#endif
                if (Ifx1edi2002as_If_checkReadResponse(&response[i]))
                {
                    data[i] = response[i].data;
                }
                else
                {
                    status = FALSE;
                }
            }
        }
    }
#if IFX1EDI2002AS_DEBUG
	if (!status)
	{
		Ifx_Console_print(" ERROR");
	}
	Ifx_Console_print(ENDL);
#endif

    return status;
}


boolean Ifx1edi2002as_If_writeRegister(Ifx1edi2002as_Driver *driver, uint32 mask, Ifx1edi2002as_Address address, uint16 *data)
{
    boolean                      status = TRUE;
    Ifx1edi2002as_If_MtsrWriteH requestWriteHigh[IFX1EDI2002AS_MAX_DAISYCHAIN_LENGTH];
    Ifx1edi2002as_If_MtsrWriteL requestWriteLow[IFX1EDI2002AS_MAX_DAISYCHAIN_LENGTH];
    Ifx1edi2002as_If_MtsrMode   requestNop[IFX1EDI2002AS_MAX_DAISYCHAIN_LENGTH];
    Ifx1edi2002as_If_Mrst       response[IFX1EDI2002AS_MAX_DAISYCHAIN_LENGTH];
    uint8                        i;

    Ifx1edi2002as_If_MtsrMode   nop;

#if IFX1EDI2002AS_DEBUG
	Ifx_Console_print(ENDL "1EDI2002AS write@ 0x%02X",address);
#endif

    Ifx1edi2002as_If_initMtsrNop(&nop);

    for (i = 0; i < driver->daisyChain.length; i++)
    {
        if (mask & 0x1)
        {
            Ifx1edi2002as_If_initMtsrWrite(&requestWriteHigh[i], &requestWriteLow[i], address, data[i]);
#if IFX1EDI2002AS_DEBUG
            Ifx_Console_print(" 0x%02X ", data[i]);
#endif
        }
        else
        {
            requestWriteHigh[i].data = nop.data;
            requestWriteLow[i].data  = nop.data;
#if IFX1EDI2002AS_DEBUG
            Ifx_Console_print("NOP");
#endif
        }

        requestNop[i] = nop;
        mask        >>= 1;
    }

    status = Ifx1edi2002as_Hal_exchangeSpi(driver->channel, requestWriteHigh, response, driver->daisyChain.length);

    if (status)
    {
        /* No need to check the response */
        status &= Ifx1edi2002as_Hal_exchangeSpi(driver->channel, requestWriteLow, response, driver->daisyChain.length);

        if (status)
        {
            for (i = 0; i < driver->daisyChain.length; i++)
            {
                status &= Ifx1edi2002as_If_checkWriteResponse(&response[i]);
            }
        }

        status &= Ifx1edi2002as_Hal_exchangeSpi(driver->channel, requestNop, response, driver->daisyChain.length);

        if (status)
        {
            for (i = 0; i < driver->daisyChain.length; i++)
            {
                status &= Ifx1edi2002as_If_checkWriteResponse(&response[i]);
            }
        }
    }
#if IFX1EDI2002AS_DEBUG
    if (!status)
    {
    	Ifx_Console_print(" ERROR");
    }
	Ifx_Console_print(ENDL);
#endif

    return status;
}


boolean Ifx1edi2002as_If_modeTransition(Ifx1edi2002as_Driver *driver, uint32 mask, Ifx1edi2002as_modeTransition mode)
{
    boolean                    status = TRUE;
    Ifx1edi2002as_If_MtsrMode requestMode[IFX1EDI2002AS_MAX_DAISYCHAIN_LENGTH];
    Ifx1edi2002as_If_MtsrMode requestNop[IFX1EDI2002AS_MAX_DAISYCHAIN_LENGTH];
    Ifx1edi2002as_If_Mrst     response[IFX1EDI2002AS_MAX_DAISYCHAIN_LENGTH];
    Ifx1edi2002as_If_MtsrMode nop;
    uint8                      i;

    Ifx1edi2002as_If_initMtsrNop(&nop);

    for (i = 0; i < driver->daisyChain.length; i++)
    {
        if (mask & 0x1)
        {
            switch (mode)
            {
            case Ifx1edi2002as_modeTransition_enterCmode:
                Ifx1edi2002as_If_initMtsrEnterCmode(&requestMode[i]);
                break;
            case Ifx1edi2002as_modeTransition_enterVmode:
                Ifx1edi2002as_If_initMtsrEnterVmode(&requestMode[i]);
                break;
            case Ifx1edi2002as_modeTransition_exitCmode:
                Ifx1edi2002as_If_initMtsrExitCmode(&requestMode[i]);
                break;
            default:
                status = FALSE;
                break;
            }
        }
        else
        {
            requestMode[i] = nop;
        }

        requestNop[i] = nop;
        mask        >>= 1;
    }

    status &= Ifx1edi2002as_Hal_exchangeSpi(driver->channel, requestMode, response, driver->daisyChain.length);

    if (status)
    {
        status &= Ifx1edi2002as_Hal_exchangeSpi(driver->channel, requestNop, response, driver->daisyChain.length);

        if (status)
        {
            for (i = 0; i < driver->daisyChain.length; i++)
            {
                status &= Ifx1edi2002as_If_checkCmdResponse(&response[i]);
                /* FIXME is it enough / required to check the parity bit only ? */
            }
        }
    }

    return status;
}
