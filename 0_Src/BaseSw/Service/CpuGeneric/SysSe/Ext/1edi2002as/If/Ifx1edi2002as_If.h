/**
 * \file Ifx1edi2002as_If.h
 * \brief
 *
 * \copyright Copyright (c) 2015 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * This file may be used, copied, and distributed, with or without modification, provided
 * that all copyright notices are retained; that all modifications to this file are
 * prominently noted in the modified file; and that this paragraph is not modified.
 *
 * \defgroup IfxLld_1edi2002as_If Communication interface
 * \ingroup IfxLld_1edi2002as
 */

#ifndef IFX1EDI2002AS_SPI_H
#define IFX1EDI2002AS_SPI_H

#include "Cpu/Std/IfxCpu_Intrinsics.h"
#include "SysSe/Ext/1edi2002as/Driver/Ifx1edi2002as_Driver.h"

/** \addtogroup IfxLld_1edi2002as_If
 * \{ */
/** Message command keys*/
typedef enum
{
    Ifx1edi2002as_modeTransition_enterCmode,
    Ifx1edi2002as_modeTransition_enterVmode,
    Ifx1edi2002as_modeTransition_exitCmode
}Ifx1edi2002as_modeTransition;

/** Read the register value out of the device
 *
 * Two frames are required to get the value
 * If the devices are in daisy chain, all the devices are accesses for read
 *
 * \param driver Pointer to the device driver
 * \param address Register address
 * \param data Returned register value
 *
 * \return TRUE in case of successful communication
 */
boolean Ifx1edi2002as_If_readRegister(Ifx1edi2002as_Driver *driver, Ifx1edi2002as_Address address, uint16 *data);

/** Write the register value to the device and check that communication was successful
 * If the devices are in daisy chain, only the required data are written to the device
 * Two frames are required to write the value
 *
 * \param driver Pointer to the device driver
 * \param mask Indicates which device should be written. Bit at offset 0 is the 1st device in the daisy chain, bit at offset 2, the second and so on...
 * \param address Register address
 * \param data Value to be written to the register
 *
 * \return TRUE in case of successful communication
 */
boolean Ifx1edi2002as_If_writeRegister(Ifx1edi2002as_Driver *driver, uint32 mask, Ifx1edi2002as_Address address, uint16 *data);

/** Switch mode
 *
 * \param driver Pointer to the device driver
 * \param mask Indicates which device should be written. Bit at offset 0 is the 1st device in the daisy chain, bit at offset 2, the second and so on...
 * \param mode Selected mode
 */
boolean Ifx1edi2002as_If_modeTransition(Ifx1edi2002as_Driver *driver, uint32 mask, Ifx1edi2002as_modeTransition mode);
/** \} */

#endif /* IFX1EDI2002AS_SPI_H */
