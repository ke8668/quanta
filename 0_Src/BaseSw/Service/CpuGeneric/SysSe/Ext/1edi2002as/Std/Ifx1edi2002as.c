/**
 * \file Ifx1edi2002as.c
 * \brief 1EDI2002AS  basic functionality 
 *
 * \copyright Copyright (c) 2016 Infineon Technologies AG. All rights reserved.
 *
 * $Date: 2016-01-05 14:43:03
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 */

/******************************************************************************/
/*----------------------------------Includes----------------------------------*/
/******************************************************************************/

#include "Ifx1edi2002as.h"

 
/******************************************************************************/
/*-------------------------Function Implementations---------------------------*/
/******************************************************************************/

boolean Ifx1edi2002as_getPrimaryChipIdentification(Ifx1edi2002as_Driver *driver, uint16 *id)
{
    boolean            status;
    Ifx_1EDI2002AS_PID reg;

    status = Ifx1edi2002as_Driver_readRegister(driver, ((uint32) & MODULE_1EDI2002AS.PID) >> IFX1EDI2002AS_ADDRESS_SHIFT, &reg.U);

    if (status)
    {
        *id = reg.B.PVERS;
    }

    return status;
}

boolean Ifx1edi2002as_isSecondaryReady(Ifx1edi2002as_Driver *driver, boolean *ready)
{
    boolean              status;
    Ifx_1EDI2002AS_PSTAT reg;

    status = Ifx1edi2002as_Driver_readRegister(driver, ((uint32) & MODULE_1EDI2002AS.PSTAT) >> IFX1EDI2002AS_ADDRESS_SHIFT, &reg.U);

    if (status)
    {
        *ready = reg.B.SRDY ? TRUE : FALSE;
    }

    return status;
}

boolean Ifx1edi2002as_enableAdvancedPrimaryConfiguration(Ifx1edi2002as_Driver *driver)
{
    boolean             status;
    Ifx_1EDI2002AS_PCFG reg;

    status = Ifx1edi2002as_Driver_readRegister(driver, ((uint32) & MODULE_1EDI2002AS.PCFG) >> IFX1EDI2002AS_ADDRESS_SHIFT, &reg.U);

    if (status)
    {
        reg.B.CFG1 = 1;

        status    &= Ifx1edi2002as_Driver_writeRegister(driver, ((uint32) & MODULE_1EDI2002AS.PCFG) >> IFX1EDI2002AS_ADDRESS_SHIFT, reg.U);
    }

    return status;
}

boolean Ifx1edi2002as_disableAdvancedPrimaryConfiguration(Ifx1edi2002as_Driver *driver)
{
    boolean             status;
    Ifx_1EDI2002AS_PCFG reg;

    status = Ifx1edi2002as_Driver_readRegister(driver, ((uint32) & MODULE_1EDI2002AS.PCFG) >> IFX1EDI2002AS_ADDRESS_SHIFT, &reg.U);

    if (status)
    {
        reg.B.CFG1 = 0;

        status    &= Ifx1edi2002as_Driver_writeRegister(driver, ((uint32) & MODULE_1EDI2002AS.PCFG) >> IFX1EDI2002AS_ADDRESS_SHIFT, reg.U);
    }

    return status;
}

boolean Ifx1edi2002as_setDio1Direction(Ifx1edi2002as_Driver *driver, Ifx1edi2002as_Dio1Direction direction)
{
    boolean              status;
    Ifx_1EDI2002AS_PCFG2 reg;

    status = Ifx1edi2002as_Driver_readRegister(driver, ((uint32) & MODULE_1EDI2002AS.PCFG2) >> IFX1EDI2002AS_ADDRESS_SHIFT, &reg.U);

    if (status)
    {
        reg.B.DIO1 = direction;

        status    &= Ifx1edi2002as_Driver_writeRegister(driver, ((uint32) & MODULE_1EDI2002AS.PCFG2) >> IFX1EDI2002AS_ADDRESS_SHIFT, reg.U);
    }

    return status;
}

boolean Ifx1edi2002as_enableDout(Ifx1edi2002as_Driver *driver)
{
    boolean              status;
    Ifx_1EDI2002AS_PCFG2 reg;

    status = Ifx1edi2002as_Driver_readRegister(driver, ((uint32) & MODULE_1EDI2002AS.PCFG2) >> IFX1EDI2002AS_ADDRESS_SHIFT, &reg.U);

    if (status)
    {
        reg.B.DOEN1 = 1;

        status     &= Ifx1edi2002as_Driver_writeRegister(driver, ((uint32) & MODULE_1EDI2002AS.PCFG2) >> IFX1EDI2002AS_ADDRESS_SHIFT, reg.U);
    }

    return status;
}

boolean Ifx1edi2002as_disableDout(Ifx1edi2002as_Driver *driver)
{
    boolean              status;
    Ifx_1EDI2002AS_PCFG2 reg;

    status = Ifx1edi2002as_Driver_readRegister(driver, ((uint32) & MODULE_1EDI2002AS.PCFG2) >> IFX1EDI2002AS_ADDRESS_SHIFT, &reg.U);

    if (status)
    {
        reg.B.DOEN1 = 0;

        status     &= Ifx1edi2002as_Driver_writeRegister(driver, ((uint32) & MODULE_1EDI2002AS.PCFG2) >> IFX1EDI2002AS_ADDRESS_SHIFT, reg.U);
    }

    return status;
}

boolean Ifx1edi2002as_clearPrimaryRequestBit(Ifx1edi2002as_Driver *driver)
{
    boolean              status;
    Ifx_1EDI2002AS_PCTRL reg;

    status = Ifx1edi2002as_Driver_readRegister(driver, ((uint32) & MODULE_1EDI2002AS.PCTRL) >> IFX1EDI2002AS_ADDRESS_SHIFT, &reg.U);

    if (status)
    {
        reg.B.CLRP = 1;

        status    &= Ifx1edi2002as_Driver_writeRegister(driver, ((uint32) & MODULE_1EDI2002AS.PCTRL) >> IFX1EDI2002AS_ADDRESS_SHIFT, reg.U);
    }

    return status;
}

boolean Ifx1edi2002as_clearSecondaryRequestBit(Ifx1edi2002as_Driver *driver)
{
    boolean              status;
    Ifx_1EDI2002AS_PCTRL reg;

    status = Ifx1edi2002as_Driver_readRegister(driver, ((uint32) & MODULE_1EDI2002AS.PCTRL) >> IFX1EDI2002AS_ADDRESS_SHIFT, &reg.U);

    if (status)
    {
        reg.B.CLRS = 1;

        status    &= Ifx1edi2002as_Driver_writeRegister(driver, ((uint32) & MODULE_1EDI2002AS.PCTRL) >> IFX1EDI2002AS_ADDRESS_SHIFT, reg.U);
    }

    return status;
}

boolean Ifx1edi2002as_getPrimariReadWriteRegister(Ifx1edi2002as_Driver *driver, uint16 *value)
{
    boolean            status;
    Ifx_1EDI2002AS_PRW reg;

    status = Ifx1edi2002as_Driver_readRegister(driver, ((uint32) & MODULE_1EDI2002AS.PRW) >> IFX1EDI2002AS_ADDRESS_SHIFT, &reg.U);

    if (status)
    {
        *value = reg.B.RWVAL;
    }

    return status;
}

boolean Ifx1edi2002as_setPrimariReadWriteRegister(Ifx1edi2002as_Driver *driver, uint16 value)
{
    boolean            status;
    Ifx_1EDI2002AS_PRW reg;

    status = Ifx1edi2002as_Driver_readRegister(driver, ((uint32) & MODULE_1EDI2002AS.PRW) >> IFX1EDI2002AS_ADDRESS_SHIFT, &reg.U);

    if (status)
    {
        reg.B.RWVAL = value;

        status     &= Ifx1edi2002as_Driver_writeRegister(driver, ((uint32) & MODULE_1EDI2002AS.PRW) >> IFX1EDI2002AS_ADDRESS_SHIFT, reg.U);
    }

    return status;
}

boolean Ifx1edi2002as_getSecondaryChipIdentification(Ifx1edi2002as_Driver *driver, uint16 *id)
{
    boolean            status;
    Ifx_1EDI2002AS_SID reg;

    status = Ifx1edi2002as_Driver_readRegister(driver, ((uint32) & MODULE_1EDI2002AS.SID) >> IFX1EDI2002AS_ADDRESS_SHIFT, &reg.U);

    if (status)
    {
        *id = reg.B.SVERS;
    }

    return status;
}

boolean Ifx1edi2002as_getOperationMode(Ifx1edi2002as_Driver *driver, Ifx1edi2002as_OperationMode *value)
{
    boolean              status;
    Ifx_1EDI2002AS_SSTAT reg;

    status = Ifx1edi2002as_Driver_readRegister(driver, ((uint32) & MODULE_1EDI2002AS.SSTAT) >> IFX1EDI2002AS_ADDRESS_SHIFT, &reg.U);

    if (status)
    {
        *value = reg.B.OPM;
    }

    return status;
}

boolean Ifx1edi2002as_enableAdvancedSecondaryConfiguration(Ifx1edi2002as_Driver *driver)
{
    boolean             status;
    Ifx_1EDI2002AS_SCFG reg;

    status = Ifx1edi2002as_Driver_readRegister(driver, ((uint32) & MODULE_1EDI2002AS.SCFG) >> IFX1EDI2002AS_ADDRESS_SHIFT, &reg.U);

    if (status)
    {
        reg.B.CFG2 = 1;

        status    &= Ifx1edi2002as_Driver_writeRegister(driver, ((uint32) & MODULE_1EDI2002AS.SCFG) >> IFX1EDI2002AS_ADDRESS_SHIFT, reg.U);
    }

    return status;
}

boolean Ifx1edi2002as_disableDesatClamping(Ifx1edi2002as_Driver *driver)
{
    boolean             status;
    Ifx_1EDI2002AS_SCFG reg;

    status = Ifx1edi2002as_Driver_readRegister(driver, ((uint32) & MODULE_1EDI2002AS.SCFG) >> IFX1EDI2002AS_ADDRESS_SHIFT, &reg.U);

    if (status)
    {
        reg.B.DSTCEN = 0;

        status    &= Ifx1edi2002as_Driver_writeRegister(driver, ((uint32) & MODULE_1EDI2002AS.SCFG) >> IFX1EDI2002AS_ADDRESS_SHIFT, reg.U);
    }

    return status;
}

boolean Ifx1edi2002as_disableAdvancedSecondaryConfiguration(Ifx1edi2002as_Driver *driver)
{
    boolean             status;
    Ifx_1EDI2002AS_SCFG reg;

    status = Ifx1edi2002as_Driver_readRegister(driver, ((uint32) & MODULE_1EDI2002AS.SCFG) >> IFX1EDI2002AS_ADDRESS_SHIFT, &reg.U);

    if (status)
    {
        reg.B.CFG2 = 0;

        status    &= Ifx1edi2002as_Driver_writeRegister(driver, ((uint32) & MODULE_1EDI2002AS.SCFG) >> IFX1EDI2002AS_ADDRESS_SHIFT, reg.U);
    }

    return status;
}

boolean Ifx1edi2002as_setDio2Direction(Ifx1edi2002as_Driver *driver, Ifx1edi2002as_Dio2Direction direction)
{
    boolean              status;
    Ifx_1EDI2002AS_SCFG2 reg;

    status = Ifx1edi2002as_Driver_readRegister(driver, ((uint32) & MODULE_1EDI2002AS.SCFG2) >> IFX1EDI2002AS_ADDRESS_SHIFT, &reg.U);

    if (status)
    {
        reg.B.DIO2 = direction;

        status    &= Ifx1edi2002as_Driver_writeRegister(driver, ((uint32) & MODULE_1EDI2002AS.SCFG2) >> IFX1EDI2002AS_ADDRESS_SHIFT, reg.U);
    }

    return status;
}
boolean Ifx1edi2002as_enableIgbtStateMonitoring(Ifx1edi2002as_Driver *driver)
{
    boolean              status;
    Ifx_1EDI2002AS_SCFG2 reg;

    status = Ifx1edi2002as_Driver_readRegister(driver, ((uint32) & MODULE_1EDI2002AS.SCFG2) >> IFX1EDI2002AS_ADDRESS_SHIFT, &reg.U);

    if (status)
    {
        reg.B.ISMEN = 1;

        status    &= Ifx1edi2002as_Driver_writeRegister(driver, ((uint32) & MODULE_1EDI2002AS.SCFG2) >> IFX1EDI2002AS_ADDRESS_SHIFT, reg.U);
    }

    return status;
}



boolean Ifx1edi2002as_setTwoLevelTurnOffLevel(Ifx1edi2002as_Driver* driver, Ifx1edi2002as_GateTurnOffPlateauLevel value)
{
    boolean status;
    Ifx_1EDI2002AS_PCTRL2 reg;
    status = Ifx1edi2002as_Driver_readRegister(driver, (Ifx1edi2002as_Address)(((uint32)&MODULE_1EDI2002AS.PCTRL2) >> IFX1EDI2002AS_ADDRESS_SHIFT), &reg.U);
    if (status)
    {
        reg.B.GPOF = value;
        status &= Ifx1edi2002as_Driver_writeRegister(driver, (Ifx1edi2002as_Address)(((uint32)&MODULE_1EDI2002AS.PCTRL2) >> IFX1EDI2002AS_ADDRESS_SHIFT), reg.U);
    }
    return status;
}
boolean Ifx1edi2002as_setTwoLevelTurnOffDelay(Ifx1edi2002as_Driver* driver, uint16 value)
{
    boolean status;
    Ifx_1EDI2002AS_SRTTOF reg;
    status = Ifx1edi2002as_Driver_readRegister(driver, (Ifx1edi2002as_Address)(((uint32)&MODULE_1EDI2002AS.SRTTOF) >> IFX1EDI2002AS_ADDRESS_SHIFT), &reg.U);
    if (status)
    {
        reg.B.RTVAL = value;
        status &= Ifx1edi2002as_Driver_writeRegister(driver, (Ifx1edi2002as_Address)(((uint32)&MODULE_1EDI2002AS.SRTTOF) >> IFX1EDI2002AS_ADDRESS_SHIFT), reg.U);
    }
    return status;
}
boolean Ifx1edi2002as_setTwoLevelTurnOnLevel(Ifx1edi2002as_Driver* driver, Ifx1edi2002as_GateTurnOnPlateauLevel value)
{
    boolean status;
    Ifx_1EDI2002AS_PCTRL reg;
    status = Ifx1edi2002as_Driver_readRegister(driver, (Ifx1edi2002as_Address)(((uint32)&MODULE_1EDI2002AS.PCTRL) >> IFX1EDI2002AS_ADDRESS_SHIFT), &reg.U);
    if (status)
    {
        reg.B.GPON = value;
        status &= Ifx1edi2002as_Driver_writeRegister(driver, (Ifx1edi2002as_Address)(((uint32)&MODULE_1EDI2002AS.PCTRL) >> IFX1EDI2002AS_ADDRESS_SHIFT), reg.U);
    }
    return status;
}
boolean Ifx1edi2002as_setTwoLevelTurnOnDelay(Ifx1edi2002as_Driver* driver, uint16 value)
{
    boolean status;
    Ifx_1EDI2002AS_SCFG2 reg;
    status = Ifx1edi2002as_Driver_readRegister(driver, (Ifx1edi2002as_Address)(((uint32)&MODULE_1EDI2002AS.SCFG2) >> IFX1EDI2002AS_ADDRESS_SHIFT), &reg.U);
    if (status)
    {
        reg.B.TTOND = value;
        status &= Ifx1edi2002as_Driver_writeRegister(driver, (Ifx1edi2002as_Address)(((uint32)&MODULE_1EDI2002AS.SCFG2) >> IFX1EDI2002AS_ADDRESS_SHIFT), reg.U);
    }
    return status;
}
boolean Ifx1edi2002as_setVbeCompensation(Ifx1edi2002as_Driver* driver, Ifx1edi2002as_VbeCompensation value)
{
    boolean status;
    Ifx_1EDI2002AS_SCFG reg;
    status = Ifx1edi2002as_Driver_readRegister(driver, (Ifx1edi2002as_Address)(((uint32)&MODULE_1EDI2002AS.SCFG) >> IFX1EDI2002AS_ADDRESS_SHIFT), &reg.U);
    if (status)
    {
        reg.B.VBEC = value;
        status &= Ifx1edi2002as_Driver_writeRegister(driver, (Ifx1edi2002as_Address)(((uint32)&MODULE_1EDI2002AS.SCFG) >> IFX1EDI2002AS_ADDRESS_SHIFT), reg.U);
    }
    return status;
}

boolean Ifx1edi2002as_setActiveClampingMode(Ifx1edi2002as_Driver* driver, uint8 value)
{
    boolean status;
    Ifx_1EDI2002AS_SCFG2 reg;
    status = Ifx1edi2002as_Driver_readRegister(driver, (Ifx1edi2002as_Address)(((uint32)&MODULE_1EDI2002AS.SCFG2) >> IFX1EDI2002AS_ADDRESS_SHIFT), &reg.U);
    if (status)
    {
        reg.B.ACLPM = value;
        status &= Ifx1edi2002as_Driver_writeRegister(driver, (Ifx1edi2002as_Address)(((uint32)&MODULE_1EDI2002AS.SCFG2) >> IFX1EDI2002AS_ADDRESS_SHIFT), reg.U);
    }
    return status;
}
boolean Ifx1edi2002as_setActiveClampingTime(Ifx1edi2002as_Driver* driver, uint8 value)
{
    boolean status;
    Ifx_1EDI2002AS_SACLT reg;
    status = Ifx1edi2002as_Driver_readRegister(driver, (Ifx1edi2002as_Address)(((uint32)&MODULE_1EDI2002AS.SACLT) >> IFX1EDI2002AS_ADDRESS_SHIFT), &reg.U);
    if (status)
    {
        reg.B.AT = value;
        status &= Ifx1edi2002as_Driver_writeRegister(driver, (Ifx1edi2002as_Address)(((uint32)&MODULE_1EDI2002AS.SACLT) >> IFX1EDI2002AS_ADDRESS_SHIFT), reg.U);
    }
    return status;
}

