/**
 * \file Ifx1edi2010as_If.c
 * \brief
 *
 * \copyright Copyright (c) 2015 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * This file may be used, copied, and distributed, with or without modification, provided
 * that all copyright notices are retained; that all modifications to this file are
 * prominently noted in the modified file; and that this paragraph is not modified.
 *
 */
#include "SysSe/Ext/1edi2010as/If/Ifx1edi2010as_If.h"
#include "SysSe/Ext/1edi2010as/Hal/Ifx1edi2010as_Hal.h"
#include "SysSe/Bsp/Bsp.h"

/** Message command field*/
typedef enum
{
    Ifx1edi2010as_If_Cmd_enterCmode = 0x1,
    Ifx1edi2010as_If_Cmd_enterVmode = 0x1,
    Ifx1edi2010as_If_Cmd_exitCmode  = 0x1,
    Ifx1edi2010as_If_Cmd_nop        = 0x1,
    Ifx1edi2010as_If_Cmd_read       = 0x0,
    Ifx1edi2010as_If_Cmd_writeh     = 0x4,
    Ifx1edi2010as_If_Cmd_writel     = 0xA
}Ifx1edi2010as_If_Cmd;

/** Message command key (fixed value of data)*/
typedef enum
{
    Ifx1edi2010as_If_CmdKey_enterCmode = 0x440,
    Ifx1edi2010as_If_CmdKey_enterVmode = 0x0A0,
    Ifx1edi2010as_If_CmdKey_exitCmode  = 0x110,
    Ifx1edi2010as_If_CmdKey_nop        = 0x208,
    Ifx1edi2010as_If_CmdKey_read       = 0x15,
    Ifx1edi2010as_If_CmdKey_writeh     = 0x2,
    Ifx1edi2010as_If_CmdKey_writel     = 0x0
}Ifx1edi2010as_If_CmdKey;

/** Message command data*/
typedef enum
{
    Ifx1edi2010as_If_CmdData_enterCmode = (Ifx1edi2010as_If_Cmd_enterCmode << 12) | (Ifx1edi2010as_If_CmdKey_enterCmode << 1),
    Ifx1edi2010as_If_CmdData_enterVmode = (Ifx1edi2010as_If_Cmd_enterVmode << 12) | (Ifx1edi2010as_If_CmdKey_enterVmode << 1),
    Ifx1edi2010as_If_CmdData_exitCmode  = (Ifx1edi2010as_If_Cmd_exitCmode << 12) | (Ifx1edi2010as_If_CmdKey_exitCmode << 1),
    Ifx1edi2010as_If_CmdData_nop        = (Ifx1edi2010as_If_Cmd_nop << 12) | (Ifx1edi2010as_If_CmdKey_nop << 1)
}Ifx1edi2010as_If_CmdData;

/** General MTSR frame*/
typedef uint16 Ifx1edi2010as_If_Mtsr;
/**
 * MRST frame definition (Message: Enter CMODE, Enter VMODE, Exit CMODE, NOP)
 */
typedef union
{
    struct
    {
        uint16 p : 1;
        uint16 data : 11;
        uint16 cmd : 4;
    }      fields;
    uint16 data;
}Ifx1edi2010as_If_MtsrMode;

/**
 * MRST frame definition (Message: READ)
 */
typedef union
{
    struct
    {
        uint16 p : 1;             /**< \brief parity bit */
        uint16 key : 6;           /**< \brief fixed data */
        uint16 address : 5;       /**< \brief register address */
        uint16 cmd : 4;           /**< \brief command */
    }      fields;
    uint16 data;
}Ifx1edi2010as_If_MtsrRead;

/**
 * MRST frame definition (Message: WRITEH)
 */
typedef union
{
    struct
    {
        uint16 p : 1;             /**< \brief parity bit */
        uint16 data : 8;          /**< \brief data */
        uint16 key : 3;           /**< \brief fixed data */
        uint16 cmd : 4;           /**< \brief command */
    }      fields;
    uint16 data;
}Ifx1edi2010as_If_MtsrWriteH;

/**
 * MRST frame definition (Message: WRITEL)
 */
typedef union
{
    struct
    {
        uint16 p : 1;             /**< \brief parity bit */
        uint16 data : 6;          /**< \brief data */
        uint16 address : 5;       /**< \brief register address */
        uint16 cmd : 4;           /**< \brief command */
    }      fields;
    uint16 data;
}Ifx1edi2010as_If_MtsrWriteL;

/**
 * MTSR frame definition
 */
typedef union
{
    struct
    {
        uint16 p : 1;
        uint16 lmi : 1;
        uint16 data : 14;
    }      fields;
    uint16 data;
}Ifx1edi2010as_If_Mrst;

uint8 Ifx1edi2010as_If_calcualteParity(uint16 data)
{
    uint8 p = 1;
    uint8 i = 15;

    while (i)
    {
        data = data >> 1;
        p    = p ^ (data & 1);
        i--;
    }

    return p;
}


/** Checks the parity
 */
boolean Ifx1edi2010as_If_checkCmdResponse(Ifx1edi2010as_If_Mrst *frame)
{
    boolean status = TRUE;

    if ((frame->fields.p == Ifx1edi2010as_If_calcualteParity(frame->data)) && (frame->fields.lmi == 0))
    {
        status = TRUE;
    }
    else
    {
        status = FALSE;
    }

    return status;
}


/** Checks the parity
 */
boolean Ifx1edi2010as_If_checkReadResponse(Ifx1edi2010as_If_Mrst *frame)
{
    boolean status = TRUE;

    if ((frame->fields.p == Ifx1edi2010as_If_calcualteParity(frame->data)) && (frame->fields.lmi == 0))
    {
        status = TRUE;
    }
    else
    {
        status = FALSE;
    }

    return status;
}


/** Checks the parity and lmi
 */
boolean Ifx1edi2010as_If_checkWriteResponse(Ifx1edi2010as_If_Mrst *frame)
{
    boolean status = TRUE;

    if (
        (frame->fields.p == Ifx1edi2010as_If_calcualteParity(frame->data)) && (frame->fields.lmi == 0)
        )
    {
        status = TRUE;
    }
    else
    {
        status = FALSE;
    }

    return status;
}


/** Initialize the MTSR frame (Message: Enter CMODE)
 * \param frame frame to be initialized
 */
void Ifx1edi2010as_If_initMtsrEnterCmode(Ifx1edi2010as_If_MtsrMode *frame)
{
    frame->data = (uint16)Ifx1edi2010as_If_CmdData_enterCmode;
}


/** Initialize the MTSR frame (Message: Enter VMODE)
 * \param frame frame to be initialized
 */
void Ifx1edi2010as_If_initMtsrEnterVmode(Ifx1edi2010as_If_MtsrMode *frame)
{
    frame->data = (uint16)Ifx1edi2010as_If_CmdData_enterVmode;
}


/** Initialize the MTSR frame (Message: Exit CMODE)
 * \param frame frame to be initialized
 */
void Ifx1edi2010as_If_initMtsrExitCmode(Ifx1edi2010as_If_MtsrMode *frame)
{
    frame->data = (uint16)Ifx1edi2010as_If_CmdData_exitCmode;
}


/** Initialize the MTSR frame (Message: NOP)
 * \param frame frame to be initialized
 */
void Ifx1edi2010as_If_initMtsrNop(Ifx1edi2010as_If_MtsrMode *frame)
{
    frame->data = (uint16)Ifx1edi2010as_If_CmdData_nop;
}


/** Initialize the MTSR frame (Message: READ)
 * \param frame frame to be initialized
 * \param address Register address
 */
void Ifx1edi2010as_If_initMtsrRead(Ifx1edi2010as_If_MtsrRead *frame, uint8 address)
{
    frame->fields.key     = Ifx1edi2010as_If_CmdKey_read;
    frame->fields.address = address;
    frame->fields.cmd     = Ifx1edi2010as_If_Cmd_read;
    frame->fields.p       = Ifx1edi2010as_If_calcualteParity(frame->data);
}


/** Initialize the MTSR frame (Message: WRITEH)
 * \param frameH frame to be initialized (High)
 * \param frameL frame to be initialized (Low)
 * \param address Register address
 * \param data Data to be written to the register
 */
void Ifx1edi2010as_If_initMtsrWrite(Ifx1edi2010as_If_MtsrWriteH *frameH, Ifx1edi2010as_If_MtsrWriteL *frameL, uint8 address, uint16 data)
{
    frameH->fields.key     = Ifx1edi2010as_If_CmdKey_writeh;
    frameH->fields.data    = data >> 8;
    frameH->fields.cmd     = Ifx1edi2010as_If_Cmd_writeh;
    frameH->fields.p       = Ifx1edi2010as_If_calcualteParity(frameH->data);

    frameL->fields.address = address;
    frameL->fields.data    = data >> 2;
    frameL->fields.cmd     = Ifx1edi2010as_If_Cmd_writel;
    frameL->fields.p       = Ifx1edi2010as_If_calcualteParity(frameL->data);
}


boolean Ifx1edi2010as_If_readRegister(Ifx1edi2010as_Driver *driver, Ifx1edi2010as_Address address, uint16 *data)
{
    boolean                   status = TRUE;
    Ifx1edi2010as_If_MtsrRead requestRead[IFX1EDI2010AS_MAX_DAISYCHAIN_LENGTH];
    Ifx1edi2010as_If_MtsrMode requestNop[IFX1EDI2010AS_MAX_DAISYCHAIN_LENGTH];
    Ifx1edi2010as_If_Mrst     response[IFX1EDI2010AS_MAX_DAISYCHAIN_LENGTH];

    uint8                     i;
    uint8 lastDevice = driver->daisyChain.length;

    Ifx1edi2010as_If_initMtsrRead(&requestRead[0], address);
    Ifx1edi2010as_If_initMtsrNop(&requestNop[0]);

    for (i = 1; i < lastDevice; i++)
    {
        requestRead[i] = requestRead[0];
        requestNop[i]  = requestNop[0];
    }

#if IFX_CFG_1EDI2010AS_DEBUG_SPI
    if (Ifx1edi2010as_g_showSpiFrame)
    {
		Ifx1edi2010as_Hal_print(_1EDI2010AS_ENDL "1EDI2010AS Read @ 0x%02X: [", address);
    }
#endif

    Ifx1edi2010as_Hal_wait(IFX1EDI2010AS_TIME_10US); /* Ensure CS\ inactive time of 10us (tCSinact=10us) */
    status = Ifx1edi2010as_Hal_exchangeSpi(driver->channel, requestRead, response, lastDevice);
    Ifx1edi2010as_Hal_wait(IFX1EDI2010AS_TIME_10US); /* Ensure CS\ inactive time of 10us (tCSinact=10us) */

    if (status)
    {
        status = Ifx1edi2010as_Hal_exchangeSpi(driver->channel, requestNop, response, lastDevice);

#if IFX_CFG_1EDI2010AS_DEBUG_SPI
        if (Ifx1edi2010as_g_showSpiFrame)
        {
			Ifx1edi2010as_Hal_print("] => ");
        }
#endif

        if (status)
        {
            for (i = 0; i < lastDevice; i++)
            {
            	uint8 daisyChainIndex = lastDevice-i-1;
                if (Ifx1edi2010as_If_checkReadResponse(&response[daisyChainIndex]))
                {
                    data[i] = response[daisyChainIndex].data;
#if IFX_CFG_1EDI2010AS_DEBUG_SPI
                    if (Ifx1edi2010as_g_showSpiFrame)
                    {
						Ifx1edi2010as_Hal_print("0x%02X; ", (data[i] & 0xFFFC)); // Ignore P and LMI
                    }
#endif
                }
                else
                {
                    status = FALSE;
#if IFX_CFG_1EDI2010AS_DEBUG_SPI
                    if (Ifx1edi2010as_g_showSpiFrame)
                    {
						Ifx1edi2010as_Hal_print("ERROR; ");
                    }
#endif
                }
            }
        }
    }

#if IFX_CFG_1EDI2010AS_DEBUG_SPI
    if (Ifx1edi2010as_g_showSpiFrame)
    {
		Ifx1edi2010as_Hal_print(_1EDI2010AS_ENDL);
    }
#endif

    return status;
}


boolean Ifx1edi2010as_If_writeRegister(Ifx1edi2010as_Driver *driver, uint32 mask, Ifx1edi2010as_Address address, uint16 *data)
{
    boolean                     status = TRUE;
    Ifx1edi2010as_If_MtsrWriteH requestWriteHigh[IFX1EDI2010AS_MAX_DAISYCHAIN_LENGTH];
    Ifx1edi2010as_If_MtsrWriteL requestWriteLow[IFX1EDI2010AS_MAX_DAISYCHAIN_LENGTH];
    Ifx1edi2010as_If_MtsrMode   requestNop[IFX1EDI2010AS_MAX_DAISYCHAIN_LENGTH];
    Ifx1edi2010as_If_Mrst       response[IFX1EDI2010AS_MAX_DAISYCHAIN_LENGTH];
    uint8                       i;
    uint8 lastDevice = driver->daisyChain.length;

    Ifx1edi2010as_If_MtsrMode   nop;

    Ifx1edi2010as_If_initMtsrNop(&nop);
#if IFX_CFG_1EDI2010AS_DEBUG_SPI
    if (Ifx1edi2010as_g_showSpiFrame)
    {
		Ifx1edi2010as_Hal_print(_1EDI2010AS_ENDL "1EDI2010AS Write @ 0x%02X: ", address);
    }
#endif

    for (i = 0; i < lastDevice; i++)
    {
    	uint8 daisyChainIndex = lastDevice-i-1;
        if (mask & 0x1)
        {
            Ifx1edi2010as_If_initMtsrWrite(&requestWriteHigh[daisyChainIndex], &requestWriteLow[daisyChainIndex], address, data[i]);
#if IFX_CFG_1EDI2010AS_DEBUG_SPI
            if (Ifx1edi2010as_g_showSpiFrame)
            {
				Ifx1edi2010as_Hal_print("0x%02X; ", (data[i] & 0xFFFC));         // Ignore P and LMI
            }
#endif
        }
        else
        {
            requestWriteHigh[daisyChainIndex].data = nop.data;
            requestWriteLow[daisyChainIndex].data  = nop.data;
#if IFX_CFG_1EDI2010AS_DEBUG_SPI
            if (Ifx1edi2010as_g_showSpiFrame)
            {
				Ifx1edi2010as_Hal_print("NOP; ");
            }
#endif
        }

        requestNop[daisyChainIndex] = nop;
        mask        >>= 1;
    }

#if IFX_CFG_1EDI2010AS_DEBUG_SPI
    if (Ifx1edi2010as_g_showSpiFrame)
    {
		Ifx1edi2010as_Hal_print("[");
    }
#endif
    status = Ifx1edi2010as_Hal_exchangeSpi(driver->channel, requestWriteHigh, response, lastDevice);

    if (status)
    {
        /* No need to check the response */
        status &= Ifx1edi2010as_Hal_exchangeSpi(driver->channel, requestWriteLow, response, lastDevice);

        if (status)
        {
            for (i = 0; i < lastDevice; i++)
            {
                status &= Ifx1edi2010as_If_checkWriteResponse(&response[i]);
            }
        }

        status &= Ifx1edi2010as_Hal_exchangeSpi(driver->channel, requestNop, response, lastDevice);

        if (status)
        {
            for (i = 0; i < lastDevice; i++)
            {
                status &= Ifx1edi2010as_If_checkWriteResponse(&response[i]);
            }
        }
    }

#if IFX_CFG_1EDI2010AS_DEBUG_SPI
    if (Ifx1edi2010as_g_showSpiFrame)
    {
		Ifx1edi2010as_Hal_print("]"_1EDI2010AS_ENDL);
    }
#endif

    return status;
}


boolean Ifx1edi2010as_If_modeTransition(Ifx1edi2010as_Driver *driver, uint32 mask, Ifx1edi2010as_modeTransition mode)
{
    boolean                   status = TRUE;
    Ifx1edi2010as_If_MtsrMode requestMode[IFX1EDI2010AS_MAX_DAISYCHAIN_LENGTH];
    Ifx1edi2010as_If_MtsrMode requestNop[IFX1EDI2010AS_MAX_DAISYCHAIN_LENGTH];
    Ifx1edi2010as_If_Mrst     response[IFX1EDI2010AS_MAX_DAISYCHAIN_LENGTH];
    Ifx1edi2010as_If_MtsrMode nop;
    uint8                     i;
    uint8 lastDevice = driver->daisyChain.length;

    Ifx1edi2010as_If_initMtsrNop(&nop);

    switch (mode)
    {
    case Ifx1edi2010as_modeTransition_enterCmode:
#if IFX_CFG_1EDI2010AS_DEBUG_SPI
        if (Ifx1edi2010as_g_showSpiFrame)
        {
			Ifx1edi2010as_Hal_print(_1EDI2010AS_ENDL "1EDI2010AS enter C MODE: ");
        }
#endif

        for (i = 0; i < lastDevice; i++)
        {
        	uint8 daisyChainIndex = lastDevice-i-1;
            if (mask & 0x1)
            {
                Ifx1edi2010as_If_initMtsrEnterCmode(&requestMode[daisyChainIndex]);
#if IFX_CFG_1EDI2010AS_DEBUG_SPI
                if (Ifx1edi2010as_g_showSpiFrame)
                {
					Ifx1edi2010as_Hal_print("YES; ");
                }
#endif
            }
            else
            {
                requestMode[daisyChainIndex] = nop;
#if IFX_CFG_1EDI2010AS_DEBUG_SPI
                if (Ifx1edi2010as_g_showSpiFrame)
                {
					Ifx1edi2010as_Hal_print("NOP; ");
                }
#endif
            }

            requestNop[daisyChainIndex] = nop;
            mask        >>= 1;
        }

        break;
    case Ifx1edi2010as_modeTransition_enterVmode:
#if IFX_CFG_1EDI2010AS_DEBUG_SPI
        if (Ifx1edi2010as_g_showSpiFrame)
        {
			Ifx1edi2010as_Hal_print(_1EDI2010AS_ENDL "1EDI2010AS enter V MODE: ");
        }
#endif

        for (i = 0; i < lastDevice; i++)
        {
        	uint8 daisyChainIndex = lastDevice-i-1;
            if (mask & 0x1)
            {
                Ifx1edi2010as_If_initMtsrEnterVmode(&requestMode[daisyChainIndex]);
#if IFX_CFG_1EDI2010AS_DEBUG_SPI
                if (Ifx1edi2010as_g_showSpiFrame)
                {
					Ifx1edi2010as_Hal_print("YES; ");
                }
#endif
            }
            else
            {
                requestMode[daisyChainIndex] = nop;
#if IFX_CFG_1EDI2010AS_DEBUG_SPI
                if (Ifx1edi2010as_g_showSpiFrame)
                {
					Ifx1edi2010as_Hal_print("NOP; ");
                }
#endif
            }

            requestNop[daisyChainIndex] = nop;
            mask        >>= 1;
        }

        break;
    case Ifx1edi2010as_modeTransition_exitCmode:
#if IFX_CFG_1EDI2010AS_DEBUG_SPI
        if (Ifx1edi2010as_g_showSpiFrame)
        {
			Ifx1edi2010as_Hal_print(_1EDI2010AS_ENDL "1EDI2010AS exit C MODE: ");
        }
#endif

        for (i = 0; i < lastDevice; i++)
        {
        	uint8 daisyChainIndex = lastDevice-i-1;
            if (mask & 0x1)
            {
                Ifx1edi2010as_If_initMtsrExitCmode(&requestMode[daisyChainIndex]);
#if IFX_CFG_1EDI2010AS_DEBUG_SPI
                if (Ifx1edi2010as_g_showSpiFrame)
                {
					Ifx1edi2010as_Hal_print("YES; ");
                }
#endif
            }
            else
            {
                requestMode[daisyChainIndex] = nop;
#if IFX_CFG_1EDI2010AS_DEBUG_SPI
                if (Ifx1edi2010as_g_showSpiFrame)
                {
					Ifx1edi2010as_Hal_print("NOP; ");
                }
#endif
            }

            requestNop[daisyChainIndex] = nop;
            mask        >>= 1;
        }

        break;
    default:
        status = FALSE;
        break;
    }

#if IFX_CFG_1EDI2010AS_DEBUG_SPI
    if (Ifx1edi2010as_g_showSpiFrame)
    {
		Ifx1edi2010as_Hal_print("[");
    }
#endif

    status &= Ifx1edi2010as_Hal_exchangeSpi(driver->channel, requestMode, response, lastDevice);

    if (status)
    {
        status &= Ifx1edi2010as_Hal_exchangeSpi(driver->channel, requestNop, response, lastDevice);

        if (status)
        {
            for (i = 0; i < lastDevice; i++)
            {
                status &= Ifx1edi2010as_If_checkCmdResponse(&response[i]);
            }
        }
    }

#if IFX_CFG_1EDI2010AS_DEBUG_SPI
    if (Ifx1edi2010as_g_showSpiFrame)
    {
		Ifx1edi2010as_Hal_print("]"_1EDI2010AS_ENDL);
    }
#endif

    return status;
}
