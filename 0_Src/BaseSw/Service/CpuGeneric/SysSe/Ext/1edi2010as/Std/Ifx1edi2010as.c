/**
 * \file Ifx1edi2010as.c
 * \brief 1EDI2010AS  basic functionality 
 *
 * \copyright Copyright (c) 2017 Infineon Technologies AG. All rights reserved.
 *
 * $Date: 2017-06-30 15:39:06
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 */

/******************************************************************************/
/*----------------------------------Includes----------------------------------*/
/******************************************************************************/

#include "Ifx1edi2010as.h"
#include <stdio.h>


            
/******************************************************************************/
/*-----------------------Exported Variables/Constants-------------------------*/
/******************************************************************************/

IFX_CONST uint32 Ifx1edi2010as_g_actionReadRegisters[1] = 
{
    0x00000000
};

IFX_CONST uint32 Ifx1edi2010as_g_actionWriteRegisters[1] = 
{
    0x001000C0
};

IFX_CONST Ifx_1EDI2010AS Ifx1edi2010as_g_noOpMask = 
{
    .PID.U = 0x0,
    .PSTAT.U = 0x0,
    .PSTAT2.U = 0x0,
    .PER.U = 0x0,
    .PCFG.U = 0x0,
    .PCFG2.U = 0x0,
    .PCTRL.U = 0x60,
    .PCTRL2.U = 0x20,
    .PSCR.U = 0x0,
    .PRW.U = 0x0,
    .PPIN.U = 0x0,
    .PCS.U = 0x0,
    .SID.U = 0x0,
    .SSTAT.U = 0x0,
    .SSTAT2.U = 0x0,
    .SER.U = 0x0,
    .SCFG.U = 0x0,
    .SCFG2.U = 0x0,
    .SSCR.U = 0x0,
    .SDESAT.U = 0x0,
    .SOCP.U = 0x0,
    .SRTTOF.U = 0x0,
    .SSTTOF.U = 0x0,
    .STTON.U = 0x0,
    .SADC.U = 0x0,
    .SBC.U = 0x0,
    .SCS.U = 0x0
};

IFX_CONST Ifx_1EDI2010AS Ifx1edi2010as_g_noOpValue = 
{
    .PID.U = 0x0,
    .PSTAT.U = 0x0,
    .PSTAT2.U = 0x0,
    .PER.U = 0x0,
    .PCFG.U = 0x0,
    .PCFG2.U = 0x0,
    .PCTRL.U = 0x0,
    .PCTRL2.U = 0x0,
    .PSCR.U = 0x0,
    .PRW.U = 0x0,
    .PPIN.U = 0x0,
    .PCS.U = 0x0,
    .SID.U = 0x0,
    .SSTAT.U = 0x0,
    .SSTAT2.U = 0x0,
    .SER.U = 0x0,
    .SCFG.U = 0x0,
    .SCFG2.U = 0x0,
    .SSCR.U = 0x0,
    .SDESAT.U = 0x0,
    .SOCP.U = 0x0,
    .SRTTOF.U = 0x0,
    .SSTTOF.U = 0x0,
    .STTON.U = 0x0,
    .SADC.U = 0x0,
    .SBC.U = 0x0,
    .SCS.U = 0x0
};

IFX_CONST uint32 Ifx1edi2010as_g_validAddress[1] = 
{
    0xFFBF0FFF
};

IFX_CONST uint32 Ifx1edi2010as_g_volatileRegisters[1] = 
{
    0xA09E0DCE
};


/******************************************************************************/
/*-------------------------Function Implementations---------------------------*/
/******************************************************************************/

Ifx1edi2010as_PrimaryChipIdentification Ifx1edi2010as_getPrimaryChipIdentification(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_PrimaryChipIdentification value;
    Ifx_1EDI2010AS_PID reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PID) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_PrimaryChipIdentification)reg.B.PVERS;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

uint8 Ifx1edi2010as_getGateRegularTtoffPlateauLevelConfigurationStatus(Ifx1edi2010as_Driver* driver, boolean* status)
{
    uint8 value;
    Ifx_1EDI2010AS_PSTAT reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PSTAT) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (uint8)reg.B.GPOFP;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

Ifx1edi2010as_SecondaryReadyStatus Ifx1edi2010as_getSecondaryReadyStatus(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_SecondaryReadyStatus value;
    Ifx_1EDI2010AS_PSTAT reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PSTAT) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_SecondaryReadyStatus)reg.B.SRDY;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean Ifx1edi2010as_isSecondaryReady(Ifx1edi2010as_Driver* driver, boolean* status)
{
    boolean value;
    Ifx_1EDI2010AS_PSTAT reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PSTAT) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.SRDY == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

Ifx1edi2010as_ActiveStateStatus Ifx1edi2010as_getActiveStateStatus(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_ActiveStateStatus value;
    Ifx_1EDI2010AS_PSTAT reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PSTAT) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_ActiveStateStatus)reg.B.ACT;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

uint8 Ifx1edi2010as_getGateTtonPlateauLevelConfigurationStatus(Ifx1edi2010as_Driver* driver, boolean* status)
{
    uint8 value;
    Ifx_1EDI2010AS_PSTAT reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PSTAT) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (uint8)reg.B.GPONP;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

Ifx1edi2010as_ErrorStatus Ifx1edi2010as_getErrorStatus(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_ErrorStatus value;
    Ifx_1EDI2010AS_PSTAT reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PSTAT) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_ErrorStatus)reg.B.ERR;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean Ifx1edi2010as_isError(Ifx1edi2010as_Driver* driver, boolean* status)
{
    boolean value;
    Ifx_1EDI2010AS_PSTAT reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PSTAT) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.ERR == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

Ifx1edi2010as_EnValidStatus Ifx1edi2010as_getEnValidStatus(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_EnValidStatus value;
    Ifx_1EDI2010AS_PSTAT2 reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PSTAT2) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_EnValidStatus)reg.B.ENVAL;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

Ifx1edi2010as_NfltaPinDriverRequest Ifx1edi2010as_getNfltaPinDriverRequest(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_NfltaPinDriverRequest value;
    Ifx_1EDI2010AS_PSTAT2 reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PSTAT2) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_NfltaPinDriverRequest)reg.B.FLTA;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

Ifx1edi2010as_NfltbPinDriverRequest Ifx1edi2010as_getNfltbPinDriverRequest(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_NfltbPinDriverRequest value;
    Ifx_1EDI2010AS_PSTAT2 reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PSTAT2) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_NfltbPinDriverRequest)reg.B.FLTB;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

Ifx1edi2010as_OperatingMode Ifx1edi2010as_getOperatingMode(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_OperatingMode value;
    Ifx_1EDI2010AS_PSTAT2 reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PSTAT2) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_OperatingMode)reg.B.OPMP;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

Ifx1edi2010as_EventClassAStatus Ifx1edi2010as_getEventClassAStatus(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_EventClassAStatus value;
    Ifx_1EDI2010AS_PSTAT2 reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PSTAT2) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_EventClassAStatus)reg.B.FLTAP;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

Ifx1edi2010as_EventClassBStatus Ifx1edi2010as_getEventClassBStatus(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_EventClassBStatus value;
    Ifx_1EDI2010AS_PSTAT2 reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PSTAT2) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_EventClassBStatus)reg.B.FLTBP;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

Ifx1edi2010as_ShootThroughProtectionStatus Ifx1edi2010as_getShootThroughProtectionStatus(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_ShootThroughProtectionStatus value;
    Ifx_1EDI2010AS_PSTAT2 reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PSTAT2) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_ShootThroughProtectionStatus)reg.B.STP;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

Ifx1edi2010as_AdcUnderOrOvervoltageErrorStatus Ifx1edi2010as_getAdcUnderOrOvervoltageErrorStatus(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_AdcUnderOrOvervoltageErrorStatus value;
    Ifx_1EDI2010AS_PSTAT2 reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PSTAT2) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_AdcUnderOrOvervoltageErrorStatus)reg.B.AXVP;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean Ifx1edi2010as_isAdcUnderOrOvervoltageError(Ifx1edi2010as_Driver* driver, boolean* status)
{
    boolean value;
    Ifx_1EDI2010AS_PSTAT2 reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PSTAT2) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.AXVP == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

Ifx1edi2010as_PrimaryCommunicationErrorFlag Ifx1edi2010as_getPrimaryCommunicationErrorFlag(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_PrimaryCommunicationErrorFlag value;
    Ifx_1EDI2010AS_PER reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PER) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_PrimaryCommunicationErrorFlag)reg.B.CERP;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean Ifx1edi2010as_isPrimaryCommunicationErrorFlagSet(Ifx1edi2010as_Driver* driver, boolean* status)
{
    boolean value;
    Ifx_1EDI2010AS_PER reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PER) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.CERP == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

Ifx1edi2010as_AdcErrorFlag Ifx1edi2010as_getAdcErrorFlag(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_AdcErrorFlag value;
    Ifx_1EDI2010AS_PER reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PER) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_AdcErrorFlag)reg.B.ADER;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean Ifx1edi2010as_isAdcErrorFlagSet(Ifx1edi2010as_Driver* driver, boolean* status)
{
    boolean value;
    Ifx_1EDI2010AS_PER reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PER) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.ADER == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

Ifx1edi2010as_SpiErrorFlag Ifx1edi2010as_getSpiErrorFlag(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_SpiErrorFlag value;
    Ifx_1EDI2010AS_PER reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PER) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_SpiErrorFlag)reg.B.SPIER;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean Ifx1edi2010as_isSpiErrorFlagSet(Ifx1edi2010as_Driver* driver, boolean* status)
{
    boolean value;
    Ifx_1EDI2010AS_PER reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PER) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.SPIER == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

Ifx1edi2010as_ShootThroughProtectionErrorFlag Ifx1edi2010as_getShootThroughProtectionErrorFlag(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_ShootThroughProtectionErrorFlag value;
    Ifx_1EDI2010AS_PER reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PER) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_ShootThroughProtectionErrorFlag)reg.B.STPER;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean Ifx1edi2010as_isShootThroughProtectionErrorFlagSet(Ifx1edi2010as_Driver* driver, boolean* status)
{
    boolean value;
    Ifx_1EDI2010AS_PER reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PER) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.STPER == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

Ifx1edi2010as_EnSignalInvalidFlag Ifx1edi2010as_getEnSignalInvalidFlag(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_EnSignalInvalidFlag value;
    Ifx_1EDI2010AS_PER reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PER) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_EnSignalInvalidFlag)reg.B.ENER;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean Ifx1edi2010as_isEnSignalInvalidFlagSet(Ifx1edi2010as_Driver* driver, boolean* status)
{
    boolean value;
    Ifx_1EDI2010AS_PER reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PER) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.ENER == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

Ifx1edi2010as_PrimaryResetFlag Ifx1edi2010as_getPrimaryResetFlag(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_PrimaryResetFlag value;
    Ifx_1EDI2010AS_PER reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PER) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_PrimaryResetFlag)reg.B.RSTP;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean Ifx1edi2010as_isPrimaryResetFlagSet(Ifx1edi2010as_Driver* driver, boolean* status)
{
    boolean value;
    Ifx_1EDI2010AS_PER reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PER) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.RSTP == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

Ifx1edi2010as_PrimaryExternalHardResetFlag Ifx1edi2010as_getPrimaryExternalHardResetFlag(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_PrimaryExternalHardResetFlag value;
    Ifx_1EDI2010AS_PER reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PER) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_PrimaryExternalHardResetFlag)reg.B.RSTEP;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean Ifx1edi2010as_isPrimaryExternalHardResetFlagSet(Ifx1edi2010as_Driver* driver, boolean* status)
{
    boolean value;
    Ifx_1EDI2010AS_PER reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PER) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.RSTEP == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

Ifx1edi2010as_SpiParityEnable Ifx1edi2010as_getSpiParityEnable(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_SpiParityEnable value;
    Ifx_1EDI2010AS_PCFG reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_SpiParityEnable)reg.B.PAREN;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean Ifx1edi2010as_isSpiParityEnabled(Ifx1edi2010as_Driver* driver, boolean* status)
{
    boolean value;
    Ifx_1EDI2010AS_PCFG reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.PAREN == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void Ifx1edi2010as_setSpiParityEnable(Ifx1edi2010as_Driver* driver, Ifx1edi2010as_SpiParityEnable value, boolean* status)
{
    Ifx_1EDI2010AS_PCFG reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.PAREN = value;
        *status &= Ifx1edi2010as_Driver_writeRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void Ifx1edi2010as_enableSpiParity(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx_1EDI2010AS_PCFG reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.PAREN = 1;
        *status &= Ifx1edi2010as_Driver_writeRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void Ifx1edi2010as_disableSpiParity(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx_1EDI2010AS_PCFG reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.PAREN = 0;
        *status &= Ifx1edi2010as_Driver_writeRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

Ifx1edi2010as_NfltaPinActivationOnBoundaryCheckEventEnable Ifx1edi2010as_getNfltaPinActivationOnBoundaryCheckEventEnable(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_NfltaPinActivationOnBoundaryCheckEventEnable value;
    Ifx_1EDI2010AS_PCFG reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_NfltaPinActivationOnBoundaryCheckEventEnable)reg.B.ADAEN;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean Ifx1edi2010as_isNfltaPinActivationOnBoundaryCheckEventEnabled(Ifx1edi2010as_Driver* driver, boolean* status)
{
    boolean value;
    Ifx_1EDI2010AS_PCFG reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.ADAEN == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void Ifx1edi2010as_setNfltaPinActivationOnBoundaryCheckEventEnable(Ifx1edi2010as_Driver* driver, Ifx1edi2010as_NfltaPinActivationOnBoundaryCheckEventEnable value, boolean* status)
{
    Ifx_1EDI2010AS_PCFG reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.ADAEN = value;
        *status &= Ifx1edi2010as_Driver_writeRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void Ifx1edi2010as_enableNfltaPinActivationOnBoundaryCheckEvent(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx_1EDI2010AS_PCFG reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.ADAEN = 1;
        *status &= Ifx1edi2010as_Driver_writeRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void Ifx1edi2010as_disableNfltaPinActivationOnBoundaryCheckEvent(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx_1EDI2010AS_PCFG reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.ADAEN = 0;
        *status &= Ifx1edi2010as_Driver_writeRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

Ifx1edi2010as_AdcTriggerInputEnable Ifx1edi2010as_getAdcTriggerInputEnable(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_AdcTriggerInputEnable value;
    Ifx_1EDI2010AS_PCFG reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_AdcTriggerInputEnable)reg.B.ADTEN;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean Ifx1edi2010as_isAdcTriggerInputEnabled(Ifx1edi2010as_Driver* driver, boolean* status)
{
    boolean value;
    Ifx_1EDI2010AS_PCFG reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.ADTEN == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void Ifx1edi2010as_setAdcTriggerInputEnable(Ifx1edi2010as_Driver* driver, Ifx1edi2010as_AdcTriggerInputEnable value, boolean* status)
{
    Ifx_1EDI2010AS_PCFG reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.ADTEN = value;
        *status &= Ifx1edi2010as_Driver_writeRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void Ifx1edi2010as_enableAdcTriggerInput(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx_1EDI2010AS_PCFG reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.ADTEN = 1;
        *status &= Ifx1edi2010as_Driver_writeRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void Ifx1edi2010as_disableAdcTriggerInput(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx_1EDI2010AS_PCFG reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.ADTEN = 0;
        *status &= Ifx1edi2010as_Driver_writeRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

Ifx1edi2010as_ShootThroughProtectionDelay Ifx1edi2010as_getShootThroughProtectionDelay(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_ShootThroughProtectionDelay value;
    Ifx_1EDI2010AS_PCFG2 reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PCFG2) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_ShootThroughProtectionDelay)reg.B.STPDEL;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void Ifx1edi2010as_setShootThroughProtectionDelay(Ifx1edi2010as_Driver* driver, Ifx1edi2010as_ShootThroughProtectionDelay value, boolean* status)
{
    Ifx_1EDI2010AS_PCFG2 reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PCFG2) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.STPDEL = value;
        *status &= Ifx1edi2010as_Driver_writeRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PCFG2) >> IFX1EDI2010AS_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

Ifx1edi2010as_Dio1PinMode Ifx1edi2010as_getDio1PinMode(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_Dio1PinMode value;
    Ifx_1EDI2010AS_PCFG2 reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PCFG2) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_Dio1PinMode)reg.B.DIO1;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void Ifx1edi2010as_setDio1PinMode(Ifx1edi2010as_Driver* driver, Ifx1edi2010as_Dio1PinMode value, boolean* status)
{
    Ifx_1EDI2010AS_PCFG2 reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PCFG2) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.DIO1 = value;
        *status &= Ifx1edi2010as_Driver_writeRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PCFG2) >> IFX1EDI2010AS_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

Ifx1edi2010as_GateTtonPlateauLevel Ifx1edi2010as_getGateTtonPlateauLevel(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_GateTtonPlateauLevel value;
    Ifx_1EDI2010AS_PCTRL reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PCTRL) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_GateTtonPlateauLevel)reg.B.GPON;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void Ifx1edi2010as_setGateTtonPlateauLevel(Ifx1edi2010as_Driver* driver, Ifx1edi2010as_GateTtonPlateauLevel value, boolean* status)
{
    Ifx_1EDI2010AS_PCTRL reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PCTRL) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.GPON = value;
        *status &= Ifx1edi2010as_Driver_writeRegisterStar(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PCTRL) >> IFX1EDI2010AS_ADDRESS_SHIFT), reg.U, IFX_1EDI2010AS_PCTRL_GPON_MSK << IFX_1EDI2010AS_PCTRL_GPON_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

Ifx1edi2010as_ClearPrimarySitckyBits Ifx1edi2010as_getClearPrimarySitckyBits(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_ClearPrimarySitckyBits value;
    Ifx_1EDI2010AS_PCTRL reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PCTRL) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_ClearPrimarySitckyBits)reg.B.CLRP;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void Ifx1edi2010as_setClearPrimarySitckyBits(Ifx1edi2010as_Driver* driver, Ifx1edi2010as_ClearPrimarySitckyBits value, boolean* status)
{
    Ifx_1EDI2010AS_PCTRL reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PCTRL) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.CLRP = value;
        *status &= Ifx1edi2010as_Driver_writeRegisterStar(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PCTRL) >> IFX1EDI2010AS_ADDRESS_SHIFT), reg.U, IFX_1EDI2010AS_PCTRL_CLRP_MSK << IFX_1EDI2010AS_PCTRL_CLRP_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

void Ifx1edi2010as_clearPrimarySitckyBits(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx_1EDI2010AS_PCTRL reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PCTRL) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.CLRP = 1;
        *status &= Ifx1edi2010as_Driver_writeRegisterStar(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PCTRL) >> IFX1EDI2010AS_ADDRESS_SHIFT), reg.U, IFX_1EDI2010AS_PCTRL_CLRP_MSK << IFX_1EDI2010AS_PCTRL_CLRP_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

Ifx1edi2010as_ClearSecondarySitckyBits Ifx1edi2010as_getClearSecondarySitckyBits(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_ClearSecondarySitckyBits value;
    Ifx_1EDI2010AS_PCTRL reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PCTRL) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_ClearSecondarySitckyBits)reg.B.CLRS;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void Ifx1edi2010as_setClearSecondarySitckyBits(Ifx1edi2010as_Driver* driver, Ifx1edi2010as_ClearSecondarySitckyBits value, boolean* status)
{
    Ifx_1EDI2010AS_PCTRL reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PCTRL) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.CLRS = value;
        *status &= Ifx1edi2010as_Driver_writeRegisterStar(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PCTRL) >> IFX1EDI2010AS_ADDRESS_SHIFT), reg.U, IFX_1EDI2010AS_PCTRL_CLRS_MSK << IFX_1EDI2010AS_PCTRL_CLRS_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

void Ifx1edi2010as_clearSecondarySitckyBits(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx_1EDI2010AS_PCTRL reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PCTRL) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.CLRS = 1;
        *status &= Ifx1edi2010as_Driver_writeRegisterStar(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PCTRL) >> IFX1EDI2010AS_ADDRESS_SHIFT), reg.U, IFX_1EDI2010AS_PCTRL_CLRS_MSK << IFX_1EDI2010AS_PCTRL_CLRS_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

Ifx1edi2010as_GateRegularTtoffPlateauLevel Ifx1edi2010as_getGateRegularTtoffPlateauLevel(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_GateRegularTtoffPlateauLevel value;
    Ifx_1EDI2010AS_PCTRL2 reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PCTRL2) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_GateRegularTtoffPlateauLevel)reg.B.GPOF;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void Ifx1edi2010as_setGateRegularTtoffPlateauLevel(Ifx1edi2010as_Driver* driver, Ifx1edi2010as_GateRegularTtoffPlateauLevel value, boolean* status)
{
    Ifx_1EDI2010AS_PCTRL2 reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PCTRL2) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.GPOF = value;
        *status &= Ifx1edi2010as_Driver_writeRegisterStar(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PCTRL2) >> IFX1EDI2010AS_ADDRESS_SHIFT), reg.U, IFX_1EDI2010AS_PCTRL2_GPOF_MSK << IFX_1EDI2010AS_PCTRL2_GPOF_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

Ifx1edi2010as_AdcConversionRequest Ifx1edi2010as_getAdcConversionRequest(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_AdcConversionRequest value;
    Ifx_1EDI2010AS_PCTRL2 reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PCTRL2) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_AdcConversionRequest)reg.B.ACRP;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void Ifx1edi2010as_setAdcConversionRequest(Ifx1edi2010as_Driver* driver, Ifx1edi2010as_AdcConversionRequest value, boolean* status)
{
    Ifx_1EDI2010AS_PCTRL2 reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PCTRL2) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.ACRP = value;
        *status &= Ifx1edi2010as_Driver_writeRegisterStar(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PCTRL2) >> IFX1EDI2010AS_ADDRESS_SHIFT), reg.U, IFX_1EDI2010AS_PCTRL2_ACRP_MSK << IFX_1EDI2010AS_PCTRL2_ACRP_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

Ifx1edi2010as_PrimaryVerificationFunction Ifx1edi2010as_getPrimaryVerificationFunction(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_PrimaryVerificationFunction value;
    Ifx_1EDI2010AS_PSCR reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PSCR) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_PrimaryVerificationFunction)reg.B.VFSP;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void Ifx1edi2010as_setPrimaryVerificationFunction(Ifx1edi2010as_Driver* driver, Ifx1edi2010as_PrimaryVerificationFunction value, boolean* status)
{
    Ifx_1EDI2010AS_PSCR reg;
    reg.U = 0;
    reg.B.VFSP = value;
    *status &= Ifx1edi2010as_Driver_writeRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PSCR) >> IFX1EDI2010AS_ADDRESS_SHIFT), reg.U);
}

uint16 Ifx1edi2010as_getDataIntegrityTestRegister(Ifx1edi2010as_Driver* driver, boolean* status)
{
    uint16 value;
    Ifx_1EDI2010AS_PRW reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PRW) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (uint16)reg.B.RWVAL;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void Ifx1edi2010as_setDataIntegrityTestRegister(Ifx1edi2010as_Driver* driver, uint16 value, boolean* status)
{
    Ifx_1EDI2010AS_PRW reg;
    reg.U = 0;
    reg.B.RWVAL = value;
    *status &= Ifx1edi2010as_Driver_writeRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PRW) >> IFX1EDI2010AS_ADDRESS_SHIFT), reg.U);
}

Ifx1edi2010as_InpPinLevel Ifx1edi2010as_getInpPinLevel(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_InpPinLevel value;
    Ifx_1EDI2010AS_PPIN reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PPIN) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_InpPinLevel)reg.B.INPL;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

Ifx1edi2010as_InstpPinLevel Ifx1edi2010as_getInstpPinLevel(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_InstpPinLevel value;
    Ifx_1EDI2010AS_PPIN reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PPIN) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_InstpPinLevel)reg.B.INSTPL;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

Ifx1edi2010as_EnPinLevel Ifx1edi2010as_getEnPinLevel(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_EnPinLevel value;
    Ifx_1EDI2010AS_PPIN reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PPIN) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_EnPinLevel)reg.B.ENL;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

Ifx1edi2010as_NfltaPinLevel Ifx1edi2010as_getNfltaPinLevel(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_NfltaPinLevel value;
    Ifx_1EDI2010AS_PPIN reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PPIN) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_NfltaPinLevel)reg.B.NFLTAL;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

Ifx1edi2010as_NfltbPinLevel Ifx1edi2010as_getNfltbPinLevel(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_NfltbPinLevel value;
    Ifx_1EDI2010AS_PPIN reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PPIN) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_NfltbPinLevel)reg.B.NFLTBL;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

Ifx1edi2010as_AdcTriggerInputLevel Ifx1edi2010as_getAdcTriggerInputLevel(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_AdcTriggerInputLevel value;
    Ifx_1EDI2010AS_PPIN reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PPIN) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_AdcTriggerInputLevel)reg.B.ADCTL;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

Ifx1edi2010as_Dio1PinLevel Ifx1edi2010as_getDio1PinLevel(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_Dio1PinLevel value;
    Ifx_1EDI2010AS_PPIN reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PPIN) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_Dio1PinLevel)reg.B.DIO1L;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

uint8 Ifx1edi2010as_getPrimaryClockSupervision(Ifx1edi2010as_Driver* driver, boolean* status)
{
    uint8 value;
    Ifx_1EDI2010AS_PCS reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.PCS) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (uint8)reg.B.CSP;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

uint16 Ifx1edi2010as_getSecondaryChipIdentification(Ifx1edi2010as_Driver* driver, boolean* status)
{
    uint16 value;
    Ifx_1EDI2010AS_SID reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SID) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (uint16)reg.B.SVERS;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

Ifx1edi2010as_PwmCommandStatus Ifx1edi2010as_getPwmCommandStatus(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_PwmCommandStatus value;
    Ifx_1EDI2010AS_SSTAT reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SSTAT) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_PwmCommandStatus)reg.B.PWM;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

Ifx1edi2010as_DebugModeActiveFlag Ifx1edi2010as_getDebugModeActiveFlag(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_DebugModeActiveFlag value;
    Ifx_1EDI2010AS_SSTAT reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SSTAT) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_DebugModeActiveFlag)reg.B.DBG;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean Ifx1edi2010as_isDebugModeActiveFlagSet(Ifx1edi2010as_Driver* driver, boolean* status)
{
    boolean value;
    Ifx_1EDI2010AS_SSTAT reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SSTAT) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.DBG == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

Ifx1edi2010as_DesatComparatorResult Ifx1edi2010as_getDesatComparatorResult(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_DesatComparatorResult value;
    Ifx_1EDI2010AS_SSTAT2 reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SSTAT2) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_DesatComparatorResult)reg.B.DSATC;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

Ifx1edi2010as_OcpComparatorResult Ifx1edi2010as_getOcpComparatorResult(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_OcpComparatorResult value;
    Ifx_1EDI2010AS_SSTAT2 reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SSTAT2) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_OcpComparatorResult)reg.B.OCPC;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

Ifx1edi2010as_Uvlo2Event Ifx1edi2010as_getUvlo2Event(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_Uvlo2Event value;
    Ifx_1EDI2010AS_SSTAT2 reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SSTAT2) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_Uvlo2Event)reg.B.UVLO2M;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

Ifx1edi2010as_Dio2PinLevel Ifx1edi2010as_getDio2PinLevel(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_Dio2PinLevel value;
    Ifx_1EDI2010AS_SSTAT2 reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SSTAT2) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_Dio2PinLevel)reg.B.DIO2L;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

Ifx1edi2010as_DaclpPinOutpoutLevel Ifx1edi2010as_getDaclpPinOutpoutLevel(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_DaclpPinOutpoutLevel value;
    Ifx_1EDI2010AS_SSTAT2 reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SSTAT2) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_DaclpPinOutpoutLevel)reg.B.DACL;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

Ifx1edi2010as_CommunicationErrorSecondaryFlag Ifx1edi2010as_getCommunicationErrorSecondaryFlag(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_CommunicationErrorSecondaryFlag value;
    Ifx_1EDI2010AS_SER reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SER) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_CommunicationErrorSecondaryFlag)reg.B.CERS;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean Ifx1edi2010as_isCommunicationErrorSecondaryFlagSet(Ifx1edi2010as_Driver* driver, boolean* status)
{
    boolean value;
    Ifx_1EDI2010AS_SER reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SER) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.CERS == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

Ifx1edi2010as_AdcUndervoltageErrorFlag Ifx1edi2010as_getAdcUndervoltageErrorFlag(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_AdcUndervoltageErrorFlag value;
    Ifx_1EDI2010AS_SER reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SER) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_AdcUndervoltageErrorFlag)reg.B.AUVER;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean Ifx1edi2010as_isAdcUndervoltageErrorFlagSet(Ifx1edi2010as_Driver* driver, boolean* status)
{
    boolean value;
    Ifx_1EDI2010AS_SER reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SER) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.AUVER == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

Ifx1edi2010as_AdcOvervoltageErrorFlag Ifx1edi2010as_getAdcOvervoltageErrorFlag(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_AdcOvervoltageErrorFlag value;
    Ifx_1EDI2010AS_SER reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SER) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_AdcOvervoltageErrorFlag)reg.B.AOVER;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean Ifx1edi2010as_isAdcOvervoltageErrorFlagSet(Ifx1edi2010as_Driver* driver, boolean* status)
{
    boolean value;
    Ifx_1EDI2010AS_SER reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SER) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.AOVER == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

Ifx1edi2010as_VerificationModeTimeOutEventFlag Ifx1edi2010as_getVerificationModeTimeOutEventFlag(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_VerificationModeTimeOutEventFlag value;
    Ifx_1EDI2010AS_SER reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SER) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_VerificationModeTimeOutEventFlag)reg.B.VMTO;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean Ifx1edi2010as_isVerificationModeTimeOutEventFlagSet(Ifx1edi2010as_Driver* driver, boolean* status)
{
    boolean value;
    Ifx_1EDI2010AS_SER reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SER) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.VMTO == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

Ifx1edi2010as_Uvlo2ErrorFlag Ifx1edi2010as_getUvlo2ErrorFlag(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_Uvlo2ErrorFlag value;
    Ifx_1EDI2010AS_SER reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SER) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_Uvlo2ErrorFlag)reg.B.UVLO2ER;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean Ifx1edi2010as_isUvlo2ErrorFlagSet(Ifx1edi2010as_Driver* driver, boolean* status)
{
    boolean value;
    Ifx_1EDI2010AS_SER reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SER) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.UVLO2ER == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

Ifx1edi2010as_DesatErrorFlag Ifx1edi2010as_getDesatErrorFlag(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_DesatErrorFlag value;
    Ifx_1EDI2010AS_SER reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SER) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_DesatErrorFlag)reg.B.DESATER;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean Ifx1edi2010as_isDesatErrorFlagSet(Ifx1edi2010as_Driver* driver, boolean* status)
{
    boolean value;
    Ifx_1EDI2010AS_SER reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SER) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.DESATER == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

Ifx1edi2010as_OcpErrorFlag Ifx1edi2010as_getOcpErrorFlag(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_OcpErrorFlag value;
    Ifx_1EDI2010AS_SER reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SER) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_OcpErrorFlag)reg.B.OCPER;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean Ifx1edi2010as_isOcpErrorFlagSet(Ifx1edi2010as_Driver* driver, boolean* status)
{
    boolean value;
    Ifx_1EDI2010AS_SER reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SER) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.OCPER == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

Ifx1edi2010as_SecondaryHardResetFlag Ifx1edi2010as_getSecondaryHardResetFlag(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_SecondaryHardResetFlag value;
    Ifx_1EDI2010AS_SER reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SER) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_SecondaryHardResetFlag)reg.B.RSTS;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean Ifx1edi2010as_isSecondaryHardResetFlagSet(Ifx1edi2010as_Driver* driver, boolean* status)
{
    boolean value;
    Ifx_1EDI2010AS_SER reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SER) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.RSTS == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

Ifx1edi2010as_VbeCompensationEnable Ifx1edi2010as_getVbeCompensationEnable(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_VbeCompensationEnable value;
    Ifx_1EDI2010AS_SCFG reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_VbeCompensationEnable)reg.B.VBEC;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean Ifx1edi2010as_isVbeCompensationEnabled(Ifx1edi2010as_Driver* driver, boolean* status)
{
    boolean value;
    Ifx_1EDI2010AS_SCFG reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.VBEC == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void Ifx1edi2010as_setVbeCompensationEnable(Ifx1edi2010as_Driver* driver, Ifx1edi2010as_VbeCompensationEnable value, boolean* status)
{
    Ifx_1EDI2010AS_SCFG reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.VBEC = value;
        *status &= Ifx1edi2010as_Driver_writeRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void Ifx1edi2010as_enableVbeCompensation(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx_1EDI2010AS_SCFG reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.VBEC = 1;
        *status &= Ifx1edi2010as_Driver_writeRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void Ifx1edi2010as_disableVbeCompensation(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx_1EDI2010AS_SCFG reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.VBEC = 0;
        *status &= Ifx1edi2010as_Driver_writeRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

Ifx1edi2010as_SecondaryAdvancedConfigurationEnable Ifx1edi2010as_getSecondaryAdvancedConfigurationEnable(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_SecondaryAdvancedConfigurationEnable value;
    Ifx_1EDI2010AS_SCFG reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_SecondaryAdvancedConfigurationEnable)reg.B.CFG2;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean Ifx1edi2010as_isSecondaryAdvancedConfigurationEnabled(Ifx1edi2010as_Driver* driver, boolean* status)
{
    boolean value;
    Ifx_1EDI2010AS_SCFG reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.CFG2 == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void Ifx1edi2010as_setSecondaryAdvancedConfigurationEnable(Ifx1edi2010as_Driver* driver, Ifx1edi2010as_SecondaryAdvancedConfigurationEnable value, boolean* status)
{
    Ifx_1EDI2010AS_SCFG reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.CFG2 = value;
        *status &= Ifx1edi2010as_Driver_writeRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void Ifx1edi2010as_enableSecondaryAdvancedConfiguration(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx_1EDI2010AS_SCFG reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.CFG2 = 1;
        *status &= Ifx1edi2010as_Driver_writeRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void Ifx1edi2010as_disableSecondaryAdvancedConfiguration(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx_1EDI2010AS_SCFG reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.CFG2 = 0;
        *status &= Ifx1edi2010as_Driver_writeRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

Ifx1edi2010as_Dio2PinMode Ifx1edi2010as_getDio2PinMode(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_Dio2PinMode value;
    Ifx_1EDI2010AS_SCFG reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_Dio2PinMode)reg.B.DIO2C;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void Ifx1edi2010as_setDio2PinMode(Ifx1edi2010as_Driver* driver, Ifx1edi2010as_Dio2PinMode value, boolean* status)
{
    Ifx_1EDI2010AS_SCFG reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.DIO2C = value;
        *status &= Ifx1edi2010as_Driver_writeRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

Ifx1edi2010as_DesatClampingEnable Ifx1edi2010as_getDesatClampingEnable(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_DesatClampingEnable value;
    Ifx_1EDI2010AS_SCFG reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_DesatClampingEnable)reg.B.DSTCEN;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean Ifx1edi2010as_isDesatClampingEnabled(Ifx1edi2010as_Driver* driver, boolean* status)
{
    boolean value;
    Ifx_1EDI2010AS_SCFG reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.DSTCEN == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void Ifx1edi2010as_setDesatClampingEnable(Ifx1edi2010as_Driver* driver, Ifx1edi2010as_DesatClampingEnable value, boolean* status)
{
    Ifx_1EDI2010AS_SCFG reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.DSTCEN = value;
        *status &= Ifx1edi2010as_Driver_writeRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void Ifx1edi2010as_enableDesatClamping(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx_1EDI2010AS_SCFG reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.DSTCEN = 1;
        *status &= Ifx1edi2010as_Driver_writeRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void Ifx1edi2010as_disableDesatClamping(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx_1EDI2010AS_SCFG reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.DSTCEN = 0;
        *status &= Ifx1edi2010as_Driver_writeRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

Ifx1edi2010as_PulseSuppressorEnable Ifx1edi2010as_getPulseSuppressorEnable(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_PulseSuppressorEnable value;
    Ifx_1EDI2010AS_SCFG reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_PulseSuppressorEnable)reg.B.PSEN;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean Ifx1edi2010as_isPulseSuppressorEnabled(Ifx1edi2010as_Driver* driver, boolean* status)
{
    boolean value;
    Ifx_1EDI2010AS_SCFG reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.PSEN == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void Ifx1edi2010as_setPulseSuppressorEnable(Ifx1edi2010as_Driver* driver, Ifx1edi2010as_PulseSuppressorEnable value, boolean* status)
{
    Ifx_1EDI2010AS_SCFG reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.PSEN = value;
        *status &= Ifx1edi2010as_Driver_writeRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void Ifx1edi2010as_enablePulseSuppressor(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx_1EDI2010AS_SCFG reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.PSEN = 1;
        *status &= Ifx1edi2010as_Driver_writeRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void Ifx1edi2010as_disablePulseSuppressor(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx_1EDI2010AS_SCFG reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.PSEN = 0;
        *status &= Ifx1edi2010as_Driver_writeRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

Ifx1edi2010as_VerificationModeTimeOutDuration Ifx1edi2010as_getVerificationModeTimeOutDuration(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_VerificationModeTimeOutDuration value;
    Ifx_1EDI2010AS_SCFG reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_VerificationModeTimeOutDuration)reg.B.TOSEN;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void Ifx1edi2010as_setVerificationModeTimeOutDuration(Ifx1edi2010as_Driver* driver, Ifx1edi2010as_VerificationModeTimeOutDuration value, boolean* status)
{
    Ifx_1EDI2010AS_SCFG reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.TOSEN = value;
        *status &= Ifx1edi2010as_Driver_writeRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

Ifx1edi2010as_DesatThresholdLevel Ifx1edi2010as_getDesatThresholdLevel(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_DesatThresholdLevel value;
    Ifx_1EDI2010AS_SCFG reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_DesatThresholdLevel)reg.B.DSATLS;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void Ifx1edi2010as_setDesatThresholdLevel(Ifx1edi2010as_Driver* driver, Ifx1edi2010as_DesatThresholdLevel value, boolean* status)
{
    Ifx_1EDI2010AS_SCFG reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.DSATLS = value;
        *status &= Ifx1edi2010as_Driver_writeRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

Ifx1edi2010as_Uvlo2ThresholdLevel Ifx1edi2010as_getUvlo2ThresholdLevel(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_Uvlo2ThresholdLevel value;
    Ifx_1EDI2010AS_SCFG reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_Uvlo2ThresholdLevel)reg.B.UVLO2S;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void Ifx1edi2010as_setUvlo2ThresholdLevel(Ifx1edi2010as_Driver* driver, Ifx1edi2010as_Uvlo2ThresholdLevel value, boolean* status)
{
    Ifx_1EDI2010AS_SCFG reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.UVLO2S = value;
        *status &= Ifx1edi2010as_Driver_writeRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

Ifx1edi2010as_OcpThresholdLevel Ifx1edi2010as_getOcpThresholdLevel(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_OcpThresholdLevel value;
    Ifx_1EDI2010AS_SCFG reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_OcpThresholdLevel)reg.B.OCPLS;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void Ifx1edi2010as_setOcpThresholdLevel(Ifx1edi2010as_Driver* driver, Ifx1edi2010as_OcpThresholdLevel value, boolean* status)
{
    Ifx_1EDI2010AS_SCFG reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.OCPLS = value;
        *status &= Ifx1edi2010as_Driver_writeRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

Ifx1edi2010as_DaclpPinClampingOutpout Ifx1edi2010as_getDaclpPinClampingOutpout(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_DaclpPinClampingOutpout value;
    Ifx_1EDI2010AS_SCFG reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_DaclpPinClampingOutpout)reg.B.DACLC;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void Ifx1edi2010as_setDaclpPinClampingOutpout(Ifx1edi2010as_Driver* driver, Ifx1edi2010as_DaclpPinClampingOutpout value, boolean* status)
{
    Ifx_1EDI2010AS_SCFG reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.DACLC = value;
        *status &= Ifx1edi2010as_Driver_writeRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

Ifx1edi2010as_AdcPwmTriggerDelay Ifx1edi2010as_getAdcPwmTriggerDelay(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_AdcPwmTriggerDelay value;
    Ifx_1EDI2010AS_SCFG2 reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG2) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_AdcPwmTriggerDelay)reg.B.PWMD;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void Ifx1edi2010as_setAdcPwmTriggerDelay(Ifx1edi2010as_Driver* driver, Ifx1edi2010as_AdcPwmTriggerDelay value, boolean* status)
{
    Ifx_1EDI2010AS_SCFG2 reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG2) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.PWMD = value;
        *status &= Ifx1edi2010as_Driver_writeRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG2) >> IFX1EDI2010AS_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

Ifx1edi2010as_AdcSecondaryTriggerMode Ifx1edi2010as_getAdcSecondaryTriggerMode(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_AdcSecondaryTriggerMode value;
    Ifx_1EDI2010AS_SCFG2 reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG2) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_AdcSecondaryTriggerMode)reg.B.ATS;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void Ifx1edi2010as_setAdcSecondaryTriggerMode(Ifx1edi2010as_Driver* driver, Ifx1edi2010as_AdcSecondaryTriggerMode value, boolean* status)
{
    Ifx_1EDI2010AS_SCFG2 reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG2) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.ATS = value;
        *status &= Ifx1edi2010as_Driver_writeRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG2) >> IFX1EDI2010AS_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

Ifx1edi2010as_AdcGain Ifx1edi2010as_getAdcGain(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_AdcGain value;
    Ifx_1EDI2010AS_SCFG2 reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG2) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_AdcGain)reg.B.AGS;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void Ifx1edi2010as_setAdcGain(Ifx1edi2010as_Driver* driver, Ifx1edi2010as_AdcGain value, boolean* status)
{
    Ifx_1EDI2010AS_SCFG2 reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG2) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.AGS = value;
        *status &= Ifx1edi2010as_Driver_writeRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG2) >> IFX1EDI2010AS_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

Ifx1edi2010as_AdcOffset Ifx1edi2010as_getAdcOffset(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_AdcOffset value;
    Ifx_1EDI2010AS_SCFG2 reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG2) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_AdcOffset)reg.B.AOS;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void Ifx1edi2010as_setAdcOffset(Ifx1edi2010as_Driver* driver, Ifx1edi2010as_AdcOffset value, boolean* status)
{
    Ifx_1EDI2010AS_SCFG2 reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG2) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.AOS = value;
        *status &= Ifx1edi2010as_Driver_writeRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG2) >> IFX1EDI2010AS_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

Ifx1edi2010as_AdcCurrentSource Ifx1edi2010as_getAdcCurrentSource(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_AdcCurrentSource value;
    Ifx_1EDI2010AS_SCFG2 reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG2) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_AdcCurrentSource)reg.B.ACSS;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void Ifx1edi2010as_setAdcCurrentSource(Ifx1edi2010as_Driver* driver, Ifx1edi2010as_AdcCurrentSource value, boolean* status)
{
    Ifx_1EDI2010AS_SCFG2 reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG2) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.ACSS = value;
        *status &= Ifx1edi2010as_Driver_writeRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG2) >> IFX1EDI2010AS_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

Ifx1edi2010as_AdcEventClassAEnable Ifx1edi2010as_getAdcEventClassAEnable(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_AdcEventClassAEnable value;
    Ifx_1EDI2010AS_SCFG2 reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG2) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_AdcEventClassAEnable)reg.B.ACAEN;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean Ifx1edi2010as_isAdcEventClassAEnabled(Ifx1edi2010as_Driver* driver, boolean* status)
{
    boolean value;
    Ifx_1EDI2010AS_SCFG2 reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG2) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.ACAEN == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void Ifx1edi2010as_setAdcEventClassAEnable(Ifx1edi2010as_Driver* driver, Ifx1edi2010as_AdcEventClassAEnable value, boolean* status)
{
    Ifx_1EDI2010AS_SCFG2 reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG2) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.ACAEN = value;
        *status &= Ifx1edi2010as_Driver_writeRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG2) >> IFX1EDI2010AS_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void Ifx1edi2010as_enableAdcEventClassA(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx_1EDI2010AS_SCFG2 reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG2) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.ACAEN = 1;
        *status &= Ifx1edi2010as_Driver_writeRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG2) >> IFX1EDI2010AS_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void Ifx1edi2010as_disableAdcEventClassA(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx_1EDI2010AS_SCFG2 reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG2) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.ACAEN = 0;
        *status &= Ifx1edi2010as_Driver_writeRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG2) >> IFX1EDI2010AS_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

Ifx1edi2010as_AdcEnable Ifx1edi2010as_getAdcEnable(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_AdcEnable value;
    Ifx_1EDI2010AS_SCFG2 reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG2) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_AdcEnable)reg.B.ADCEN;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean Ifx1edi2010as_isAdcEnabled(Ifx1edi2010as_Driver* driver, boolean* status)
{
    boolean value;
    Ifx_1EDI2010AS_SCFG2 reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG2) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.ADCEN == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void Ifx1edi2010as_setAdcEnable(Ifx1edi2010as_Driver* driver, Ifx1edi2010as_AdcEnable value, boolean* status)
{
    Ifx_1EDI2010AS_SCFG2 reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG2) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.ADCEN = value;
        *status &= Ifx1edi2010as_Driver_writeRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG2) >> IFX1EDI2010AS_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void Ifx1edi2010as_enableAdc(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx_1EDI2010AS_SCFG2 reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG2) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.ADCEN = 1;
        *status &= Ifx1edi2010as_Driver_writeRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG2) >> IFX1EDI2010AS_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void Ifx1edi2010as_disableAdc(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx_1EDI2010AS_SCFG2 reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG2) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.ADCEN = 0;
        *status &= Ifx1edi2010as_Driver_writeRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCFG2) >> IFX1EDI2010AS_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

Ifx1edi2010as_SecondaryVerificationFunction Ifx1edi2010as_getSecondaryVerificationFunction(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_SecondaryVerificationFunction value;
    Ifx_1EDI2010AS_SSCR reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SSCR) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_SecondaryVerificationFunction)reg.B.VFS2;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void Ifx1edi2010as_setSecondaryVerificationFunction(Ifx1edi2010as_Driver* driver, Ifx1edi2010as_SecondaryVerificationFunction value, boolean* status)
{
    Ifx_1EDI2010AS_SSCR reg;
    reg.U = 0;
    reg.B.VFS2 = value;
    *status &= Ifx1edi2010as_Driver_writeRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SSCR) >> IFX1EDI2010AS_ADDRESS_SHIFT), reg.U);
}

uint8 Ifx1edi2010as_getDesatBlankingTime(Ifx1edi2010as_Driver* driver, boolean* status)
{
    uint8 value;
    Ifx_1EDI2010AS_SDESAT reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SDESAT) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (uint8)reg.B.DSATBT;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void Ifx1edi2010as_setDesatBlankingTime(Ifx1edi2010as_Driver* driver, uint8 value, boolean* status)
{
    Ifx_1EDI2010AS_SDESAT reg;
    reg.U = 0;
    reg.B.DSATBT = value;
    *status &= Ifx1edi2010as_Driver_writeRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SDESAT) >> IFX1EDI2010AS_ADDRESS_SHIFT), reg.U);
}

uint8 Ifx1edi2010as_getOcpBlankingTime(Ifx1edi2010as_Driver* driver, boolean* status)
{
    uint8 value;
    Ifx_1EDI2010AS_SOCP reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SOCP) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (uint8)reg.B.OCPBT;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void Ifx1edi2010as_setOcpBlankingTime(Ifx1edi2010as_Driver* driver, uint8 value, boolean* status)
{
    Ifx_1EDI2010AS_SOCP reg;
    reg.U = 0;
    reg.B.OCPBT = value;
    *status &= Ifx1edi2010as_Driver_writeRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SOCP) >> IFX1EDI2010AS_ADDRESS_SHIFT), reg.U);
}

uint8 Ifx1edi2010as_getGateRegularTtoffDelay(Ifx1edi2010as_Driver* driver, boolean* status)
{
    uint8 value;
    Ifx_1EDI2010AS_SRTTOF reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SRTTOF) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (uint8)reg.B.RTVAL;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void Ifx1edi2010as_setGateRegularTtoffDelay(Ifx1edi2010as_Driver* driver, uint8 value, boolean* status)
{
    Ifx_1EDI2010AS_SRTTOF reg;
    reg.U = 0;
    reg.B.RTVAL = value;
    *status &= Ifx1edi2010as_Driver_writeRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SRTTOF) >> IFX1EDI2010AS_ADDRESS_SHIFT), reg.U);
}

uint8 Ifx1edi2010as_getGateSafeTtoffPlateauVoltage(Ifx1edi2010as_Driver* driver, boolean* status)
{
    uint8 value;
    Ifx_1EDI2010AS_SSTTOF reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SSTTOF) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (uint8)reg.B.GPS;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void Ifx1edi2010as_setGateSafeTtoffPlateauVoltage(Ifx1edi2010as_Driver* driver, uint8 value, boolean* status)
{
    Ifx_1EDI2010AS_SSTTOF reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SSTTOF) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.GPS = value;
        *status &= Ifx1edi2010as_Driver_writeRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SSTTOF) >> IFX1EDI2010AS_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

uint8 Ifx1edi2010as_getGateSafeTtoffDelay(Ifx1edi2010as_Driver* driver, boolean* status)
{
    uint8 value;
    Ifx_1EDI2010AS_SSTTOF reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SSTTOF) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (uint8)reg.B.STVAL;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void Ifx1edi2010as_setGateSafeTtoffDelay(Ifx1edi2010as_Driver* driver, uint8 value, boolean* status)
{
    Ifx_1EDI2010AS_SSTTOF reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SSTTOF) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.STVAL = value;
        *status &= Ifx1edi2010as_Driver_writeRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SSTTOF) >> IFX1EDI2010AS_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

uint8 Ifx1edi2010as_getGateTtonDelay(Ifx1edi2010as_Driver* driver, boolean* status)
{
    uint8 value;
    Ifx_1EDI2010AS_STTON reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.STTON) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (uint8)reg.B.TTONVAL;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void Ifx1edi2010as_setGateTtonDelay(Ifx1edi2010as_Driver* driver, uint8 value, boolean* status)
{
    Ifx_1EDI2010AS_STTON reg;
    reg.U = 0;
    reg.B.TTONVAL = value;
    *status &= Ifx1edi2010as_Driver_writeRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.STTON) >> IFX1EDI2010AS_ADDRESS_SHIFT), reg.U);
}

Ifx1edi2010as_AdcResultValidFlag Ifx1edi2010as_getAdcResultValidFlag(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_AdcResultValidFlag value;
    Ifx_1EDI2010AS_SADC reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SADC) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_AdcResultValidFlag)reg.B.AVFS;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

Ifx1edi2010as_AdcUndervoltageErrorStatus Ifx1edi2010as_getAdcUndervoltageErrorStatus(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_AdcUndervoltageErrorStatus value;
    Ifx_1EDI2010AS_SADC reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SADC) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_AdcUndervoltageErrorStatus)reg.B.AUVS;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean Ifx1edi2010as_isAdcUndervoltageError(Ifx1edi2010as_Driver* driver, boolean* status)
{
    boolean value;
    Ifx_1EDI2010AS_SADC reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SADC) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.AUVS == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

Ifx1edi2010as_AdcOvervoltageErrorStatus Ifx1edi2010as_getAdcOvervoltageErrorStatus(Ifx1edi2010as_Driver* driver, boolean* status)
{
    Ifx1edi2010as_AdcOvervoltageErrorStatus value;
    Ifx_1EDI2010AS_SADC reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SADC) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (Ifx1edi2010as_AdcOvervoltageErrorStatus)reg.B.AOVS;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean Ifx1edi2010as_isAdcOvervoltageError(Ifx1edi2010as_Driver* driver, boolean* status)
{
    boolean value;
    Ifx_1EDI2010AS_SADC reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SADC) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.AOVS == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

uint8 Ifx1edi2010as_getAdcResult(Ifx1edi2010as_Driver* driver, boolean* status)
{
    uint8 value;
    Ifx_1EDI2010AS_SADC reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SADC) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (uint8)reg.B.ADCVAL;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

uint8 Ifx1edi2010as_getAdcLimitCheckingBoundaryA(Ifx1edi2010as_Driver* driver, boolean* status)
{
    uint8 value;
    Ifx_1EDI2010AS_SBC reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SBC) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (uint8)reg.B.LCB1A;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void Ifx1edi2010as_setAdcLimitCheckingBoundaryA(Ifx1edi2010as_Driver* driver, uint8 value, boolean* status)
{
    Ifx_1EDI2010AS_SBC reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SBC) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.LCB1A = value;
        *status &= Ifx1edi2010as_Driver_writeRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SBC) >> IFX1EDI2010AS_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

uint8 Ifx1edi2010as_getAdcLimitCheckingBoundaryB(Ifx1edi2010as_Driver* driver, boolean* status)
{
    uint8 value;
    Ifx_1EDI2010AS_SBC reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SBC) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (uint8)reg.B.LCB1B;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void Ifx1edi2010as_setAdcLimitCheckingBoundaryB(Ifx1edi2010as_Driver* driver, uint8 value, boolean* status)
{
    Ifx_1EDI2010AS_SBC reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SBC) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.LCB1B = value;
        *status &= Ifx1edi2010as_Driver_writeRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SBC) >> IFX1EDI2010AS_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

uint8 Ifx1edi2010as_getSecondarySupervisionOscillatorClockCycles(Ifx1edi2010as_Driver* driver, boolean* status)
{
    uint8 value;
    Ifx_1EDI2010AS_SCS reg;
    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)(((uint32)&MODULE_1EDI2010AS.SCS) >> IFX1EDI2010AS_ADDRESS_SHIFT), &reg.U))
    {
        value = (uint8)reg.B.SCSS;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean Ifx1edi2010as_registerDump(Ifx1edi2010as_Address address, Ifx1edi2010as_UData value, char* text, size_t maxLength)
{
    int result;
    uint32 a = address << IFX1EDI2010AS_ADDRESS_SHIFT;
    switch(a)
    {
        case (const uint32)&MODULE_1EDI2010AS.PID:
            result = snprintf(text, maxLength, "PID @ 0x%X = 0x%X = [P:0x%X, LMI:0x%X, PVERS:0x%X]",
                address,
                value,
                ((value >> 0) & 0x1), ((value >> 1) & 0x1), ((value >> 4) & 0xfff)
                );
            break;
        case (const uint32)&MODULE_1EDI2010AS.PSTAT:
            result = snprintf(text, maxLength, "PSTAT @ 0x%X = 0x%X = [P:0x%X, LMI:0x%X, GPOFP:0x%X, AVFP:0x%X, SRDY:0x%X, ACT:0x%X, GPONP:0x%X, ERR:0x%X]",
                address,
                value,
                ((value >> 0) & 0x1), ((value >> 1) & 0x1), ((value >> 2) & 0x7), ((value >> 5) & 0x1), ((value >> 6) & 0x1), ((value >> 7) & 0x1), ((value >> 8) & 0x7), ((value >> 11) & 0x1)
                );
            break;
        case (const uint32)&MODULE_1EDI2010AS.PSTAT2:
            result = snprintf(text, maxLength, "PSTAT2 @ 0x%X = 0x%X = [P:0x%X, LMI:0x%X, ENVAL:0x%X, FLTA:0x%X, FLTB:0x%X, OPMP:0x%X, FLTAP:0x%X, FLTBP:0x%X, STP:0x%X, AXVP:0x%X]",
                address,
                value,
                ((value >> 0) & 0x1), ((value >> 1) & 0x1), ((value >> 2) & 0x1), ((value >> 3) & 0x1), ((value >> 4) & 0x1), ((value >> 5) & 0x7), ((value >> 8) & 0x1), ((value >> 9) & 0x1), ((value >> 10) & 0x1), ((value >> 11) & 0x1)
                );
            break;
        case (const uint32)&MODULE_1EDI2010AS.PER:
            result = snprintf(text, maxLength, "PER @ 0x%X = 0x%X = [P:0x%X, LMI:0x%X, CERP:0x%X, ADER:0x%X, SPIER:0x%X, STPER:0x%X, ENER:0x%X, RSTP:0x%X, RSTEP:0x%X]",
                address,
                value,
                ((value >> 0) & 0x1), ((value >> 1) & 0x1), ((value >> 2) & 0x1), ((value >> 6) & 0x1), ((value >> 8) & 0x1), ((value >> 9) & 0x1), ((value >> 10) & 0x1), ((value >> 11) & 0x1), ((value >> 12) & 0x1)
                );
            break;
        case (const uint32)&MODULE_1EDI2010AS.PCFG:
            result = snprintf(text, maxLength, "PCFG @ 0x%X = 0x%X = [P:0x%X, LMI:0x%X, PAREN:0x%X, ADAEN:0x%X, ADTEN:0x%X]",
                address,
                value,
                ((value >> 0) & 0x1), ((value >> 1) & 0x1), ((value >> 2) & 0x1), ((value >> 5) & 0x1), ((value >> 6) & 0x1)
                );
            break;
        case (const uint32)&MODULE_1EDI2010AS.PCFG2:
            result = snprintf(text, maxLength, "PCFG2 @ 0x%X = 0x%X = [P:0x%X, LMI:0x%X, STPDEL:0x%X, DIO1:0x%X]",
                address,
                value,
                ((value >> 0) & 0x1), ((value >> 1) & 0x1), ((value >> 2) & 0x3f), ((value >> 8) & 0x1)
                );
            break;
        case (const uint32)&MODULE_1EDI2010AS.PCTRL:
            result = snprintf(text, maxLength, "PCTRL @ 0x%X = 0x%X = [P:0x%X, LMI:0x%X, GPON:0x%X, CLRP:0x%X, CLRS:0x%X]",
                address,
                value,
                ((value >> 0) & 0x1), ((value >> 1) & 0x1), ((value >> 2) & 0x7), ((value >> 5) & 0x1), ((value >> 6) & 0x1)
                );
            break;
        case (const uint32)&MODULE_1EDI2010AS.PCTRL2:
            result = snprintf(text, maxLength, "PCTRL2 @ 0x%X = 0x%X = [P:0x%X, LMI:0x%X, GPOF:0x%X, ACRP:0x%X]",
                address,
                value,
                ((value >> 0) & 0x1), ((value >> 1) & 0x1), ((value >> 2) & 0x7), ((value >> 5) & 0x1)
                );
            break;
        case (const uint32)&MODULE_1EDI2010AS.PSCR:
            result = snprintf(text, maxLength, "PSCR @ 0x%X = 0x%X = [P:0x%X, LMI:0x%X, VFSP:0x%X]",
                address,
                value,
                ((value >> 0) & 0x1), ((value >> 1) & 0x1), ((value >> 2) & 0x3)
                );
            break;
        case (const uint32)&MODULE_1EDI2010AS.PRW:
            result = snprintf(text, maxLength, "PRW @ 0x%X = 0x%X = [P:0x%X, LMI:0x%X, RWVAL:0x%X]",
                address,
                value,
                ((value >> 0) & 0x1), ((value >> 1) & 0x1), ((value >> 2) & 0x3fff)
                );
            break;
        case (const uint32)&MODULE_1EDI2010AS.PPIN:
            result = snprintf(text, maxLength, "PPIN @ 0x%X = 0x%X = [P:0x%X, LMI:0x%X, INPL:0x%X, INSTPL:0x%X, ENL:0x%X, NFLTAL:0x%X, NFLTBL:0x%X, ADCTL:0x%X, DIO1L:0x%X]",
                address,
                value,
                ((value >> 0) & 0x1), ((value >> 1) & 0x1), ((value >> 2) & 0x1), ((value >> 3) & 0x1), ((value >> 4) & 0x1), ((value >> 5) & 0x1), ((value >> 6) & 0x1), ((value >> 7) & 0x1), ((value >> 8) & 0x1)
                );
            break;
        case (const uint32)&MODULE_1EDI2010AS.PCS:
            result = snprintf(text, maxLength, "PCS @ 0x%X = 0x%X = [P:0x%X, LMI:0x%X, CSP:0x%X]",
                address,
                value,
                ((value >> 0) & 0x1), ((value >> 1) & 0x1), ((value >> 8) & 0xff)
                );
            break;
        case (const uint32)&MODULE_1EDI2010AS.SID:
            result = snprintf(text, maxLength, "SID @ 0x%X = 0x%X = [P:0x%X, LMI:0x%X, SVERS:0x%X]",
                address,
                value,
                ((value >> 0) & 0x1), ((value >> 1) & 0x1), ((value >> 4) & 0xfff)
                );
            break;
        case (const uint32)&MODULE_1EDI2010AS.SSTAT:
            result = snprintf(text, maxLength, "SSTAT @ 0x%X = 0x%X = [P:0x%X, LMI:0x%X, PWM:0x%X, DBG:0x%X]",
                address,
                value,
                ((value >> 0) & 0x1), ((value >> 1) & 0x1), ((value >> 4) & 0x1), ((value >> 10) & 0x1)
                );
            break;
        case (const uint32)&MODULE_1EDI2010AS.SSTAT2:
            result = snprintf(text, maxLength, "SSTAT2 @ 0x%X = 0x%X = [P:0x%X, LMI:0x%X, DSATC:0x%X, OCPC:0x%X, UVLO2M:0x%X, DIO2L:0x%X, DACL:0x%X]",
                address,
                value,
                ((value >> 0) & 0x1), ((value >> 1) & 0x1), ((value >> 4) & 0x1), ((value >> 5) & 0x1), ((value >> 6) & 0x1), ((value >> 7) & 0x1), ((value >> 8) & 0x1)
                );
            break;
        case (const uint32)&MODULE_1EDI2010AS.SER:
            result = snprintf(text, maxLength, "SER @ 0x%X = 0x%X = [P:0x%X, LMI:0x%X, CERS:0x%X, AUVER:0x%X, AOVER:0x%X, VMTO:0x%X, UVLO2ER:0x%X, DESATER:0x%X, OCPER:0x%X, RSTS:0x%X]",
                address,
                value,
                ((value >> 0) & 0x1), ((value >> 1) & 0x1), ((value >> 4) & 0x1), ((value >> 5) & 0x1), ((value >> 6) & 0x1), ((value >> 9) & 0x1), ((value >> 12) & 0x1), ((value >> 13) & 0x1), ((value >> 14) & 0x1), ((value >> 15) & 0x1)
                );
            break;
        case (const uint32)&MODULE_1EDI2010AS.SCFG:
            result = snprintf(text, maxLength, "SCFG @ 0x%X = 0x%X = [P:0x%X, LMI:0x%X, VBEC:0x%X, CFG2:0x%X, DIO2C:0x%X, DSTCEN:0x%X, PSEN:0x%X, TOSEN:0x%X, DSATLS:0x%X, UVLO2S:0x%X, OCPLS:0x%X, DACLC:0x%X]",
                address,
                value,
                ((value >> 0) & 0x1), ((value >> 1) & 0x1), ((value >> 4) & 0x1), ((value >> 5) & 0x1), ((value >> 6) & 0x3), ((value >> 8) & 0x1), ((value >> 9) & 0x1), ((value >> 10) & 0x1), ((value >> 11) & 0x1), ((value >> 12) & 0x1), ((value >> 13) & 0x1), ((value >> 14) & 0x3)
                );
            break;
        case (const uint32)&MODULE_1EDI2010AS.SCFG2:
            result = snprintf(text, maxLength, "SCFG2 @ 0x%X = 0x%X = [P:0x%X, LMI:0x%X, PWMD:0x%X, ATS:0x%X, AGS:0x%X, AOS:0x%X, ACSS:0x%X, ACAEN:0x%X, ADCEN:0x%X]",
                address,
                value,
                ((value >> 0) & 0x1), ((value >> 1) & 0x1), ((value >> 4) & 0x3), ((value >> 6) & 0x3), ((value >> 8) & 0x3), ((value >> 10) & 0x7), ((value >> 13) & 0x1), ((value >> 14) & 0x1), ((value >> 15) & 0x1)
                );
            break;
        case (const uint32)&MODULE_1EDI2010AS.SSCR:
            result = snprintf(text, maxLength, "SSCR @ 0x%X = 0x%X = [P:0x%X, LMI:0x%X, VFS2:0x%X]",
                address,
                value,
                ((value >> 0) & 0x1), ((value >> 1) & 0x1), ((value >> 4) & 0x3)
                );
            break;
        case (const uint32)&MODULE_1EDI2010AS.SDESAT:
            result = snprintf(text, maxLength, "SDESAT @ 0x%X = 0x%X = [P:0x%X, LMI:0x%X, DSATBT:0x%X]",
                address,
                value,
                ((value >> 0) & 0x1), ((value >> 1) & 0x1), ((value >> 8) & 0xff)
                );
            break;
        case (const uint32)&MODULE_1EDI2010AS.SOCP:
            result = snprintf(text, maxLength, "SOCP @ 0x%X = 0x%X = [P:0x%X, LMI:0x%X, OCPBT:0x%X]",
                address,
                value,
                ((value >> 0) & 0x1), ((value >> 1) & 0x1), ((value >> 8) & 0xff)
                );
            break;
        case (const uint32)&MODULE_1EDI2010AS.SRTTOF:
            result = snprintf(text, maxLength, "SRTTOF @ 0x%X = 0x%X = [P:0x%X, LMI:0x%X, RTVAL:0x%X]",
                address,
                value,
                ((value >> 0) & 0x1), ((value >> 1) & 0x1), ((value >> 8) & 0xff)
                );
            break;
        case (const uint32)&MODULE_1EDI2010AS.SSTTOF:
            result = snprintf(text, maxLength, "SSTTOF @ 0x%X = 0x%X = [P:0x%X, LMI:0x%X, GPS:0x%X, STVAL:0x%X]",
                address,
                value,
                ((value >> 0) & 0x1), ((value >> 1) & 0x1), ((value >> 5) & 0x7), ((value >> 8) & 0xff)
                );
            break;
        case (const uint32)&MODULE_1EDI2010AS.STTON:
            result = snprintf(text, maxLength, "STTON @ 0x%X = 0x%X = [P:0x%X, LMI:0x%X, TTONVAL:0x%X]",
                address,
                value,
                ((value >> 0) & 0x1), ((value >> 1) & 0x1), ((value >> 8) & 0xff)
                );
            break;
        case (const uint32)&MODULE_1EDI2010AS.SADC:
            result = snprintf(text, maxLength, "SADC @ 0x%X = 0x%X = [P:0x%X, LMI:0x%X, AVFS:0x%X, AUVS:0x%X, AOVS:0x%X, ADCVAL:0x%X]",
                address,
                value,
                ((value >> 0) & 0x1), ((value >> 1) & 0x1), ((value >> 4) & 0x1), ((value >> 5) & 0x1), ((value >> 6) & 0x1), ((value >> 8) & 0xff)
                );
            break;
        case (const uint32)&MODULE_1EDI2010AS.SBC:
            result = snprintf(text, maxLength, "SBC @ 0x%X = 0x%X = [P:0x%X, LMI:0x%X, LCB1A:0x%X, LCB1B:0x%X]",
                address,
                value,
                ((value >> 0) & 0x1), ((value >> 1) & 0x1), ((value >> 4) & 0x3f), ((value >> 10) & 0x3f)
                );
            break;
        case (const uint32)&MODULE_1EDI2010AS.SCS:
            result = snprintf(text, maxLength, "SCS @ 0x%X = 0x%X = [P:0x%X, LMI:0x%X, SCSS:0x%X]",
                address,
                value,
                ((value >> 0) & 0x1), ((value >> 1) & 0x1), ((value >> 8) & 0xff)
                );
            break;
        default:
            result = snprintf(text, maxLength, "ERROR: invalid address 0x%X", address);
            break;
    }
    return (result < maxLength) && (result >= 0);
}
