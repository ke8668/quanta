#include "SysSe/Ext/1edi2010as/_Reg/Ifx1edi2010as_bf.h"
#include "SysSe/Ext/1edi2010as/_Impl/Ifx1edi2010as_data.h"
#include <string.h>

/* *INDENT-OFF* */
const Ifx_DeviceData_Bitfield Ifx1edi2010as_dataBitfield_PID[] =
{
    { .name = "PVERS", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R, .offset = IFX_1EDI2010AS_PID_PVERS_OFF, .mask = IFX_1EDI2010AS_PID_PVERS_MSK, .index = 0 },
    IFX_DEVICEDATA_BITFIELD_END
};
const Ifx_DeviceData_Bitfield Ifx1edi2010as_dataBitfield_PSTAT[] =
{
    { .name = "GPOFP", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_H, .offset = IFX_1EDI2010AS_PSTAT_GPOFP_OFF, .mask = IFX_1EDI2010AS_PSTAT_GPOFP_MSK, .index = 1 },
    { .name = "SRDY", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_H, .offset = IFX_1EDI2010AS_PSTAT_SRDY_OFF, .mask = IFX_1EDI2010AS_PSTAT_SRDY_MSK, .index = 2 },
    { .name = "ACT", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_H, .offset = IFX_1EDI2010AS_PSTAT_ACT_OFF, .mask = IFX_1EDI2010AS_PSTAT_ACT_MSK, .index = 3 },
    { .name = "GPONP", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_H, .offset = IFX_1EDI2010AS_PSTAT_GPONP_OFF, .mask = IFX_1EDI2010AS_PSTAT_GPONP_MSK, .index = 4 },
    { .name = "ERR", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_H, .offset = IFX_1EDI2010AS_PSTAT_ERR_OFF, .mask = IFX_1EDI2010AS_PSTAT_ERR_MSK, .index = 5 },
    IFX_DEVICEDATA_BITFIELD_END
};
const Ifx_DeviceData_Bitfield Ifx1edi2010as_dataBitfield_PSTAT2[] =
{
    { .name = "ENVAL", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_H, .offset = IFX_1EDI2010AS_PSTAT2_ENVAL_OFF, .mask = IFX_1EDI2010AS_PSTAT2_ENVAL_MSK, .index = 6 },
    { .name = "FLTA", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_H, .offset = IFX_1EDI2010AS_PSTAT2_FLTA_OFF, .mask = IFX_1EDI2010AS_PSTAT2_FLTA_MSK, .index = 7 },
    { .name = "FLTB", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_H, .offset = IFX_1EDI2010AS_PSTAT2_FLTB_OFF, .mask = IFX_1EDI2010AS_PSTAT2_FLTB_MSK, .index = 8 },
    { .name = "OPMP", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_H, .offset = IFX_1EDI2010AS_PSTAT2_OPMP_OFF, .mask = IFX_1EDI2010AS_PSTAT2_OPMP_MSK, .index = 9 },
    { .name = "FLTAP", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_H, .offset = IFX_1EDI2010AS_PSTAT2_FLTAP_OFF, .mask = IFX_1EDI2010AS_PSTAT2_FLTAP_MSK, .index = 10 },
    { .name = "FLTBP", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_H, .offset = IFX_1EDI2010AS_PSTAT2_FLTBP_OFF, .mask = IFX_1EDI2010AS_PSTAT2_FLTBP_MSK, .index = 11 },
    { .name = "STP", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_H, .offset = IFX_1EDI2010AS_PSTAT2_STP_OFF, .mask = IFX_1EDI2010AS_PSTAT2_STP_MSK, .index = 12 },
    { .name = "AXVP", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_H, .offset = IFX_1EDI2010AS_PSTAT2_AXVP_OFF, .mask = IFX_1EDI2010AS_PSTAT2_AXVP_MSK, .index = 13 },
    IFX_DEVICEDATA_BITFIELD_END
};
const Ifx_DeviceData_Bitfield Ifx1edi2010as_dataBitfield_PER[] =
{
    { .name = "CERP", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_H, .offset = IFX_1EDI2010AS_PER_CERP_OFF, .mask = IFX_1EDI2010AS_PER_CERP_MSK, .index = 14 },
    { .name = "ADER", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_H, .offset = IFX_1EDI2010AS_PER_ADER_OFF, .mask = IFX_1EDI2010AS_PER_ADER_MSK, .index = 15 },
    { .name = "SPIER", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_H, .offset = IFX_1EDI2010AS_PER_SPIER_OFF, .mask = IFX_1EDI2010AS_PER_SPIER_MSK, .index = 16 },
    { .name = "STPER", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_H, .offset = IFX_1EDI2010AS_PER_STPER_OFF, .mask = IFX_1EDI2010AS_PER_STPER_MSK, .index = 17 },
    { .name = "ENER", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_H, .offset = IFX_1EDI2010AS_PER_ENER_OFF, .mask = IFX_1EDI2010AS_PER_ENER_MSK, .index = 18 },
    { .name = "RSTP", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_H, .offset = IFX_1EDI2010AS_PER_RSTP_OFF, .mask = IFX_1EDI2010AS_PER_RSTP_MSK, .index = 19 },
    { .name = "RSTEP", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_H, .offset = IFX_1EDI2010AS_PER_RSTEP_OFF, .mask = IFX_1EDI2010AS_PER_RSTEP_MSK, .index = 20 },
    IFX_DEVICEDATA_BITFIELD_END
};
const Ifx_DeviceData_Bitfield Ifx1edi2010as_dataBitfield_PCFG[] =
{
    { .name = "PAREN", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_W, .offset = IFX_1EDI2010AS_PCFG_PAREN_OFF, .mask = IFX_1EDI2010AS_PCFG_PAREN_MSK, .index = 21 },
    { .name = "ADAEN", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_W, .offset = IFX_1EDI2010AS_PCFG_ADAEN_OFF, .mask = IFX_1EDI2010AS_PCFG_ADAEN_MSK, .index = 22 },
    { .name = "ADTEN", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_W, .offset = IFX_1EDI2010AS_PCFG_ADTEN_OFF, .mask = IFX_1EDI2010AS_PCFG_ADTEN_MSK, .index = 23 },
    IFX_DEVICEDATA_BITFIELD_END
};
const Ifx_DeviceData_Bitfield Ifx1edi2010as_dataBitfield_PCFG2[] =
{
    { .name = "STPDEL", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_W, .offset = IFX_1EDI2010AS_PCFG2_STPDEL_OFF, .mask = IFX_1EDI2010AS_PCFG2_STPDEL_MSK, .index = 24 },
    { .name = "DIO1", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_W, .offset = IFX_1EDI2010AS_PCFG2_DIO1_OFF, .mask = IFX_1EDI2010AS_PCFG2_DIO1_MSK, .index = 25 },
    IFX_DEVICEDATA_BITFIELD_END
};
const Ifx_DeviceData_Bitfield Ifx1edi2010as_dataBitfield_PCTRL[] =
{
    { .name = "GPON", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_W, .offset = IFX_1EDI2010AS_PCTRL_GPON_OFF, .mask = IFX_1EDI2010AS_PCTRL_GPON_MSK, .index = 26 },
    { .name = "CLRP", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_W|IFX_DEVICEDATA_ACCESS_FLAG_H, .offset = IFX_1EDI2010AS_PCTRL_CLRP_OFF, .mask = IFX_1EDI2010AS_PCTRL_CLRP_MSK, .index = 27 },
    { .name = "CLRS", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_W|IFX_DEVICEDATA_ACCESS_FLAG_H, .offset = IFX_1EDI2010AS_PCTRL_CLRS_OFF, .mask = IFX_1EDI2010AS_PCTRL_CLRS_MSK, .index = 28 },
    IFX_DEVICEDATA_BITFIELD_END
};
const Ifx_DeviceData_Bitfield Ifx1edi2010as_dataBitfield_PCTRL2[] =
{
    { .name = "GPOF", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_W, .offset = IFX_1EDI2010AS_PCTRL2_GPOF_OFF, .mask = IFX_1EDI2010AS_PCTRL2_GPOF_MSK, .index = 29 },
    { .name = "ACRP", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_W|IFX_DEVICEDATA_ACCESS_FLAG_H, .offset = IFX_1EDI2010AS_PCTRL2_ACRP_OFF, .mask = IFX_1EDI2010AS_PCTRL2_ACRP_MSK, .index = 30 },
    IFX_DEVICEDATA_BITFIELD_END
};
const Ifx_DeviceData_Bitfield Ifx1edi2010as_dataBitfield_PSCR[] =
{
    { .name = "VFSP", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_W|IFX_DEVICEDATA_ACCESS_FLAG_H, .offset = IFX_1EDI2010AS_PSCR_VFSP_OFF, .mask = IFX_1EDI2010AS_PSCR_VFSP_MSK, .index = 31 },
    IFX_DEVICEDATA_BITFIELD_END
};
const Ifx_DeviceData_Bitfield Ifx1edi2010as_dataBitfield_PRW[] =
{
    { .name = "RWVAL", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_W, .offset = IFX_1EDI2010AS_PRW_RWVAL_OFF, .mask = IFX_1EDI2010AS_PRW_RWVAL_MSK, .index = 32 },
    IFX_DEVICEDATA_BITFIELD_END
};
const Ifx_DeviceData_Bitfield Ifx1edi2010as_dataBitfield_PPIN[] =
{
    { .name = "INPL", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_H, .offset = IFX_1EDI2010AS_PPIN_INPL_OFF, .mask = IFX_1EDI2010AS_PPIN_INPL_MSK, .index = 33 },
    { .name = "INSTPL", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_H, .offset = IFX_1EDI2010AS_PPIN_INSTPL_OFF, .mask = IFX_1EDI2010AS_PPIN_INSTPL_MSK, .index = 34 },
    { .name = "ENL", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_H, .offset = IFX_1EDI2010AS_PPIN_ENL_OFF, .mask = IFX_1EDI2010AS_PPIN_ENL_MSK, .index = 35 },
    { .name = "NFLTAL", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_H, .offset = IFX_1EDI2010AS_PPIN_NFLTAL_OFF, .mask = IFX_1EDI2010AS_PPIN_NFLTAL_MSK, .index = 36 },
    { .name = "NFLTBL", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_H, .offset = IFX_1EDI2010AS_PPIN_NFLTBL_OFF, .mask = IFX_1EDI2010AS_PPIN_NFLTBL_MSK, .index = 37 },
    { .name = "ADCTL", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_H, .offset = IFX_1EDI2010AS_PPIN_ADCTL_OFF, .mask = IFX_1EDI2010AS_PPIN_ADCTL_MSK, .index = 38 },
    { .name = "DIO1L", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_H, .offset = IFX_1EDI2010AS_PPIN_DIO1L_OFF, .mask = IFX_1EDI2010AS_PPIN_DIO1L_MSK, .index = 39 },
    IFX_DEVICEDATA_BITFIELD_END
};
const Ifx_DeviceData_Bitfield Ifx1edi2010as_dataBitfield_PCS[] =
{
    { .name = "CSP", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_H, .offset = IFX_1EDI2010AS_PCS_CSP_OFF, .mask = IFX_1EDI2010AS_PCS_CSP_MSK, .index = 40 },
    IFX_DEVICEDATA_BITFIELD_END
};
const Ifx_DeviceData_Bitfield Ifx1edi2010as_dataBitfield_SID[] =
{
    { .name = "SVERS", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R, .offset = IFX_1EDI2010AS_SID_SVERS_OFF, .mask = IFX_1EDI2010AS_SID_SVERS_MSK, .index = 41 },
    IFX_DEVICEDATA_BITFIELD_END
};
const Ifx_DeviceData_Bitfield Ifx1edi2010as_dataBitfield_SSTAT[] =
{
    { .name = "PWM", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_H, .offset = IFX_1EDI2010AS_SSTAT_PWM_OFF, .mask = IFX_1EDI2010AS_SSTAT_PWM_MSK, .index = 42 },
    { .name = "DBG", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_H, .offset = IFX_1EDI2010AS_SSTAT_DBG_OFF, .mask = IFX_1EDI2010AS_SSTAT_DBG_MSK, .index = 43 },
    IFX_DEVICEDATA_BITFIELD_END
};
const Ifx_DeviceData_Bitfield Ifx1edi2010as_dataBitfield_SSTAT2[] =
{
    { .name = "DSATC", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_H, .offset = IFX_1EDI2010AS_SSTAT2_DSATC_OFF, .mask = IFX_1EDI2010AS_SSTAT2_DSATC_MSK, .index = 44 },
    { .name = "OCPC", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_H, .offset = IFX_1EDI2010AS_SSTAT2_OCPC_OFF, .mask = IFX_1EDI2010AS_SSTAT2_OCPC_MSK, .index = 45 },
    { .name = "UVLO2M", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_H, .offset = IFX_1EDI2010AS_SSTAT2_UVLO2M_OFF, .mask = IFX_1EDI2010AS_SSTAT2_UVLO2M_MSK, .index = 46 },
    { .name = "DIO2L", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_H, .offset = IFX_1EDI2010AS_SSTAT2_DIO2L_OFF, .mask = IFX_1EDI2010AS_SSTAT2_DIO2L_MSK, .index = 47 },
    { .name = "DACL", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_H, .offset = IFX_1EDI2010AS_SSTAT2_DACL_OFF, .mask = IFX_1EDI2010AS_SSTAT2_DACL_MSK, .index = 48 },
    IFX_DEVICEDATA_BITFIELD_END
};
const Ifx_DeviceData_Bitfield Ifx1edi2010as_dataBitfield_SER[] =
{
    { .name = "CERS", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_H, .offset = IFX_1EDI2010AS_SER_CERS_OFF, .mask = IFX_1EDI2010AS_SER_CERS_MSK, .index = 49 },
    { .name = "AUVER", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_H, .offset = IFX_1EDI2010AS_SER_AUVER_OFF, .mask = IFX_1EDI2010AS_SER_AUVER_MSK, .index = 50 },
    { .name = "AOVER", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_H, .offset = IFX_1EDI2010AS_SER_AOVER_OFF, .mask = IFX_1EDI2010AS_SER_AOVER_MSK, .index = 51 },
    { .name = "VMTO", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_H, .offset = IFX_1EDI2010AS_SER_VMTO_OFF, .mask = IFX_1EDI2010AS_SER_VMTO_MSK, .index = 52 },
    { .name = "UVLO2ER", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_H, .offset = IFX_1EDI2010AS_SER_UVLO2ER_OFF, .mask = IFX_1EDI2010AS_SER_UVLO2ER_MSK, .index = 53 },
    { .name = "DESATER", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_H, .offset = IFX_1EDI2010AS_SER_DESATER_OFF, .mask = IFX_1EDI2010AS_SER_DESATER_MSK, .index = 54 },
    { .name = "OCPER", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_H, .offset = IFX_1EDI2010AS_SER_OCPER_OFF, .mask = IFX_1EDI2010AS_SER_OCPER_MSK, .index = 55 },
    { .name = "RSTS", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_H, .offset = IFX_1EDI2010AS_SER_RSTS_OFF, .mask = IFX_1EDI2010AS_SER_RSTS_MSK, .index = 56 },
    IFX_DEVICEDATA_BITFIELD_END
};
const Ifx_DeviceData_Bitfield Ifx1edi2010as_dataBitfield_SCFG[] =
{
    { .name = "VBEC", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_W, .offset = IFX_1EDI2010AS_SCFG_VBEC_OFF, .mask = IFX_1EDI2010AS_SCFG_VBEC_MSK, .index = 57 },
    { .name = "CFG2", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_W|IFX_DEVICEDATA_ACCESS_FLAG_H, .offset = IFX_1EDI2010AS_SCFG_CFG2_OFF, .mask = IFX_1EDI2010AS_SCFG_CFG2_MSK, .index = 58 },
    { .name = "DIO2C", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_W, .offset = IFX_1EDI2010AS_SCFG_DIO2C_OFF, .mask = IFX_1EDI2010AS_SCFG_DIO2C_MSK, .index = 59 },
    { .name = "DSTCEN", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_W, .offset = IFX_1EDI2010AS_SCFG_DSTCEN_OFF, .mask = IFX_1EDI2010AS_SCFG_DSTCEN_MSK, .index = 60 },
    { .name = "PSEN", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_W, .offset = IFX_1EDI2010AS_SCFG_PSEN_OFF, .mask = IFX_1EDI2010AS_SCFG_PSEN_MSK, .index = 61 },
    { .name = "TOSEN", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_W, .offset = IFX_1EDI2010AS_SCFG_TOSEN_OFF, .mask = IFX_1EDI2010AS_SCFG_TOSEN_MSK, .index = 62 },
    { .name = "DSATLS", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_W, .offset = IFX_1EDI2010AS_SCFG_DSATLS_OFF, .mask = IFX_1EDI2010AS_SCFG_DSATLS_MSK, .index = 63 },
    { .name = "UVLO2S", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_W, .offset = IFX_1EDI2010AS_SCFG_UVLO2S_OFF, .mask = IFX_1EDI2010AS_SCFG_UVLO2S_MSK, .index = 64 },
    { .name = "OCPLS", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_W, .offset = IFX_1EDI2010AS_SCFG_OCPLS_OFF, .mask = IFX_1EDI2010AS_SCFG_OCPLS_MSK, .index = 65 },
    { .name = "DACLC", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_W, .offset = IFX_1EDI2010AS_SCFG_DACLC_OFF, .mask = IFX_1EDI2010AS_SCFG_DACLC_MSK, .index = 66 },
    IFX_DEVICEDATA_BITFIELD_END
};
const Ifx_DeviceData_Bitfield Ifx1edi2010as_dataBitfield_SCFG2[] =
{
    { .name = "PWMD", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_W, .offset = IFX_1EDI2010AS_SCFG2_PWMD_OFF, .mask = IFX_1EDI2010AS_SCFG2_PWMD_MSK, .index = 67 },
    { .name = "ATS", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_W, .offset = IFX_1EDI2010AS_SCFG2_ATS_OFF, .mask = IFX_1EDI2010AS_SCFG2_ATS_MSK, .index = 68 },
    { .name = "AGS", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_W, .offset = IFX_1EDI2010AS_SCFG2_AGS_OFF, .mask = IFX_1EDI2010AS_SCFG2_AGS_MSK, .index = 69 },
    { .name = "AOS", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_W, .offset = IFX_1EDI2010AS_SCFG2_AOS_OFF, .mask = IFX_1EDI2010AS_SCFG2_AOS_MSK, .index = 70 },
    { .name = "ACSS", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_W, .offset = IFX_1EDI2010AS_SCFG2_ACSS_OFF, .mask = IFX_1EDI2010AS_SCFG2_ACSS_MSK, .index = 71 },
    { .name = "ACAEN", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_W, .offset = IFX_1EDI2010AS_SCFG2_ACAEN_OFF, .mask = IFX_1EDI2010AS_SCFG2_ACAEN_MSK, .index = 72 },
    { .name = "ADCEN", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_W, .offset = IFX_1EDI2010AS_SCFG2_ADCEN_OFF, .mask = IFX_1EDI2010AS_SCFG2_ADCEN_MSK, .index = 73 },
    IFX_DEVICEDATA_BITFIELD_END
};
const Ifx_DeviceData_Bitfield Ifx1edi2010as_dataBitfield_SSCR[] =
{
    { .name = "VFS2", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_W|IFX_DEVICEDATA_ACCESS_FLAG_H, .offset = IFX_1EDI2010AS_SSCR_VFS2_OFF, .mask = IFX_1EDI2010AS_SSCR_VFS2_MSK, .index = 74 },
    IFX_DEVICEDATA_BITFIELD_END
};
const Ifx_DeviceData_Bitfield Ifx1edi2010as_dataBitfield_SDESAT[] =
{
    { .name = "DSATBT", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_W, .offset = IFX_1EDI2010AS_SDESAT_DSATBT_OFF, .mask = IFX_1EDI2010AS_SDESAT_DSATBT_MSK, .index = 75 },
    IFX_DEVICEDATA_BITFIELD_END
};
const Ifx_DeviceData_Bitfield Ifx1edi2010as_dataBitfield_SOCP[] =
{
    { .name = "OCPBT", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_W, .offset = IFX_1EDI2010AS_SOCP_OCPBT_OFF, .mask = IFX_1EDI2010AS_SOCP_OCPBT_MSK, .index = 76 },
    IFX_DEVICEDATA_BITFIELD_END
};
const Ifx_DeviceData_Bitfield Ifx1edi2010as_dataBitfield_SRTTOF[] =
{
    { .name = "RTVAL", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_W, .offset = IFX_1EDI2010AS_SRTTOF_RTVAL_OFF, .mask = IFX_1EDI2010AS_SRTTOF_RTVAL_MSK, .index = 77 },
    IFX_DEVICEDATA_BITFIELD_END
};
const Ifx_DeviceData_Bitfield Ifx1edi2010as_dataBitfield_SSTTOF[] =
{
    { .name = "GPS", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_W, .offset = IFX_1EDI2010AS_SSTTOF_GPS_OFF, .mask = IFX_1EDI2010AS_SSTTOF_GPS_MSK, .index = 78 },
    { .name = "STVAL", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_W, .offset = IFX_1EDI2010AS_SSTTOF_STVAL_OFF, .mask = IFX_1EDI2010AS_SSTTOF_STVAL_MSK, .index = 79 },
    IFX_DEVICEDATA_BITFIELD_END
};
const Ifx_DeviceData_Bitfield Ifx1edi2010as_dataBitfield_STTON[] =
{
    { .name = "TTONVAL", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_W, .offset = IFX_1EDI2010AS_STTON_TTONVAL_OFF, .mask = IFX_1EDI2010AS_STTON_TTONVAL_MSK, .index = 80 },
    IFX_DEVICEDATA_BITFIELD_END
};
const Ifx_DeviceData_Bitfield Ifx1edi2010as_dataBitfield_SADC[] =
{
    { .name = "AVFS", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_H, .offset = IFX_1EDI2010AS_SADC_AVFS_OFF, .mask = IFX_1EDI2010AS_SADC_AVFS_MSK, .index = 81 },
    { .name = "AUVS", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_H, .offset = IFX_1EDI2010AS_SADC_AUVS_OFF, .mask = IFX_1EDI2010AS_SADC_AUVS_MSK, .index = 82 },
    { .name = "AOVS", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_H, .offset = IFX_1EDI2010AS_SADC_AOVS_OFF, .mask = IFX_1EDI2010AS_SADC_AOVS_MSK, .index = 83 },
    { .name = "ADCVAL", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_H, .offset = IFX_1EDI2010AS_SADC_ADCVAL_OFF, .mask = IFX_1EDI2010AS_SADC_ADCVAL_MSK, .index = 84 },
    IFX_DEVICEDATA_BITFIELD_END
};
const Ifx_DeviceData_Bitfield Ifx1edi2010as_dataBitfield_SBC[] =
{
    { .name = "LCB1A", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_W, .offset = IFX_1EDI2010AS_SBC_LCB1A_OFF, .mask = IFX_1EDI2010AS_SBC_LCB1A_MSK, .index = 85 },
    { .name = "LCB1B", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_W, .offset = IFX_1EDI2010AS_SBC_LCB1B_OFF, .mask = IFX_1EDI2010AS_SBC_LCB1B_MSK, .index = 86 },
    IFX_DEVICEDATA_BITFIELD_END
};
const Ifx_DeviceData_Bitfield Ifx1edi2010as_dataBitfield_SCS[] =
{
    { .name = "SCSS", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_H, .offset = IFX_1EDI2010AS_SCS_SCSS_OFF, .mask = IFX_1EDI2010AS_SCS_SCSS_MSK, .index = 87 },
    IFX_DEVICEDATA_BITFIELD_END
};
const Ifx_DeviceData_Register Ifx1edi2010as_dataRegisters[] =
{
    { .name = "PID", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R, .offset = (uint16)(((uint32)&MODULE_1EDI2010AS.PID) >> IFX1EDI2010AS_ADDRESS_SHIFT), .bitfieldList = Ifx1edi2010as_dataBitfield_PID },
    { .name = "PSTAT", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_H, .offset = (uint16)(((uint32)&MODULE_1EDI2010AS.PSTAT) >> IFX1EDI2010AS_ADDRESS_SHIFT), .bitfieldList = Ifx1edi2010as_dataBitfield_PSTAT },
    { .name = "PSTAT2", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_H, .offset = (uint16)(((uint32)&MODULE_1EDI2010AS.PSTAT2) >> IFX1EDI2010AS_ADDRESS_SHIFT), .bitfieldList = Ifx1edi2010as_dataBitfield_PSTAT2 },
    { .name = "PER", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_H, .offset = (uint16)(((uint32)&MODULE_1EDI2010AS.PER) >> IFX1EDI2010AS_ADDRESS_SHIFT), .bitfieldList = Ifx1edi2010as_dataBitfield_PER },
    { .name = "PCFG", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_W, .offset = (uint16)(((uint32)&MODULE_1EDI2010AS.PCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), .bitfieldList = Ifx1edi2010as_dataBitfield_PCFG },
    { .name = "PCFG2", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_W, .offset = (uint16)(((uint32)&MODULE_1EDI2010AS.PCFG2) >> IFX1EDI2010AS_ADDRESS_SHIFT), .bitfieldList = Ifx1edi2010as_dataBitfield_PCFG2 },
    { .name = "PCTRL", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_W|IFX_DEVICEDATA_ACCESS_FLAG_H, .offset = (uint16)(((uint32)&MODULE_1EDI2010AS.PCTRL) >> IFX1EDI2010AS_ADDRESS_SHIFT), .bitfieldList = Ifx1edi2010as_dataBitfield_PCTRL },
    { .name = "PCTRL2", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_W|IFX_DEVICEDATA_ACCESS_FLAG_H, .offset = (uint16)(((uint32)&MODULE_1EDI2010AS.PCTRL2) >> IFX1EDI2010AS_ADDRESS_SHIFT), .bitfieldList = Ifx1edi2010as_dataBitfield_PCTRL2 },
    { .name = "PSCR", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_W|IFX_DEVICEDATA_ACCESS_FLAG_H, .offset = (uint16)(((uint32)&MODULE_1EDI2010AS.PSCR) >> IFX1EDI2010AS_ADDRESS_SHIFT), .bitfieldList = Ifx1edi2010as_dataBitfield_PSCR },
    { .name = "PRW", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_W, .offset = (uint16)(((uint32)&MODULE_1EDI2010AS.PRW) >> IFX1EDI2010AS_ADDRESS_SHIFT), .bitfieldList = Ifx1edi2010as_dataBitfield_PRW },
    { .name = "PPIN", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_H, .offset = (uint16)(((uint32)&MODULE_1EDI2010AS.PPIN) >> IFX1EDI2010AS_ADDRESS_SHIFT), .bitfieldList = Ifx1edi2010as_dataBitfield_PPIN },
    { .name = "PCS", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_H, .offset = (uint16)(((uint32)&MODULE_1EDI2010AS.PCS) >> IFX1EDI2010AS_ADDRESS_SHIFT), .bitfieldList = Ifx1edi2010as_dataBitfield_PCS },
    { .name = "SID", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R, .offset = (uint16)(((uint32)&MODULE_1EDI2010AS.SID) >> IFX1EDI2010AS_ADDRESS_SHIFT), .bitfieldList = Ifx1edi2010as_dataBitfield_SID },
    { .name = "SSTAT", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_H, .offset = (uint16)(((uint32)&MODULE_1EDI2010AS.SSTAT) >> IFX1EDI2010AS_ADDRESS_SHIFT), .bitfieldList = Ifx1edi2010as_dataBitfield_SSTAT },
    { .name = "SSTAT2", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_H, .offset = (uint16)(((uint32)&MODULE_1EDI2010AS.SSTAT2) >> IFX1EDI2010AS_ADDRESS_SHIFT), .bitfieldList = Ifx1edi2010as_dataBitfield_SSTAT2 },
    { .name = "SER", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_H, .offset = (uint16)(((uint32)&MODULE_1EDI2010AS.SER) >> IFX1EDI2010AS_ADDRESS_SHIFT), .bitfieldList = Ifx1edi2010as_dataBitfield_SER },
    { .name = "SCFG", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_W|IFX_DEVICEDATA_ACCESS_FLAG_H, .offset = (uint16)(((uint32)&MODULE_1EDI2010AS.SCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT), .bitfieldList = Ifx1edi2010as_dataBitfield_SCFG },
    { .name = "SCFG2", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_W, .offset = (uint16)(((uint32)&MODULE_1EDI2010AS.SCFG2) >> IFX1EDI2010AS_ADDRESS_SHIFT), .bitfieldList = Ifx1edi2010as_dataBitfield_SCFG2 },
    { .name = "SSCR", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_W|IFX_DEVICEDATA_ACCESS_FLAG_H, .offset = (uint16)(((uint32)&MODULE_1EDI2010AS.SSCR) >> IFX1EDI2010AS_ADDRESS_SHIFT), .bitfieldList = Ifx1edi2010as_dataBitfield_SSCR },
    { .name = "SDESAT", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_W, .offset = (uint16)(((uint32)&MODULE_1EDI2010AS.SDESAT) >> IFX1EDI2010AS_ADDRESS_SHIFT), .bitfieldList = Ifx1edi2010as_dataBitfield_SDESAT },
    { .name = "SOCP", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_W, .offset = (uint16)(((uint32)&MODULE_1EDI2010AS.SOCP) >> IFX1EDI2010AS_ADDRESS_SHIFT), .bitfieldList = Ifx1edi2010as_dataBitfield_SOCP },
    { .name = "SRTTOF", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_W, .offset = (uint16)(((uint32)&MODULE_1EDI2010AS.SRTTOF) >> IFX1EDI2010AS_ADDRESS_SHIFT), .bitfieldList = Ifx1edi2010as_dataBitfield_SRTTOF },
    { .name = "SSTTOF", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_W, .offset = (uint16)(((uint32)&MODULE_1EDI2010AS.SSTTOF) >> IFX1EDI2010AS_ADDRESS_SHIFT), .bitfieldList = Ifx1edi2010as_dataBitfield_SSTTOF },
    { .name = "STTON", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_W, .offset = (uint16)(((uint32)&MODULE_1EDI2010AS.STTON) >> IFX1EDI2010AS_ADDRESS_SHIFT), .bitfieldList = Ifx1edi2010as_dataBitfield_STTON },
    { .name = "SADC", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_H, .offset = (uint16)(((uint32)&MODULE_1EDI2010AS.SADC) >> IFX1EDI2010AS_ADDRESS_SHIFT), .bitfieldList = Ifx1edi2010as_dataBitfield_SADC },
    { .name = "SBC", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_W, .offset = (uint16)(((uint32)&MODULE_1EDI2010AS.SBC) >> IFX1EDI2010AS_ADDRESS_SHIFT), .bitfieldList = Ifx1edi2010as_dataBitfield_SBC },
    { .name = "SCS", .flags = IFX_DEVICEDATA_ACCESS_FLAG_R|IFX_DEVICEDATA_ACCESS_FLAG_H, .offset = (uint16)(((uint32)&MODULE_1EDI2010AS.SCS) >> IFX1EDI2010AS_ADDRESS_SHIFT), .bitfieldList = Ifx1edi2010as_dataBitfield_SCS },
    IFX_DEVICEDATA_REGISTER_END
};
const Ifx_DeviceData Ifx1edi2010as_data =
{
    .name = "1EDI2010AS",
    .registerList = Ifx1edi2010as_dataRegisters
};
/* *INDENT-ON* */

const Ifx_DeviceData_Register *Ifx1edi2010as_Data_findRegister(pchar name)
{
    const Ifx_DeviceData_Register *r = Ifx1edi2010as_dataRegisters;
    while (r->name != NULL_PTR)
    {
        if (strcmp(name, r->name) == 0)
        {
            return r;
        }
        r++;
    }
    return NULL_PTR;
}

const Ifx_DeviceData_Bitfield *Ifx1edi2010as_Data_findBitfield(const Ifx_DeviceData_Register *r, pchar name)
{
    if (r == NULL_PTR)
    {
        return NULL_PTR;
    }

    const Ifx_DeviceData_Bitfield *b = r->bitfieldList;
    while (b->name != NULL_PTR)
    {
        if (strcmp(name, b->name) == 0)
        {
            return b;
        }
        b++;
    }
    return NULL_PTR;
}

boolean Ifx1edi2010as_Data_getRegister(Ifx1edi2010as_Driver* driver, const Ifx_DeviceData_Register *r, Ifx1edi2010as_UData *value)
{
    boolean status;
    if ((r == NULL_PTR) || ((r->flags & IFX_DEVICEDATA_ACCESS_FLAG_R) == 0))
    {
        return FALSE;
    }

    status = Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)r->offset, value);
    *value = (*value) & 0xFFFC; /* Remove P and LMI */
    return status;
}



boolean Ifx1edi2010as_Data_setRegister(Ifx1edi2010as_Driver* driver, const Ifx_DeviceData_Register *r, Ifx1edi2010as_UData value)
{
    if ((r == NULL_PTR) || ((r->flags & IFX_DEVICEDATA_ACCESS_FLAG_W) == 0))
    {
        return FALSE;
    }

    return Ifx1edi2010as_Driver_writeRegister(driver, (Ifx1edi2010as_Address)r->offset, value);
}


boolean Ifx1edi2010as_Data_getBitfield(Ifx1edi2010as_Driver* driver, const Ifx_DeviceData_Register *r, const Ifx_DeviceData_Bitfield *b, Ifx1edi2010as_UData *value)
{
    if ((r == NULL_PTR) || (b == NULL_PTR) || ((b->flags & IFX_DEVICEDATA_ACCESS_FLAG_R) == 0))
    {
        return FALSE;
    }

    if (Ifx1edi2010as_Driver_readRegister(driver, (Ifx1edi2010as_Address)r->offset, value))
    {
        *value = (*value >> b->offset) & b->mask;
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}
boolean Ifx1edi2010as_Data_setBitfield(Ifx1edi2010as_Driver* driver, const Ifx_DeviceData_Register *r, const Ifx_DeviceData_Bitfield *b, Ifx1edi2010as_UData value)
{
    boolean status = TRUE;
    if ((r == NULL_PTR) || (b == NULL_PTR) || ((b->flags & IFX_DEVICEDATA_ACCESS_FLAG_W) == 0))
    {
        return FALSE;
    }

    switch (b->index)
    {
    case 21: Ifx1edi2010as_setSpiParityEnable(driver, (Ifx1edi2010as_SpiParityEnable)value, &status); break;
    case 22: Ifx1edi2010as_setNfltaPinActivationOnBoundaryCheckEventEnable(driver, (Ifx1edi2010as_NfltaPinActivationOnBoundaryCheckEventEnable)value, &status); break;
    case 23: Ifx1edi2010as_setAdcTriggerInputEnable(driver, (Ifx1edi2010as_AdcTriggerInputEnable)value, &status); break;
    case 24: Ifx1edi2010as_setShootThroughProtectionDelay(driver, (Ifx1edi2010as_ShootThroughProtectionDelay)value, &status); break;
    case 25: Ifx1edi2010as_setDio1PinMode(driver, (Ifx1edi2010as_Dio1PinMode)value, &status); break;
    case 26: Ifx1edi2010as_setGateTtonPlateauLevel(driver, (Ifx1edi2010as_GateTtonPlateauLevel)value, &status); break;
    case 27: Ifx1edi2010as_setClearPrimarySitckyBits(driver, (Ifx1edi2010as_ClearPrimarySitckyBits)value, &status); break;
    case 28: Ifx1edi2010as_setClearSecondarySitckyBits(driver, (Ifx1edi2010as_ClearSecondarySitckyBits)value, &status); break;
    case 29: Ifx1edi2010as_setGateRegularTtoffPlateauLevel(driver, (Ifx1edi2010as_GateRegularTtoffPlateauLevel)value, &status); break;
    case 30: Ifx1edi2010as_setAdcConversionRequest(driver, (Ifx1edi2010as_AdcConversionRequest)value, &status); break;
    case 31: Ifx1edi2010as_setPrimaryVerificationFunction(driver, (Ifx1edi2010as_PrimaryVerificationFunction)value, &status); break;
    case 32: Ifx1edi2010as_setDataIntegrityTestRegister(driver, (uint16)value, &status); break;
    case 57: Ifx1edi2010as_setVbeCompensationEnable(driver, (Ifx1edi2010as_VbeCompensationEnable)value, &status); break;
    case 58: Ifx1edi2010as_setSecondaryAdvancedConfigurationEnable(driver, (Ifx1edi2010as_SecondaryAdvancedConfigurationEnable)value, &status); break;
    case 59: Ifx1edi2010as_setDio2PinMode(driver, (Ifx1edi2010as_Dio2PinMode)value, &status); break;
    case 60: Ifx1edi2010as_setDesatClampingEnable(driver, (Ifx1edi2010as_DesatClampingEnable)value, &status); break;
    case 61: Ifx1edi2010as_setPulseSuppressorEnable(driver, (Ifx1edi2010as_PulseSuppressorEnable)value, &status); break;
    case 62: Ifx1edi2010as_setVerificationModeTimeOutDuration(driver, (Ifx1edi2010as_VerificationModeTimeOutDuration)value, &status); break;
    case 63: Ifx1edi2010as_setDesatThresholdLevel(driver, (Ifx1edi2010as_DesatThresholdLevel)value, &status); break;
    case 64: Ifx1edi2010as_setUvlo2ThresholdLevel(driver, (Ifx1edi2010as_Uvlo2ThresholdLevel)value, &status); break;
    case 65: Ifx1edi2010as_setOcpThresholdLevel(driver, (Ifx1edi2010as_OcpThresholdLevel)value, &status); break;
    case 66: Ifx1edi2010as_setDaclpPinClampingOutpout(driver, (Ifx1edi2010as_DaclpPinClampingOutpout)value, &status); break;
    case 67: Ifx1edi2010as_setAdcPwmTriggerDelay(driver, (Ifx1edi2010as_AdcPwmTriggerDelay)value, &status); break;
    case 68: Ifx1edi2010as_setAdcSecondaryTriggerMode(driver, (Ifx1edi2010as_AdcSecondaryTriggerMode)value, &status); break;
    case 69: Ifx1edi2010as_setAdcGain(driver, (Ifx1edi2010as_AdcGain)value, &status); break;
    case 70: Ifx1edi2010as_setAdcOffset(driver, (Ifx1edi2010as_AdcOffset)value, &status); break;
    case 71: Ifx1edi2010as_setAdcCurrentSource(driver, (Ifx1edi2010as_AdcCurrentSource)value, &status); break;
    case 72: Ifx1edi2010as_setAdcEventClassAEnable(driver, (Ifx1edi2010as_AdcEventClassAEnable)value, &status); break;
    case 73: Ifx1edi2010as_setAdcEnable(driver, (Ifx1edi2010as_AdcEnable)value, &status); break;
    case 74: Ifx1edi2010as_setSecondaryVerificationFunction(driver, (Ifx1edi2010as_SecondaryVerificationFunction)value, &status); break;
    case 75: Ifx1edi2010as_setDesatBlankingTime(driver, (uint8)value, &status); break;
    case 76: Ifx1edi2010as_setOcpBlankingTime(driver, (uint8)value, &status); break;
    case 77: Ifx1edi2010as_setGateRegularTtoffDelay(driver, (uint8)value, &status); break;
    case 78: Ifx1edi2010as_setGateSafeTtoffPlateauVoltage(driver, (uint8)value, &status); break;
    case 79: Ifx1edi2010as_setGateSafeTtoffDelay(driver, (uint8)value, &status); break;
    case 80: Ifx1edi2010as_setGateTtonDelay(driver, (uint8)value, &status); break;
    case 85: Ifx1edi2010as_setAdcLimitCheckingBoundaryA(driver, (uint8)value, &status); break;
    case 86: Ifx1edi2010as_setAdcLimitCheckingBoundaryB(driver, (uint8)value, &status); break;
    default: break;
    }
    return status;
}
