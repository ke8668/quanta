/**
 * \file Ifx1edi2010as_cfg.h
 * \brief 1EDI2010AS on-chip implementation data 
 * \ingroup IfxLld_1edi2010as 
 *
 * \copyright Copyright (c) 2017 Infineon Technologies AG. All rights reserved.
 *
 * $Date: 2017-06-30 15:39:05
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * \defgroup IfxLld_1edi2010as 1EDI2010AS
 * \ingroup IfxLld_1edi2010as_Driver
 * \defgroup IfxLld_1edi2010as_Impl Implementation
 * \ingroup IfxLld_1edi2010as
 * \defgroup IfxLld_1edi2010as_Std Standard Driver
 * \ingroup IfxLld_1edi2010as
 */

#ifndef IFX1EDI2010AS_CFG_H
#define IFX1EDI2010AS_CFG_H 1

/******************************************************************************/
/*----------------------------------Includes----------------------------------*/
/******************************************************************************/

#include "SysSe/Ext/1edi2010as/_Reg/Ifx1edi2010as_regdef.h"
#include "SysSe/Ext/1edi2010as/_Reg/Ifx1edi2010as_enum.h"

#endif /* IFX1EDI2010AS_CFG_H */
