/*
 * Ifx1edi2010as_data.h
 *
 *  Created on: 29 Sep 2016
 *      Author: denais
 */

#ifndef IFX1EDI2010AS_DATA_H_
#define IFX1EDI2010AS_DATA_H_

#include "Common/Ifx_DeviceData.h"
#include "SysSe/Ext/1edi2010as/Std/Ifx1edi2010as.h"

extern const Ifx_DeviceData Ifx1edi2010as_data;

const Ifx_DeviceData_Register *Ifx1edi2010as_Data_findRegister(pchar name);
const Ifx_DeviceData_Bitfield *Ifx1edi2010as_Data_findBitfield(const Ifx_DeviceData_Register *r, pchar name);
boolean Ifx1edi2010as_Data_getRegister(Ifx1edi2010as_Driver* driver, const Ifx_DeviceData_Register *r, uint16 *value);
boolean Ifx1edi2010as_Data_setRegister(Ifx1edi2010as_Driver* driver, const Ifx_DeviceData_Register *r, uint16 value);
boolean Ifx1edi2010as_Data_getBitfield(Ifx1edi2010as_Driver* driver, const Ifx_DeviceData_Register *r, const Ifx_DeviceData_Bitfield *b, uint16 *value);
boolean Ifx1edi2010as_Data_setBitfield(Ifx1edi2010as_Driver* driver, const Ifx_DeviceData_Register *r, const Ifx_DeviceData_Bitfield *b, uint16 value);




#endif /* IFX1EDI2010AS_DATA_H_ */
