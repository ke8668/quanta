/**
 * \file Ifx1edi2010as_reg.h
 * \brief
 * \copyright Copyright (c) 2017 Infineon Technologies AG. All rights reserved.
 *
 *
 * Date: 2017-06-30 15:39:21 GMT
 * Version: TBD
 * Specification: TBD
 * MAY BE CHANGED BY USER [yes/no]: Yes
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * \defgroup IfxLld_1edi2010as_Registers_Cfg 1edi2010as address
 * \ingroup IfxLld_1edi2010as_Registers
 * 
 * \defgroup IfxLld_1edi2010as_Registers_Cfg_BaseAddress Base address
 * \ingroup IfxLld_1edi2010as_Registers_Cfg
 *
 * \defgroup IfxLld_1edi2010as_Registers_Cfg_1edi2010as 2-1EDI2010AS
 * \ingroup IfxLld_1edi2010as_Registers_Cfg
 *
 *
 */
#ifndef IFX1EDI2010AS_REG_H
#define IFX1EDI2010AS_REG_H 1
/******************************************************************************/
#include "Ifx1edi2010as_regdef.h"
/******************************************************************************/
/** \addtogroup IfxLld_1edi2010as_Registers_Cfg_BaseAddress
 * \{  */

/** \brief 1EDI2010AS object */
#define MODULE_1EDI2010AS /*lint --e(923)*/ ((*(Ifx_1EDI2010AS*)0x0u))
/** \}  */


/******************************************************************************/
/******************************************************************************/
/** \addtogroup IfxLld_1edi2010as_Registers_Cfg_1edi2010as
 * \{  */
/** \brief 0, Primary ID Register */
#define _1EDI2010AS_PID /*lint --e(923)*/ (*(volatile Ifx_1EDI2010AS_PID*)0x0u)

/** \brief 1, Primary Status Register */
#define _1EDI2010AS_PSTAT /*lint --e(923)*/ (*(volatile Ifx_1EDI2010AS_PSTAT*)0x1u)

/** \brief 2, Primary Second Status Register */
#define _1EDI2010AS_PSTAT2 /*lint --e(923)*/ (*(volatile Ifx_1EDI2010AS_PSTAT2*)0x2u)

/** \brief 3, Primary Error Register */
#define _1EDI2010AS_PER /*lint --e(923)*/ (*(volatile Ifx_1EDI2010AS_PER*)0x3u)

/** \brief 4, Primary Configuration Register */
#define _1EDI2010AS_PCFG /*lint --e(923)*/ (*(volatile Ifx_1EDI2010AS_PCFG*)0x4u)

/** \brief 5, Primary Second Configuration Register */
#define _1EDI2010AS_PCFG2 /*lint --e(923)*/ (*(volatile Ifx_1EDI2010AS_PCFG2*)0x5u)

/** \brief 6, Primary Control Register */
#define _1EDI2010AS_PCTRL /*lint --e(923)*/ (*(volatile Ifx_1EDI2010AS_PCTRL*)0x6u)

/** \brief 7, Primary Second Control Register */
#define _1EDI2010AS_PCTRL2 /*lint --e(923)*/ (*(volatile Ifx_1EDI2010AS_PCTRL2*)0x7u)

/** \brief 8, Primary Supervision Function Control Register */
#define _1EDI2010AS_PSCR /*lint --e(923)*/ (*(volatile Ifx_1EDI2010AS_PSCR*)0x8u)

/** \brief 9, Primary Read/Write Register */
#define _1EDI2010AS_PRW /*lint --e(923)*/ (*(volatile Ifx_1EDI2010AS_PRW*)0x9u)

/** \brief A, Primary Pin Status Register */
#define _1EDI2010AS_PPIN /*lint --e(923)*/ (*(volatile Ifx_1EDI2010AS_PPIN*)0xAu)

/** \brief B, Primary Clock Supervision Register */
#define _1EDI2010AS_PCS /*lint --e(923)*/ (*(volatile Ifx_1EDI2010AS_PCS*)0xBu)

/** \brief 10, Secondary ID Register */
#define _1EDI2010AS_SID /*lint --e(923)*/ (*(volatile Ifx_1EDI2010AS_SID*)0x10u)

/** \brief 11, Secondary Status Register */
#define _1EDI2010AS_SSTAT /*lint --e(923)*/ (*(volatile Ifx_1EDI2010AS_SSTAT*)0x11u)

/** \brief 12, Secondary Second Status Register */
#define _1EDI2010AS_SSTAT2 /*lint --e(923)*/ (*(volatile Ifx_1EDI2010AS_SSTAT2*)0x12u)

/** \brief 13, Secondary Error Register */
#define _1EDI2010AS_SER /*lint --e(923)*/ (*(volatile Ifx_1EDI2010AS_SER*)0x13u)

/** \brief 14, Secondary Configuration Register */
#define _1EDI2010AS_SCFG /*lint --e(923)*/ (*(volatile Ifx_1EDI2010AS_SCFG*)0x14u)

/** \brief 15, Secondary Second Configuration Register */
#define _1EDI2010AS_SCFG2 /*lint --e(923)*/ (*(volatile Ifx_1EDI2010AS_SCFG2*)0x15u)

/** \brief 17, Secondary Supervision Function Control Register */
#define _1EDI2010AS_SSCR /*lint --e(923)*/ (*(volatile Ifx_1EDI2010AS_SSCR*)0x17u)

/** \brief 18, Secondary DESAT Blanking Time Register */
#define _1EDI2010AS_SDESAT /*lint --e(923)*/ (*(volatile Ifx_1EDI2010AS_SDESAT*)0x18u)

/** \brief 19, Secondary OCP Blanking Time Register */
#define _1EDI2010AS_SOCP /*lint --e(923)*/ (*(volatile Ifx_1EDI2010AS_SOCP*)0x19u)

/** \brief 1A, Secondary Regular TTOFF Configuration Register */
#define _1EDI2010AS_SRTTOF /*lint --e(923)*/ (*(volatile Ifx_1EDI2010AS_SRTTOF*)0x1Au)

/** \brief 1B, Secondary Safe TTOFF Configuration Register */
#define _1EDI2010AS_SSTTOF /*lint --e(923)*/ (*(volatile Ifx_1EDI2010AS_SSTTOF*)0x1Bu)

/** \brief 1C, Secondary TTON Configuration Register */
#define _1EDI2010AS_STTON /*lint --e(923)*/ (*(volatile Ifx_1EDI2010AS_STTON*)0x1Cu)

/** \brief 1D, Secondary ADC Result Register */
#define _1EDI2010AS_SADC /*lint --e(923)*/ (*(volatile Ifx_1EDI2010AS_SADC*)0x1Du)

/** \brief 1E, Secondary ADC Boundary Register */
#define _1EDI2010AS_SBC /*lint --e(923)*/ (*(volatile Ifx_1EDI2010AS_SBC*)0x1Eu)

/** \brief 1F, Secondary Clock Supervision Register */
#define _1EDI2010AS_SCS /*lint --e(923)*/ (*(volatile Ifx_1EDI2010AS_SCS*)0x1Fu)


/** \}  */
/******************************************************************************/
/******************************************************************************/
#endif /* IFX1EDI2010AS_REG_H */
