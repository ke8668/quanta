/**
 * \file Ifx1edi2010as_regdef.h
 * \brief
 * \copyright Copyright (c) 2017 Infineon Technologies AG. All rights reserved.
 *
 *
 * Date: 2017-06-30 15:39:21 GMT
 * Version: TBD
 * Specification: TBD
 * MAY BE CHANGED BY USER [yes/no]: Yes
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * \defgroup IfxLld_1edi2010as_Registers 1edi2010as Registers
 * \ingroup IfxLld_1edi2010as
 * 
 * \defgroup IfxLld_1edi2010as_Registers_Bitfields Bitfields
 * \ingroup IfxLld_1edi2010as_Registers
 * 
 * \defgroup IfxLld_1edi2010as_Registers_union Register unions
 * \ingroup IfxLld_1edi2010as_Registers
 * 
 * \defgroup IfxLld_1edi2010as_Registers_struct Memory map
 * \ingroup IfxLld_1edi2010as_Registers
 */
#ifndef IFX1EDI2010AS_REGDEF_H
#define IFX1EDI2010AS_REGDEF_H 1
/******************************************************************************/
#include "Platform_Types.h"
#include "Compilers.h"
#include "Ifx_TypesReg.h"
#define IFX1EDI2010AS_ADDRESS_UNIT  (2)
#define IFX1EDI2010AS_ADDRESS_SHIFT (1)
typedef uint8 Ifx1edi2010as_Address;
typedef Ifx_UReg_16Bit Ifx1edi2010as_UData;
typedef Ifx_UReg_16Bit Ifx1edi2010as_SData;
/******************************************************************************/
/** \addtogroup IfxLld_1edi2010as_Registers_Bitfields
 * \{  */
/** \brief Primary Configuration Register */
typedef struct _Ifx_1EDI2010AS_PCFG_Bits
{
    Ifx_UReg_16Bit P:1;               /**< \brief [0:0] Parity Bit (rh) */
    Ifx_UReg_16Bit LMI:1;             /**< \brief [1:1] Last Message Invalid Notification (rh) */
    Ifx_UReg_16Bit PAREN:1;           /**< \brief [2:2] SPI Parity Enable (rw) */
    Ifx_UReg_16Bit reserved_3:2;      /**< \brief [4:3] \internal Reserved */
    Ifx_UReg_16Bit ADAEN:1;           /**< \brief [5:5] NFLTA Pin Activation on Boundary Check Event Enable (rw) */
    Ifx_UReg_16Bit ADTEN:1;           /**< \brief [6:6] ADC Trigger Input Enable (rw) */
    Ifx_UReg_16Bit reserved_7:9;      /**< \brief [15:7] \internal Reserved */
} Ifx_1EDI2010AS_PCFG_Bits;

/** \brief Primary Second Configuration Register */
typedef struct _Ifx_1EDI2010AS_PCFG2_Bits
{
    Ifx_UReg_16Bit P:1;               /**< \brief [0:0] Parity Bit (rh) */
    Ifx_UReg_16Bit LMI:1;             /**< \brief [1:1] Last Message Invalid Notification (rh) */
    Ifx_UReg_16Bit STPDEL:6;          /**< \brief [7:2] Shoot Through Protection Delay (rw) */
    Ifx_UReg_16Bit DIO1:1;            /**< \brief [8:8] DIO1 Pin Mode (rw) */
    Ifx_UReg_16Bit reserved_9:7;      /**< \brief [15:9] \internal Reserved */
} Ifx_1EDI2010AS_PCFG2_Bits;

/** \brief Primary Clock Supervision Register */
typedef struct _Ifx_1EDI2010AS_PCS_Bits
{
    Ifx_UReg_16Bit P:1;               /**< \brief [0:0] Parity Bit (rh) */
    Ifx_UReg_16Bit LMI:1;             /**< \brief [1:1] Last Message Invalid Notification (rh) */
    Ifx_UReg_16Bit reserved_2:6;      /**< \brief [7:2] \internal Reserved */
    Ifx_UReg_16Bit CSP:8;             /**< \brief [15:8] Primary Clock Supervision (rh) */
} Ifx_1EDI2010AS_PCS_Bits;

/** \brief Primary Control Register */
typedef struct _Ifx_1EDI2010AS_PCTRL_Bits
{
    Ifx_UReg_16Bit P:1;               /**< \brief [0:0] Parity Bit (rh) */
    Ifx_UReg_16Bit LMI:1;             /**< \brief [1:1] Last Message Invalid Notification (rh) */
    Ifx_UReg_16Bit GPON:3;            /**< \brief [4:2] Gate TTON Plateau Level (rw) */
    Ifx_UReg_16Bit CLRP:1;            /**< \brief [5:5] Clear Primary Sitcky Bits (rwh) */
    Ifx_UReg_16Bit CLRS:1;            /**< \brief [6:6] Clear Secondary Sitcky Bits (rwh) */
    Ifx_UReg_16Bit reserved_7:9;      /**< \brief [15:7] \internal Reserved */
} Ifx_1EDI2010AS_PCTRL_Bits;

/** \brief Primary Second Control Register */
typedef struct _Ifx_1EDI2010AS_PCTRL2_Bits
{
    Ifx_UReg_16Bit P:1;               /**< \brief [0:0] Parity Bit (rh) */
    Ifx_UReg_16Bit LMI:1;             /**< \brief [1:1] Last Message Invalid Notification (rh) */
    Ifx_UReg_16Bit GPOF:3;            /**< \brief [4:2] Gate Regular TTOFF Plateau Level (rw) */
    Ifx_UReg_16Bit ACRP:1;            /**< \brief [5:5] ADC Conversion Request (rwh) */
    Ifx_UReg_16Bit reserved_6:10;     /**< \brief [15:6] \internal Reserved */
} Ifx_1EDI2010AS_PCTRL2_Bits;

/** \brief Primary Error Register */
typedef struct _Ifx_1EDI2010AS_PER_Bits
{
    Ifx_UReg_16Bit P:1;               /**< \brief [0:0] Parity Bit (rh) */
    Ifx_UReg_16Bit LMI:1;             /**< \brief [1:1] Last Message Invalid Notification (rh) */
    Ifx_UReg_16Bit CERP:1;            /**< \brief [2:2] Primary Communication Error Flag (rh) */
    Ifx_UReg_16Bit reserved_3:3;      /**< \brief [5:3] \internal Reserved */
    Ifx_UReg_16Bit ADER:1;            /**< \brief [6:6] ADC Error Flag (rh) */
    Ifx_UReg_16Bit reserved_7:1;      /**< \brief [7:7] \internal Reserved */
    Ifx_UReg_16Bit SPIER:1;           /**< \brief [8:8] SPI Error Flag (rh) */
    Ifx_UReg_16Bit STPER:1;           /**< \brief [9:9] Shoot Through Protection Error Flag (rh) */
    Ifx_UReg_16Bit ENER:1;            /**< \brief [10:10] EN Signal Invalid Flag (rh) */
    Ifx_UReg_16Bit RSTP:1;            /**< \brief [11:11] Primary Reset Flag (rh) */
    Ifx_UReg_16Bit RSTEP:1;           /**< \brief [12:12] Primary External Hard Reset flag (rh) */
    Ifx_UReg_16Bit reserved_13:3;     /**< \brief [15:13] \internal Reserved */
} Ifx_1EDI2010AS_PER_Bits;

/** \brief Primary ID Register */
typedef struct _Ifx_1EDI2010AS_PID_Bits
{
    Ifx_UReg_16Bit P:1;               /**< \brief [0:0] Parity Bit (rh) */
    Ifx_UReg_16Bit LMI:1;             /**< \brief [1:1] Last Message Invalid Notification (rh) */
    Ifx_UReg_16Bit reserved_2:2;      /**< \brief [3:2] \internal Reserved */
    Ifx_UReg_16Bit PVERS:12;          /**< \brief [15:4] Primary Chip Identification (rh) */
} Ifx_1EDI2010AS_PID_Bits;

/** \brief Primary Pin Status Register */
typedef struct _Ifx_1EDI2010AS_PPIN_Bits
{
    Ifx_UReg_16Bit P:1;               /**< \brief [0:0] Parity Bit (rh) */
    Ifx_UReg_16Bit LMI:1;             /**< \brief [1:1] Last Message Invalid Notification (rh) */
    Ifx_UReg_16Bit INPL:1;            /**< \brief [2:2] INP Pin Level (rh) */
    Ifx_UReg_16Bit INSTPL:1;          /**< \brief [3:3] INSTP Pin Level (rh) */
    Ifx_UReg_16Bit ENL:1;             /**< \brief [4:4] EN Pin Level (rh) */
    Ifx_UReg_16Bit NFLTAL:1;          /**< \brief [5:5] NFLTA pin Level (rh) */
    Ifx_UReg_16Bit NFLTBL:1;          /**< \brief [6:6] NFLTB pin level (rh) */
    Ifx_UReg_16Bit ADCTL:1;           /**< \brief [7:7] ADC Trigger Input Level (rh) */
    Ifx_UReg_16Bit DIO1L:1;           /**< \brief [8:8] DIO1 Pin Level (rh) */
    Ifx_UReg_16Bit reserved_9:7;      /**< \brief [15:9] \internal Reserved */
} Ifx_1EDI2010AS_PPIN_Bits;

/** \brief Primary Read/Write Register */
typedef struct _Ifx_1EDI2010AS_PRW_Bits
{
    Ifx_UReg_16Bit P:1;               /**< \brief [0:0] Parity Bit (rh) */
    Ifx_UReg_16Bit LMI:1;             /**< \brief [1:1] Last Message Invalid Flag (rh) */
    Ifx_UReg_16Bit RWVAL:14;          /**< \brief [15:2] Data Integrity Test Register (rw) */
} Ifx_1EDI2010AS_PRW_Bits;

/** \brief Primary Supervision Function Control Register */
typedef struct _Ifx_1EDI2010AS_PSCR_Bits
{
    Ifx_UReg_16Bit P:1;               /**< \brief [0:0] Parity Bit (rh) */
    Ifx_UReg_16Bit LMI:1;             /**< \brief [1:1] Last Message Invalid Notification (rh) */
    Ifx_UReg_16Bit VFSP:2;            /**< \brief [3:2] Primary Verification Function (rwh) */
    Ifx_UReg_16Bit reserved_4:12;     /**< \brief [15:4] \internal Reserved */
} Ifx_1EDI2010AS_PSCR_Bits;

/** \brief Primary Status Register */
typedef struct _Ifx_1EDI2010AS_PSTAT_Bits
{
    Ifx_UReg_16Bit P:1;               /**< \brief [0:0] Parity Bit (rh) */
    Ifx_UReg_16Bit LMI:1;             /**< \brief [1:1] Last Message Invalid Notification (rh) */
    Ifx_UReg_16Bit GPOFP:3;           /**< \brief [4:2] Gate Regular TTOFF Plateau Level Configuration Status (rh) */
    Ifx_UReg_16Bit AVFP:1;            /**< \brief [5:5] ADC Result Valid Flag (rh) */
    Ifx_UReg_16Bit SRDY:1;            /**< \brief [6:6] Secondary Ready Status (rh) */
    Ifx_UReg_16Bit ACT:1;             /**< \brief [7:7] Active State Status (rh) */
    Ifx_UReg_16Bit GPONP:3;           /**< \brief [10:8] Gate TTON Plateau Level Configuration Status (rh) */
    Ifx_UReg_16Bit ERR:1;             /**< \brief [11:11] Error Status (rh) */
    Ifx_UReg_16Bit reserved_12:4;     /**< \brief [15:12] \internal Reserved */
} Ifx_1EDI2010AS_PSTAT_Bits;

/** \brief Primary Second Status Register */
typedef struct _Ifx_1EDI2010AS_PSTAT2_Bits
{
    Ifx_UReg_16Bit P:1;               /**< \brief [0:0] Parity Bit (rh) */
    Ifx_UReg_16Bit LMI:1;             /**< \brief [1:1] Last Message Invalid Notification (rh) */
    Ifx_UReg_16Bit ENVAL:1;           /**< \brief [2:2] EN Valid Status (rh) */
    Ifx_UReg_16Bit FLTA:1;            /**< \brief [3:3] NFLTA Pin Driver Request (rh) */
    Ifx_UReg_16Bit FLTB:1;            /**< \brief [4:4] NFLTB Pin Driver Request (rh) */
    Ifx_UReg_16Bit OPMP:3;            /**< \brief [7:5] Operating Mode (rh) */
    Ifx_UReg_16Bit FLTAP:1;           /**< \brief [8:8] Event Class A Status (rh) */
    Ifx_UReg_16Bit FLTBP:1;           /**< \brief [9:9] Event Class B Status (rh) */
    Ifx_UReg_16Bit STP:1;             /**< \brief [10:10] Shoot Through Protection Status (rh) */
    Ifx_UReg_16Bit AXVP:1;            /**< \brief [11:11] ADC Under Or Overvoltage Error Status (rh) */
    Ifx_UReg_16Bit reserved_12:4;     /**< \brief [15:12] \internal Reserved */
} Ifx_1EDI2010AS_PSTAT2_Bits;

/** \brief Secondary ADC Result Register */
typedef struct _Ifx_1EDI2010AS_SADC_Bits
{
    Ifx_UReg_16Bit P:1;               /**< \brief [0:0] Parity Bit (rh) */
    Ifx_UReg_16Bit LMI:1;             /**< \brief [1:1] Last Message Invalid Notification (rh) */
    Ifx_UReg_16Bit reserved_2:2;      /**< \brief [3:2] \internal Reserved */
    Ifx_UReg_16Bit AVFS:1;            /**< \brief [4:4] ADC Result Valid Flag (rh) */
    Ifx_UReg_16Bit AUVS:1;            /**< \brief [5:5] ADC Undervoltage Error Status (rh) */
    Ifx_UReg_16Bit AOVS:1;            /**< \brief [6:6] ADC Overvoltage Error Status (rh) */
    Ifx_UReg_16Bit reserved_7:1;      /**< \brief [7:7] \internal Reserved */
    Ifx_UReg_16Bit ADCVAL:8;          /**< \brief [15:8] ADC Result (rh) */
} Ifx_1EDI2010AS_SADC_Bits;

/** \brief Secondary ADC Boundary Register */
typedef struct _Ifx_1EDI2010AS_SBC_Bits
{
    Ifx_UReg_16Bit P:1;               /**< \brief [0:0] Parity Bit (rh) */
    Ifx_UReg_16Bit LMI:1;             /**< \brief [1:1] Last Message Invalid Notification (rh) */
    Ifx_UReg_16Bit reserved_2:2;      /**< \brief [3:2] \internal Reserved */
    Ifx_UReg_16Bit LCB1A:6;           /**< \brief [9:4] ADC Limit Checking Boundary A (rw) */
    Ifx_UReg_16Bit LCB1B:6;           /**< \brief [15:10] ADC Limit Checking Boundary B (rw) */
} Ifx_1EDI2010AS_SBC_Bits;

/** \brief Secondary Configuration Register */
typedef struct _Ifx_1EDI2010AS_SCFG_Bits
{
    Ifx_UReg_16Bit P:1;               /**< \brief [0:0] Parity Bit (rh) */
    Ifx_UReg_16Bit LMI:1;             /**< \brief [1:1] Last Message Invalid Notification (rh) */
    Ifx_UReg_16Bit reserved_2:2;      /**< \brief [3:2] \internal Reserved */
    Ifx_UReg_16Bit VBEC:1;            /**< \brief [4:4] VBE Compensation Enable (rw) */
    Ifx_UReg_16Bit CFG2:1;            /**< \brief [5:5] Secondary Advanced Configuration Enable (rwh) */
    Ifx_UReg_16Bit DIO2C:2;           /**< \brief [7:6] DIO2 Pin Mode (rw) */
    Ifx_UReg_16Bit DSTCEN:1;          /**< \brief [8:8] DESAT Clamping Enable (rw) */
    Ifx_UReg_16Bit PSEN:1;            /**< \brief [9:9] Pulse Suppressor Enable (rw) */
    Ifx_UReg_16Bit TOSEN:1;           /**< \brief [10:10] Verification Mode Time Out Duration (rw) */
    Ifx_UReg_16Bit DSATLS:1;          /**< \brief [11:11] DESAT Threshold Level (rw) */
    Ifx_UReg_16Bit UVLO2S:1;          /**< \brief [12:12] UVLO2 Threshold Level (rw) */
    Ifx_UReg_16Bit OCPLS:1;           /**< \brief [13:13] OCP Threshold Level (rw) */
    Ifx_UReg_16Bit DACLC:2;           /**< \brief [15:14] DACLP Pin clamping outpout (rw) */
} Ifx_1EDI2010AS_SCFG_Bits;

/** \brief Secondary Second Configuration Register */
typedef struct _Ifx_1EDI2010AS_SCFG2_Bits
{
    Ifx_UReg_16Bit P:1;               /**< \brief [0:0] Parity Bit (rh) */
    Ifx_UReg_16Bit LMI:1;             /**< \brief [1:1] Last Message Invalid Notification (rh) */
    Ifx_UReg_16Bit reserved_2:2;      /**< \brief [3:2] \internal Reserved */
    Ifx_UReg_16Bit PWMD:2;            /**< \brief [5:4] ADC PWM Trigger Delay (rw) */
    Ifx_UReg_16Bit ATS:2;             /**< \brief [7:6] ADC Secondary Trigger Mode (rw) */
    Ifx_UReg_16Bit AGS:2;             /**< \brief [9:8] ADC Gain (rw) */
    Ifx_UReg_16Bit AOS:3;             /**< \brief [12:10] ADC Offset (rw) */
    Ifx_UReg_16Bit ACSS:1;            /**< \brief [13:13] ADC Current Source (rw) */
    Ifx_UReg_16Bit ACAEN:1;           /**< \brief [14:14] ADC Event Class A Enable (rw) */
    Ifx_UReg_16Bit ADCEN:1;           /**< \brief [15:15] ADC Enable (rw) */
} Ifx_1EDI2010AS_SCFG2_Bits;

/** \brief Secondary Clock Supervision Register */
typedef struct _Ifx_1EDI2010AS_SCS_Bits
{
    Ifx_UReg_16Bit P:1;               /**< \brief [0:0] Parity Bit (rh) */
    Ifx_UReg_16Bit LMI:1;             /**< \brief [1:1] Last Message Invalid Notification (rh) */
    Ifx_UReg_16Bit reserved_2:6;      /**< \brief [7:2] \internal Reserved */
    Ifx_UReg_16Bit SCSS:8;            /**< \brief [15:8] Secondary Supervision Oscillator Clock Cycles (rh) */
} Ifx_1EDI2010AS_SCS_Bits;

/** \brief Secondary DESAT Blanking Time Register */
typedef struct _Ifx_1EDI2010AS_SDESAT_Bits
{
    Ifx_UReg_16Bit P:1;               /**< \brief [0:0] Parity Bit (rh) */
    Ifx_UReg_16Bit LMI:1;             /**< \brief [1:1] Last Message Invalid Notification (rh) */
    Ifx_UReg_16Bit reserved_2:6;      /**< \brief [7:2] \internal Reserved */
    Ifx_UReg_16Bit DSATBT:8;          /**< \brief [15:8] DESAT Blanking Time (rw) */
} Ifx_1EDI2010AS_SDESAT_Bits;

/** \brief Secondary Error Register */
typedef struct _Ifx_1EDI2010AS_SER_Bits
{
    Ifx_UReg_16Bit P:1;               /**< \brief [0:0] Parity Bit (rh) */
    Ifx_UReg_16Bit LMI:1;             /**< \brief [1:1] Last Message Invalid Notification (rh) */
    Ifx_UReg_16Bit reserved_2:2;      /**< \brief [3:2] \internal Reserved */
    Ifx_UReg_16Bit CERS:1;            /**< \brief [4:4] Communication Error Secondary Flag (rh) */
    Ifx_UReg_16Bit AUVER:1;           /**< \brief [5:5] ADC Undervoltage Error Flag (rh) */
    Ifx_UReg_16Bit AOVER:1;           /**< \brief [6:6] ADC Overvoltage Error Flag (rh) */
    Ifx_UReg_16Bit reserved_7:2;      /**< \brief [8:7] \internal Reserved */
    Ifx_UReg_16Bit VMTO:1;            /**< \brief [9:9] Verification Mode Time-Out Event Flag (rh) */
    Ifx_UReg_16Bit reserved_10:2;     /**< \brief [11:10] \internal Reserved */
    Ifx_UReg_16Bit UVLO2ER:1;         /**< \brief [12:12] UVLO2 Error Flag (rh) */
    Ifx_UReg_16Bit DESATER:1;         /**< \brief [13:13] DESAT Error Flag (rh) */
    Ifx_UReg_16Bit OCPER:1;           /**< \brief [14:14] OCP Error Flag (rh) */
    Ifx_UReg_16Bit RSTS:1;            /**< \brief [15:15] Secondary Hard Reset Flag (rh) */
} Ifx_1EDI2010AS_SER_Bits;

/** \brief Secondary ID Register */
typedef struct _Ifx_1EDI2010AS_SID_Bits
{
    Ifx_UReg_16Bit P:1;               /**< \brief [0:0] Parity Bit (rh) */
    Ifx_UReg_16Bit LMI:1;             /**< \brief [1:1] Last Message Invalid Notification (rh) */
    Ifx_UReg_16Bit reserved_2:2;      /**< \brief [3:2] \internal Reserved */
    Ifx_UReg_16Bit SVERS:12;          /**< \brief [15:4] Secondary Chip Identification (rh) */
} Ifx_1EDI2010AS_SID_Bits;

/** \brief Secondary OCP Blanking Time Register */
typedef struct _Ifx_1EDI2010AS_SOCP_Bits
{
    Ifx_UReg_16Bit P:1;               /**< \brief [0:0] Parity Bit (rh) */
    Ifx_UReg_16Bit LMI:1;             /**< \brief [1:1] Last Message Invalid Notification (rh) */
    Ifx_UReg_16Bit reserved_2:6;      /**< \brief [7:2] \internal Reserved */
    Ifx_UReg_16Bit OCPBT:8;           /**< \brief [15:8] OCP Blanking Time (rw) */
} Ifx_1EDI2010AS_SOCP_Bits;

/** \brief Secondary Regular TTOFF Configuration Register */
typedef struct _Ifx_1EDI2010AS_SRTTOF_Bits
{
    Ifx_UReg_16Bit P:1;               /**< \brief [0:0] Parity Bit (rh) */
    Ifx_UReg_16Bit LMI:1;             /**< \brief [1:1] Last Message Invalid Notification (rh) */
    Ifx_UReg_16Bit reserved_2:6;      /**< \brief [7:2] \internal Reserved */
    Ifx_UReg_16Bit RTVAL:8;           /**< \brief [15:8] Gate Regular TTOFF delay (rw) */
} Ifx_1EDI2010AS_SRTTOF_Bits;

/** \brief Secondary Supervision Function Control Register */
typedef struct _Ifx_1EDI2010AS_SSCR_Bits
{
    Ifx_UReg_16Bit P:1;               /**< \brief [0:0] Parity Bit (rh) */
    Ifx_UReg_16Bit LMI:1;             /**< \brief [1:1] Last Message Invalid Notification (rh) */
    Ifx_UReg_16Bit reserved_2:2;      /**< \brief [3:2] \internal Reserved */
    Ifx_UReg_16Bit VFS2:2;            /**< \brief [5:4] Secondary Verification Function (rwh) */
    Ifx_UReg_16Bit reserved_6:10;     /**< \brief [15:6] \internal Reserved */
} Ifx_1EDI2010AS_SSCR_Bits;

/** \brief Secondary Status Register */
typedef struct _Ifx_1EDI2010AS_SSTAT_Bits
{
    Ifx_UReg_16Bit P:1;               /**< \brief [0:0] Parity Bit (rh) */
    Ifx_UReg_16Bit LMI:1;             /**< \brief [1:1] Last Message Invalid Notification (rh) */
    Ifx_UReg_16Bit reserved_2:2;      /**< \brief [3:2] \internal Reserved */
    Ifx_UReg_16Bit PWM:1;             /**< \brief [4:4] PWM Command Status (rh) */
    Ifx_UReg_16Bit reserved_5:5;      /**< \brief [9:5] \internal Reserved */
    Ifx_UReg_16Bit DBG:1;             /**< \brief [10:10] Debug Mode Active Flag (rh) */
    Ifx_UReg_16Bit reserved_11:4;     /**< \brief [14:11] \internal Reserved */
    Ifx_UReg_16Bit reserved_15:1;     /**< \brief [15:15] \internal Reserved */
} Ifx_1EDI2010AS_SSTAT_Bits;

/** \brief Secondary Second Status Register */
typedef struct _Ifx_1EDI2010AS_SSTAT2_Bits
{
    Ifx_UReg_16Bit P:1;               /**< \brief [0:0] Parity Bit (rh) */
    Ifx_UReg_16Bit LMI:1;             /**< \brief [1:1] Last Message Invalid Notification (rh) */
    Ifx_UReg_16Bit reserved_2:2;      /**< \brief [3:2] \internal Reserved */
    Ifx_UReg_16Bit DSATC:1;           /**< \brief [4:4] DESAT Comparator Result (rh) */
    Ifx_UReg_16Bit OCPC:1;            /**< \brief [5:5] OCP Comparator Result (rh) */
    Ifx_UReg_16Bit UVLO2M:1;          /**< \brief [6:6] UVLO2 Event (rh) */
    Ifx_UReg_16Bit DIO2L:1;           /**< \brief [7:7] DIO2 Pin Level (rh) */
    Ifx_UReg_16Bit DACL:1;            /**< \brief [8:8] DACLP Pin outpout level (rh) */
    Ifx_UReg_16Bit reserved_9:7;      /**< \brief [15:9] \internal Reserved */
} Ifx_1EDI2010AS_SSTAT2_Bits;

/** \brief Secondary Safe TTOFF Configuration Register */
typedef struct _Ifx_1EDI2010AS_SSTTOF_Bits
{
    Ifx_UReg_16Bit P:1;               /**< \brief [0:0] Parity Bit (rh) */
    Ifx_UReg_16Bit LMI:1;             /**< \brief [1:1] Last Message Invalid Notification (rh) */
    Ifx_UReg_16Bit reserved_2:3;      /**< \brief [4:2] \internal Reserved */
    Ifx_UReg_16Bit GPS:3;             /**< \brief [7:5] Gate Safe TTOFF Plateau Voltage (rw) */
    Ifx_UReg_16Bit STVAL:8;           /**< \brief [15:8] Gate Safe TTOFF delay (rw) */
} Ifx_1EDI2010AS_SSTTOF_Bits;

/** \brief Secondary TTON Configuration Register */
typedef struct _Ifx_1EDI2010AS_STTON_Bits
{
    Ifx_UReg_16Bit P:1;               /**< \brief [0:0] Parity Bit (rh) */
    Ifx_UReg_16Bit LMI:1;             /**< \brief [1:1] Last Message Invalid Notification (rh) */
    Ifx_UReg_16Bit reserved_2:6;      /**< \brief [7:2] \internal Reserved */
    Ifx_UReg_16Bit TTONVAL:8;         /**< \brief [15:8] Gate TTON Delay (rw) */
} Ifx_1EDI2010AS_STTON_Bits;

/** \}  */
/******************************************************************************/
/******************************************************************************/
/** \addtogroup IfxLld_1edi2010as_Registers_union
 * \{   */
/** \brief Primary Configuration Register   */
typedef union
{
    Ifx_UReg_16Bit U;                 /**< \brief Unsigned access */
    Ifx_SReg_16Bit I;                 /**< \brief Signed access */
    Ifx_1EDI2010AS_PCFG_Bits B;       /**< \brief Bitfield access */
} Ifx_1EDI2010AS_PCFG;

/** \brief Primary Second Configuration Register   */
typedef union
{
    Ifx_UReg_16Bit U;                 /**< \brief Unsigned access */
    Ifx_SReg_16Bit I;                 /**< \brief Signed access */
    Ifx_1EDI2010AS_PCFG2_Bits B;      /**< \brief Bitfield access */
} Ifx_1EDI2010AS_PCFG2;

/** \brief Primary Clock Supervision Register   */
typedef union
{
    Ifx_UReg_16Bit U;                 /**< \brief Unsigned access */
    Ifx_SReg_16Bit I;                 /**< \brief Signed access */
    Ifx_1EDI2010AS_PCS_Bits B;        /**< \brief Bitfield access */
} Ifx_1EDI2010AS_PCS;

/** \brief Primary Control Register   */
typedef union
{
    Ifx_UReg_16Bit U;                 /**< \brief Unsigned access */
    Ifx_SReg_16Bit I;                 /**< \brief Signed access */
    Ifx_1EDI2010AS_PCTRL_Bits B;      /**< \brief Bitfield access */
} Ifx_1EDI2010AS_PCTRL;

/** \brief Primary Second Control Register   */
typedef union
{
    Ifx_UReg_16Bit U;                 /**< \brief Unsigned access */
    Ifx_SReg_16Bit I;                 /**< \brief Signed access */
    Ifx_1EDI2010AS_PCTRL2_Bits B;     /**< \brief Bitfield access */
} Ifx_1EDI2010AS_PCTRL2;

/** \brief Primary Error Register   */
typedef union
{
    Ifx_UReg_16Bit U;                 /**< \brief Unsigned access */
    Ifx_SReg_16Bit I;                 /**< \brief Signed access */
    Ifx_1EDI2010AS_PER_Bits B;        /**< \brief Bitfield access */
} Ifx_1EDI2010AS_PER;

/** \brief Primary ID Register   */
typedef union
{
    Ifx_UReg_16Bit U;                 /**< \brief Unsigned access */
    Ifx_SReg_16Bit I;                 /**< \brief Signed access */
    Ifx_1EDI2010AS_PID_Bits B;        /**< \brief Bitfield access */
} Ifx_1EDI2010AS_PID;

/** \brief Primary Pin Status Register   */
typedef union
{
    Ifx_UReg_16Bit U;                 /**< \brief Unsigned access */
    Ifx_SReg_16Bit I;                 /**< \brief Signed access */
    Ifx_1EDI2010AS_PPIN_Bits B;       /**< \brief Bitfield access */
} Ifx_1EDI2010AS_PPIN;

/** \brief Primary Read/Write Register   */
typedef union
{
    Ifx_UReg_16Bit U;                 /**< \brief Unsigned access */
    Ifx_SReg_16Bit I;                 /**< \brief Signed access */
    Ifx_1EDI2010AS_PRW_Bits B;        /**< \brief Bitfield access */
} Ifx_1EDI2010AS_PRW;

/** \brief Primary Supervision Function Control Register   */
typedef union
{
    Ifx_UReg_16Bit U;                 /**< \brief Unsigned access */
    Ifx_SReg_16Bit I;                 /**< \brief Signed access */
    Ifx_1EDI2010AS_PSCR_Bits B;       /**< \brief Bitfield access */
} Ifx_1EDI2010AS_PSCR;

/** \brief Primary Status Register   */
typedef union
{
    Ifx_UReg_16Bit U;                 /**< \brief Unsigned access */
    Ifx_SReg_16Bit I;                 /**< \brief Signed access */
    Ifx_1EDI2010AS_PSTAT_Bits B;      /**< \brief Bitfield access */
} Ifx_1EDI2010AS_PSTAT;

/** \brief Primary Second Status Register   */
typedef union
{
    Ifx_UReg_16Bit U;                 /**< \brief Unsigned access */
    Ifx_SReg_16Bit I;                 /**< \brief Signed access */
    Ifx_1EDI2010AS_PSTAT2_Bits B;     /**< \brief Bitfield access */
} Ifx_1EDI2010AS_PSTAT2;

/** \brief Secondary ADC Result Register   */
typedef union
{
    Ifx_UReg_16Bit U;                 /**< \brief Unsigned access */
    Ifx_SReg_16Bit I;                 /**< \brief Signed access */
    Ifx_1EDI2010AS_SADC_Bits B;       /**< \brief Bitfield access */
} Ifx_1EDI2010AS_SADC;

/** \brief Secondary ADC Boundary Register   */
typedef union
{
    Ifx_UReg_16Bit U;                 /**< \brief Unsigned access */
    Ifx_SReg_16Bit I;                 /**< \brief Signed access */
    Ifx_1EDI2010AS_SBC_Bits B;        /**< \brief Bitfield access */
} Ifx_1EDI2010AS_SBC;

/** \brief Secondary Configuration Register   */
typedef union
{
    Ifx_UReg_16Bit U;                 /**< \brief Unsigned access */
    Ifx_SReg_16Bit I;                 /**< \brief Signed access */
    Ifx_1EDI2010AS_SCFG_Bits B;       /**< \brief Bitfield access */
} Ifx_1EDI2010AS_SCFG;

/** \brief Secondary Second Configuration Register   */
typedef union
{
    Ifx_UReg_16Bit U;                 /**< \brief Unsigned access */
    Ifx_SReg_16Bit I;                 /**< \brief Signed access */
    Ifx_1EDI2010AS_SCFG2_Bits B;      /**< \brief Bitfield access */
} Ifx_1EDI2010AS_SCFG2;

/** \brief Secondary Clock Supervision Register   */
typedef union
{
    Ifx_UReg_16Bit U;                 /**< \brief Unsigned access */
    Ifx_SReg_16Bit I;                 /**< \brief Signed access */
    Ifx_1EDI2010AS_SCS_Bits B;        /**< \brief Bitfield access */
} Ifx_1EDI2010AS_SCS;

/** \brief Secondary DESAT Blanking Time Register   */
typedef union
{
    Ifx_UReg_16Bit U;                 /**< \brief Unsigned access */
    Ifx_SReg_16Bit I;                 /**< \brief Signed access */
    Ifx_1EDI2010AS_SDESAT_Bits B;     /**< \brief Bitfield access */
} Ifx_1EDI2010AS_SDESAT;

/** \brief Secondary Error Register   */
typedef union
{
    Ifx_UReg_16Bit U;                 /**< \brief Unsigned access */
    Ifx_SReg_16Bit I;                 /**< \brief Signed access */
    Ifx_1EDI2010AS_SER_Bits B;        /**< \brief Bitfield access */
} Ifx_1EDI2010AS_SER;

/** \brief Secondary ID Register   */
typedef union
{
    Ifx_UReg_16Bit U;                 /**< \brief Unsigned access */
    Ifx_SReg_16Bit I;                 /**< \brief Signed access */
    Ifx_1EDI2010AS_SID_Bits B;        /**< \brief Bitfield access */
} Ifx_1EDI2010AS_SID;

/** \brief Secondary OCP Blanking Time Register   */
typedef union
{
    Ifx_UReg_16Bit U;                 /**< \brief Unsigned access */
    Ifx_SReg_16Bit I;                 /**< \brief Signed access */
    Ifx_1EDI2010AS_SOCP_Bits B;       /**< \brief Bitfield access */
} Ifx_1EDI2010AS_SOCP;

/** \brief Secondary Regular TTOFF Configuration Register   */
typedef union
{
    Ifx_UReg_16Bit U;                 /**< \brief Unsigned access */
    Ifx_SReg_16Bit I;                 /**< \brief Signed access */
    Ifx_1EDI2010AS_SRTTOF_Bits B;     /**< \brief Bitfield access */
} Ifx_1EDI2010AS_SRTTOF;

/** \brief Secondary Supervision Function Control Register   */
typedef union
{
    Ifx_UReg_16Bit U;                 /**< \brief Unsigned access */
    Ifx_SReg_16Bit I;                 /**< \brief Signed access */
    Ifx_1EDI2010AS_SSCR_Bits B;       /**< \brief Bitfield access */
} Ifx_1EDI2010AS_SSCR;

/** \brief Secondary Status Register   */
typedef union
{
    Ifx_UReg_16Bit U;                 /**< \brief Unsigned access */
    Ifx_SReg_16Bit I;                 /**< \brief Signed access */
    Ifx_1EDI2010AS_SSTAT_Bits B;      /**< \brief Bitfield access */
} Ifx_1EDI2010AS_SSTAT;

/** \brief Secondary Second Status Register   */
typedef union
{
    Ifx_UReg_16Bit U;                 /**< \brief Unsigned access */
    Ifx_SReg_16Bit I;                 /**< \brief Signed access */
    Ifx_1EDI2010AS_SSTAT2_Bits B;     /**< \brief Bitfield access */
} Ifx_1EDI2010AS_SSTAT2;

/** \brief Secondary Safe TTOFF Configuration Register   */
typedef union
{
    Ifx_UReg_16Bit U;                 /**< \brief Unsigned access */
    Ifx_SReg_16Bit I;                 /**< \brief Signed access */
    Ifx_1EDI2010AS_SSTTOF_Bits B;     /**< \brief Bitfield access */
} Ifx_1EDI2010AS_SSTTOF;

/** \brief Secondary TTON Configuration Register   */
typedef union
{
    Ifx_UReg_16Bit U;                 /**< \brief Unsigned access */
    Ifx_SReg_16Bit I;                 /**< \brief Signed access */
    Ifx_1EDI2010AS_STTON_Bits B;      /**< \brief Bitfield access */
} Ifx_1EDI2010AS_STTON;

/** \}  */

/******************************************************************************/
/** \addtogroup IfxLld_1edi2010as_Registers_struct
 * \{  */
/******************************************************************************/
/** \name Object L0
 * \{  */

/** \brief 1EDI2010AS object */
typedef volatile struct _Ifx_1EDI2010AS
{
       Ifx_1EDI2010AS_PID                  PID;                    /**< \brief 0, Primary ID Register*/
       Ifx_1EDI2010AS_PSTAT                PSTAT;                  /**< \brief 1, Primary Status Register*/
       Ifx_1EDI2010AS_PSTAT2               PSTAT2;                 /**< \brief 2, Primary Second Status Register*/
       Ifx_1EDI2010AS_PER                  PER;                    /**< \brief 3, Primary Error Register*/
       Ifx_1EDI2010AS_PCFG                 PCFG;                   /**< \brief 4, Primary Configuration Register*/
       Ifx_1EDI2010AS_PCFG2                PCFG2;                  /**< \brief 5, Primary Second Configuration Register*/
       Ifx_1EDI2010AS_PCTRL                PCTRL;                  /**< \brief 6, Primary Control Register*/
       Ifx_1EDI2010AS_PCTRL2               PCTRL2;                 /**< \brief 7, Primary Second Control Register*/
       Ifx_1EDI2010AS_PSCR                 PSCR;                   /**< \brief 8, Primary Supervision Function Control Register*/
       Ifx_1EDI2010AS_PRW                  PRW;                    /**< \brief 9, Primary Read/Write Register*/
       Ifx_1EDI2010AS_PPIN                 PPIN;                   /**< \brief A, Primary Pin Status Register*/
       Ifx_1EDI2010AS_PCS                  PCS;                    /**< \brief B, Primary Clock Supervision Register*/
       Ifx_UReg_16Bit                      reserved_C[4];          /**< \brief C, \internal Reserved */
       Ifx_1EDI2010AS_SID                  SID;                    /**< \brief 10, Secondary ID Register*/
       Ifx_1EDI2010AS_SSTAT                SSTAT;                  /**< \brief 11, Secondary Status Register*/
       Ifx_1EDI2010AS_SSTAT2               SSTAT2;                 /**< \brief 12, Secondary Second Status Register*/
       Ifx_1EDI2010AS_SER                  SER;                    /**< \brief 13, Secondary Error Register*/
       Ifx_1EDI2010AS_SCFG                 SCFG;                   /**< \brief 14, Secondary Configuration Register*/
       Ifx_1EDI2010AS_SCFG2                SCFG2;                  /**< \brief 15, Secondary Second Configuration Register*/
       Ifx_UReg_16Bit                      reserved_16[1];         /**< \brief 16, \internal Reserved */
       Ifx_1EDI2010AS_SSCR                 SSCR;                   /**< \brief 17, Secondary Supervision Function Control Register*/
       Ifx_1EDI2010AS_SDESAT               SDESAT;                 /**< \brief 18, Secondary DESAT Blanking Time Register*/
       Ifx_1EDI2010AS_SOCP                 SOCP;                   /**< \brief 19, Secondary OCP Blanking Time Register*/
       Ifx_1EDI2010AS_SRTTOF               SRTTOF;                 /**< \brief 1A, Secondary Regular TTOFF Configuration Register*/
       Ifx_1EDI2010AS_SSTTOF               SSTTOF;                 /**< \brief 1B, Secondary Safe TTOFF Configuration Register*/
       Ifx_1EDI2010AS_STTON                STTON;                  /**< \brief 1C, Secondary TTON Configuration Register*/
       Ifx_1EDI2010AS_SADC                 SADC;                   /**< \brief 1D, Secondary ADC Result Register*/
       Ifx_1EDI2010AS_SBC                  SBC;                    /**< \brief 1E, Secondary ADC Boundary Register*/
       Ifx_1EDI2010AS_SCS                  SCS;                    /**< \brief 1F, Secondary Clock Supervision Register*/
} Ifx_1EDI2010AS;

/** \}  */
/******************************************************************************/
/** \}  */
/******************************************************************************/
/******************************************************************************/
#endif /* IFX1EDI2010AS_REGDEF_H */
