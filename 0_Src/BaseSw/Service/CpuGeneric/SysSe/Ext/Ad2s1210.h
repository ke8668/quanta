/**
 * \file Ad2s1210.h
 * \brief Ad2s1210 resolver interface.
 * \ingroup library_srvsw_sysse_ext_ad2s1210
 *
 *
 *
 * \version disabled
 * \copyright Copyright (c) 2013 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * \defgroup library_srvsw_sysse_ext_ad2s1210 AD2S1210 resolver interface
 *
 * Serial configuration:
 *	- spiChannelConfig.base.mode.autoCS         = 0;
 *	- spiChannelConfig.base.mode.dataWidth      = 8; Uses 8 bit config because of the configuration mode, could be optimized for data and config if two channel are used
 *	- spiChannelConfig.base.mode.csActiveLevel  = Ifx_ActiveState_low;
 *
 * \ingroup library_srvsw_sysse_ext
 *
 */
#ifndef AD2S1210_H
#define AD2S1210_H

//________________________________________________________________________________________
// INCLUDES

#include "If/SpiIf.h"
#include "StdIf/IfxStdIf_Pos.h"
#include "SysSe/Bsp/Bsp.h"
#include "SysSe/Math/Ifx_LowPassPt1F32.h"

//________________________________________________________________________________________
// CONFIGURATION MACROS
//#define AD2S1210_DEBUG (1)

#ifndef AD2S1210_EXT_BUS_AUTO_SELECT
/** \brief Define whether configuration select signal is set directly through EBU: 1=yes, 0=no */
#define AD2S1210_EXT_BUS_AUTO_SELECT (0)
#endif

#ifndef AD2S1210_RD_TO_DATA_WAIT
/** \brief Specifies the wait cycles inserted between the RD\\and the Data read.
 * Series of __nop() should be defined */
#define AD2S1210_RD_TO_DATA_WAIT     wait(TimeConst_10ns)
#endif

//________________________________________________________________________________________
// HELPER MACROS

/** \brief Input clock frequency in Hz */
#ifndef AD2S1210_CLKIN
#    define AD2S1210_CLKIN               (8192000)
#endif

#if AD2S1210_CLKIN == 8192000
/** \brief full scale speed in rad/s */
#define AD2S1210_SPEED_FULL_SCALE_10BITS (2500.0 * 2 * IFX_PI)
#define AD2S1210_SPEED_FULL_SCALE_12BITS (1000.0 * 2 * IFX_PI)
#define AD2S1210_SPEED_FULL_SCALE_14BITS (500.0 * 2 * IFX_PI)
#define AD2S1210_SPEED_FULL_SCALE_16BITS (125.0 * 2 * IFX_PI)
#endif

/**<
 * Settings with \ref AD2S1210_CLKIN = 8.192 MHz:
 *
 * Resolution | Max speed | Min. code | Max. code | Note
 * ---------- | --------- | --------- | --------- | ----------------
 * 16-bits    |  +-125rps | 0x8000    | 0x7FFF    |
 * 14-bits    |  +-500rps | 0x8000    | 0x7FFB    | D1..0 is ignored
 * 12-bits    | +-1000rps | 0x8000    | 0x7FF8    | D3..0 is ignored
 * 10-bits    | +-2500rps | 0x8000    | 0x7FC0    | D5..0 is ignored
 */

//________________________________________________________________________________________
// ENUMERATIONS

/** \brief Resolver state flags
 */
typedef enum
{
    Ad2s1210_State_ready,           /**< \brief The resolver is ready for use */
    Ad2s1210_State_startUp,         /**< \brief The resolver is starting up */
    Ad2s1210_State_reset            /**< \brief The resolver is being reset */
} Ad2s1210_State;

/** \brief Interfacing modes */
typedef enum
{
    Ad2s1210_Mode_serial,
    Ad2s1210_Mode_parallel,
    Ad2s1210_Mode_extbus
} Ad2s1210_Mode;

/** \brief Chip operation modes */
typedef enum
{
    Ad2s1210_OperationMode_normalPosition = 0,
    Ad2s1210_OperationMode_normalVelocity = 2,
    Ad2s1210_OperationMode_configuration  = 3
} Ad2s1210_OperationMode;

/** \brief Supported operations */
typedef enum
{
    Ad2s1210_Resolution_10Bits = 0,
    Ad2s1210_Resolution_14Bits = 1,
    Ad2s1210_Resolution_12Bits = 2,
    Ad2s1210_Resolution_16Bits = 3
} Ad2s1210_Resolution;

/** \brief Lock ranges */
typedef enum
{
    Ad2s1210_PhaseLockRange_44Degree  = 1,
    Ad2s1210_PhaseLockRange_360Degree = 0
} Ad2s1210_PhaseLockRange;

/** \brief Control register */
typedef union
{
    struct
    {
        uint8 resolution : 2;
        uint8 encoderResolution : 2;
        uint8 hysteresis : 1;
        uint8 phaseLockRange : 1;
        uint8 : 1;
        uint8 controlBit : 1;
    }     bitfields;
    uint8 value;
} Ad2s1210_controlReg;

/** \brief Fault register */
typedef union
{
    struct
    {
        uint8 inputsClipped : 1;
        uint8 los : 1;
        uint8 dosOverrange : 1;
        uint8 dosMismatch : 1;
        uint8 lot : 1;
        uint8 trackingRate : 1;
        uint8 phaseLockRange : 1;
        uint8 configurationParity : 1;
    }     bitfields;
    uint8 value;
} Ad2s1210_FaultReg;

//________________________________________________________________________________________
// DATA STRUCTURES

/** \brief Common structure */
typedef struct
{
    IfxPort_Pin A0;         /**< \brief A0 pin */
    IfxPort_Pin A1;         /**< \brief A1 pin */
    IfxPort_Pin RESET;      /**< \brief Reset pin */
    IfxPort_Pin RES0;       /**< \brief RES0 pin */
    IfxPort_Pin RES1;       /**< \brief RES1 pin */
} Ad2s1210_Common;

/** \brief Serial interface runtime structure */
typedef struct
{
    SpiIf_Ch   *channel;
    IfxPort_Pin CSn;        /**< \brief CS\\pin */
    IfxPort_Pin WRn;        /**< \brief WR\\FSYNCn pin */
} Ad2s1210_Serial;

/** \brief Parallel port interface runtime structure */
typedef struct
{
    IfxPort_Pin CSn;        /**< \brief CS\\pin */
    IfxPort_Pin WRn;        /**< \brief WR\\FSYNCn pin */
    IfxPort_Pin RDn;        /**< \brief RDn pin */
    IfxPort_Pin DB0;        /**< \brief DB0 pin. DB0 to DB15 must be adjacent pins, with DB0 having the lowest pin index */
    IfxPort_Pin LOT;        /**< \brief LOT pin */
    IfxPort_Pin DOS;        /**< \brief DOS pin */
} Ad2s1210_Par;

/** \brief External bus interface runtime structure */
typedef struct
{
    void       *data;       /**< \brief Resolver data register address for position (and speed when AD2S1210_EXT_BUS_AUTO_SELECT == 1)*/
    void       *dataVel;    /**< \brief Resolver data register address for speed (optional, only when AD2S1210_EXT_BUS_AUTO_SELECT == 0) */
    IfxPort_Pin LOT;        /**< \brief LOT pin */
    IfxPort_Pin DOS;        /**< \brief DOS pin */
} Ad2s1210_Ebu;

/** \brief Position sensor diagnostic/statistic */
typedef union
{
    uint32 byIndex[5];
    struct
    {
        uint32 signalLoss;
        uint32 signalDegradation;
        uint32 trackingLoss;
        uint32 noError;
        uint32 commError;
    } byType;
} Ad2s1210_Diag;

/** Mode specific run-time data */
typedef struct
{
    Ad2s1210_Mode   selected;   /**< \brief Resolver chip selected mode*/
    Ad2s1210_Common common;
    union
    {
        Ad2s1210_Serial serial;
        /* TODO add configuration SSC channel */
        Ad2s1210_Par    parallel;
        Ad2s1210_Ebu    extbus;
    } i;
} Ad2s1210_ModeRT;

/** Register index */
typedef enum
{
    Ad2s1200_RegIdx_positionH,
    Ad2s1200_RegIdx_positionL,
    Ad2s1200_RegIdx_velocityH,
    Ad2s1200_RegIdx_velocityL,
    Ad2s1200_RegIdx_losThreshold,
    Ad2s1200_RegIdx_dosOverrangeThreshold,
    Ad2s1200_RegIdx_dosMismatchThreshold,
    Ad2s1200_RegIdx_dosResetMaxThreshold,
    Ad2s1200_RegIdx_dosResetMinThreshold,
    Ad2s1200_RegIdx_lotMaxThreshold,
    Ad2s1200_RegIdx_lotMinThreshold,
    Ad2s1200_RegIdx_excitationFrequency,
    Ad2s1200_RegIdx_control,
    Ad2s1200_RegIdx_softReset,
    Ad2s1200_RegIdx_fault,
    AD2S1210_REGS_COUNT
} Ad2s1200_RegIdx;

/** Register file */
typedef union
{
    uint8 A[AD2S1210_REGS_COUNT];
    struct
    {
        uint8 positionH;
        uint8 positionL;
        uint8 velocityH;
        uint8 velocityL;
        uint8 losThreshold;
        uint8 dosOverrangeThreshold;
        uint8 dosMismatchThreshold;
        uint8 dosResetMaxThreshold;
        uint8 dosResetMinThreshold;
        uint8 lotMaxThreshold;
        uint8 lotMinThreshold;
        uint8 excitationFrequency;
        uint8 control;
        uint8 softReset;
        uint8 fault;
    } I;
} Ad2s1210_Regs;

/** \brief Alias to resolver object definition */
typedef struct Ad2s1210_s Ad2s1210;

/** \brief Resolver object definition */
struct Ad2s1210_s
{
    /* status */
    sint32              position;            /**< \brief raw position in ticks. \note: the value already contains the offset */
    float32             speed;               /**< \brief mechanical speed in rad/s */
    sint32              turn;                /**< \brief number of mechanical turns */
    IfxStdIf_Pos_Dir    direction;           /**< \brief rotation direction */
    IfxStdIf_Pos_Status stdifStatus;         /**< \brief error code (0 = no error) */
#if AD2S1210_DEBUG
    Ad2s1210_Diag       diag;                /**< \brief diagnostic and statistic */
#endif
    /* configuration */
    sint32             offset;               /**< \brief raw position offset */
    boolean            reversed;             /**< \brief reverse direction */
    uint16             periodPerRotation;    /**< \brief 'electrical' periods per mechanical rotation */
    sint32             resolution;           /**< \brief resolution of this position sensor interface */
    float32            speedConstPulseCount; /**< \brief constant for calculating mechanical speed (in rad/s) from raw speed */

    sint16             speedCounter;         /**< \brief Temporary counter for internal speed calculation */
    sint16             speedReload;          /**< \brief Refresh factor for the speed. each call to Ad2s1210_update() increase the counter SpeedCounter */
    Ad2s1210_State     state;                /**< \brief Resolver State */
    Ad2s1210_ModeRT    modes;                /**< \brief */
    Ad2s1210_FaultReg  status;               /**< \brief internal status */
    uint8              (*writeData)(Ad2s1210 *driver, uint8 data);
    uint8              (*readData)(Ad2s1210 *driver);
    uint8              dataBits;             /**< \brief number of data bits */
    Ifx_LowPassPt1F32 speedLpf;             /**< \brief PT1 low-pass filter of this input */
    boolean            speedFilterEnabled;   /**< \brief Specifies whether the filter is enabled */
};

/** \brief Mode configuration */
typedef struct
{
    Ad2s1210_Mode       selected;      /**< \brief Resolver selected mode*/
    Ad2s1210_Common     common;
    const Ad2s1210_Par *parallel;
    const Ad2s1210_Ebu *extbus;
    Ad2s1210_Serial     serial; /**< \brief Driver dependent channel structure. E.g. if IfxQspi_Spi, then put IfxQspi_Spi_Ch* as value to this field */
} Ad2s1210_ModeConfig;

/** \brief Resolver configuration definition */
typedef struct
{
    sint32              offset;                    /**< \brief Specified the resolver offset. */
    uint16              periodPerRotation;         /**< \brief Number of period per mechanical rotation */
    boolean             reversed;                  /**< \brief if TRUE, the direction is reversed */
    sint32              frequency;                 /**< \brief Resolver excitation frequency */
    sint32              updateFrequency;           /**< \brief Update frequency of the sensor position in Hz (call to Ad2s1210_*_update)  */
    uint16              speedUpdateFactor;         /**< \brief Update factor for the speed relative to the position update  */
    Ad2s1210_ModeConfig modes;
    Ad2s1210_Resolution resolution;
    float32             speedFilerCutOffFrequency; /**< \brief speed cut off frequency in Hz. a value of <= 0 disable the filter;*/
} Ad2s1210_Config;

//________________________________________________________________________________________
// FUNCTION PROTOTYPES

/** \addtogroup library_srvsw_sysse_ext_ad2s1210
 * \{ */

/** \name Initialisation functions
 * Initialisation shall be done by calling Ad2s1210_init(), e.g.:
 * \code
 * extern Ad2s1210 driverData;
 * extern const Ad2s1210_Config driverConfig;
 * Ad2s1210_init(&driverData, &driverConfig);
 * Ad2s1210_reset(&driverData);
 * \endcode
 *
 * Application (e.g. running inside motor PWM interrupt context) should use \ref library_srvsw_stdif_posif
 * functions for updating and accessing the actual results, e.g:
 * \code
 * extern Ad2s1210 driverData;
 * IfxStdIf_Pos* handle = &driverData.base;
 * PosIf_update(handle);
 * elAngle = PosIf_getElAngle(handle);
 * elSpeed = PosIf_getElSpeed(handle);
 * \endcode
 * Prototypes:
 * \{ */
IFX_EXTERN boolean        Ad2s1210_init(Ad2s1210 *driver, const Ad2s1210_Config *config);
void                      Ad2s1210_initConfig(Ad2s1210_Config *config);
IFX_EXTERN Ad2s1210_State Ad2s1210_enable(Ad2s1210 *driver, WaitTimeFunction waitFunction);
IFX_EXTERN void           Ad2s1210_reset(Ad2s1210 *driver);
IFX_EXTERN void           Ad2s1210_resetFaults(Ad2s1210 *driver);
IFX_EXTERN sint32         Ad2s1210_setFrequency(Ad2s1210 *driver, sint32 frequency);
boolean                   Ad2s1210_stdIfPosInit(IfxStdIf_Pos *stdif, Ad2s1210 *driver);

float32                 Ad2s1210_getAbsolutePosition(Ad2s1210 *driver);
sint32                  Ad2s1210_getOffset(Ad2s1210 *driver);
float32                 Ad2s1210_getRefreshPeriod(Ad2s1210 *driver);
IfxStdIf_Pos_SensorType Ad2s1210_getSensorType(Ad2s1210 *driver);
float32                 Ad2s1210_getSpeed(Ad2s1210 *driver);
void                    Ad2s1210_setRefreshPeriod(Ad2s1210 *driver, float32 updatePeriod);
void                    Ad2s1210_setOffset(Ad2s1210 *driver, sint32 offset);
sint32 Ad2s1210_getResolutionBit(Ad2s1210_Resolution resolution);
Ad2s1210_Resolution Ad2s1210_getResolutionCode(sint32 resolution);
uint8 Ad2s1210_getResolutionDataBits(Ad2s1210_Resolution resolution);

/** \} */
/** \name Status functions
 * Additional functions not covered by \ref library_srvsw_stdif_posif are provided.
 *
 * Prototypes:
 * \{ */
IFX_INLINE boolean Ad2s1210_isLossOfSignal(Ad2s1210 *driver);
IFX_INLINE boolean Ad2s1210_isDegradationOfSignal(Ad2s1210 *driver);
IFX_INLINE boolean Ad2s1210_isLossOfTracking(Ad2s1210 *driver);
IFX_INLINE sint32  Ad2s1210_getRawPosition(Ad2s1210 *driver);
IFX_INLINE float32 Ad2s1210_getPosition(Ad2s1210 *driver);
IFX_INLINE sint32  Ad2s1210_getTurn(Ad2s1210 *driver);
IFX_INLINE boolean Ad2s1210_reverse(Ad2s1210 *driver, boolean reverse);
/** \brief \see IfxStdIf_Pos_GetDirection
 * \param driver driver handle
 * \return direction
 */
IFX_EXTERN IfxStdIf_Pos_Dir Ad2s1210_getDirection(Ad2s1210 *driver);

/** \brief \see IfxStdIf_Pos_GetFault
 * \param driver driver handle
 * \return Fault
 */
IFX_EXTERN IfxStdIf_Pos_Status Ad2s1210_getFault(Ad2s1210 *driver);

IFX_EXTERN void Ad2s1210_readAllRegs(Ad2s1210 *driver, Ad2s1210_Regs *regs);

IFX_EXTERN sint32            Ad2s1210_getResolution(Ad2s1210 *driver);
IFX_EXTERN sint32            Ad2s1210_readResolution(Ad2s1210 *driver);
IFX_EXTERN void              Ad2s1210_setEncoderResolution(Ad2s1210 *driver, Ad2s1210_Resolution resolution);
IFX_EXTERN void              Ad2s1210_setPhaseLockRange(Ad2s1210 *driver, Ad2s1210_PhaseLockRange phaseLockRange);
IFX_EXTERN void              Ad2s1210_setHysteresis(Ad2s1210 *driver, boolean enabled);
IFX_EXTERN uint16            Ad2s1210_readPosition(Ad2s1210 *driver);
IFX_EXTERN sint16            Ad2s1210_readVelocity(Ad2s1210 *driver);
IFX_EXTERN void              Ad2s1210_setLosThreshold(Ad2s1210 *driver, float32 threshold);
IFX_EXTERN void              Ad2s1210_setDosOverrangeThreshold(Ad2s1210 *driver, float32 threshold);
IFX_EXTERN void              Ad2s1210_setDosMismatchThreshold(Ad2s1210 *driver, float32 threshold);
IFX_EXTERN void              Ad2s1210_setDosResetThreshold(Ad2s1210 *driver, float32 minThreshold, float32 maxThreshold);
IFX_EXTERN void              Ad2s1210_softwareReset(Ad2s1210 *driver);
IFX_EXTERN float32           Ad2s1210_getLosThreshold(Ad2s1210 *driver);
IFX_EXTERN float32           Ad2s1210_getDosOverrangeThreshold(Ad2s1210 *driver);
IFX_EXTERN float32           Ad2s1210_getDosMismatchThreshold(Ad2s1210 *driver);
IFX_EXTERN void              Ad2s1210_getDosResetThreshold(Ad2s1210 *driver, float32 *minThreshold, float32 *maxThreshold);
IFX_EXTERN Ad2s1210_FaultReg Ad2s1210_getFaults(Ad2s1210 *driver);
/** \} */

/** \} */

//________________________________________________________________________________________
// INLINE FUNCTION IMPLEMENTATIONS

/** \brief Return the resolver position is in Loss of Signal (LOS).
 * \param driver Specifies resolver object.
 * \return if TRUE, the resolver is in Loss of Signal (LOS)..
 */
IFX_INLINE boolean Ad2s1210_isLossOfSignal(Ad2s1210 *driver)
{
    return (driver->stdifStatus.B.signalLoss != 0) ? TRUE : FALSE;
}


/** \brief Return the resolver position is in Degradation of Signal (DOS).
 * \param driver Specifies resolver object. *
 * \return if TRUE, the resolver is in Degradation of Signal (DOS).
 */
IFX_INLINE boolean Ad2s1210_isDegradationOfSignal(Ad2s1210 *driver)
{
    return (driver->stdifStatus.B.signalDegradation != 0) ? TRUE : FALSE;
}


/** \brief Return the resolver position is  in loss of tracking (LOT)
 * \param driver Specifies resolver object.
 * \return if TRUE, the resolver is in loss of tracking (LOT).
 */
IFX_INLINE boolean Ad2s1210_isLossOfTracking(Ad2s1210 *driver)
{
    return (driver->stdifStatus.B.trackingLoss != 0) ? TRUE : FALSE;
}


/** \brief Return the resolver position in resolver ticks
 *
 * This function returns the resolver position in resolver ticks.
 * In order to get the actual position, the function Ad2s1210_update() must have been
 * called just before.
 *
 * \param driver Specifies resolver object.
 *
 * \return returns the resolver position in resolver ticks.
 */
IFX_INLINE sint32 Ad2s1210_getRawPosition(Ad2s1210 *driver)
{
    return driver->position;
}


/** \brief \see IfxStdIf_Pos_GetPeriodPerRotation
 * \param driver driver handle
 * \return Period per rotation
 */
IFX_EXTERN uint16 Ad2s1210_getPeriodPerRotation(Ad2s1210 *driver);

/** \brief Return the resolver position in rad
 *
 * This function returns the resolver position in rad.
 * In order to get the actual position, the function Ad2s1210_update() must have been
 * called just before.
 *
 * \param driver Specifies resolver object.
 * \return returns the resolver position in rad.
 */
IFX_INLINE float32 Ad2s1210_getPosition(Ad2s1210 *driver)
{
    return (float32)(driver->position * ((2 * IFX_PI) / driver->resolution));
}


/** \brief Return the resolver sensor turn count
 * \param driver Specifies resolver object.
 * \return returns the resolver sensor turn count.
 * \todo Implements Turn feature
 */
IFX_INLINE sint32 Ad2s1210_getTurn(Ad2s1210 *driver)
{
    return driver->turn;
}


/** \brief Set the resolver positive direction
 * \param driver Specifies resolver object.
 * \param reversed Specifies if the resolver direction should be reversed (TRUE) or not (FALSE).
 * \return None.
 */
IFX_INLINE boolean Ad2s1210_reverse(Ad2s1210 *driver, boolean reversed)
{
    driver->reversed = reversed;
    return reversed;
}


//------------------------------------------------------------------------------
#endif
