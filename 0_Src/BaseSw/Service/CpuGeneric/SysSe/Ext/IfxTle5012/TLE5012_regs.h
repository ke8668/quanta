/*
 * TLE5012_regs.h
 *
 *  Created on: 25-Feb-2010
 *      Author: nugrahad
 */

#ifndef TLE5012_REGS_H
#define TLE5012_REGS_H

#include "Platform_Types.h"
#include "Compilers.h"

typedef union
{
	struct
	{
		uint16 S_RST       : 1;
		uint16 S_WD        : 1;
		uint16 S_VR        : 1;
		uint16 S_FUSE      : 1;
		uint16 S_DSPU      : 1;
		uint16 S_OV        : 1;
		uint16 S_XYOL      : 1;
		uint16 S_MAGOL     : 1;
		uint16 reserved    : 1;
		uint16 S_ADCT      : 1;
		uint16 S_ROM       : 1;
		uint16 NO_GMR_XY   : 1;
		uint16 NO_GMR_A    : 1;
		uint16 S_NR        : 2;
		uint16 RD_ST       : 1;
	} B;
	uint16 U;
} Ifx_TLE5012_Stat;

typedef union
{
	struct
	{
	    uint16 AS_RST      : 1;
	    uint16 AS_WD       : 1;
	    uint16 AS_VR       : 1;
	    uint16 AS_FUSE     : 1;
	    uint16 AS_DSPU     : 1;
	    uint16 AS_OV       : 1;
	    uint16 AS_VEC_XY   : 1;
	    uint16 AS_VEC_MAG  : 1;
	    uint16 reserved    : 1;
	    uint16 AS_ADCT     : 1;
	    uint16 reserved_2  : 6;
	} B;
	uint16 U;
} Ifx_TLE5012_AcStat;

typedef union
{
    struct
    {
        uint16 ANG_VAL     : 15; /* 0x4000 = -180deg, 0x0 = 0deg, 0x3fff = 179.99deg */
        uint16 RD_AV       : 1;
    } B;
    uint16 U;
} Ifx_TLE5012_AVal;

typedef union
{
    struct
    {
        uint16 ANG_SPD     : 15;
        uint16 RD_AS       : 1;
    } B;
    uint16 U;
} Ifx_TLE5012_ASpd;

typedef union
{
    struct
    {
        uint16 REVOL       : 9; /* signed 9-bit, number of revolution */
        uint16 FCNT        : 6; /* unsigned 6-bit, count number of new angle values */
        uint16 RD_REV      : 1;
    } B;
    uint16 U;
} Ifx_TLE5012_ARev;

typedef union
{
    struct
    {
        uint16 reserved    : 9;
        uint16 FSYNC       : 7;
    } B;
    uint16 U;
} Ifx_TLE5012_FSync;

typedef union
{
    struct
    {
        uint16 IIF_MOD     : 2; /* 0: IIF disabled, 1: A/B operation with Index on DATA (default) */
        uint16 DSPU_HOLD   : 1; /* 0: DSPU normal operation, 1: DSPU on hold */
        uint16 SSC_OD      : 1; /* 0: push-pull, 1: open-drain (default) */
        uint16 CLK_SEL     : 1; /* 0: internal oscillator (default) , 1: external 4MHz */
        uint16 reserved    : 9;
        uint16 FIR_MD      : 2; /* Filter Decimation Settings:
                            0=21.3us, 1=42.7us, 2=85.3us (default), 3=170.6 us */
    } B;
    uint16 U;
} Ifx_TLE5012_Mod_1;

typedef union
{
    struct
    {
        uint16 ADCTV_X     : 3; /* Test vector X: 0=0V, 1=+70%, 2=+100%, 3=+overflow
                                                 5=-70%, 6=-100%, 7=-overflow */
        uint16 ADCTV_Y     : 3; /* Test vector Y: 0=0V, 1=+70%, 2=+100%, 3=+overflow
                                                 5=-70%, 6=-100%, 7=-overflow */
        uint16 ADCTV_EN    : 1; /* ADC-test vector: 0=disabled, 1=enabled */
        uint16 reserved    : 3;
        uint16 FUSE_REL    : 1; /* Fuse reload: 0=disabled, 1=reload to DSPU at next cycle */
        uint16 reserved_2  : 3;
        uint16 FILT_INV    : 1; /* Filter inverted: 0=disabled, 1=enabled */
        uint16 FILT_PAR    : 1; /* Filter parallel: 0=disabled, 1=enabled */
    } B;
    uint16 U;
} Ifx_TLE5012_SIL;

typedef union
{
    struct
    {
        uint16 AUTOCAL     : 2;  /* Autocalibration mode: 0=none, 1=time mode, 2=angle mode 1, 3=angle mode 2 */
        uint16 PREDICT     : 1;  /* Prediction: 0=disabled, 1=enabled(default for TLE5012-E0318, TLE5012-E0742) */
        uint16 ANG_DIR     : 1;  /* Angle direction: 0=counterclockwise, 1=clockwise */
        uint16 ANG_RANGE   : 11; /* Angle range: 360deg x (2^7 / ANG_RANGE )
                             0x200 = 90deg
                             0x080 = 360deg (default) */
        uint16 reserved    : 1;
    } B;
    uint16 U;
} Ifx_TLE5012_Mod_2;

typedef union
{
    struct
    {
        uint16 PAD_DRV     : 2;  /* Pad-driver config */
        uint16 reserved    : 1;
        uint16 SPIKEF      : 1;  /* Spike filter: 0: disabled (default), 1: enabled */
        uint16 ANG_BASE    : 12; /* 0x800=-180deg, 0x000=0deg (default), 0x001=0.0879deg, 0x7FF=+179.912deg*/
    } B;
    uint16 U;
} Ifx_TLE5012_Mod_3;

typedef union
{
    struct
    {
        uint16 reserved    : 4;
        uint16 X_OFFSET    : 12; /* Offset correction X, default=0 */
    } B;
    uint16 U;
} Ifx_TLE5012_OffX;

typedef union
{
    struct
    {
        uint16 reserved    : 4;
        uint16 Y_OFFSET    : 12; /* Offset correction Y, default=0 */
    } B;
    uint16 U;
} Ifx_TLE5012_OffY;

typedef union
{
    struct
    {
        uint16 reserved    : 4;
        uint16 SYNCH       : 12; /* Amplitude synchronicity, +2047=112.494%,  0 = 100%, -2048=87.5%*/
    } B;
    uint16 U;
} Ifx_TLE5012_Synch;

typedef union
{
    struct
    {
        uint16 IFAB_HYST   : 2;  /* HSM hysteresis: 0=0deg, 1=0.09deg, 2=0.27deg, 3=0.625deg*/
        uint16 IFAB_OD     : 1;  /* IFA & IFB open-drain: 0=push-pull, 1=open-drain (default) */
        uint16 reserved    : 1;
        uint16 ORTHO       : 12; /* Orthogonality correction X and Y: +2047=11.2445deg, 0=0deg, -2048=-11.25deg */
    } B;
    uint16 U;
} Ifx_TLE5012_IFAB;

typedef union
{
    struct
    {
        uint16 IF_MD       : 3; /* Interface mode:
                            0 = SSC + IIF
                            1 = SSC + PWM
                            2 = SSC + HSM (default for TLE5012-E0318, TLE5012-E0742)
                            */
        uint16 IFAB_RES    : 2; /* IFAB resolution:
                            0 = 12-bit = 0.088deg (244Hz)
                            1 = 11-bit = 0.176deg (488Hz)
                            2 = 10-bit = 0.352deg (977Hz)
                            3 =  9-bit = 0.703deg(1953Hz) */
        uint16 HSM_PLP     : 3; /* Hall-switch polepairs: 0=2pp, 1=3pp, 2=4pp,3=6pp, 4=7pp, 5=8pp, 6=12pp, 7=16pp */
        uint16 reserved    : 1;
        uint16 TCO_X_T     : 7; /* Offset temperature coefficient for X-component */
    } B;
    uint16 U;
} Ifx_TLE5012_Mod_4;

typedef union
{
    struct
    {
        uint16 CRC_PAR     : 8; /* CRC of parameters from address 08 to 0F */
        uint16 reserved    : 1;
        uint16 TCO_Y_T     : 7; /* Offset temperature coefficient for Y */
    } B;
    uint16 U;
} Ifx_TLE5012_TCo_Y;

typedef union
{
    uint16 U;
} Ifx_TLE5012_ADC_X;

typedef union
{
    uint16 U;
} Ifx_TLE5012_ADC_Y;

typedef union
{
	uint16 A[18];
	struct
	{
		Ifx_TLE5012_Stat     STAT;       /* Status Register 00*/
		Ifx_TLE5012_AcStat   ACSTAT;     /* Activation Status Register 01*/
		Ifx_TLE5012_AVal     AVAL;       /* Angle Value Register 02*/
		Ifx_TLE5012_ASpd     ASPD;       /* Angle Speed Register 03*/
		Ifx_TLE5012_ARev     AREV;       /* Angle Revolution Register 04*/
		Ifx_TLE5012_FSync    FSYNC;      /* Frame Synchronization Register 05*/
		Ifx_TLE5012_Mod_1    MOD_1;      /* Interface Mode1 Register 06*/
		Ifx_TLE5012_SIL      SIL;        /* SIL Register 07*/
		Ifx_TLE5012_Mod_2    MOD_2;      /* Interface Mode2 Register 08*/
		Ifx_TLE5012_Mod_3    MOD_3;      /* Interface Mode3 Register 09*/
		Ifx_TLE5012_OffX     OFFX;       /* Offset X 0A*/
		Ifx_TLE5012_OffY     OFFY;       /* Offset Y 0B*/
		Ifx_TLE5012_Synch    SYNCH;      /* Synchronicity 0C*/
		Ifx_TLE5012_IFAB     IFAB;       /* IFAB Register 0D*/
		Ifx_TLE5012_Mod_4    MOD_4;      /* Interface Mode4 Register 0E*/
		Ifx_TLE5012_TCo_Y    TCO_Y;      /* Temperature Coeffizient Register 0F*/
		Ifx_TLE5012_ADC_X    ADC_X;      /* X-raw value 10*/
		Ifx_TLE5012_ADC_Y    ADC_Y;      /* Y-raw value 11*/
	} I;
} Ifx_TLE5012;

#endif /* TLE5012_REGS_H_ */
