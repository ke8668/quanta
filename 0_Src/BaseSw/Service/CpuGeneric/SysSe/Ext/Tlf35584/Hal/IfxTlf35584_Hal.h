/**
 * \file IfxTlf35584_Hal.h
 * \brief
 *
 * \copyright Copyright (c) 2015 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * This file may be used, copied, and distributed, with or without modification, provided
 * that all copyright notices are retained; that all modifications to this file are
 * prominently noted in the modified file; and that this paragraph is not modified.
 *
 * \defgroup IfxLld_Tlf35584_Hal Hardware abstraction layer
 * \ingroup IfxLld_Tlf35584
 *
 * This layer abstracts the SPI and port modules used to interact with the device.
 * An example is provided here using the AURIX iLLD, those files (IfxTlf35584_Hal.*) must be adapteed to the user environment.
 */

#ifndef IFXTLF35584_HAL_H
#define IFXTLF35584_HAL_H 1

#include "Ifx_Cfg.h"
#include "Platform_Types.h"
#include "Compilers.h"
#include "If/SpiIf.h"
#include "IfxPort.h"
#include "SysSe/Bsp/Bsp.h"

#ifndef IFX_CFG_TLF35584_DEBUG
#define IFX_CFG_TLF35584_DEBUG (0)
#endif

#ifndef IFX_CFG_TLF35584_DEBUG_SPI
#define IFX_CFG_TLF35584_DEBUG_SPI (0)
#endif

#ifndef TLF35584_ENDL
#define TLF35584_ENDL          "\r\n"
#endif

/** SPI object definition
 * IfxTlf35584_Spi is an handle used to identify the SPI used for communcation with the device
 */
typedef SpiIf_Ch IfxTlf35584_Spi;

/** Pin object definition
 * IfxTlf35584_Pin is a handle used to identify a digital input / output used for controlling the device
 */
typedef IfxPort_Pin IfxTlf35584_Pin;

/** Pin action definition
 *
 */
typedef enum
{
    IfxTlf35584_PinState_high = IfxPort_State_high,              /**< \brief Set the pin high */
    IfxTlf35584_PinState_low  = IfxPort_State_low,               /**< \brief Set the pin low */
} IfxTlf35584_PinState;

typedef Ifx_TickTime IfxTlf35584_Time;                           /**< \brief time type, this type should allows time definition below 1s */

#define IFXTLF35584_TIME_1S   ((IfxTlf35584_Time)TimeConst_1s)   /**< \brief value that represent a time of 1s, used by \ref IfxTlf35584_Hal_wait() */
#define IFXTLF35584_TIME_10MS ((IfxTlf35584_Time)TimeConst_10ms) /**< \brief value that represent a time of 10ms, used by \ref SpiIf_waitWithTimeout() */

#define IFXTLF35584_HAL_MIN(X,Y)                     ( ((X) < (Y)) ? (X) : (Y) )
#define IFXTLF35584_HAL_MAX(X,Y)                     ( ((X) > (Y)) ? (X) : (Y) )

/** Hal configuration
 *
 */
typedef struct
{
    IfxTlf35584_Spi *channel;
    IfxTlf35584_Pin  wdi;       /**< \brief WDI pin */
}IfxTlf35584_Hal_Config;

/* Pure virtual function. to be implemented outside of the driver */

/** \addtogroup IfxLld_Tlf35584_Hal
 * Those API must be adapted to the mircocontroller / software framework
 * \{ */

/** Perform an SPI data exchange operation (send/receive).
 *
 * This function send the data pointer by src and store the data received to dest.
 * The number of data element to be send/received is specified by length.
 *
 * \param spiChannel SPI channel handle used for data transmission
 * \param src Pointer to the start of transmit data buffer
 * \param dest Pointer to the start of received data buffer
 * \param length Number of SPI data element to be transfered
 *
 * \return TRUE in case of success else FALSE
 *
 * \note The function body has to be filled in depending on the microcontroller and SPI driver used. An example is provided for the iLLD only.
 */
boolean IfxTlf35584_Hal_exchangeSpi(IfxTlf35584_Spi *spiChannel, const void *src, void *dest, uint8 length);

/** Microcontroller hardware initialization
 *
 * This function initializes the following:
 * - WDI port as output, low level
 *
 * \param config Configuration data
 * \return Returns TRUE in case of success, else FALSE
 */
boolean IfxTlf35584_Hal_init(IfxTlf35584_Hal_Config *config);

/** Initialize the configuration structure to default values
 * \param config HAL configuration
 */
void IfxTlf35584_Hal_initConfig(IfxTlf35584_Hal_Config *config);

/** Set the port output to high level
 *
 * \param port Port handle
 *
 */
IFX_INLINE void IfxTlf35584_Hal_setPortHigh(IfxTlf35584_Pin *port)
{
    IfxPort_setPinState(port->port, port->pinIndex, IfxPort_State_high);
}


/** Set the port output to low level
 *
 * \param pin Port handle
 *
 */
IFX_INLINE void IfxTlf35584_Hal_setPortLow(IfxTlf35584_Pin *port)
{
    IfxPort_setPinState(port->port, port->pinIndex, IfxPort_State_low);
}


/** Set the port output to a defined level
 *
 * \param port Port handle
 * \param action Action to be done on the port
 *
 */
IFX_INLINE void IfxTlf35584_Hal_setPortState(IfxTlf35584_Pin *port, IfxTlf35584_PinState state)
{
    IfxPort_setPinState(port->port, port->pinIndex, (IfxPort_State)state);
}


/** Get the port input level
 *
 * \param port Port handle
 *
 * \return Return TRUE for level high and FALSE for level LOW
 *
 */
IFX_INLINE boolean IfxTlf35584_Hal_getPortState(IfxTlf35584_Pin *port)
{
    return IfxPort_getPinState(port->port, port->pinIndex);
}


/** Wait for the given timeout
 * The function returns once the timeout elapsed.
 *
 * \param Timeout Time value before the function returns
 *
 * \return None
 *
 */
IFX_INLINE void IfxTlf35584_Hal_wait(IfxTlf35584_Time timeout)
{
    wait(timeout);
}


/** Wait for the given deadline
 * The function returns once the deadline is over.
 *
 * \param deadline Time value before the function returns
 *
 * \return None
 *
 */
IFX_INLINE void IfxTlf35584_Hal_waitForDeadline(IfxTlf35584_Time deadline)
{
    while (!isDeadLine(deadline))
    {}
}


/** Return the timeout deadline
 *
 * \param timeout Specifies the dead line from now: Deadline = Now + Timeout
 *
 * \return Return the timeout deadline
 *
 */
IFX_INLINE IfxTlf35584_Time IfxTlf35584_Hal_getDeadline(IfxTlf35584_Time timeout)
{
    return getDeadLine(timeout);
}


/** Return the current time
 *
 * \return The current time
 *
 */
IFX_INLINE IfxTlf35584_Time IfxTlf35584_Hal_now(void)
{
    return now();
}


/**
 * \brief Print formatted string
 * \param format printf-compatible formatted string.
 * \retval TRUE if the string is printed successfully
 * \retval FALSE if the function failed.
 */
boolean IfxTlf35584_Hal_print(pchar format, ...);
/** \} */

#endif /* IFXTLF35584_HAL_H */
