/**
 * \file IfxTlf35584.c
 * \brief TLF35584  basic functionality 
 *
 * \copyright Copyright (c) 2017 Infineon Technologies AG. All rights reserved.
 *
 * $Date: 2017-05-15 04:24:16
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 */

/******************************************************************************/
/*----------------------------------Includes----------------------------------*/
/******************************************************************************/

#include "IfxTlf35584.h"
#include <stdio.h>


            
/******************************************************************************/
/*-----------------------Exported Variables/Constants-------------------------*/
/******************************************************************************/

IFX_CONST uint32 IfxTlf35584_g_actionReadRegisters[2] = 
{
    0x00000000,
    0x00000000
};

IFX_CONST uint32 IfxTlf35584_g_actionWriteRegisters[2] = 
{
    0xFFE00008,
    0x0000103F
};

IFX_CONST Ifx_TLF35584 IfxTlf35584_g_noOpMask = 
{
    .DEVCFG0.U = 0x0,
    .DEVCFG1.U = 0x0,
    .DEVCFG2.U = 0x0,
    .PROTCFG.U = 0xff,
    .SYSPCFG0.U = 0xfe,
    .SYSPCFG1.U = 0x0,
    .WDCFG0.U = 0x0,
    .WDCFG1.U = 0xe0,
    .FWDCFG.U = 0xe0,
    .WWDCFG0.U = 0xe0,
    .WWDCFG1.U = 0xe0,
    .RSYSPCFG0.U = 0x0,
    .RSYSPCFG1.U = 0x0,
    .RWDCFG0.U = 0x0,
    .RWDCFG1.U = 0x0,
    .RFWDCFG.U = 0x0,
    .RWWDCFG0.U = 0x0,
    .RWWDCFG1.U = 0x0,
    .WKTIMCFG0.U = 0x0,
    .WKTIMCFG1.U = 0x0,
    .WKTIMCFG2.U = 0x0,
    .DEVCTRL.U = 0x0,
    .DEVCTRLN.U = 0x0,
    .WWDSCMD.U = 0x0,
    .FWDRSP.U = 0xff,
    .FWDRSPSYNC.U = 0xff,
    .SYSFAIL.U = 0xc7,
    .INITERR.U = 0xff,
    .IF.U = 0x7f,
    .SYSSF.U = 0x3f,
    .WKSF.U = 0x1f,
    .SPISF.U = 0x1f,
    .MONSF0.U = 0xff,
    .MONSF1.U = 0xff,
    .MONSF2.U = 0xff,
    .MONSF3.U = 0xf1,
    .OTFAIL.U = 0x93,
    .OTWRNSF.U = 0x37,
    .VMONSTAT.U = 0x0,
    .DEVSTAT.U = 0x0,
    .PROTSTAT.U = 0x0,
    .WWDSTAT.U = 0x0,
    .FWDSTAT0.U = 0x0,
    .FWDSTAT1.U = 0x0,
    .ABIST_CTRL0.U = 0x0,
    .ABIST_CTRL1.U = 0x0,
    .ABIST_SELECT0.U = 0x0,
    .ABIST_SELECT1.U = 0x0,
    .ABIST_SELECT2.U = 0x0,
    .BCK_FREQ_CHANGE.U = 0x0,
    .BCK_FRE_SPREAD.U = 0x0,
    .BCK_MAIN_CTRL.U = 0x0,
    .GTM.U = 0x0
};

IFX_CONST Ifx_TLF35584 IfxTlf35584_g_noOpValue = 
{
    .DEVCFG0.U = 0x0,
    .DEVCFG1.U = 0x0,
    .DEVCFG2.U = 0x0,
    .PROTCFG.U = 0x0,
    .SYSPCFG0.U = 0x0,
    .SYSPCFG1.U = 0x0,
    .WDCFG0.U = 0x0,
    .WDCFG1.U = 0x0,
    .FWDCFG.U = 0x0,
    .WWDCFG0.U = 0x0,
    .WWDCFG1.U = 0x0,
    .RSYSPCFG0.U = 0x0,
    .RSYSPCFG1.U = 0x0,
    .RWDCFG0.U = 0x0,
    .RWDCFG1.U = 0x0,
    .RFWDCFG.U = 0x0,
    .RWWDCFG0.U = 0x0,
    .RWWDCFG1.U = 0x0,
    .WKTIMCFG0.U = 0x0,
    .WKTIMCFG1.U = 0x0,
    .WKTIMCFG2.U = 0x0,
    .DEVCTRL.U = 0x0,
    .DEVCTRLN.U = 0x0,
    .WWDSCMD.U = 0x0,
    .FWDRSP.U = 0x0,
    .FWDRSPSYNC.U = 0x0,
    .SYSFAIL.U = 0x0,
    .INITERR.U = 0x0,
    .IF.U = 0x0,
    .SYSSF.U = 0x0,
    .WKSF.U = 0x0,
    .SPISF.U = 0x0,
    .MONSF0.U = 0x0,
    .MONSF1.U = 0x0,
    .MONSF2.U = 0x0,
    .MONSF3.U = 0x0,
    .OTFAIL.U = 0x0,
    .OTWRNSF.U = 0x0,
    .VMONSTAT.U = 0x0,
    .DEVSTAT.U = 0x0,
    .PROTSTAT.U = 0x0,
    .WWDSTAT.U = 0x0,
    .FWDSTAT0.U = 0x0,
    .FWDSTAT1.U = 0x0,
    .ABIST_CTRL0.U = 0x0,
    .ABIST_CTRL1.U = 0x0,
    .ABIST_SELECT0.U = 0x0,
    .ABIST_SELECT1.U = 0x0,
    .ABIST_SELECT2.U = 0x0,
    .BCK_FREQ_CHANGE.U = 0x0,
    .BCK_FRE_SPREAD.U = 0x0,
    .BCK_MAIN_CTRL.U = 0x0,
    .GTM.U = 0x0
};

IFX_CONST uint32 IfxTlf35584_g_validAddress[2] = 
{
    0xFFFFFFFF,
    0x800FFFFF
};

IFX_CONST uint32 IfxTlf35584_g_volatileRegisters[2] = 
{
    0xFCE3F804,
    0x8009DFFF
};


/******************************************************************************/
/*-------------------------Function Implementations---------------------------*/
/******************************************************************************/

IfxTlf35584_TransitionDelayIntoLowPowerStates IfxTlf35584_getTransitionDelayIntoLowPowerStates(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_TransitionDelayIntoLowPowerStates value;
    Ifx_TLF35584_DEVCFG0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCFG0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_TransitionDelayIntoLowPowerStates)reg.B.TRDEL;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_setTransitionDelayIntoLowPowerStates(IfxTlf35584_Driver* driver, IfxTlf35584_TransitionDelayIntoLowPowerStates value, boolean* status)
{
    Ifx_TLF35584_DEVCFG0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCFG0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.TRDEL = value;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCFG0) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void IfxTlf35584_setTransitionDelayIntoLowPowerStatesUs(IfxTlf35584_Driver* driver, sint16 value, boolean* status)
{
    Ifx_TLF35584_DEVCFG0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCFG0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        sint16 temp;
        temp=(value-100)/100;
        reg.B.TRDEL = IFXTLF35584_HAL_MIN(IFXTLF35584_HAL_MAX(0,temp),IFX_TLF35584_DEVCFG0_TRDEL_MSK);
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCFG0) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

sint16 IfxTlf35584_getTransitionDelayIntoLowPowerStatesUs(IfxTlf35584_Driver* driver, boolean* status)
{
    sint16 value;
    Ifx_TLF35584_DEVCFG0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCFG0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value=reg.B.TRDEL*100+100;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

IfxTlf35584_WakeTimerCyclePeriod IfxTlf35584_getWakeTimerCyclePeriod(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_WakeTimerCyclePeriod value;
    Ifx_TLF35584_DEVCFG0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCFG0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_WakeTimerCyclePeriod)reg.B.WKTIMCYC;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_setWakeTimerCyclePeriod(IfxTlf35584_Driver* driver, IfxTlf35584_WakeTimerCyclePeriod value, boolean* status)
{
    Ifx_TLF35584_DEVCFG0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCFG0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.WKTIMCYC = value;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCFG0) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_WakeTimerEnable IfxTlf35584_getWakeTimerEnable(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_WakeTimerEnable value;
    Ifx_TLF35584_DEVCFG0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCFG0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_WakeTimerEnable)reg.B.WKTIMEN;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isWakeTimerEnabled(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_WakeTimerEnable value;
    Ifx_TLF35584_DEVCFG0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCFG0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.WKTIMEN == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_setWakeTimerEnable(IfxTlf35584_Driver* driver, IfxTlf35584_WakeTimerEnable value, boolean* status)
{
    Ifx_TLF35584_DEVCFG0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCFG0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.WKTIMEN = value;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCFG0) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void IfxTlf35584_enableWakeTimer(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_DEVCFG0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCFG0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.WKTIMEN = 1;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCFG0) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void IfxTlf35584_disableWakeTimer(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_DEVCFG0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCFG0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.WKTIMEN = 0;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCFG0) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_ResetReleaseDelayTime IfxTlf35584_getResetReleaseDelayTime(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_ResetReleaseDelayTime value;
    Ifx_TLF35584_DEVCFG1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCFG1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_ResetReleaseDelayTime)reg.B.RESDEL;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_setResetReleaseDelayTime(IfxTlf35584_Driver* driver, IfxTlf35584_ResetReleaseDelayTime value, boolean* status)
{
    Ifx_TLF35584_DEVCFG1 reg;
    reg.U = 0;
    reg.B.RESDEL = value;
    *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCFG1) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
}

IfxTlf35584_SynchronizationOutputForExternalSwitchmodeRegulatorEnable IfxTlf35584_getSynchronizationOutputForExternalSwitchmodeRegulatorEnable(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_SynchronizationOutputForExternalSwitchmodeRegulatorEnable value;
    Ifx_TLF35584_DEVCFG2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCFG2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_SynchronizationOutputForExternalSwitchmodeRegulatorEnable)reg.B.ESYNEN;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isSynchronizationOutputForExternalSwitchmodeRegulatorEnabled(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_SynchronizationOutputForExternalSwitchmodeRegulatorEnable value;
    Ifx_TLF35584_DEVCFG2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCFG2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.ESYNEN == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_setSynchronizationOutputForExternalSwitchmodeRegulatorEnable(IfxTlf35584_Driver* driver, IfxTlf35584_SynchronizationOutputForExternalSwitchmodeRegulatorEnable value, boolean* status)
{
    Ifx_TLF35584_DEVCFG2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCFG2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.ESYNEN = value;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCFG2) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void IfxTlf35584_enableSynchronizationOutputForExternalSwitchmodeRegulator(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_DEVCFG2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCFG2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.ESYNEN = 1;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCFG2) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void IfxTlf35584_disableSynchronizationOutputForExternalSwitchmodeRegulator(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_DEVCFG2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCFG2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.ESYNEN = 0;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCFG2) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_ExternalSynchronizationOutputPhase IfxTlf35584_getExternalSynchronizationOutputPhase(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_ExternalSynchronizationOutputPhase value;
    Ifx_TLF35584_DEVCFG2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCFG2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_ExternalSynchronizationOutputPhase)reg.B.ESYNPHA;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_setExternalSynchronizationOutputPhase(IfxTlf35584_Driver* driver, IfxTlf35584_ExternalSynchronizationOutputPhase value, boolean* status)
{
    Ifx_TLF35584_DEVCFG2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCFG2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.ESYNPHA = value;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCFG2) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_QucCurrentMonitoringThresholdValue IfxTlf35584_getQucCurrentMonitoringThresholdValue(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_QucCurrentMonitoringThresholdValue value;
    Ifx_TLF35584_DEVCFG2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCFG2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_QucCurrentMonitoringThresholdValue)reg.B.CTHR;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_setQucCurrentMonitoringThresholdValue(IfxTlf35584_Driver* driver, IfxTlf35584_QucCurrentMonitoringThresholdValue value, boolean* status)
{
    Ifx_TLF35584_DEVCFG2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCFG2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.CTHR = value;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCFG2) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_QucCurrentMonitorEnableForTransitionToALowPowerState IfxTlf35584_getQucCurrentMonitorEnableForTransitionToALowPowerState(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_QucCurrentMonitorEnableForTransitionToALowPowerState value;
    Ifx_TLF35584_DEVCFG2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCFG2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_QucCurrentMonitorEnableForTransitionToALowPowerState)reg.B.CMONEN;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_setQucCurrentMonitorEnableForTransitionToALowPowerState(IfxTlf35584_Driver* driver, IfxTlf35584_QucCurrentMonitorEnableForTransitionToALowPowerState value, boolean* status)
{
    Ifx_TLF35584_DEVCFG2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCFG2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.CMONEN = value;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCFG2) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_StepDownConverterFrequencySelectionStatus IfxTlf35584_getStepDownConverterFrequencySelectionStatus(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_StepDownConverterFrequencySelectionStatus value;
    Ifx_TLF35584_DEVCFG2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCFG2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_StepDownConverterFrequencySelectionStatus)reg.B.FRE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

IfxTlf35584_StepUpConverterEnableStatus IfxTlf35584_getStepUpConverterEnableStatus(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_StepUpConverterEnableStatus value;
    Ifx_TLF35584_DEVCFG2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCFG2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_StepUpConverterEnableStatus)reg.B.STU;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isStepUpConverterEnabled(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_StepUpConverterEnableStatus value;
    Ifx_TLF35584_DEVCFG2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCFG2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.STU == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

IfxTlf35584_ExternalCoreSupplyEnableStatus IfxTlf35584_getExternalCoreSupplyEnableStatus(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_ExternalCoreSupplyEnableStatus value;
    Ifx_TLF35584_DEVCFG2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCFG2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_ExternalCoreSupplyEnableStatus)reg.B.EVCEN;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isExternalCoreSupplyEnabled(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_ExternalCoreSupplyEnableStatus value;
    Ifx_TLF35584_DEVCFG2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCFG2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.EVCEN == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

IfxTlf35584_ProtectionKey IfxTlf35584_getProtectionKey(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_ProtectionKey value;
    Ifx_TLF35584_PROTCFG reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.PROTCFG) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_ProtectionKey)reg.B.KEY;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_setProtectionKey(IfxTlf35584_Driver* driver, IfxTlf35584_ProtectionKey value, boolean* status)
{
    Ifx_TLF35584_PROTCFG reg;
    reg.U = 0;
    reg.B.KEY = value;
    *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.PROTCFG) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
}

IfxTlf35584_RequestStandbyRegulatorQstEnable IfxTlf35584_getRequestStandbyRegulatorQstEnable(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_RequestStandbyRegulatorQstEnable value;
    Ifx_TLF35584_SYSPCFG0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSPCFG0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_RequestStandbyRegulatorQstEnable)reg.B.STBYEN;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isRequestStandbyRegulatorQstEnabled(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_RequestStandbyRegulatorQstEnable value;
    Ifx_TLF35584_SYSPCFG0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSPCFG0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.STBYEN == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_setRequestStandbyRegulatorQstEnable(IfxTlf35584_Driver* driver, IfxTlf35584_RequestStandbyRegulatorQstEnable value, boolean* status)
{
    Ifx_TLF35584_SYSPCFG0 reg;
    reg.U = 0;
    reg.B.STBYEN = value;
    *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSPCFG0) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_SYSPCFG0_STBYEN_MSK << IFX_TLF35584_SYSPCFG0_STBYEN_OFF);
}

void IfxTlf35584_enableRequestStandbyRegulatorQst(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_SYSPCFG0 reg;
    reg.U = 0;
    reg.B.STBYEN = 1;
    *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSPCFG0) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_SYSPCFG0_STBYEN_MSK << IFX_TLF35584_SYSPCFG0_STBYEN_OFF);
}

void IfxTlf35584_disableRequestStandbyRegulatorQst(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_SYSPCFG0 reg;
    reg.U = 0;
    reg.B.STBYEN = 0;
    *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSPCFG0) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_SYSPCFG0_STBYEN_MSK << IFX_TLF35584_SYSPCFG0_STBYEN_OFF);
}

IfxTlf35584_RequestErrPinMonitorRecoveryTime IfxTlf35584_getRequestErrPinMonitorRecoveryTime(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_RequestErrPinMonitorRecoveryTime value;
    Ifx_TLF35584_SYSPCFG1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSPCFG1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_RequestErrPinMonitorRecoveryTime)reg.B.ERRREC;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_setRequestErrPinMonitorRecoveryTime(IfxTlf35584_Driver* driver, IfxTlf35584_RequestErrPinMonitorRecoveryTime value, boolean* status)
{
    Ifx_TLF35584_SYSPCFG1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSPCFG1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.ERRREC = value;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSPCFG1) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_RequestErrPinMonitorRecoveryEnable IfxTlf35584_getRequestErrPinMonitorRecoveryEnable(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_RequestErrPinMonitorRecoveryEnable value;
    Ifx_TLF35584_SYSPCFG1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSPCFG1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_RequestErrPinMonitorRecoveryEnable)reg.B.ERRRECEN;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isRequestErrPinMonitorRecoveryEnabled(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_RequestErrPinMonitorRecoveryEnable value;
    Ifx_TLF35584_SYSPCFG1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSPCFG1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.ERRRECEN == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_setRequestErrPinMonitorRecoveryEnable(IfxTlf35584_Driver* driver, IfxTlf35584_RequestErrPinMonitorRecoveryEnable value, boolean* status)
{
    Ifx_TLF35584_SYSPCFG1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSPCFG1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.ERRRECEN = value;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSPCFG1) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void IfxTlf35584_enableRequestErrPinMonitorRecovery(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_SYSPCFG1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSPCFG1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.ERRRECEN = 1;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSPCFG1) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void IfxTlf35584_disableRequestErrPinMonitorRecovery(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_SYSPCFG1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSPCFG1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.ERRRECEN = 0;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSPCFG1) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_RequestErrPinMonitorEnable IfxTlf35584_getRequestErrPinMonitorEnable(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_RequestErrPinMonitorEnable value;
    Ifx_TLF35584_SYSPCFG1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSPCFG1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_RequestErrPinMonitorEnable)reg.B.ERREN;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isRequestErrPinMonitorEnabled(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_RequestErrPinMonitorEnable value;
    Ifx_TLF35584_SYSPCFG1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSPCFG1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.ERREN == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_setRequestErrPinMonitorEnable(IfxTlf35584_Driver* driver, IfxTlf35584_RequestErrPinMonitorEnable value, boolean* status)
{
    Ifx_TLF35584_SYSPCFG1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSPCFG1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.ERREN = value;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSPCFG1) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void IfxTlf35584_enableRequestErrPinMonitor(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_SYSPCFG1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSPCFG1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.ERREN = 1;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSPCFG1) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void IfxTlf35584_disableRequestErrPinMonitor(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_SYSPCFG1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSPCFG1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.ERREN = 0;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSPCFG1) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_RequestErrPinMonitorFunctionailityEnableWhileTheSystemIsInSleep IfxTlf35584_getRequestErrPinMonitorFunctionailityEnableWhileTheSystemIsInSleep(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_RequestErrPinMonitorFunctionailityEnableWhileTheSystemIsInSleep value;
    Ifx_TLF35584_SYSPCFG1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSPCFG1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_RequestErrPinMonitorFunctionailityEnableWhileTheSystemIsInSleep)reg.B.ERRSLPEN;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_setRequestErrPinMonitorFunctionailityEnableWhileTheSystemIsInSleep(IfxTlf35584_Driver* driver, IfxTlf35584_RequestErrPinMonitorFunctionailityEnableWhileTheSystemIsInSleep value, boolean* status)
{
    Ifx_TLF35584_SYSPCFG1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSPCFG1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.ERRSLPEN = value;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSPCFG1) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_RequestSafeState2Delay IfxTlf35584_getRequestSafeState2Delay(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_RequestSafeState2Delay value;
    Ifx_TLF35584_SYSPCFG1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSPCFG1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_RequestSafeState2Delay)reg.B.SS2DEL;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_setRequestSafeState2Delay(IfxTlf35584_Driver* driver, IfxTlf35584_RequestSafeState2Delay value, boolean* status)
{
    Ifx_TLF35584_SYSPCFG1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSPCFG1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.SS2DEL = value;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSPCFG1) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_RequestWatchdogCycleTime IfxTlf35584_getRequestWatchdogCycleTime(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_RequestWatchdogCycleTime value;
    Ifx_TLF35584_WDCFG0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WDCFG0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_RequestWatchdogCycleTime)reg.B.WDCYC;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_setRequestWatchdogCycleTime(IfxTlf35584_Driver* driver, IfxTlf35584_RequestWatchdogCycleTime value, boolean* status)
{
    Ifx_TLF35584_WDCFG0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WDCFG0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.WDCYC = value;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WDCFG0) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_RequestWindowWatchdogTriggerSelection IfxTlf35584_getRequestWindowWatchdogTriggerSelection(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_RequestWindowWatchdogTriggerSelection value;
    Ifx_TLF35584_WDCFG0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WDCFG0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_RequestWindowWatchdogTriggerSelection)reg.B.WWDTSEL;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_setRequestWindowWatchdogTriggerSelection(IfxTlf35584_Driver* driver, IfxTlf35584_RequestWindowWatchdogTriggerSelection value, boolean* status)
{
    Ifx_TLF35584_WDCFG0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WDCFG0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.WWDTSEL = value;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WDCFG0) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_RequestFunctionalWatchdogEnable IfxTlf35584_getRequestFunctionalWatchdogEnable(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_RequestFunctionalWatchdogEnable value;
    Ifx_TLF35584_WDCFG0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WDCFG0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_RequestFunctionalWatchdogEnable)reg.B.FWDEN;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isRequestFunctionalWatchdogEnabled(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_RequestFunctionalWatchdogEnable value;
    Ifx_TLF35584_WDCFG0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WDCFG0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.FWDEN == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_setRequestFunctionalWatchdogEnable(IfxTlf35584_Driver* driver, IfxTlf35584_RequestFunctionalWatchdogEnable value, boolean* status)
{
    Ifx_TLF35584_WDCFG0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WDCFG0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.FWDEN = value;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WDCFG0) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void IfxTlf35584_enableRequestFunctionalWatchdog(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_WDCFG0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WDCFG0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.FWDEN = 1;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WDCFG0) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void IfxTlf35584_disableRequestFunctionalWatchdog(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_WDCFG0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WDCFG0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.FWDEN = 0;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WDCFG0) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_RequestWindowWatchdogEnable IfxTlf35584_getRequestWindowWatchdogEnable(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_RequestWindowWatchdogEnable value;
    Ifx_TLF35584_WDCFG0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WDCFG0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_RequestWindowWatchdogEnable)reg.B.WWDEN;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isRequestWindowWatchdogEnabled(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_RequestWindowWatchdogEnable value;
    Ifx_TLF35584_WDCFG0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WDCFG0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.WWDEN == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_setRequestWindowWatchdogEnable(IfxTlf35584_Driver* driver, IfxTlf35584_RequestWindowWatchdogEnable value, boolean* status)
{
    Ifx_TLF35584_WDCFG0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WDCFG0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.WWDEN = value;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WDCFG0) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void IfxTlf35584_enableRequestWindowWatchdog(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_WDCFG0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WDCFG0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.WWDEN = 1;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WDCFG0) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void IfxTlf35584_disableRequestWindowWatchdog(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_WDCFG0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WDCFG0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.WWDEN = 0;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WDCFG0) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

uint8 IfxTlf35584_getRequestWindowWatchdogErrorThreshold(IfxTlf35584_Driver* driver, boolean* status)
{
    uint8 value;
    Ifx_TLF35584_WDCFG0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WDCFG0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (uint8)reg.B.WWDETHR;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_setRequestWindowWatchdogErrorThreshold(IfxTlf35584_Driver* driver, uint8 value, boolean* status)
{
    Ifx_TLF35584_WDCFG0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WDCFG0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.WWDETHR = value;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WDCFG0) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

uint8 IfxTlf35584_getRequestFunctionalWatchdogErrorThreshold(IfxTlf35584_Driver* driver, boolean* status)
{
    uint8 value;
    Ifx_TLF35584_WDCFG1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WDCFG1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (uint8)reg.B.FWDETHR;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_setRequestFunctionalWatchdogErrorThreshold(IfxTlf35584_Driver* driver, uint8 value, boolean* status)
{
    Ifx_TLF35584_WDCFG1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WDCFG1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.FWDETHR = value;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WDCFG1) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_WDCFG1_FWDETHR_MSK << IFX_TLF35584_WDCFG1_FWDETHR_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_RequestWatchdogFunctionalityEnableWhileTheDeviceIsInSleep IfxTlf35584_getRequestWatchdogFunctionalityEnableWhileTheDeviceIsInSleep(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_RequestWatchdogFunctionalityEnableWhileTheDeviceIsInSleep value;
    Ifx_TLF35584_WDCFG1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WDCFG1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_RequestWatchdogFunctionalityEnableWhileTheDeviceIsInSleep)reg.B.WDSLPEN;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_setRequestWatchdogFunctionalityEnableWhileTheDeviceIsInSleep(IfxTlf35584_Driver* driver, IfxTlf35584_RequestWatchdogFunctionalityEnableWhileTheDeviceIsInSleep value, boolean* status)
{
    Ifx_TLF35584_WDCFG1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WDCFG1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.WDSLPEN = value;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WDCFG1) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_WDCFG1_WDSLPEN_MSK << IFX_TLF35584_WDCFG1_WDSLPEN_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_RequestFunctionalWatchdogHeartbeatTimerPeriod IfxTlf35584_getRequestFunctionalWatchdogHeartbeatTimerPeriod(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_RequestFunctionalWatchdogHeartbeatTimerPeriod value;
    Ifx_TLF35584_FWDCFG reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.FWDCFG) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_RequestFunctionalWatchdogHeartbeatTimerPeriod)reg.B.WDHBTP;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_setRequestFunctionalWatchdogHeartbeatTimerPeriod(IfxTlf35584_Driver* driver, IfxTlf35584_RequestFunctionalWatchdogHeartbeatTimerPeriod value, boolean* status)
{
    Ifx_TLF35584_FWDCFG reg;
    reg.U = 0;
    reg.B.WDHBTP = value;
    *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.FWDCFG) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_FWDCFG_WDHBTP_MSK << IFX_TLF35584_FWDCFG_WDHBTP_OFF);
}

void IfxTlf35584_setRequestFunctionalWatchdogHeartbeatTimerPeriodWdCycle(IfxTlf35584_Driver* driver, sint16 value, boolean* status)
{
    Ifx_TLF35584_FWDCFG reg;
    sint16 temp;
    reg.U = 0;
    temp=(value-50)/50;
    reg.B.WDHBTP = IFXTLF35584_HAL_MIN(IFXTLF35584_HAL_MAX(0,temp),IFX_TLF35584_FWDCFG_WDHBTP_MSK);
    *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.FWDCFG) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_FWDCFG_WDHBTP_MSK << IFX_TLF35584_FWDCFG_WDHBTP_OFF);
}

sint16 IfxTlf35584_getRequestFunctionalWatchdogHeartbeatTimerPeriodWdCycle(IfxTlf35584_Driver* driver, boolean* status)
{
    sint16 value;
    Ifx_TLF35584_FWDCFG reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.FWDCFG) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value=reg.B.WDHBTP*50+50;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

IfxTlf35584_RequestWindowWatchdogClosedWindowTime IfxTlf35584_getRequestWindowWatchdogClosedWindowTime(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_RequestWindowWatchdogClosedWindowTime value;
    Ifx_TLF35584_WWDCFG0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WWDCFG0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_RequestWindowWatchdogClosedWindowTime)reg.B.CW;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_setRequestWindowWatchdogClosedWindowTime(IfxTlf35584_Driver* driver, IfxTlf35584_RequestWindowWatchdogClosedWindowTime value, boolean* status)
{
    Ifx_TLF35584_WWDCFG0 reg;
    reg.U = 0;
    reg.B.CW = value;
    *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WWDCFG0) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_WWDCFG0_CW_MSK << IFX_TLF35584_WWDCFG0_CW_OFF);
}

void IfxTlf35584_setRequestWindowWatchdogClosedWindowTimeWdCycle(IfxTlf35584_Driver* driver, sint16 value, boolean* status)
{
    Ifx_TLF35584_WWDCFG0 reg;
    sint16 temp;
    reg.U = 0;
    temp=(value-50)/50;
    reg.B.CW = IFXTLF35584_HAL_MIN(IFXTLF35584_HAL_MAX(0,temp),IFX_TLF35584_WWDCFG0_CW_MSK);
    *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WWDCFG0) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_WWDCFG0_CW_MSK << IFX_TLF35584_WWDCFG0_CW_OFF);
}

sint16 IfxTlf35584_getRequestWindowWatchdogClosedWindowTimeWdCycle(IfxTlf35584_Driver* driver, boolean* status)
{
    sint16 value;
    Ifx_TLF35584_WWDCFG0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WWDCFG0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value=reg.B.CW*50+50;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

IfxTlf35584_RequestWindowWatchdogOpenWindowTime IfxTlf35584_getRequestWindowWatchdogOpenWindowTime(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_RequestWindowWatchdogOpenWindowTime value;
    Ifx_TLF35584_WWDCFG1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WWDCFG1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_RequestWindowWatchdogOpenWindowTime)reg.B.OW;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_setRequestWindowWatchdogOpenWindowTime(IfxTlf35584_Driver* driver, IfxTlf35584_RequestWindowWatchdogOpenWindowTime value, boolean* status)
{
    Ifx_TLF35584_WWDCFG1 reg;
    reg.U = 0;
    reg.B.OW = value;
    *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WWDCFG1) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_WWDCFG1_OW_MSK << IFX_TLF35584_WWDCFG1_OW_OFF);
}

void IfxTlf35584_setRequestWindowWatchdogOpenWindowTimeWdCycle(IfxTlf35584_Driver* driver, sint16 value, boolean* status)
{
    Ifx_TLF35584_WWDCFG1 reg;
    sint16 temp;
    reg.U = 0;
    temp=(value-50)/50;
    reg.B.OW = IFXTLF35584_HAL_MIN(IFXTLF35584_HAL_MAX(0,temp),IFX_TLF35584_WWDCFG1_OW_MSK);
    *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WWDCFG1) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_WWDCFG1_OW_MSK << IFX_TLF35584_WWDCFG1_OW_OFF);
}

sint16 IfxTlf35584_getRequestWindowWatchdogOpenWindowTimeWdCycle(IfxTlf35584_Driver* driver, boolean* status)
{
    sint16 value;
    Ifx_TLF35584_WWDCFG1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WWDCFG1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value=reg.B.OW*50+50;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

IfxTlf35584_StandbyRegulatorQstEnableStatus IfxTlf35584_getStandbyRegulatorQstEnableStatus(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_StandbyRegulatorQstEnableStatus value;
    Ifx_TLF35584_RSYSPCFG0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.RSYSPCFG0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_StandbyRegulatorQstEnableStatus)reg.B.STBYEN;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isStandbyRegulatorQstEnabled(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_StandbyRegulatorQstEnableStatus value;
    Ifx_TLF35584_RSYSPCFG0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.RSYSPCFG0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.STBYEN == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

IfxTlf35584_ErrPinMonitorRecoveryTimeStatus IfxTlf35584_getErrPinMonitorRecoveryTimeStatus(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_ErrPinMonitorRecoveryTimeStatus value;
    Ifx_TLF35584_RSYSPCFG1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.RSYSPCFG1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_ErrPinMonitorRecoveryTimeStatus)reg.B.ERRREC;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

IfxTlf35584_ErrPinMonitorRecoveryEnableStatus IfxTlf35584_getErrPinMonitorRecoveryEnableStatus(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_ErrPinMonitorRecoveryEnableStatus value;
    Ifx_TLF35584_RSYSPCFG1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.RSYSPCFG1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_ErrPinMonitorRecoveryEnableStatus)reg.B.ERRRECEN;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isErrPinMonitorRecoveryEnabled(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_ErrPinMonitorRecoveryEnableStatus value;
    Ifx_TLF35584_RSYSPCFG1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.RSYSPCFG1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.ERRRECEN == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

IfxTlf35584_ErrPinMonitorEnableStatus IfxTlf35584_getErrPinMonitorEnableStatus(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_ErrPinMonitorEnableStatus value;
    Ifx_TLF35584_RSYSPCFG1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.RSYSPCFG1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_ErrPinMonitorEnableStatus)reg.B.ERREN;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isErrPinMonitorEnabled(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_ErrPinMonitorEnableStatus value;
    Ifx_TLF35584_RSYSPCFG1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.RSYSPCFG1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.ERREN == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

IfxTlf35584_ErrPinMonitorFunctionalityEnableStatusWhileTheDeviceIsInSleep IfxTlf35584_getErrPinMonitorFunctionalityEnableStatusWhileTheDeviceIsInSleep(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_ErrPinMonitorFunctionalityEnableStatusWhileTheDeviceIsInSleep value;
    Ifx_TLF35584_RSYSPCFG1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.RSYSPCFG1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_ErrPinMonitorFunctionalityEnableStatusWhileTheDeviceIsInSleep)reg.B.ERRSLPEN;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

IfxTlf35584_SafeState2DelayStatus IfxTlf35584_getSafeState2DelayStatus(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_SafeState2DelayStatus value;
    Ifx_TLF35584_RSYSPCFG1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.RSYSPCFG1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_SafeState2DelayStatus)reg.B.SS2DEL;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

IfxTlf35584_WatchdogCycleTimeStatus IfxTlf35584_getWatchdogCycleTimeStatus(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_WatchdogCycleTimeStatus value;
    Ifx_TLF35584_RWDCFG0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.RWDCFG0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_WatchdogCycleTimeStatus)reg.B.WDCYC;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

IfxTlf35584_WindowWatchdogTriggerSelectionStatus IfxTlf35584_getWindowWatchdogTriggerSelectionStatus(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_WindowWatchdogTriggerSelectionStatus value;
    Ifx_TLF35584_RWDCFG0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.RWDCFG0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_WindowWatchdogTriggerSelectionStatus)reg.B.WWDTSEL;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

IfxTlf35584_FunctionalWatchdogEnableStatus IfxTlf35584_getFunctionalWatchdogEnableStatus(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_FunctionalWatchdogEnableStatus value;
    Ifx_TLF35584_RWDCFG0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.RWDCFG0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_FunctionalWatchdogEnableStatus)reg.B.FWDEN;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isFunctionalWatchdogEnabled(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_FunctionalWatchdogEnableStatus value;
    Ifx_TLF35584_RWDCFG0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.RWDCFG0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.FWDEN == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

IfxTlf35584_WindowWatchdogEnableStatus IfxTlf35584_getWindowWatchdogEnableStatus(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_WindowWatchdogEnableStatus value;
    Ifx_TLF35584_RWDCFG0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.RWDCFG0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_WindowWatchdogEnableStatus)reg.B.WWDEN;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isWindowWatchdogEnabled(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_WindowWatchdogEnableStatus value;
    Ifx_TLF35584_RWDCFG0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.RWDCFG0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.WWDEN == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

uint8 IfxTlf35584_getWindowWatchdogErrorThresholdStatus(IfxTlf35584_Driver* driver, boolean* status)
{
    uint8 value;
    Ifx_TLF35584_RWDCFG0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.RWDCFG0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (uint8)reg.B.WWDETHR;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

uint8 IfxTlf35584_getFunctionalWatchdogErrorThresholdStatus(IfxTlf35584_Driver* driver, boolean* status)
{
    uint8 value;
    Ifx_TLF35584_RWDCFG1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.RWDCFG1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (uint8)reg.B.FWDETHR;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

IfxTlf35584_WatchdogFunctionalityEnableStatusWhileTheDeviceIsInSleep IfxTlf35584_getWatchdogFunctionalityEnableStatusWhileTheDeviceIsInSleep(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_WatchdogFunctionalityEnableStatusWhileTheDeviceIsInSleep value;
    Ifx_TLF35584_RWDCFG1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.RWDCFG1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_WatchdogFunctionalityEnableStatusWhileTheDeviceIsInSleep)reg.B.WDSLPEN;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

IfxTlf35584_FunctionalWatchdogHeartbeatTimerPeriodStatus IfxTlf35584_getFunctionalWatchdogHeartbeatTimerPeriodStatus(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_FunctionalWatchdogHeartbeatTimerPeriodStatus value;
    Ifx_TLF35584_RFWDCFG reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.RFWDCFG) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_FunctionalWatchdogHeartbeatTimerPeriodStatus)reg.B.WDHBTP;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

sint16 IfxTlf35584_getFunctionalWatchdogHeartbeatTimerPeriodStatusWdCycle(IfxTlf35584_Driver* driver, boolean* status)
{
    sint16 value;
    Ifx_TLF35584_RFWDCFG reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.RFWDCFG) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value=reg.B.WDHBTP*50+50;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

IfxTlf35584_WindowWatchdogClosedWindowTimeStatus IfxTlf35584_getWindowWatchdogClosedWindowTimeStatus(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_WindowWatchdogClosedWindowTimeStatus value;
    Ifx_TLF35584_RWWDCFG0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.RWWDCFG0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_WindowWatchdogClosedWindowTimeStatus)reg.B.CW;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

sint16 IfxTlf35584_getWindowWatchdogClosedWindowTimeStatusWdCycle(IfxTlf35584_Driver* driver, boolean* status)
{
    sint16 value;
    Ifx_TLF35584_RWWDCFG0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.RWWDCFG0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value=reg.B.CW*50+50;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

IfxTlf35584_WindowWatchdogOpenWindowTimeStatus IfxTlf35584_getWindowWatchdogOpenWindowTimeStatus(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_WindowWatchdogOpenWindowTimeStatus value;
    Ifx_TLF35584_RWWDCFG1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.RWWDCFG1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_WindowWatchdogOpenWindowTimeStatus)reg.B.OW;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

sint16 IfxTlf35584_getWindowWatchdogOpenWindowTimeStatusWdCycle(IfxTlf35584_Driver* driver, boolean* status)
{
    sint16 value;
    Ifx_TLF35584_RWWDCFG1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.RWWDCFG1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value=reg.B.OW*50+50;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

uint8 IfxTlf35584_getWakeTimerValueLowerBits(IfxTlf35584_Driver* driver, boolean* status)
{
    uint8 value;
    Ifx_TLF35584_WKTIMCFG0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WKTIMCFG0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (uint8)reg.B.TIMVALL;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_setWakeTimerValueLowerBits(IfxTlf35584_Driver* driver, uint8 value, boolean* status)
{
    Ifx_TLF35584_WKTIMCFG0 reg;
    reg.U = 0;
    reg.B.TIMVALL = value;
    *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WKTIMCFG0) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
}

void IfxTlf35584_setWakeTimerValueLowerBitsWkTimCycle(IfxTlf35584_Driver* driver, sint16 value, boolean* status)
{
    Ifx_TLF35584_WKTIMCFG0 reg;
    sint16 temp;
    reg.U = 0;
    temp=value;
    reg.B.TIMVALL = IFXTLF35584_HAL_MIN(IFXTLF35584_HAL_MAX(0,temp),IFX_TLF35584_WKTIMCFG0_TIMVALL_MSK);
    *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WKTIMCFG0) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
}

sint16 IfxTlf35584_getWakeTimerValueLowerBitsWkTimCycle(IfxTlf35584_Driver* driver, boolean* status)
{
    sint16 value;
    Ifx_TLF35584_WKTIMCFG0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WKTIMCFG0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value=reg.B.TIMVALL;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

uint8 IfxTlf35584_getWakeTimerValueMiddleBits(IfxTlf35584_Driver* driver, boolean* status)
{
    uint8 value;
    Ifx_TLF35584_WKTIMCFG1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WKTIMCFG1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (uint8)reg.B.TIMVALM;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_setWakeTimerValueMiddleBits(IfxTlf35584_Driver* driver, uint8 value, boolean* status)
{
    Ifx_TLF35584_WKTIMCFG1 reg;
    reg.U = 0;
    reg.B.TIMVALM = value;
    *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WKTIMCFG1) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
}

void IfxTlf35584_setWakeTimerValueMiddleBitsWkTimCycle(IfxTlf35584_Driver* driver, sint32 value, boolean* status)
{
    Ifx_TLF35584_WKTIMCFG1 reg;
    sint32 temp;
    reg.U = 0;
    temp=value/256;
    reg.B.TIMVALM = IFXTLF35584_HAL_MIN(IFXTLF35584_HAL_MAX(0,temp),IFX_TLF35584_WKTIMCFG1_TIMVALM_MSK);
    *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WKTIMCFG1) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
}

sint32 IfxTlf35584_getWakeTimerValueMiddleBitsWkTimCycle(IfxTlf35584_Driver* driver, boolean* status)
{
    sint32 value;
    Ifx_TLF35584_WKTIMCFG1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WKTIMCFG1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value=reg.B.TIMVALM*256;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

uint8 IfxTlf35584_getWakeTimerValueHigherBits(IfxTlf35584_Driver* driver, boolean* status)
{
    uint8 value;
    Ifx_TLF35584_WKTIMCFG2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WKTIMCFG2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (uint8)reg.B.TIMVALH;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_setWakeTimerValueHigherBits(IfxTlf35584_Driver* driver, uint8 value, boolean* status)
{
    Ifx_TLF35584_WKTIMCFG2 reg;
    reg.U = 0;
    reg.B.TIMVALH = value;
    *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WKTIMCFG2) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
}

void IfxTlf35584_setWakeTimerValueHigherBitsWkTimCycle(IfxTlf35584_Driver* driver, sint32 value, boolean* status)
{
    Ifx_TLF35584_WKTIMCFG2 reg;
    sint32 temp;
    reg.U = 0;
    temp=value/65536;
    reg.B.TIMVALH = IFXTLF35584_HAL_MIN(IFXTLF35584_HAL_MAX(0,temp),IFX_TLF35584_WKTIMCFG2_TIMVALH_MSK);
    *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WKTIMCFG2) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
}

sint32 IfxTlf35584_getWakeTimerValueHigherBitsWkTimCycle(IfxTlf35584_Driver* driver, boolean* status)
{
    sint32 value;
    Ifx_TLF35584_WKTIMCFG2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WKTIMCFG2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value=reg.B.TIMVALH*65536;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

IfxTlf35584_RequestForDeviceStateTransition IfxTlf35584_getRequestForDeviceStateTransition(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_RequestForDeviceStateTransition value;
    Ifx_TLF35584_DEVCTRL reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCTRL) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_RequestForDeviceStateTransition)reg.B.STATEREQ;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_setRequestForDeviceStateTransition(IfxTlf35584_Driver* driver, IfxTlf35584_RequestForDeviceStateTransition value, boolean* status)
{
    Ifx_TLF35584_DEVCTRL reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCTRL) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.STATEREQ = value;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCTRL) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_RequestVoltageReferenceQvrEnable IfxTlf35584_getRequestVoltageReferenceQvrEnable(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_RequestVoltageReferenceQvrEnable value;
    Ifx_TLF35584_DEVCTRL reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCTRL) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_RequestVoltageReferenceQvrEnable)reg.B.VREFEN;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isRequestVoltageReferenceQvrEnabled(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_RequestVoltageReferenceQvrEnable value;
    Ifx_TLF35584_DEVCTRL reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCTRL) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.VREFEN == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_setRequestVoltageReferenceQvrEnable(IfxTlf35584_Driver* driver, IfxTlf35584_RequestVoltageReferenceQvrEnable value, boolean* status)
{
    Ifx_TLF35584_DEVCTRL reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCTRL) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.VREFEN = value;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCTRL) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void IfxTlf35584_enableRequestVoltageReferenceQvr(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_DEVCTRL reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCTRL) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.VREFEN = 1;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCTRL) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void IfxTlf35584_disableRequestVoltageReferenceQvr(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_DEVCTRL reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCTRL) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.VREFEN = 0;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCTRL) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_RequestCommunicationLdoQcoEnable IfxTlf35584_getRequestCommunicationLdoQcoEnable(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_RequestCommunicationLdoQcoEnable value;
    Ifx_TLF35584_DEVCTRL reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCTRL) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_RequestCommunicationLdoQcoEnable)reg.B.COMEN;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isRequestCommunicationLdoQcoEnabled(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_RequestCommunicationLdoQcoEnable value;
    Ifx_TLF35584_DEVCTRL reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCTRL) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.COMEN == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_setRequestCommunicationLdoQcoEnable(IfxTlf35584_Driver* driver, IfxTlf35584_RequestCommunicationLdoQcoEnable value, boolean* status)
{
    Ifx_TLF35584_DEVCTRL reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCTRL) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.COMEN = value;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCTRL) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void IfxTlf35584_enableRequestCommunicationLdoQco(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_DEVCTRL reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCTRL) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.COMEN = 1;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCTRL) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void IfxTlf35584_disableRequestCommunicationLdoQco(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_DEVCTRL reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCTRL) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.COMEN = 0;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCTRL) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_RequestTracker1Qt1Enable IfxTlf35584_getRequestTracker1Qt1Enable(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_RequestTracker1Qt1Enable value;
    Ifx_TLF35584_DEVCTRL reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCTRL) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_RequestTracker1Qt1Enable)reg.B.TRK1EN;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isRequestTracker1Qt1Enabled(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_RequestTracker1Qt1Enable value;
    Ifx_TLF35584_DEVCTRL reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCTRL) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.TRK1EN == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_setRequestTracker1Qt1Enable(IfxTlf35584_Driver* driver, IfxTlf35584_RequestTracker1Qt1Enable value, boolean* status)
{
    Ifx_TLF35584_DEVCTRL reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCTRL) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.TRK1EN = value;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCTRL) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void IfxTlf35584_enableRequestTracker1Qt1(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_DEVCTRL reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCTRL) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.TRK1EN = 1;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCTRL) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void IfxTlf35584_disableRequestTracker1Qt1(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_DEVCTRL reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCTRL) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.TRK1EN = 0;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCTRL) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_RequestTracker2Qt2Enable IfxTlf35584_getRequestTracker2Qt2Enable(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_RequestTracker2Qt2Enable value;
    Ifx_TLF35584_DEVCTRL reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCTRL) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_RequestTracker2Qt2Enable)reg.B.TRK2EN;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isRequestTracker2Qt2Enabled(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_RequestTracker2Qt2Enable value;
    Ifx_TLF35584_DEVCTRL reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCTRL) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.TRK2EN == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_setRequestTracker2Qt2Enable(IfxTlf35584_Driver* driver, IfxTlf35584_RequestTracker2Qt2Enable value, boolean* status)
{
    Ifx_TLF35584_DEVCTRL reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCTRL) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.TRK2EN = value;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCTRL) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void IfxTlf35584_enableRequestTracker2Qt2(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_DEVCTRL reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCTRL) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.TRK2EN = 1;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCTRL) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void IfxTlf35584_disableRequestTracker2Qt2(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_DEVCTRL reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCTRL) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.TRK2EN = 0;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCTRL) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

boolean IfxTlf35584_getWindowWatchdogSpiTriggerCommand(IfxTlf35584_Driver* driver, boolean* status)
{
    boolean value;
    Ifx_TLF35584_WWDSCMD reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WWDSCMD) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (boolean)reg.B.TRIG;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_setWindowWatchdogSpiTriggerCommand(IfxTlf35584_Driver* driver, boolean value, boolean* status)
{
    Ifx_TLF35584_WWDSCMD reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WWDSCMD) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.TRIG = value;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WWDSCMD) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

boolean IfxTlf35584_getLastSpiTriggerReceived(IfxTlf35584_Driver* driver, boolean* status)
{
    boolean value;
    Ifx_TLF35584_WWDSCMD reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WWDSCMD) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (boolean)reg.B.TRIG_STATUS;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

uint8 IfxTlf35584_getFunctionalWatchdogResponse(IfxTlf35584_Driver* driver, boolean* status)
{
    uint8 value;
    Ifx_TLF35584_FWDRSP reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.FWDRSP) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (uint8)reg.B.FWDRSP;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_setFunctionalWatchdogResponse(IfxTlf35584_Driver* driver, uint8 value, boolean* status)
{
    Ifx_TLF35584_FWDRSP reg;
    reg.U = 0;
    reg.B.FWDRSP = value;
    *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.FWDRSP) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
}

uint8 IfxTlf35584_getFunctionalWatchdogHeartbeatSynchronizationResponse(IfxTlf35584_Driver* driver, boolean* status)
{
    uint8 value;
    Ifx_TLF35584_FWDRSPSYNC reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.FWDRSPSYNC) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (uint8)reg.B.FWDRSPS;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_setFunctionalWatchdogHeartbeatSynchronizationResponse(IfxTlf35584_Driver* driver, uint8 value, boolean* status)
{
    Ifx_TLF35584_FWDRSPSYNC reg;
    reg.U = 0;
    reg.B.FWDRSPS = value;
    *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.FWDRSPSYNC) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
}

IfxTlf35584_DoubleBitErrorOnVoltageSelectionFlag IfxTlf35584_getDoubleBitErrorOnVoltageSelectionFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_DoubleBitErrorOnVoltageSelectionFlag value;
    Ifx_TLF35584_SYSFAIL reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSFAIL) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_DoubleBitErrorOnVoltageSelectionFlag)reg.B.VOLTSELERR;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isDoubleBitErrorOnVoltageSelectionFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_DoubleBitErrorOnVoltageSelectionFlag value;
    Ifx_TLF35584_SYSFAIL reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSFAIL) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.VOLTSELERR == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearDoubleBitErrorOnVoltageSelectionFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_SYSFAIL reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSFAIL) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.VOLTSELERR = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSFAIL) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_SYSFAIL_VOLTSELERR_MSK << IFX_TLF35584_SYSFAIL_VOLTSELERR_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_OverTemperatureFailureFlag IfxTlf35584_getOverTemperatureFailureFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_OverTemperatureFailureFlag value;
    Ifx_TLF35584_SYSFAIL reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSFAIL) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_OverTemperatureFailureFlag)reg.B.OTF;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isOverTemperatureFailureFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_OverTemperatureFailureFlag value;
    Ifx_TLF35584_SYSFAIL reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSFAIL) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.OTF == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearOverTemperatureFailureFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_SYSFAIL reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSFAIL) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.OTF = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSFAIL) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_SYSFAIL_OTF_MSK << IFX_TLF35584_SYSFAIL_OTF_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_VoltageMonitorFailureFlag IfxTlf35584_getVoltageMonitorFailureFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_VoltageMonitorFailureFlag value;
    Ifx_TLF35584_SYSFAIL reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSFAIL) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_VoltageMonitorFailureFlag)reg.B.VMONF;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isVoltageMonitorFailureFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_VoltageMonitorFailureFlag value;
    Ifx_TLF35584_SYSFAIL reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSFAIL) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.VMONF == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearVoltageMonitorFailureFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_SYSFAIL reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSFAIL) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.VMONF = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSFAIL) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_SYSFAIL_VMONF_MSK << IFX_TLF35584_SYSFAIL_VMONF_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_AbistOperationInterruptedFlag IfxTlf35584_getAbistOperationInterruptedFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_AbistOperationInterruptedFlag value;
    Ifx_TLF35584_SYSFAIL reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSFAIL) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_AbistOperationInterruptedFlag)reg.B.ABISTERR;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isAbistOperationInterruptedFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_AbistOperationInterruptedFlag value;
    Ifx_TLF35584_SYSFAIL reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSFAIL) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.ABISTERR == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearAbistOperationInterruptedFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_SYSFAIL reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSFAIL) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.ABISTERR = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSFAIL) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_SYSFAIL_ABISTERR_MSK << IFX_TLF35584_SYSFAIL_ABISTERR_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_InitFailureFlag IfxTlf35584_getInitFailureFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_InitFailureFlag value;
    Ifx_TLF35584_SYSFAIL reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSFAIL) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_InitFailureFlag)reg.B.INITF;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isInitFailureFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_InitFailureFlag value;
    Ifx_TLF35584_SYSFAIL reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSFAIL) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.INITF == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearInitFailureFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_SYSFAIL reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSFAIL) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.INITF = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSFAIL) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_SYSFAIL_INITF_MSK << IFX_TLF35584_SYSFAIL_INITF_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_VoltageMonitorInitFailureFlag IfxTlf35584_getVoltageMonitorInitFailureFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_VoltageMonitorInitFailureFlag value;
    Ifx_TLF35584_INITERR reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.INITERR) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_VoltageMonitorInitFailureFlag)reg.B.VMONF;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isVoltageMonitorInitFailureFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_VoltageMonitorInitFailureFlag value;
    Ifx_TLF35584_INITERR reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.INITERR) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.VMONF == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearVoltageMonitorInitFailureFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_INITERR reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.INITERR) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.VMONF = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.INITERR) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_INITERR_VMONF_MSK << IFX_TLF35584_INITERR_VMONF_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_WindowWatchdogErrorCounterOverflowFailureFlag IfxTlf35584_getWindowWatchdogErrorCounterOverflowFailureFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_WindowWatchdogErrorCounterOverflowFailureFlag value;
    Ifx_TLF35584_INITERR reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.INITERR) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_WindowWatchdogErrorCounterOverflowFailureFlag)reg.B.WWDF;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isWindowWatchdogErrorCounterOverflowFailureFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_WindowWatchdogErrorCounterOverflowFailureFlag value;
    Ifx_TLF35584_INITERR reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.INITERR) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.WWDF == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearWindowWatchdogErrorCounterOverflowFailureFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_INITERR reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.INITERR) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.WWDF = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.INITERR) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_INITERR_WWDF_MSK << IFX_TLF35584_INITERR_WWDF_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_FunctionalWatchdogErrorCounterOverflowFailureFlag IfxTlf35584_getFunctionalWatchdogErrorCounterOverflowFailureFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_FunctionalWatchdogErrorCounterOverflowFailureFlag value;
    Ifx_TLF35584_INITERR reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.INITERR) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_FunctionalWatchdogErrorCounterOverflowFailureFlag)reg.B.FWDF;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isFunctionalWatchdogErrorCounterOverflowFailureFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_FunctionalWatchdogErrorCounterOverflowFailureFlag value;
    Ifx_TLF35584_INITERR reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.INITERR) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.FWDF == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearFunctionalWatchdogErrorCounterOverflowFailureFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_INITERR reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.INITERR) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.FWDF = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.INITERR) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_INITERR_FWDF_MSK << IFX_TLF35584_INITERR_FWDF_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_McuErrorMonitorFailureFlag IfxTlf35584_getMcuErrorMonitorFailureFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_McuErrorMonitorFailureFlag value;
    Ifx_TLF35584_INITERR reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.INITERR) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_McuErrorMonitorFailureFlag)reg.B.ERRF;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isMcuErrorMonitorFailureFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_McuErrorMonitorFailureFlag value;
    Ifx_TLF35584_INITERR reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.INITERR) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.ERRF == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearMcuErrorMonitorFailureFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_INITERR reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.INITERR) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.ERRF = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.INITERR) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_INITERR_ERRF_MSK << IFX_TLF35584_INITERR_ERRF_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_SoftResetFlag IfxTlf35584_getSoftResetFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_SoftResetFlag value;
    Ifx_TLF35584_INITERR reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.INITERR) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_SoftResetFlag)reg.B.SOFTRES;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isSoftResetFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_SoftResetFlag value;
    Ifx_TLF35584_INITERR reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.INITERR) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.SOFTRES == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearSoftResetFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_INITERR reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.INITERR) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.SOFTRES = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.INITERR) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_INITERR_SOFTRES_MSK << IFX_TLF35584_INITERR_SOFTRES_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_HardResetFlag IfxTlf35584_getHardResetFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_HardResetFlag value;
    Ifx_TLF35584_INITERR reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.INITERR) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_HardResetFlag)reg.B.HARDRES;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isHardResetFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_HardResetFlag value;
    Ifx_TLF35584_INITERR reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.INITERR) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.HARDRES == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearHardResetFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_INITERR reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.INITERR) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.HARDRES = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.INITERR) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_INITERR_HARDRES_MSK << IFX_TLF35584_INITERR_HARDRES_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_SystemInterruptFlag IfxTlf35584_getSystemInterruptFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_SystemInterruptFlag value;
    Ifx_TLF35584_IF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.IF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_SystemInterruptFlag)reg.B.SYS;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isSystemInterruptFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_SystemInterruptFlag value;
    Ifx_TLF35584_IF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.IF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.SYS == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearSystemInterruptFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_IF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.IF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.SYS = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.IF) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_IF_SYS_MSK << IFX_TLF35584_IF_SYS_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_WakeInterruptFlag IfxTlf35584_getWakeInterruptFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_WakeInterruptFlag value;
    Ifx_TLF35584_IF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.IF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_WakeInterruptFlag)reg.B.WK;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isWakeInterruptFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_WakeInterruptFlag value;
    Ifx_TLF35584_IF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.IF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.WK == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearWakeInterruptFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_IF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.IF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.WK = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.IF) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_IF_WK_MSK << IFX_TLF35584_IF_WK_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_SpiInterruptFlag IfxTlf35584_getSpiInterruptFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_SpiInterruptFlag value;
    Ifx_TLF35584_IF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.IF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_SpiInterruptFlag)reg.B.SPI;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isSpiInterruptFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_SpiInterruptFlag value;
    Ifx_TLF35584_IF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.IF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.SPI == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearSpiInterruptFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_IF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.IF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.SPI = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.IF) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_IF_SPI_MSK << IFX_TLF35584_IF_SPI_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_MonitorInterruptFlag IfxTlf35584_getMonitorInterruptFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_MonitorInterruptFlag value;
    Ifx_TLF35584_IF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.IF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_MonitorInterruptFlag)reg.B.MON;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isMonitorInterruptFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_MonitorInterruptFlag value;
    Ifx_TLF35584_IF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.IF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.MON == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearMonitorInterruptFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_IF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.IF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.MON = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.IF) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_IF_MON_MSK << IFX_TLF35584_IF_MON_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_OverTemperatureWarningInterruptFlag IfxTlf35584_getOverTemperatureWarningInterruptFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_OverTemperatureWarningInterruptFlag value;
    Ifx_TLF35584_IF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.IF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_OverTemperatureWarningInterruptFlag)reg.B.OTW;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isOverTemperatureWarningInterruptFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_OverTemperatureWarningInterruptFlag value;
    Ifx_TLF35584_IF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.IF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.OTW == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearOverTemperatureWarningInterruptFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_IF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.IF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.OTW = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.IF) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_IF_OTW_MSK << IFX_TLF35584_IF_OTW_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_OverTemperatureFailureInterruptFlag IfxTlf35584_getOverTemperatureFailureInterruptFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_OverTemperatureFailureInterruptFlag value;
    Ifx_TLF35584_IF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.IF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_OverTemperatureFailureInterruptFlag)reg.B.OTF;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isOverTemperatureFailureInterruptFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_OverTemperatureFailureInterruptFlag value;
    Ifx_TLF35584_IF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.IF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.OTF == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearOverTemperatureFailureInterruptFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_IF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.IF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.OTF = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.IF) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_IF_OTF_MSK << IFX_TLF35584_IF_OTF_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_RequestedAbistOperationPerformedFlag IfxTlf35584_getRequestedAbistOperationPerformedFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_RequestedAbistOperationPerformedFlag value;
    Ifx_TLF35584_IF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.IF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_RequestedAbistOperationPerformedFlag)reg.B.ABIST;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isRequestedAbistOperationPerformedFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_RequestedAbistOperationPerformedFlag value;
    Ifx_TLF35584_IF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.IF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.ABIST == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearRequestedAbistOperationPerformedFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_IF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.IF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.ABIST = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.IF) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_IF_ABIST_MSK << IFX_TLF35584_IF_ABIST_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_InterruptNotServicedInTimeFlag IfxTlf35584_getInterruptNotServicedInTimeFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_InterruptNotServicedInTimeFlag value;
    Ifx_TLF35584_IF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.IF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_InterruptNotServicedInTimeFlag)reg.B.INTMISS;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isInterruptNotServicedInTimeFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_InterruptNotServicedInTimeFlag value;
    Ifx_TLF35584_IF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.IF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.INTMISS == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

IfxTlf35584_ProtectedConfigurationDoubleBitErrorFlag IfxTlf35584_getProtectedConfigurationDoubleBitErrorFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_ProtectedConfigurationDoubleBitErrorFlag value;
    Ifx_TLF35584_SYSSF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSSF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_ProtectedConfigurationDoubleBitErrorFlag)reg.B.CFGE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isProtectedConfigurationDoubleBitErrorFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_ProtectedConfigurationDoubleBitErrorFlag value;
    Ifx_TLF35584_SYSSF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSSF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.CFGE == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearProtectedConfigurationDoubleBitErrorFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_SYSSF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSSF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.CFGE = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSSF) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_SYSSF_CFGE_MSK << IFX_TLF35584_SYSSF_CFGE_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_WindowWatchdogErrorInterruptFlag IfxTlf35584_getWindowWatchdogErrorInterruptFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_WindowWatchdogErrorInterruptFlag value;
    Ifx_TLF35584_SYSSF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSSF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_WindowWatchdogErrorInterruptFlag)reg.B.WWDE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isWindowWatchdogErrorInterruptFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_WindowWatchdogErrorInterruptFlag value;
    Ifx_TLF35584_SYSSF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSSF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.WWDE == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearWindowWatchdogErrorInterruptFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_SYSSF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSSF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.WWDE = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSSF) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_SYSSF_WWDE_MSK << IFX_TLF35584_SYSSF_WWDE_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_FunctionalWatchdogErrorInterruptFlag IfxTlf35584_getFunctionalWatchdogErrorInterruptFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_FunctionalWatchdogErrorInterruptFlag value;
    Ifx_TLF35584_SYSSF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSSF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_FunctionalWatchdogErrorInterruptFlag)reg.B.FWDE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isFunctionalWatchdogErrorInterruptFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_FunctionalWatchdogErrorInterruptFlag value;
    Ifx_TLF35584_SYSSF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSSF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.FWDE == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearFunctionalWatchdogErrorInterruptFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_SYSSF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSSF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.FWDE = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSSF) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_SYSSF_FWDE_MSK << IFX_TLF35584_SYSSF_FWDE_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_McuErrorMissStatusFlag IfxTlf35584_getMcuErrorMissStatusFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_McuErrorMissStatusFlag value;
    Ifx_TLF35584_SYSSF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSSF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_McuErrorMissStatusFlag)reg.B.ERRMISS;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isMcuErrorMissStatusFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_McuErrorMissStatusFlag value;
    Ifx_TLF35584_SYSSF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSSF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.ERRMISS == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearMcuErrorMissStatusFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_SYSSF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSSF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.ERRMISS = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSSF) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_SYSSF_ERRMISS_MSK << IFX_TLF35584_SYSSF_ERRMISS_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_TransitionToLowPowerFailedFlag IfxTlf35584_getTransitionToLowPowerFailedFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_TransitionToLowPowerFailedFlag value;
    Ifx_TLF35584_SYSSF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSSF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_TransitionToLowPowerFailedFlag)reg.B.TRFAIL;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isTransitionToLowPowerFailedFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_TransitionToLowPowerFailedFlag value;
    Ifx_TLF35584_SYSSF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSSF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.TRFAIL == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearTransitionToLowPowerFailedFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_SYSSF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSSF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.TRFAIL = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSSF) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_SYSSF_TRFAIL_MSK << IFX_TLF35584_SYSSF_TRFAIL_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_StateTransitionRequestFailureFlag IfxTlf35584_getStateTransitionRequestFailureFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_StateTransitionRequestFailureFlag value;
    Ifx_TLF35584_SYSSF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSSF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_StateTransitionRequestFailureFlag)reg.B.NO_OP;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isStateTransitionRequestFailureFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_StateTransitionRequestFailureFlag value;
    Ifx_TLF35584_SYSSF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSSF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.NO_OP == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearStateTransitionRequestFailureFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_SYSSF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSSF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.NO_OP = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSSF) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_SYSSF_NO_OP_MSK << IFX_TLF35584_SYSSF_NO_OP_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_WakSignalWakeupFlag IfxTlf35584_getWakSignalWakeupFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_WakSignalWakeupFlag value;
    Ifx_TLF35584_WKSF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WKSF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_WakSignalWakeupFlag)reg.B.WAK;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isWakSignalWakeupFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_WakSignalWakeupFlag value;
    Ifx_TLF35584_WKSF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WKSF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.WAK == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearWakSignalWakeupFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_WKSF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WKSF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.WAK = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WKSF) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_WKSF_WAK_MSK << IFX_TLF35584_WKSF_WAK_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_EnaSignalWakeupFlag IfxTlf35584_getEnaSignalWakeupFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_EnaSignalWakeupFlag value;
    Ifx_TLF35584_WKSF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WKSF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_EnaSignalWakeupFlag)reg.B.ENA;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isEnaSignalWakeupFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_EnaSignalWakeupFlag value;
    Ifx_TLF35584_WKSF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WKSF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.ENA == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearEnaSignalWakeupFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_WKSF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WKSF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.ENA = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WKSF) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_WKSF_ENA_MSK << IFX_TLF35584_WKSF_ENA_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_QucCurrentMonitorThresholdWakeupFlag IfxTlf35584_getQucCurrentMonitorThresholdWakeupFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_QucCurrentMonitorThresholdWakeupFlag value;
    Ifx_TLF35584_WKSF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WKSF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_QucCurrentMonitorThresholdWakeupFlag)reg.B.CMON;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isQucCurrentMonitorThresholdWakeupFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_QucCurrentMonitorThresholdWakeupFlag value;
    Ifx_TLF35584_WKSF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WKSF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.CMON == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearQucCurrentMonitorThresholdWakeupFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_WKSF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WKSF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.CMON = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WKSF) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_WKSF_CMON_MSK << IFX_TLF35584_WKSF_CMON_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_WakeTimerWakeupFlag IfxTlf35584_getWakeTimerWakeupFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_WakeTimerWakeupFlag value;
    Ifx_TLF35584_WKSF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WKSF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_WakeTimerWakeupFlag)reg.B.WKTIM;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isWakeTimerWakeupFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_WakeTimerWakeupFlag value;
    Ifx_TLF35584_WKSF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WKSF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.WKTIM == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearWakeTimerWakeupFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_WKSF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WKSF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.WKTIM = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WKSF) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_WKSF_WKTIM_MSK << IFX_TLF35584_WKSF_WKTIM_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_WakeupFromSleepBySpiFlag IfxTlf35584_getWakeupFromSleepBySpiFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_WakeupFromSleepBySpiFlag value;
    Ifx_TLF35584_WKSF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WKSF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_WakeupFromSleepBySpiFlag)reg.B.WKSPI;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isWakeupFromSleepBySpiFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_WakeupFromSleepBySpiFlag value;
    Ifx_TLF35584_WKSF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WKSF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.WKSPI == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearWakeupFromSleepBySpiFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_WKSF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WKSF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.WKSPI = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WKSF) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_WKSF_WKSPI_MSK << IFX_TLF35584_WKSF_WKSPI_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_SpiFrameParityErrorFlag IfxTlf35584_getSpiFrameParityErrorFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_SpiFrameParityErrorFlag value;
    Ifx_TLF35584_SPISF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SPISF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_SpiFrameParityErrorFlag)reg.B.PARE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isSpiFrameParityErrorFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_SpiFrameParityErrorFlag value;
    Ifx_TLF35584_SPISF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SPISF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.PARE == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearSpiFrameParityErrorFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_SPISF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SPISF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.PARE = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SPISF) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_SPISF_PARE_MSK << IFX_TLF35584_SPISF_PARE_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_SpiFrameLengthInvalidFlag IfxTlf35584_getSpiFrameLengthInvalidFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_SpiFrameLengthInvalidFlag value;
    Ifx_TLF35584_SPISF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SPISF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_SpiFrameLengthInvalidFlag)reg.B.LENE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isSpiFrameLengthInvalidFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_SpiFrameLengthInvalidFlag value;
    Ifx_TLF35584_SPISF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SPISF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.LENE == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearSpiFrameLengthInvalidFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_SPISF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SPISF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.LENE = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SPISF) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_SPISF_LENE_MSK << IFX_TLF35584_SPISF_LENE_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_SpiAddressInvalidFlag IfxTlf35584_getSpiAddressInvalidFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_SpiAddressInvalidFlag value;
    Ifx_TLF35584_SPISF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SPISF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_SpiAddressInvalidFlag)reg.B.ADDRE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isSpiAddressInvalidFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_SpiAddressInvalidFlag value;
    Ifx_TLF35584_SPISF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SPISF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.ADDRE == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearSpiAddressInvalidFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_SPISF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SPISF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.ADDRE = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SPISF) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_SPISF_ADDRE_MSK << IFX_TLF35584_SPISF_ADDRE_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_SpiFrameDurationErrorFlag IfxTlf35584_getSpiFrameDurationErrorFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_SpiFrameDurationErrorFlag value;
    Ifx_TLF35584_SPISF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SPISF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_SpiFrameDurationErrorFlag)reg.B.DURE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isSpiFrameDurationErrorFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_SpiFrameDurationErrorFlag value;
    Ifx_TLF35584_SPISF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SPISF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.DURE == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearSpiFrameDurationErrorFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_SPISF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SPISF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.DURE = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SPISF) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_SPISF_DURE_MSK << IFX_TLF35584_SPISF_DURE_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_LockOrUnlockProcedureErrorFlag IfxTlf35584_getLockOrUnlockProcedureErrorFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_LockOrUnlockProcedureErrorFlag value;
    Ifx_TLF35584_SPISF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SPISF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_LockOrUnlockProcedureErrorFlag)reg.B.LOCK;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isLockOrUnlockProcedureErrorFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_LockOrUnlockProcedureErrorFlag value;
    Ifx_TLF35584_SPISF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SPISF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.LOCK == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearLockOrUnlockProcedureErrorFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_SPISF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SPISF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.LOCK = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SPISF) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_SPISF_LOCK_MSK << IFX_TLF35584_SPISF_LOCK_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_PreRegulatorVoltageShortToGroundStatusFlag IfxTlf35584_getPreRegulatorVoltageShortToGroundStatusFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_PreRegulatorVoltageShortToGroundStatusFlag value;
    Ifx_TLF35584_MONSF0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_PreRegulatorVoltageShortToGroundStatusFlag)reg.B.PREGSG;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isPreRegulatorVoltageShortToGroundStatusFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_PreRegulatorVoltageShortToGroundStatusFlag value;
    Ifx_TLF35584_MONSF0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.PREGSG == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearPreRegulatorVoltageShortToGroundStatusFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_MONSF0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.PREGSG = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF0) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_MONSF0_PREGSG_MSK << IFX_TLF35584_MONSF0_PREGSG_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_UcLdoShortToGroundStatusFlag IfxTlf35584_getUcLdoShortToGroundStatusFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_UcLdoShortToGroundStatusFlag value;
    Ifx_TLF35584_MONSF0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_UcLdoShortToGroundStatusFlag)reg.B.UCSG;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isUcLdoShortToGroundStatusFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_UcLdoShortToGroundStatusFlag value;
    Ifx_TLF35584_MONSF0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.UCSG == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearUcLdoShortToGroundStatusFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_MONSF0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.UCSG = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF0) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_MONSF0_UCSG_MSK << IFX_TLF35584_MONSF0_UCSG_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_StandbyLdoShortToGroundStatusFlag IfxTlf35584_getStandbyLdoShortToGroundStatusFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_StandbyLdoShortToGroundStatusFlag value;
    Ifx_TLF35584_MONSF0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_StandbyLdoShortToGroundStatusFlag)reg.B.STBYSG;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isStandbyLdoShortToGroundStatusFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_StandbyLdoShortToGroundStatusFlag value;
    Ifx_TLF35584_MONSF0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.STBYSG == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearStandbyLdoShortToGroundStatusFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_MONSF0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.STBYSG = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF0) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_MONSF0_STBYSG_MSK << IFX_TLF35584_MONSF0_STBYSG_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_CoreVoltageShortToGroundStatusFlag IfxTlf35584_getCoreVoltageShortToGroundStatusFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_CoreVoltageShortToGroundStatusFlag value;
    Ifx_TLF35584_MONSF0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_CoreVoltageShortToGroundStatusFlag)reg.B.VCORESG;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isCoreVoltageShortToGroundStatusFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_CoreVoltageShortToGroundStatusFlag value;
    Ifx_TLF35584_MONSF0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.VCORESG == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearCoreVoltageShortToGroundStatusFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_MONSF0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.VCORESG = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF0) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_MONSF0_VCORESG_MSK << IFX_TLF35584_MONSF0_VCORESG_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_CommunicationLdoShortToGroundStatusFlag IfxTlf35584_getCommunicationLdoShortToGroundStatusFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_CommunicationLdoShortToGroundStatusFlag value;
    Ifx_TLF35584_MONSF0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_CommunicationLdoShortToGroundStatusFlag)reg.B.COMSG;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isCommunicationLdoShortToGroundStatusFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_CommunicationLdoShortToGroundStatusFlag value;
    Ifx_TLF35584_MONSF0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.COMSG == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearCommunicationLdoShortToGroundStatusFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_MONSF0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.COMSG = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF0) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_MONSF0_COMSG_MSK << IFX_TLF35584_MONSF0_COMSG_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_VoltageReferenceShortToGroundStatusFlag IfxTlf35584_getVoltageReferenceShortToGroundStatusFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_VoltageReferenceShortToGroundStatusFlag value;
    Ifx_TLF35584_MONSF0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_VoltageReferenceShortToGroundStatusFlag)reg.B.VREFSG;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isVoltageReferenceShortToGroundStatusFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_VoltageReferenceShortToGroundStatusFlag value;
    Ifx_TLF35584_MONSF0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.VREFSG == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearVoltageReferenceShortToGroundStatusFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_MONSF0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.VREFSG = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF0) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_MONSF0_VREFSG_MSK << IFX_TLF35584_MONSF0_VREFSG_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_Tracker1ShortToGroundStatusFlag IfxTlf35584_getTracker1ShortToGroundStatusFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_Tracker1ShortToGroundStatusFlag value;
    Ifx_TLF35584_MONSF0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_Tracker1ShortToGroundStatusFlag)reg.B.TRK1SG;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isTracker1ShortToGroundStatusFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_Tracker1ShortToGroundStatusFlag value;
    Ifx_TLF35584_MONSF0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.TRK1SG == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearTracker1ShortToGroundStatusFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_MONSF0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.TRK1SG = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF0) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_MONSF0_TRK1SG_MSK << IFX_TLF35584_MONSF0_TRK1SG_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_Tracker2ShortToGroundStatusFlag IfxTlf35584_getTracker2ShortToGroundStatusFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_Tracker2ShortToGroundStatusFlag value;
    Ifx_TLF35584_MONSF0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_Tracker2ShortToGroundStatusFlag)reg.B.TRK2SG;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isTracker2ShortToGroundStatusFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_Tracker2ShortToGroundStatusFlag value;
    Ifx_TLF35584_MONSF0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.TRK2SG == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearTracker2ShortToGroundStatusFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_MONSF0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.TRK2SG = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF0) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_MONSF0_TRK2SG_MSK << IFX_TLF35584_MONSF0_TRK2SG_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_PreRegulatorVoltageOverVoltageStatusFlag IfxTlf35584_getPreRegulatorVoltageOverVoltageStatusFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_PreRegulatorVoltageOverVoltageStatusFlag value;
    Ifx_TLF35584_MONSF1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_PreRegulatorVoltageOverVoltageStatusFlag)reg.B.PREGOV;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isPreRegulatorVoltageOverVoltageStatusFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_PreRegulatorVoltageOverVoltageStatusFlag value;
    Ifx_TLF35584_MONSF1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.PREGOV == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearPreRegulatorVoltageOverVoltageStatusFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_MONSF1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.PREGOV = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF1) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_MONSF1_PREGOV_MSK << IFX_TLF35584_MONSF1_PREGOV_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_UcLdoOverVoltageStatusFlag IfxTlf35584_getUcLdoOverVoltageStatusFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_UcLdoOverVoltageStatusFlag value;
    Ifx_TLF35584_MONSF1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_UcLdoOverVoltageStatusFlag)reg.B.UCOV;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isUcLdoOverVoltageStatusFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_UcLdoOverVoltageStatusFlag value;
    Ifx_TLF35584_MONSF1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.UCOV == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearUcLdoOverVoltageStatusFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_MONSF1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.UCOV = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF1) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_MONSF1_UCOV_MSK << IFX_TLF35584_MONSF1_UCOV_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_StandbyLdoOverVoltageStatusFlag IfxTlf35584_getStandbyLdoOverVoltageStatusFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_StandbyLdoOverVoltageStatusFlag value;
    Ifx_TLF35584_MONSF1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_StandbyLdoOverVoltageStatusFlag)reg.B.STBYOV;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isStandbyLdoOverVoltageStatusFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_StandbyLdoOverVoltageStatusFlag value;
    Ifx_TLF35584_MONSF1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.STBYOV == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearStandbyLdoOverVoltageStatusFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_MONSF1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.STBYOV = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF1) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_MONSF1_STBYOV_MSK << IFX_TLF35584_MONSF1_STBYOV_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_CoreVoltageOverVoltageStatusFlag IfxTlf35584_getCoreVoltageOverVoltageStatusFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_CoreVoltageOverVoltageStatusFlag value;
    Ifx_TLF35584_MONSF1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_CoreVoltageOverVoltageStatusFlag)reg.B.VCOREOV;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isCoreVoltageOverVoltageStatusFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_CoreVoltageOverVoltageStatusFlag value;
    Ifx_TLF35584_MONSF1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.VCOREOV == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearCoreVoltageOverVoltageStatusFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_MONSF1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.VCOREOV = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF1) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_MONSF1_VCOREOV_MSK << IFX_TLF35584_MONSF1_VCOREOV_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_CommunicationLdoOverVoltageStatusFlag IfxTlf35584_getCommunicationLdoOverVoltageStatusFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_CommunicationLdoOverVoltageStatusFlag value;
    Ifx_TLF35584_MONSF1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_CommunicationLdoOverVoltageStatusFlag)reg.B.COMOV;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isCommunicationLdoOverVoltageStatusFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_CommunicationLdoOverVoltageStatusFlag value;
    Ifx_TLF35584_MONSF1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.COMOV == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearCommunicationLdoOverVoltageStatusFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_MONSF1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.COMOV = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF1) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_MONSF1_COMOV_MSK << IFX_TLF35584_MONSF1_COMOV_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_VoltageReferenceOverVoltageStatusFlag IfxTlf35584_getVoltageReferenceOverVoltageStatusFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_VoltageReferenceOverVoltageStatusFlag value;
    Ifx_TLF35584_MONSF1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_VoltageReferenceOverVoltageStatusFlag)reg.B.VREFOV;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isVoltageReferenceOverVoltageStatusFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_VoltageReferenceOverVoltageStatusFlag value;
    Ifx_TLF35584_MONSF1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.VREFOV == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearVoltageReferenceOverVoltageStatusFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_MONSF1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.VREFOV = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF1) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_MONSF1_VREFOV_MSK << IFX_TLF35584_MONSF1_VREFOV_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_Tracker1OverVoltageStatusFlag IfxTlf35584_getTracker1OverVoltageStatusFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_Tracker1OverVoltageStatusFlag value;
    Ifx_TLF35584_MONSF1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_Tracker1OverVoltageStatusFlag)reg.B.TRK1OV;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isTracker1OverVoltageStatusFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_Tracker1OverVoltageStatusFlag value;
    Ifx_TLF35584_MONSF1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.TRK1OV == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearTracker1OverVoltageStatusFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_MONSF1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.TRK1OV = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF1) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_MONSF1_TRK1OV_MSK << IFX_TLF35584_MONSF1_TRK1OV_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_Tracker2OverVoltageStatusFlag IfxTlf35584_getTracker2OverVoltageStatusFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_Tracker2OverVoltageStatusFlag value;
    Ifx_TLF35584_MONSF1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_Tracker2OverVoltageStatusFlag)reg.B.TRK2OV;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isTracker2OverVoltageStatusFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_Tracker2OverVoltageStatusFlag value;
    Ifx_TLF35584_MONSF1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.TRK2OV == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearTracker2OverVoltageStatusFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_MONSF1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.TRK2OV = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF1) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_MONSF1_TRK2OV_MSK << IFX_TLF35584_MONSF1_TRK2OV_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_PreRegulatorVoltageUnderVoltageStatusFlag IfxTlf35584_getPreRegulatorVoltageUnderVoltageStatusFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_PreRegulatorVoltageUnderVoltageStatusFlag value;
    Ifx_TLF35584_MONSF2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_PreRegulatorVoltageUnderVoltageStatusFlag)reg.B.PREGUV;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isPreRegulatorVoltageUnderVoltageStatusFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_PreRegulatorVoltageUnderVoltageStatusFlag value;
    Ifx_TLF35584_MONSF2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.PREGUV == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearPreRegulatorVoltageUnderVoltageStatusFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_MONSF2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.PREGUV = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF2) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_MONSF2_PREGUV_MSK << IFX_TLF35584_MONSF2_PREGUV_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_UcLdoUnderVoltageStatusFlag IfxTlf35584_getUcLdoUnderVoltageStatusFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_UcLdoUnderVoltageStatusFlag value;
    Ifx_TLF35584_MONSF2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_UcLdoUnderVoltageStatusFlag)reg.B.UCUV;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isUcLdoUnderVoltageStatusFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_UcLdoUnderVoltageStatusFlag value;
    Ifx_TLF35584_MONSF2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.UCUV == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearUcLdoUnderVoltageStatusFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_MONSF2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.UCUV = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF2) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_MONSF2_UCUV_MSK << IFX_TLF35584_MONSF2_UCUV_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_StandbyLdoUnderVoltageStatusFlag IfxTlf35584_getStandbyLdoUnderVoltageStatusFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_StandbyLdoUnderVoltageStatusFlag value;
    Ifx_TLF35584_MONSF2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_StandbyLdoUnderVoltageStatusFlag)reg.B.STBYUV;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isStandbyLdoUnderVoltageStatusFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_StandbyLdoUnderVoltageStatusFlag value;
    Ifx_TLF35584_MONSF2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.STBYUV == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearStandbyLdoUnderVoltageStatusFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_MONSF2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.STBYUV = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF2) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_MONSF2_STBYUV_MSK << IFX_TLF35584_MONSF2_STBYUV_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_CoreVoltageUnderVoltageStatusFlag IfxTlf35584_getCoreVoltageUnderVoltageStatusFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_CoreVoltageUnderVoltageStatusFlag value;
    Ifx_TLF35584_MONSF2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_CoreVoltageUnderVoltageStatusFlag)reg.B.VCOREUV;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isCoreVoltageUnderVoltageStatusFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_CoreVoltageUnderVoltageStatusFlag value;
    Ifx_TLF35584_MONSF2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.VCOREUV == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearCoreVoltageUnderVoltageStatusFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_MONSF2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.VCOREUV = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF2) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_MONSF2_VCOREUV_MSK << IFX_TLF35584_MONSF2_VCOREUV_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_CommunicationLdoUnderVoltageStatusFlag IfxTlf35584_getCommunicationLdoUnderVoltageStatusFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_CommunicationLdoUnderVoltageStatusFlag value;
    Ifx_TLF35584_MONSF2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_CommunicationLdoUnderVoltageStatusFlag)reg.B.COMUV;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isCommunicationLdoUnderVoltageStatusFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_CommunicationLdoUnderVoltageStatusFlag value;
    Ifx_TLF35584_MONSF2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.COMUV == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearCommunicationLdoUnderVoltageStatusFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_MONSF2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.COMUV = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF2) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_MONSF2_COMUV_MSK << IFX_TLF35584_MONSF2_COMUV_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_VoltageReferenceUnderVoltageStatusFlag IfxTlf35584_getVoltageReferenceUnderVoltageStatusFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_VoltageReferenceUnderVoltageStatusFlag value;
    Ifx_TLF35584_MONSF2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_VoltageReferenceUnderVoltageStatusFlag)reg.B.VREFUV;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isVoltageReferenceUnderVoltageStatusFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_VoltageReferenceUnderVoltageStatusFlag value;
    Ifx_TLF35584_MONSF2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.VREFUV == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearVoltageReferenceUnderVoltageStatusFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_MONSF2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.VREFUV = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF2) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_MONSF2_VREFUV_MSK << IFX_TLF35584_MONSF2_VREFUV_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_Tracker1UnderVoltageStatusFlag IfxTlf35584_getTracker1UnderVoltageStatusFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_Tracker1UnderVoltageStatusFlag value;
    Ifx_TLF35584_MONSF2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_Tracker1UnderVoltageStatusFlag)reg.B.TRK1UV;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isTracker1UnderVoltageStatusFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_Tracker1UnderVoltageStatusFlag value;
    Ifx_TLF35584_MONSF2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.TRK1UV == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearTracker1UnderVoltageStatusFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_MONSF2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.TRK1UV = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF2) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_MONSF2_TRK1UV_MSK << IFX_TLF35584_MONSF2_TRK1UV_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_Tracker2UnderVoltageStatusFlag IfxTlf35584_getTracker2UnderVoltageStatusFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_Tracker2UnderVoltageStatusFlag value;
    Ifx_TLF35584_MONSF2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_Tracker2UnderVoltageStatusFlag)reg.B.TRK2UV;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isTracker2UnderVoltageStatusFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_Tracker2UnderVoltageStatusFlag value;
    Ifx_TLF35584_MONSF2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.TRK2UV == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearTracker2UnderVoltageStatusFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_MONSF2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.TRK2UV = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF2) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_MONSF2_TRK2UV_MSK << IFX_TLF35584_MONSF2_TRK2UV_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_SupplyVoltageVs12OverVoltageFlag IfxTlf35584_getSupplyVoltageVs12OverVoltageFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_SupplyVoltageVs12OverVoltageFlag value;
    Ifx_TLF35584_MONSF3 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF3) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_SupplyVoltageVs12OverVoltageFlag)reg.B.VBATOV;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isSupplyVoltageVs12OverVoltageFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_SupplyVoltageVs12OverVoltageFlag value;
    Ifx_TLF35584_MONSF3 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF3) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.VBATOV == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearSupplyVoltageVs12OverVoltageFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_MONSF3 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF3) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.VBATOV = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF3) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_MONSF3_VBATOV_MSK << IFX_TLF35584_MONSF3_VBATOV_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_BandgapComparatorUnderVoltageConditionFlag IfxTlf35584_getBandgapComparatorUnderVoltageConditionFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_BandgapComparatorUnderVoltageConditionFlag value;
    Ifx_TLF35584_MONSF3 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF3) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_BandgapComparatorUnderVoltageConditionFlag)reg.B.BG12UV;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isBandgapComparatorUnderVoltageConditionFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_BandgapComparatorUnderVoltageConditionFlag value;
    Ifx_TLF35584_MONSF3 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF3) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.BG12UV == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearBandgapComparatorUnderVoltageConditionFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_MONSF3 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF3) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.BG12UV = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF3) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_MONSF3_BG12UV_MSK << IFX_TLF35584_MONSF3_BG12UV_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_BandgapComparatorOverVoltageConditionFlag IfxTlf35584_getBandgapComparatorOverVoltageConditionFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_BandgapComparatorOverVoltageConditionFlag value;
    Ifx_TLF35584_MONSF3 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF3) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_BandgapComparatorOverVoltageConditionFlag)reg.B.BG12OV;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isBandgapComparatorOverVoltageConditionFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_BandgapComparatorOverVoltageConditionFlag value;
    Ifx_TLF35584_MONSF3 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF3) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.BG12OV == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearBandgapComparatorOverVoltageConditionFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_MONSF3 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF3) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.BG12OV = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF3) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_MONSF3_BG12OV_MSK << IFX_TLF35584_MONSF3_BG12OV_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_BiasCurrentTooLowFlag IfxTlf35584_getBiasCurrentTooLowFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_BiasCurrentTooLowFlag value;
    Ifx_TLF35584_MONSF3 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF3) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_BiasCurrentTooLowFlag)reg.B.BIASLOW;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isBiasCurrentTooLowFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_BiasCurrentTooLowFlag value;
    Ifx_TLF35584_MONSF3 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF3) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.BIASLOW == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearBiasCurrentTooLowFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_MONSF3 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF3) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.BIASLOW = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF3) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_MONSF3_BIASLOW_MSK << IFX_TLF35584_MONSF3_BIASLOW_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_BiasCurrentTooHighFlag IfxTlf35584_getBiasCurrentTooHighFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_BiasCurrentTooHighFlag value;
    Ifx_TLF35584_MONSF3 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF3) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_BiasCurrentTooHighFlag)reg.B.BIASHI;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isBiasCurrentTooHighFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_BiasCurrentTooHighFlag value;
    Ifx_TLF35584_MONSF3 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF3) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.BIASHI == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearBiasCurrentTooHighFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_MONSF3 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF3) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.BIASHI = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.MONSF3) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_MONSF3_BIASHI_MSK << IFX_TLF35584_MONSF3_BIASHI_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_PreRegulatorOverTemperatureFlag IfxTlf35584_getPreRegulatorOverTemperatureFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_PreRegulatorOverTemperatureFlag value;
    Ifx_TLF35584_OTFAIL reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.OTFAIL) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_PreRegulatorOverTemperatureFlag)reg.B.PREG;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isPreRegulatorOverTemperatureFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_PreRegulatorOverTemperatureFlag value;
    Ifx_TLF35584_OTFAIL reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.OTFAIL) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.PREG == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearPreRegulatorOverTemperatureFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_OTFAIL reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.OTFAIL) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.PREG = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.OTFAIL) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_OTFAIL_PREG_MSK << IFX_TLF35584_OTFAIL_PREG_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_UcLdoOverTemperatureFlag IfxTlf35584_getUcLdoOverTemperatureFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_UcLdoOverTemperatureFlag value;
    Ifx_TLF35584_OTFAIL reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.OTFAIL) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_UcLdoOverTemperatureFlag)reg.B.UC;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isUcLdoOverTemperatureFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_UcLdoOverTemperatureFlag value;
    Ifx_TLF35584_OTFAIL reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.OTFAIL) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.UC == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearUcLdoOverTemperatureFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_OTFAIL reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.OTFAIL) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.UC = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.OTFAIL) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_OTFAIL_UC_MSK << IFX_TLF35584_OTFAIL_UC_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_CommunicationLdoOverTemperatureFlag IfxTlf35584_getCommunicationLdoOverTemperatureFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_CommunicationLdoOverTemperatureFlag value;
    Ifx_TLF35584_OTFAIL reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.OTFAIL) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_CommunicationLdoOverTemperatureFlag)reg.B.COM;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isCommunicationLdoOverTemperatureFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_CommunicationLdoOverTemperatureFlag value;
    Ifx_TLF35584_OTFAIL reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.OTFAIL) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.COM == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearCommunicationLdoOverTemperatureFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_OTFAIL reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.OTFAIL) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.COM = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.OTFAIL) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_OTFAIL_COM_MSK << IFX_TLF35584_OTFAIL_COM_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_MonitoringOverTemperatureFlag IfxTlf35584_getMonitoringOverTemperatureFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_MonitoringOverTemperatureFlag value;
    Ifx_TLF35584_OTFAIL reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.OTFAIL) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_MonitoringOverTemperatureFlag)reg.B.MON;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isMonitoringOverTemperatureFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_MonitoringOverTemperatureFlag value;
    Ifx_TLF35584_OTFAIL reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.OTFAIL) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.MON == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearMonitoringOverTemperatureFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_OTFAIL reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.OTFAIL) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.MON = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.OTFAIL) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_OTFAIL_MON_MSK << IFX_TLF35584_OTFAIL_MON_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_PreRegulatorOverTemperatureWarningFlag IfxTlf35584_getPreRegulatorOverTemperatureWarningFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_PreRegulatorOverTemperatureWarningFlag value;
    Ifx_TLF35584_OTWRNSF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.OTWRNSF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_PreRegulatorOverTemperatureWarningFlag)reg.B.PREG;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isPreRegulatorOverTemperatureWarningFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_PreRegulatorOverTemperatureWarningFlag value;
    Ifx_TLF35584_OTWRNSF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.OTWRNSF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.PREG == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearPreRegulatorOverTemperatureWarningFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_OTWRNSF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.OTWRNSF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.PREG = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.OTWRNSF) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_OTWRNSF_PREG_MSK << IFX_TLF35584_OTWRNSF_PREG_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_UcLdoOverTemperatureWarningFlag IfxTlf35584_getUcLdoOverTemperatureWarningFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_UcLdoOverTemperatureWarningFlag value;
    Ifx_TLF35584_OTWRNSF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.OTWRNSF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_UcLdoOverTemperatureWarningFlag)reg.B.UC;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isUcLdoOverTemperatureWarningFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_UcLdoOverTemperatureWarningFlag value;
    Ifx_TLF35584_OTWRNSF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.OTWRNSF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.UC == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearUcLdoOverTemperatureWarningFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_OTWRNSF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.OTWRNSF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.UC = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.OTWRNSF) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_OTWRNSF_UC_MSK << IFX_TLF35584_OTWRNSF_UC_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_StandbyLdoOverLoadFlag IfxTlf35584_getStandbyLdoOverLoadFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_StandbyLdoOverLoadFlag value;
    Ifx_TLF35584_OTWRNSF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.OTWRNSF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_StandbyLdoOverLoadFlag)reg.B.STDBY;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isStandbyLdoOverLoadFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_StandbyLdoOverLoadFlag value;
    Ifx_TLF35584_OTWRNSF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.OTWRNSF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.STDBY == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearStandbyLdoOverLoadFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_OTWRNSF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.OTWRNSF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.STDBY = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.OTWRNSF) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_OTWRNSF_STDBY_MSK << IFX_TLF35584_OTWRNSF_STDBY_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_CommunicationLdoOverTemperatureWarningFlag IfxTlf35584_getCommunicationLdoOverTemperatureWarningFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_CommunicationLdoOverTemperatureWarningFlag value;
    Ifx_TLF35584_OTWRNSF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.OTWRNSF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_CommunicationLdoOverTemperatureWarningFlag)reg.B.COM;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isCommunicationLdoOverTemperatureWarningFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_CommunicationLdoOverTemperatureWarningFlag value;
    Ifx_TLF35584_OTWRNSF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.OTWRNSF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.COM == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearCommunicationLdoOverTemperatureWarningFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_OTWRNSF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.OTWRNSF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.COM = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.OTWRNSF) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_OTWRNSF_COM_MSK << IFX_TLF35584_OTWRNSF_COM_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_VoltageReferenceOverLoadFlag IfxTlf35584_getVoltageReferenceOverLoadFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_VoltageReferenceOverLoadFlag value;
    Ifx_TLF35584_OTWRNSF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.OTWRNSF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_VoltageReferenceOverLoadFlag)reg.B.VREF;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isVoltageReferenceOverLoadFlagSet(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_VoltageReferenceOverLoadFlag value;
    Ifx_TLF35584_OTWRNSF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.OTWRNSF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.VREF == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_clearVoltageReferenceOverLoadFlag(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_OTWRNSF reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.OTWRNSF) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.VREF = 1;
        *status &= IfxTlf35584_Driver_writeRegisterStar(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.OTWRNSF) >> IFXTLF35584_ADDRESS_SHIFT), reg.U, IFX_TLF35584_OTWRNSF_VREF_MSK << IFX_TLF35584_OTWRNSF_VREF_OFF);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_StandbyLdoVoltageReadyStatus IfxTlf35584_getStandbyLdoVoltageReadyStatus(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_StandbyLdoVoltageReadyStatus value;
    Ifx_TLF35584_VMONSTAT reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.VMONSTAT) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_StandbyLdoVoltageReadyStatus)reg.B.STBYST;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isStandbyLdoVoltageReady(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_StandbyLdoVoltageReadyStatus value;
    Ifx_TLF35584_VMONSTAT reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.VMONSTAT) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.STBYST == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

IfxTlf35584_CoreVoltageReadyStatus IfxTlf35584_getCoreVoltageReadyStatus(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_CoreVoltageReadyStatus value;
    Ifx_TLF35584_VMONSTAT reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.VMONSTAT) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_CoreVoltageReadyStatus)reg.B.VCOREST;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isCoreVoltageReady(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_CoreVoltageReadyStatus value;
    Ifx_TLF35584_VMONSTAT reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.VMONSTAT) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.VCOREST == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

IfxTlf35584_CommunicationLdoVoltageReadyStatus IfxTlf35584_getCommunicationLdoVoltageReadyStatus(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_CommunicationLdoVoltageReadyStatus value;
    Ifx_TLF35584_VMONSTAT reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.VMONSTAT) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_CommunicationLdoVoltageReadyStatus)reg.B.COMST;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isCommunicationLdoVoltageReady(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_CommunicationLdoVoltageReadyStatus value;
    Ifx_TLF35584_VMONSTAT reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.VMONSTAT) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.COMST == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

IfxTlf35584_VoltageReferenceVoltageReadyStatus IfxTlf35584_getVoltageReferenceVoltageReadyStatus(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_VoltageReferenceVoltageReadyStatus value;
    Ifx_TLF35584_VMONSTAT reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.VMONSTAT) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_VoltageReferenceVoltageReadyStatus)reg.B.VREFST;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isVoltageReferenceVoltageReady(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_VoltageReferenceVoltageReadyStatus value;
    Ifx_TLF35584_VMONSTAT reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.VMONSTAT) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.VREFST == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

IfxTlf35584_Tracker1VoltageReadyStatus IfxTlf35584_getTracker1VoltageReadyStatus(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_Tracker1VoltageReadyStatus value;
    Ifx_TLF35584_VMONSTAT reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.VMONSTAT) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_Tracker1VoltageReadyStatus)reg.B.TRK1ST;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isTracker1VoltageReady(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_Tracker1VoltageReadyStatus value;
    Ifx_TLF35584_VMONSTAT reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.VMONSTAT) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.TRK1ST == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

IfxTlf35584_Tracker2VoltageReadyStatus IfxTlf35584_getTracker2VoltageReadyStatus(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_Tracker2VoltageReadyStatus value;
    Ifx_TLF35584_VMONSTAT reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.VMONSTAT) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_Tracker2VoltageReadyStatus)reg.B.TRK2ST;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isTracker2VoltageReady(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_Tracker2VoltageReadyStatus value;
    Ifx_TLF35584_VMONSTAT reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.VMONSTAT) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.TRK2ST == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

IfxTlf35584_DeviceState IfxTlf35584_getDeviceState(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_DeviceState value;
    Ifx_TLF35584_DEVSTAT reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVSTAT) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_DeviceState)reg.B.STATE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

IfxTlf35584_ReferenceVoltageEnableStatus IfxTlf35584_getReferenceVoltageEnableStatus(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_ReferenceVoltageEnableStatus value;
    Ifx_TLF35584_DEVSTAT reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVSTAT) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_ReferenceVoltageEnableStatus)reg.B.VREFEN;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isReferenceVoltageEnabled(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_ReferenceVoltageEnableStatus value;
    Ifx_TLF35584_DEVSTAT reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVSTAT) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.VREFEN == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

IfxTlf35584_StandbyLdoEnableStatus IfxTlf35584_getStandbyLdoEnableStatus(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_StandbyLdoEnableStatus value;
    Ifx_TLF35584_DEVSTAT reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVSTAT) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_StandbyLdoEnableStatus)reg.B.STBYEN;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isStandbyLdoEnabled(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_StandbyLdoEnableStatus value;
    Ifx_TLF35584_DEVSTAT reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVSTAT) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.STBYEN == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

IfxTlf35584_CommunicationLdoEnableStatus IfxTlf35584_getCommunicationLdoEnableStatus(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_CommunicationLdoEnableStatus value;
    Ifx_TLF35584_DEVSTAT reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVSTAT) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_CommunicationLdoEnableStatus)reg.B.COMEN;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isCommunicationLdoEnabled(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_CommunicationLdoEnableStatus value;
    Ifx_TLF35584_DEVSTAT reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVSTAT) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.COMEN == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

IfxTlf35584_Tracker1VoltageEnableStatus IfxTlf35584_getTracker1VoltageEnableStatus(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_Tracker1VoltageEnableStatus value;
    Ifx_TLF35584_DEVSTAT reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVSTAT) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_Tracker1VoltageEnableStatus)reg.B.TRK1EN;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isTracker1VoltageEnabled(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_Tracker1VoltageEnableStatus value;
    Ifx_TLF35584_DEVSTAT reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVSTAT) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.TRK1EN == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

IfxTlf35584_Tracker2VoltageEnableStatus IfxTlf35584_getTracker2VoltageEnableStatus(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_Tracker2VoltageEnableStatus value;
    Ifx_TLF35584_DEVSTAT reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVSTAT) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_Tracker2VoltageEnableStatus)reg.B.TRK2EN;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isTracker2VoltageEnabled(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_Tracker2VoltageEnableStatus value;
    Ifx_TLF35584_DEVSTAT reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVSTAT) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.TRK2EN == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

IfxTlf35584_ProtectedRegisterLockStatus IfxTlf35584_getProtectedRegisterLockStatus(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_ProtectedRegisterLockStatus value;
    Ifx_TLF35584_PROTSTAT reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.PROTSTAT) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_ProtectedRegisterLockStatus)reg.B.LOCK;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isProtectedRegisterLocked(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_ProtectedRegisterLockStatus value;
    Ifx_TLF35584_PROTSTAT reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.PROTSTAT) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.LOCK == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

IfxTlf35584_Key1OkStatus IfxTlf35584_getKey1OkStatus(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_Key1OkStatus value;
    Ifx_TLF35584_PROTSTAT reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.PROTSTAT) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_Key1OkStatus)reg.B.KEY1OK;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

IfxTlf35584_Key2OkStatus IfxTlf35584_getKey2OkStatus(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_Key2OkStatus value;
    Ifx_TLF35584_PROTSTAT reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.PROTSTAT) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_Key2OkStatus)reg.B.KEY2OK;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

IfxTlf35584_Key3OkStatus IfxTlf35584_getKey3OkStatus(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_Key3OkStatus value;
    Ifx_TLF35584_PROTSTAT reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.PROTSTAT) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_Key3OkStatus)reg.B.KEY3OK;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

IfxTlf35584_Key4OkStatus IfxTlf35584_getKey4OkStatus(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_Key4OkStatus value;
    Ifx_TLF35584_PROTSTAT reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.PROTSTAT) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_Key4OkStatus)reg.B.KEY4OK;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

uint8 IfxTlf35584_getWindowWatchdogErrorCounterStatus(IfxTlf35584_Driver* driver, boolean* status)
{
    uint8 value;
    Ifx_TLF35584_WWDSTAT reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WWDSTAT) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (uint8)reg.B.WWDECNT;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

uint8 IfxTlf35584_getFunctionalWatchdogQuestion(IfxTlf35584_Driver* driver, boolean* status)
{
    uint8 value;
    Ifx_TLF35584_FWDSTAT0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.FWDSTAT0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (uint8)reg.B.FWDQUEST;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

uint8 IfxTlf35584_getFunctionalWatchdogResponseCounterValue(IfxTlf35584_Driver* driver, boolean* status)
{
    uint8 value;
    Ifx_TLF35584_FWDSTAT0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.FWDSTAT0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (uint8)reg.B.FWDRSPC;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

IfxTlf35584_FunctionalWatchdogResponseCheckErrorStatus IfxTlf35584_getFunctionalWatchdogResponseCheckErrorStatus(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_FunctionalWatchdogResponseCheckErrorStatus value;
    Ifx_TLF35584_FWDSTAT0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.FWDSTAT0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_FunctionalWatchdogResponseCheckErrorStatus)reg.B.FWDRSPOK;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

uint8 IfxTlf35584_getFunctionalWatchdogErrorCounterValue(IfxTlf35584_Driver* driver, boolean* status)
{
    uint8 value;
    Ifx_TLF35584_FWDSTAT1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.FWDSTAT1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (uint8)reg.B.FWDECNT;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

IfxTlf35584_StartAbistOperation IfxTlf35584_getStartAbistOperation(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_StartAbistOperation value;
    Ifx_TLF35584_ABIST_CTRL0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_CTRL0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_StartAbistOperation)reg.B.START;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_setStartAbistOperation(IfxTlf35584_Driver* driver, IfxTlf35584_StartAbistOperation value, boolean* status)
{
    Ifx_TLF35584_ABIST_CTRL0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_CTRL0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.START = value;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_CTRL0) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_FullPathTestSelection IfxTlf35584_getFullPathTestSelection(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_FullPathTestSelection value;
    Ifx_TLF35584_ABIST_CTRL0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_CTRL0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_FullPathTestSelection)reg.B.PATH;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_setFullPathTestSelection(IfxTlf35584_Driver* driver, IfxTlf35584_FullPathTestSelection value, boolean* status)
{
    Ifx_TLF35584_ABIST_CTRL0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_CTRL0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.PATH = value;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_CTRL0) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_AbistSequenceSelection IfxTlf35584_getAbistSequenceSelection(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_AbistSequenceSelection value;
    Ifx_TLF35584_ABIST_CTRL0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_CTRL0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_AbistSequenceSelection)reg.B.SINGLE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_setAbistSequenceSelection(IfxTlf35584_Driver* driver, IfxTlf35584_AbistSequenceSelection value, boolean* status)
{
    Ifx_TLF35584_ABIST_CTRL0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_CTRL0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.SINGLE = value;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_CTRL0) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_SafetyPathSelection IfxTlf35584_getSafetyPathSelection(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_SafetyPathSelection value;
    Ifx_TLF35584_ABIST_CTRL0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_CTRL0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_SafetyPathSelection)reg.B.INT;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_setSafetyPathSelection(IfxTlf35584_Driver* driver, IfxTlf35584_SafetyPathSelection value, boolean* status)
{
    Ifx_TLF35584_ABIST_CTRL0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_CTRL0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.INT = value;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_CTRL0) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_AbistGlobalErrorStatus IfxTlf35584_getAbistGlobalErrorStatus(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_AbistGlobalErrorStatus value;
    Ifx_TLF35584_ABIST_CTRL0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_CTRL0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_AbistGlobalErrorStatus)reg.B.STATUS;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

IfxTlf35584_OvervoltageTriggerForSecondaryInternalMonitorEnable IfxTlf35584_getOvervoltageTriggerForSecondaryInternalMonitorEnable(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_OvervoltageTriggerForSecondaryInternalMonitorEnable value;
    Ifx_TLF35584_ABIST_CTRL1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_CTRL1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_OvervoltageTriggerForSecondaryInternalMonitorEnable)reg.B.OV_TRIG;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isOvervoltageTriggerForSecondaryInternalMonitorEnabled(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_OvervoltageTriggerForSecondaryInternalMonitorEnable value;
    Ifx_TLF35584_ABIST_CTRL1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_CTRL1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.OV_TRIG == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_setOvervoltageTriggerForSecondaryInternalMonitorEnable(IfxTlf35584_Driver* driver, IfxTlf35584_OvervoltageTriggerForSecondaryInternalMonitorEnable value, boolean* status)
{
    Ifx_TLF35584_ABIST_CTRL1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_CTRL1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.OV_TRIG = value;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_CTRL1) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void IfxTlf35584_enableOvervoltageTriggerForSecondaryInternalMonitor(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_ABIST_CTRL1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_CTRL1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.OV_TRIG = 1;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_CTRL1) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void IfxTlf35584_disableOvervoltageTriggerForSecondaryInternalMonitor(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_ABIST_CTRL1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_CTRL1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.OV_TRIG = 0;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_CTRL1) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_AbistClockCheckEnable IfxTlf35584_getAbistClockCheckEnable(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_AbistClockCheckEnable value;
    Ifx_TLF35584_ABIST_CTRL1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_CTRL1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_AbistClockCheckEnable)reg.B.ABIST_CLK_EN;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isAbistClockCheckEnabled(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_AbistClockCheckEnable value;
    Ifx_TLF35584_ABIST_CTRL1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_CTRL1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.ABIST_CLK_EN == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_setAbistClockCheckEnable(IfxTlf35584_Driver* driver, IfxTlf35584_AbistClockCheckEnable value, boolean* status)
{
    Ifx_TLF35584_ABIST_CTRL1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_CTRL1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.ABIST_CLK_EN = value;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_CTRL1) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void IfxTlf35584_enableAbistClockCheck(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_ABIST_CTRL1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_CTRL1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.ABIST_CLK_EN = 1;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_CTRL1) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void IfxTlf35584_disableAbistClockCheck(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_ABIST_CTRL1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_CTRL1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.ABIST_CLK_EN = 0;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_CTRL1) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_SelectPreRegulatorOvComparatorForAbistOperationEnable IfxTlf35584_getSelectPreRegulatorOvComparatorForAbistOperationEnable(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_SelectPreRegulatorOvComparatorForAbistOperationEnable value;
    Ifx_TLF35584_ABIST_SELECT0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_SelectPreRegulatorOvComparatorForAbistOperationEnable)reg.B.PREGOV;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isSelectPreRegulatorOvComparatorForAbistOperationEnabled(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_SelectPreRegulatorOvComparatorForAbistOperationEnable value;
    Ifx_TLF35584_ABIST_SELECT0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.PREGOV == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_setSelectPreRegulatorOvComparatorForAbistOperationEnable(IfxTlf35584_Driver* driver, IfxTlf35584_SelectPreRegulatorOvComparatorForAbistOperationEnable value, boolean* status)
{
    Ifx_TLF35584_ABIST_SELECT0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.PREGOV = value;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT0) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void IfxTlf35584_enableSelectPreRegulatorOvComparatorForAbistOperation(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_ABIST_SELECT0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.PREGOV = 1;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT0) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void IfxTlf35584_disableSelectPreRegulatorOvComparatorForAbistOperation(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_ABIST_SELECT0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.PREGOV = 0;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT0) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_SelectUcLdoOvComparatorForAbistOperationEnable IfxTlf35584_getSelectUcLdoOvComparatorForAbistOperationEnable(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_SelectUcLdoOvComparatorForAbistOperationEnable value;
    Ifx_TLF35584_ABIST_SELECT0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_SelectUcLdoOvComparatorForAbistOperationEnable)reg.B.UCOV;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isSelectUcLdoOvComparatorForAbistOperationEnabled(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_SelectUcLdoOvComparatorForAbistOperationEnable value;
    Ifx_TLF35584_ABIST_SELECT0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.UCOV == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_setSelectUcLdoOvComparatorForAbistOperationEnable(IfxTlf35584_Driver* driver, IfxTlf35584_SelectUcLdoOvComparatorForAbistOperationEnable value, boolean* status)
{
    Ifx_TLF35584_ABIST_SELECT0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.UCOV = value;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT0) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void IfxTlf35584_enableSelectUcLdoOvComparatorForAbistOperation(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_ABIST_SELECT0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.UCOV = 1;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT0) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void IfxTlf35584_disableSelectUcLdoOvComparatorForAbistOperation(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_ABIST_SELECT0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.UCOV = 0;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT0) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_SelectStandbyLdoOvComparatorForAbistOperationEnable IfxTlf35584_getSelectStandbyLdoOvComparatorForAbistOperationEnable(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_SelectStandbyLdoOvComparatorForAbistOperationEnable value;
    Ifx_TLF35584_ABIST_SELECT0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_SelectStandbyLdoOvComparatorForAbistOperationEnable)reg.B.STBYOV;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isSelectStandbyLdoOvComparatorForAbistOperationEnabled(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_SelectStandbyLdoOvComparatorForAbistOperationEnable value;
    Ifx_TLF35584_ABIST_SELECT0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.STBYOV == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_setSelectStandbyLdoOvComparatorForAbistOperationEnable(IfxTlf35584_Driver* driver, IfxTlf35584_SelectStandbyLdoOvComparatorForAbistOperationEnable value, boolean* status)
{
    Ifx_TLF35584_ABIST_SELECT0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.STBYOV = value;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT0) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void IfxTlf35584_enableSelectStandbyLdoOvComparatorForAbistOperation(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_ABIST_SELECT0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.STBYOV = 1;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT0) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void IfxTlf35584_disableSelectStandbyLdoOvComparatorForAbistOperation(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_ABIST_SELECT0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.STBYOV = 0;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT0) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_SelectCoreVoltageOvComparatorForAbistOperationEnable IfxTlf35584_getSelectCoreVoltageOvComparatorForAbistOperationEnable(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_SelectCoreVoltageOvComparatorForAbistOperationEnable value;
    Ifx_TLF35584_ABIST_SELECT0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_SelectCoreVoltageOvComparatorForAbistOperationEnable)reg.B.VCOREOV;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isSelectCoreVoltageOvComparatorForAbistOperationEnabled(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_SelectCoreVoltageOvComparatorForAbistOperationEnable value;
    Ifx_TLF35584_ABIST_SELECT0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.VCOREOV == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_setSelectCoreVoltageOvComparatorForAbistOperationEnable(IfxTlf35584_Driver* driver, IfxTlf35584_SelectCoreVoltageOvComparatorForAbistOperationEnable value, boolean* status)
{
    Ifx_TLF35584_ABIST_SELECT0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.VCOREOV = value;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT0) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void IfxTlf35584_enableSelectCoreVoltageOvComparatorForAbistOperation(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_ABIST_SELECT0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.VCOREOV = 1;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT0) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void IfxTlf35584_disableSelectCoreVoltageOvComparatorForAbistOperation(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_ABIST_SELECT0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.VCOREOV = 0;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT0) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_SelectComOvComparatorForAbistOperationEnable IfxTlf35584_getSelectComOvComparatorForAbistOperationEnable(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_SelectComOvComparatorForAbistOperationEnable value;
    Ifx_TLF35584_ABIST_SELECT0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_SelectComOvComparatorForAbistOperationEnable)reg.B.COMOV;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isSelectComOvComparatorForAbistOperationEnabled(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_SelectComOvComparatorForAbistOperationEnable value;
    Ifx_TLF35584_ABIST_SELECT0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.COMOV == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_setSelectComOvComparatorForAbistOperationEnable(IfxTlf35584_Driver* driver, IfxTlf35584_SelectComOvComparatorForAbistOperationEnable value, boolean* status)
{
    Ifx_TLF35584_ABIST_SELECT0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.COMOV = value;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT0) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void IfxTlf35584_enableSelectComOvComparatorForAbistOperation(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_ABIST_SELECT0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.COMOV = 1;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT0) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void IfxTlf35584_disableSelectComOvComparatorForAbistOperation(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_ABIST_SELECT0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.COMOV = 0;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT0) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_SelectVrefOvComparatorForAbistOperationEnable IfxTlf35584_getSelectVrefOvComparatorForAbistOperationEnable(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_SelectVrefOvComparatorForAbistOperationEnable value;
    Ifx_TLF35584_ABIST_SELECT0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_SelectVrefOvComparatorForAbistOperationEnable)reg.B.VREFOV;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isSelectVrefOvComparatorForAbistOperationEnabled(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_SelectVrefOvComparatorForAbistOperationEnable value;
    Ifx_TLF35584_ABIST_SELECT0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.VREFOV == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_setSelectVrefOvComparatorForAbistOperationEnable(IfxTlf35584_Driver* driver, IfxTlf35584_SelectVrefOvComparatorForAbistOperationEnable value, boolean* status)
{
    Ifx_TLF35584_ABIST_SELECT0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.VREFOV = value;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT0) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void IfxTlf35584_enableSelectVrefOvComparatorForAbistOperation(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_ABIST_SELECT0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.VREFOV = 1;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT0) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void IfxTlf35584_disableSelectVrefOvComparatorForAbistOperation(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_ABIST_SELECT0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.VREFOV = 0;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT0) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_SelectTrk1OvComparatorForAbistOperationEnable IfxTlf35584_getSelectTrk1OvComparatorForAbistOperationEnable(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_SelectTrk1OvComparatorForAbistOperationEnable value;
    Ifx_TLF35584_ABIST_SELECT0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_SelectTrk1OvComparatorForAbistOperationEnable)reg.B.TRK1OV;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isSelectTrk1OvComparatorForAbistOperationEnabled(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_SelectTrk1OvComparatorForAbistOperationEnable value;
    Ifx_TLF35584_ABIST_SELECT0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.TRK1OV == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_setSelectTrk1OvComparatorForAbistOperationEnable(IfxTlf35584_Driver* driver, IfxTlf35584_SelectTrk1OvComparatorForAbistOperationEnable value, boolean* status)
{
    Ifx_TLF35584_ABIST_SELECT0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.TRK1OV = value;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT0) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void IfxTlf35584_enableSelectTrk1OvComparatorForAbistOperation(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_ABIST_SELECT0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.TRK1OV = 1;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT0) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void IfxTlf35584_disableSelectTrk1OvComparatorForAbistOperation(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_ABIST_SELECT0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.TRK1OV = 0;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT0) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_SelectTrk2OvComparatorForAbistOperationEnable IfxTlf35584_getSelectTrk2OvComparatorForAbistOperationEnable(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_SelectTrk2OvComparatorForAbistOperationEnable value;
    Ifx_TLF35584_ABIST_SELECT0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_SelectTrk2OvComparatorForAbistOperationEnable)reg.B.TRK2OV;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isSelectTrk2OvComparatorForAbistOperationEnabled(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_SelectTrk2OvComparatorForAbistOperationEnable value;
    Ifx_TLF35584_ABIST_SELECT0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.TRK2OV == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_setSelectTrk2OvComparatorForAbistOperationEnable(IfxTlf35584_Driver* driver, IfxTlf35584_SelectTrk2OvComparatorForAbistOperationEnable value, boolean* status)
{
    Ifx_TLF35584_ABIST_SELECT0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.TRK2OV = value;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT0) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void IfxTlf35584_enableSelectTrk2OvComparatorForAbistOperation(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_ABIST_SELECT0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.TRK2OV = 1;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT0) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void IfxTlf35584_disableSelectTrk2OvComparatorForAbistOperation(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_ABIST_SELECT0 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT0) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.TRK2OV = 0;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT0) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_SelectPreRegulatorUvComparatorForAbistOperationEnable IfxTlf35584_getSelectPreRegulatorUvComparatorForAbistOperationEnable(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_SelectPreRegulatorUvComparatorForAbistOperationEnable value;
    Ifx_TLF35584_ABIST_SELECT1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_SelectPreRegulatorUvComparatorForAbistOperationEnable)reg.B.PREGUV;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isSelectPreRegulatorUvComparatorForAbistOperationEnabled(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_SelectPreRegulatorUvComparatorForAbistOperationEnable value;
    Ifx_TLF35584_ABIST_SELECT1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.PREGUV == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_setSelectPreRegulatorUvComparatorForAbistOperationEnable(IfxTlf35584_Driver* driver, IfxTlf35584_SelectPreRegulatorUvComparatorForAbistOperationEnable value, boolean* status)
{
    Ifx_TLF35584_ABIST_SELECT1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.PREGUV = value;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT1) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void IfxTlf35584_enableSelectPreRegulatorUvComparatorForAbistOperation(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_ABIST_SELECT1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.PREGUV = 1;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT1) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void IfxTlf35584_disableSelectPreRegulatorUvComparatorForAbistOperation(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_ABIST_SELECT1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.PREGUV = 0;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT1) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_SelectUcUvComparatorForAbistOperationEnable IfxTlf35584_getSelectUcUvComparatorForAbistOperationEnable(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_SelectUcUvComparatorForAbistOperationEnable value;
    Ifx_TLF35584_ABIST_SELECT1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_SelectUcUvComparatorForAbistOperationEnable)reg.B.UCUV;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isSelectUcUvComparatorForAbistOperationEnabled(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_SelectUcUvComparatorForAbistOperationEnable value;
    Ifx_TLF35584_ABIST_SELECT1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.UCUV == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_setSelectUcUvComparatorForAbistOperationEnable(IfxTlf35584_Driver* driver, IfxTlf35584_SelectUcUvComparatorForAbistOperationEnable value, boolean* status)
{
    Ifx_TLF35584_ABIST_SELECT1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.UCUV = value;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT1) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void IfxTlf35584_enableSelectUcUvComparatorForAbistOperation(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_ABIST_SELECT1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.UCUV = 1;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT1) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void IfxTlf35584_disableSelectUcUvComparatorForAbistOperation(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_ABIST_SELECT1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.UCUV = 0;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT1) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_SelectStbyUvComparatorForAbistOperationEnable IfxTlf35584_getSelectStbyUvComparatorForAbistOperationEnable(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_SelectStbyUvComparatorForAbistOperationEnable value;
    Ifx_TLF35584_ABIST_SELECT1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_SelectStbyUvComparatorForAbistOperationEnable)reg.B.STBYUV;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isSelectStbyUvComparatorForAbistOperationEnabled(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_SelectStbyUvComparatorForAbistOperationEnable value;
    Ifx_TLF35584_ABIST_SELECT1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.STBYUV == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_setSelectStbyUvComparatorForAbistOperationEnable(IfxTlf35584_Driver* driver, IfxTlf35584_SelectStbyUvComparatorForAbistOperationEnable value, boolean* status)
{
    Ifx_TLF35584_ABIST_SELECT1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.STBYUV = value;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT1) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void IfxTlf35584_enableSelectStbyUvComparatorForAbistOperation(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_ABIST_SELECT1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.STBYUV = 1;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT1) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void IfxTlf35584_disableSelectStbyUvComparatorForAbistOperation(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_ABIST_SELECT1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.STBYUV = 0;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT1) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_SelectVcoreUvComparatorForAbistOperationEnable IfxTlf35584_getSelectVcoreUvComparatorForAbistOperationEnable(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_SelectVcoreUvComparatorForAbistOperationEnable value;
    Ifx_TLF35584_ABIST_SELECT1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_SelectVcoreUvComparatorForAbistOperationEnable)reg.B.VCOREUV;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isSelectVcoreUvComparatorForAbistOperationEnabled(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_SelectVcoreUvComparatorForAbistOperationEnable value;
    Ifx_TLF35584_ABIST_SELECT1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.VCOREUV == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_setSelectVcoreUvComparatorForAbistOperationEnable(IfxTlf35584_Driver* driver, IfxTlf35584_SelectVcoreUvComparatorForAbistOperationEnable value, boolean* status)
{
    Ifx_TLF35584_ABIST_SELECT1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.VCOREUV = value;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT1) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void IfxTlf35584_enableSelectVcoreUvComparatorForAbistOperation(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_ABIST_SELECT1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.VCOREUV = 1;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT1) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void IfxTlf35584_disableSelectVcoreUvComparatorForAbistOperation(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_ABIST_SELECT1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.VCOREUV = 0;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT1) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_SelectComUvComparatorForAbistOperationEnable IfxTlf35584_getSelectComUvComparatorForAbistOperationEnable(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_SelectComUvComparatorForAbistOperationEnable value;
    Ifx_TLF35584_ABIST_SELECT1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_SelectComUvComparatorForAbistOperationEnable)reg.B.COMUV;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isSelectComUvComparatorForAbistOperationEnabled(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_SelectComUvComparatorForAbistOperationEnable value;
    Ifx_TLF35584_ABIST_SELECT1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.COMUV == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_setSelectComUvComparatorForAbistOperationEnable(IfxTlf35584_Driver* driver, IfxTlf35584_SelectComUvComparatorForAbistOperationEnable value, boolean* status)
{
    Ifx_TLF35584_ABIST_SELECT1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.COMUV = value;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT1) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void IfxTlf35584_enableSelectComUvComparatorForAbistOperation(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_ABIST_SELECT1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.COMUV = 1;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT1) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void IfxTlf35584_disableSelectComUvComparatorForAbistOperation(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_ABIST_SELECT1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.COMUV = 0;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT1) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_SelectVrefUvComparatorForAbistOperationEnable IfxTlf35584_getSelectVrefUvComparatorForAbistOperationEnable(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_SelectVrefUvComparatorForAbistOperationEnable value;
    Ifx_TLF35584_ABIST_SELECT1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_SelectVrefUvComparatorForAbistOperationEnable)reg.B.VREFUV;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isSelectVrefUvComparatorForAbistOperationEnabled(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_SelectVrefUvComparatorForAbistOperationEnable value;
    Ifx_TLF35584_ABIST_SELECT1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.VREFUV == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_setSelectVrefUvComparatorForAbistOperationEnable(IfxTlf35584_Driver* driver, IfxTlf35584_SelectVrefUvComparatorForAbistOperationEnable value, boolean* status)
{
    Ifx_TLF35584_ABIST_SELECT1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.VREFUV = value;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT1) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void IfxTlf35584_enableSelectVrefUvComparatorForAbistOperation(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_ABIST_SELECT1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.VREFUV = 1;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT1) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void IfxTlf35584_disableSelectVrefUvComparatorForAbistOperation(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_ABIST_SELECT1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.VREFUV = 0;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT1) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_SelectTrk1UvComparatorForAbistOperationEnable IfxTlf35584_getSelectTrk1UvComparatorForAbistOperationEnable(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_SelectTrk1UvComparatorForAbistOperationEnable value;
    Ifx_TLF35584_ABIST_SELECT1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_SelectTrk1UvComparatorForAbistOperationEnable)reg.B.TRK1UV;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isSelectTrk1UvComparatorForAbistOperationEnabled(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_SelectTrk1UvComparatorForAbistOperationEnable value;
    Ifx_TLF35584_ABIST_SELECT1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.TRK1UV == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_setSelectTrk1UvComparatorForAbistOperationEnable(IfxTlf35584_Driver* driver, IfxTlf35584_SelectTrk1UvComparatorForAbistOperationEnable value, boolean* status)
{
    Ifx_TLF35584_ABIST_SELECT1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.TRK1UV = value;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT1) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void IfxTlf35584_enableSelectTrk1UvComparatorForAbistOperation(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_ABIST_SELECT1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.TRK1UV = 1;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT1) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void IfxTlf35584_disableSelectTrk1UvComparatorForAbistOperation(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_ABIST_SELECT1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.TRK1UV = 0;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT1) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_SelectTrk2UvComparatorForAbistOperationEnable IfxTlf35584_getSelectTrk2UvComparatorForAbistOperationEnable(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_SelectTrk2UvComparatorForAbistOperationEnable value;
    Ifx_TLF35584_ABIST_SELECT1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_SelectTrk2UvComparatorForAbistOperationEnable)reg.B.TRK2UV;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isSelectTrk2UvComparatorForAbistOperationEnabled(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_SelectTrk2UvComparatorForAbistOperationEnable value;
    Ifx_TLF35584_ABIST_SELECT1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.TRK2UV == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_setSelectTrk2UvComparatorForAbistOperationEnable(IfxTlf35584_Driver* driver, IfxTlf35584_SelectTrk2UvComparatorForAbistOperationEnable value, boolean* status)
{
    Ifx_TLF35584_ABIST_SELECT1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.TRK2UV = value;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT1) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void IfxTlf35584_enableSelectTrk2UvComparatorForAbistOperation(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_ABIST_SELECT1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.TRK2UV = 1;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT1) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void IfxTlf35584_disableSelectTrk2UvComparatorForAbistOperation(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_ABIST_SELECT1 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT1) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.TRK2UV = 0;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT1) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_SelectSupplyVs12OvervoltageEnable IfxTlf35584_getSelectSupplyVs12OvervoltageEnable(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_SelectSupplyVs12OvervoltageEnable value;
    Ifx_TLF35584_ABIST_SELECT2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_SelectSupplyVs12OvervoltageEnable)reg.B.VBATOV;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isSelectSupplyVs12OvervoltageEnabled(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_SelectSupplyVs12OvervoltageEnable value;
    Ifx_TLF35584_ABIST_SELECT2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.VBATOV == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_setSelectSupplyVs12OvervoltageEnable(IfxTlf35584_Driver* driver, IfxTlf35584_SelectSupplyVs12OvervoltageEnable value, boolean* status)
{
    Ifx_TLF35584_ABIST_SELECT2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.VBATOV = value;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT2) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void IfxTlf35584_enableSelectSupplyVs12Overvoltage(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_ABIST_SELECT2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.VBATOV = 1;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT2) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void IfxTlf35584_disableSelectSupplyVs12Overvoltage(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_ABIST_SELECT2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.VBATOV = 0;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT2) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_SelectInternalSupplyOvConditionEnable IfxTlf35584_getSelectInternalSupplyOvConditionEnable(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_SelectInternalSupplyOvConditionEnable value;
    Ifx_TLF35584_ABIST_SELECT2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_SelectInternalSupplyOvConditionEnable)reg.B.INTOV;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isSelectInternalSupplyOvConditionEnabled(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_SelectInternalSupplyOvConditionEnable value;
    Ifx_TLF35584_ABIST_SELECT2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.INTOV == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_setSelectInternalSupplyOvConditionEnable(IfxTlf35584_Driver* driver, IfxTlf35584_SelectInternalSupplyOvConditionEnable value, boolean* status)
{
    Ifx_TLF35584_ABIST_SELECT2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.INTOV = value;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT2) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void IfxTlf35584_enableSelectInternalSupplyOvCondition(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_ABIST_SELECT2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.INTOV = 1;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT2) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void IfxTlf35584_disableSelectInternalSupplyOvCondition(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_ABIST_SELECT2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.INTOV = 0;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT2) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_SelectBandgapComparatorUvConditionEnable IfxTlf35584_getSelectBandgapComparatorUvConditionEnable(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_SelectBandgapComparatorUvConditionEnable value;
    Ifx_TLF35584_ABIST_SELECT2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_SelectBandgapComparatorUvConditionEnable)reg.B.BG12UV;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isSelectBandgapComparatorUvConditionEnabled(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_SelectBandgapComparatorUvConditionEnable value;
    Ifx_TLF35584_ABIST_SELECT2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.BG12UV == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_setSelectBandgapComparatorUvConditionEnable(IfxTlf35584_Driver* driver, IfxTlf35584_SelectBandgapComparatorUvConditionEnable value, boolean* status)
{
    Ifx_TLF35584_ABIST_SELECT2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.BG12UV = value;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT2) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void IfxTlf35584_enableSelectBandgapComparatorUvCondition(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_ABIST_SELECT2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.BG12UV = 1;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT2) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void IfxTlf35584_disableSelectBandgapComparatorUvCondition(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_ABIST_SELECT2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.BG12UV = 0;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT2) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_SelectBandgapComparatorOvConditionEnable IfxTlf35584_getSelectBandgapComparatorOvConditionEnable(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_SelectBandgapComparatorOvConditionEnable value;
    Ifx_TLF35584_ABIST_SELECT2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_SelectBandgapComparatorOvConditionEnable)reg.B.BG12OV;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isSelectBandgapComparatorOvConditionEnabled(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_SelectBandgapComparatorOvConditionEnable value;
    Ifx_TLF35584_ABIST_SELECT2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.BG12OV == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_setSelectBandgapComparatorOvConditionEnable(IfxTlf35584_Driver* driver, IfxTlf35584_SelectBandgapComparatorOvConditionEnable value, boolean* status)
{
    Ifx_TLF35584_ABIST_SELECT2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.BG12OV = value;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT2) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void IfxTlf35584_enableSelectBandgapComparatorOvCondition(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_ABIST_SELECT2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.BG12OV = 1;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT2) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void IfxTlf35584_disableSelectBandgapComparatorOvCondition(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_ABIST_SELECT2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.BG12OV = 0;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT2) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_SelectBiasCurrentTooLowEnable IfxTlf35584_getSelectBiasCurrentTooLowEnable(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_SelectBiasCurrentTooLowEnable value;
    Ifx_TLF35584_ABIST_SELECT2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_SelectBiasCurrentTooLowEnable)reg.B.BIASLOW;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isSelectBiasCurrentTooLowEnabled(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_SelectBiasCurrentTooLowEnable value;
    Ifx_TLF35584_ABIST_SELECT2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.BIASLOW == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_setSelectBiasCurrentTooLowEnable(IfxTlf35584_Driver* driver, IfxTlf35584_SelectBiasCurrentTooLowEnable value, boolean* status)
{
    Ifx_TLF35584_ABIST_SELECT2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.BIASLOW = value;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT2) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void IfxTlf35584_enableSelectBiasCurrentTooLow(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_ABIST_SELECT2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.BIASLOW = 1;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT2) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void IfxTlf35584_disableSelectBiasCurrentTooLow(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_ABIST_SELECT2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.BIASLOW = 0;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT2) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_SelectBiasCurrentTooHighEnable IfxTlf35584_getSelectBiasCurrentTooHighEnable(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_SelectBiasCurrentTooHighEnable value;
    Ifx_TLF35584_ABIST_SELECT2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_SelectBiasCurrentTooHighEnable)reg.B.BIASHI;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isSelectBiasCurrentTooHighEnabled(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_SelectBiasCurrentTooHighEnable value;
    Ifx_TLF35584_ABIST_SELECT2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.BIASHI == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_setSelectBiasCurrentTooHighEnable(IfxTlf35584_Driver* driver, IfxTlf35584_SelectBiasCurrentTooHighEnable value, boolean* status)
{
    Ifx_TLF35584_ABIST_SELECT2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.BIASHI = value;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT2) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void IfxTlf35584_enableSelectBiasCurrentTooHigh(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_ABIST_SELECT2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.BIASHI = 1;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT2) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void IfxTlf35584_disableSelectBiasCurrentTooHigh(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_ABIST_SELECT2 reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT2) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.BIASHI = 0;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.ABIST_SELECT2) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_BuckSwitchingFrequencyChange IfxTlf35584_getBuckSwitchingFrequencyChange(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_BuckSwitchingFrequencyChange value;
    Ifx_TLF35584_BCK_FREQ_CHANGE reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.BCK_FREQ_CHANGE) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_BuckSwitchingFrequencyChange)reg.B.BCK_FREQ_SEL;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_setBuckSwitchingFrequencyChange(IfxTlf35584_Driver* driver, IfxTlf35584_BuckSwitchingFrequencyChange value, boolean* status)
{
    Ifx_TLF35584_BCK_FREQ_CHANGE reg;
    reg.U = 0;
    reg.B.BCK_FREQ_SEL = value;
    *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.BCK_FREQ_CHANGE) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
}

IfxTlf35584_SpreadSpectrumDownSpread IfxTlf35584_getSpreadSpectrumDownSpread(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_SpreadSpectrumDownSpread value;
    Ifx_TLF35584_BCK_FRE_SPREAD reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.BCK_FRE_SPREAD) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_SpreadSpectrumDownSpread)reg.B.FRE_SP_THR;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_setSpreadSpectrumDownSpread(IfxTlf35584_Driver* driver, IfxTlf35584_SpreadSpectrumDownSpread value, boolean* status)
{
    Ifx_TLF35584_BCK_FRE_SPREAD reg;
    reg.U = 0;
    reg.B.FRE_SP_THR = value;
    *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.BCK_FRE_SPREAD) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
}

IfxTlf35584_EnableBuckUpdate IfxTlf35584_getEnableBuckUpdate(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_EnableBuckUpdate value;
    Ifx_TLF35584_BCK_MAIN_CTRL reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.BCK_MAIN_CTRL) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_EnableBuckUpdate)reg.B.DATA_VALID;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isBuckUpdateEnabled(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_EnableBuckUpdate value;
    Ifx_TLF35584_BCK_MAIN_CTRL reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.BCK_MAIN_CTRL) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.DATA_VALID == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

void IfxTlf35584_setEnableBuckUpdate(IfxTlf35584_Driver* driver, IfxTlf35584_EnableBuckUpdate value, boolean* status)
{
    Ifx_TLF35584_BCK_MAIN_CTRL reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.BCK_MAIN_CTRL) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.DATA_VALID = value;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.BCK_MAIN_CTRL) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void IfxTlf35584_enableBuckUpdate(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_BCK_MAIN_CTRL reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.BCK_MAIN_CTRL) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.DATA_VALID = 1;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.BCK_MAIN_CTRL) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

void IfxTlf35584_disableBuckUpdate(IfxTlf35584_Driver* driver, boolean* status)
{
    Ifx_TLF35584_BCK_MAIN_CTRL reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.BCK_MAIN_CTRL) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        reg.B.DATA_VALID = 0;
        *status &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.BCK_MAIN_CTRL) >> IFXTLF35584_ADDRESS_SHIFT), reg.U);
    }
    else
    {
        *status = FALSE;
    }
}

IfxTlf35584_DatavalidParameterUpdateReadyStatus IfxTlf35584_getDatavalidParameterUpdateReadyStatus(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_DatavalidParameterUpdateReadyStatus value;
    Ifx_TLF35584_BCK_MAIN_CTRL reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.BCK_MAIN_CTRL) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_DatavalidParameterUpdateReadyStatus)reg.B.BUSY;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

IfxTlf35584_TestModeEnableStatus IfxTlf35584_getTestModeEnableStatus(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_TestModeEnableStatus value;
    Ifx_TLF35584_GTM reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.GTM) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_TestModeEnableStatus)reg.B.TM;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_isTestModeEnabled(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_TestModeEnableStatus value;
    Ifx_TLF35584_GTM reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.GTM) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = reg.B.TM == 1 ? TRUE : FALSE;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

IfxTlf35584_TestModeInvertedEnableStatus IfxTlf35584_getTestModeInvertedEnableStatus(IfxTlf35584_Driver* driver, boolean* status)
{
    IfxTlf35584_TestModeInvertedEnableStatus value;
    Ifx_TLF35584_GTM reg;
    if (IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.GTM) >> IFXTLF35584_ADDRESS_SHIFT), &reg.U))
    {
        value = (IfxTlf35584_TestModeInvertedEnableStatus)reg.B.NTM;
    }
    else
    {
        *status = FALSE;
        value = 0;
    }
    return value;
}

boolean IfxTlf35584_registerDump(IfxTlf35584_Address address, IfxTlf35584_UData value, char* text, size_t maxLength)
{
    int result;
    uint32 a = address << IFXTLF35584_ADDRESS_SHIFT;
    switch(a)
    {
        case (const uint32)&MODULE_TLF35584.DEVCFG0:
            result = snprintf(text, maxLength, "DEVCFG0 @ 0x%X = 0x%X = [TRDEL:0x%X, WKTIMCYC:0x%X, WKTIMEN:0x%X]",
                address,
                value,
                ((value >> 0) & 0xf), ((value >> 6) & 0x1), ((value >> 7) & 0x1)
                );
            break;
        case (const uint32)&MODULE_TLF35584.DEVCFG1:
            result = snprintf(text, maxLength, "DEVCFG1 @ 0x%X = 0x%X = [RESDEL:0x%X]",
                address,
                value,
                ((value >> 0) & 0x7)
                );
            break;
        case (const uint32)&MODULE_TLF35584.DEVCFG2:
            result = snprintf(text, maxLength, "DEVCFG2 @ 0x%X = 0x%X = [ESYNEN:0x%X, ESYNPHA:0x%X, CTHR:0x%X, CMONEN:0x%X, FRE:0x%X, STU:0x%X, EVCEN:0x%X]",
                address,
                value,
                ((value >> 0) & 0x1), ((value >> 1) & 0x1), ((value >> 2) & 0x3), ((value >> 4) & 0x1), ((value >> 5) & 0x1), ((value >> 6) & 0x1), ((value >> 7) & 0x1)
                );
            break;
        case (const uint32)&MODULE_TLF35584.PROTCFG:
            result = snprintf(text, maxLength, "PROTCFG @ 0x%X = 0x%X = [KEY:0x%X]",
                address,
                value,
                ((value >> 0) & 0xff)
                );
            break;
        case (const uint32)&MODULE_TLF35584.SYSPCFG0:
            result = snprintf(text, maxLength, "SYSPCFG0 @ 0x%X = 0x%X = [STBYEN:0x%X]",
                address,
                value,
                ((value >> 0) & 0x1)
                );
            break;
        case (const uint32)&MODULE_TLF35584.SYSPCFG1:
            result = snprintf(text, maxLength, "SYSPCFG1 @ 0x%X = 0x%X = [ERRREC:0x%X, ERRRECEN:0x%X, ERREN:0x%X, ERRSLPEN:0x%X, SS2DEL:0x%X]",
                address,
                value,
                ((value >> 0) & 0x3), ((value >> 2) & 0x1), ((value >> 3) & 0x1), ((value >> 4) & 0x1), ((value >> 5) & 0x7)
                );
            break;
        case (const uint32)&MODULE_TLF35584.WDCFG0:
            result = snprintf(text, maxLength, "WDCFG0 @ 0x%X = 0x%X = [WDCYC:0x%X, WWDTSEL:0x%X, FWDEN:0x%X, WWDEN:0x%X, WWDETHR:0x%X]",
                address,
                value,
                ((value >> 0) & 0x1), ((value >> 1) & 0x1), ((value >> 2) & 0x1), ((value >> 3) & 0x1), ((value >> 4) & 0xf)
                );
            break;
        case (const uint32)&MODULE_TLF35584.WDCFG1:
            result = snprintf(text, maxLength, "WDCFG1 @ 0x%X = 0x%X = [FWDETHR:0x%X, WDSLPEN:0x%X]",
                address,
                value,
                ((value >> 0) & 0xf), ((value >> 4) & 0x1)
                );
            break;
        case (const uint32)&MODULE_TLF35584.FWDCFG:
            result = snprintf(text, maxLength, "FWDCFG @ 0x%X = 0x%X = [WDHBTP:0x%X]",
                address,
                value,
                ((value >> 0) & 0x1f)
                );
            break;
        case (const uint32)&MODULE_TLF35584.WWDCFG0:
            result = snprintf(text, maxLength, "WWDCFG0 @ 0x%X = 0x%X = [CW:0x%X]",
                address,
                value,
                ((value >> 0) & 0x1f)
                );
            break;
        case (const uint32)&MODULE_TLF35584.WWDCFG1:
            result = snprintf(text, maxLength, "WWDCFG1 @ 0x%X = 0x%X = [OW:0x%X]",
                address,
                value,
                ((value >> 0) & 0x1f)
                );
            break;
        case (const uint32)&MODULE_TLF35584.RSYSPCFG0:
            result = snprintf(text, maxLength, "RSYSPCFG0 @ 0x%X = 0x%X = [STBYEN:0x%X]",
                address,
                value,
                ((value >> 0) & 0x1)
                );
            break;
        case (const uint32)&MODULE_TLF35584.RSYSPCFG1:
            result = snprintf(text, maxLength, "RSYSPCFG1 @ 0x%X = 0x%X = [ERRREC:0x%X, ERRRECEN:0x%X, ERREN:0x%X, ERRSLPEN:0x%X, SS2DEL:0x%X]",
                address,
                value,
                ((value >> 0) & 0x3), ((value >> 2) & 0x1), ((value >> 3) & 0x1), ((value >> 4) & 0x1), ((value >> 5) & 0x7)
                );
            break;
        case (const uint32)&MODULE_TLF35584.RWDCFG0:
            result = snprintf(text, maxLength, "RWDCFG0 @ 0x%X = 0x%X = [WDCYC:0x%X, WWDTSEL:0x%X, FWDEN:0x%X, WWDEN:0x%X, WWDETHR:0x%X]",
                address,
                value,
                ((value >> 0) & 0x1), ((value >> 1) & 0x1), ((value >> 2) & 0x1), ((value >> 3) & 0x1), ((value >> 4) & 0xf)
                );
            break;
        case (const uint32)&MODULE_TLF35584.RWDCFG1:
            result = snprintf(text, maxLength, "RWDCFG1 @ 0x%X = 0x%X = [FWDETHR:0x%X, WDSLPEN:0x%X]",
                address,
                value,
                ((value >> 0) & 0xf), ((value >> 4) & 0x1)
                );
            break;
        case (const uint32)&MODULE_TLF35584.RFWDCFG:
            result = snprintf(text, maxLength, "RFWDCFG @ 0x%X = 0x%X = [WDHBTP:0x%X]",
                address,
                value,
                ((value >> 0) & 0x1f)
                );
            break;
        case (const uint32)&MODULE_TLF35584.RWWDCFG0:
            result = snprintf(text, maxLength, "RWWDCFG0 @ 0x%X = 0x%X = [CW:0x%X]",
                address,
                value,
                ((value >> 0) & 0x1f)
                );
            break;
        case (const uint32)&MODULE_TLF35584.RWWDCFG1:
            result = snprintf(text, maxLength, "RWWDCFG1 @ 0x%X = 0x%X = [OW:0x%X]",
                address,
                value,
                ((value >> 0) & 0x1f)
                );
            break;
        case (const uint32)&MODULE_TLF35584.WKTIMCFG0:
            result = snprintf(text, maxLength, "WKTIMCFG0 @ 0x%X = 0x%X = [TIMVALL:0x%X]",
                address,
                value,
                ((value >> 0) & 0xff)
                );
            break;
        case (const uint32)&MODULE_TLF35584.WKTIMCFG1:
            result = snprintf(text, maxLength, "WKTIMCFG1 @ 0x%X = 0x%X = [TIMVALM:0x%X]",
                address,
                value,
                ((value >> 0) & 0xff)
                );
            break;
        case (const uint32)&MODULE_TLF35584.WKTIMCFG2:
            result = snprintf(text, maxLength, "WKTIMCFG2 @ 0x%X = 0x%X = [TIMVALH:0x%X]",
                address,
                value,
                ((value >> 0) & 0xff)
                );
            break;
        case (const uint32)&MODULE_TLF35584.DEVCTRL:
            result = snprintf(text, maxLength, "DEVCTRL @ 0x%X = 0x%X = [STATEREQ:0x%X, VREFEN:0x%X, COMEN:0x%X, TRK1EN:0x%X, TRK2EN:0x%X]",
                address,
                value,
                ((value >> 0) & 0x7), ((value >> 3) & 0x1), ((value >> 5) & 0x1), ((value >> 6) & 0x1), ((value >> 7) & 0x1)
                );
            break;
        case (const uint32)&MODULE_TLF35584.DEVCTRLN:
            result = snprintf(text, maxLength, "DEVCTRLN @ 0x%X = 0x%X = [STATEREQ:0x%X, VREFEN:0x%X, COMEN:0x%X, TRK1EN:0x%X, TRK2EN:0x%X]",
                address,
                value,
                ((value >> 0) & 0x7), ((value >> 3) & 0x1), ((value >> 5) & 0x1), ((value >> 6) & 0x1), ((value >> 7) & 0x1)
                );
            break;
        case (const uint32)&MODULE_TLF35584.WWDSCMD:
            result = snprintf(text, maxLength, "WWDSCMD @ 0x%X = 0x%X = [TRIG:0x%X, TRIG_STATUS:0x%X]",
                address,
                value,
                ((value >> 0) & 0x1), ((value >> 7) & 0x1)
                );
            break;
        case (const uint32)&MODULE_TLF35584.FWDRSP:
            result = snprintf(text, maxLength, "FWDRSP @ 0x%X = 0x%X = [FWDRSP:0x%X]",
                address,
                value,
                ((value >> 0) & 0xff)
                );
            break;
        case (const uint32)&MODULE_TLF35584.FWDRSPSYNC:
            result = snprintf(text, maxLength, "FWDRSPSYNC @ 0x%X = 0x%X = [FWDRSPS:0x%X]",
                address,
                value,
                ((value >> 0) & 0xff)
                );
            break;
        case (const uint32)&MODULE_TLF35584.SYSFAIL:
            result = snprintf(text, maxLength, "SYSFAIL @ 0x%X = 0x%X = [VOLTSELERR:0x%X, OTF:0x%X, VMONF:0x%X, ABISTERR:0x%X, INITF:0x%X]",
                address,
                value,
                ((value >> 0) & 0x1), ((value >> 1) & 0x1), ((value >> 2) & 0x1), ((value >> 6) & 0x1), ((value >> 7) & 0x1)
                );
            break;
        case (const uint32)&MODULE_TLF35584.INITERR:
            result = snprintf(text, maxLength, "INITERR @ 0x%X = 0x%X = [VMONF:0x%X, WWDF:0x%X, FWDF:0x%X, ERRF:0x%X, SOFTRES:0x%X, HARDRES:0x%X]",
                address,
                value,
                ((value >> 2) & 0x1), ((value >> 3) & 0x1), ((value >> 4) & 0x1), ((value >> 5) & 0x1), ((value >> 6) & 0x1), ((value >> 7) & 0x1)
                );
            break;
        case (const uint32)&MODULE_TLF35584.IF:
            result = snprintf(text, maxLength, "IF @ 0x%X = 0x%X = [SYS:0x%X, WK:0x%X, SPI:0x%X, MON:0x%X, OTW:0x%X, OTF:0x%X, ABIST:0x%X, INTMISS:0x%X]",
                address,
                value,
                ((value >> 0) & 0x1), ((value >> 1) & 0x1), ((value >> 2) & 0x1), ((value >> 3) & 0x1), ((value >> 4) & 0x1), ((value >> 5) & 0x1), ((value >> 6) & 0x1), ((value >> 7) & 0x1)
                );
            break;
        case (const uint32)&MODULE_TLF35584.SYSSF:
            result = snprintf(text, maxLength, "SYSSF @ 0x%X = 0x%X = [CFGE:0x%X, WWDE:0x%X, FWDE:0x%X, ERRMISS:0x%X, TRFAIL:0x%X, NO_OP:0x%X]",
                address,
                value,
                ((value >> 0) & 0x1), ((value >> 1) & 0x1), ((value >> 2) & 0x1), ((value >> 3) & 0x1), ((value >> 4) & 0x1), ((value >> 5) & 0x1)
                );
            break;
        case (const uint32)&MODULE_TLF35584.WKSF:
            result = snprintf(text, maxLength, "WKSF @ 0x%X = 0x%X = [WAK:0x%X, ENA:0x%X, CMON:0x%X, WKTIM:0x%X, WKSPI:0x%X]",
                address,
                value,
                ((value >> 0) & 0x1), ((value >> 1) & 0x1), ((value >> 2) & 0x1), ((value >> 3) & 0x1), ((value >> 4) & 0x1)
                );
            break;
        case (const uint32)&MODULE_TLF35584.SPISF:
            result = snprintf(text, maxLength, "SPISF @ 0x%X = 0x%X = [PARE:0x%X, LENE:0x%X, ADDRE:0x%X, DURE:0x%X, LOCK:0x%X]",
                address,
                value,
                ((value >> 0) & 0x1), ((value >> 1) & 0x1), ((value >> 2) & 0x1), ((value >> 3) & 0x1), ((value >> 4) & 0x1)
                );
            break;
        case (const uint32)&MODULE_TLF35584.MONSF0:
            result = snprintf(text, maxLength, "MONSF0 @ 0x%X = 0x%X = [PREGSG:0x%X, UCSG:0x%X, STBYSG:0x%X, VCORESG:0x%X, COMSG:0x%X, VREFSG:0x%X, TRK1SG:0x%X, TRK2SG:0x%X]",
                address,
                value,
                ((value >> 0) & 0x1), ((value >> 1) & 0x1), ((value >> 2) & 0x1), ((value >> 3) & 0x1), ((value >> 4) & 0x1), ((value >> 5) & 0x1), ((value >> 6) & 0x1), ((value >> 7) & 0x1)
                );
            break;
        case (const uint32)&MODULE_TLF35584.MONSF1:
            result = snprintf(text, maxLength, "MONSF1 @ 0x%X = 0x%X = [PREGOV:0x%X, UCOV:0x%X, STBYOV:0x%X, VCOREOV:0x%X, COMOV:0x%X, VREFOV:0x%X, TRK1OV:0x%X, TRK2OV:0x%X]",
                address,
                value,
                ((value >> 0) & 0x1), ((value >> 1) & 0x1), ((value >> 2) & 0x1), ((value >> 3) & 0x1), ((value >> 4) & 0x1), ((value >> 5) & 0x1), ((value >> 6) & 0x1), ((value >> 7) & 0x1)
                );
            break;
        case (const uint32)&MODULE_TLF35584.MONSF2:
            result = snprintf(text, maxLength, "MONSF2 @ 0x%X = 0x%X = [PREGUV:0x%X, UCUV:0x%X, STBYUV:0x%X, VCOREUV:0x%X, COMUV:0x%X, VREFUV:0x%X, TRK1UV:0x%X, TRK2UV:0x%X]",
                address,
                value,
                ((value >> 0) & 0x1), ((value >> 1) & 0x1), ((value >> 2) & 0x1), ((value >> 3) & 0x1), ((value >> 4) & 0x1), ((value >> 5) & 0x1), ((value >> 6) & 0x1), ((value >> 7) & 0x1)
                );
            break;
        case (const uint32)&MODULE_TLF35584.MONSF3:
            result = snprintf(text, maxLength, "MONSF3 @ 0x%X = 0x%X = [VBATOV:0x%X, BG12UV:0x%X, BG12OV:0x%X, BIASLOW:0x%X, BIASHI:0x%X]",
                address,
                value,
                ((value >> 0) & 0x1), ((value >> 4) & 0x1), ((value >> 5) & 0x1), ((value >> 6) & 0x1), ((value >> 7) & 0x1)
                );
            break;
        case (const uint32)&MODULE_TLF35584.OTFAIL:
            result = snprintf(text, maxLength, "OTFAIL @ 0x%X = 0x%X = [PREG:0x%X, UC:0x%X, COM:0x%X, MON:0x%X]",
                address,
                value,
                ((value >> 0) & 0x1), ((value >> 1) & 0x1), ((value >> 4) & 0x1), ((value >> 7) & 0x1)
                );
            break;
        case (const uint32)&MODULE_TLF35584.OTWRNSF:
            result = snprintf(text, maxLength, "OTWRNSF @ 0x%X = 0x%X = [PREG:0x%X, UC:0x%X, STDBY:0x%X, COM:0x%X, VREF:0x%X]",
                address,
                value,
                ((value >> 0) & 0x1), ((value >> 1) & 0x1), ((value >> 2) & 0x1), ((value >> 4) & 0x1), ((value >> 5) & 0x1)
                );
            break;
        case (const uint32)&MODULE_TLF35584.VMONSTAT:
            result = snprintf(text, maxLength, "VMONSTAT @ 0x%X = 0x%X = [STBYST:0x%X, VCOREST:0x%X, COMST:0x%X, VREFST:0x%X, TRK1ST:0x%X, TRK2ST:0x%X]",
                address,
                value,
                ((value >> 2) & 0x1), ((value >> 3) & 0x1), ((value >> 4) & 0x1), ((value >> 5) & 0x1), ((value >> 6) & 0x1), ((value >> 7) & 0x1)
                );
            break;
        case (const uint32)&MODULE_TLF35584.DEVSTAT:
            result = snprintf(text, maxLength, "DEVSTAT @ 0x%X = 0x%X = [STATE:0x%X, VREFEN:0x%X, STBYEN:0x%X, COMEN:0x%X, TRK1EN:0x%X, TRK2EN:0x%X]",
                address,
                value,
                ((value >> 0) & 0x7), ((value >> 3) & 0x1), ((value >> 4) & 0x1), ((value >> 5) & 0x1), ((value >> 6) & 0x1), ((value >> 7) & 0x1)
                );
            break;
        case (const uint32)&MODULE_TLF35584.PROTSTAT:
            result = snprintf(text, maxLength, "PROTSTAT @ 0x%X = 0x%X = [LOCK:0x%X, KEY1OK:0x%X, KEY2OK:0x%X, KEY3OK:0x%X, KEY4OK:0x%X]",
                address,
                value,
                ((value >> 0) & 0x1), ((value >> 4) & 0x1), ((value >> 5) & 0x1), ((value >> 6) & 0x1), ((value >> 7) & 0x1)
                );
            break;
        case (const uint32)&MODULE_TLF35584.WWDSTAT:
            result = snprintf(text, maxLength, "WWDSTAT @ 0x%X = 0x%X = [WWDECNT:0x%X]",
                address,
                value,
                ((value >> 0) & 0xf)
                );
            break;
        case (const uint32)&MODULE_TLF35584.FWDSTAT0:
            result = snprintf(text, maxLength, "FWDSTAT0 @ 0x%X = 0x%X = [FWDQUEST:0x%X, FWDRSPC:0x%X, FWDRSPOK:0x%X]",
                address,
                value,
                ((value >> 0) & 0xf), ((value >> 4) & 0x3), ((value >> 6) & 0x1)
                );
            break;
        case (const uint32)&MODULE_TLF35584.FWDSTAT1:
            result = snprintf(text, maxLength, "FWDSTAT1 @ 0x%X = 0x%X = [FWDECNT:0x%X]",
                address,
                value,
                ((value >> 0) & 0xf)
                );
            break;
        case (const uint32)&MODULE_TLF35584.ABIST_CTRL0:
            result = snprintf(text, maxLength, "ABIST_CTRL0 @ 0x%X = 0x%X = [START:0x%X, PATH:0x%X, SINGLE:0x%X, INT:0x%X, STATUS:0x%X]",
                address,
                value,
                ((value >> 0) & 0x1), ((value >> 1) & 0x1), ((value >> 2) & 0x1), ((value >> 3) & 0x1), ((value >> 4) & 0xf)
                );
            break;
        case (const uint32)&MODULE_TLF35584.ABIST_CTRL1:
            result = snprintf(text, maxLength, "ABIST_CTRL1 @ 0x%X = 0x%X = [OV_TRIG:0x%X, ABIST_CLK_EN:0x%X]",
                address,
                value,
                ((value >> 0) & 0x1), ((value >> 1) & 0x1)
                );
            break;
        case (const uint32)&MODULE_TLF35584.ABIST_SELECT0:
            result = snprintf(text, maxLength, "ABIST_SELECT0 @ 0x%X = 0x%X = [PREGOV:0x%X, UCOV:0x%X, STBYOV:0x%X, VCOREOV:0x%X, COMOV:0x%X, VREFOV:0x%X, TRK1OV:0x%X, TRK2OV:0x%X]",
                address,
                value,
                ((value >> 0) & 0x1), ((value >> 1) & 0x1), ((value >> 2) & 0x1), ((value >> 3) & 0x1), ((value >> 4) & 0x1), ((value >> 5) & 0x1), ((value >> 6) & 0x1), ((value >> 7) & 0x1)
                );
            break;
        case (const uint32)&MODULE_TLF35584.ABIST_SELECT1:
            result = snprintf(text, maxLength, "ABIST_SELECT1 @ 0x%X = 0x%X = [PREGUV:0x%X, UCUV:0x%X, STBYUV:0x%X, VCOREUV:0x%X, COMUV:0x%X, VREFUV:0x%X, TRK1UV:0x%X, TRK2UV:0x%X]",
                address,
                value,
                ((value >> 0) & 0x1), ((value >> 1) & 0x1), ((value >> 2) & 0x1), ((value >> 3) & 0x1), ((value >> 4) & 0x1), ((value >> 5) & 0x1), ((value >> 6) & 0x1), ((value >> 7) & 0x1)
                );
            break;
        case (const uint32)&MODULE_TLF35584.ABIST_SELECT2:
            result = snprintf(text, maxLength, "ABIST_SELECT2 @ 0x%X = 0x%X = [VBATOV:0x%X, INTOV:0x%X, BG12UV:0x%X, BG12OV:0x%X, BIASLOW:0x%X, BIASHI:0x%X]",
                address,
                value,
                ((value >> 0) & 0x1), ((value >> 3) & 0x1), ((value >> 4) & 0x1), ((value >> 5) & 0x1), ((value >> 6) & 0x1), ((value >> 7) & 0x1)
                );
            break;
        case (const uint32)&MODULE_TLF35584.BCK_FREQ_CHANGE:
            result = snprintf(text, maxLength, "BCK_FREQ_CHANGE @ 0x%X = 0x%X = [BCK_FREQ_SEL:0x%X]",
                address,
                value,
                ((value >> 0) & 0x7)
                );
            break;
        case (const uint32)&MODULE_TLF35584.BCK_FRE_SPREAD:
            result = snprintf(text, maxLength, "BCK_FRE_SPREAD @ 0x%X = 0x%X = [FRE_SP_THR:0x%X]",
                address,
                value,
                ((value >> 0) & 0xff)
                );
            break;
        case (const uint32)&MODULE_TLF35584.BCK_MAIN_CTRL:
            result = snprintf(text, maxLength, "BCK_MAIN_CTRL @ 0x%X = 0x%X = [DATA_VALID:0x%X, BUSY:0x%X]",
                address,
                value,
                ((value >> 6) & 0x1), ((value >> 7) & 0x1)
                );
            break;
        case (const uint32)&MODULE_TLF35584.GTM:
            result = snprintf(text, maxLength, "GTM @ 0x%X = 0x%X = [TM:0x%X, NTM:0x%X]",
                address,
                value,
                ((value >> 0) & 0x1), ((value >> 1) & 0x1)
                );
            break;
        default:
            result = snprintf(text, maxLength, "ERROR: invalid address 0x%X", address);
            break;
    }
    return (result < maxLength) && (result >= 0);
}
