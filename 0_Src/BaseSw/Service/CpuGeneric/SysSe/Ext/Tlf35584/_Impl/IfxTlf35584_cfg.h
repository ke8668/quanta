/**
 * \file IfxTlf35584_cfg.h
 * \brief TLF35584 on-chip implementation data 
 * \ingroup IfxLld_Tlf35584 
 *
 * \copyright Copyright (c) 2017 Infineon Technologies AG. All rights reserved.
 *
 * $Date: 2017-05-15 04:24:15
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * \defgroup IfxLld_Tlf35584 TLF35584
 * \ingroup IfxLld_Tlf35584_Driver
 * \defgroup IfxLld_Tlf35584_Impl Implementation
 * \ingroup IfxLld_Tlf35584
 * \defgroup IfxLld_Tlf35584_Std Standard Driver
 * \ingroup IfxLld_Tlf35584
 */

#ifndef IFXTLF35584_CFG_H
#define IFXTLF35584_CFG_H 1

/******************************************************************************/
/*----------------------------------Includes----------------------------------*/
/******************************************************************************/

#include "SysSe/Ext/Tlf35584/_Reg/IfxTlf35584_regdef.h"
#include "SysSe/Ext/Tlf35584/_Reg/IfxTlf35584_enum.h"

#endif /* IFXTLF35584_CFG_H */
