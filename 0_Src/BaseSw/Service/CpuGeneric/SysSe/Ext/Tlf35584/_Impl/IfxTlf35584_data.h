/*
 * IfxTlf35584_data.h
 *
 *  Created on: 29 Sep 2016
 *      Author: denais
 */

#ifndef IFXTLF35584_DATA_H_
#define IFXTLF35584_DATA_H_

#include "Common/Ifx_DeviceData.h"
#include "SysSe/Ext/Tlf35584/Std/IfxTlf35584.h"

extern const Ifx_DeviceData IfxTlf35584_data;

const Ifx_DeviceData_Register *IfxTlf35584_Data_findRegister(pchar name);
const Ifx_DeviceData_Bitfield *IfxTlf35584_Data_findBitfield(const Ifx_DeviceData_Register *r, pchar name);
boolean IfxTlf35584_Data_getRegister(IfxTlf35584_Driver* driver, const Ifx_DeviceData_Register *r, IfxTlf35584_UData *value);
boolean IfxTlf35584_Data_setRegister(IfxTlf35584_Driver* driver, const Ifx_DeviceData_Register *r, IfxTlf35584_UData value);
boolean IfxTlf35584_Data_getBitfield(IfxTlf35584_Driver* driver, const Ifx_DeviceData_Register *r, const Ifx_DeviceData_Bitfield *b, IfxTlf35584_UData *value);
boolean IfxTlf35584_Data_setBitfield(IfxTlf35584_Driver* driver, const Ifx_DeviceData_Register *r, const Ifx_DeviceData_Bitfield *b, IfxTlf35584_UData value);




#endif /* IFXTLF35584_DATA_H_ */
