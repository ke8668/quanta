/**
 * \file IfxTlf35584_enum.h 
 * \brief Enumeration for TLF35584 register bitfields 
 *  
 * Date: 2017-05-15 04:24:32 GMT
 * Version: TBD
 * Specification: TBD
 * MAY BE CHANGED BY USER [yes/no]: Yes
 *
 * \copyright Copyright (c) 2017 Infineon Technologies AG. All rights reserved. 
 *  
 *  
 *                                 IMPORTANT NOTICE 
 *  
 * Infineon Technologies AG (Infineon) is supplying this file for use 
 * exclusively with Infineon's microcontroller products. This file can be freely 
 * distributed within development tools that are supporting such microcontroller 
 * products. 
 *  
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED 
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. 
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL, 
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER. 
 *  
 * \defgroup IfxLld_Tlf35584_Registers_Enum Enumeration 
 * \ingroup IfxLld_Tlf35584_Registers
 */

#ifndef IFXTLF35584_ENUM_H
#define IFXTLF35584_ENUM_H 1
/******************************************************************************/
/** \addtogroup IfxLld_Tlf35584_Registers_Enum
 * \{  */

/** \brief Transition delay into low power states
 * Bitfield(s): IfxTlf35584_DEVCFG0.TRDEL
 */
typedef enum
{
    IfxTlf35584_TransitionDelayIntoLowPowerStates_100us = 0,/**< \brief 100 us */
    IfxTlf35584_TransitionDelayIntoLowPowerStates_200us = 1,/**< \brief 200 us */
    IfxTlf35584_TransitionDelayIntoLowPowerStates_300us = 2,/**< \brief 300 us */
    IfxTlf35584_TransitionDelayIntoLowPowerStates_1600us = 15,/**< \brief 1600 us */
}IfxTlf35584_TransitionDelayIntoLowPowerStates;

/** \brief Wake timer cycle period
 * Bitfield(s): IfxTlf35584_DEVCFG0.WKTIMCYC
 */
typedef enum
{
    IfxTlf35584_WakeTimerCyclePeriod_10us = 0,        /**< \brief 10 us */
    IfxTlf35584_WakeTimerCyclePeriod_10ms = 1,        /**< \brief 10 ms */
}IfxTlf35584_WakeTimerCyclePeriod;

/** \brief Wake timer enable
 * Bitfield(s): IfxTlf35584_DEVCFG0.WKTIMEN
 */
typedef enum
{
    IfxTlf35584_WakeTimerEnable_disabled = 0,         /**< \brief Wake timer disabled */
    IfxTlf35584_WakeTimerEnable_enabled = 1,          /**< \brief Wake timer enabled in SLEEP or STANDBY state */
}IfxTlf35584_WakeTimerEnable;

/** \brief Reset release delay time
 * Bitfield(s): IfxTlf35584_DEVCFG1.RESDEL
 */
typedef enum
{
    IfxTlf35584_ResetReleaseDelayTime_200us = 0,      /**< \brief 200 us */
    IfxTlf35584_ResetReleaseDelayTime_400us = 1,      /**< \brief 400 us */
    IfxTlf35584_ResetReleaseDelayTime_800us = 2,      /**< \brief 800 us */
    IfxTlf35584_ResetReleaseDelayTime_1ms = 3,        /**< \brief 1ms */
    IfxTlf35584_ResetReleaseDelayTime_2ms = 4,        /**< \brief 2 ms */
    IfxTlf35584_ResetReleaseDelayTime_4ms = 5,        /**< \brief 4 ms */
    IfxTlf35584_ResetReleaseDelayTime_10ms = 6,       /**< \brief 10 ms */
    IfxTlf35584_ResetReleaseDelayTime_15ms = 7,       /**< \brief 15 ms */
}IfxTlf35584_ResetReleaseDelayTime;

/** \brief Synchronization output for external switchmode regulator\nenable
 * Bitfield(s): IfxTlf35584_DEVCFG2.ESYNEN
 */
typedef enum
{
    IfxTlf35584_SynchronizationOutputForExternalSwitchmodeRegulatorEnable_disabled = 0,/**< \brief Disable */
    IfxTlf35584_SynchronizationOutputForExternalSwitchmodeRegulatorEnable_enabled = 1,/**< \brief Enable */
}IfxTlf35584_SynchronizationOutputForExternalSwitchmodeRegulatorEnable;

/** \brief External synchronization output phase
 * Bitfield(s): IfxTlf35584_DEVCFG2.ESYNPHA
 */
typedef enum
{
    IfxTlf35584_ExternalSynchronizationOutputPhase_noPhaseShift = 0,/**< \brief No phase shift */
    IfxTlf35584_ExternalSynchronizationOutputPhase_180degPhaseShift = 1,/**< \brief 180 phase shift */
}IfxTlf35584_ExternalSynchronizationOutputPhase;

/** \brief QUC current monitoring threshold value
 * Bitfield(s): IfxTlf35584_DEVCFG2.CTHR
 */
typedef enum
{
    IfxTlf35584_QucCurrentMonitoringThresholdValue_10mA = 0,/**< \brief 10 mA */
    IfxTlf35584_QucCurrentMonitoringThresholdValue_30mA = 1,/**< \brief 30 mA */
    IfxTlf35584_QucCurrentMonitoringThresholdValue_60mA = 2,/**< \brief 60 mA */
    IfxTlf35584_QucCurrentMonitoringThresholdValue_100mA = 3,/**< \brief 100 mA */
}IfxTlf35584_QucCurrentMonitoringThresholdValue;

/** \brief QUC current monitor enable for transition to a low\npower state
 * Bitfield(s): IfxTlf35584_DEVCFG2.CMONEN
 */
typedef enum
{
    IfxTlf35584_QucCurrentMonitorEnableForTransitionToALowPowerState_disabled = 0,/**< \brief Disabled */
    IfxTlf35584_QucCurrentMonitorEnableForTransitionToALowPowerState_enabled = 1,/**< \brief Enabled */
}IfxTlf35584_QucCurrentMonitorEnableForTransitionToALowPowerState;

/** \brief Step-down converter frequency selection status
 * Bitfield(s): IfxTlf35584_DEVCFG2.FRE
 */
typedef enum
{
    IfxTlf35584_StepDownConverterFrequencySelectionStatus_lowFrequency = 0,/**< \brief Step-down converter runs on low frequency range */
    IfxTlf35584_StepDownConverterFrequencySelectionStatus_highFrequency = 1,/**< \brief Step-down converter runs on high frequency range */
}IfxTlf35584_StepDownConverterFrequencySelectionStatus;

/** \brief Step-up converter enable status
 * Bitfield(s): IfxTlf35584_DEVCFG2.STU
 */
typedef enum
{
    IfxTlf35584_StepUpConverterEnableStatus_disabled = 0,/**< \brief Disabled */
    IfxTlf35584_StepUpConverterEnableStatus_enabled = 1,/**< \brief Enabled */
}IfxTlf35584_StepUpConverterEnableStatus;

/** \brief External core supply enable status
 * Bitfield(s): IfxTlf35584_DEVCFG2.EVCEN
 */
typedef enum
{
    IfxTlf35584_ExternalCoreSupplyEnableStatus_disabled = 0,/**< \brief External core supply disabled */
    IfxTlf35584_ExternalCoreSupplyEnableStatus_enabled = 1,/**< \brief External core supply enabled */
}IfxTlf35584_ExternalCoreSupplyEnableStatus;

/** \brief Protection key
 * Bitfield(s): IfxTlf35584_PROTCFG.KEY
 */
typedef enum
{
    IfxTlf35584_ProtectionKey_unlockKey4 = 18,        /**< \brief Key 4 to unlock protected registers. */
    IfxTlf35584_ProtectionKey_lockKey2 = 52,          /**< \brief Key 2 to lock protected registers. */
    IfxTlf35584_ProtectionKey_unlockKey3 = 86,        /**< \brief Key 3 to unlock protected registers. */
    IfxTlf35584_ProtectionKey_unlockKey1 = 171,       /**< \brief Key 1 to unlock protected registers. */
    IfxTlf35584_ProtectionKey_lockKey3 = 190,         /**< \brief Key 3 to lock protected registers. */
    IfxTlf35584_ProtectionKey_lockKey4 = 202,         /**< \brief Key 4 to lock protected registers. */
    IfxTlf35584_ProtectionKey_lockKey1 = 223,         /**< \brief Key 1 to lock protected registers. */
    IfxTlf35584_ProtectionKey_unlockKey2 = 239,       /**< \brief Key 2 to unlock protected registers. */
}IfxTlf35584_ProtectionKey;

/** \brief Request standby regulator QST enable
 * Bitfield(s): IfxTlf35584_SYSPCFG0.STBYEN
 */
typedef enum
{
    IfxTlf35584_RequestStandbyRegulatorQstEnable_disabled = 0,/**< \brief Disabled */
    IfxTlf35584_RequestStandbyRegulatorQstEnable_enabled = 1,/**< \brief Enabled */
}IfxTlf35584_RequestStandbyRegulatorQstEnable;

/** \brief Request ERR pin monitor recovery time
 * Bitfield(s): IfxTlf35584_SYSPCFG1.ERRREC
 */
typedef enum
{
    IfxTlf35584_RequestErrPinMonitorRecoveryTime_1ms = 0,/**< \brief 1 ms */
    IfxTlf35584_RequestErrPinMonitorRecoveryTime_2ms5 = 1,/**< \brief 2.5 ms */
    IfxTlf35584_RequestErrPinMonitorRecoveryTime_5ms = 2,/**< \brief 5 ms */
    IfxTlf35584_RequestErrPinMonitorRecoveryTime_10ms = 3,/**< \brief 10 ms */
}IfxTlf35584_RequestErrPinMonitorRecoveryTime;

/** \brief Request ERR pin monitor recovery enable
 * Bitfield(s): IfxTlf35584_SYSPCFG1.ERRRECEN
 */
typedef enum
{
    IfxTlf35584_RequestErrPinMonitorRecoveryEnable_disabled = 0,/**< \brief Disabled */
    IfxTlf35584_RequestErrPinMonitorRecoveryEnable_enabled = 1,/**< \brief Enabled */
}IfxTlf35584_RequestErrPinMonitorRecoveryEnable;

/** \brief Request ERR pin monitor enable
 * Bitfield(s): IfxTlf35584_SYSPCFG1.ERREN
 */
typedef enum
{
    IfxTlf35584_RequestErrPinMonitorEnable_disabled = 0,/**< \brief Disabled */
    IfxTlf35584_RequestErrPinMonitorEnable_enabled = 1,/**< \brief Enabled */
}IfxTlf35584_RequestErrPinMonitorEnable;

/** \brief Request ERR pin monitor functionaility enable while\nthe system is in SLEEP
 * Bitfield(s): IfxTlf35584_SYSPCFG1.ERRSLPEN
 */
typedef enum
{
    IfxTlf35584_RequestErrPinMonitorFunctionailityEnableWhileTheSystemIsInSleep_disabled = 0,/**< \brief ERR pin monitor is disabled in SLEEP */
    IfxTlf35584_RequestErrPinMonitorFunctionailityEnableWhileTheSystemIsInSleep_enabled = 1,/**< \brief ERR pin monitor can be active in SLEEP depending on ERREN bit value. */
}IfxTlf35584_RequestErrPinMonitorFunctionailityEnableWhileTheSystemIsInSleep;

/** \brief Request safe state 2 delay
 * Bitfield(s): IfxTlf35584_SYSPCFG1.SS2DEL
 */
typedef enum
{
    IfxTlf35584_RequestSafeState2Delay_noDelay = 0,   /**< \brief no delay */
    IfxTlf35584_RequestSafeState2Delay_10ms = 1,      /**< \brief 10 ms */
    IfxTlf35584_RequestSafeState2Delay_50ms = 2,      /**< \brief 50 ms */
    IfxTlf35584_RequestSafeState2Delay_100ms = 3,     /**< \brief 100 ms */
    IfxTlf35584_RequestSafeState2Delay_250ms = 4,     /**< \brief 250 ms */
}IfxTlf35584_RequestSafeState2Delay;

/** \brief Request watchdog cycle time
 * Bitfield(s): IfxTlf35584_WDCFG0.WDCYC
 */
typedef enum
{
    IfxTlf35584_RequestWatchdogCycleTime_100us = 0,   /**< \brief 0,1 ms tick period */
    IfxTlf35584_RequestWatchdogCycleTime_1ms = 1,     /**< \brief 1 ms tick period */
}IfxTlf35584_RequestWatchdogCycleTime;

/** \brief Request window watchdog trigger selection
 * Bitfield(s): IfxTlf35584_WDCFG0.WWDTSEL
 */
typedef enum
{
    IfxTlf35584_RequestWindowWatchdogTriggerSelection_wdiPin = 0,/**< \brief External WDI input used as a WWD trigger */
    IfxTlf35584_RequestWindowWatchdogTriggerSelection_spi = 1,/**< \brief WWD is triggered by SPI write to WWDSCMD register */
}IfxTlf35584_RequestWindowWatchdogTriggerSelection;

/** \brief Request functional watchdog enable
 * Bitfield(s): IfxTlf35584_WDCFG0.FWDEN
 */
typedef enum
{
    IfxTlf35584_RequestFunctionalWatchdogEnable_disabled = 0,/**< \brief Disabled */
    IfxTlf35584_RequestFunctionalWatchdogEnable_enabled = 1,/**< \brief Enabled */
}IfxTlf35584_RequestFunctionalWatchdogEnable;

/** \brief Request window watchdog enable
 * Bitfield(s): IfxTlf35584_WDCFG0.WWDEN
 */
typedef enum
{
    IfxTlf35584_RequestWindowWatchdogEnable_disabled = 0,/**< \brief Disabled */
    IfxTlf35584_RequestWindowWatchdogEnable_enabled = 1,/**< \brief Enabled */
}IfxTlf35584_RequestWindowWatchdogEnable;

/** \brief Request watchdog functionality enable while the\ndevice is in SLEEP
 * Bitfield(s): IfxTlf35584_WDCFG1.WDSLPEN
 */
typedef enum
{
    IfxTlf35584_RequestWatchdogFunctionalityEnableWhileTheDeviceIsInSleep_disabled = 0,/**< \brief Disabled */
    IfxTlf35584_RequestWatchdogFunctionalityEnableWhileTheDeviceIsInSleep_enabled = 1,/**< \brief Enabled, the WD will work based on individual configuration (WWDEN & FWDEN) settings while the system is in SLEEP mode */
}IfxTlf35584_RequestWatchdogFunctionalityEnableWhileTheDeviceIsInSleep;

/** \brief Request functional watchdog heartbeat timer period
 * Bitfield(s): IfxTlf35584_FWDCFG.WDHBTP
 */
typedef enum
{
    IfxTlf35584_RequestFunctionalWatchdogHeartbeatTimerPeriod_50 = 0,/**< \brief 50 wd cycles */
    IfxTlf35584_RequestFunctionalWatchdogHeartbeatTimerPeriod_100 = 1,/**< \brief 100 wd cycles */
    IfxTlf35584_RequestFunctionalWatchdogHeartbeatTimerPeriod_150 = 2,/**< \brief 150 wd cycles */
    IfxTlf35584_RequestFunctionalWatchdogHeartbeatTimerPeriod_1600 = 31,/**< \brief 1600 wd cycles */
}IfxTlf35584_RequestFunctionalWatchdogHeartbeatTimerPeriod;

/** \brief Request window watchdog closed window time
 * Bitfield(s): IfxTlf35584_WWDCFG0.CW
 */
typedef enum
{
    IfxTlf35584_RequestWindowWatchdogClosedWindowTime_50 = 0,/**< \brief 50 wd cycles */
    IfxTlf35584_RequestWindowWatchdogClosedWindowTime_100 = 1,/**< \brief 100 wd cycles */
    IfxTlf35584_RequestWindowWatchdogClosedWindowTime_150 = 2,/**< \brief 150 wd cycles */
    IfxTlf35584_RequestWindowWatchdogClosedWindowTime_1600 = 31,/**< \brief 1600 wd cycles */
}IfxTlf35584_RequestWindowWatchdogClosedWindowTime;

/** \brief Request window watchdog open window time
 * Bitfield(s): IfxTlf35584_WWDCFG1.OW
 */
typedef enum
{
    IfxTlf35584_RequestWindowWatchdogOpenWindowTime_50 = 0,/**< \brief 50 wd cycles */
    IfxTlf35584_RequestWindowWatchdogOpenWindowTime_100 = 1,/**< \brief 100 wd cycles */
    IfxTlf35584_RequestWindowWatchdogOpenWindowTime_150 = 2,/**< \brief 150 wd cycles */
    IfxTlf35584_RequestWindowWatchdogOpenWindowTime_1600 = 31,/**< \brief 1600 wd cycles */
}IfxTlf35584_RequestWindowWatchdogOpenWindowTime;

/** \brief Standby regulator QST enable status
 * Bitfield(s): IfxTlf35584_RSYSPCFG0.STBYEN
 */
typedef enum
{
    IfxTlf35584_StandbyRegulatorQstEnableStatus_disabled = 0,/**< \brief Disabled */
    IfxTlf35584_StandbyRegulatorQstEnableStatus_enabled = 1,/**< \brief Enabled */
}IfxTlf35584_StandbyRegulatorQstEnableStatus;

/** \brief ERR pin monitor recovery time status
 * Bitfield(s): IfxTlf35584_RSYSPCFG1.ERRREC
 */
typedef enum
{
    IfxTlf35584_ErrPinMonitorRecoveryTimeStatus_1ms = 0,/**< \brief 1 ms */
    IfxTlf35584_ErrPinMonitorRecoveryTimeStatus_2ms5 = 1,/**< \brief 2.5 ms */
    IfxTlf35584_ErrPinMonitorRecoveryTimeStatus_5ms = 2,/**< \brief 5 ms */
    IfxTlf35584_ErrPinMonitorRecoveryTimeStatus_10ms = 3,/**< \brief 10 ms */
}IfxTlf35584_ErrPinMonitorRecoveryTimeStatus;

/** \brief ERR pin monitor recovery enable status
 * Bitfield(s): IfxTlf35584_RSYSPCFG1.ERRRECEN
 */
typedef enum
{
    IfxTlf35584_ErrPinMonitorRecoveryEnableStatus_disabled = 0,/**< \brief Disabled */
    IfxTlf35584_ErrPinMonitorRecoveryEnableStatus_enabled = 1,/**< \brief Enabled */
}IfxTlf35584_ErrPinMonitorRecoveryEnableStatus;

/** \brief ERR pin monitor enable status
 * Bitfield(s): IfxTlf35584_RSYSPCFG1.ERREN
 */
typedef enum
{
    IfxTlf35584_ErrPinMonitorEnableStatus_disabled = 0,/**< \brief Disabled */
    IfxTlf35584_ErrPinMonitorEnableStatus_enabled = 1,/**< \brief Enabled */
}IfxTlf35584_ErrPinMonitorEnableStatus;

/** \brief ERR pin monitor functionality enable status while\nthe device is in SLEEP
 * Bitfield(s): IfxTlf35584_RSYSPCFG1.ERRSLPEN
 */
typedef enum
{
    IfxTlf35584_ErrPinMonitorFunctionalityEnableStatusWhileTheDeviceIsInSleep_disabled = 0,/**< \brief ERR pin monitor is disabled in SLEEP */
    IfxTlf35584_ErrPinMonitorFunctionalityEnableStatusWhileTheDeviceIsInSleep_enabled = 1,/**< \brief ERR pin monitor can be active in SLEEP depending on ERREN bit value. */
}IfxTlf35584_ErrPinMonitorFunctionalityEnableStatusWhileTheDeviceIsInSleep;

/** \brief Safe state 2 delay status
 * Bitfield(s): IfxTlf35584_RSYSPCFG1.SS2DEL
 */
typedef enum
{
    IfxTlf35584_SafeState2DelayStatus_noDelay = 0,    /**< \brief no delay */
    IfxTlf35584_SafeState2DelayStatus_10ms = 1,       /**< \brief 10 ms */
    IfxTlf35584_SafeState2DelayStatus_50ms = 2,       /**< \brief 50 ms */
    IfxTlf35584_SafeState2DelayStatus_100ms = 3,      /**< \brief 100 ms */
    IfxTlf35584_SafeState2DelayStatus_250ms = 4,      /**< \brief 250 ms */
}IfxTlf35584_SafeState2DelayStatus;

/** \brief Watchdog cycle time status
 * Bitfield(s): IfxTlf35584_RWDCFG0.WDCYC
 */
typedef enum
{
    IfxTlf35584_WatchdogCycleTimeStatus_100us = 0,    /**< \brief 0,1 ms tick period */
    IfxTlf35584_WatchdogCycleTimeStatus_1ms = 1,      /**< \brief 1 ms tick period */
}IfxTlf35584_WatchdogCycleTimeStatus;

/** \brief Window watchdog trigger selection status
 * Bitfield(s): IfxTlf35584_RWDCFG0.WWDTSEL
 */
typedef enum
{
    IfxTlf35584_WindowWatchdogTriggerSelectionStatus_wdiPin = 0,/**< \brief External WDI input used as a WWD trigger */
    IfxTlf35584_WindowWatchdogTriggerSelectionStatus_spi = 1,/**< \brief WWD is triggered by SPI write to WWDSCMD register */
}IfxTlf35584_WindowWatchdogTriggerSelectionStatus;

/** \brief Functional watchdog enable status
 * Bitfield(s): IfxTlf35584_RWDCFG0.FWDEN
 */
typedef enum
{
    IfxTlf35584_FunctionalWatchdogEnableStatus_disabled = 0,/**< \brief Disabled */
    IfxTlf35584_FunctionalWatchdogEnableStatus_enabled = 1,/**< \brief Enabled */
}IfxTlf35584_FunctionalWatchdogEnableStatus;

/** \brief Window watchdog enable status
 * Bitfield(s): IfxTlf35584_RWDCFG0.WWDEN
 */
typedef enum
{
    IfxTlf35584_WindowWatchdogEnableStatus_disabled = 0,/**< \brief Disabled */
    IfxTlf35584_WindowWatchdogEnableStatus_enabled = 1,/**< \brief Enabled */
}IfxTlf35584_WindowWatchdogEnableStatus;

/** \brief Watchdog functionality enable status while the device\nis in SLEEP
 * Bitfield(s): IfxTlf35584_RWDCFG1.WDSLPEN
 */
typedef enum
{
    IfxTlf35584_WatchdogFunctionalityEnableStatusWhileTheDeviceIsInSleep_disabled = 0,/**< \brief Disabled */
    IfxTlf35584_WatchdogFunctionalityEnableStatusWhileTheDeviceIsInSleep_enabled = 1,/**< \brief Enabled, the WD will work based on individual configuration (WWDEN & FWDEN) settings while the system is in SLEEP mode */
}IfxTlf35584_WatchdogFunctionalityEnableStatusWhileTheDeviceIsInSleep;

/** \brief Functional watchdog heartbeat timer period status
 * Bitfield(s): IfxTlf35584_RFWDCFG.WDHBTP
 */
typedef enum
{
    IfxTlf35584_FunctionalWatchdogHeartbeatTimerPeriodStatus_50 = 0,/**< \brief 50 wd cycles */
    IfxTlf35584_FunctionalWatchdogHeartbeatTimerPeriodStatus_100 = 1,/**< \brief 100 wd cycles */
    IfxTlf35584_FunctionalWatchdogHeartbeatTimerPeriodStatus_150 = 2,/**< \brief 150 wd cycles */
    IfxTlf35584_FunctionalWatchdogHeartbeatTimerPeriodStatus_1600 = 31,/**< \brief 1600 wd cycles */
}IfxTlf35584_FunctionalWatchdogHeartbeatTimerPeriodStatus;

/** \brief Window watchdog closed window time status
 * Bitfield(s): IfxTlf35584_RWWDCFG0.CW
 */
typedef enum
{
    IfxTlf35584_WindowWatchdogClosedWindowTimeStatus_50 = 0,/**< \brief 50 wd cycles */
    IfxTlf35584_WindowWatchdogClosedWindowTimeStatus_100 = 1,/**< \brief 100 wd cycles */
    IfxTlf35584_WindowWatchdogClosedWindowTimeStatus_150 = 2,/**< \brief 150 wd cycles */
    IfxTlf35584_WindowWatchdogClosedWindowTimeStatus_1600 = 31,/**< \brief 1600 wd cycles */
}IfxTlf35584_WindowWatchdogClosedWindowTimeStatus;

/** \brief Window watchdog open window time status
 * Bitfield(s): IfxTlf35584_RWWDCFG1.OW
 */
typedef enum
{
    IfxTlf35584_WindowWatchdogOpenWindowTimeStatus_50 = 0,/**< \brief 50 wd cycles */
    IfxTlf35584_WindowWatchdogOpenWindowTimeStatus_100 = 1,/**< \brief 100 wd cycles */
    IfxTlf35584_WindowWatchdogOpenWindowTimeStatus_150 = 2,/**< \brief 150 wd cycles */
    IfxTlf35584_WindowWatchdogOpenWindowTimeStatus_1600 = 31,/**< \brief 1600 wd cycles */
}IfxTlf35584_WindowWatchdogOpenWindowTimeStatus;

/** \brief Request for device state transition
 * Bitfield(s): IfxTlf35584_DEVCTRL.STATEREQ
 */
typedef enum
{
    IfxTlf35584_RequestForDeviceStateTransition_noStateChange = 0,/**< \brief NONE */
    IfxTlf35584_RequestForDeviceStateTransition_init = 1,/**< \brief INIT */
    IfxTlf35584_RequestForDeviceStateTransition_normal = 2,/**< \brief NORMAL */
    IfxTlf35584_RequestForDeviceStateTransition_sleep = 3,/**< \brief SLEEP */
    IfxTlf35584_RequestForDeviceStateTransition_standby = 4,/**< \brief STANDBY */
    IfxTlf35584_RequestForDeviceStateTransition_wake = 5,/**< \brief WAKE */
}IfxTlf35584_RequestForDeviceStateTransition;

/** \brief Request voltage reference QVR enable
 * Bitfield(s): IfxTlf35584_DEVCTRL.VREFEN
 */
typedef enum
{
    IfxTlf35584_RequestVoltageReferenceQvrEnable_disabled = 0,/**< \brief QVR will be disabled after valid request */
    IfxTlf35584_RequestVoltageReferenceQvrEnable_enabled = 1,/**< \brief QVR will be enabled after valid request */
}IfxTlf35584_RequestVoltageReferenceQvrEnable;

/** \brief Request communication ldo QCO enable
 * Bitfield(s): IfxTlf35584_DEVCTRL.COMEN
 */
typedef enum
{
    IfxTlf35584_RequestCommunicationLdoQcoEnable_disabled = 0,/**< \brief QCO will be disabled after valid request */
    IfxTlf35584_RequestCommunicationLdoQcoEnable_enabled = 1,/**< \brief QCO will be enabled after valid request */
}IfxTlf35584_RequestCommunicationLdoQcoEnable;

/** \brief Request tracker1 QT1 enable
 * Bitfield(s): IfxTlf35584_DEVCTRL.TRK1EN
 */
typedef enum
{
    IfxTlf35584_RequestTracker1Qt1Enable_disabled = 0,/**< \brief QT1 will be disabled after valid request */
    IfxTlf35584_RequestTracker1Qt1Enable_enabled = 1, /**< \brief QT1 will be enabled after valid request */
}IfxTlf35584_RequestTracker1Qt1Enable;

/** \brief Request tracker2 QT2 enable
 * Bitfield(s): IfxTlf35584_DEVCTRL.TRK2EN
 */
typedef enum
{
    IfxTlf35584_RequestTracker2Qt2Enable_disabled = 0,/**< \brief QT2 will be disabled after valid request */
    IfxTlf35584_RequestTracker2Qt2Enable_enabled = 1, /**< \brief QT2 will be enabled after valid request */
}IfxTlf35584_RequestTracker2Qt2Enable;

/** \brief Double Bit error on voltage selection flag
 * Bitfield(s): IfxTlf35584_SYSFAIL.VOLTSELERR
 */
typedef enum
{
    IfxTlf35584_DoubleBitErrorOnVoltageSelectionFlag_notSet = 0,/**< \brief No fault, write 0 - no action */
    IfxTlf35584_DoubleBitErrorOnVoltageSelectionFlag_setOrClearFlag = 1,/**< \brief Fault occurred, write 1 to clear the flags */
}IfxTlf35584_DoubleBitErrorOnVoltageSelectionFlag;

/** \brief Over temperature failure flag
 * Bitfield(s): IfxTlf35584_SYSFAIL.OTF
 */
typedef enum
{
    IfxTlf35584_OverTemperatureFailureFlag_notSet = 0,/**< \brief No fault, write 0 - no action */
    IfxTlf35584_OverTemperatureFailureFlag_setOrClearFlag = 1,/**< \brief Fault occurred, write 1 to clear the flags, read */
}IfxTlf35584_OverTemperatureFailureFlag;

/** \brief Voltage monitor failure flag
 * Bitfield(s): IfxTlf35584_SYSFAIL.VMONF
 */
typedef enum
{
    IfxTlf35584_VoltageMonitorFailureFlag_notSet = 0, /**< \brief No fault, write 0 - no action */
    IfxTlf35584_VoltageMonitorFailureFlag_setOrClearFlag = 1,/**< \brief Fault occurred, write 1 to clear the flags, read */
}IfxTlf35584_VoltageMonitorFailureFlag;

/** \brief ABIST operation interrupted flag
 * Bitfield(s): IfxTlf35584_SYSFAIL.ABISTERR
 */
typedef enum
{
    IfxTlf35584_AbistOperationInterruptedFlag_notSet = 0,/**< \brief No fault, write 0 - no action */
    IfxTlf35584_AbistOperationInterruptedFlag_setOrClearFlag = 1,/**< \brief Fault occurred, write 1 to clear the flags */
}IfxTlf35584_AbistOperationInterruptedFlag;

/** \brief INIT failure flag
 * Bitfield(s): IfxTlf35584_SYSFAIL.INITF
 */
typedef enum
{
    IfxTlf35584_InitFailureFlag_notSet = 0,           /**< \brief No fault, write 0 - no action */
    IfxTlf35584_InitFailureFlag_setOrClearFlag = 1,   /**< \brief Fault occurred, write 1 to clear the flags */
}IfxTlf35584_InitFailureFlag;

/** \brief Voltage monitor init failure flag
 * Bitfield(s): IfxTlf35584_INITERR.VMONF
 */
typedef enum
{
    IfxTlf35584_VoltageMonitorInitFailureFlag_notSet = 0,/**< \brief No fault, write 0 - no action */
    IfxTlf35584_VoltageMonitorInitFailureFlag_setOrClearFlag = 1,/**< \brief Fault occurred, write 1 to clear the flags, read */
}IfxTlf35584_VoltageMonitorInitFailureFlag;

/** \brief Window watchdog error counter overflow failure flag
 * Bitfield(s): IfxTlf35584_INITERR.WWDF
 */
typedef enum
{
    IfxTlf35584_WindowWatchdogErrorCounterOverflowFailureFlag_notSet = 0,/**< \brief No fault, write 0 - no action */
    IfxTlf35584_WindowWatchdogErrorCounterOverflowFailureFlag_setOrClearFlag = 1,/**< \brief Fault occurred, write 1 to clear the flags */
}IfxTlf35584_WindowWatchdogErrorCounterOverflowFailureFlag;

/** \brief Functional watchdog error counter overflow failure\nflag
 * Bitfield(s): IfxTlf35584_INITERR.FWDF
 */
typedef enum
{
    IfxTlf35584_FunctionalWatchdogErrorCounterOverflowFailureFlag_notSet = 0,/**< \brief No fault, write 0 - no action */
    IfxTlf35584_FunctionalWatchdogErrorCounterOverflowFailureFlag_setOrClearFlag = 1,/**< \brief Fault occurred, write 1 to clear the flags */
}IfxTlf35584_FunctionalWatchdogErrorCounterOverflowFailureFlag;

/** \brief MCU error monitor failure flag
 * Bitfield(s): IfxTlf35584_INITERR.ERRF
 */
typedef enum
{
    IfxTlf35584_McuErrorMonitorFailureFlag_notSet = 0,/**< \brief No fault, write 0 - no action */
    IfxTlf35584_McuErrorMonitorFailureFlag_setOrClearFlag = 1,/**< \brief Fault occurred, write 1 to clear the flags */
}IfxTlf35584_McuErrorMonitorFailureFlag;

/** \brief Soft reset flag
 * Bitfield(s): IfxTlf35584_INITERR.SOFTRES
 */
typedef enum
{
    IfxTlf35584_SoftResetFlag_notSet = 0,             /**< \brief No fault, write 0 - no action */
    IfxTlf35584_SoftResetFlag_setOrClearFlag = 1,     /**< \brief Fault occurred, write 1 to clear the flags */
}IfxTlf35584_SoftResetFlag;

/** \brief Hard reset flag
 * Bitfield(s): IfxTlf35584_INITERR.HARDRES
 */
typedef enum
{
    IfxTlf35584_HardResetFlag_notSet = 0,             /**< \brief No fault, write 0 - no action */
    IfxTlf35584_HardResetFlag_setOrClearFlag = 1,     /**< \brief Fault occurred, write 1 to clear the flags */
}IfxTlf35584_HardResetFlag;

/** \brief System interrupt flag
 * Bitfield(s): IfxTlf35584_IF.SYS
 */
typedef enum
{
    IfxTlf35584_SystemInterruptFlag_notSet = 0,       /**< \brief No interrupt, write 0 - no action */
    IfxTlf35584_SystemInterruptFlag_setOrClearFlag = 1,/**< \brief Interrupt flag active, write 1 to clear the flag, read */
}IfxTlf35584_SystemInterruptFlag;

/** \brief Wake interrupt flag
 * Bitfield(s): IfxTlf35584_IF.WK
 */
typedef enum
{
    IfxTlf35584_WakeInterruptFlag_notSet = 0,         /**< \brief No interrupt, write 0 - no action */
    IfxTlf35584_WakeInterruptFlag_setOrClearFlag = 1, /**< \brief Interrupt flag active, write 1 to clear the flag, read */
}IfxTlf35584_WakeInterruptFlag;

/** \brief SPI interrupt flag
 * Bitfield(s): IfxTlf35584_IF.SPI
 */
typedef enum
{
    IfxTlf35584_SpiInterruptFlag_notSet = 0,          /**< \brief No interrupt, write 0 - no action */
    IfxTlf35584_SpiInterruptFlag_setOrClearFlag = 1,  /**< \brief Interrupt flag active, write 1 to clear the flag, read */
}IfxTlf35584_SpiInterruptFlag;

/** \brief Monitor interrupt flag
 * Bitfield(s): IfxTlf35584_IF.MON
 */
typedef enum
{
    IfxTlf35584_MonitorInterruptFlag_notSet = 0,      /**< \brief No interrupt, write 0 - no action */
    IfxTlf35584_MonitorInterruptFlag_setOrClearFlag = 1,/**< \brief Interrupt flag active, write 1 to clear the flag, read */
}IfxTlf35584_MonitorInterruptFlag;

/** \brief Over temperature warning interrupt flag
 * Bitfield(s): IfxTlf35584_IF.OTW
 */
typedef enum
{
    IfxTlf35584_OverTemperatureWarningInterruptFlag_notSet = 0,/**< \brief No interrupt, write 0 - no action */
    IfxTlf35584_OverTemperatureWarningInterruptFlag_setOrClearFlag = 1,/**< \brief Interrupt flag active, write 1 to clear the flag, read */
}IfxTlf35584_OverTemperatureWarningInterruptFlag;

/** \brief Over temperature failure interrupt flag
 * Bitfield(s): IfxTlf35584_IF.OTF
 */
typedef enum
{
    IfxTlf35584_OverTemperatureFailureInterruptFlag_notSet = 0,/**< \brief No interrupt, write 0 - no action */
    IfxTlf35584_OverTemperatureFailureInterruptFlag_setOrClearFlag = 1,/**< \brief Interrupt flag active, write 1 to clear the flag, read */
}IfxTlf35584_OverTemperatureFailureInterruptFlag;

/** \brief Requested ABIST operation performed flag
 * Bitfield(s): IfxTlf35584_IF.ABIST
 */
typedef enum
{
    IfxTlf35584_RequestedAbistOperationPerformedFlag_notSet = 0,/**< \brief No interrupt, write 0 - no action */
    IfxTlf35584_RequestedAbistOperationPerformedFlag_setOrClearFlag = 1,/**< \brief Interrupt flag active, write 1 to clear the flag */
}IfxTlf35584_RequestedAbistOperationPerformedFlag;

/** \brief Interrupt not serviced in time flag
 * Bitfield(s): IfxTlf35584_IF.INTMISS
 */
typedef enum
{
    IfxTlf35584_InterruptNotServicedInTimeFlag_notSet = 0,/**< \brief No interrupt timeout happened */
    IfxTlf35584_InterruptNotServicedInTimeFlag_setOrClearFlag = 1,/**< \brief Interrupt timeout happened, cleared by hardware when all other flags in IF are cleared. */
}IfxTlf35584_InterruptNotServicedInTimeFlag;

/** \brief Protected configuration double bit error flag
 * Bitfield(s): IfxTlf35584_SYSSF.CFGE
 */
typedef enum
{
    IfxTlf35584_ProtectedConfigurationDoubleBitErrorFlag_notSet = 0,/**< \brief Write 0 - no action */
    IfxTlf35584_ProtectedConfigurationDoubleBitErrorFlag_setOrClearFlag = 1,/**< \brief Event detected, write 1 to clear the flag */
}IfxTlf35584_ProtectedConfigurationDoubleBitErrorFlag;

/** \brief Window watchdog error interrupt flag
 * Bitfield(s): IfxTlf35584_SYSSF.WWDE
 */
typedef enum
{
    IfxTlf35584_WindowWatchdogErrorInterruptFlag_notSet = 0,/**< \brief Write 0 - no action */
    IfxTlf35584_WindowWatchdogErrorInterruptFlag_setOrClearFlag = 1,/**< \brief Event detected, write 1 to clear the flag */
}IfxTlf35584_WindowWatchdogErrorInterruptFlag;

/** \brief Functional watchdog error interrupt flag
 * Bitfield(s): IfxTlf35584_SYSSF.FWDE
 */
typedef enum
{
    IfxTlf35584_FunctionalWatchdogErrorInterruptFlag_notSet = 0,/**< \brief Write 0 - no action */
    IfxTlf35584_FunctionalWatchdogErrorInterruptFlag_setOrClearFlag = 1,/**< \brief Event detected, write 1 to clear the flag */
}IfxTlf35584_FunctionalWatchdogErrorInterruptFlag;

/** \brief MCU error miss status flag
 * Bitfield(s): IfxTlf35584_SYSSF.ERRMISS
 */
typedef enum
{
    IfxTlf35584_McuErrorMissStatusFlag_notSet = 0,    /**< \brief Write 0 - no action */
    IfxTlf35584_McuErrorMissStatusFlag_setOrClearFlag = 1,/**< \brief Event detected, write 1 to clear the flag */
}IfxTlf35584_McuErrorMissStatusFlag;

/** \brief Transition to low power failed flag
 * Bitfield(s): IfxTlf35584_SYSSF.TRFAIL
 */
typedef enum
{
    IfxTlf35584_TransitionToLowPowerFailedFlag_notSet = 0,/**< \brief Write 0 - no action */
    IfxTlf35584_TransitionToLowPowerFailedFlag_setOrClearFlag = 1,/**< \brief Event detected, write 1 to clear the flag */
}IfxTlf35584_TransitionToLowPowerFailedFlag;

/** \brief State transition request failure flag
 * Bitfield(s): IfxTlf35584_SYSSF.NO_OP
 */
typedef enum
{
    IfxTlf35584_StateTransitionRequestFailureFlag_notSet = 0,/**< \brief Write 0 - no action */
    IfxTlf35584_StateTransitionRequestFailureFlag_setOrClearFlag = 1,/**< \brief Event detected, write 1 to clear the flag */
}IfxTlf35584_StateTransitionRequestFailureFlag;

/** \brief WAK signal wakeup flag
 * Bitfield(s): IfxTlf35584_WKSF.WAK
 */
typedef enum
{
    IfxTlf35584_WakSignalWakeupFlag_notSet = 0,       /**< \brief Write 0 - no action */
    IfxTlf35584_WakSignalWakeupFlag_setOrClearFlag = 1,/**< \brief Event detected, write 1 to clear the flag */
}IfxTlf35584_WakSignalWakeupFlag;

/** \brief ENA signal wakeup flag
 * Bitfield(s): IfxTlf35584_WKSF.ENA
 */
typedef enum
{
    IfxTlf35584_EnaSignalWakeupFlag_notSet = 0,       /**< \brief Write 0 - no action */
    IfxTlf35584_EnaSignalWakeupFlag_setOrClearFlag = 1,/**< \brief Event detected, write 1 to clear the flag */
}IfxTlf35584_EnaSignalWakeupFlag;

/** \brief QUC current monitor threshold wakeup flag
 * Bitfield(s): IfxTlf35584_WKSF.CMON
 */
typedef enum
{
    IfxTlf35584_QucCurrentMonitorThresholdWakeupFlag_notSet = 0,/**< \brief Write 0 - no action */
    IfxTlf35584_QucCurrentMonitorThresholdWakeupFlag_setOrClearFlag = 1,/**< \brief Event detected, write 1 to clear the flag */
}IfxTlf35584_QucCurrentMonitorThresholdWakeupFlag;

/** \brief Wake timer wakeup flag
 * Bitfield(s): IfxTlf35584_WKSF.WKTIM
 */
typedef enum
{
    IfxTlf35584_WakeTimerWakeupFlag_notSet = 0,       /**< \brief Write 0 - no action */
    IfxTlf35584_WakeTimerWakeupFlag_setOrClearFlag = 1,/**< \brief Event detected, write 1 to clear the flag */
}IfxTlf35584_WakeTimerWakeupFlag;

/** \brief Wakeup from SLEEP by SPI flag
 * Bitfield(s): IfxTlf35584_WKSF.WKSPI
 */
typedef enum
{
    IfxTlf35584_WakeupFromSleepBySpiFlag_notSet = 0,  /**< \brief Write 0 - no action */
    IfxTlf35584_WakeupFromSleepBySpiFlag_setOrClearFlag = 1,/**< \brief Event detected, write 1 to clear the flag */
}IfxTlf35584_WakeupFromSleepBySpiFlag;

/** \brief SPI frame parity error flag
 * Bitfield(s): IfxTlf35584_SPISF.PARE
 */
typedef enum
{
    IfxTlf35584_SpiFrameParityErrorFlag_notSet = 0,   /**< \brief Write 0 - no action */
    IfxTlf35584_SpiFrameParityErrorFlag_setOrClearFlag = 1,/**< \brief Event detected, write 1 to clear the flag */
}IfxTlf35584_SpiFrameParityErrorFlag;

/** \brief SPI frame length invalid flag
 * Bitfield(s): IfxTlf35584_SPISF.LENE
 */
typedef enum
{
    IfxTlf35584_SpiFrameLengthInvalidFlag_notSet = 0, /**< \brief Write 0 - no action */
    IfxTlf35584_SpiFrameLengthInvalidFlag_setOrClearFlag = 1,/**< \brief Event detected, write 1 to clear the flag */
}IfxTlf35584_SpiFrameLengthInvalidFlag;

/** \brief SPI address invalid flag
 * Bitfield(s): IfxTlf35584_SPISF.ADDRE
 */
typedef enum
{
    IfxTlf35584_SpiAddressInvalidFlag_notSet = 0,     /**< \brief Write 0 - no action */
    IfxTlf35584_SpiAddressInvalidFlag_setOrClearFlag = 1,/**< \brief Event detected, write 1 to clear the flag */
}IfxTlf35584_SpiAddressInvalidFlag;

/** \brief SPI frame duration error flag
 * Bitfield(s): IfxTlf35584_SPISF.DURE
 */
typedef enum
{
    IfxTlf35584_SpiFrameDurationErrorFlag_notSet = 0, /**< \brief Write 0 - no action */
    IfxTlf35584_SpiFrameDurationErrorFlag_setOrClearFlag = 1,/**< \brief Event detected, write 1 to clear the flag */
}IfxTlf35584_SpiFrameDurationErrorFlag;

/** \brief LOCK or UNLOCK procedure error flag
 * Bitfield(s): IfxTlf35584_SPISF.LOCK
 */
typedef enum
{
    IfxTlf35584_LockOrUnlockProcedureErrorFlag_notSet = 0,/**< \brief Write 0 - no action */
    IfxTlf35584_LockOrUnlockProcedureErrorFlag_setOrClearFlag = 1,/**< \brief Event detected, write 1 to clear the flag */
}IfxTlf35584_LockOrUnlockProcedureErrorFlag;

/** \brief Pre-regulator voltage short to ground status flag
 * Bitfield(s): IfxTlf35584_MONSF0.PREGSG
 */
typedef enum
{
    IfxTlf35584_PreRegulatorVoltageShortToGroundStatusFlag_notSet = 0,/**< \brief Write 0 - no action */
    IfxTlf35584_PreRegulatorVoltageShortToGroundStatusFlag_setOrClearFlag = 1,/**< \brief Event detected, write 1 to clear the flag */
}IfxTlf35584_PreRegulatorVoltageShortToGroundStatusFlag;

/** \brief uC LDO short to ground status flag
 * Bitfield(s): IfxTlf35584_MONSF0.UCSG
 */
typedef enum
{
    IfxTlf35584_UcLdoShortToGroundStatusFlag_notSet = 0,/**< \brief Write 0 - no action */
    IfxTlf35584_UcLdoShortToGroundStatusFlag_setOrClearFlag = 1,/**< \brief Event detected, write 1 to clear the flag */
}IfxTlf35584_UcLdoShortToGroundStatusFlag;

/** \brief Standby LDO short to ground status flag
 * Bitfield(s): IfxTlf35584_MONSF0.STBYSG
 */
typedef enum
{
    IfxTlf35584_StandbyLdoShortToGroundStatusFlag_notSet = 0,/**< \brief Write 0 - no action */
    IfxTlf35584_StandbyLdoShortToGroundStatusFlag_setOrClearFlag = 1,/**< \brief Event detected, write 1 to clear the flag */
}IfxTlf35584_StandbyLdoShortToGroundStatusFlag;

/** \brief Core voltage short to ground status flag
 * Bitfield(s): IfxTlf35584_MONSF0.VCORESG
 */
typedef enum
{
    IfxTlf35584_CoreVoltageShortToGroundStatusFlag_notSet = 0,/**< \brief Write 0 - no action */
    IfxTlf35584_CoreVoltageShortToGroundStatusFlag_setOrClearFlag = 1,/**< \brief Event detected, write 1 to clear the flag */
}IfxTlf35584_CoreVoltageShortToGroundStatusFlag;

/** \brief Communication LDO short to ground status flag
 * Bitfield(s): IfxTlf35584_MONSF0.COMSG
 */
typedef enum
{
    IfxTlf35584_CommunicationLdoShortToGroundStatusFlag_notSet = 0,/**< \brief Write 0 - no action */
    IfxTlf35584_CommunicationLdoShortToGroundStatusFlag_setOrClearFlag = 1,/**< \brief Event detected, write 1 to clear the flag */
}IfxTlf35584_CommunicationLdoShortToGroundStatusFlag;

/** \brief Voltage reference short to ground status flag
 * Bitfield(s): IfxTlf35584_MONSF0.VREFSG
 */
typedef enum
{
    IfxTlf35584_VoltageReferenceShortToGroundStatusFlag_notSet = 0,/**< \brief Write 0 - no action */
    IfxTlf35584_VoltageReferenceShortToGroundStatusFlag_setOrClearFlag = 1,/**< \brief Event detected, write 1 to clear the flag */
}IfxTlf35584_VoltageReferenceShortToGroundStatusFlag;

/** \brief Tracker1 short to ground status flag
 * Bitfield(s): IfxTlf35584_MONSF0.TRK1SG
 */
typedef enum
{
    IfxTlf35584_Tracker1ShortToGroundStatusFlag_notSet = 0,/**< \brief Write 0 - no action */
    IfxTlf35584_Tracker1ShortToGroundStatusFlag_setOrClearFlag = 1,/**< \brief Event detected, write 1 to clear the flag */
}IfxTlf35584_Tracker1ShortToGroundStatusFlag;

/** \brief Tracker2 short to ground status flag
 * Bitfield(s): IfxTlf35584_MONSF0.TRK2SG
 */
typedef enum
{
    IfxTlf35584_Tracker2ShortToGroundStatusFlag_notSet = 0,/**< \brief Write 0 - no action */
    IfxTlf35584_Tracker2ShortToGroundStatusFlag_setOrClearFlag = 1,/**< \brief Event detected, write 1 to clear the flag */
}IfxTlf35584_Tracker2ShortToGroundStatusFlag;

/** \brief Pre-regulator voltage over voltage status flag
 * Bitfield(s): IfxTlf35584_MONSF1.PREGOV
 */
typedef enum
{
    IfxTlf35584_PreRegulatorVoltageOverVoltageStatusFlag_notSet = 0,/**< \brief Write 0 - no action */
    IfxTlf35584_PreRegulatorVoltageOverVoltageStatusFlag_setOrClearFlag = 1,/**< \brief Event detected, write 1 to clear the flag */
}IfxTlf35584_PreRegulatorVoltageOverVoltageStatusFlag;

/** \brief uC LDO over voltage status flag
 * Bitfield(s): IfxTlf35584_MONSF1.UCOV
 */
typedef enum
{
    IfxTlf35584_UcLdoOverVoltageStatusFlag_notSet = 0,/**< \brief Write 0 - no action */
    IfxTlf35584_UcLdoOverVoltageStatusFlag_setOrClearFlag = 1,/**< \brief Event detected, write 1 to clear the flag */
}IfxTlf35584_UcLdoOverVoltageStatusFlag;

/** \brief Standby LDO over voltage status flag
 * Bitfield(s): IfxTlf35584_MONSF1.STBYOV
 */
typedef enum
{
    IfxTlf35584_StandbyLdoOverVoltageStatusFlag_notSet = 0,/**< \brief Write 0 - no action */
    IfxTlf35584_StandbyLdoOverVoltageStatusFlag_setOrClearFlag = 1,/**< \brief Event detected, write 1 to clear the flag */
}IfxTlf35584_StandbyLdoOverVoltageStatusFlag;

/** \brief Core voltage over voltage status flag
 * Bitfield(s): IfxTlf35584_MONSF1.VCOREOV
 */
typedef enum
{
    IfxTlf35584_CoreVoltageOverVoltageStatusFlag_notSet = 0,/**< \brief Write 0 - no action */
    IfxTlf35584_CoreVoltageOverVoltageStatusFlag_setOrClearFlag = 1,/**< \brief Event detected, write 1 to clear the flag */
}IfxTlf35584_CoreVoltageOverVoltageStatusFlag;

/** \brief Communication LDO over voltage status flag
 * Bitfield(s): IfxTlf35584_MONSF1.COMOV
 */
typedef enum
{
    IfxTlf35584_CommunicationLdoOverVoltageStatusFlag_notSet = 0,/**< \brief Write 0 - no action */
    IfxTlf35584_CommunicationLdoOverVoltageStatusFlag_setOrClearFlag = 1,/**< \brief Event detected, write 1 to clear the flag */
}IfxTlf35584_CommunicationLdoOverVoltageStatusFlag;

/** \brief Voltage reference over voltage status flag
 * Bitfield(s): IfxTlf35584_MONSF1.VREFOV
 */
typedef enum
{
    IfxTlf35584_VoltageReferenceOverVoltageStatusFlag_notSet = 0,/**< \brief Write 0 - no action */
    IfxTlf35584_VoltageReferenceOverVoltageStatusFlag_setOrClearFlag = 1,/**< \brief Event detected, write 1 to clear the flag */
}IfxTlf35584_VoltageReferenceOverVoltageStatusFlag;

/** \brief Tracker1 over voltage status flag
 * Bitfield(s): IfxTlf35584_MONSF1.TRK1OV
 */
typedef enum
{
    IfxTlf35584_Tracker1OverVoltageStatusFlag_notSet = 0,/**< \brief Write 0 - no action */
    IfxTlf35584_Tracker1OverVoltageStatusFlag_setOrClearFlag = 1,/**< \brief Event detected, write 1 to clear the flag */
}IfxTlf35584_Tracker1OverVoltageStatusFlag;

/** \brief Tracker2 over voltage status flag
 * Bitfield(s): IfxTlf35584_MONSF1.TRK2OV
 */
typedef enum
{
    IfxTlf35584_Tracker2OverVoltageStatusFlag_notSet = 0,/**< \brief Write 0 - no action */
    IfxTlf35584_Tracker2OverVoltageStatusFlag_setOrClearFlag = 1,/**< \brief Event detected, write 1 to clear the flag */
}IfxTlf35584_Tracker2OverVoltageStatusFlag;

/** \brief Pre-regulator voltage under voltage status flag
 * Bitfield(s): IfxTlf35584_MONSF2.PREGUV
 */
typedef enum
{
    IfxTlf35584_PreRegulatorVoltageUnderVoltageStatusFlag_notSet = 0,/**< \brief Write 0 - no action */
    IfxTlf35584_PreRegulatorVoltageUnderVoltageStatusFlag_setOrClearFlag = 1,/**< \brief Event detected, write 1 to clear the flag */
}IfxTlf35584_PreRegulatorVoltageUnderVoltageStatusFlag;

/** \brief uC LDO under voltage status flag
 * Bitfield(s): IfxTlf35584_MONSF2.UCUV
 */
typedef enum
{
    IfxTlf35584_UcLdoUnderVoltageStatusFlag_notSet = 0,/**< \brief Write 0 - no action */
    IfxTlf35584_UcLdoUnderVoltageStatusFlag_setOrClearFlag = 1,/**< \brief Event detected, write 1 to clear the flag */
}IfxTlf35584_UcLdoUnderVoltageStatusFlag;

/** \brief Standby LDO under voltage status flag
 * Bitfield(s): IfxTlf35584_MONSF2.STBYUV
 */
typedef enum
{
    IfxTlf35584_StandbyLdoUnderVoltageStatusFlag_notSet = 0,/**< \brief Write 0 - no action */
    IfxTlf35584_StandbyLdoUnderVoltageStatusFlag_setOrClearFlag = 1,/**< \brief Event detected, write 1 to clear the flag */
}IfxTlf35584_StandbyLdoUnderVoltageStatusFlag;

/** \brief Core voltage under voltage status flag
 * Bitfield(s): IfxTlf35584_MONSF2.VCOREUV
 */
typedef enum
{
    IfxTlf35584_CoreVoltageUnderVoltageStatusFlag_notSet = 0,/**< \brief Write 0 - no action */
    IfxTlf35584_CoreVoltageUnderVoltageStatusFlag_setOrClearFlag = 1,/**< \brief Event detected, write 1 to clear the flag */
}IfxTlf35584_CoreVoltageUnderVoltageStatusFlag;

/** \brief Communication LDO under voltage status flag
 * Bitfield(s): IfxTlf35584_MONSF2.COMUV
 */
typedef enum
{
    IfxTlf35584_CommunicationLdoUnderVoltageStatusFlag_notSet = 0,/**< \brief Write 0 - no action */
    IfxTlf35584_CommunicationLdoUnderVoltageStatusFlag_setOrClearFlag = 1,/**< \brief Event detected, write 1 to clear the flag */
}IfxTlf35584_CommunicationLdoUnderVoltageStatusFlag;

/** \brief Voltage reference under voltage status flag
 * Bitfield(s): IfxTlf35584_MONSF2.VREFUV
 */
typedef enum
{
    IfxTlf35584_VoltageReferenceUnderVoltageStatusFlag_notSet = 0,/**< \brief Write 0 - no action */
    IfxTlf35584_VoltageReferenceUnderVoltageStatusFlag_setOrClearFlag = 1,/**< \brief Event detected, write 1 to clear the flag */
}IfxTlf35584_VoltageReferenceUnderVoltageStatusFlag;

/** \brief Tracker1 under voltage status flag
 * Bitfield(s): IfxTlf35584_MONSF2.TRK1UV
 */
typedef enum
{
    IfxTlf35584_Tracker1UnderVoltageStatusFlag_notSet = 0,/**< \brief Write 0 - no action */
    IfxTlf35584_Tracker1UnderVoltageStatusFlag_setOrClearFlag = 1,/**< \brief Event detected, write 1 to clear the flag */
}IfxTlf35584_Tracker1UnderVoltageStatusFlag;

/** \brief Tracker2 under voltage status flag
 * Bitfield(s): IfxTlf35584_MONSF2.TRK2UV
 */
typedef enum
{
    IfxTlf35584_Tracker2UnderVoltageStatusFlag_notSet = 0,/**< \brief Write 0 - no action */
    IfxTlf35584_Tracker2UnderVoltageStatusFlag_setOrClearFlag = 1,/**< \brief Event detected, write 1 to clear the flag */
}IfxTlf35584_Tracker2UnderVoltageStatusFlag;

/** \brief Supply voltage VS12 over voltage flag
 * Bitfield(s): IfxTlf35584_MONSF3.VBATOV
 */
typedef enum
{
    IfxTlf35584_SupplyVoltageVs12OverVoltageFlag_notSet = 0,/**< \brief Write 0 - no action */
    IfxTlf35584_SupplyVoltageVs12OverVoltageFlag_setOrClearFlag = 1,/**< \brief Event detected, write 1 to clear the flag */
}IfxTlf35584_SupplyVoltageVs12OverVoltageFlag;

/** \brief Bandgap comparator under voltage condition flag
 * Bitfield(s): IfxTlf35584_MONSF3.BG12UV
 */
typedef enum
{
    IfxTlf35584_BandgapComparatorUnderVoltageConditionFlag_notSet = 0,/**< \brief Write 0 - no action */
    IfxTlf35584_BandgapComparatorUnderVoltageConditionFlag_setOrClearFlag = 1,/**< \brief Event detected, write 1 to clear the flag */
}IfxTlf35584_BandgapComparatorUnderVoltageConditionFlag;

/** \brief Bandgap comparator over voltage condition flag
 * Bitfield(s): IfxTlf35584_MONSF3.BG12OV
 */
typedef enum
{
    IfxTlf35584_BandgapComparatorOverVoltageConditionFlag_notSet = 0,/**< \brief Write 0 - no action */
    IfxTlf35584_BandgapComparatorOverVoltageConditionFlag_setOrClearFlag = 1,/**< \brief Event detected, write 1 to clear the flag */
}IfxTlf35584_BandgapComparatorOverVoltageConditionFlag;

/** \brief Bias current too low flag
 * Bitfield(s): IfxTlf35584_MONSF3.BIASLOW
 */
typedef enum
{
    IfxTlf35584_BiasCurrentTooLowFlag_notSet = 0,     /**< \brief Write 0 - no action */
    IfxTlf35584_BiasCurrentTooLowFlag_setOrClearFlag = 1,/**< \brief Event detected, write 1 to clear the flag */
}IfxTlf35584_BiasCurrentTooLowFlag;

/** \brief Bias current too high flag
 * Bitfield(s): IfxTlf35584_MONSF3.BIASHI
 */
typedef enum
{
    IfxTlf35584_BiasCurrentTooHighFlag_notSet = 0,    /**< \brief Write 0 - no action */
    IfxTlf35584_BiasCurrentTooHighFlag_setOrClearFlag = 1,/**< \brief Event detected, write 1 to clear the flag */
}IfxTlf35584_BiasCurrentTooHighFlag;

/** \brief Pre-regulator over temperature flag
 * Bitfield(s): IfxTlf35584_OTFAIL.PREG
 */
typedef enum
{
    IfxTlf35584_PreRegulatorOverTemperatureFlag_notSet = 0,/**< \brief Write 0 - no action */
    IfxTlf35584_PreRegulatorOverTemperatureFlag_setOrClearFlag = 1,/**< \brief Event detected, write 1 to clear the flag */
}IfxTlf35584_PreRegulatorOverTemperatureFlag;

/** \brief uC LDO over temperature flag
 * Bitfield(s): IfxTlf35584_OTFAIL.UC
 */
typedef enum
{
    IfxTlf35584_UcLdoOverTemperatureFlag_notSet = 0,  /**< \brief Write 0 - no action */
    IfxTlf35584_UcLdoOverTemperatureFlag_setOrClearFlag = 1,/**< \brief Event detected, write 1 to clear the flag */
}IfxTlf35584_UcLdoOverTemperatureFlag;

/** \brief Communication LDO over temperature flag
 * Bitfield(s): IfxTlf35584_OTFAIL.COM
 */
typedef enum
{
    IfxTlf35584_CommunicationLdoOverTemperatureFlag_notSet = 0,/**< \brief Write 0 - no action */
    IfxTlf35584_CommunicationLdoOverTemperatureFlag_setOrClearFlag = 1,/**< \brief Event detected, write 1 to clear the flag */
}IfxTlf35584_CommunicationLdoOverTemperatureFlag;

/** \brief Monitoring over temperature flag
 * Bitfield(s): IfxTlf35584_OTFAIL.MON
 */
typedef enum
{
    IfxTlf35584_MonitoringOverTemperatureFlag_notSet = 0,/**< \brief Write 0 - no action */
    IfxTlf35584_MonitoringOverTemperatureFlag_setOrClearFlag = 1,/**< \brief Event detected, write 1 to clear the flag */
}IfxTlf35584_MonitoringOverTemperatureFlag;

/** \brief Pre-regulator over temperature warning flag
 * Bitfield(s): IfxTlf35584_OTWRNSF.PREG
 */
typedef enum
{
    IfxTlf35584_PreRegulatorOverTemperatureWarningFlag_notSet = 0,/**< \brief Write 0 - no action */
    IfxTlf35584_PreRegulatorOverTemperatureWarningFlag_setOrClearFlag = 1,/**< \brief Event detected, write 1 to clear the flag */
}IfxTlf35584_PreRegulatorOverTemperatureWarningFlag;

/** \brief uC LDO over temperature warning flag
 * Bitfield(s): IfxTlf35584_OTWRNSF.UC
 */
typedef enum
{
    IfxTlf35584_UcLdoOverTemperatureWarningFlag_notSet = 0,/**< \brief Write 0 - no action */
    IfxTlf35584_UcLdoOverTemperatureWarningFlag_setOrClearFlag = 1,/**< \brief Event detected, write 1 to clear the flag */
}IfxTlf35584_UcLdoOverTemperatureWarningFlag;

/** \brief Standby LDO over load flag
 * Bitfield(s): IfxTlf35584_OTWRNSF.STDBY
 */
typedef enum
{
    IfxTlf35584_StandbyLdoOverLoadFlag_notSet = 0,    /**< \brief Write 0 - no action */
    IfxTlf35584_StandbyLdoOverLoadFlag_setOrClearFlag = 1,/**< \brief Event detected, write 1 to clear the flag */
}IfxTlf35584_StandbyLdoOverLoadFlag;

/** \brief Communication LDO over temperature warning flag
 * Bitfield(s): IfxTlf35584_OTWRNSF.COM
 */
typedef enum
{
    IfxTlf35584_CommunicationLdoOverTemperatureWarningFlag_notSet = 0,/**< \brief Write 0 - no action */
    IfxTlf35584_CommunicationLdoOverTemperatureWarningFlag_setOrClearFlag = 1,/**< \brief Event detected, write 1 to clear the flag */
}IfxTlf35584_CommunicationLdoOverTemperatureWarningFlag;

/** \brief Voltage reference over load flag
 * Bitfield(s): IfxTlf35584_OTWRNSF.VREF
 */
typedef enum
{
    IfxTlf35584_VoltageReferenceOverLoadFlag_notSet = 0,/**< \brief Write 0 no action */
    IfxTlf35584_VoltageReferenceOverLoadFlag_setOrClearFlag = 1,/**< \brief Event detected, write 1 to clear the flag */
}IfxTlf35584_VoltageReferenceOverLoadFlag;

/** \brief Standby LDO voltage ready status
 * Bitfield(s): IfxTlf35584_VMONSTAT.STBYST
 */
typedef enum
{
    IfxTlf35584_StandbyLdoVoltageReadyStatus_notReady = 0,/**< \brief Voltage is out of range or not enabled */
    IfxTlf35584_StandbyLdoVoltageReadyStatus_ready = 1,/**< \brief Voltage is OK */
}IfxTlf35584_StandbyLdoVoltageReadyStatus;

/** \brief Core voltage ready status
 * Bitfield(s): IfxTlf35584_VMONSTAT.VCOREST
 */
typedef enum
{
    IfxTlf35584_CoreVoltageReadyStatus_notReady = 0,  /**< \brief Voltage is out of range or not enabled */
    IfxTlf35584_CoreVoltageReadyStatus_ready = 1,     /**< \brief Voltage is OK */
}IfxTlf35584_CoreVoltageReadyStatus;

/** \brief Communication LDO voltage ready status
 * Bitfield(s): IfxTlf35584_VMONSTAT.COMST
 */
typedef enum
{
    IfxTlf35584_CommunicationLdoVoltageReadyStatus_notReady = 0,/**< \brief Voltage is out of range or not enabled */
    IfxTlf35584_CommunicationLdoVoltageReadyStatus_ready = 1,/**< \brief Voltage is OK */
}IfxTlf35584_CommunicationLdoVoltageReadyStatus;

/** \brief Voltage reference voltage ready status
 * Bitfield(s): IfxTlf35584_VMONSTAT.VREFST
 */
typedef enum
{
    IfxTlf35584_VoltageReferenceVoltageReadyStatus_notReady = 0,/**< \brief Voltage is out of range or not enabled */
    IfxTlf35584_VoltageReferenceVoltageReadyStatus_ready = 1,/**< \brief Voltage is OK */
}IfxTlf35584_VoltageReferenceVoltageReadyStatus;

/** \brief Tracker1 voltage ready status
 * Bitfield(s): IfxTlf35584_VMONSTAT.TRK1ST
 */
typedef enum
{
    IfxTlf35584_Tracker1VoltageReadyStatus_notReady = 0,/**< \brief Voltage is out of range or not enabled */
    IfxTlf35584_Tracker1VoltageReadyStatus_ready = 1, /**< \brief Voltage is OK */
}IfxTlf35584_Tracker1VoltageReadyStatus;

/** \brief Tracker2 voltage ready status
 * Bitfield(s): IfxTlf35584_VMONSTAT.TRK2ST
 */
typedef enum
{
    IfxTlf35584_Tracker2VoltageReadyStatus_notReady = 0,/**< \brief Voltage is out of range or not enabled */
    IfxTlf35584_Tracker2VoltageReadyStatus_ready = 1, /**< \brief Voltage is OK */
}IfxTlf35584_Tracker2VoltageReadyStatus;

/** \brief Device state
 * Bitfield(s): IfxTlf35584_DEVSTAT.STATE
 */
typedef enum
{
    IfxTlf35584_DeviceState_none = 0,                 /**< \brief NONE */
    IfxTlf35584_DeviceState_init = 1,                 /**< \brief INIT */
    IfxTlf35584_DeviceState_normal = 2,               /**< \brief NORMAL */
    IfxTlf35584_DeviceState_sleep = 3,                /**< \brief SLEEP */
    IfxTlf35584_DeviceState_standby = 4,              /**< \brief STANDBY */
    IfxTlf35584_DeviceState_wake = 5,                 /**< \brief WAKE */
}IfxTlf35584_DeviceState;

/** \brief Reference voltage enable status
 * Bitfield(s): IfxTlf35584_DEVSTAT.VREFEN
 */
typedef enum
{
    IfxTlf35584_ReferenceVoltageEnableStatus_disabled = 0,/**< \brief Voltage is disabled */
    IfxTlf35584_ReferenceVoltageEnableStatus_enabled = 1,/**< \brief Voltage is enabled */
}IfxTlf35584_ReferenceVoltageEnableStatus;

/** \brief Standby LDO enable status
 * Bitfield(s): IfxTlf35584_DEVSTAT.STBYEN
 */
typedef enum
{
    IfxTlf35584_StandbyLdoEnableStatus_disabled = 0,  /**< \brief Voltage is disabled */
    IfxTlf35584_StandbyLdoEnableStatus_enabled = 1,   /**< \brief Voltage is enabled */
}IfxTlf35584_StandbyLdoEnableStatus;

/** \brief Communication LDO enable status
 * Bitfield(s): IfxTlf35584_DEVSTAT.COMEN
 */
typedef enum
{
    IfxTlf35584_CommunicationLdoEnableStatus_disabled = 0,/**< \brief Voltage is disabled */
    IfxTlf35584_CommunicationLdoEnableStatus_enabled = 1,/**< \brief Voltage is enabled */
}IfxTlf35584_CommunicationLdoEnableStatus;

/** \brief Tracker1 voltage enable status
 * Bitfield(s): IfxTlf35584_DEVSTAT.TRK1EN
 */
typedef enum
{
    IfxTlf35584_Tracker1VoltageEnableStatus_disabled = 0,/**< \brief Voltage is disabled */
    IfxTlf35584_Tracker1VoltageEnableStatus_enabled = 1,/**< \brief Voltage is enabled */
}IfxTlf35584_Tracker1VoltageEnableStatus;

/** \brief Tracker2 voltage enable status
 * Bitfield(s): IfxTlf35584_DEVSTAT.TRK2EN
 */
typedef enum
{
    IfxTlf35584_Tracker2VoltageEnableStatus_disabled = 0,/**< \brief Voltage is disabled */
    IfxTlf35584_Tracker2VoltageEnableStatus_enabled = 1,/**< \brief Voltage is enabled */
}IfxTlf35584_Tracker2VoltageEnableStatus;

/** \brief Protected register lock status
 * Bitfield(s): IfxTlf35584_PROTSTAT.LOCK
 */
typedef enum
{
    IfxTlf35584_ProtectedRegisterLockStatus_unlocked = 0,/**< \brief Access is unlocked */
    IfxTlf35584_ProtectedRegisterLockStatus_locked = 1,/**< \brief Access is locked */
}IfxTlf35584_ProtectedRegisterLockStatus;

/** \brief Key1 ok status
 * Bitfield(s): IfxTlf35584_PROTSTAT.KEY1OK
 */
typedef enum
{
    IfxTlf35584_Key1OkStatus_locked = 0,              /**< \brief Key not valid */
    IfxTlf35584_Key1OkStatus_unlocked = 1,            /**< \brief Key valid */
}IfxTlf35584_Key1OkStatus;

/** \brief Key2 ok status
 * Bitfield(s): IfxTlf35584_PROTSTAT.KEY2OK
 */
typedef enum
{
    IfxTlf35584_Key2OkStatus_locked = 0,              /**< \brief Key not valid */
    IfxTlf35584_Key2OkStatus_unlocked = 1,            /**< \brief Key valid */
}IfxTlf35584_Key2OkStatus;

/** \brief Key3 ok status
 * Bitfield(s): IfxTlf35584_PROTSTAT.KEY3OK
 */
typedef enum
{
    IfxTlf35584_Key3OkStatus_locked = 0,              /**< \brief Key not valid */
    IfxTlf35584_Key3OkStatus_unlocked = 1,            /**< \brief Key valid */
}IfxTlf35584_Key3OkStatus;

/** \brief Key4 ok status
 * Bitfield(s): IfxTlf35584_PROTSTAT.KEY4OK
 */
typedef enum
{
    IfxTlf35584_Key4OkStatus_locked = 0,              /**< \brief Key not valid */
    IfxTlf35584_Key4OkStatus_unlocked = 1,            /**< \brief Key valid */
}IfxTlf35584_Key4OkStatus;

/** \brief Functional watchdog response check error status
 * Bitfield(s): IfxTlf35584_FWDSTAT0.FWDRSPOK
 */
typedef enum
{
    IfxTlf35584_FunctionalWatchdogResponseCheckErrorStatus_error = 0,/**< \brief Response message is wrong */
    IfxTlf35584_FunctionalWatchdogResponseCheckErrorStatus_noError = 1,/**< \brief All received bytes in response message are correct */
}IfxTlf35584_FunctionalWatchdogResponseCheckErrorStatus;

/** \brief Start ABIST operation
 * Bitfield(s): IfxTlf35584_ABIST_CTRL0.START
 */
typedef enum
{
    IfxTlf35584_StartAbistOperation_noAction = 0,     /**< \brief Operation done */
    IfxTlf35584_StartAbistOperation_start = 1,        /**< \brief Start operation */
}IfxTlf35584_StartAbistOperation;

/** \brief Full path test selection
 * Bitfield(s): IfxTlf35584_ABIST_CTRL0.PATH
 */
typedef enum
{
    IfxTlf35584_FullPathTestSelection_comparatorOnly = 0,/**< \brief Comparator only */
    IfxTlf35584_FullPathTestSelection_fullPath = 1,   /**< \brief Comparator and corresponding deglitching logic, shall be selected in case contribution to respective safety measure needs to be tested */
}IfxTlf35584_FullPathTestSelection;

/** \brief ABIST Sequence selection
 * Bitfield(s): IfxTlf35584_ABIST_CTRL0.SINGLE
 */
typedef enum
{
    IfxTlf35584_AbistSequenceSelection_sequence = 0,  /**< \brief Predefined sequence */
    IfxTlf35584_AbistSequenceSelection_single = 1,    /**< \brief Single comparator test */
}IfxTlf35584_AbistSequenceSelection;

/** \brief Safety path selection
 * Bitfield(s): IfxTlf35584_ABIST_CTRL0.INT
 */
typedef enum
{
    IfxTlf35584_SafetyPathSelection_safeStateRelated = 0,/**< \brief safe state related comparators shall be tested */
    IfxTlf35584_SafetyPathSelection_intRelated = 1,   /**< \brief interrupt related comparators shall be tested */
}IfxTlf35584_SafetyPathSelection;

/** \brief ABIST global error status
 * Bitfield(s): IfxTlf35584_ABIST_CTRL0.STATUS
 */
typedef enum
{
    IfxTlf35584_AbistGlobalErrorStatus_noError = 5,   /**< \brief Selected ABIST operation performed with no errors */
    IfxTlf35584_AbistGlobalErrorStatus_error = 10,    /**< \brief Selected ABIST operation performed with errors, check respective SELECT registers */
}IfxTlf35584_AbistGlobalErrorStatus;

/** \brief Overvoltage trigger for secondary internal monitor enable
 * Bitfield(s): IfxTlf35584_ABIST_CTRL1.OV_TRIG
 */
typedef enum
{
    IfxTlf35584_OvervoltageTriggerForSecondaryInternalMonitorEnable_disabled = 0,/**< \brief Disable */
    IfxTlf35584_OvervoltageTriggerForSecondaryInternalMonitorEnable_enabled = 1,/**< \brief Enable */
}IfxTlf35584_OvervoltageTriggerForSecondaryInternalMonitorEnable;

/** \brief ABIST clock check enable
 * Bitfield(s): IfxTlf35584_ABIST_CTRL1.ABIST_CLK_EN
 */
typedef enum
{
    IfxTlf35584_AbistClockCheckEnable_disabled = 0,   /**< \brief Disable */
    IfxTlf35584_AbistClockCheckEnable_enabled = 1,    /**< \brief Enable */
}IfxTlf35584_AbistClockCheckEnable;

/** \brief Select Pre-regulator OV comparator for ABIST operation enable
 * Bitfield(s): IfxTlf35584_ABIST_SELECT0.PREGOV
 */
typedef enum
{
    IfxTlf35584_SelectPreRegulatorOvComparatorForAbistOperationEnable_disabled = 0,/**< \brief Not selected */
    IfxTlf35584_SelectPreRegulatorOvComparatorForAbistOperationEnable_enabled = 1,/**< \brief Selected, bit will be cleared upon successful ABIST operation on comparator, bit will be set in case of ABIST fail for this comparator */
}IfxTlf35584_SelectPreRegulatorOvComparatorForAbistOperationEnable;

/** \brief Select uC LDO OV comparator for ABIST operation enable
 * Bitfield(s): IfxTlf35584_ABIST_SELECT0.UCOV
 */
typedef enum
{
    IfxTlf35584_SelectUcLdoOvComparatorForAbistOperationEnable_disabled = 0,/**< \brief Not selected */
    IfxTlf35584_SelectUcLdoOvComparatorForAbistOperationEnable_enabled = 1,/**< \brief Selected, bit will be cleared upon successful ABIST operation on comparator, bit will be set in case of ABIST fail for this comparator */
}IfxTlf35584_SelectUcLdoOvComparatorForAbistOperationEnable;

/** \brief Select Standby LDO OV comparator for ABIST operation enable
 * Bitfield(s): IfxTlf35584_ABIST_SELECT0.STBYOV
 */
typedef enum
{
    IfxTlf35584_SelectStandbyLdoOvComparatorForAbistOperationEnable_disabled = 0,/**< \brief Not selected */
    IfxTlf35584_SelectStandbyLdoOvComparatorForAbistOperationEnable_enabled = 1,/**< \brief Selected, bit will be cleared upon successful ABIST operation on comparator, bit will be set in case of ABIST fail for this comparator */
}IfxTlf35584_SelectStandbyLdoOvComparatorForAbistOperationEnable;

/** \brief Select Core voltage OV comparator for ABIST operation enable
 * Bitfield(s): IfxTlf35584_ABIST_SELECT0.VCOREOV
 */
typedef enum
{
    IfxTlf35584_SelectCoreVoltageOvComparatorForAbistOperationEnable_disabled = 0,/**< \brief Not selected */
    IfxTlf35584_SelectCoreVoltageOvComparatorForAbistOperationEnable_enabled = 1,/**< \brief Selected, bit will be cleared upon successful ABIST operation on comparator, bit will be set in case of ABIST fail for this comparator */
}IfxTlf35584_SelectCoreVoltageOvComparatorForAbistOperationEnable;

/** \brief Select COM OV comparator for ABIST operation enable
 * Bitfield(s): IfxTlf35584_ABIST_SELECT0.COMOV
 */
typedef enum
{
    IfxTlf35584_SelectComOvComparatorForAbistOperationEnable_disabled = 0,/**< \brief Not selected */
    IfxTlf35584_SelectComOvComparatorForAbistOperationEnable_enabled = 1,/**< \brief Selected, bit will be cleared upon successful ABIST operation on comparator, bit will be set in case of ABIST fail for this comparator */
}IfxTlf35584_SelectComOvComparatorForAbistOperationEnable;

/** \brief Select VREF OV comparator for ABIST operation enable
 * Bitfield(s): IfxTlf35584_ABIST_SELECT0.VREFOV
 */
typedef enum
{
    IfxTlf35584_SelectVrefOvComparatorForAbistOperationEnable_disabled = 0,/**< \brief Not selected */
    IfxTlf35584_SelectVrefOvComparatorForAbistOperationEnable_enabled = 1,/**< \brief Selected, bit will be cleared upon successful ABIST operation on comparator, bit will be set in case of ABIST fail for this comparator */
}IfxTlf35584_SelectVrefOvComparatorForAbistOperationEnable;

/** \brief Select TRK1 OV comparator for ABIST operation enable
 * Bitfield(s): IfxTlf35584_ABIST_SELECT0.TRK1OV
 */
typedef enum
{
    IfxTlf35584_SelectTrk1OvComparatorForAbistOperationEnable_disabled = 0,/**< \brief Not selected */
    IfxTlf35584_SelectTrk1OvComparatorForAbistOperationEnable_enabled = 1,/**< \brief Selected, bit will be cleared upon successful ABIST operation on comparator, bit will be set in case of ABIST fail for this comparator */
}IfxTlf35584_SelectTrk1OvComparatorForAbistOperationEnable;

/** \brief Select TRK2 OV comparator for ABIST operation enable
 * Bitfield(s): IfxTlf35584_ABIST_SELECT0.TRK2OV
 */
typedef enum
{
    IfxTlf35584_SelectTrk2OvComparatorForAbistOperationEnable_disabled = 0,/**< \brief Not selected */
    IfxTlf35584_SelectTrk2OvComparatorForAbistOperationEnable_enabled = 1,/**< \brief Selected, bit will be cleared upon successful ABIST operation on comparator, bit will be set in case of ABIST fail for this comparator */
}IfxTlf35584_SelectTrk2OvComparatorForAbistOperationEnable;

/** \brief Select pre regulator UV comparator for ABIST operation enable
 * Bitfield(s): IfxTlf35584_ABIST_SELECT1.PREGUV
 */
typedef enum
{
    IfxTlf35584_SelectPreRegulatorUvComparatorForAbistOperationEnable_disabled = 0,/**< \brief Not selected */
    IfxTlf35584_SelectPreRegulatorUvComparatorForAbistOperationEnable_enabled = 1,/**< \brief Selected, bit will be cleared upon successful ABIST operation on comparator, bit will be set in case of ABIST fail for this comparator */
}IfxTlf35584_SelectPreRegulatorUvComparatorForAbistOperationEnable;

/** \brief Select uC UV comparator for ABIST operation enable
 * Bitfield(s): IfxTlf35584_ABIST_SELECT1.UCUV
 */
typedef enum
{
    IfxTlf35584_SelectUcUvComparatorForAbistOperationEnable_disabled = 0,/**< \brief Not selected */
    IfxTlf35584_SelectUcUvComparatorForAbistOperationEnable_enabled = 1,/**< \brief selected, bit will be cleared upon successful ABIST operation on comparator, bit will be set in case of ABIST fail for this comparator */
}IfxTlf35584_SelectUcUvComparatorForAbistOperationEnable;

/** \brief Select STBY UV comparator for ABIST operation enable
 * Bitfield(s): IfxTlf35584_ABIST_SELECT1.STBYUV
 */
typedef enum
{
    IfxTlf35584_SelectStbyUvComparatorForAbistOperationEnable_disabled = 0,/**< \brief Not selected */
    IfxTlf35584_SelectStbyUvComparatorForAbistOperationEnable_enabled = 1,/**< \brief selected, bit will be cleared upon successful ABIST operation on comparator, bit will be set in case of ABIST fail for this comparator */
}IfxTlf35584_SelectStbyUvComparatorForAbistOperationEnable;

/** \brief Select VCORE UV comparator for ABIST operation enable
 * Bitfield(s): IfxTlf35584_ABIST_SELECT1.VCOREUV
 */
typedef enum
{
    IfxTlf35584_SelectVcoreUvComparatorForAbistOperationEnable_disabled = 0,/**< \brief Not selected */
    IfxTlf35584_SelectVcoreUvComparatorForAbistOperationEnable_enabled = 1,/**< \brief selected, bit will be cleared upon successful ABIST operation on comparator, bit will be set in case of ABIST fail for this comparator */
}IfxTlf35584_SelectVcoreUvComparatorForAbistOperationEnable;

/** \brief Select COM UV comparator for ABIST operation enable
 * Bitfield(s): IfxTlf35584_ABIST_SELECT1.COMUV
 */
typedef enum
{
    IfxTlf35584_SelectComUvComparatorForAbistOperationEnable_disabled = 0,/**< \brief Not selected */
    IfxTlf35584_SelectComUvComparatorForAbistOperationEnable_enabled = 1,/**< \brief Selected, bit will be cleared upon successful ABIST operation on comparator, bit will be set in case of ABIST fail for this comparator */
}IfxTlf35584_SelectComUvComparatorForAbistOperationEnable;

/** \brief Select VREF UV comparator for ABIST operation enable
 * Bitfield(s): IfxTlf35584_ABIST_SELECT1.VREFUV
 */
typedef enum
{
    IfxTlf35584_SelectVrefUvComparatorForAbistOperationEnable_disabled = 0,/**< \brief Not selected */
    IfxTlf35584_SelectVrefUvComparatorForAbistOperationEnable_enabled = 1,/**< \brief Selected, bit will be cleared upon successful ABIST operation on comparator, bit will be set in case of ABIST fail for this comparator */
}IfxTlf35584_SelectVrefUvComparatorForAbistOperationEnable;

/** \brief Select TRK1 UV comparator for ABIST operation enable
 * Bitfield(s): IfxTlf35584_ABIST_SELECT1.TRK1UV
 */
typedef enum
{
    IfxTlf35584_SelectTrk1UvComparatorForAbistOperationEnable_disabled = 0,/**< \brief Not selected */
    IfxTlf35584_SelectTrk1UvComparatorForAbistOperationEnable_enabled = 1,/**< \brief Selected, bit will be cleared upon successful ABIST operation on comparator, bit will be set in case of ABIST fail for this voltage */
}IfxTlf35584_SelectTrk1UvComparatorForAbistOperationEnable;

/** \brief Select TRK2 UV comparator for ABIST operation enable
 * Bitfield(s): IfxTlf35584_ABIST_SELECT1.TRK2UV
 */
typedef enum
{
    IfxTlf35584_SelectTrk2UvComparatorForAbistOperationEnable_disabled = 0,/**< \brief Not selected */
    IfxTlf35584_SelectTrk2UvComparatorForAbistOperationEnable_enabled = 1,/**< \brief Selected, bit will be cleared upon successful ABIST operation on comparator, bit will be set in case of ABIST fail for this comparator */
}IfxTlf35584_SelectTrk2UvComparatorForAbistOperationEnable;

/** \brief Select supply VS12 overvoltage enable
 * Bitfield(s): IfxTlf35584_ABIST_SELECT2.VBATOV
 */
typedef enum
{
    IfxTlf35584_SelectSupplyVs12OvervoltageEnable_disabled = 0,/**< \brief Not selected */
    IfxTlf35584_SelectSupplyVs12OvervoltageEnable_enabled = 1,/**< \brief Selected, bit will be cleared upon successful ABIST operation on comparator, bit will be set in case of ABIST fail for this comparator */
}IfxTlf35584_SelectSupplyVs12OvervoltageEnable;

/** \brief Select internal supply OV condition enable
 * Bitfield(s): IfxTlf35584_ABIST_SELECT2.INTOV
 */
typedef enum
{
    IfxTlf35584_SelectInternalSupplyOvConditionEnable_disabled = 0,/**< \brief Not selected */
    IfxTlf35584_SelectInternalSupplyOvConditionEnable_enabled = 1,/**< \brief Selected, bit will be cleared upon successful ABIST operation on comparator, bit will be set in case of ABIST fail for this comparator */
}IfxTlf35584_SelectInternalSupplyOvConditionEnable;

/** \brief Select bandgap comparator UV condition enable
 * Bitfield(s): IfxTlf35584_ABIST_SELECT2.BG12UV
 */
typedef enum
{
    IfxTlf35584_SelectBandgapComparatorUvConditionEnable_disabled = 0,/**< \brief Not selected */
    IfxTlf35584_SelectBandgapComparatorUvConditionEnable_enabled = 1,/**< \brief Selected, bit will be cleared upon successful ABIST operation on comparator, bit will be set in case of ABIST fail for this comparator */
}IfxTlf35584_SelectBandgapComparatorUvConditionEnable;

/** \brief Select bandgap comparator OV condition enable
 * Bitfield(s): IfxTlf35584_ABIST_SELECT2.BG12OV
 */
typedef enum
{
    IfxTlf35584_SelectBandgapComparatorOvConditionEnable_disabled = 0,/**< \brief Not selected */
    IfxTlf35584_SelectBandgapComparatorOvConditionEnable_enabled = 1,/**< \brief Selected, bit will be cleared upon successful ABIST operation on comparator, bit will be set in case of ABIST fail for this comparator */
}IfxTlf35584_SelectBandgapComparatorOvConditionEnable;

/** \brief Select bias current too low enable
 * Bitfield(s): IfxTlf35584_ABIST_SELECT2.BIASLOW
 */
typedef enum
{
    IfxTlf35584_SelectBiasCurrentTooLowEnable_disabled = 0,/**< \brief Not selected */
    IfxTlf35584_SelectBiasCurrentTooLowEnable_enabled = 1,/**< \brief Selected, bit will be cleared upon successful ABIST operation on comparator, bit will be set in case of ABIST fail for this comparator */
}IfxTlf35584_SelectBiasCurrentTooLowEnable;

/** \brief Select bias current too high enable
 * Bitfield(s): IfxTlf35584_ABIST_SELECT2.BIASHI
 */
typedef enum
{
    IfxTlf35584_SelectBiasCurrentTooHighEnable_disabled = 0,/**< \brief Not selected */
    IfxTlf35584_SelectBiasCurrentTooHighEnable_enabled = 1,/**< \brief Selected, bit will be cleared upon successful ABIST operation on comparator, bit will be set in case of ABIST fail for this comparator */
}IfxTlf35584_SelectBiasCurrentTooHighEnable;

/** \brief BUCK switching frequency change
 * Bitfield(s): IfxTlf35584_BCK_FREQ_CHANGE.BCK_FREQ_SEL
 */
typedef enum
{
    IfxTlf35584_BuckSwitchingFrequencyChange_noChange = 0,/**< \brief No Change */
    IfxTlf35584_BuckSwitchingFrequencyChange_plusOneDotFivePercent = 1,/**< \brief Change buck frequency by approx. +1.5% from   (LF:+1%;HF:+1.49%) */
    IfxTlf35584_BuckSwitchingFrequencyChange_plusThreePercent = 2,/**< \brief Change buck frequency by approx. +3.0% from   (LF:+3%;HF:+2.93%) */
    IfxTlf35584_BuckSwitchingFrequencyChange_plusFourDotFivePercent = 3,/**< \brief Change buck frequency by approx. +4.5% from   (LF:+5%;HF:+4.46%) */
    IfxTlf35584_BuckSwitchingFrequencyChange_minusOneDotFivePercent = 5,/**< \brief Change buck frequency by approx. -1.5% from   (LF:-1%;HF:-2.41%) */
    IfxTlf35584_BuckSwitchingFrequencyChange_minusThreePercent = 6,/**< \brief Change buck frequency by approx. -3.0% from   (LF:-3%;HF:-3.92%) */
    IfxTlf35584_BuckSwitchingFrequencyChange_minusFourDotFivePercent = 7,/**< \brief Change buck frequency by approx. -4.5% from   (LF:-4.75%;HF:-5.18%) */
}IfxTlf35584_BuckSwitchingFrequencyChange;

/** \brief Spread spectrum down spread
 * Bitfield(s): IfxTlf35584_BCK_FRE_SPREAD.FRE_SP_THR
 */
typedef enum
{
    IfxTlf35584_SpreadSpectrumDownSpread_noSpread = 0,/**< \brief No spread */
    IfxTlf35584_SpreadSpectrumDownSpread_1percent = 43,/**< \brief -1% */
    IfxTlf35584_SpreadSpectrumDownSpread_2percent = 85,/**< \brief -2% */
    IfxTlf35584_SpreadSpectrumDownSpread_3percent = 128,/**< \brief -3% */
    IfxTlf35584_SpreadSpectrumDownSpread_4percent = 170,/**< \brief -4% */
    IfxTlf35584_SpreadSpectrumDownSpread_5percent = 213,/**< \brief -5% */
    IfxTlf35584_SpreadSpectrumDownSpread_6percent = 255,/**< \brief -6% */
}IfxTlf35584_SpreadSpectrumDownSpread;

/** \brief Enable buck update
 * Bitfield(s): IfxTlf35584_BCK_MAIN_CTRL.DATA_VALID
 */
typedef enum
{
    IfxTlf35584_EnableBuckUpdate_disable = 0,         /**< \brief No action */
    IfxTlf35584_EnableBuckUpdate_enable = 1,          /**< \brief Load new parameters */
}IfxTlf35584_EnableBuckUpdate;

/** \brief DATA_VALID parameter update ready status
 * Bitfield(s): IfxTlf35584_BCK_MAIN_CTRL.BUSY
 */
typedef enum
{
    IfxTlf35584_DatavalidParameterUpdateReadyStatus_ready = 0,/**< \brief update done */
    IfxTlf35584_DatavalidParameterUpdateReadyStatus_notReady = 1,/**< \brief update ongoing */
}IfxTlf35584_DatavalidParameterUpdateReadyStatus;

/** \brief Test mode enable status
 * Bitfield(s): IfxTlf35584_GTM.TM
 */
typedef enum
{
    IfxTlf35584_TestModeEnableStatus_disabled = 0,    /**< \brief Device is in normal mode */
    IfxTlf35584_TestModeEnableStatus_enabled = 1,     /**< \brief Device is in test mode */
}IfxTlf35584_TestModeEnableStatus;

/** \brief Test mode inverted enable status
 * Bitfield(s): IfxTlf35584_GTM.NTM
 */
typedef enum
{
    IfxTlf35584_TestModeInvertedEnableStatus_enabled = 0,/**< \brief Device is in test mode */
    IfxTlf35584_TestModeInvertedEnableStatus_disabled = 1,/**< \brief Device is in normal mode */
}IfxTlf35584_TestModeInvertedEnableStatus;

/** \}  */
#endif /* IFXTLF35584_ENUM_H */
