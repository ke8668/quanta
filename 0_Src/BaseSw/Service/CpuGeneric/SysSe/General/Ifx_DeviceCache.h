/**
 * \file Ifx_DeviceCache.h
 * \brief Device cache
 *
 * \version disabled
 * \copyright Copyright (c) 2013 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * \defgroup library_srvsw_sysse_general_devicecache Device register cache
 * This module implements External device register caching
 * \ingroup library_srvsw_sysse_general
 *
 *
 * \b WRANING: The cache functionality is experimental and should be used with caution.
 *
 * This library enables caching of register from an external device(s) to local microcontroller RAM.
 * It can be used for single device or devices in daisy chain.
 *
 * The following wording is used:
 * - cache: RAM memory storing the device register values. The cache is used for local, temporary operations
 * - cache line: in case of daisy chained devices, a cache line represent all device registers with identical address.
 *   The 1st item in the cache line represent the 1st device in the daisy chain, the 2nd item the 2nd device, and so on.
 *
 *  Register type                | Description                                                | cache handling
 * ------------------------------|------------------------------------------------------------|--------------------------------------
 * <b>Cachable register</b>      | register content not modified by the device                | Register is cached for read / write access
 * <b>Non cachable register</b>  | register content can be modified by the device             | Register is not cached for read / write access
 * <b>Action write register</b>  | Register that when written trigger an action on the device | Similar to <b>Non cachable registers</b>. Additional cache write back before register write access
 * <b>Action read register</b>   | Register that when read trigger an action on the device    | Similar to <b>Non cachable registers</b>. Additionally register is not read when a full cache read synchronization is done
 *
 * \note <b>Action write register</b> and <b>Action read register</b> must be flagged as <b>Non cachable register</b>.
 *
 * The cache handles the register types as follow:
 * - <b>Cachable registers</b> as for example configuration register are read directly from the device
 *   on the 1st read access
 *   using \ref Ifx_DeviceCache_getValue8() or \ref Ifx_DeviceCache_getValue16(), and stored locally to the cache memory. On the
 *   subsequent reads, the register value is taken from the cache memory instead. In case the value is
 *   modified using \ref Ifx_DeviceCache_setValue8() or \ref Ifx_DeviceCache_setValue16(), it is 1st locally stored, and written to the device by
 *   a cache write synchronization operation (\ref Ifx_DeviceCache_writeBack8() or \ref Ifx_DeviceCache_writeBack16())
 * - <b>Non cachable registers</b> as for example status registers are by default not cached, and
 *    the register value is directly taken form the device. Writing to such register using
 *   \ref Ifx_DeviceCache_setValue8() or \ref Ifx_DeviceCache_setValue16() will write the device directly. Caching of such registers can be
 *   enabled by
 *   \ref Ifx_DeviceCache_setVolatileCache(). This is useful for device in daisy chain, where anyway all
 *   device are accessed at a time. In this case, \ref Ifx_DeviceCache_invalidateVolatile() must be called
 *   to ensure device read / write. Note that \ref Ifx_DeviceCache_invalidate() also invalidate the volatile registers.
 * - <b>Action write register</b> trigger a cache write synchronization when accessed for write to ensure the sequence. In case the volatile cache is enabled, not action is done on write. Example of use for action write registers: TLF35584 FWDRSP and FWDRSPSYNC registers
 * - <b>Action read register</b> are not read during cache update to avoid side effect
 *
 * The cache implements API version for 8 bits and 16 bits register width.
 *
 * The cache can be invalidated at two levels:
 * - entire cache: \ref Ifx_DeviceCache_invalidate()
 * - cache of volatile registers only: \ref Ifx_DeviceCache_invalidateVolatile()
 *
 *
 * The full cache line is updated when:
 * - reading one of the daisy chain device register FIXME [New feature] add configuration switch for this feature
 *
 * \note the cache granularity is register wise, not bitfied wise.
 *
 * Feature request:
 *  - Enable multiple register access by using pipeline implementation. Use case readSync() / writeSync
 *
 */
#ifndef IFX_DEVICECACHE_H_
#define IFX_DEVICECACHE_H_
#include "Cpu/Std/IfxCpu_Intrinsics.h" /* Do not remove this include */

#define IFX_CFG_DEVICECACHE_ADDRESS_WIDTH_8  (8)
#define IFX_CFG_DEVICECACHE_ADDRESS_WIDTH_16 (16)
#define IFX_CFG_DEVICECACHE_ADDRESS_WIDTH_32 (32)

/* FIXME [New feature] if IFX_CFG_DEVICECACHE_ADDRESS_WIDTH is too small, the cache will endless loop in  for (address = 0; address < cache->ItemCount; address++), add ASSERT() to check the case */
#ifndef IFX_CFG_DEVICECACHE_ADDRESS_WIDTH
#define IFX_CFG_DEVICECACHE_ADDRESS_WIDTH    (IFX_CFG_DEVICECACHE_ADDRESS_WIDTH_16) /**< \brief Defines the data type used for the address parameter. The cache must enable to address all registers + 1. For example if a device defines registers from address 0 to 255, then the data type must be able to hold the value (255+1)=256 so 16 bits data type has to be selected */
#endif

#if IFX_CFG_DEVICECACHE_ADDRESS_WIDTH == IFX_CFG_DEVICECACHE_ADDRESS_WIDTH_8
typedef uint8  Ifx_DeviceCache_Address; /**< 8 bit address range */
#elif IFX_CFG_DEVICECACHE_ADDRESS_WIDTH == IFX_CFG_DEVICECACHE_ADDRESS_WIDTH_16
typedef uint16 Ifx_DeviceCache_Address; /**< 16 bit address range */
#elif IFX_CFG_DEVICECACHE_ADDRESS_WIDTH == IFX_CFG_DEVICECACHE_ADDRESS_WIDTH_32
typedef uint32 Ifx_DeviceCache_Address; /**< 32 bit address range */
#endif

/** Generic device driver type
 */
typedef void *Ifx_DeviceCache_DeviceDriver;

typedef struct Ifx_DeviceCache_ Ifx_DeviceCache;

/** API prototype: Write to device
 * \param driver Driver used to communicate with the device
 * \param mask For devices in daisy chain, this parameter is used to identify which devices in the chain should be written.
 *             The 1st device corresponding to bit 0, 2nd to bit 1, ... .
 *             If not used this parameter should be set to 1.
 * \param address register address
 * \param data Data(s) to be written to the device.
 *             In case of single device, this points to a single data item.
 *             In case of daisy chain, this points to an array of item with size equal to the daisy chain length. The 1st item corresponding to the 1st device in the chain.
 */
typedef boolean (*Ifx_DeviceCache_WriteToDevice)(Ifx_DeviceCache_DeviceDriver driver, uint32 mask, Ifx_DeviceCache_Address address, void *data);

/** API prototype: Read from device
 * \param driver Driver used to communicate with the device
 * \param address register address
 * \param data Buffer to store the data(s) read from the device.
 *             In case of single device, this points to a single data item.
 *             In case of daisy chain, this points to an array of item with size equal to the daisy chain length. The 1st item corresponding to the 1st device in the chain.
 */
typedef boolean (*Ifx_DeviceCache_ReadFromDevice)(Ifx_DeviceCache_DeviceDriver driver, Ifx_DeviceCache_Address address, void *data);

/** API prototype: Write back to device
 * \param driver Driver used to communicate with the device
 */
typedef boolean (*Ifx_DeviceCache_WriteBack)(Ifx_DeviceCache *cache);

/** Device register with definition
 *
 */
typedef enum
{
    Ifx_DeviceCache_RegisterWidth_8bit  = 1, /**< 8 bits registers */
    Ifx_DeviceCache_RegisterWidth_16bit = 2, /**< 16 bits registers */
    Ifx_DeviceCache_RegisterWidth_32bit = 4, /**< 32 bits registers */
}Ifx_DeviceCache_RegisterWidth;


/** Device cache object
 *
 */
struct Ifx_DeviceCache_
{
    /* generic cache part */
    void                 *buffer;               /* Pointer to the device cache */
    IFX_CONST void                *bufferNoOpMaskDefault;/**< \brief Pointer to the device no operation mask (Default mask). This buffer is not modified. Buffer size must be equal to the  ItemCount * registerWidth */
    void                          *bufferNoOpMaskActual; /**< \brief Pointer to the device no operation mask (actual mask). This buffer is modified. Buffer size must be equal to the  ItemCount * registerWidth */
    IFX_CONST void                *bufferNoOpValue;      /**< \brief Pointer to the device no operation value. This buffer is not modified. Buffer size must be equal to the  ItemCount * registerWidth */
    uint16                         ItemCount;            /* Memory map size divided by the memory map item size (Number of registers including reserved area)*/
    uint32                        *modificationMask;     /**< \brief pointer to the register modification mask array. If bit with offset x is 1, corresponding register address x has been modified and need to be written to the device */
    IFX_CONST uint32              *validAddress;         /**< \brief pointer to the register valid address flag array. If bit with offset x is 1, corresponding register address x is a valid address */
    IFX_CONST uint32              *volatileRegisters;    /**< \brief pointer to the volatile register flag array. If bit with offset x is 1, corresponding register at address is a volatile register */
    IFX_CONST uint32              *actionReadRegisters;  /**< \brief pointer to the action read register flag array. If bit with offset x is 1, corresponding register at address is an action read register */
    IFX_CONST uint32              *actionWriteRegisters; /**< \brief pointer to the action write register flag array. If bit with offset x is 1, corresponding register at address is an action write register */
    uint32                        *valid;                /**< \brief pointer to the register cache valid flag array. If bit with offset x is 1, corresponding register cache x is valid for read access */
    uint32                        *writeBackErrorFlags;  /**< \brief pointer to the register write back error flag array. If bit with offset x is 1, corresponding register shows a write back error. Only valid if \ref Ifx_DeviceCache_isWriteBackError() returns true */
    uint32                         volatileEnabled;      /**< enable (0xFFFFFFFF) / disable(0, default) caching of volatile registers. When in daisy chain, only the line.first.volatileEnabled is used */
    uint32                         enabled;      	     /**< enable (0xFFFFFFFF) / disable(0, default) caching of non volatile registers. When in daisy chain, only the line.first.enabled is used */
    uint32                         writeBackError;       /**< error (0xFFFFFFFF) / no error(0, default) for the last write back operation. When in daisy chain, only the line.first.writeBackError is used */
    uint16                         flagArraySize;        /**< \brief  Size of the flag array (number of element in the validAddress buffer )*/

    Ifx_DeviceCache_WriteToDevice  write;                /**< \brief Register write call back function */
    Ifx_DeviceCache_WriteBack      writeBack;            /**< \brief Register write back call back function */
    Ifx_DeviceCache_ReadFromDevice read;                 /**< \brief Register read call back function */
    Ifx_DeviceCache_DeviceDriver   deviceDriver;         /**< \brief pointer to the device driver */
    Ifx_DeviceCache_RegisterWidth  registerWidth;        /**< \brief Register width in byte */
    /** Cache line object
     *
     */
    struct
    {
        Ifx_DeviceCache *first; /**< First cache of the cache line. */
        Ifx_DeviceCache *next;  /**< Next cache of the cache line */
        void            *temp;  /**< Temporary cache line buffer. It points to an array where the cache line is temporary stored. The size must be the size of the cache line */
        uint8            index; /**< \brief index of the cache in the cache line. First cache has index 0 */
    } line;
};

typedef struct
{
    void                          *buffer;               /**< \brief Pointer to the device cache. Buffer size must be equal to the  ItemCount * registerWidth */
    IFX_CONST void                *bufferNoOpMaskDefault;/**< \brief Pointer to the device no operation mask (Default mask). This buffer is not modified. Buffer size must be equal to the  ItemCount * registerWidth */
    void                          *bufferNoOpMaskActual; /**< \brief Pointer to the device no operation mask (actual mask). This buffer is modified. Buffer size must be equal to the  ItemCount * registerWidth */
    IFX_CONST void                *bufferNoOpValue;      /**< \brief Pointer to the device no operation value. This buffer is not modified. Buffer size must be equal to the  ItemCount * registerWidth */
    uint16                         ItemCount;            /**< \brief Memory map size divided by the memory map item size (Number of registers including reserved area)*/
    uint32                        *modificationMask;     /**< \brief pointer to the register modification mask array. If bit with offset x is 1, corresponding register address x has been modified and need to be written to the device */
    IFX_CONST uint32              *validAddress;         /**< \brief pointer to the register valid address flag array. If bit with offset x is 1, corresponding register address x is a valid address */
    IFX_CONST uint32              *volatileRegisters;    /**< \brief pointer to the volatile register flag array. If bit with offset x is 1, corresponding register at address is a volatile register */
    IFX_CONST uint32              *actionReadRegisters;  /**< \brief pointer to the action read register flag array. If bit with offset x is 1, corresponding register at address is an action read register */
    IFX_CONST uint32              *actionWriteRegisters; /**< \brief pointer to the action write register flag array. If bit with offset x is 1, corresponding register at address is an action write register */
    uint32                        *valid;                /**< \brief pointer to the register cache valid flag array. If bit with offset x is 1, corresponding register cache x is valid */
    uint32                        *writeBackErrorFlags;   /**< \brief pointer to the register write back error flag array. If bit with offset x is 1, corresponding register cache x is valid for read access */
    uint16                         flagArraySize;        /**  Size of the flag array (number of element in the validAddress buffer )*/

    Ifx_DeviceCache_WriteToDevice  write;                /**< \brief Register write call back function */
    Ifx_DeviceCache_WriteBack      writeBack;            /**< \brief Register write back call back function */
    Ifx_DeviceCache_ReadFromDevice read;                 /**< \brief Register read call back function */
    Ifx_DeviceCache_DeviceDriver   deviceDriver;         /**< \brief pointer to the device driver */
    Ifx_DeviceCache_RegisterWidth  registerWidth;        /**< \brief Register width in byte */
    Ifx_DeviceCache               *previousCache;        /**< Previous cache in the cache line. The first cache in the cache line must have be NULL. */
    void                          *tempCacheLine;        /**< Temporary cache line buffer. It points to an array where the cache line is temporary stored. The size must be the size of the cache line */
}Ifx_DeviceCache_Config;

/** \addtogroup library_srvsw_sysse_general_devicecache
 * \{ */

/** Return the cache enable status
 *
 * \param cache pointer to the cache driver
 *
 * \return TRUE if the cache is enabled else FALSE
 */
boolean Ifx_DeviceCache_isCacheEnabled(Ifx_DeviceCache *cache);

/** Enable the cache
 *
 * \image html "DeviceCache_enable().png"
 *
 * \param cache pointer to the cache driver
 *
 * \return None
 */
void Ifx_DeviceCache_enable(Ifx_DeviceCache *cache);

/** Disable the cache
 *
 * \image html "DeviceCache_disable().png"
 *
 * \param cache pointer to the cache driver
 *
 * \return TRUE if the cache was enabled else FALSE
 */

boolean Ifx_DeviceCache_disable(Ifx_DeviceCache *cache);

/** Return the value at the specified address (8 bit version)
 *
 * \image html "DeviceCache_getValue().png"
 *
 * \param cache pointer to the cache driver
 * \param address Register address
 * \param data returned value
 *
 * \return return TRUE in case of success else false
 */
boolean Ifx_DeviceCache_getValue8(Ifx_DeviceCache *cache, Ifx_DeviceCache_Address address, uint8 *data);

/** Return the value at the specified address (16 bit version)
 *
 * \image html "DeviceCache_getValue().png"
 *
 * \param cache pointer to the cache driver
 * \param address Register address
 * \param data returned value
 *
 * \return return TRUE in case of success else false
 */
boolean Ifx_DeviceCache_getValue16(Ifx_DeviceCache *cache, Ifx_DeviceCache_Address address, uint16 *data);

/** Initialize the cache driver
 *
 * \param cache pointer to the cache driver, will be initialized by the API
 * \param config cache configuration
 *
 * \return return TRUE in case of success else false
 */
boolean Ifx_DeviceCache_init(Ifx_DeviceCache *cache, Ifx_DeviceCache_Config *config);

/** Set the configuration to default
 *
 * \param config driver configuration, will be initialized by the API
 */
void Ifx_DeviceCache_initConfig(Ifx_DeviceCache_Config *config);

/** Invalidate the cache (volatile and non volatile registers)
 *
 * \image html "DeviceCache_Invalidate().png"
 *
 * When device are in daisy chain, all devices cache are invalidated.
 *
 * \param cache pointer to the cache driver
 */
void Ifx_DeviceCache_invalidate(Ifx_DeviceCache *cache);

/** Invalidate the cache (volatile registers only)
 *
 * \image html "DeviceCache_Invalidate().png"
 *
 * When device are in daisy chain, all devices cache are invalidated.
 *
 * \param cache pointer to the cache driver
 */
void Ifx_DeviceCache_invalidateVolatile(Ifx_DeviceCache *cache);

/** Return if an address is valid or not
 *
 * \param cache pointer to the cache driver
 * \param address Register address
 *
 * \return TRUE if the address is a valid address else FALSE
 */
IFX_INLINE boolean Ifx_DeviceCache_isAddressValid(Ifx_DeviceCache *cache, Ifx_DeviceCache_Address address)
{
    return (cache->validAddress[address >> 5] & (1 << (address % 32))) != 0;
}

/** Return true if a write back error occurred
 *
 * Return true if \ref Ifx_DeviceCache_writeBack8(), \ref Ifx_DeviceCache_writeBack16(),
 * \ref Ifx_DeviceCache_setValue8(), \ref Ifx_DeviceCache_setValue8Star(), \ref Ifx_DeviceCache_setValue16(),
 * \ref Ifx_DeviceCache_setValue16Star() had an error during write back operation.
 *
 * \return true if a write back error occurred
 *
 * FIXME add Ifx_DevcieCache_getWriteBackErrorAddress(Ifx_DeviceCache *cache, uint8 *deviceIndex, Ifx_DeviceCache_Address address)
 * that return the device and address for which the error occurred
 */
IFX_INLINE boolean Ifx_DeviceCache_isWriteBackError(Ifx_DeviceCache *cache)
{
	return cache->line.first->writeBackError != 0;
}

/** Return TRUE if the register can be cached
 *
 * \image html "DeviceCache_isCachable().png"
 *
 * \param cache pointer to the cache driver
 * \param address Register address
 * Return TRUE if the register can be cached
 */
boolean Ifx_DeviceCache_isCachable(Ifx_DeviceCache *cache, Ifx_DeviceCache_Address address);

/** return if the cached driver is initialized or not
 *
 * \param cache pointer to the cache driver
 *
 * \return returns TRUE if the cache is initialized else FALSE
 */
boolean Ifx_DeviceCache_isInitialized(Ifx_DeviceCache *cache);

/** return if the cached register is valid or not
 *
 * \image html "DeviceCache_isValid().png"
 *
 * \param cache pointer to the cache driver
 * \param address Register address
 *
 * \return return TRUE is the cached register is valid else FALSE
 */
boolean Ifx_DeviceCache_isValid(Ifx_DeviceCache *cache, Ifx_DeviceCache_Address address);

/** Write to the cache (8 bit version)
 *
 * \image html "DeviceCache_setValue().png"
 *
 * \param cache pointer to the cache driver
 * \param address Register address
 * \param data Register value
 *
 * \return return TRUE in case of success else false
 */
boolean Ifx_DeviceCache_setValue8(Ifx_DeviceCache *cache, Ifx_DeviceCache_Address address, uint8 data);

/** Write to the cache for registers with action write bitfield 't' flag set (8 bit version)
 *
 * \image html "DeviceCache_setValueStar().png"
 *
 * \param cache pointer to the cache driver
 * \param address Register address
 * \param data Register value
 * \param noOpMask mask of the modified bitfield, used to clear the noOperationFlag
 *
 * \return return TRUE in case of success else false
 */
boolean Ifx_DeviceCache_setValue8Star(Ifx_DeviceCache *cache, Ifx_DeviceCache_Address address, uint8 data, uint8 noOpMask);

/** Write to the cache (16 bit version)
 *
 * \image html "DeviceCache_setValue().png"
 *
 * \param cache pointer to the cache driver
 * \param address Register address
 * \param data Register value
 *
 * \return return TRUE in case of success else false
 */
boolean Ifx_DeviceCache_setValue16(Ifx_DeviceCache *cache, Ifx_DeviceCache_Address address, uint16 data);

/** Write to the cache for registers with action write bitfield 't' flag set (16 bit version)
 *
 * \image html "DeviceCache_setValueStar().png"
 *
 * \param cache pointer to the cache driver
 * \param address Register address
 * \param data Register value
 * \param noOpMask mask of the modified bitfield, used to clear the noOperationFlag
 *
 * \return return TRUE in case of success else false
 */
boolean Ifx_DeviceCache_setValue16Star(Ifx_DeviceCache *cache, Ifx_DeviceCache_Address address, uint16 data, uint16 noOpMask);

/** Enable / disable the cache of volatile registers
 *
 * \image html "DeviceCache_setVolatileCache(setEnabled).png"
 *
 * \param cache pointer to the cache driver
 * \param enabled Enable the cache of volatile registers if TRUE else disable the cache or volatile registers
 *
 * \return Return FALSE if an error occured during write back operation, else TRUE
 */
boolean Ifx_DeviceCache_setVolatileCache(Ifx_DeviceCache *cache, boolean enabled);

/** Synchronize the whole cache with the device (device register read, 8 bit version)
 *
 * \image html "DeviceCache_synchronize().png"
 *
 * \param cache pointer to the cache driver
 *
 * \return return TRUE in case of success else false
 */
boolean Ifx_DeviceCache_synchronize8(Ifx_DeviceCache *cache);

/** Synchronize the whole cache with the device (device register read, 16 bit version)
 *
 * \image html "DeviceCache_synchronize().png"
 *
 * \param cache pointer to the cache driver
 *
 * \return return TRUE in case of success else false
 */
boolean Ifx_DeviceCache_synchronize16(Ifx_DeviceCache *cache);

/** Read all registers into the cache buffer
 *
 * \image html "DeviceCache_readAll().png"
 *
 * \note this function will also access action read registers
 *
 * \param cache pointer to the cache driver
 *
 * \return return TRUE in case of success else false
 */
boolean Ifx_DeviceCache_readAll8(Ifx_DeviceCache *cache);

/** Read all registers into the cache buffer
 *
 * \image html "DeviceCache_readAll().png"
 *
 * \note this function will also access action read registers
 *
 * \param cache pointer to the cache driver
 *
 * \return return TRUE in case of success else false
 */
boolean Ifx_DeviceCache_readAll16(Ifx_DeviceCache *cache);

/** Cache write back (device register write, 8 bit version)
 *
 * \image html "DeviceCache_writeBack().png"
 *
 * \param cache pointer to the cache driver
 * \param address Register address
 *
 * \return return TRUE in case of success else false
 */
boolean Ifx_DeviceCache_writeBack8(Ifx_DeviceCache *cache);

/** Cache write back (device register write, 16 bit version)
 *
 * \image html "DeviceCache_writeBack().png"
 *
 * \param cache pointer to the cache driver
 *
 *
 * \return return TRUE in case of success else false
 */
boolean Ifx_DeviceCache_writeBack16(Ifx_DeviceCache *cache);

/** \} */
#endif /* IFX_DEVICECACHE_H_ */
