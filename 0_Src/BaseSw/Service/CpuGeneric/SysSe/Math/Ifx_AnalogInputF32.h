/**
 * \file Ifx_AnalogInputF32.h
 * \brief Analog input interface.
 *
 *
 *
 * \version disabled
 * \copyright Copyright (c) 2013 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * \defgroup library_srvsw_sysse_math_f32_analoginput Analog input interface
 *
 * \ingroup library_srvsw_sysse_math_f32
 */

#ifndef ANALOG_I_H
#define ANALOG_I_H

//----------------------------------------------------------------------------------------
#include "Cpu/Std/Ifx_Types.h"
#include "Ifx_LowPassPt1F32.h"
#include "Ifx_LutLinearF32.h"
//----------------------------------------------------------------------------------------
/** \addtogroup library_srvsw_sysse_math_f32_analoginput
 * \{ */

/**< \def ANALOG_I_LIMIT_CHECKING
 * \brief  1=SW limit checking is enabled (default), 0=disabled */
#ifndef ANALOG_I_LIMIT_CHECKING
#define ANALOG_I_LIMIT_CHECKING (1)
#endif

/**< \def ANALOG_I_FILTERING
 * \brief 1=SW filtering is enabled, 0=disabled */
#ifndef ANALOG_I_FILTERING
#define ANALOG_I_FILTERING      (1)
#endif

/**< \def ANALOG_I_COPY_RAW
 * \brief 1=Raw value is copied, 0=disabled */
#ifndef ANALOG_I_COPY_RAW
#define ANALOG_I_COPY_RAW       (1)
#endif
/** \} */
//----------------------------------------------------------------------------------------

/*
 * typedef struct
 * {
 *  <type> gain;
 *  <type> offset;
 * } StdiScale_template;
 * <type> StdiScale_template_execute(StdiScale_template *scale, <type> value);
 */

/** \brief Scaling object definition */
typedef struct
{
    float32 gain;                          /**< \brief channel gain */
    float32 offset;                        /**< \brief channel offset */
} Ifx_AnanogInput_Scale;

//----------------------------------------------------------------------------------------
/** \brief Limits status enumeration */
typedef enum
{
    Limits_Status_Ok,
    Limits_Status_Min,
    Limits_Status_Max
} Limits_Status;

/** \brief Callback function type definition when value is out of range
 * \param object Pointer to an object as specified by Ifx_AnalogInputF32_Params
 * \param status Status indicating reason why this function is called
 * \param info ?
 */
typedef void (*Limit_onOutOfRange)(void *object, Limits_Status status, sint32 info);

/** \brief Limits object definition */
typedef struct
{
    float32            lower;           /**< \brief lower limit */
    float32            upper;           /**< \brief upper limit */
    Limits_Status      status;          /**< \brief last limit checking status */
    Limit_onOutOfRange onOutOfRange;    /**< \brief call-back function when out of range*/
    void              *object;          /**< \brief pointer to an object passed into the call-back function */
    sint32             info;            /**< \brief ? */
} Ifx_AnanogInput_Limits;

/** \brief Process the out of range error. Needs to be implemented by application */
IFX_EXTERN void    Ifx_AnalogInputF32_onErrorDefault(void *object, Limits_Status status, sint32 info);
IFX_EXTERN float32 Limits_execute(Ifx_AnanogInput_Limits *limits, float32 value);

//----------------------------------------------------------------------------------------

/** \brief Analog input definition */
typedef struct
{
    float32            value;
#if ANALOG_I_FILTERING
    Ifx_LowPassPt1F32 filter;                  /**< \brief PT1 low-pass filter of this input */
    boolean            filterEnabled;           /**< \brief Specifies whether the filter is enabled */
#endif
#if ANALOG_I_LIMIT_CHECKING
    Ifx_AnanogInput_Limits limits;
#endif
#if ANALOG_I_COPY_RAW
    float32               raw;
#endif
    Ifx_AnanogInput_Scale scale;        /**< \biref: Scaling information. If lut is not null, this parameter is ignored */
    Ifx_LutLinearF32   *lut;          /**< \biref: Look up table information. If not null, this parameter is used instead of scale */

    pchar                 name;
} Ifx_AnalogInputF32;

/** \brief Analog input SW configuration */
typedef struct
{
    float32                   gain;             /**< \brief input gain. If lut is not null, this parameter is ignored */
    float32                   offset;           /**< \brief input offset. If lut is not null, this parameter is ignored */
    Ifx_LutLinearF32       *lut;              /**< \biref: Look up table information. If not null, this parameter is used instead of gain and offset */
    float32                   lower;            /**< \brief lower limit */
    float32                   upper;            /**< \brief upper limit */
    boolean                   filterEnabled;    /**< \brief specifies whether filter is enabled */
    Ifx_LowPassPt1F32_Config filterConfig;     /**< \brief PT1 filter configuration */
    Limit_onOutOfRange        onOutOfRange;     /**< \brief call-back function on out of range. See \ref Limit_onOutOfRange */
    void                     *object;           /**< \brief pointer to an object passed to the call-back function */
    sint32                    info;             /**< \brief info passed to the call back function */
} Ifx_AnalogInputF32_Params;

/** \brief Analog input configuration */
typedef struct
{
    pchar                      name;            /**<\brief String name of this input, for information to the Console */
    Ifx_AnalogInputF32_Params params;          /**<\brief Runtime parameters */
//    IfxVadc_Adc_Channel     hwInfo;
} Ifx_AnalogInputF32_Config;

/** \addtogroup library_srvsw_sysse_math_f32_analoginput
 * \{ */

//----------------------------------------------------------------------------------------
/** \name S/W functionality.
 * \{ */
/* FIXME add initConfig API*/
IFX_EXTERN void    Ifx_AnalogInputF32_init(Ifx_AnalogInputF32 *input, const Ifx_AnalogInputF32_Config *config);
IFX_EXTERN void    Ifx_AnalogInputF32_reset(Ifx_AnalogInputF32 *input);
IFX_EXTERN void    Ifx_AnalogInputF32_forceInput(Ifx_AnalogInputF32 *input, float32 raw);
IFX_EXTERN float32 Ifx_AnalogInputF32_update(Ifx_AnalogInputF32 *input, float32 raw);

/** \brief Get the actual input value in float32 */
IFX_INLINE float32 Ifx_AnalogInputF32_getValue(Ifx_AnalogInputF32 *input)
{
    return input->value;
}


/** \} */
//----------------------------------------------------------------------------------------
/** \name Scaling functions
 * \{ */

/** \brief Set the gain
 * only valid if lut in not NULL
 */
IFX_INLINE void Ifx_AnalogInputF32_setGain(Ifx_AnalogInputF32 *input, float32 gain, uint8 shift)
{
    input->scale.gain = gain;
}


/** \brief Set the offset
 * only valid if lut in not NULL
 */
IFX_INLINE void Ifx_AnalogInputF32_setOffset(Ifx_AnalogInputF32 *input, float32 offset)
{
    input->scale.offset = offset;
}


/** \brief Get the actual gain
 * only valid if lut in not NULL
 */
IFX_INLINE float32 Ifx_AnalogInputF32_getGain(Ifx_AnalogInputF32 *input)
{
    return input->scale.gain;
}


/** \brief Get the actual offset
 * only valid if lut in not NULL
 */
IFX_INLINE float32 Ifx_AnalogInputF32_getOffset(Ifx_AnalogInputF32 *input)
{
    return input->scale.offset;
}


/** \brief Get the input status
 */
IFX_INLINE Limits_Status Ifx_AnalogInputF32_getStatus(Ifx_AnalogInputF32 *input)
{
    return input->limits.status;
}


/** \brief Reset the input status to Limits_Status_Ok
 */
IFX_INLINE void Ifx_AnalogInputF32_resetStatus(Ifx_AnalogInputF32 *input)
{
    input->limits.status = Limits_Status_Ok;
}


/** \} */
//----------------------------------------------------------------------------------------

/** \} */

#endif
