/**
 * \file Ifx_VirtualPositionSensor.c
 * \brief Virtual position sensor
 *
 * MCMETILLD-521
 *
 * \version disabled
 * \copyright Copyright (c) 2013 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 */

/******************************************************************************/
/*----------------------------------Includes----------------------------------*/
/******************************************************************************/

#include "Ifx_VirtualPositionSensor.h"
#include "_Utilities/Ifx_Assert.h"
#include "string.h"

/******************************************************************************/
/*-----------------------Private Function Prototypes--------------------------*/
/******************************************************************************/

/******************************************************************************/
/*-------------------------Function Implementations---------------------------*/
/******************************************************************************/

float32 Ifx_VirtualPositionSensor_getAbsolutePosition(Ifx_VirtualPositionSensor *driver)
{
    return ((float32)driver->turn + (float32)driver->rawPosition / (float32)driver->resolution) * 2.0 * IFX_PI;
}


IfxStdIf_Pos_Dir Ifx_VirtualPositionSensor_getDirection(Ifx_VirtualPositionSensor *driver)
{
    return driver->direction;
}


IfxStdIf_Pos_Status Ifx_VirtualPositionSensor_getFault(Ifx_VirtualPositionSensor *driver)
{
    return driver->status;
}


sint32 Ifx_VirtualPositionSensor_getOffset(Ifx_VirtualPositionSensor *driver)
{
    return driver->offset;
}


uint16 Ifx_VirtualPositionSensor_getPeriodPerRotation(Ifx_VirtualPositionSensor *driver)
{
    return 1; /* Period per rotation is 1*/
}


float32 Ifx_VirtualPositionSensor_getPosition(Ifx_VirtualPositionSensor *driver)
{
    return (float32)driver->rawPosition * driver->positionConst;
}


sint32 Ifx_VirtualPositionSensor_getRawPosition(Ifx_VirtualPositionSensor *driver)
{
    return driver->rawPosition;
}


float32 Ifx_VirtualPositionSensor_getRefreshPeriod(Ifx_VirtualPositionSensor *driver)
{
    return driver->updatePeriod;
}


sint32 Ifx_VirtualPositionSensor_getTurn(Ifx_VirtualPositionSensor *driver)
{
    return driver->turn;
}


sint32 Ifx_VirtualPositionSensor_getResolution(Ifx_VirtualPositionSensor *driver)
{
    return driver->resolution;
}


IfxStdIf_Pos_SensorType Ifx_VirtualPositionSensor_getSensorType(Ifx_VirtualPositionSensor *driver)
{
    return IfxStdIf_Pos_SensorType_virtual;
}


float32 Ifx_VirtualPositionSensor_getSpeed(Ifx_VirtualPositionSensor *driver)
{
    return driver->freq.uk;
}


boolean Ifx_VirtualPositionSensor_init(Ifx_VirtualPositionSensor *driver, const Ifx_VirtualPositionSensor_Config *config)
{
    boolean status = TRUE;

    driver->offset        = config->base.offset;
    driver->resolution    = IFX_LUT_ANGLE_RESOLUTION;
    driver->positionConst = 1.0 / (float32)driver->resolution * 2.0 * IFX_PI;
    Ifx_VirtualPositionSensor_setRefreshPeriod(driver, config->base.updatePeriod);

    Ifx_VirtualPositionSensor_reset(driver);
    driver->minSpeed           = config->base.minSpeed;
    driver->maxSpeed           = config->base.maxSpeed;

    driver->speedFilterEnabled = config->base.speedFilterEnabled;

    if (config->base.speedFilterEnabled)
    {
        Ifx_LowPassPt1F32_Config lpfConfig;
        lpfConfig.gain            = 1.0;
        lpfConfig.cutOffFrequency = config->base.speedFilerCutOffFrequency;
        lpfConfig.samplingTime    = config->base.updatePeriod;
        Ifx_LowPassPt1F32_init(&driver->speedLpf, &lpfConfig);
    }

    Ifx_RampF32_init(&driver->freq, config->frequencyRate, config->base.updatePeriod);

    Ifx_AngleGenF32_init(&driver->angle, Ifx_AngleGenF32_SpeedUnit_rad_s, config->base.updatePeriod);
    Ifx_RampF32_reset(&driver->freq);

    return status;
}


void Ifx_VirtualPositionSensor_initConfig(Ifx_VirtualPositionSensor_Config *config)
{
    IfxStdIf_Pos_initConfig(&config->base);
    config->base.resolutionFactor          = IfxStdIf_Pos_ResolutionFactor_oneFold;
    config->base.resolution                = IFX_LUT_ANGLE_RESOLUTION;
    config->base.minSpeed                  = 1.0 / 60.0 * (2 * IFX_PI);     // 1 rpm
    config->base.maxSpeed                  = 20000.0 / 60.0 * (2 * IFX_PI); // 20000 rpm
    config->base.speedFilterEnabled        = TRUE;
    config->base.speedFilerCutOffFrequency = config->base.maxSpeed / 2 * IFX_PI * 10;
    config->frequencyRate                  = 2*IFX_PI;                             // 1 round/s
}


void Ifx_VirtualPositionSensor_reset(Ifx_VirtualPositionSensor *driver)
{
    driver->rawPosition   = 0;
    driver->turn          = 0;
    driver->speed         = 0;
    driver->status.status = 0;
    driver->direction     = IfxStdIf_Pos_Dir_unknown;
    Ifx_AngleGenF32_reset(&driver->angle);
    Ifx_RampF32_reset(&driver->freq);
}


void Ifx_VirtualPositionSensor_resetFaults(Ifx_VirtualPositionSensor *driver)
{
    IfxStdIf_Pos_Status status;
    status.status            = 0;
    status.B.notSynchronised = driver->status.B.notSynchronised;
    driver->status.status    = status.status;
}


void Ifx_VirtualPositionSensor_setOffset(Ifx_VirtualPositionSensor *driver, sint32 offset)
{
	while (offset <0)
	{ /* Make sure the offset is positive */
		offset += driver->resolution;
	}
    driver->rawPosition = driver->rawPosition - driver->offset + offset;
    driver->offset = offset;
}


void Ifx_VirtualPositionSensor_setPosition(Ifx_VirtualPositionSensor *driver, float32 position)
{
    Ifx_AngleGenF32_setValue(&driver->angle, position * IFX_LUT_ANGLE_RESOLUTION / (2 * IFX_PI));
}


void Ifx_VirtualPositionSensor_setRawPosition(Ifx_VirtualPositionSensor *driver, sint32 position)
{
    Ifx_AngleGenF32_setValue(&driver->angle, (float32)position);
}


void Ifx_VirtualPositionSensor_setSpeed(Ifx_VirtualPositionSensor *driver, float32 speed)
{
    Ifx_RampF32_setRef(&driver->freq, speed);
}


void Ifx_VirtualPositionSensor_setRefreshPeriod(Ifx_VirtualPositionSensor *driver, float32 updatePeriod)
{
    driver->updatePeriod = updatePeriod;
}


boolean Ifx_VirtualPositionSensor_stdIfPosInit(IfxStdIf_Pos *stdif, Ifx_VirtualPositionSensor *driver)
{
    /* Ensure the stdif is reset to zeros */
    memset(stdif, 0, sizeof(IfxStdIf_Pos));

    /* Set the driver */
    stdif->driver = driver;

    /* *INDENT-OFF* Note: this file was indented manually by the author. */
    /* Set the API link */
	stdif->onZeroIrq          =(IfxStdIf_Pos_OnZeroIrq               )NULL_PTR;
	stdif->getAbsolutePosition=(IfxStdIf_Pos_GetAbsolutePosition     )&Ifx_VirtualPositionSensor_getAbsolutePosition;
	stdif->getDirection		  =(IfxStdIf_Pos_GetDirection            )&Ifx_VirtualPositionSensor_getDirection;
	stdif->getFault           =(IfxStdIf_Pos_GetFault                )&Ifx_VirtualPositionSensor_getFault;
	stdif->getOffset		  =(IfxStdIf_Pos_GetOffset			     )&Ifx_VirtualPositionSensor_getOffset;
	stdif->getPeriodPerRotation  =(IfxStdIf_Pos_GetPeriodPerRotation )&Ifx_VirtualPositionSensor_getPeriodPerRotation;
	stdif->getPosition		  =(IfxStdIf_Pos_GetPosition			 )&Ifx_VirtualPositionSensor_getPosition;
	stdif->getRawPosition	  =(IfxStdIf_Pos_GetRawPosition         )&Ifx_VirtualPositionSensor_getRawPosition;
	stdif->getRefreshPeriod   =(IfxStdIf_Pos_GetRefreshPeriod        )&Ifx_VirtualPositionSensor_getRefreshPeriod;
	/*MCMETILLD-406 */
	stdif->getResolution      =(IfxStdIf_Pos_GetResolution           )&Ifx_VirtualPositionSensor_getResolution;
    stdif->getSensorType      =(IfxStdIf_Pos_GetSensorType           )&Ifx_VirtualPositionSensor_getSensorType;
	stdif->reset			  =(IfxStdIf_Pos_Reset				     )&Ifx_VirtualPositionSensor_reset;
	stdif->resetFaults		  =(IfxStdIf_Pos_ResetFaults			 )&Ifx_VirtualPositionSensor_resetFaults;
	stdif->getSpeed           =(IfxStdIf_Pos_GetSpeed                )&Ifx_VirtualPositionSensor_getSpeed;
	stdif->update			  =(IfxStdIf_Pos_Update				     )&Ifx_VirtualPositionSensor_update;
	stdif->setPosition		  =(IfxStdIf_Pos_SetPosition			 )&Ifx_VirtualPositionSensor_setPosition;
	stdif->setRawPosition	  =(IfxStdIf_Pos_SetRawPosition			 )&Ifx_VirtualPositionSensor_setRawPosition;
	stdif->setSpeed		      =(IfxStdIf_Pos_SetSpeed			     )&Ifx_VirtualPositionSensor_setSpeed;
	stdif->setOffset		  =(IfxStdIf_Pos_SetOffset			     )&Ifx_VirtualPositionSensor_setOffset;
	stdif->setRefreshPeriod   =(IfxStdIf_Pos_SetRefreshPeriod        )&Ifx_VirtualPositionSensor_setRefreshPeriod;
	stdif->getTurn            =(IfxStdIf_Pos_GetTurn                 )&Ifx_VirtualPositionSensor_getTurn;
    /* *INDENT-ON* */

    return TRUE;
}


void Ifx_VirtualPositionSensor_update(Ifx_VirtualPositionSensor *driver)
{
    float32 freq = Ifx_RampF32_step(&driver->freq);
    driver->direction   = freq < 0 ? IfxStdIf_Pos_Dir_backward : IfxStdIf_Pos_Dir_forward;
    Ifx_AngleGenF32_step(&driver->angle, freq);
    driver->rawPosition = (Ifx_Lut_FxpAngle)Ifx_AngleGenF32_getValue(&driver->angle);

    if (driver->angle.status == Ifx_AngleGenF32_Status_overflow)
    {
        driver->turn++;
    }
    else if (driver->angle.status == Ifx_AngleGenF32_Status_underflow)
    {
        driver->turn--;
    }
}
