/**
 * \file Ifx_VirtualPositionSensor.h
 * \brief Virtual position sensor
 *
 * MCMETILLD-521
 *
 * \version disabled
 * \copyright Copyright (c) 2013 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * \defgroup library_srvsw_sysse_virtualDevice_positionSensor Virtual position sensor
 * \ingroup library_srvsw_sysse_virtualDevice
 *
 * \defgroup library_srvsw_sysse_virtualDevice_positionSensor_Datastructures Data structures
 * \ingroup library_srvsw_sysse_virtualDevice_positionSensor
 * \defgroup library_srvsw_sysse_virtualDevice_positionSensor_Functions Functions
 * \ingroup library_srvsw_sysse_virtualDevice_positionSensor
 * \defgroup library_srvsw_sysse_virtualDevice_positionSensor_StdIf_Functions StdIf Functions
 * \ingroup library_srvsw_sysse_virtualDevice_positionSensor
 */

#ifndef IFX_VIRTUAL_POSITIONSENSOR_H
#define IFX_VIRTUAL_POSITIONSENSOR_H 1

/******************************************************************************/
/*----------------------------------Includes----------------------------------*/
/******************************************************************************/

#include "StdIf/IfxStdIf_Pos.h"
#include "SysSe/Math/Ifx_LowPassPt1F32.h"
#include "SysSe/Math/Ifx_AngleGenF32.h"
#include "SysSe/Math/Ifx_RampF32.h"

/******************************************************************************/
/*------------------------------Type Definitions------------------------------*/
/******************************************************************************/

typedef IfxStdIf_Pos_Update Ifx_VirtualPositionSensor_Update;

/******************************************************************************/
/*-----------------------------Data Structures--------------------------------*/
/******************************************************************************/

/** \addtogroup library_srvsw_sysse_virtualDevice_positionSensor_Datastructures
 * \{ */
/** \brief Incremental encoder object
 */
typedef struct
{
    sint32              rawPosition;                    /**< \brief raw position in ticks. \note: the value already contains the offset */
    float32             speed;                          /**< \brief mechanical speed in rad/s FIXME obsolete / unused? */
    sint32              turn;                           /**< \brief number of mechanical turns */
    IfxStdIf_Pos_Dir    direction;                      /**< \brief rotation direction */
    IfxStdIf_Pos_Status status;                         /**< \brief error code (0 = no error) */
    sint32              offset;                         /**< \brief raw position offset */
    sint32              resolution;                     /**< \brief resolution of this position sensor interface */
    float32             updatePeriod;                   /**< \brief update period in seconds */
    float32             positionConst;                  /**< \brief constant for calculating mechanical position (in rad) from raw position */
    float32             minSpeed;                       /**< \brief Absolute minimal allowed speed. below speed is recognized as 0rad/s */
    float32             maxSpeed;                       /**< \brief Absolute maximal allowed speed. Above speed is recognized as error */
    Ifx_LowPassPt1F32  speedLpf;                       /**< \brief Low pass filter object */
    boolean             speedFilterEnabled;             /**< \brief Enable / disable the speed low pass filter */
    Ifx_RampF32        freq;
    Ifx_AngleGenF32    angle;
} Ifx_VirtualPositionSensor;

/** \brief Configuration structure for Virtual position sensor
 */
typedef struct
{
    IfxStdIf_Pos_Config base;               /**< \brief Configuration data of \ref library_srvsw_stdif_posif */
    float32             frequencyRate;      /**< \brief Frequency rate, in rad/s/s */
} Ifx_VirtualPositionSensor_Config;

/** \} */

/** \addtogroup library_srvsw_sysse_virtualDevice_positionSensor_Functions
 * \{ */

/******************************************************************************/
/*-------------------------Global Function Prototypes-------------------------*/
/******************************************************************************/

/** \brief Initialises the Virtual position sensor
 * \param driver Virtual position sensor
 * \param config Configuration structure for Virtual position sensor
 * \return TRUE on success else FALSE
 *
 * \code
 *     // create module config
 *     Ifx_VirtualPositionSensor_Config config;
 *     Ifx_VirtualPositionSensor_initConfig(&config);
 *
 *     // initialize module
 *     //Ifx_VirtualPositionSensor sensor; // defined globally
 *
 *     Ifx_VirtualPositionSensor_init(&sensor, &config);
 * \endcode
 *
 */
IFX_EXTERN boolean Ifx_VirtualPositionSensor_init(Ifx_VirtualPositionSensor *driver, const Ifx_VirtualPositionSensor_Config *config);

/** \brief Initializes the configuration structure to default
 * \param config Configuration structure for incremental encoder
 * \return None
 *
 * see \ref Ifx_VirtualPositionSensor_init
 *
 */
IFX_EXTERN void Ifx_VirtualPositionSensor_initConfig(Ifx_VirtualPositionSensor_Config *config);

/** \} */

/** \addtogroup library_srvsw_sysse_virtualDevice_positionSensor_StdIf_Functions
 * \{ */

/******************************************************************************/
/*-------------------------Global Function Prototypes-------------------------*/
/******************************************************************************/

/** \brief \see IfxStdIf_Pos_GetAbsolutePosition
 * \param driver driver handle
 * \return absolute position
 */
IFX_EXTERN float32 Ifx_VirtualPositionSensor_getAbsolutePosition(Ifx_VirtualPositionSensor *driver);

/** \brief \see IfxStdIf_Pos_GetDirection
 * \param driver driver handle
 * \return direction
 */
IFX_EXTERN IfxStdIf_Pos_Dir Ifx_VirtualPositionSensor_getDirection(Ifx_VirtualPositionSensor *driver);

/** \brief \see IfxStdIf_Pos_GetFault
 * \param driver driver handle
 * \return Fault
 */
IFX_EXTERN IfxStdIf_Pos_Status Ifx_VirtualPositionSensor_getFault(Ifx_VirtualPositionSensor *driver);

/** \brief \see IfxStdIf_Pos_GetOffset
 * \param driver driver handle
 * \return offset address
 */
IFX_EXTERN sint32 Ifx_VirtualPositionSensor_getOffset(Ifx_VirtualPositionSensor *driver);

/** \brief \see IfxStdIf_Pos_GetPeriodPerRotation
 * \param driver driver handle
 * \return Period per rotation
 */
IFX_EXTERN uint16 Ifx_VirtualPositionSensor_getPeriodPerRotation(Ifx_VirtualPositionSensor *driver);

/** \brief \see IfxStdIf_Pos_GetPosition
 * \param driver driver handle
 * \return position
 */
IFX_EXTERN float32 Ifx_VirtualPositionSensor_getPosition(Ifx_VirtualPositionSensor *driver);

/** \brief \see IfxStdIf_Pos_GetRawPosition
 * \param driver driver handle
 * \return position in ticks
 */
IFX_EXTERN sint32 Ifx_VirtualPositionSensor_getRawPosition(Ifx_VirtualPositionSensor *driver);

/** \brief \see IfxStdIf_Pos_GetRefreshPeriod
 * \param driver driver handle
 * \return update period
 */
IFX_EXTERN float32 Ifx_VirtualPositionSensor_getRefreshPeriod(Ifx_VirtualPositionSensor *driver);

/** \brief \see IfxStdIf_Pos_GetTurn
 * \param driver driver handle
 * \return Returns the number of turns
 */
IFX_EXTERN sint32 Ifx_VirtualPositionSensor_getTurn(Ifx_VirtualPositionSensor *driver);

/** \brief \see IfxStdIf_Pos_GetSensorType
 * \param driver driver handle
 * \return sensor type
 */
IFX_EXTERN IfxStdIf_Pos_SensorType Ifx_VirtualPositionSensor_getSensorType(Ifx_VirtualPositionSensor *driver);

/** \brief \see IfxStdIf_Pos_GetSpeed
 * \param driver driver handle
 * \return speed
 */
IFX_EXTERN float32 Ifx_VirtualPositionSensor_getSpeed(Ifx_VirtualPositionSensor *driver);

/** \brief \see IfxStdIf_Pos_OnZeroIrq
 * \param driver driver handle
 * \return None
 */
IFX_EXTERN void Ifx_VirtualPositionSensor_onZeroIrq(Ifx_VirtualPositionSensor *driver);

/** \brief \see IfxStdIf_Pos_Reset
 * \param driver driver handle
 * \return None
 */
IFX_EXTERN void Ifx_VirtualPositionSensor_reset(Ifx_VirtualPositionSensor *driver);

/** \brief \see IfxStdIf_Pos_ResetFaults
 * \param driver driver handle
 * \return None
 */
IFX_EXTERN void Ifx_VirtualPositionSensor_resetFaults(Ifx_VirtualPositionSensor *driver);

/** \brief \see IfxStdIf_Pos_SetOffset
 * \param driver driver handle
 * \param offset offset
 * \return None
 */
IFX_EXTERN void Ifx_VirtualPositionSensor_setOffset(Ifx_VirtualPositionSensor *driver, sint32 offset);

/** \brief \see IfxStdIf_Pos_SetPosition
 * \param driver driver handle
 * \param position position
 * \return None
 */
IFX_EXTERN void Ifx_VirtualPositionSensor_setPosition(Ifx_VirtualPositionSensor *driver, float32 position);
/** \brief \see IfxStdIf_Pos_SetRawPosition
 * \param driver driver handle
 * \param position position
 * \return None
 */
IFX_EXTERN void Ifx_VirtualPositionSensor_setRawPosition(Ifx_VirtualPositionSensor *driver, sint32 position);

/** \brief \see IfxStdIf_Pos_SetSpeed
 * \param driver driver handle
 * \param speed speed
 * \return None
 */
IFX_EXTERN void Ifx_VirtualPositionSensor_setSpeed(Ifx_VirtualPositionSensor *driver, float32 speed);

/** \brief \see IfxStdIf_Pos_SetRefreshPeriod
 * \param driver driver handle
 * \param updatePeriod update period
 * \return None
 */
IFX_EXTERN void Ifx_VirtualPositionSensor_setRefreshPeriod(Ifx_VirtualPositionSensor *driver, float32 updatePeriod);

/** \brief \see IfxStdIf_Pos_Update
 * \param driver driver handle
 * \return None
 */
IFX_EXTERN void Ifx_VirtualPositionSensor_update(Ifx_VirtualPositionSensor *driver);

/** \} */

/******************************************************************************/
/*-------------------------Global Function Prototypes-------------------------*/
/******************************************************************************/

/** \brief Initializes the standard interface "Pos"
 * \param stdif Standard interface position object
 * \param driver Virtual position sensor
 * \return TRUE on success else FALSE
 */
IFX_EXTERN boolean Ifx_VirtualPositionSensor_stdIfPosInit(IfxStdIf_Pos *stdif, Ifx_VirtualPositionSensor *driver);

#endif /* IFX_VIRTUAL_POSITIONSENSOR_H */
