/*
 * Trial License - for use to evaluate programs for possible purchase as
 * an end-user only.
 *
 * File: Estimated_Torque_types.h
 *
 * Code generated for Simulink model 'Estimated_Torque'.
 *
 * Model version                  : 1.4937
 * Simulink Coder version         : 9.2 (R2019b) 18-Jul-2019
 * C/C++ source code generated on : Mon Mar 16 09:54:40 2020
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Infineon->TriCore
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_Estimated_Torque_types_h_
#define RTW_HEADER_Estimated_Torque_types_h_
#include "rtwtypes.h"
#ifndef DEFINED_TYPEDEF_FOR_OuterLoopControl_
#define DEFINED_TYPEDEF_FOR_OuterLoopControl_

typedef struct {
  real32_T ref_torque;
  real32_T Id;
  real32_T Iq;
  real32_T Foc_Idq_Real;
  real32_T FluxM;
  real32_T Foc_Idq_Imag;
  real32_T PolePair;
} OuterLoopControl;

#endif

#ifndef DEFINED_TYPEDEF_FOR_torque_
#define DEFINED_TYPEDEF_FOR_torque_

typedef struct {
  real32_T Est_torque;
} torque;

#endif

/* Forward declaration for rtModel */
typedef struct tag_RTM_Estimated_Torque_T RT_MODEL_Estimated_Torque_T;

#endif                                /* RTW_HEADER_Estimated_Torque_types_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
