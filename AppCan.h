/**
 * \file AppCan.h
 * \brief Application CAN
 *
 * \copyright Copyright (c) 2015 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * This file may be used, copied, and distributed, with or without modification, provided
 * that all copyright notices are retained; that all modifications to this file are
 * prominently noted in the modified file; and that this paragraph is not modified.
 *
 * \page doc_comm_canMessages CAN interface
 *
 * The CAN interface is used to configure, control and get the status of the ECU.
 *
 * The \ref doc_oneeye can be used to easily communicate with the ECU.
 *
 * # Control messages (CAN0)
 * During runtime, the ECU is controlled though periodically received CAN messages. The following parameters can be controlled:
 * - application mode
 * - clear error
 * - depending on the application mode: torque, speed, voltage and frequency, angle
 *
 * \note The command messages must be received at least every 100ms. If the messages are not received in the interval while in run state the application enter the emergency mode.
 *
 * <b>Message Id=\ref APPCAN_MSG_ID_RX_MODECMD</b>
 * signal       |  format | Offset (bit) | size (bit) | description
 * ----------   | ------- | ------------ | ---------- | -----------
 * modeCmd      | uint8   | 0            | 8          | Application mode. 0: passive, 1: current control, 2: torque control, 3: speed control, 4: open loop, 5:open loop with position, 6 test PI, 7: Hardware Pulse Mode, 254: Configuration, 255: Emergency
 * run          | bool    | 8            | 1          | Run command. 0: OFF, 1: RUN
 * clearErrorCmd| bool    | 9            | 1          | Clear error command. 0: no change, 1: clear the error state
 * pulseMode    | bool    | 10           | 1          | Pulse mode (test mode)
 * configExit   | bool    | 11           | 1          | Request to exit the config mode. 0: no request, 1: request to exit
 *
 * <b>Message Id=\ref APPCAN_MSG_ID_RX_MODECMD1 (modeCmd=1, motor type=2|3)</b>
 * signal       |  format | Offset (bit) | size (bit) | description
 * ----------   | ------- | ------------ | ---------- | -----------
 * currentIdCmd | float32 | 0            | 32         | Id current command in A peak
 * currentIqCmd | float32 | 32           | 32         | Iq current command in A peak
 *
 * <b>Message Id=\ref APPCAN_MSG_ID_RX_MODECMD2A (modeCmd=2, motor type=1)</b>
 * signal       |  format | Offset (bit) | size (bit) | description
 * ----------   | ------- | ------------ | ---------- | -----------
 * currentCmd   | float32 | 0            | 32         | Current command in A peak
 * slipCmd      | float32 | 32           | 32         | Slip command in percentage of the real speed
 *
 * <b>Message Id=\ref APPCAN_MSG_ID_RX_MODECMD2S (modeCmd=2)</b>
 * signal       |  format | Offset (bit) | size (bit) | description
 * ----------   | ------- | ------------ | ---------- | -----------
 * torqueCmd    | float32 | 0            | 32         | Torque command in Nm
 *
 * <b>Message Id=\ref APPCAN_MSG_ID_RX_MODECMD3 (modeCmd=3)</b>
 * signal       |  format | Offset (bit) | size (bit) | description
 * ----------   | ------- | ------------ | ---------- | -----------
 * speedCmd     | float32 | 0            | 32         | Mechanical speed command in rad/s
 *
 * <b>Message Id=\ref APPCAN_MSG_ID_RX_MODECMD4 (modeCmd=4, 5)</b>
 * signal       |  format | Offset (bit) | size (bit) | description
 * ----------   | ------- | ------------ | ---------- | -----------
 * amplitudeCmd | float32 | 0            | 32         | Voltage amplitude in % of VDC
 *
 * <b>Message Id=\ref APPCAN_MSG_ID_RX_MODECMD5A (modeCmd=pulse TDB)</b>
 * signal       |  format | Offset (bit) | size (bit) | description
 * ----------   | ------- | ------------ | ---------- | -----------
 * U top offset | float32 | 0            | 32         | Phase U top offset in s
 * U top on time| float32 | 32           | 32         | Phase U top on time in s
 *
 * <b>Message Id=\ref APPCAN_MSG_ID_RX_MODECMD5B (modeCmd=pulse TDB)</b>
 * signal          |  format | Offset (bit) | size (bit) | description
 * -------------   | ------- | ------------ | ---------- | -----------
 * U bottom offset | float32 | 0            | 32         | Phase U bottom offset in s
 * U bottom on time| float32 | 32           | 32         | Phase U bottom on time in s
 *
 * <b>Message Id=\ref APPCAN_MSG_ID_RX_MODECMD5C (modeCmd=pulse TDB)</b>
 * signal       |  format | Offset (bit) | size (bit) | description
 * ----------   | ------- | ------------ | ---------- | -----------
 * V top offset | float32 | 0            | 32         | Phase V top offset in s
 * V top on time| float32 | 32           | 32         | Phase V top on time in s
 *
 * <b>Message Id=\ref APPCAN_MSG_ID_RX_MODECMD5D (modeCmd=pulse TDB)</b>
 * signal          |  format | Offset (bit) | size (bit) | description
 * -------------   | ------- | ------------ | ---------- | -----------
 * V bottom offset | float32 | 0            | 32         | Phase V bottom offset in s
 * V bottom on time| float32 | 32           | 32         | Phase V bottom on time in s
 *
 * <b>Message Id=\ref APPCAN_MSG_ID_RX_MODECMD5E (modeCmd=pulse TDB)</b>
 * signal       |  format | Offset (bit) | size (bit) | description
 * ----------   | ------- | ------------ | ---------- | -----------
 * W top offset | float32 | 0            | 32         | Phase W top offset in s
 * W top on time| float32 | 32           | 32         | Phase W top on time in s
 *
 * <b>Message Id=\ref APPCAN_MSG_ID_RX_MODECMD5F (modeCmd=pulse TDB)</b>
 * signal          |  format | Offset (bit) | size (bit) | description
 * -------------   | ------- | ------------ | ---------- | -----------
 * W bottom offset | float32 | 0            | 32         | Phase W bottom offset in s
 * W bottom on time| float32 | 32           | 32         | Phase W bottom on time in s
 *
 *
 *
 * # Status messages (CAN0)
 * The ECU periodically sends status messages, with the following information:
 * - application mode, and status (errors, warnings)
 * - DC link voltage, phase currents
 * - mechanical speed, position and estimated torque
 * - depending on the application mode, the reference speed, torque or position
 * - debug values such as electrical angle park, electrical angle reverse park, ...
 *
 * ## Message send every 10 millisecond
 * <b>Message Id=\ref APPCAN_MSG_ID_TX_STAT1</b>
 * signal       |  format | Offset (bit) | size (bit) | description
 * ----------   | ------- | ------------ | ---------- | -----------
 * mode         | uint8   | 0            | 8          | see modeCmd
 * run          | bool    | 8            | 1          | Run command. 0: OFF, 1: RUN
 * error        | bool    | 9            | 1          | Error state: 0: no error, 1: error state
 * clearError   | bool    | 10           | 1          | Clear error status. 0: no change, 1: clear the error on going
 * configExit   | bool    | 11           | 1          | Request to exit the config mode. 0: no request, 1: request to exit
 * ecuReset     | bool    | 12           | 1          | ECU reset. This bit is set one time after an ECU reset
 * ecuState     | uint8   | 24           | 8          | ECU state, see \ref AppMode
 *
 * <b>Message Id=\ref APPCAN_MSG_ID_TX_STAT2</b>
 * signal       |  format | Offset (bit) | size (bit) | description
 * ----------   | ------- | ------------ | ---------- | -----------
 * torqueMeas   | float32 | 0            | 32         | Torque Nm
 * speedMeas    | float32 | 32           | 32         | Mechanical speed in rad/s
 *
 * <b>Message Id=\ref APPCAN_MSG_ID_TX_STAT3</b>
 * signal       |  format | Offset (bit) | size (bit) | description
 * ----------   | ------- | ------------ | ---------- | -----------
 * voltageDcMeas| float32 | 0            | 32         | DC link voltage in V
 * currentW     | float32 | 32            | 32         | Phase W current in A
 *
 * <b>Message Id=\ref APPCAN_MSG_ID_TX_STAT7</b>
 * signal       |  format | Offset (bit) | size (bit) | description
 * ----------   | ------- | ------------ | ---------- | -----------
 * currentU     | float32 | 0            | 32         | Phase U current in A
 * currentV     | float32 | 32           | 32         | Phase V current in A
 *
 * <b>Message Id=\ref APPCAN_MSG_ID_TX_STAT8</b>
 * signal       |  format | Offset (bit) | size (bit) | description
 * ----------   | ------- | ------------ | ---------- | -----------
 * elecPos      | float32 | 0           | 32         | Mechanical position in rad
 * mechPos      | float32 | 32           | 32         | Mechanical position in rad
 *
 * <b>Message Id=\ref APPCAN_MSG_ID_TX_STAT9</b>
 * signal       |  format | Offset (bit) | size (bit) | description
 * ----------   | ------- | ------------ | ---------- | -----------
 * mechSpeedR   | float32 | 0           | 32          | Mechanical speed in rpm from redundant sensor
 * mechPosR     | float32 | 32           | 32         | Mechanical position in rad from redundant sensor
 *
 * ## Message send every 100 millisecond
 * <b>Message Id=\ref APPCAN_MSG_ID_TX_STAT4</b>
 * signal       |  format | Offset (bit) | size (bit) | description
 * ----------   | ------- | ------------ | ---------- | -----------
 * tempIgbtMeas[0] | float32 | 0            | 32         | IGBT temp 0
 * tempIgbtMeas[1] | float32 | 32           | 32         | IGBT temp 1
 *
 * <b>Message Id=\ref APPCAN_MSG_ID_TX_STAT5</b>
 * signal       |  format | Offset (bit) | size (bit) | description
 * ----------   | ------- | ------------ | ---------- | -----------
 * tempIgbtMeas[2] | float32 | 0            | 32         | IGBT temp 2
 *
 * <b>Message Id=\ref APPCAN_MSG_ID_TX_STAT6</b>
 * signal       |  format | Offset (bit) | size (bit) | description
 * ----------   | ------- | ------------ | ---------- | -----------
 * tempMotorMeas| float32 | 0            | 32         | Motor temperature in degree C (Max value if multiple sensors)
 * tempBoardMeas| float32 | 32           | 32         | Board temperature in degree C (Max value if multiple sensors)
 *
 * # Messages send on CAN 1
 * The CAN 1 is used for data logging
 *
 * ## Message send every 1 millisecond, only valid if CFG_CURRENT_SENSOR_TEST=1
 * <b>Message Id=\ref APPCAN_MSG_ID_TX_LOG1</b>
 * signal          |  format | Offset (bit) | size (bit) | description
 * ----------      | ------- | ------------ | ---------- | -----------
 * logCurrent0U    | uint16  | 0            | 16         | Inverter 0 Current phase U (Q12.4)
 * logCurrent1U    | uint16  | 16           | 16         | Inverter 1 Current phase U (Q12.4)
 * logCurrentDiffU | uint16  | 32           | 16         | (Inverter 1 Current phase U - Inverter 0 Current phase U) (Q8.8)
 *
 * <b>Message Id=\ref APPCAN_MSG_ID_TX_LOG2</b>
 * signal       |  format | Offset (bit) | size (bit) | description
 * ----------   | ------- | ------------ | ---------- | -----------
 * logCurrent0V | uint16  | 0            | 16         | Inverter 0 Current phase V (Q12.4)
 * logCurrent1V | uint16  | 16           | 16         | Inverter 1 Current phase V (Q12.4)
 *
 * <b>Message Id=\ref APPCAN_MSG_ID_TX_LOG3</b>
 * signal       |  format | Offset (bit) | size (bit) | description
 * ----------   | ------- | ------------ | ---------- | -----------
 * logCurrent0W | uint16  | 0            | 16         | Inverter 0 Current phase W (Q12.4)
 * logCurrent1U | uint16  | 16           | 16         | Inverter 1 Current phase W (Q12.4)
 *
 * # CAN message raw data format example (Bit stream)
 * &nbsp;             | DB7          | DB6          | DB5          | DB4          | DB3          | DB2          | DB1          | DB0
 * ------------------ | ------------ | ------------ | ------------ | ------------ | ------------ | ------------ | ------------ | ----------
 * Bits               | bits[63..56] | bits[55..48] | bits[47..40] | bits[39..32] | bits[31..24] | bits[23..16] | bits[15..8]  | bits[7..0]
 * float (offset 0)   | 0            | 0            | 0            | 0            | 0xD          | 0xC          | 0xB          | 0xA
 * float (offset 32)  | 0xD          | 0xC          | 0xB          | 0xA          | 0            | 0            | 0            | 0
 * uint8 (offset 16)  | 0            | 0            | 0            | 0            | 0            | 0xA          | 0            | 0
 * boolean (offset 4) | 0            | 0            | 0            | 0            | 0            | 0xA          | 0            | 0x8
 *
 * float hex value: 0x0D0C0B0A (431.54119e-33 real)
 * uint8 hex value: 0x0A (10 decimal)
 * boolean hex value: 0x1 (1 decimal)
 *
 */
#ifndef APPCAN_H
#define APPCAN_H

#include "Can/Can/IfxCan_Can.h"

#define APPCAN_MSG_ID_RX_MODECMD   (0x01)
#define APPCAN_MSG_ID_RX_MODECMD1  (0x02)
#define APPCAN_MSG_ID_RX_MODECMD2A (0x03)
#define APPCAN_MSG_ID_RX_MODECMD2S (0x04)
#define APPCAN_MSG_ID_RX_MODECMD3  (0x05)
#define APPCAN_MSG_ID_TX_STAT1     (0x06)
#define APPCAN_MSG_ID_TX_STAT2     (0x07)
#define APPCAN_MSG_ID_TX_STAT3     (0x08)
#define APPCAN_MSG_ID_TX_STAT4     (0x09)
#define APPCAN_MSG_ID_TX_STAT5     (0x0A)
#define APPCAN_MSG_ID_TX_STAT6     (0x0B)
#define APPCAN_MSG_ID_TX_STAT7     (0x0F)
#define APPCAN_MSG_ID_TX_STAT8     (0x10)
#define APPCAN_MSG_ID_TX_STAT9     (0x11)
#define APPCAN_MSG_ID_RX_CFG       (0x0C)
#define APPCAN_MSG_ID_RX_CFG_REQ   (0x0D)
#define APPCAN_MSG_ID_RX_MODECMD4  (0x0E)

#define APPCAN_MSG_ID_TX_LOG0      (0x30)
#define APPCAN_MSG_ID_TX_LOG1      (0x31)
#define APPCAN_MSG_ID_TX_LOG2      (0x32)

#define APPCAN_MSG_ID_RX_MODECMD5A (0x20)
#define APPCAN_MSG_ID_RX_MODECMD5B (0x21)
#define APPCAN_MSG_ID_RX_MODECMD5C (0x22)
#define APPCAN_MSG_ID_RX_MODECMD5D (0x23)
#define APPCAN_MSG_ID_RX_MODECMD5E (0x24)
#define APPCAN_MSG_ID_RX_MODECMD5F (0x25)

#define APPCAN_MSG_ID_TX_CFG       (0x1C)
#define APPCAN_MSG_ID_RX_CONSOLE   (0xFE)
#define APPCAN_MSG_ID_TX_CONSOLE   (0xFF)

/******************************************************************************/
/*--------------------------------Enumerations--------------------------------*/
/******************************************************************************/

/******************************************************************************/
/*-----------------------------Data Structures--------------------------------*/
/******************************************************************************/

/** \brief Can information */
typedef struct
{
    struct
    {
        IfxCan_Can can[5];             /**< \brief CAN handle */
		IfxCan_Can_Node canNode[5];    /**< \brief CAN Node handles */
    }         drivers;

    Ifx_SizeT count;
} App_CanBasic;

/******************************************************************************/
/*------------------------------Global variables------------------------------*/
/******************************************************************************/

IFX_EXTERN App_CanBasic g_CanBasic;

/******************************************************************************/
/*-------------------------Function Prototypes--------------------------------*/
/******************************************************************************/

IFX_EXTERN void AppCan_init(void);
IFX_EXTERN void AppCan_initMessageCan1(void);

#endif /* APPCAN_H */
